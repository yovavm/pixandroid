package com.pixplit.android.volley;

import com.pixplit.android.util.bitmap.PixBitmapHolder;

public interface BitmapFetcherDelegate {
	public void onBitmapFetchFinished(String url, PixBitmapHolder bitmap);
	public void onBitmapFetchFailed();
}
