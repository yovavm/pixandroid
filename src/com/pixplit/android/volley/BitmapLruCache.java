package com.pixplit.android.volley;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.pixplit.android.PixApp;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public class BitmapLruCache extends LruCache<String, PixBitmapHolder> implements PixVolleyImageLoader.ImageCache {
	    public static int getDefaultLruCacheSize() {
	        final int memorySize = ((ActivityManager) PixApp.getContext().getSystemService(
                    Context.ACTIVITY_SERVICE)).getMemoryClass();
	        final int cacheSize = Math.round(0.16f * memorySize * 1024 * 1024);
	        return cacheSize;
	    }

	    public BitmapLruCache() {
	        this(getDefaultLruCacheSize());
	    }

	    public BitmapLruCache(int sizeInKiloBytes) {
	        super(sizeInKiloBytes);
	    }

	    @Override
	    protected int sizeOf(String key, PixBitmapHolder value) {
	        return getBitmapDrawableSize(value);
	    }

	    @Override
	    public PixBitmapHolder getBitmap(String url) {
	        return get(url);
	    }

	    @Override
	    public void putBitmap(String url, PixBitmapHolder bitmap) {
	    	bitmap.useBitmap("BitmapLruCache: put bitmap");
	        put(url, bitmap);
	    }
	    
	    /**
	     * Get the size in bytes of a bitmap.
	     * @param bitmap
	     * @return size in bytes
	     */
	    @TargetApi(12)
	    private static int getBitmapDrawableSize(PixBitmapHolder bitmapHolder) {
	    	if (bitmapHolder == null) return 0;

	    	Bitmap bitmap = bitmapHolder.getBitmap();
	    	if (bitmap == null) return 0;

	    	if (Utils.hasHoneycombMR1()) {
	            return bitmap.getByteCount();
	        }
	        // Pre HC-MR1
	        return bitmap.getRowBytes() * bitmap.getHeight();
	    }
	    
	    @Override
        protected void entryRemoved(boolean evicted, String key, PixBitmapHolder oldBitmap, PixBitmapHolder newBitmap) {
	    	if (PixDefs.recycleBitmaps) {
        		if (oldBitmap != null && 
        				oldBitmap != newBitmap) {
        			oldBitmap.releaseBitmap("entryRemoved in LruCache (with key:"+key+")");
        		}
        	}
	    }
	}