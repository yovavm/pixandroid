package com.pixplit.android.volley;

import java.util.HashMap;
import java.util.Map;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView.ScaleType;

import com.android.volley.Request.Priority;
import com.android.volley.VolleyError;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageListener;

public class PixImagesLoader {
	private static PixVolleyImageLoader mImageLoader;
		
	public static void init() {
		mImageLoader = new PixVolleyImageLoader(PixAppState.getState().getRequestQueue(), 
				PixAppState.getState().getBitmapCache());
	}
	
	// basic load image method, without adding additional headers
	public static ImageContainer loadImage(String url, BitmapFetcherDelegate delegate, Priority priority) {
		ImageContainer imageContainer = mImageLoader.get(url, getImageFetcher(delegate), priority, null);
		return imageContainer;
	}
	
	// loads an image into an image view
	public static ImageContainer loadImage(String url, PixImageView imageView, int defaultImageResId, 
			int errorImageResId, Priority priority, boolean withSpinner) {
		return loadImage(url, imageView, defaultImageResId, errorImageResId, priority, withSpinner, null, null);
	}
	
	// loads an image into an image view with a delegate
	public static ImageContainer loadImageWithDelegate(String url, PixImageView imageView, int defaultImageResId, 
			int errorImageResId, Priority priority, boolean withSpinner, final BitmapFetcherDelegate delegate) {
		return loadImage(url, imageView, defaultImageResId, errorImageResId, priority, withSpinner, null, delegate);
	}
	
	public static ImageContainer loadPrivateImage(String url, PixImageView imageView, int defaultImageResId, 
			int errorImageResId, Priority priority, boolean withSpinner, final BitmapFetcherDelegate delegate) {
		Map<String, String> headers = new HashMap<String, String>();
		String userToken = UserInfo.getUserToken();
		headers.put(PixHttpAPI.AUTH_HDR, userToken);
		return loadImage(url, imageView, defaultImageResId, errorImageResId, priority, withSpinner, headers, delegate);
	}
	
	// loads an image into an image view
	private static ImageContainer loadImage(String url, PixImageView imageView, int defaultImageResId, 
			int errorImageResId, Priority priority, boolean withSpinner, Map<String, String> hdrs,
			final BitmapFetcherDelegate delegate) {
		ImageContainer imageContainer = mImageLoader.get(url, getImageListener(imageView, withSpinner, 
				defaultImageResId, errorImageResId, delegate), priority, hdrs);
		return imageContainer;
	}
	
	// loads an image in the background
	public static ImageContainer loadImageInBg(String url) {
		if (!PixDefs.fetchBitmapsInBg) return null;
		ImageContainer imageContainer = mImageLoader.getInBg(url, getImageFetcher(null), Priority.LOW, null);
		return imageContainer;
	}

	// loads a private image in the background
	public static ImageContainer loadPrivateImageInBg(String url) {
		if (!PixDefs.fetchBitmapsInBg) return null;
		
		Map<String, String> headers = new HashMap<String, String>();
		String userToken = UserInfo.getUserToken();
		headers.put(PixHttpAPI.AUTH_HDR, userToken);
		
		ImageContainer imageContainer = mImageLoader.getInBg(url, getImageFetcher(null), Priority.LOW, headers);
		return imageContainer;
	}

	private static ImageListener getImageListener(final PixImageView imageView,
												  final boolean withSpinner, 
												  final int defaultImageResId, 
												  final int errorImageResId,
												  final BitmapFetcherDelegate delegate) {
    	return new ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	if (withSpinner) {
                	// hide the spinner
                	imageView.setAnimation(null);
                	imageView.setImageBitmap(null);
                	imageView.setScaleType(ScaleType.CENTER_CROP);
                }
            	
                if (errorImageResId != 0) {
                    imageView.setImageResource(errorImageResId);
                }
            	
                if (delegate != null) {
					delegate.onBitmapFetchFailed();
				}
            }

            @Override
            public void onResponse(ImageContainer response, boolean isImmediate) {
            	PixBitmapHolder bitmapHolder = response.getBitmapHolder();
                if (bitmapHolder != null) {
                	// final response
                	if (withSpinner) {
                		imageView.setScaleType(ScaleType.CENTER_CROP);
                		imageView.setAnimation(null);
                	}
                	imageView.setImageBitmapHolder(bitmapHolder);
                	
                    if (delegate != null) {
    					delegate.onBitmapFetchFinished(response.getRequestUrl(), bitmapHolder);
    				}

                } else {
                	// non final response
                	if (withSpinner) {
	                	Animation spinnerAnim = AnimationUtils.loadAnimation(imageView.getContext(), R.anim.spinning_anim);
	                	imageView.setImageResource(R.drawable.spinner);
	                	imageView.setScaleType(ScaleType.CENTER_INSIDE);
	                	imageView.startAnimation(spinnerAnim);
	                } else if (defaultImageResId != 0) {
	                    imageView.setImageResource(defaultImageResId);
	                }
                }
            }
        };
    }
	
	private static ImageListener getImageFetcher(final BitmapFetcherDelegate delegate) {
		return new ImageListener() {
			
			@Override
			public void onErrorResponse(VolleyError error) {
				if (delegate != null) {
					delegate.onBitmapFetchFailed();
				}
			}
			
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				if (delegate != null) {
					if (response != null) {
						PixBitmapHolder bitmapHolder = response.getBitmapHolder();
						if (bitmapHolder != null) {
							delegate.onBitmapFetchFinished(response.getRequestUrl(), bitmapHolder);
						}
					} else {
						// response is null
						if (!isImmediate) {
							delegate.onBitmapFetchFailed();
						}
					}
				}
			}
		};
	}
}
