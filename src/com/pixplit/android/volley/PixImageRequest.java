package com.pixplit.android.volley;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;

public class PixImageRequest extends ImageRequest{

	private Priority mPriority;
	private static final Map<String, String> DEFAULT_HEADERS = new HashMap<String, String>();
	private Map<String, String> mHeaders;
	
	public PixImageRequest(String url, Listener<Bitmap> listener, int maxWidth,
			int maxHeight, Config decodeConfig, ErrorListener errorListener, Priority priority,
			Map<String, String> hdrs) {
		super(url, listener, maxWidth, maxHeight, decodeConfig, errorListener);
		this.mPriority = priority;
		if (hdrs != null) {
			this.mHeaders = hdrs;
		} else {
			this.mHeaders = DEFAULT_HEADERS;
		}
	}

	@Override
    public Priority getPriority() {
        return mPriority;
    }
	
	@Override
	public Map<String, String> getHeaders(){
		return mHeaders;
	}
	
//	@Override
//	public String getCacheKey() {
//		return super.getCacheKey()+"P:"+mPriority.toString();
//	}
}
