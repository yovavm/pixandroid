package com.pixplit.android.capture;

public enum SplitBorderType {
	BORDER_TYPE_NONE,
	BORDER_TYPE_DEFAULT,
	BORDER_TYPE_WIDE
}
