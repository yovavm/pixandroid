package com.pixplit.android.capture;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.pixplit.android.R;
import com.pixplit.android.capture.SplitPartFrame.SplitPartFrameDelegate;
import com.pixplit.android.globals.PixCompositionComponent;
import com.pixplit.android.globals.PixCompositionType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.CreateSplitPreviewSpinner;
import com.pixplit.android.ui.elements.PixBorder;

public class SplitPreviewFrame extends RelativeLayout implements SplitPartFrameDelegate{
	
//	private static final String TAG = "CreateSplitPreviewFrame";
	public static final int PART_SCALE_TYPE_DEFAULT = 1;
	public static final int PART_SCALE_TYPE_SCALE = 2;
	
	private PixCompositionType compositionType;
	private Bitmap compositionImage;
	int previewFrameWidth;
	int previewFrameHeight;
	private SplitPartFrame[] splitPartsFrames;
	final int ACTIVE_FRAME_NONE = -1;
	private PixBorder border = null;
	private CreateSplitPreviewSpinner previewSpinner;
	
	public SplitPreviewFrame(Context context, int previewWidth, int previewHeight, 
									PixCompositionType compositionType, final SplitBorderType borderType, Bitmap joinedComp) {
		super(context);
		if (compositionType != null) {
			this.compositionType = compositionType;
			this.compositionImage = joinedComp;
			this.previewFrameWidth = previewWidth;
			this.previewFrameHeight = previewHeight;
			splitPartsFrames = new SplitPartFrame[this.compositionType.getMaxComponentsForType()];
			init(borderType);
		}
		previewSpinner = new CreateSplitPreviewSpinner(context, previewWidth, previewHeight);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		previewSpinner.hide();
		addView(previewSpinner, params);
	}
	
	/*
	 *  Should not be used - this is a constructor for XML inflating which should not be used on this object. 
	 *  only here to avoid warning
	 */
	public SplitPreviewFrame(Context context, AttributeSet attrs) {
		super(context, attrs);
		/*
		 *  Should not be used - this is a constructor for XML inflating which should not be used on this object. 
		 *  only here to avoid warning
		 */
	}
	
	public void show() {
		previewSpinner.hide();
	}
	
	public void hide() {
		previewSpinner.bringToFront();
		previewSpinner.show();
	}
	
	public Bitmap getActivePartBitmap() {
		int activeFrame = getActivePartNum();
		if (activeFrame != ACTIVE_FRAME_NONE) {
			Rect partDimen = splitPartsFrames[activeFrame].getDisplayRelativeRect();
			return splitPartsFrames[activeFrame].getPartBitmap(partDimen, false, false, false);
		}
		return null;
	}

//	public Bitmap getActivePartScaleInfo() {
//		int activeFrame = getActivePartNum();
//		if (activeFrame != ACTIVE_FRAME_NONE) {
//			return splitPartsFrames[activeFrame].getPartBitmap();
//		}
//		return null;
//	}

	public int getActivatePartIndex() {
		int activePartIndex = getActivePartNum();
		if (activePartIndex != ACTIVE_FRAME_NONE) {
			activePartIndex++;
		}
		return activePartIndex;
	}
	
	private int getActivePartNum() {
		for (int i = 0; (i < splitPartsFrames.length  && splitPartsFrames[i] != null); i++) {
			if (splitPartsFrames[i].isActive()) {
				return i;
			}
		}
		return ACTIVE_FRAME_NONE;
	}
	
	public Rect getActivePartDisplayRelativeRect() {
		int activeFrame = getActivePartNum();
		if (activeFrame != ACTIVE_FRAME_NONE) {
			return splitPartsFrames[activeFrame].getDisplayRelativeRect();
		}
		return null;
	}

	private Bitmap createFinalSplitBitmap(boolean withBorder) {
		int splitWidth = PixDefs.SPLIT_BITMAP_WIDTH;
		int splitHeight = PixDefs.SPLIT_BITMAP_HIGHT;
		boolean addJoinIcon = true;
		Bitmap splitBitmap = null;
		try {
			splitBitmap = Bitmap.createBitmap(splitWidth, splitHeight, Config.ARGB_8888);
		} catch (Exception e){
			//Utils.pixLog(TAG, "createSplitBitmap: createBitmap failed");
			e.printStackTrace();
		}
		if (splitBitmap != null) {
		    Canvas canvas = new Canvas(splitBitmap); 
		    for (int i = 0; (i < splitPartsFrames.length  && splitPartsFrames[i] != null); i++) {
		    	Rect partDimen = splitPartsFrames[i].getPartSplitRelativeRect();
		    	if (partDimen != null) {
			    	Bitmap partBitmap = null;
			    	partBitmap = splitPartsFrames[i].getPartBitmap(partDimen, withBorder, addJoinIcon, true);
			    	if (partBitmap != null) {
			    		canvas.drawBitmap(partBitmap, null, partDimen, null);
			    	}
			    	if (!splitPartsFrames[i].isActive() && !splitPartsFrames[i].isClosed()) {
			    		// this part is open and therefore added the join icon. no need to add it again
			    		addJoinIcon = false;
			    	}
		    	}
			}
		    if (withBorder) {
		    	setFrameBorder(SplitBorderType.BORDER_TYPE_WIDE);
		    	if (border != null) {
		    		border.layout(0, 0, splitWidth-1, splitHeight-1);
		    		border.draw(canvas);
		    	}
		    }
		}
		return splitBitmap;
	}
	
	public void setBorder(SplitBorderType borderType) {
		for (int i = 0; (i < splitPartsFrames.length  && splitPartsFrames[i] != null); i++) {
			splitPartsFrames[i].setBorder(borderType);
		}
		setFrameBorder(borderType);
	}
	
	public void setFrameBorder(SplitBorderType borderType) {
		switch (borderType) {
		case BORDER_TYPE_WIDE:
			if (border != null) {
				border.setVisibility(View.VISIBLE);
				border.bringToFront();
			} else {
				// create the border
				border = new PixBorder(getContext(), getContext().getResources().getDimensionPixelOffset(R.dimen.pix_split_border_width)*2);
				border.setBackgroundColor(getResources().getColor(R.color.transparent));
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
						RelativeLayout.LayoutParams.MATCH_PARENT);
				addView(border, params);
			}
			break;
			
		case BORDER_TYPE_DEFAULT:
		case BORDER_TYPE_NONE:
			if (border != null) {
				border.setVisibility(View.INVISIBLE);
			}
		default:
			break;
		}
	}
	
	public void setPartBitmap(int partIndex, Bitmap partBitmap, int partType) {
		if (partIndex > 0 && 
				partIndex <= splitPartsFrames.length && 
				splitPartsFrames[partIndex-1] != null && 
				!splitPartsFrames[partIndex-1].isClosed() && 
				partBitmap != null) {
			this.activateSplitPart(partIndex);
			for (int i = 0; (i < splitPartsFrames.length  && splitPartsFrames[i] != null); i++) {
				if (splitPartsFrames[i].isActive()) {
					if (partType == PART_SCALE_TYPE_SCALE) {
						splitPartsFrames[i].setScalableBitmap(partBitmap);						
					} else {
						splitPartsFrames[i].setBitmap(partBitmap);
					}
				} else if (!splitPartsFrames[i].isClosed()){
					splitPartsFrames[i].disablePart();
				}
			}
		}
	}
	
	@Override
	public void activateSplitPart(int partIndex) {
		for (int i = 0; (i < splitPartsFrames.length && splitPartsFrames[i] != null); i++) {
			if (!splitPartsFrames[i].isClosed()) {
				if (i == partIndex-1) {
					splitPartsFrames[i].setPartState(SplitPartFrame.SPLIT_PART_STATE_ACTIVE);
				} else {
					splitPartsFrames[i].setPartState(SplitPartFrame.SPLIT_PART_STATE_NOT_ACTIVE);
				}
			}
		}
	}
	
	public static Bitmap createFinalSplit(Context context, PixCompositionType compType, Bitmap joinedComp, Bitmap newPartBitmap, int newComponentIndex,
											boolean isCompleted, boolean hasBorder) {
		if (context != null && compType != null) {
			// create the final split image
			boolean withBorder = false;
			int splitWidth = PixDefs.SPLIT_BITMAP_WIDTH;
			int splitHeight = PixDefs.SPLIT_BITMAP_HIGHT;
			
			if (isCompleted && hasBorder) {
				withBorder = true;
			}
			SplitBorderType borderType = withBorder ? SplitBorderType.BORDER_TYPE_WIDE : SplitBorderType.BORDER_TYPE_NONE;
			SplitPreviewFrame previewFrame = new SplitPreviewFrame(context, 
					splitWidth, splitHeight, compType, borderType, joinedComp);
			// validate that the active part doesn't have a participant already
			PixCompositionComponent activeComponent = compType.getComponentWithIndex(newComponentIndex);
			if (activeComponent != null && activeComponent.getParticipant() == null) {
				previewFrame.setPartBitmap(newComponentIndex, newPartBitmap, PART_SCALE_TYPE_DEFAULT);
			}
			return previewFrame.createFinalSplitBitmap(withBorder);
		}
		return null;
	}
	
	@Override
	public int getDisplayWidth() {
		return previewFrameWidth;
	}
	@Override
	public int getDisplayHeight() {
		return previewFrameHeight;
	}
	
	private void init(SplitBorderType borderType) {
		if (compositionType != null) {
			switch (compositionType.getCompFramesetType()) {
			case Pix1Frameset:
				createPix1FramesetFrame();
				break;
			case Pix2HorizontalFrameset:
				createPix2HorizontalFramesetFrame();
				break;
			case Pix2QuarterBottomFrameset:
				createPix2QuarterBottomFramesetFrame();
				break;
			case Pix2QuarterLeftFrameset:
				createPix2QuarterLeftFramesetFrame();
				break;
			case Pix2QuarterRightFrameset:
				createPix2QuarterRightFramesetFrame();
				break;
			case Pix2QuarterTopFrameset:
				createPix2QuarterTopFramesetFrame();
				break;
			case Pix2ThirdBottomFrameset:
				createPix2ThirdBottomFramesetFrame();
				break;
			case Pix2ThirdLeftFrameset:
				createPix2ThirdLeftFramesetFrame();
				break;
			case Pix2ThirdRightFrameset:
				createPix2ThirdRightFramesetFrame();
				break;
			case Pix2ThirdTopFrameset:
				createPix2ThirdTopFramesetFrame();
				break;
			case Pix2VerticalFrameset:
				createPix2VerticalFramesetFrame();
				break;
			case Pix3HalfBottomFrameset:
				createPix3HalfBottomFramesetFrame();
				break;
			case Pix3HalfLeftFrameset:
				createPix3HalfLeftFramesetFrame();
				break;
			case Pix3HalfRightFrameset:
				createPix3HalfRightFramesetFrame();
				break;
			case Pix3HalfTopFrameset:
				createPix3HalfTopFramesetFrame();
				break;
			case Pix3HorizontalFrameset:
				createPix3HorizontalFramesetFrame();
				break;
			case Pix3QuarterBottomFrameset:
				createPix3QuarterBottomFramesetFrame();
				break;
			case Pix3QuarterHorizontalFrameset:
				createPix3QuarterHorizontalFramesetFrame();
				break;
			case Pix3QuarterLeftFrameset:
				createPix3QuarterLeftFramesetFrame();
				break;
			case Pix3QuarterRightFrameset:
				createPix3QuarterRightFramesetFrame();
				break;
			case Pix3QuarterTopFrameset:
				createPix3QuarterTopFramesetFrame();
				break;
			case Pix3QuarterVerticalFrameset:
				createPix3QuarterVerticalFramesetFrame();
				break;
			case Pix3VerticalFrameset:
				createPix3VerticalFramesetFrame();
				break;
			case Pix4Frameset:
				createPix4FramesetFrame();
				break;
			case Pix4HalfBottomFrameset:
				createPix4HalfBottomFramesetFrame();
				break;
			case Pix4HalfLeftFrameset:
				createPix4HalfLeftFramesetFrame();
				break;
			case Pix4HalfRightFrameset:
				createPix4HalfRightFramesetFrame();
				break;
			case Pix4HalfTopFrameset:
				createPix4HalfTopFramesetFrame();
				break;
			case Pix4HorizontalFrameset:
				createPix4HorizontalFramesetFrame();
				break;
			case Pix4SixthHorizontalLeft:
				createPix4SixthHorizontalLeftFrame();
				break;
			case Pix4SixthHorizontalRight:
				createPix4SixthHorizontalRightFrame();
				break;
			case Pix4SixthVerticalLeft:
				createPix4SixthVerticalLeftFrame();
				break;
			case Pix4SixthVerticalRight:
				createPix4SixthVerticalRightFrame();
				break;
			case Pix4ThirdBottomFrameset:
				createPix4ThirdBottomFramesetFrame();
				break;
			case Pix4ThirdLeftFrameset:
				craetePix4ThirdLeftFramesetFrame();
				break;
			case Pix4ThirdRightFrameset:
				createPix4ThirdRightFramesetFrame();
				break;
			case Pix4ThirdTopFrameset:
				createPix4ThirdTopFramesetFrame();
				break;
			case Pix4TwoSixthBottomFrameset:
				createPix4TwoSixthBottomFramesetFrame();
				break;
			case Pix4TwoSixthLeftFrameset:
				createPix4TwoSixthLeftFramesetFrame();
				break;
			case Pix4TwoSixthRightFrameset:
				createPix4TwoSixthRightFramesetFrame();
				break;
			case Pix4TwoSixthTopFrameset:
				createPix4TwoSixthTopFramesetFrame();
				break;
			case Pix4VerticalFrameset:
				createPix4VerticalFramesetFrame();
				break;
			default:
				//Utils.pixLog(TAG, "Frame type is not supported:"+compositionType.getCompFramesetType());
				break;
			
			}

			// set the first open part as active
			for (int i = 0; (i < splitPartsFrames.length  && splitPartsFrames[i] != null); i++) {
				if (!splitPartsFrames[i].isClosed()) {
					activateSplitPart(splitPartsFrames[i].getIndex());
					break;
				}
			}
		}
		
		this.setBorder(borderType);
	}
	
//	private void createSplitParts() {
//		if (this.compositionType != null) {
//			RelativeLayout.LayoutParams params;
//			Rect partPreviewRect;
//			
//			for (int i=0; i<this.compositionType.getMaxComponentsForType(); i++) {
//				splitPartsFrames[i] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(i+1), this.compositionImage, this);
//				partPreviewRect = splitPartsFrames[i].getDisplayRelativeRect();
//				splitPartsFrames[i].setX(partPreviewRect.left);
//				splitPartsFrames[i].setY(partPreviewRect.top);
//				params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
//				addView(splitPartsFrames[i], params);
//			}
//		}
//	}
	
	private void createPix2VerticalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix1FramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	}

	private void createPix2HorizontalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}

	private void createPix2QuarterBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}

	private void createPix2ThirdBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix2ThirdLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix2ThirdRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix2ThirdTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix2QuarterLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix2QuarterTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}

	private void createPix2QuarterRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
	}
	
	private void createPix3QuarterLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3QuarterRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3QuarterVerticalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3VerticalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3HorizontalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3QuarterBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3HalfLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3HalfRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3QuarterTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3HalfTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3HalfBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	}
	
	private void createPix3QuarterHorizontalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;
		
		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
				
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[1], params);
		
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[2], params);
	}

	private void createPix4FramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4HalfBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4HalfLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4HalfRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4HalfTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4VerticalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4HorizontalFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4SixthHorizontalLeftFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[3], params);
	
	}
	
	private void createPix4SixthHorizontalRightFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4SixthVerticalLeftFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4SixthVerticalRightFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4ThirdBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void craetePix4ThirdLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4ThirdRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_TOP, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4ThirdTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[1].getIndex());
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4TwoSixthBottomFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[1].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4TwoSixthLeftFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[2].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4TwoSixthRightFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		addView(splitPartsFrames[3], params);
	}
	
	private void createPix4TwoSixthTopFramesetFrame() {
		RelativeLayout.LayoutParams params;
		Rect partPreviewRect;

		splitPartsFrames[0] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(1), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[0].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		addView(splitPartsFrames[0], params);
	
		splitPartsFrames[1] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(2), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[1].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.RIGHT_OF, splitPartsFrames[0].getIndex());
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		addView(splitPartsFrames[1], params);
	
		splitPartsFrames[2] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(3), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[2].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[0].getIndex());
		addView(splitPartsFrames[2], params);
	
		splitPartsFrames[3] = new SplitPartFrame(getContext(), this.compositionType.getComponentWithIndex(4), this.compositionImage, this);
		partPreviewRect = splitPartsFrames[3].getDisplayRelativeRect();
		params = new RelativeLayout.LayoutParams(partPreviewRect.width(), partPreviewRect.height());
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, splitPartsFrames[2].getIndex());
		addView(splitPartsFrames[3], params);
	}
}
