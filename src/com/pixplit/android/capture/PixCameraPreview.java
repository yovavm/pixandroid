package com.pixplit.android.capture;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.pixplit.android.PixApp;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.PixGestureDetector;
import com.pixplit.android.util.Utils;

public class PixCameraPreview extends SurfaceView implements SurfaceHolder.Callback{
//  	private static final String TAG = "CameraPreview";
	
	private SurfaceHolder mHolder;
    private Camera mCamera;
    private int cameraId;
    private boolean tapToFocusEnabled;
    private int mPreviewDimen;
    private ImageView focusTargetIndicator;

    @SuppressWarnings("deprecation")
	public PixCameraPreview(Context context, Camera camera, int cameraId, int previewDimen, ImageView focusTargetIndicator) {
        super(context);
        this.mCamera = camera;
        this.cameraId = cameraId;
        this.mPreviewDimen = previewDimen;
        this.focusTargetIndicator = focusTargetIndicator;
        this.checkForTapToFocus();
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public PixCameraPreview(Context context) {
    	super(context);
        // needed to avoid compilation warnings
    }
    
    final GestureDetector gestureDetector = new GestureDetector(getContext(),  new PixGestureDetector() {
		public void onSwipeRight() {}
		public void onSwipeLeft() {}
		public void onDoubleTapEvnt(MotionEvent e) {
			focusOnTap(e);
		}
		public void onSingleTap(MotionEvent e) {
			focusOnTap(e);
		}
	});
	final View.OnTouchListener gestureListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }
    };
    
    @TargetApi(14)
	private void checkForTapToFocus() {
    	this.tapToFocusEnabled = false;
    	if (Utils.hasIceCreamSandwich()) {
    		try {
	    		Camera.Parameters parameters = mCamera.getParameters(); 
	            if (parameters != null) {
	            	if (parameters.getMaxNumFocusAreas() > 0) {
	            		this.tapToFocusEnabled = true;
	            		
	            		// add double tap for focus            			
	            		this.setOnTouchListener(gestureListener);
	            	}
	            }
    		} catch (Exception e) {
    			//Utils.pixLog(TAG, "failed to get parameters:"+e);
    			e.printStackTrace();
    		}
    	}
    }
    
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		 // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (Exception e) {
            //Utils.pixLog(TAG, "Error setting camera preview: " + e.getMessage());
        }
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
        	// ignore: tried to stop a non-existent preview
        	e.printStackTrace();
        }
        
        this.setCameraDisplayOrientation(this.cameraId, this.mCamera);
        
        Size cameraPreviewSize = null;
        try {
    		Camera.Parameters parameters = mCamera.getParameters(); 
    		if (parameters != null) {
	    		cameraPreviewSize = parameters.getPreviewSize();
    		}
        } catch (Exception e){
            //Utils.pixLog(TAG, "Error setting camera parameters: " + e.getMessage());
        }
        
		if (cameraPreviewSize != null) {
			int splitWidth = mPreviewDimen;
			float previewRatio = splitWidth / (float) cameraPreviewSize.height;
			int surfaceWidth = (int) Math.floor(cameraPreviewSize.height*previewRatio);
			int surfaceHeight = (int)Math.floor(cameraPreviewSize.width*previewRatio);
    	    LayoutParams params = new LayoutParams(surfaceWidth, surfaceHeight);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
        	setLayoutParams(params);
        }
    
	    try {
    		Camera.Parameters parameters = mCamera.getParameters(); 
    		if (parameters != null) {
	    		float cameraPreviewRatio = (float)(cameraPreviewSize.width/(float)cameraPreviewSize.height);
	    		Size pictureSize = parameters.getPictureSize();
	    		if (pictureSize != null &&
	    				(pictureSize.width < PixDefs.SPLIT_BITMAP_MIN_SCALE_WIDTH ||
	    				pictureSize.height < PixDefs.SPLIT_BITMAP_MIN_SCALE_HIGHT ||
	    				pictureSize.width/(float)pictureSize.height != cameraPreviewRatio)) {
	    			List<Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
	    			if (supportedPictureSizes != null) {
	    				Size minLargerSize = null;
			        	for (Size supportedSize : supportedPictureSizes) {
			        		if (supportedSize != null && 
			        				supportedSize.width >= PixDefs.SPLIT_BITMAP_MIN_SCALE_WIDTH &&
			        				supportedSize.height >= PixDefs.SPLIT_BITMAP_MIN_SCALE_HIGHT &&
			        				supportedSize.width/(float)supportedSize.height == cameraPreviewRatio) {
			        			if (minLargerSize == null) {
			        				minLargerSize = supportedSize;
			        			} else {
			        				if (supportedSize.width <= minLargerSize.width &&
			        						supportedSize.height <= minLargerSize.height) {
			        					minLargerSize = supportedSize;
			        				}
			        			}
			        		}
			        	}
			        	if (minLargerSize != null) {
			        		parameters.setPictureSize(minLargerSize.width, minLargerSize.height);
			        		mCamera.setParameters(parameters);
			        	}
		        	}
	    		}
    		}
        } catch (Exception e){
            //Utils.pixLog(TAG, "Error setting camera parameters: " + e.getMessage());
        }
        
	    if (!tapToFocusEnabled) {
	        try {
	    		Camera.Parameters parameters = mCamera.getParameters(); 
	    		if (parameters != null && cameraPreviewSize != null) {
		        	parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
		        	mCamera.setParameters(parameters);
		        	mCamera.autoFocus(pixAutoFocusCallback);
	    		}
	    	} catch (Exception e){
		        //Utils.pixLog(TAG, "Error setting camera parameters: " + e.getMessage());
	    	}
	    }
                
        try {
    		Camera.Parameters parameters = mCamera.getParameters(); 
    		if (parameters != null) {
	    		if (parameters.getJpegQuality() < 100) {
	    			parameters.setJpegQuality(100);
	    			mCamera.setParameters(parameters);
	    		}
    		}
    	} catch (Exception e){
            //Utils.pixLog(TAG, "Error setting camera parameters: " + e.getMessage());
        }
        
        try {
			// start preview with new settings
	        mCamera.setPreviewDisplay(mHolder);
	        mCamera.startPreview();
	    } catch (Exception e){
	        //Utils.pixLog(TAG, "Error starting camera preview: " + e.getMessage());
	    }
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
	    if (mCamera != null) {
	        /**
	         * Call stopPreview() to stop updating the preview surface.
	         */
	    	try {
	            mCamera.stopPreview();
	        } catch (Exception e){
	          // ignore: tried to stop a non-existent preview
	        }
	    }
	}
	
	/**
	 * Calling this method makes the camera image show in the same orientation as the display. 
	 * NOTE: This method is not allowed to be called during preview.
	 * 
	 * @param cameraId
	 * @param camera
	 */
	@SuppressLint("NewApi")
	public void setCameraDisplayOrientation(int cameraId, android.hardware.Camera camera) {
	     int rotation = ((WindowManager)PixApp.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
	             .getRotation();
	     int degrees = 0;
	     switch (rotation) {
	         case Surface.ROTATION_0: degrees = 0; break;
	         case Surface.ROTATION_90: degrees = 90; break;
	         case Surface.ROTATION_180: degrees = 180; break;
	         case Surface.ROTATION_270: degrees = 270; break;
	     }

	     int result;
	     if (Utils.hasGingerbread()) {
		     android.hardware.Camera.CameraInfo info =
		             new android.hardware.Camera.CameraInfo();
		     android.hardware.Camera.getCameraInfo(cameraId, info);
		     if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
		         result = (info.orientation + degrees) % 360;
		         result = (360 - result) % 360;  // compensate the mirror
		     } else {  // back-facing
		         result = (info.orientation - degrees + 360) % 360;
		     }
	     } else {
	    	 // on API 8 and lower devices
	    	 if (PixApp.getContext().getResources().getConfiguration().orientation !=Configuration.ORIENTATION_LANDSCAPE) {
	    		 result = 90;
	         } else {
	        	 result = 0;
	         }
	     }
	     try {
	    	 camera.setDisplayOrientation(result);
	     } catch (Exception e) {
	    	 // may fail on old OS versions. ignore it.
	    	 e.printStackTrace();
	     }
	 }
	
	

	@TargetApi(14)
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean handled = false;
		
		if (this.tapToFocusEnabled) {
			final int action = event.getAction();
	        switch (action & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	        	focusOnTap(event);
	            
	        	handled = true;
	        	break;
	        default:
	        	break;
	        }
		}
		return handled;
	}
	
	private AutoFocusCallback pixAutoFocusCallback = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			//Utils.pixLog(TAG, "auto focus:"+success);
		}
	};
	
	@TargetApi(11)
	private void updateUiWithFocusTarget(int x, int y) {
		if (!Utils.hasHoneycomb()) {
			return; // set focus area was not available before honeycomb. also needed for setX and setY API
		}
		if (this.focusTargetIndicator != null) {
			int indicatorWidth = this.focusTargetIndicator.getWidth();
			int indicatorHeight = this.focusTargetIndicator.getHeight();
			Drawable indicatorDrawable = this.focusTargetIndicator.getBackground();
			if (indicatorDrawable != null) {
				indicatorWidth = indicatorDrawable.getIntrinsicWidth();
				indicatorHeight = indicatorDrawable.getIntrinsicHeight();
			}
			int maxX = PixAppState.getState().getFrameWidth();
			int maxY = PixAppState.getState().getFrameHeight();
			
			int displayX = Utils.clamp(x - indicatorWidth/2, 0, maxX);
			int displayY = Utils.clamp(y - indicatorHeight/2, 0, maxY);
			this.focusTargetIndicator.setX(displayX);
			this.focusTargetIndicator.setY(displayY);
			this.focusTargetIndicator.setVisibility(View.VISIBLE);
			
			// animation for focus on target
			AnimationSet animSet = new AnimationSet(true);
			ScaleAnimation scaleUpAnimation = new ScaleAnimation(1f, 1.25f, 1f, 1.25f, 
					Animation.ABSOLUTE, displayX+indicatorWidth/2, Animation.ABSOLUTE, displayY+indicatorHeight/2);
			scaleUpAnimation.setDuration(150);
			scaleUpAnimation.setStartOffset(0);
			animSet.addAnimation(scaleUpAnimation);
			ScaleAnimation scaleDownAnimation = new ScaleAnimation(1f, 0.8f, 1f, 0.8f, 
					Animation.ABSOLUTE, displayX+indicatorWidth/2, Animation.ABSOLUTE, displayY+indicatorHeight/2);
			scaleDownAnimation.setDuration(150);
			scaleDownAnimation.setStartOffset(150);
			animSet.addAnimation(scaleDownAnimation);
			this.focusTargetIndicator.setAnimation(animSet);
			
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if (focusTargetIndicator != null) {
						focusTargetIndicator.setVisibility(View.GONE);
					}
				}
			}, 800);
		}
	}
	
	@TargetApi(14)
	private void focusOnTap(MotionEvent event) {
		if (this.tapToFocusEnabled) {
			final int action = event.getAction();
			final int focusDimen = 50;
	        switch (action & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	            final int y = (int)event.getRawY();
	            final int x = (int)event.getRawX();
	
	            List<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
	
	            Rect areaRect1 = new Rect(x-focusDimen, y-focusDimen, x+focusDimen, y+focusDimen);
	            focusAreas.add(new Camera.Area(areaRect1, 1)); 
	            try {
		            if (mCamera != null) {
		            	Camera.Parameters parameters = mCamera.getParameters(); 
		                if (parameters != null) {
		                	//Utils.pixLog(TAG, "setting focus area to:"+areaRect1);
		                	parameters.setFocusAreas(focusAreas);
		                	mCamera.setParameters(parameters);
		                	updateUiWithFocusTarget(x, y);
		                	mCamera.autoFocus(pixAutoFocusCallback);
		                }
		            }
	            } catch (Exception e) {
	            	//Utils.pixLog(TAG, "failed to set focus areas parameters."+e);
	            	e.printStackTrace();
	            }
	            
	        	break;
	        default:
	        	break;
	        }
		}
	}
}
