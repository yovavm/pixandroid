package com.pixplit.android.capture;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixCompositionComponent;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.PixBorder;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.multitouch.PhotoSortrView;

public class SplitPartFrame extends RelativeLayout implements OnClickListener{
//  	private static final String TAG = "SplitPartFrame";
		
	public static final int SPLIT_PART_STATE_ACTIVE = 1;		// the part is the one who is edited 
	public static final int SPLIT_PART_STATE_NOT_ACTIVE = 2;	// the part is open, but not currently active
	public static final int SPLIT_PART_STATE_CLOSED = 3;		// the part is occupied by previous participant
	
	private PixCompositionComponent compComponent;
	private SplitPartFrameDelegate mDelegate;
	private int splitPartState = 0;
	private ImageView selectPartBtn;
	private PixBorder border = null;
	private PhotoSortrView scalableImageView = null;
	ImageView defaultBorder = null;
	
	private Bitmap splitPartImage = null;
	
	private Rect splitRelativeRect;
	private Rect mDisplayRelativeRect;

	public interface SplitPartFrameDelegate {
		public void activateSplitPart(int partNum);
		public int getDisplayWidth();
		public int getDisplayHeight();
	}
	
	public SplitPartFrame(Context context, PixCompositionComponent component, Bitmap split, SplitPartFrameDelegate delegate) {
		super(context);
		this.mDelegate = delegate;
		if (component != null) {
			this.compComponent = component;
			if (this.compComponent != null) {
				setId(this.compComponent.getIndex());
			}
			calcSplitRelativeRect(split);
			calcDisplayRelativeRect();
			
			// default border
			defaultBorder = new ImageView(getContext());
    		defaultBorder.setBackgroundResource(R.drawable.capture_slot_gray);
    		defaultBorder.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams defaultBorderParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT);
			addView(defaultBorder, defaultBorderParams);
			
			// select part button
			selectPartBtn = new ImageView(getContext());
			selectPartBtn.setBackgroundResource(R.drawable.capture_switch_slot_selector);
			selectPartBtn.setOnClickListener(this);
			RelativeLayout.LayoutParams selectPartParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT);
			addView(selectPartBtn, selectPartParams);
			
			if (this.compComponent != null &&
					this.compComponent.getParticipant() != null) {
				setPartBitmapFromSplit(split);
				setPartState(SPLIT_PART_STATE_CLOSED);
			} else {
				setPartState(SPLIT_PART_STATE_NOT_ACTIVE);
			}
		}
	}
	
	public SplitPartFrame(Context context, AttributeSet attrs) {
		super(context, attrs);
		// This is the XML type constructor. This object should not be created through XML and should not reach here.
	}
	
	private void calcSplitRelativeRect(Bitmap split) {
		int splitWidth;
		int splitHeight;
		/*
		 *  if we have a split (on join case) then use its dimensions instead of the default 
		 *  (for future option of splits in variant sizes)
		 */
		if (split != null) {
			splitWidth = split.getWidth();
			splitHeight = split.getHeight();
		} else {
			splitWidth = PixDefs.SPLIT_BITMAP_WIDTH;
			splitHeight = PixDefs.SPLIT_BITMAP_HIGHT;
		}
		splitRelativeRect = getRelativeRect(splitWidth, splitHeight);
	}
	
	public Rect getPartSplitRelativeRect() {
		return splitRelativeRect;
	}

	private void calcDisplayRelativeRect() {
		if (mDelegate != null) {
			mDisplayRelativeRect = getRelativeRect(mDelegate.getDisplayWidth(), mDelegate.getDisplayHeight());
		}
	}

	private Rect getRelativeRect(int width, int height) {
		Rect baseRect = this.compComponent.getRect();
		if (baseRect != null) {
			float scaleWidth = ((float)PixDefs.SPLIT_BITMAP_RELATIVE_BASE_WIDTH) / width;
			float scaleHeight = ((float)PixDefs.SPLIT_BITMAP_RELATIVE_BASE_HIGHT) / height;
			int partX = (int) Math.round(baseRect.left/scaleWidth);
			int partY = (int) Math.round(baseRect.top/scaleHeight);
			int partW = (int) Math.round(baseRect.width()/scaleWidth);
			int partH = (int) Math.round(baseRect.height()/scaleHeight);
			return new Rect(partX, partY, partX+partW, partY+partH);
		}
		return null;
	}
	
	public void setPartState(int state) {
		if (this.splitPartState != SPLIT_PART_STATE_CLOSED) {
			this.splitPartState = state;
			switch (state) {
			case SPLIT_PART_STATE_ACTIVE:
				// the active part should not intercept the touch event (for tap to focus)
				setOnClickListener(null);
				selectPartBtn.setVisibility(View.GONE);
				break;
			case SPLIT_PART_STATE_NOT_ACTIVE:
				setOnClickListener(this);
				selectPartBtn.setVisibility(View.VISIBLE);
				break;
			case SPLIT_PART_STATE_CLOSED:
				selectPartBtn.setVisibility(View.GONE);
				break;
			default:
				break;
			}
		}
	}
	
	public boolean isClosed() {
		return (splitPartState == SPLIT_PART_STATE_CLOSED);
	}
	
	public boolean isActive() {
		return (splitPartState == SPLIT_PART_STATE_ACTIVE);
	}
	
	public int getIndex() {
		if (this.compComponent != null) {
			return this.compComponent.getIndex();
		}
		return -1;
	}
	
	@Override
	public boolean onTouchEvent (MotionEvent event) {
		
		return false;
	}
	
	@Override
	public void onClick(View v) {
		if (splitPartState == SPLIT_PART_STATE_NOT_ACTIVE) {
			if (mDelegate != null && this.compComponent != null) {
				mDelegate.activateSplitPart(this.compComponent.getIndex());
			}
		} else {
			// in case the item is active/closed do nothing
		}
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void setBitmap(Bitmap partBitmap) {
		if(Utils.hasJellyBean()) {
			// set the background
			this.setBackground(new BitmapDrawable(getResources(), partBitmap));
		} else {
			// set the background
			this.setBackgroundDrawable(new BitmapDrawable(getResources(), partBitmap));
		}
		this.splitPartImage = partBitmap;
	}
	
	private void setPartBitmapFromSplit(Bitmap splitBitmap) {
		if (splitBitmap != null && splitRelativeRect != null) {
			Bitmap splitPartBitmap = null;
			try {
				splitPartBitmap = Bitmap.createBitmap(splitBitmap, splitRelativeRect.left, splitRelativeRect.top, 
						splitRelativeRect.width(), splitRelativeRect.height());
			} catch (Exception e){
				//Utils.pixLog(TAG, "setPartBitmapFromSplit: createBitmap failed");
				e.printStackTrace();
			}
			if (splitPartBitmap != null) {
				setBitmap(splitPartBitmap);
			}
		}
	}
	
	public void disablePart() {
		setEnabled(false);
		selectPartBtn.setEnabled(false);
		selectPartBtn.setBackgroundResource(R.drawable.capture_switch_slot_gray);
	}

	public Bitmap getPartBitmap(Rect partBitmapDimen, boolean withBorder, boolean addJoinIcon, boolean finalizeSplit) {
		
		// extract the bitmap from the preview view
		Bitmap partBitmap = null;
		try {
			partBitmap = Bitmap.createBitmap(partBitmapDimen.width(), partBitmapDimen.height(), Config.ARGB_8888);
		} catch (Exception e){
			e.printStackTrace();
		}
		if (partBitmap == null) return null;
		
		Canvas canvas = new Canvas(partBitmap);
		
		if (this.splitPartState == SPLIT_PART_STATE_ACTIVE ||
			this.splitPartState == SPLIT_PART_STATE_CLOSED) {
			Drawable drawable = null;
			if (scalableImageView != null) {
				// remove borders visibility - only 'cut' the relevant image part
				int defaultBorderVisibility = 0;
				int borderVisibility = 0;
				if (defaultBorder != null) {
					defaultBorderVisibility = defaultBorder.getVisibility();
					defaultBorder.setVisibility(View.GONE);
				}
				if (border != null) {
					borderVisibility = border.getVisibility();
					border.setVisibility(View.GONE);
				}
				Bitmap b = null;
				try {
					int width = this.getWidth();
					int height = this.getHeight();
					b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
				}  catch (Exception e){
					e.printStackTrace();
				}
				if (b != null) {
					Canvas c = new Canvas(b);
					this.draw(c);
					drawable = new BitmapDrawable(getResources(), b);
					drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
				    drawable.draw(canvas);
				}
				// restore borders visibility state
				if (defaultBorder != null) {
					defaultBorder.setVisibility(defaultBorderVisibility);
				}
				if (border != null) {
					border.setVisibility(borderVisibility);
				}
			} else {
				if (this.splitPartImage != null) {
//						componentBitmap.setDensity(canvas.getDensity());
					Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
					p.setFilterBitmap(true);
					canvas.drawBitmap(this.splitPartImage, null, new Rect(0, 0, partBitmapDimen.width(), partBitmapDimen.height()), p);
				}			
			}
		} else {
			// this part has no image associated with. create an empty split part view
    		FrameLayout openPartFrame = new FrameLayout(getContext());
    		openPartFrame.setBackgroundColor(getResources().getColor(R.color.open_part_bg_color));
		    openPartFrame.layout(partBitmapDimen.left, partBitmapDimen.top, partBitmapDimen.right, partBitmapDimen.bottom);
		    openPartFrame.draw(canvas);
		    if (addJoinIcon) {
		    	BitmapFactory.Options options = new BitmapFactory.Options();
				options.inScaled = false;
				Bitmap joinBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.join_icon, options);
				int joinWidth = joinBitmap.getWidth();
				int joinHeight = joinBitmap.getHeight();
				int joinTopOffset = partBitmapDimen.height()/2 - joinHeight/2;
				int joinLeftOffset = partBitmapDimen.width()/2 - joinWidth/2;
				joinBitmap.setDensity(canvas.getDensity());
				canvas.drawBitmap(joinBitmap, joinLeftOffset, joinTopOffset, null);
		    }
		}
		
		/**
		 *  Last: draw border if needed. in case the part is scrollable (on move and scale action), no need to add borders 
		 *  as we are only 'cutting' the relevant part from the whole scrollView
		 */
		if (scalableImageView == null) {
			if (withBorder) {
		    	setBorder(SplitBorderType.BORDER_TYPE_WIDE);
		    	if (border != null) {
		    		Bitmap borderBitmap = Utils.loadBitmapFromView(border, canvas.getWidth(), canvas.getHeight());
		    		if (borderBitmap != null) { 
		    			canvas.drawBitmap(borderBitmap, null, new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), null);
		    		}
		    	}
		    } 
			else {
		    	// default is 1 pixel border
		    	if (!finalizeSplit ||
		    			(finalizeSplit && this.splitPartState == SPLIT_PART_STATE_NOT_ACTIVE)) {
			    	ImageView defaultBorder = new ImageView(getContext());
			    	if (finalizeSplit) {
			    		defaultBorder.setBackgroundResource(R.drawable.capture_slot_white);
			    	} else {
			    		defaultBorder.setBackgroundResource(R.drawable.capture_slot_gray);
			    	}
		    		defaultBorder.layout(partBitmapDimen.left, partBitmapDimen.top, partBitmapDimen.right, partBitmapDimen.bottom);
				    defaultBorder.draw(canvas);
		    	}
		    }
		}
		return partBitmap;
	}
	
	public Rect getDisplayRelativeRect() {
		return mDisplayRelativeRect;
	}
	
	public void setBorder(SplitBorderType borderType) {
		switch (borderType) {
		case BORDER_TYPE_WIDE:
			if (border != null) {
				border.setVisibility(View.VISIBLE);
			} else {
				// create the border
				border = new PixBorder(getContext(), getContext().getResources().getDimensionPixelOffset(R.dimen.pix_split_border_width));
				border.setBackgroundColor(getResources().getColor(R.color.transparent));
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
						RelativeLayout.LayoutParams.MATCH_PARENT);
				addView(border, params);
			}
			break;
		
		case BORDER_TYPE_DEFAULT:
			// hide the wide border
			if (border != null) {
				border.setVisibility(View.INVISIBLE);
			}
			break;
		case BORDER_TYPE_NONE:
			if (border != null) {
				border.setVisibility(View.INVISIBLE);
			}
			if (defaultBorder != null) {
				defaultBorder.setVisibility(View.INVISIBLE);
			}
			break;
		default:
			break;
		}
	}

	public void setScalableBitmap(Bitmap bitmap) {
		if (bitmap == null) return;
		// remove the old background
		if (scalableImageView != null) {
			scalableImageView.reloadImg(getResources(), bitmap);
		} else {
			scalableImageView = new PhotoSortrView(getContext(), bitmap);
			if (mDisplayRelativeRect != null) {
				scalableImageView.loadImg(getResources(), bitmap, mDisplayRelativeRect);
			}		
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT);
			this.addView(scalableImageView, 0, params);
		}
	}
}
