package com.pixplit.android.http;

import java.lang.ref.WeakReference;

import android.os.Bundle;

import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;

public class UserFollowRequestDelegate implements RequestDelegate{
//  	private static final String TAG = "CompLikeRequestDelegate";
	private WeakReference<PixUser> userRef;
	
	public UserFollowRequestDelegate(PixUser user) {
		this.userRef = new WeakReference<PixUser>(user);
	}
	
	@Override
	public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
		//Utils.pixLog(TAG, "Request to follow/unfollow finished successfully");
		if (request != null) {
			handleResponse(request.getRequestContext());
		}
	}

	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		PixUser user = userRef.get();
		if (user != null && request != null) {
			Bundle requestContext = request.getRequestContext();
			if (requestContext != null) {
				// roll back the previous follow status
				int followStatus = requestContext.getInt(PixDefs.REQ_CTX_USER_FOLLOW_STATUS);
				user.setFollowStatus(followStatus);
			}
		}
	}
	
	private void handleResponse(Bundle requestContext) {
		PixUser user = userRef.get();
		if (user != null && requestContext != null) {
			int followStatus = requestContext.getInt(PixDefs.REQ_CTX_USER_FOLLOW_STATUS);
			if (followStatus==PixUser.FOLLOW_STATUS_FOLLOWING) {
				user.setFollowStatus(PixUser.FOLLOW_STATUS_NOT_FOLLOWING);
			} else {
				user.setFollowStatus(PixUser.FOLLOW_STATUS_FOLLOWING);
			}
		}
	}

}
