package com.pixplit.android.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class PixHttpAPI {
	
	// Feeds URLs
	public static final String PIX_SCHEME = "https";
//	public static final String PIX_SCHEME = "http";
	public static final String PIX_SERVER_HOST = "pixplit-srv.appspot.com";
//	public static final String PIX_SERVER_HOST = "pixplit-dev-3-srv.appspot.com";
//	public static final String PIX_SERVER_HOST = "192.168.0.107";
	public static final int PIX_SERVER_PORT_NUM = 443;
//	public static final int PIX_SERVER_PORT_NUM = 8082;
	public static final String PIX_SERVER_PORT = ":"+ PIX_SERVER_PORT_NUM;
		
	public static final String API_PREFIX_URL = PIX_SCHEME + "://" + PIX_SERVER_HOST + PIX_SERVER_PORT;
	public static final String POPULAR_FEED_URL = API_PREFIX_URL + "/popular";
	public static final String SINGLE_COMP_URL_PREFIX = API_PREFIX_URL + "/comp/";
	public static final String PLAYGROUND_FEED_URL = API_PREFIX_URL + "/playground";
	public static final String COMPLETED_FEED_URL = API_PREFIX_URL + "/stream";
	public static final String USER_COMPLETED_FEED_URL = API_PREFIX_URL + "/usercompleted/";
	public static final String USER_NONCOMPLETED_FEED_URL = API_PREFIX_URL + "/usernoncompleted/";
	public static final String USER_LIKED_FEED_URL = API_PREFIX_URL + "/userliked/";
	public static final String THEME_FEED_URL_PREFIX = API_PREFIX_URL + "/theme/";
	public static final String CHALLENGES_FEED_URL = API_PREFIX_URL + "/challenges";
	public static final String USER_FOLLOWING_URL_PREFIX = API_PREFIX_URL + "/followings/";
	public static final String USER_FOLLOWERS_URL_PREFIX = API_PREFIX_URL + "/followers/";
	public static final String USER_FOLLOW_URL = API_PREFIX_URL + "/follow";
	public static final String USER_UNFOLLOW_URL = API_PREFIX_URL + "/unfollow";
	public static final String SEARCH_USER_URL_PATH_PREFIX = "/searchuser/";
	public static final String SEARCH_USER_URL_PREFIX = API_PREFIX_URL + SEARCH_USER_URL_PATH_PREFIX;
	public static final String NOTIFICATIONS_URL = API_PREFIX_URL + "/notifications";
	public static final String NEW_RECORDS_URL = API_PREFIX_URL + "/newrecords";
	public static final String FB_AUTH_URL = API_PREFIX_URL + "/fbauth";
	public static final String VALIDATE_EMAIL_URL = API_PREFIX_URL + "/validateemail";
	public static final String SIGNUP_URL = API_PREFIX_URL + "/signup";
	public static final String LOGIN_URL = API_PREFIX_URL + "/login";
	public static final String LOGOUT_URL = API_PREFIX_URL + "/logout";
	public static final String RESET_PASSWORD_URL = API_PREFIX_URL + "/sendpwdresetemail";
	public static final String GET_ACCOUNT_INFO_URL = API_PREFIX_URL + "/getuseraccountinfo";
	public static final String EDIT_ACCOUNT_INFO_URL = API_PREFIX_URL + "/edituseraccountinfo";
	public static final String NOTIFICATIONS_SETTINGS_URL = API_PREFIX_URL + "/notificationsettings";
	public static final String SET_NOTIFICATIONS_SETTINGS_URL = API_PREFIX_URL + "/setnotificationsettings";
	public static final String UPDATE_COVER_PIC_URL = API_PREFIX_URL + "/updatecoverpic";
	public static final String FRIENDS_FOLLOW_STATUS_URL = API_PREFIX_URL + "/friendsfollowstatus";
	public static final String FOLLOW_USERS_URL = API_PREFIX_URL + "/followusers";
	public static final String CHECK_VERSION_URL = API_PREFIX_URL + "/checkandroidversion";
	public static final String CONVERSATIONS_FEED_URL = API_PREFIX_URL + "/conversations";
	public static final String CONVERSATION_FEED_URL = API_PREFIX_URL + "/conversation";
	public static final String CONVERSATION_MESSAGE_ACK_URL = API_PREFIX_URL + "/messageack";
	public static final String CONVERSATION_ADD_MESSAGE_URL = API_PREFIX_URL + "/addprivatemessage";
	public static final String CONVERSATION_HIDE_URL = API_PREFIX_URL + "/hideconversation";
	public static final String CONVERSATION_DELETE_MSG_URL = API_PREFIX_URL + "/deleteprivatemessage";
	public static final String DELETE_COMMENT_URL = API_PREFIX_URL + "/deletecomment";
	public static final String RESET_BADGE_COUNT_URL = API_PREFIX_URL + "/resetbadgecount";
	public static final String ADMIN_AUTHENTICATE_URL = API_PREFIX_URL + "/authenticateadmin";
	public static final String ADMIN_LOGOUT_URL = API_PREFIX_URL + "/logoutadmin";
	public static final String ADMIN_POPULAR_FULL_URL = API_PREFIX_URL + "/popularfull";
	public static final String ADMIN_POPULAR_CANDIDATES_URL = API_PREFIX_URL + "/popularcandidates";
	public static final String ADMIN_ADD_TO_POPULAR_URL = API_PREFIX_URL + "/addtopopular";
	public static final String ADMIN_REMOVE_FROM_POPULAR_URL = API_PREFIX_URL + "/removefrompopular";
	public static final String ADMIN_APPROVE_POPULAR_CANDIDATES_URL = API_PREFIX_URL + "/approvecandidate";
	public static final String ADMIN_ADD_SUGGESTED_USER_URL = API_PREFIX_URL + "/addtosuggested";
	public static final String ADMIN_REMOVE_SUGGESTED_USER_URL = API_PREFIX_URL + "/removefromsuggested";
	public static final String ADMIN_SUGGESTED_FULL_URL = API_PREFIX_URL + "/suggestedfull";
	public static final String ADMIN_REMOVE_POPULAR_CANDIDATES_URL = API_PREFIX_URL + "/discardcandidates";
	public static final String ADMIN_DELETE_COMP_URL = API_PREFIX_URL + "/deletecompositionbyadmin";
	public static final String ADMIN_CREATE_EUREKA_ACCOUNT_URL = API_PREFIX_URL + "/createaccount";
	public static final String ADMIN_EUREKA_ACCOUNTS_URL = API_PREFIX_URL + "/accounts";
	public static final String ADMIN_EUREKA_UPDATE_ACCOUNT_LIMIT_URL_PREFIX = API_PREFIX_URL + "/updateaccountlimits/";
	public static final String GPLUS_AUTH_URL = API_PREFIX_URL + "/gauth";
	public static final String VIEW_ACK_URL_PREFIX = API_PREFIX_URL + "/ackview/";
	
	// Composition services
	public static final String COMP_LIKE_URL = API_PREFIX_URL + "/like";
	public static final String COMP_DISLIKE_URL = API_PREFIX_URL + "/dislike";
	public static final String COMP_COMMENTS_URL_PREFIX = API_PREFIX_URL + "/comments/";
	public static final String COMP_ADD_COMMENT_URL = API_PREFIX_URL + "/comment";
	public static final String COMP_RELATED_URL_PREFIX = API_PREFIX_URL + "/relatedcompleted/";
	public static final String COMP_LIKERS_URL_PREFIX = API_PREFIX_URL + "/likers/";
	public static final String COMP_FLAG_URL = API_PREFIX_URL + "/flagcomp";
	public static final String COMP_NEW_COMP = API_PREFIX_URL + "/newcomp";
	public static final String COMP_NEW_TEMP_COMP = API_PREFIX_URL + "/newtempcomp";
	public static final String COMP_SET_COMP_DATA = API_PREFIX_URL + "/setcompdata";
	public static final String COMP_DELETE_TEMP_COMP = API_PREFIX_URL + "/deletetempcomp";
	public static final String COMP_SHARE_BY_EMAIL = API_PREFIX_URL + "/sharebymail";
	public static final String COMP_DELETE_COMP = API_PREFIX_URL + "/deletecomp";
	
	// Headers
	public static final String AUTH_HDR = "X-USERTOKEN";
	public static final String ADMIN_AUTH_HDR = "X-ADMINTOKEN";
	
	// Params
	public static final String PAGE_PARAM = "p";
	public static final String NEXT_PARAM = "next";
	public static final String IMAGE_SIZE_PARAM = "size";
	public static final String COMP_ID_PARAM = "cid";
	public static final String COMP_IDS_PARAM = "cids";
	public static final String ACTION_ID_PARAM = "aid";
	public static final String ROOT_ID_PARAM = "rid";
	public static final String PARENT_ID_PARAM = "pid";
	public static final String MSG_PARENT_ID_PARAM = "mpid";
	public static final String COMP_TYPE_PARAM = "comptype";
	public static final String IS_COMPLETE_PARAM = "ic";
	public static final String INDEX_PARAM = "index";
	public static final String TITLE_PARAM = "title";
	public static final String TAGS_PARAM = "tags";
	public static final String LOCATION_PARAM = "location";
	public static final String HAS_BORDER_PARAM = "hb";
	public static final String USERNAME_PARAM = "un";
	public static final String FULL_NAME_PARAM = "fn";
	public static final String ACCOUNT_NAME_PARAM = "an";
	public static final String FACEBOOK_ID_PARAM = "fbid";
	public static final String FACEBOOK_USERNAME_PARAM = "fbun";
	public static final String TOKEN_PARAM = "token";
	public static final String TIMESTAMP_PARAM = "timestamp";
	public static final String TEXT_PARAM = "text";
	public static final String IMAGE_SIZE_THUMBNAIL = "?"+IMAGE_SIZE_PARAM+"=t";
	public static final String NEWS_TIMESTAMP_PARAM = "ntimestamp";
	public static final String ACTIONS_TIMESTAMP_PARAM = "atimestamp";
	public static final String COMPLETED_TIMESTAMP_PARAM = "ctimestamp";
	public static final String INVITEES_PARAM = "invitees";
	public static final String TO_PARAM = "to";
	public static final String SUBJECT_PARAM = "subject";
	public static final String BODY_PARAM = "body";
	public static final String EMAIL_PARAM = "email";
	public static final String PASSWORD_PARAM = "pwd";
	public static final String DELETE_IMAGE_PARAM = "delimg";
	public static final String NOTIFY_LIKED_PARAM = "liked";
	public static final String NOTIFY_COMMENTED_PARAM = "commented";
	public static final String NOTIFY_JOINED_PARAM = "joined";
	public static final String NOTIFY_INVITED_PARAM = "invited";
	public static final String NOTIFY_FOLLOWED_PARAM = "followed";
	public static final String NOTIFY_FB_FRIEND_PARAM = "fbFriendJoined";
	public static final String NOTIFY_SYSTEM_PARAM = "system";
	public static final String COVER_PIC_ID_PARAM = "cpic";
	public static final String USERS_PARAM = "users";
	public static final String FOLLOW_USERS_PARAM = "fusers";
	public static final String NOTIFY_USERS_PARAM = "nusers";
	public static final String VERSION_PARAM = "version";
	public static final String CONVERSATION_ID_PARAM = "conversationId";
	public static final String MESSAGE_ID_PARAM = "messageId";
	public static final String RECIPIENT_PARAM = "recipient";	
	public static final String MESSAGE_TYPE_PARAM = "messagetype";
	public static final String COMMENT_ID_PARAM = "comid";
	public static final String GOOGLE_ID_PARAM = "gid";
	public static final String GOOGLE_USERNAME_PARAM = "gun";
	public static final String AGE_RANGE_PARAM = "ar";
	public static final String GENDER_PARAM = "gender";
	public static final String LOCALE_PARAM = "locale";
	public static final String IMAGE_PARAM = "image";
	public static final String SUBTITLE_PARAM = "stitle";
	public static final String COLOR_SCHEME_PARAM = "color";
	public static final String COUNT_PARAM = "count";
	
	// web pages
	public static final String termsUrl = "http://pixplit.com/m_terms";
	public static final String privacyUrl = "http://pixplit.com/m_privacy";
	public static final String aboutUrl = "http://pixplit.com/m_about";
	
	// instagram
	public static final String INSTAGRAM_TOKEN_PARAM ="access_token=";
	public static final String INSTAGRAM_CLIENT_ID_PARAM = "client_id=";
	public static final String INSTAGRAM_CLIENT_ID_VAL = "91dc92a1f42f4b58bb9aaefeb1274f9d";
	public static final String INSTAGRAM_REDIRECT_URI_PARAM = "redirect_uri=";
	public static final String INSTAGRAM_REDIRECT_URI_VAL = "http://www.pixplit.com/";
	public static final String INSTAGRAM_RESPONSE_TYPE = "response_type=token";
	public static final String INSTAGRAM_SCOPE = "scope=%s";
	public static final String INSTAGRAM_SCOPE_FOLLOW = "basic+relationships";
	public static final String INSTAGRAM_AUTH_URL = "https://instagram.com/oauth/authorize/";
	public static final String INSTAGRAM_PHOTOS_URL = "https://api.instagram.com/v1/users/self/media/recent";
	public static final String INSTAGRAM_USER_INFO_URL = "https://api.instagram.com/v1/users/self";
	public static final String INSTAGRAM_PIXPLIT_USER_ID = "41479549";
	public static final String INSTAGRAM_FOLLOW_PIXPLIT_URL = "https://api.instagram.com/v1/users/"+INSTAGRAM_PIXPLIT_USER_ID+"/relationship";
	public static final String INSTAGRAM_ACCESS_TOKEN_PARAM = "access_token";
	
	// facebook
	public static final String FACEBOOK_ALBUMS_URL = "https://graph.facebook.com/me/albums";
	public static final String FACEBOOK_ALBUMS_COVER_URL = "https://graph.facebook.com/%s/picture";
	public static final String FACEBOOK_PHOTOS_URL = "https://graph.facebook.com/me/photos";
	public static final String FACEBOOK_ALBUM_PHOTOS_URL = "https://graph.facebook.com/%s/photos";
	public static final String FACEBOOK_ACCESS_TOKEN_PARAM = "access_token";
	
	// google images search
	// info from: https://developers.google.com/image-search/v1/jsondevguide
	public static final String GOOGLE_IMAGE_SEARCH_URL = "https://ajax.googleapis.com/ajax/services/search/images";
	public static final String GOOGLE_SEARCH_QUERY_PARAM = "q";
	public static final String GOOGLE_SEARCH_VERSION_PARAM = "v";
	public static final String GOOGLE_SEARCH_VERSION_NUMBER = "1.0";
	public static final String GOOGLE_SEARCH_LICENSE_PARAM = "as_rights";
//	public static final String GOOGLE_SEARCH_LICENSE_PUBLIC = "cc_publicdomain";
	public static final String GOOGLE_SEARCH_LICENSE_PUBLIC = "(cc_publicdomain|cc_attribute|cc_sharealike)";
	public static final String GOOGLE_SEARCH_RESULTS_PER_PAGE_PARAM = "rsz";
	public static final String GOOGLE_SEARCH_RESULTS_PER_PAGE_NUM = "8";
	public static final String GOOGLE_SEARCH_FILE_TYPE_PARAM = "as_filetype";
	public static final String GOOGLE_SEARCH_FILE_TYPE_VAL = "(png|jpg)";
	public static final String GOOGLE_SEARCH_START_PARAM = "start";
	public static final String GOOGLE_SEARCH_IMAGE_SIZE_PARAM = "imgsz";
	public static final String GOOGLE_SEARCH_IMAGE_SIZE_VAL = "(icon|small|medium|large|xlarge)";
	
	/**
	 * Encodes the passed String as UTF-8 using an algorithm that's compatible
	 * with JavaScript's <code>encodeURIComponent</code> function. Returns
	 * <code>null</code> if the String is <code>null</code>.
	 * 
	 * @param s The String to be encoded
	 * @return the encoded String
	 */
	public static String encodeURIComponent(String s) {
		String result = null;
	    try {
	      result = URLEncoder.encode(s, "UTF-8")
	                         .replaceAll("\\+", "%20")
	                         .replaceAll("\\%21", "!")
	                         .replaceAll("\\%27", "'")
	                         .replaceAll("\\%28", "(")
	                         .replaceAll("\\%29", ")")
	                         .replaceAll("\\%7E", "~");
	    }

	    // This exception should never occur.
	    catch (UnsupportedEncodingException e) {
	      result = s;
	    }
	    return result;
	}
}
