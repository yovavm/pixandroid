package com.pixplit.android.http;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.graphics.Bitmap;

public class RestClient {
//	private static final String TAG = "RestClient";
	
	private RequestMethod method = null;
    private ArrayList <NameValuePair> params;
    private ArrayList <NameValuePair> headers;
    private Bitmap bitmap;
    
    private String url;

    private int responseCode;
    private String message;

    private String response;
    
    public RestClient(String url)
    {
        this.url = url;
        params = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
        bitmap = null;
    }

    public enum RequestMethod {
    	GET, POST
	}

    public void setRequest(RequestMethod method) {
    	this.method = method;
    }
    
    public String getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void addParam(String name, String value)
    {
    	//Utils.pixLog(TAG, "Adding param:"+name+"="+value);
        params.add(new BasicNameValuePair(name, value));
    }

    public void addHeader(String name, String value)
    {
    	//Utils.pixLog(TAG, "Adding header:"+name+":"+value);
        headers.add(new BasicNameValuePair(name, value));
    }
    
    public void addBitmap(Bitmap bitmap) {
    	//Utils.pixLog(TAG, "Adding bitmap:"+bitmap);
    	this.bitmap = bitmap;
    }
    
    public void execute() throws Exception
    {
    	if (method != null) {
    		execute(method);
    	}
    }

    public void execute(RequestMethod method) throws Exception
    {
        switch(method) {
            case GET:
            {
                //add parameters
                String combinedParams = "";
                if(!params.isEmpty()){
                    combinedParams += "?";
                    for(NameValuePair p : params)
                    {
                        String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(),"UTF-8");
                        if(combinedParams.length() > 1)
                        {
                            combinedParams  +=  "&" + paramString;
                        }
                        else
                        {
                            combinedParams += paramString;
                        }
                    }
                }

                HttpGet request = new HttpGet(url + combinedParams);
                
                //add headers
                for(NameValuePair h : headers)
                {
                    request.addHeader(h.getName(), h.getValue());
                }

                executeRequest(request, url);
                break;
            }
            case POST:
            {
                HttpPost request = new HttpPost(url);

                //add headers
                for(NameValuePair h : headers)
                {
                    request.addHeader(h.getName(), h.getValue());
                }

                if(!params.isEmpty() && bitmap == null){
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                }
                
                if (bitmap != null) {
                	ByteArrayOutputStream bos = new ByteArrayOutputStream();
                	bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                	byte[] data = bos.toByteArray();
                	
                	MultipartEntity reqEntity;
                     
                    // Indicate that this information comes in parts (text and file)
                    reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                    
                    ContentBody fileBody = new ByteArrayBody(data, "application/octet-stream", "file");
                    reqEntity.addPart("img", fileBody);
                     
                    // The rest of the data
                    if(!params.isEmpty()) { 
	                	for(NameValuePair p : params) {
	                		FormBodyPart paramPart = new FormBodyPart(p.getName(), new StringBody(p.getValue()));
	                        reqEntity.addPart(paramPart);
	                    }
                    }
                	
                    request.setEntity(reqEntity);                  
                }

                executeRequest(request, url);
                break;
            }
        }
    }

    private void executeRequest(HttpUriRequest request, String url) throws Exception
    {
        HttpClient client = new DefaultHttpClient();

        HttpResponse httpResponse;

        try {
        	//Utils.pixLog(TAG, "sending request:"+url+"\n"+request.getRequestLine().toString());
        	
        	
        	HttpHost target = new HttpHost(PixHttpAPI.PIX_SERVER_HOST, PixHttpAPI.PIX_SERVER_PORT_NUM, PixHttpAPI.PIX_SCHEME);
        	
        	
            httpResponse = client.execute(target, request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {

                InputStream instream = entity.getContent();
                response = convertStreamToString(instream);

                // Closing the input stream will trigger connection release
                instream.close();
            }

        } catch (ClientProtocolException e)  {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}