package com.pixplit.android.http;

import java.lang.ref.WeakReference;

import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.data.CompositionLikes;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;

public class CompLikeRequestDelegate implements RequestDelegate{
	private WeakReference<PixComposition> compRef;
	private LikeRequestDelegate mDelegate;
	
	public interface LikeRequestDelegate {
		public void LikeRequestFinished(PixComposition comp);
		public void LikeRequestFinishedWithError(PixError error);	
	}
	
	public CompLikeRequestDelegate(PixComposition comp, LikeRequestDelegate delegate) {
		this.compRef = new WeakReference<PixComposition>(comp);
		this.mDelegate = delegate;
	}
	
	@Override
	public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
		if (response != null && response instanceof CompositionLikes) {
			CompositionLikes likeResponse = (CompositionLikes)response;
			handleResponse(likeResponse, request.getRequestContext());
		}
		
	}

	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		PixComposition comp = compRef.get();
		if (comp != null && request != null) {
			Bundle requestContext = request.getRequestContext();
			if (requestContext != null) {
				// roll back the liked state
				int liked = requestContext.getInt(PixDefs.REQ_CTX_COMP_LIKED);
				comp.setLiked(liked);
			}
		}
		if (mDelegate != null) {
			mDelegate.LikeRequestFinishedWithError(error);
		}
	}
	
	private void handleResponse(CompositionLikes response, Bundle requestContext) {
		PixComposition comp = compRef.get();
		if (comp != null && response != null) {
			comp.setLikesCount(response.getCount());
			comp.setLiked(response.getLiked());
			/* not updating the liker in order to fix the case where the server returns the logged-in user 
			 * as the liker
			 * comp.setLiker(response.getLiker());
			 */
			if (mDelegate != null) {
				mDelegate.LikeRequestFinished(comp);
			}
		}
	}

}
