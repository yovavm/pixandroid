package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.util.Utils;
import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class ResetPasswordHttpRequest extends BaseHttpRequest{
	
	public ResetPasswordHttpRequest(RequestDelegate delegate, String email){
		super(PixHttpAPI.RESET_PASSWORD_URL, delegate);
		if (Utils.isValidString(email)) {
			this.addParam(PixHttpAPI.EMAIL_PARAM, email);
			String token = PixAppState.UserInfo.craetePixToken(email);
			if (token != null) {
				addParam(PixHttpAPI.TOKEN_PARAM, token);
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}

}
