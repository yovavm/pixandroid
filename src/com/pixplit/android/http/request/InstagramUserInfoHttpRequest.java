package com.pixplit.android.http.request;

import com.pixplit.android.data.InstagramUserInfo;
import com.pixplit.android.http.PixHttpAPI;


public class InstagramUserInfoHttpRequest extends InstagramHttpRequest{
	
	public InstagramUserInfoHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.INSTAGRAM_USER_INFO_URL, delegate, Priority.IMMEDIATE);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			InstagramUserInfo userInfo = new InstagramUserInfo(this.code, this.message);
			if (userInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = userInfo;				
			}
		}
	}
}
