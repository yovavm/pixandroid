package com.pixplit.android.http.request;

import com.pixplit.android.data.PixUsers;

public class UsersFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public UsersFeedHttpRequest(RequestDelegate delegate, String url){
		super(Method.GET, url, delegate, Priority.IMMEDIATE);
		this.addUserIdentity();
	}

	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixUsers users = new PixUsers(this.code, this.message);
			if(users.parseJsonObject(responseJsonObject)) {			
				this.pixObject = users;
			}
		}
	}
}
