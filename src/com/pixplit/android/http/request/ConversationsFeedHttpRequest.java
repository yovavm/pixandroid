package com.pixplit.android.http.request;

import com.pixplit.android.data.PixConversations;
import com.pixplit.android.http.PixHttpAPI;

public class ConversationsFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public ConversationsFeedHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.CONVERSATIONS_FEED_URL, delegate, Priority.IMMEDIATE);
		this.addUserIdentity();
	}

	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixConversations conversations = new PixConversations(this.code, this.message);
			if(conversations.parseJsonObject(responseJsonObject)) {			
				this.pixObject = conversations;
			}
		}
	}
}
