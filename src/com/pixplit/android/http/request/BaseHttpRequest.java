package com.pixplit.android.http.request;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.os.Bundle;

import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.JsonHttpTask;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public abstract class BaseHttpRequest extends RestClient{
  	private static final String TAG = "BaseHttpRequest";
	
	public interface RequestDelegate {
		public void requestFinished(BaseHttpRequest request, PixObject response);
		public void requestFinishedWithError(BaseHttpRequest request, PixError error);	
	}
	
	private RequestDelegate mDelegate;
	protected Bundle mReqContext = null;
	
	protected int code;
	protected String message;
	protected JSONObject responseJsonObject;
	
	protected PixObject pixObject;
	
	public BaseHttpRequest(String url, RequestDelegate delegate){
		super(url);
		this.mDelegate = delegate;
		mReqContext = new Bundle();
	}
	
	public void addUserIdentity(){
		String userToken = UserInfo.getUserToken();
		if (Utils.isValidString(userToken)) {
			addHeader(PixHttpAPI.AUTH_HDR, userToken);
		}
	}

	public void addAdminIdentity(){
		String adminToken = UserInfo.getAdminToken();
		if (Utils.isValidString(adminToken)) {
			addHeader(PixHttpAPI.ADMIN_AUTH_HDR, adminToken);
		}
	}
	
	public void send(){
		new JsonHttpTask(this).execute();
	}
	
	// run in background
	public void execute() throws Exception {
		try {
			super.execute();
		}
		catch (Exception e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			return;
		}
		
		String responseString = this.getResponse();
		
		if (responseString != null) {
			Utils.pixLog(TAG,  this.getClass().getName() +", Got Response:"+responseString);
			JSONObject jsonObj = new JSONObject(responseString);
			
			this.code = jsonObj.optInt("status", -1);
			this.message = jsonObj.optString("message", "");

			if (code == HttpStatus.SC_OK) {	
				responseJsonObject = jsonObj.optJSONObject("response");
				this.parseResponse(); // will build the pixObject for us (will handle null value as well)
			}
		} else {
			this.code = PixError.PixErrorCodeNetworkFailure;
			return;
		}
	}
	
	protected abstract void parseResponse();
	
	// called in the UI thread
	public void onRequestFinish() {
		if (this.code == HttpStatus.SC_OK) {
			if (this.pixObject != null) {
				this.handleHttpResponse(this.pixObject);				
			}
			else {
				handleHttpError(PixError.PixErrorCodeInvalidRequest);
			}
		}
		else {
			if (this.code == -1 || this.code == 0) { 
				// no "status" key in response json or parsing error
				handleHttpError(PixError.PixErrorCodeInvalidRequest);
			}
			else {
				handleHttpError(this.code);
			}
		}		
	}
	
	public Bundle getRequestContext() {
		return mReqContext;
	}
	
	private void handleHttpResponse(PixObject object) {
		if (this.mDelegate != null) {
			this.mDelegate.requestFinished(this, object);
		}
	}
	
	private void handleHttpError(int code) {
		if (mDelegate != null) {
			this.mDelegate.requestFinishedWithError(this, new PixError(code));
		}
	}
}
