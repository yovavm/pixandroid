package com.pixplit.android.http.request;

import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.data.PixChatMessageContainer;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;

public class ChatAddMessageHttpRequest extends BaseHttpRequest{
	
	public ChatAddMessageHttpRequest(RequestDelegate delegate, String conversationId, String recipient, String parentMsgId, 
									 int messageType, String text, PixCompositionMetadata compMetadata){
		super(PixHttpAPI.CONVERSATION_ADD_MESSAGE_URL, delegate);
		
		if (Utils.isValidString(conversationId)) {
			this.addParam(PixHttpAPI.CONVERSATION_ID_PARAM, conversationId);
		} 
		if (Utils.isValidString(recipient)) {
			this.addParam(PixHttpAPI.RECIPIENT_PARAM, recipient);
		}
		if (Utils.isValidString(parentMsgId)) {
			this.addParam(PixHttpAPI.MSG_PARENT_ID_PARAM, parentMsgId);
		}
		
		this.addParam(PixHttpAPI.MESSAGE_TYPE_PARAM, String.format("%d", messageType));
		if (messageType == PixChatMessage.PIX_MESSAGE_TYPE_TEXT) {
			if (Utils.isValidString(text)) {
				this.addParam(PixHttpAPI.TEXT_PARAM, text);
			}
		} else if (messageType == PixChatMessage.PIX_MESSAGE_TYPE_COMP) {
			if (compMetadata != null && compMetadata.getCompositionImage() != null) {
				this.addBitmap(compMetadata.getCompositionImage());
				this.addParam(PixHttpAPI.ROOT_ID_PARAM, compMetadata.getRootId());
				this.addParam(PixHttpAPI.PARENT_ID_PARAM, compMetadata.getParentId());
				if (compMetadata.getCompType() != null) {
					this.addParam(PixHttpAPI.COMP_TYPE_PARAM, String.format("%d", PixCompositionFramesetType.getInt(compMetadata.getCompType())));
				}
				this.addParam(PixHttpAPI.IS_COMPLETE_PARAM, compMetadata.isCompleted() ? "1" : "0");
				this.addParam(PixHttpAPI.INDEX_PARAM, String.format("%d", compMetadata.getNewComponentIndex()));
				this.addParam(PixHttpAPI.HAS_BORDER_PARAM, compMetadata.hasBorder() ? "1" : "0");
				
				this.mReqContext.putInt(PixDefs.REQ_CTX_NEW_COMP_TYPE, PixDefs.REQ_CTX_NEW_PRIVATE_COMP);
			}
		}	
		
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
		
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixChatMessageContainer message = new PixChatMessageContainer(this.code, this.message);
			if(message.parseJsonObject(responseJsonObject)) {			
				this.pixObject = message;
			}
		}
	}
    
    
}
