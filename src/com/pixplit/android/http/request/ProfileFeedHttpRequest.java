package com.pixplit.android.http.request;

import com.pixplit.android.data.PixProfileCompositions;


public class ProfileFeedHttpRequest extends CompsFeedHttpRequest{
	
	public ProfileFeedHttpRequest(RequestDelegate delegate, String url){
		super(delegate, url, Priority.IMMEDIATE);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixProfileCompositions profileWithComps = new PixProfileCompositions(this.code, this.message);
			if(profileWithComps.parseJsonObject(responseJsonObject)) {			
				this.pixObject = profileWithComps;
			}
		}
	}
}
