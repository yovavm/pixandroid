package com.pixplit.android.http.request;

import com.pixplit.android.data.FacebookPhotos;


public class FbPhotosHttpRequest extends FbHttpRequest{
	
	public FbPhotosHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			FacebookPhotos facebookPhotos = new FacebookPhotos(this.code, this.message);
			if(facebookPhotos.parseJsonObject(responseJsonObject)) {			
				this.pixObject = facebookPhotos;				
			}
		}
	}
}
