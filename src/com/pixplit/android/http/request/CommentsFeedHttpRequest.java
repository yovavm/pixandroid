package com.pixplit.android.http.request;

import com.pixplit.android.data.PixComments;
import com.pixplit.android.http.PixHttpAPI;

public class CommentsFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public CommentsFeedHttpRequest(RequestDelegate delegate, String compId){
		super(Method.GET, PixHttpAPI.COMP_COMMENTS_URL_PREFIX + compId, delegate, Priority.IMMEDIATE);
//		if (Utils.isValidString(compId)) {
//    		this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
//    	}
		
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixComments comments = new PixComments(this.code, this.message);
			if(comments.parseJsonObject(responseJsonObject)) {			
				this.pixObject = comments;
			}
		}
	}
    
}
