package com.pixplit.android.http.request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public abstract class BaseVolleyHttpRequest extends Request<PixObject>{
	private static final String TAG = "BaseVolleyHttpRequest";
  	private HashMap<String, String> mHeaders;
  	private HashMap<String, String> mParams;
  	private Priority mPriority;
  	
  	/** Socket timeout in milliseconds for image requests */
    private static final int TIMEOUT_MS = 15000;

    /** Default number of retries for image requests */
    private static final int MAX_RETRIES = 0;

    /** Default backoff multiplier for image requests */
    private static final float IMAGE_BACKOFF_MULT = 2f;
    
	public interface RequestDelegate {
		public void requestFinished(BaseVolleyHttpRequest request, PixObject response);
		public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error);	
	}
	
	private RequestDelegate mDelegate;
	protected Bundle mReqContext = null;
	
	protected int code;
	protected String message;
	protected JSONObject responseJsonObject;
	
	protected PixObject pixObject;
	
	private static ErrorListener getErrorListener(final RequestDelegate delegate) {
		return new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				if (delegate != null) {
					delegate.requestFinishedWithError(null, 
							new PixError((PixError.PixErrorCodeNetworkFailure)));
				}
			}
		};
	}
	
	public BaseVolleyHttpRequest(int method, String url, final RequestDelegate delegate, Priority priority){
		super(method, url, getErrorListener(delegate));
		this.mDelegate = delegate;
		this.mHeaders = new HashMap<String, String>();
		this.mParams = new HashMap<String, String>();
		this.mReqContext = new Bundle();
		this.mPriority = priority;
		setRetryPolicy(
                new DefaultRetryPolicy(TIMEOUT_MS, MAX_RETRIES, IMAGE_BACKOFF_MULT));
	}

	public void addUserIdentity(){
		String userToken = UserInfo.getUserToken();
		if (Utils.isValidString(userToken)) {
			addHeader(PixHttpAPI.AUTH_HDR, userToken);
		}
	}
	
	public void addAdminIdentity(){
		String adminToken = UserInfo.getAdminToken();
		if (Utils.isValidString(adminToken)) {
			addHeader(PixHttpAPI.ADMIN_AUTH_HDR, adminToken);
		}
	}
	
	public void addHeader(String name, String value) {
		this.mHeaders.put(name, value);
	}
	
	public void addParam(String name, String value) {
		this.mParams.put(name, value);
	}
	
	@Override
    public Priority getPriority() {
        return mPriority;
    }
	
	@Override
	public Map<String, String> getHeaders(){
		return mHeaders;
	}
	
	@Override
	public Map<String, String> getParams(){
		return mParams;
	}
	
	@Override
	public String getUrl() {
		//add parameters to GET method url
        String combinedParams = "";
        if(getMethod() == Method.GET &&
        		!mParams.isEmpty()){
            combinedParams += "?";
            for(Map.Entry<String,String> entry : mParams.entrySet()) {
            	String paramString = null;
				try {
					paramString = entry.getKey() + "=" + URLEncoder.encode(entry.getValue(),"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
                if (combinedParams.length() > 1) {
                    combinedParams  +=  "&" + paramString;
                }
                else {
                    combinedParams += paramString;
                }
            }
        }
		return super.getUrl() + combinedParams;
	}
	
	@Override
    protected Response<PixObject> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			
			Utils.pixLog(TAG, this.getClass().getName() +", Got Response:"+json);
			JSONObject jsonObj = new JSONObject(json);
			
			this.code = jsonObj.optInt("status", -1);
			this.message = jsonObj.optString("message", "");

			if (code == HttpStatus.SC_OK) {	
				responseJsonObject = jsonObj.optJSONObject("response");
				this.parseResponse(); // will build the pixObject for us (will handle null value as well)
			}
		
		} catch (UnsupportedEncodingException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		} catch (JSONException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		}
		return Response.success(pixObject, null/* HttpHeaderParser.parseCacheHeaders(response)*/ );
	}
	
	protected abstract void parseResponse();
	
	@Override
    protected void deliverResponse(PixObject response) {
		if (mDelegate == null) return;
		
		if (response != null) {
			mDelegate.requestFinished(this, response);
		} else {
			if (this.code == -1 || this.code == 0) { 
				// no "status" key in response json or parsing error
				mDelegate.requestFinishedWithError(this, 
						new PixError(PixError.PixErrorCodeInvalidRequest));
			}
			else {
				mDelegate.requestFinishedWithError(this, 
						new PixError(this.code));
			}
		}
    }
	
	public Bundle getRequestContext() {
		return mReqContext;
	}

}
