package com.pixplit.android.http.request;

import com.pixplit.android.data.PixCompositions;


public class CompsFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public CompsFeedHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
		this.addUserIdentity();
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixCompositions compositions = new PixCompositions(this.code, this.message);
			if(compositions.parseJsonObject(responseJsonObject)) {			
				this.pixObject = compositions;				
			}
		}
	}
}
