package com.pixplit.android.http.request;

import com.pixplit.android.data.CompositionLikes;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.time.PixDateFormat;

public class CompLikeHttpRequest extends BaseVolleyHttpRequest{
	
	public CompLikeHttpRequest(RequestDelegate delegate, PixComposition comp, boolean liked){
		super(Method.POST, liked ? PixHttpAPI.COMP_LIKE_URL : PixHttpAPI.COMP_DISLIKE_URL, 
				delegate, Priority.HIGH);
		
		// set previous state in order to rollback in case the request will finish with error
		this.getRequestContext().putInt(PixDefs.REQ_CTX_COMP_LIKED, liked?0:1);
		if (comp != null) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, comp.getCompId());
		}
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		this.addUserIdentity();
	}
	
	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			CompositionLikes compLike = new CompositionLikes(this.code, this.message);
			if(compLike.parseJsonObject(responseJsonObject)) {			
				this.pixObject = compLike;
			}
		}
	}
}
