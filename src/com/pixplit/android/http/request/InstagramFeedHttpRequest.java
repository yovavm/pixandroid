package com.pixplit.android.http.request;

import com.pixplit.android.data.InstagramItems;


public class InstagramFeedHttpRequest extends InstagramHttpRequest{
	
	public InstagramFeedHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			InstagramItems instaItems = new InstagramItems(this.code, this.message);
			if(instaItems.parseJsonObject(responseJsonObject)) {			
				this.pixObject = instaItems;				
			}
		}
	}
}
