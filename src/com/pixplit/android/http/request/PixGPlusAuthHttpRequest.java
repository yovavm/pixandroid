package com.pixplit.android.http.request;

import com.pixplit.android.data.PixAuthUserInfo;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class PixGPlusAuthHttpRequest extends BaseHttpRequest{
	public PixGPlusAuthHttpRequest(RequestDelegate delegate, PixUserInfo user){
		super(PixHttpAPI.GPLUS_AUTH_URL, delegate);
		if (user != null) {
			if (Utils.isValidString(user.getFullName())) {
				this.addParam(PixHttpAPI.FULL_NAME_PARAM, user.getFullName());
			}
			if (Utils.isValidString(user.getId())) {
				this.addParam(PixHttpAPI.GOOGLE_ID_PARAM, user.getId());
			}
			if (Utils.isValidString(user.getUserName())) {
				this.addParam(PixHttpAPI.GOOGLE_USERNAME_PARAM, user.getUserName());
			}
			if (Utils.isValidString(user.getToken())) {
				this.addParam(PixHttpAPI.TOKEN_PARAM, user.getToken());
			}
			if (Utils.isValidString(user.getAgeRange())) {
				this.addParam(PixHttpAPI.AGE_RANGE_PARAM, user.getAgeRange());
			}
			if (Utils.isValidString(user.getGender())) {
				this.addParam(PixHttpAPI.GENDER_PARAM, user.getGender());
			}
			if (Utils.isValidString(user.getLocale())) {
				this.addParam(PixHttpAPI.LOCALE_PARAM, user.getLocale());
			}
			if (Utils.isValidString(user.getImageUrl())) {
				this.addParam(PixHttpAPI.IMAGE_PARAM, user.getImageUrl());
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixAuthUserInfo authUserInfo = new PixAuthUserInfo(this.code, this.message);
			if(authUserInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = authUserInfo;
			}
		}
	}
}