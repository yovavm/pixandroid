package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixEmptyObject;


public class CompFlagHttpRequest extends BaseHttpRequest{
	
	public CompFlagHttpRequest(RequestDelegate delegate, PixComposition comp){
		super(PixHttpAPI.COMP_FLAG_URL, delegate);
		if (comp != null) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, comp.getCompId());
		}
		this.addUserIdentity();
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
    @Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
