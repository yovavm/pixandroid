package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;


public class ShareCompByEmailHttpRequest extends BaseVolleyHttpRequest{
	
	public ShareCompByEmailHttpRequest(String recipientAddress, String messageSubject, String messageBody){
		super(Method.POST, PixHttpAPI.COMP_SHARE_BY_EMAIL, null, Priority.LOW);
		if (Utils.isValidString(recipientAddress) && Utils.isValidString(messageBody)) {
			this.addParam(PixHttpAPI.TO_PARAM, recipientAddress);
			if (Utils.isValidString(messageSubject)) {
				this.addParam(PixHttpAPI.SUBJECT_PARAM, messageSubject);
			}
			this.addParam(PixHttpAPI.BODY_PARAM, messageBody);
			this.addUserIdentity();
		}
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
