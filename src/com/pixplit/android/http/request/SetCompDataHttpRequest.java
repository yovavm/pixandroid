package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.data.PixNewComp;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class SetCompDataHttpRequest extends BaseHttpRequest{
	
	public SetCompDataHttpRequest(RequestDelegate delegate, PixCompositionMetadata compMetadata){
		super(PixHttpAPI.COMP_SET_COMP_DATA, delegate);
		
		if (compMetadata != null && 
				compMetadata.getReservedCompId() != null &&
				!compMetadata.getReservedCompId().equalsIgnoreCase(PixDefs.defaultCompId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compMetadata.getReservedCompId());
			this.addParam(PixHttpAPI.ACTION_ID_PARAM, String.format("%d", compMetadata.getActionId()));
			this.addParam(PixHttpAPI.ROOT_ID_PARAM, compMetadata.getRootId());
			this.addParam(PixHttpAPI.PARENT_ID_PARAM, compMetadata.getParentId());
			if (compMetadata.getCompType() != null) {
				this.addParam(PixHttpAPI.COMP_TYPE_PARAM, String.format("%d", PixCompositionFramesetType.getInt(compMetadata.getCompType())));
			}
			this.addParam(PixHttpAPI.IS_COMPLETE_PARAM, compMetadata.isCompleted() ? "1" : "0");
			this.addParam(PixHttpAPI.INDEX_PARAM, String.format("%d", compMetadata.getNewComponentIndex()));
			if (Utils.isValidString(compMetadata.getTitle())) {
				this.addParam(PixHttpAPI.TITLE_PARAM, compMetadata.getTitle());
			}
			if (Utils.isValidString(compMetadata.getSubtitle())) {
				this.addParam(PixHttpAPI.SUBTITLE_PARAM, compMetadata.getSubtitle());
			}
			if (Utils.isValidString(compMetadata.getColorScheme())) {
				this.addParam(PixHttpAPI.COLOR_SCHEME_PARAM, compMetadata.getColorScheme());
			}
			if (Utils.isValidString(compMetadata.getTags())) {
				this.addParam(PixHttpAPI.TAGS_PARAM, compMetadata.getTags());
			}
			if (Utils.isValidString(compMetadata.getLocation())) {
				this.addParam(PixHttpAPI.LOCATION_PARAM, compMetadata.getLocation());
			}
			this.addParam(PixHttpAPI.HAS_BORDER_PARAM, compMetadata.hasBorder() ? "1" : "0");
			if (Utils.isValidString(compMetadata.getInviteesList())){
				this.addParam(PixHttpAPI.INVITEES_PARAM, compMetadata.getInviteesList());
			}
			if (Utils.isValidString(compMetadata.getShareToEmailAddress())) {
				this.mReqContext.putString(PixDefs.REQ_CTX_NEW_COMP_SHARE_BY_EMAIL, compMetadata.getShareToEmailAddress());
			}
			if (compMetadata.getShareWithFacebook()) {
				this.mReqContext.putBoolean(PixDefs.REQ_CTX_NEW_COMP_SHARE_TO_FB, true);
			}
			if (Utils.isValidString(compMetadata.getParentId()) && 
					!compMetadata.getParentId().equalsIgnoreCase(PixDefs.defaultParentId)) {
				this.mReqContext.putString(PixDefs.REQ_CTX_NEW_COMP_PARENT_ID, compMetadata.getParentId());
			}
		}
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		this.mReqContext.putInt(PixDefs.REQ_CTX_NEW_COMP_TYPE, PixDefs.REQ_CTX_NEW_FINAL_COMP);
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
    		PixNewComp newComp = new PixNewComp(this.code, this.message);
			if(newComp.parseJsonObject(responseJsonObject)) {			
				this.pixObject = newComp;
			}
		}
	}
}
