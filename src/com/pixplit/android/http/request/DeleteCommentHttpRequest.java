package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;


public class DeleteCommentHttpRequest extends BaseHttpRequest{
	
	public DeleteCommentHttpRequest(String compId, String commentId){
		super(PixHttpAPI.DELETE_COMMENT_URL, null);
		if (Utils.isValidString(compId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
		}
		if (Utils.isValidString(commentId)) {
			this.addParam(PixHttpAPI.COMMENT_ID_PARAM, commentId);
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
