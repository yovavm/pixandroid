package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class AdminRemoveSuggestedUserHttpRequest extends BaseHttpRequest{

	public AdminRemoveSuggestedUserHttpRequest(RequestDelegate delegate, String username){
		super(PixHttpAPI.ADMIN_REMOVE_SUGGESTED_USER_URL, delegate);
		this.addUserIdentity();
		this.addAdminIdentity();
		
		if (Utils.isValidString(username)) {
			this.addParam(PixHttpAPI.USERS_PARAM, username);
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
