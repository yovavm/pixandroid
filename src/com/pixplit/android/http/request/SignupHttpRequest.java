package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.data.PixAuthUserInfo;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class SignupHttpRequest extends BaseHttpRequest{
	
	public SignupHttpRequest(RequestDelegate delegate, PixUserInfo userInfo){
		super(PixHttpAPI.SIGNUP_URL, delegate);
		
		if (userInfo != null) {
			if (userInfo.getFullName() != null) {
				this.addParam(PixHttpAPI.FULL_NAME_PARAM, userInfo.getFullName());
			}
			if (userInfo.getEmail() != null) {
				this.addParam(PixHttpAPI.EMAIL_PARAM, userInfo.getEmail());
				String token = PixAppState.UserInfo.craetePixToken(userInfo.getEmail());
				if (token != null) {
					this.addParam(PixHttpAPI.TOKEN_PARAM, token);
				}
			}
			if (userInfo.getPassword() != null) {
				this.addParam(PixHttpAPI.PASSWORD_PARAM, userInfo.getPassword());
			}
		}
		if (userInfo.getUserPhoto() != null) {
			this.addBitmap(userInfo.getUserPhoto());
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixAuthUserInfo authUserInfo = new PixAuthUserInfo(this.code, this.message);
			if(authUserInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = authUserInfo;
			}
		}
	}
}
