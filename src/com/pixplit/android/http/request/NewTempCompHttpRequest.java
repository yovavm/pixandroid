package com.pixplit.android.http.request;

import android.graphics.Bitmap;

import com.pixplit.android.data.PixNewComp;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class NewTempCompHttpRequest extends BaseHttpRequest{
	RequestDelegate mCaller = null;
	
	public NewTempCompHttpRequest(RequestDelegate delegate, Bitmap compImage){
		super(PixHttpAPI.COMP_NEW_TEMP_COMP, delegate);
		if (compImage != null) {
			this.addBitmap(compImage);
		}
		
		this.mReqContext.putInt(PixDefs.REQ_CTX_NEW_COMP_TYPE, PixDefs.REQ_CTX_NEW_TEMP_COMP);
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
    		PixNewComp newComp = new PixNewComp(this.code, this.message);
			if(newComp.parseJsonObject(responseJsonObject)) {			
				this.pixObject = newComp;
			}
		}
	}
}
