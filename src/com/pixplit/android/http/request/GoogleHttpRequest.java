package com.pixplit.android.http.request;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixError;

public abstract class GoogleHttpRequest extends BaseVolleyHttpRequest {

	public GoogleHttpRequest(int method, String url, RequestDelegate delegate, Priority priority) {
		super(method, url, delegate, priority);
	}

	@Override
    protected Response<PixObject> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			JSONObject jsonObj = new JSONObject(json);
			
			this.code = -1;
			this.code = jsonObj.optInt("responseStatus", -1);
			this.message = jsonObj.optString("responseDetails", "");
		
			if (code == HttpStatus.SC_OK) {	
				this.responseJsonObject = jsonObj.optJSONObject("responseData");
				this.parseResponse();
			}
		
		} catch (UnsupportedEncodingException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		} catch (JSONException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		}
		return Response.success(pixObject, HttpHeaderParser.parseCacheHeaders(response) );
	}

}
