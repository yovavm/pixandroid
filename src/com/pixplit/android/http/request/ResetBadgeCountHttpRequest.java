package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;


public class ResetBadgeCountHttpRequest extends BaseHttpRequest{
	
	public ResetBadgeCountHttpRequest(){
		super(PixHttpAPI.RESET_BADGE_COUNT_URL, null);
		this.addParam(PixHttpAPI.NEWS_TIMESTAMP_PARAM, PixAppState.getTimestamp(PixAppState.NEWS_TIMESTAMP));
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
