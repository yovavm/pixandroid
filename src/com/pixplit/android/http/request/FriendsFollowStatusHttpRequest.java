package com.pixplit.android.http.request;

import com.pixplit.android.data.PixFriendsFollowStatus;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public class FriendsFollowStatusHttpRequest extends BaseVolleyHttpRequest{
	
	public FriendsFollowStatusHttpRequest(RequestDelegate delegate, String users){
		super(Method.POST, PixHttpAPI.FRIENDS_FOLLOW_STATUS_URL, delegate, Priority.HIGH);
		this.addUserIdentity();
		if (Utils.isValidString(users)) {
			this.addParam(PixHttpAPI.USERS_PARAM, users);
		}
		String fbUserId = PixAppState.UserInfo.getFbUserId();
		if (Utils.isValidString(fbUserId)) {
			this.addParam(PixHttpAPI.FACEBOOK_ID_PARAM, fbUserId);
		}
	}

	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixFriendsFollowStatus friendsFollowStatus = new PixFriendsFollowStatus(this.code, this.message);
			if(friendsFollowStatus.parseJsonObject(responseJsonObject)) {			
				this.pixObject = friendsFollowStatus;
			}
		}
	}
}
