package com.pixplit.android.http.request;

import com.pixplit.android.data.GoogleImages;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;


public class GoogleImagesHttpRequest extends GoogleHttpRequest{
	
	public GoogleImagesHttpRequest(RequestDelegate delegate, String query, Priority priority){
		super(Method.GET, PixHttpAPI.GOOGLE_IMAGE_SEARCH_URL, delegate, priority);
		if (Utils.isValidString(query)) {
			String queryEncoded = PixHttpAPI.encodeURIComponent(query);
			if (Utils.isValidString(queryEncoded)) {
				this.addParam(PixHttpAPI.GOOGLE_SEARCH_QUERY_PARAM, queryEncoded);
			}
		}
		this.addParam(PixHttpAPI.GOOGLE_SEARCH_VERSION_PARAM, PixHttpAPI.GOOGLE_SEARCH_VERSION_NUMBER);
		this.addParam(PixHttpAPI.GOOGLE_SEARCH_LICENSE_PARAM, PixHttpAPI.GOOGLE_SEARCH_LICENSE_PUBLIC);
		this.addParam(PixHttpAPI.GOOGLE_SEARCH_RESULTS_PER_PAGE_PARAM, PixHttpAPI.GOOGLE_SEARCH_RESULTS_PER_PAGE_NUM);
		this.addParam(PixHttpAPI.GOOGLE_SEARCH_FILE_TYPE_PARAM, PixHttpAPI.GOOGLE_SEARCH_FILE_TYPE_VAL);
		this.addParam(PixHttpAPI.GOOGLE_SEARCH_IMAGE_SIZE_PARAM, PixHttpAPI.GOOGLE_SEARCH_IMAGE_SIZE_VAL);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			GoogleImages images = new GoogleImages(this.code, this.message);
			if(images.parseJsonObject(responseJsonObject)) {			
				this.pixObject = images;				
			}
		}
	}
}
