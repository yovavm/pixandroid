package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.time.PixDateFormat;

public class UserFollowHttpRequest extends BaseVolleyHttpRequest{
	
	public UserFollowHttpRequest(RequestDelegate delegate, PixUser user, int followStatus){
		super(Method.POST, (followStatus==PixUser.FOLLOW_STATUS_NOT_FOLLOWING) ? 
				PixHttpAPI.USER_FOLLOW_URL : PixHttpAPI.USER_UNFOLLOW_URL, delegate, Priority.HIGH);
		
		// set previous state in order to rollback in case the request will finish with error
		this.getRequestContext().putInt(PixDefs.REQ_CTX_USER_FOLLOW_STATUS, followStatus);
		if (user != null) {
			this.addParam(PixHttpAPI.USERNAME_PARAM, user.getUsername());
		}
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
	
}
