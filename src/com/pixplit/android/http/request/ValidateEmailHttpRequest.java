package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.data.PixEmailValidation;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;


public class ValidateEmailHttpRequest extends BaseHttpRequest{
	public ValidateEmailHttpRequest(String email, RequestDelegate delegate){
		super(PixHttpAPI.VALIDATE_EMAIL_URL, delegate);
		if (Utils.isValidString(email)) {
			this.addParam(PixHttpAPI.EMAIL_PARAM, email);
			String token = PixAppState.UserInfo.craetePixToken(email);
			if (token != null) {
				this.addParam(PixHttpAPI.TOKEN_PARAM, token);
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
		
	}
	
    @Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixEmailValidation emailValidation = new PixEmailValidation(this.code, this.message);
			if(emailValidation.parseJsonObject(responseJsonObject)) {			
				this.pixObject = emailValidation;
			}
		}
	}
}
