package com.pixplit.android.http.request;

import com.pixplit.android.data.PixAuthUserInfo;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class PixFbAuthHttpRequest extends BaseHttpRequest{
	public PixFbAuthHttpRequest(RequestDelegate delegate, PixUserInfo user){
		super(PixHttpAPI.FB_AUTH_URL, delegate);
		
		if (user != null) {
			if (Utils.isValidString(user.getFullName())) {
				this.addParam(PixHttpAPI.FULL_NAME_PARAM, user.getFullName());
			}
			if (Utils.isValidString(user.getId())) {
				this.addParam(PixHttpAPI.FACEBOOK_ID_PARAM, user.getId());
			}
			if (Utils.isValidString(user.getUserName())) {
				this.addParam(PixHttpAPI.FACEBOOK_USERNAME_PARAM, user.getUserName());
			}
			if (Utils.isValidString(user.getToken())) {
				this.addParam(PixHttpAPI.TOKEN_PARAM, user.getToken());
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixAuthUserInfo authUserInfo = new PixAuthUserInfo(this.code, this.message);
			if(authUserInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = authUserInfo;
			}
		}
	}
}
