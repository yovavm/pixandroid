package com.pixplit.android.http.request;

import com.pixplit.android.util.Utils;
import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;


public class ConversationMessageAckHttpRequest extends BaseHttpRequest{
	
	public ConversationMessageAckHttpRequest(String conversationId, String messageId){
		super(PixHttpAPI.CONVERSATION_MESSAGE_ACK_URL, null);
		if (Utils.isValidString(conversationId)) {
			this.addParam(PixHttpAPI.CONVERSATION_ID_PARAM, conversationId);
		}
		if (Utils.isValidString(messageId)) {
			this.addParam(PixHttpAPI.MESSAGE_ID_PARAM, messageId);
		}
		
		this.setRequest(RestClient.RequestMethod.GET);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
