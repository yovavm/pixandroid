package com.pixplit.android.http.request;

import com.pixplit.android.data.PixConversation;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public class ConversationFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public ConversationFeedHttpRequest(RequestDelegate delegate, String conversationId, String recipient){
		super(Method.GET, PixHttpAPI.CONVERSATION_FEED_URL, delegate, Priority.IMMEDIATE);
		if (Utils.isValidString(conversationId)) {
			this.addParam(PixHttpAPI.CONVERSATION_ID_PARAM, conversationId);
		}
		if (Utils.isValidString(recipient)) {
			this.addParam(PixHttpAPI.RECIPIENT_PARAM, recipient);
		}
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixConversation conversation = new PixConversation(this.code, this.message);
			if(conversation.parseJsonObject(responseJsonObject)) {			
				this.pixObject = conversation;
			}
		}
	}
    
}
