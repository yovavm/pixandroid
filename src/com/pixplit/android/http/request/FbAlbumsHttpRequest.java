package com.pixplit.android.http.request;

import com.pixplit.android.data.FacebookAlbums;


public class FbAlbumsHttpRequest extends FbHttpRequest{
	
	public FbAlbumsHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			FacebookAlbums facebookAlbums = new FacebookAlbums(this.code, this.message);
			if(facebookAlbums.parseJsonObject(responseJsonObject)) {			
				this.pixObject = facebookAlbums;				
			}
		}
	}
}
