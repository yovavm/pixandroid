package com.pixplit.android.http.request;

import com.pixplit.android.data.PixNewRecords;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;

public class NewRecordsHttpRequest extends BaseVolleyHttpRequest{
	
	public NewRecordsHttpRequest(RequestDelegate delegate){
		super(Method.POST, PixHttpAPI.NEW_RECORDS_URL, delegate, Priority.LOW);
		
		this.addParam(PixHttpAPI.NEWS_TIMESTAMP_PARAM, PixAppState.getTimestamp(PixAppState.NEWS_TIMESTAMP));
		this.addParam(PixHttpAPI.ACTIONS_TIMESTAMP_PARAM, PixAppState.getTimestamp(PixAppState.ACTIONS_TIMESTAMP));
		this.addParam(PixHttpAPI.COMPLETED_TIMESTAMP_PARAM, PixAppState.getTimestamp(PixAppState.COMPLETED_TIMESTAMP));
		this.addUserIdentity();
		
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixNewRecords newRecords = new PixNewRecords(this.code, this.message);
			if(newRecords.parseJsonObject(responseJsonObject)) {			
				this.pixObject = newRecords;
			}
		}
	}
}
