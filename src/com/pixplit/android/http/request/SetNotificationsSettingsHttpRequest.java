package com.pixplit.android.http.request;

import com.pixplit.android.data.PixNotificationsSettings;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class SetNotificationsSettingsHttpRequest extends BaseHttpRequest{
	public SetNotificationsSettingsHttpRequest(RequestDelegate delegate, PixNotificationsSettings settings){
		super(PixHttpAPI.SET_NOTIFICATIONS_SETTINGS_URL, delegate);
		
		if (settings != null) {
			this.addParam(PixHttpAPI.NOTIFY_LIKED_PARAM, String.format("%d", settings.getLiked()));
			this.addParam(PixHttpAPI.NOTIFY_COMMENTED_PARAM, String.format("%d", settings.getCommented()));
			this.addParam(PixHttpAPI.NOTIFY_JOINED_PARAM, String.format("%d", settings.getJoined()));
			this.addParam(PixHttpAPI.NOTIFY_INVITED_PARAM, String.format("%d", settings.getInvited()));
			this.addParam(PixHttpAPI.NOTIFY_FOLLOWED_PARAM, String.format("%d", settings.getFollowed()));
			this.addParam(PixHttpAPI.NOTIFY_FB_FRIEND_PARAM, String.format("%d", settings.getFbFriendJoined()));
			this.addParam(PixHttpAPI.NOTIFY_SYSTEM_PARAM, String.format("%d", settings.getSystem()));
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixNotificationsSettings settings = new PixNotificationsSettings(this.code, this.message);
			if(settings.parseJsonObject(responseJsonObject)) {			
				this.pixObject = settings;
			}
		}
	}
	
}
