package com.pixplit.android.http.request;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;


public class ViewAckHttpRequest extends BaseHttpRequest{
	
	public ViewAckHttpRequest(PixComposition comp){
		super(PixHttpAPI.VIEW_ACK_URL_PREFIX + ((comp != null) ? comp.getCompId() : ""), null);
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
