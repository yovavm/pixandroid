package com.pixplit.android.http.request;

import com.pixplit.android.data.PixPopularCompositions;


public class AdminPopularFullFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public AdminPopularFullFeedHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
		this.addUserIdentity();
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixPopularCompositions compositions = new PixPopularCompositions(this.code, this.message);
			if(compositions.parseJsonObject(responseJsonObject)) {			
				this.pixObject = compositions;				
			}
		}
	}
}
