package com.pixplit.android.http.request;

import com.pixplit.android.data.PixChallenges;
import com.pixplit.android.http.PixHttpAPI;

public class ChallengesFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public ChallengesFeedHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.CHALLENGES_FEED_URL, delegate, Priority.IMMEDIATE);
		this.addUserIdentity();
	}

	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixChallenges challenges = new PixChallenges(this.code, this.message);
			if(challenges.parseJsonObject(responseJsonObject)) {			
				this.pixObject = challenges;
			}
		}
	}
}
