package com.pixplit.android.http.request;

import com.pixplit.android.data.PixMessage;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class UpdateCoverPicHttpRequest extends BaseHttpRequest{
	public UpdateCoverPicHttpRequest(RequestDelegate delegate, int coverIndex){
		super(PixHttpAPI.UPDATE_COVER_PIC_URL, delegate);
		this.addParam(PixHttpAPI.COVER_PIC_ID_PARAM, String.format("%d", coverIndex));
		this.setRequest(RestClient.RequestMethod.GET);
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixMessage message = new PixMessage(this.code, this.message);
			if(message.parseJsonObject(responseJsonObject)) {			
				this.pixObject = message;
			}
		}
	}
	
}
