package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixNotificationsSettings;

public class GetNotificationsSettingsHttpRequest extends BaseHttpRequest{
	
	public GetNotificationsSettingsHttpRequest(RequestDelegate delegate){
		super(PixHttpAPI.NOTIFICATIONS_SETTINGS_URL, delegate);
		this.setRequest(RestClient.RequestMethod.GET);
		this.addUserIdentity();
		
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixNotificationsSettings notificationsSettings = new PixNotificationsSettings(this.code, this.message);
			if(notificationsSettings.parseJsonObject(responseJsonObject)) {			
				this.pixObject = notificationsSettings;
			}
		}
	}
	
}
