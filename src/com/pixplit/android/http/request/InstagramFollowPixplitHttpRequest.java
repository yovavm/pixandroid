package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;


public class InstagramFollowPixplitHttpRequest extends InstagramHttpRequest{
	
	public InstagramFollowPixplitHttpRequest(RequestDelegate delegate){
		super(Method.POST, PixHttpAPI.INSTAGRAM_FOLLOW_PIXPLIT_URL, delegate, Priority.IMMEDIATE);
		this.addParam("action", "follow");
	}
	
    @Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
