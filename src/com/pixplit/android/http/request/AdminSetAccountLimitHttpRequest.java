package com.pixplit.android.http.request;

import com.pixplit.android.data.PixAccount;
import com.pixplit.android.data.PixAccounts;
import com.pixplit.android.http.PixHttpAPI;

public class AdminSetAccountLimitHttpRequest extends BaseHttpRequest{
	
	public AdminSetAccountLimitHttpRequest(RequestDelegate delegate, PixAccount account, int limit){
		super(PixHttpAPI.ADMIN_EUREKA_UPDATE_ACCOUNT_LIMIT_URL_PREFIX + ((account != null)?account.getUsername():""), delegate);
		this.addParam(PixHttpAPI.COUNT_PARAM, Integer.valueOf(limit).toString());
		this.addUserIdentity();
		this.addAdminIdentity();
		this.setRequest(RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
    		PixAccounts accounts = new PixAccounts(this.code, this.message);
			if(accounts.parseJsonObject(responseJsonObject)) {			
				this.pixObject = accounts;
			}
		}
	}
}
