package com.pixplit.android.http.request;

import com.pixplit.android.data.PixAuthUserInfo;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class SignInHttpRequest extends BaseHttpRequest{

	public SignInHttpRequest(RequestDelegate delegate, PixUserInfo userInfo){
		super(PixHttpAPI.LOGIN_URL, delegate);
		if (userInfo != null) {
			if (Utils.isValidString(userInfo.getEmail())) {
				this.addParam(PixHttpAPI.EMAIL_PARAM, userInfo.getEmail());
				String token = PixAppState.UserInfo.craetePixToken(userInfo.getEmail());
				if (token != null) {
					this.addParam(PixHttpAPI.TOKEN_PARAM, token);
				}
			}
			if (userInfo.getPassword() != null) {
				this.addParam(PixHttpAPI.PASSWORD_PARAM, userInfo.getPassword());
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixAuthUserInfo authUserInfo = new PixAuthUserInfo(this.code, this.message);
			if(authUserInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = authUserInfo;
			}
		}
	}
}
