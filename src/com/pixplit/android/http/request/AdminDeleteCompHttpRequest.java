package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class AdminDeleteCompHttpRequest extends BaseHttpRequest{

	public AdminDeleteCompHttpRequest(RequestDelegate delegate, String compId){
		super(PixHttpAPI.ADMIN_DELETE_COMP_URL, delegate);
		this.addUserIdentity();
		this.addAdminIdentity();
		
		if (Utils.isValidString(compId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
		}
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
