package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;
import com.pixplit.android.data.PixComments;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;

public class PostCommentHttpRequest extends BaseHttpRequest{
	
	public PostCommentHttpRequest(RequestDelegate delegate, String compId, String comment){
		super(PixHttpAPI.COMP_ADD_COMMENT_URL, delegate);
		
		// set previous state in order to rollback in case the request will finish with error
		this.getRequestContext().putBoolean(PixDefs.REQ_CTX_COMP_ADD_COMMENT, true);
		if (Utils.isValidString(compId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
		}
		if (Utils.isValidString(comment)) {
			this.addParam(PixHttpAPI.TEXT_PARAM, comment);
		}
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
		
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixComments comments = new PixComments(this.code, this.message);
			if(comments.parseJsonObject(responseJsonObject)) {			
				this.pixObject = comments;
			}
		}
	}
    
    
}
