package com.pixplit.android.http.request;

import com.pixplit.android.data.PixAccounts;
import com.pixplit.android.http.PixHttpAPI;

public class AdminEurekaAccountsHttpRequest extends BaseVolleyHttpRequest{
	
	public AdminEurekaAccountsHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.ADMIN_EUREKA_ACCOUNTS_URL, delegate, Priority.HIGH);
		this.addUserIdentity();
		this.addAdminIdentity();
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
    		PixAccounts accounts = new PixAccounts(this.code, this.message);
			if(accounts.parseJsonObject(responseJsonObject)) {			
				this.pixObject = accounts;
			}
		}
	}
}
