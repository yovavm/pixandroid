package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixEmptyObject;

public class AdminLogoutHttpRequest extends BaseHttpRequest{
	
	public AdminLogoutHttpRequest(){
		super(PixHttpAPI.ADMIN_LOGOUT_URL, null);
		this.addAdminIdentity();
		this.addUserIdentity();
		this.setRequest(RestClient.RequestMethod.POST);
		
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
