package com.pixplit.android.http.request;

import com.pixplit.android.data.PixNotifications;
import com.pixplit.android.http.PixHttpAPI;

public class NotificationsFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public NotificationsFeedHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.NOTIFICATIONS_URL, delegate, Priority.IMMEDIATE);
		this.addUserIdentity();
	}

    @Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
			PixNotifications notifications = new PixNotifications(this.code, this.message);
			if(notifications.parseJsonObject(responseJsonObject)) {			
				this.pixObject = notifications;
			}
		}
	}
}
