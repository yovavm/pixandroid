package com.pixplit.android.http.request;

import com.pixplit.android.data.PixCheckVersion;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;

public class CheckVersionHttpRequest extends BaseVolleyHttpRequest{
	public CheckVersionHttpRequest(RequestDelegate delegate){
		super(Method.GET, PixHttpAPI.CHECK_VERSION_URL, delegate, Priority.NORMAL);
		this.addParam(PixHttpAPI.VERSION_PARAM, PixAppState.getAppVersionNumber());
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixCheckVersion checkVersion = new PixCheckVersion(this.code, this.message);
			if(checkVersion.parseJsonObject(responseJsonObject)) {			
				this.pixObject = checkVersion;
			}
		}
	}
	
}
