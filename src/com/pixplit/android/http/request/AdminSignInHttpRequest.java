package com.pixplit.android.http.request;

import com.pixplit.android.data.AdminAuthInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;

public class AdminSignInHttpRequest extends BaseHttpRequest{

	public AdminSignInHttpRequest(RequestDelegate delegate, String password){
		super(PixHttpAPI.ADMIN_AUTHENTICATE_URL, delegate);
		String username = UserInfo.getUserName();
		if (Utils.isValidString(username)) {
			String token = PixAppState.UserInfo.craetePixToken(username);
			if (token != null) {
				this.addParam(PixHttpAPI.TOKEN_PARAM, token);
			}
			if (Utils.isValidString(password)) {
				this.addParam(PixHttpAPI.PASSWORD_PARAM, password);
			}
		}
		this.addUserIdentity();
		this.setRequest(RestClient.RequestMethod.POST);
	}
	
	@Override
	protected void parseResponse() {
    	if (this.responseJsonObject != null) {
    		AdminAuthInfo adminUserInfo = new AdminAuthInfo(this.code, this.message);
			if(adminUserInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = adminUserInfo;
			}
		}
	}
}
