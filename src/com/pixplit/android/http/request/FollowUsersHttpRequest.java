package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;
import com.pixplit.android.data.PixEmptyObject;

public class FollowUsersHttpRequest extends BaseHttpRequest{
	
	public FollowUsersHttpRequest(String followUsers, String notifyUsers){
		super(PixHttpAPI.FOLLOW_USERS_URL, null);
		this.addUserIdentity();
		this.setRequest(RestClient.RequestMethod.POST);
		this.addParam(PixHttpAPI.TIMESTAMP_PARAM, PixDateFormat.getPixDateInstance().getTimestamp());
		if (Utils.isValidString(followUsers)) {
			this.addParam(PixHttpAPI.FOLLOW_USERS_PARAM, followUsers);
		}
		if (Utils.isValidString(notifyUsers)) {
			this.addParam(PixHttpAPI.NOTIFY_USERS_PARAM, notifyUsers);
		}
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
