package com.pixplit.android.http.request;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.fb.FacebookSessionManager;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public abstract class FbHttpRequest extends BaseVolleyHttpRequest {

	public FbHttpRequest(int method, String url, RequestDelegate delegate, Priority priority) {
		super(method, url, delegate, priority);
		
		// add facebook access token - only if missing from url - on paging the url of next has a token
		if (!url.contains(PixHttpAPI.FACEBOOK_ACCESS_TOKEN_PARAM)) {
			String accessToken = FacebookSessionManager.getAccessToken();
			if (Utils.isValidString(accessToken)) {
				addParam(PixHttpAPI.FACEBOOK_ACCESS_TOKEN_PARAM, accessToken);
			}
		}
	}

	@Override
    protected Response<PixObject> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			JSONObject jsonObj = new JSONObject(json);
			
			this.code = -1;
			if (jsonObj != null) {
				this.code = HttpStatus.SC_OK;
				this.message = jsonObj.optString("message", "");
			}

			if (code == HttpStatus.SC_OK) {	
				this.responseJsonObject = jsonObj;
				this.parseResponse();
			}
		
		} catch (UnsupportedEncodingException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		} catch (JSONException e) {
			this.code = PixError.PixErrorCodeNetworkFailure;
			e.printStackTrace();
			return Response.error(new ParseError(e));
		}
		return Response.success(pixObject, null/* HttpHeaderParser.parseCacheHeaders(response)*/ );
	}

}
