package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixAccountInfo;
import com.pixplit.android.data.PixEditAccountInfo;

public class EditAccountInfoHttpRequest extends BaseHttpRequest{
	public EditAccountInfoHttpRequest(RequestDelegate delegate, PixEditAccountInfo editedAccountInfo){
		super(PixHttpAPI.EDIT_ACCOUNT_INFO_URL, delegate);
		
		if (editedAccountInfo != null) {
			if (editedAccountInfo.getFullName() != null) {
				addParam(PixHttpAPI.FULL_NAME_PARAM, editedAccountInfo.getFullName());
			}
			if (editedAccountInfo.getPwd() != null) {
				addParam(PixHttpAPI.PASSWORD_PARAM, editedAccountInfo.getPwd());
			}
			if (editedAccountInfo.getEmail() != null) {
				addParam(PixHttpAPI.EMAIL_PARAM, editedAccountInfo.getEmail());
			}
			if (editedAccountInfo.getDelimg() == 1) {
				addParam(PixHttpAPI.DELETE_IMAGE_PARAM, String.format("%d", editedAccountInfo.getDelimg()));
			}
			if (editedAccountInfo.getImg() != null) {
				addBitmap(editedAccountInfo.getImg());
			}
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixAccountInfo accountInfo = new PixAccountInfo(this.code, this.message);
			if(accountInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = accountInfo;
			}
		}
	}
	
}
