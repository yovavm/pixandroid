package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixEmptyObject;

public class LogoutHttpRequest extends BaseHttpRequest{
	
	public LogoutHttpRequest(){
		super(PixHttpAPI.LOGOUT_URL, null);
		this.addUserIdentity();
		this.setRequest(RestClient.RequestMethod.GET);
		
	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
