package com.pixplit.android.http.request;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;
import com.pixplit.android.data.PixDeleteComp;


public class DeleteCompHttpRequest extends BaseHttpRequest{
	
	public DeleteCompHttpRequest(String compId, RequestDelegate delegate){
		super(PixHttpAPI.COMP_DELETE_COMP, delegate);
		if (Utils.isValidString(compId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
			this.mReqContext.putString(PixDefs.REQ_CTX_COMP_ID, compId);
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixDeleteComp deleteComp = new PixDeleteComp(this.code, this.message);
			if (deleteComp.parseJsonObject(responseJsonObject)) {
				this.pixObject = deleteComp;
			}
		}
		
	}
	
}
