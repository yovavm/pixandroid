package com.pixplit.android.http.request;

import com.pixplit.android.data.PixEmptyObject;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;


public class ConversationHideHttpRequest extends BaseHttpRequest{
	
	public ConversationHideHttpRequest(String conversationId){
		super(PixHttpAPI.CONVERSATION_HIDE_URL, null);
		if (Utils.isValidString(conversationId)) {
			this.addParam(PixHttpAPI.CONVERSATION_ID_PARAM, conversationId);
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();

	}
	
	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
