package com.pixplit.android.http.request;

import com.pixplit.android.data.PixUsers;


public class AdminSuggestedFeedHttpRequest extends BaseVolleyHttpRequest{
	
	public AdminSuggestedFeedHttpRequest(RequestDelegate delegate, String url, Priority priority){
		super(Method.GET, url, delegate, priority);
		this.addUserIdentity();
	}
	
	@Override 
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixUsers users = new PixUsers(this.code, this.message);
			if(users.parseJsonObject(responseJsonObject)) {			
				this.pixObject = users;
			}
		}
	}
}
