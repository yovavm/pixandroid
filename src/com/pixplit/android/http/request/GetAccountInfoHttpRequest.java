package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.data.PixAccountInfo;

public class GetAccountInfoHttpRequest extends BaseHttpRequest{
	
	public GetAccountInfoHttpRequest(RequestDelegate delegate){
		super(PixHttpAPI.GET_ACCOUNT_INFO_URL, delegate);
		this.setRequest(RestClient.RequestMethod.GET);
		this.addUserIdentity();
		
	}

	@Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixAccountInfo accountInfo = new PixAccountInfo(this.code, this.message);
			if(accountInfo.parseJsonObject(responseJsonObject)) {			
				this.pixObject = accountInfo;
			}
		}
	}
	
}
