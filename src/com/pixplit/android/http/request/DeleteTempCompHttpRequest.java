package com.pixplit.android.http.request;

import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.RestClient;
import com.pixplit.android.util.Utils;
import com.pixplit.android.data.PixEmptyObject;


public class DeleteTempCompHttpRequest extends BaseHttpRequest{
	
	public DeleteTempCompHttpRequest(String compId){
		super(PixHttpAPI.COMP_DELETE_TEMP_COMP, null);
		if (Utils.isValidString(compId)) {
			this.addParam(PixHttpAPI.COMP_ID_PARAM, compId);
		}
		this.setRequest(RestClient.RequestMethod.POST);
		this.addUserIdentity();
		
	}
	
    @Override
	protected void parseResponse() {
		if (this.responseJsonObject != null) {
			PixEmptyObject emptyResponse = new PixEmptyObject(this.code, this.message);
			this.pixObject = emptyResponse;
		}
	}
}
