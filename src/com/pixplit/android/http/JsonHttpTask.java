package com.pixplit.android.http;

import android.os.AsyncTask;

import com.pixplit.android.http.request.BaseHttpRequest;


public class JsonHttpTask extends AsyncTask<Void, Integer, Void>{
//	private final static String TAG = "JsonHttpTask";
	private BaseHttpRequest client;
	
	public JsonHttpTask(BaseHttpRequest client){
		this.client = client;
    }
    
	protected Void doInBackground(Void... jsonUrl) {
		
		try {
			if (client != null) {
				client.execute();
			}
		}
		catch (Exception ex){
			//Utils.pixLog(TAG, "Exception was thrown while doing work in BG:" +ex.toString());
			ex.printStackTrace();
		}
		return null;
    }
	
	// Called on the UI thread
    protected void onPostExecute(Void result) {
    	//Utils.pixLog(TAG, "Done!");
    	if (client != null) {
    		client.onRequestFinish();
    	}
    }
}