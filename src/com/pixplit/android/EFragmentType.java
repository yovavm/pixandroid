package com.pixplit.android;

public enum EFragmentType {
	PIX_FRAGMENT {
	    public String toString() {
	        return "Pix Base Fragment";
	    }			
	}, 
	MAIN_MENU {
	    public String toString() {
	        return "Main Menu";
	    }			
	}, 
	POPULAR {
	    public String toString() {
	        return "Popular";
	    }			
	},
	PROFILE {
	    public String toString() {
	        return "Profile";
	    }			
	},
	MY_COMPLETED {
	    public String toString() {
	        return "My Completed";
	    }			
	},
	MY_PLAYGROUND {
	    public String toString() {
	        return "My Playground";
	    }			
	},
	RELATED {
	    public String toString() {
	        return "Related";
	    }			
	},
	CHALLENGES {
	    public String toString() {
	        return "Challenges";
	    }			
	},
	FOLLOWING {
	    public String toString() {
	        return "Following";
	    }			
	},
	FOLLOWERS {
	    public String toString() {
	        return "Followers";
	    }			
	},
	LIKERS {
	    public String toString() {
	        return "Likers";
	    }			
	},FEATURED {
	    public String toString() {
	        return "Featured";
	    }			
	},
	SINGLE_VIEW {
	    public String toString() {
	        return "Single View";
	    }			
	},
	GALLERY_VIEW {
	    public String toString() {
	        return "Gallery View";
	    }			
	},
	COMMENTS {
	    public String toString() {
	        return "Commnets";
	    }			
	},
	ACCOUNTS {
	    public String toString() {
	        return "Accounts";
	    }			
	},
	NEWS {
	    public String toString() {
	        return "News";
	    }			
	},
	INSTAGRAM_PICK_PHOTO {
	    public String toString() {
	        return "Instagram pick photo";
	    }			
	},
	GOOGLE_PICK_PHOTO {
	    public String toString() {
	        return "Google pick photo";
	    }			
	},
	FACEBOOK_PICK_ALBUM {
	    public String toString() {
	        return "Facebook pick album";
	    }			
	},
	FACEBOOK_PICK_PHOTO {
	    public String toString() {
	        return "Facebook pick photo";
	    }			
	},
	SETTINGS {
	    public String toString() {
	        return "Settings";
	    }			
	},
	FB_SETTINGS {
	    public String toString() {
	        return "Facebook Settings";
	    }			
	},
	ADMIN_SETTINGS {
	    public String toString() {
	        return "Admin Settings";
	    }			
	},
	ADMIN_POPULAR_FULL {
	    public String toString() {
	        return "Admin Popular Full";
	    }			
	},
	ADMIN_SUGGESTED_FULL {
	    public String toString() {
	        return "Admin Suggested Full";
	    }			
	},
	ADMIN_POPULAR_CANDIDATES {
	    public String toString() {
	        return "Admin Popular Candidates";
	    }			
	},
	CREATE_SPLIT {
	    public String toString() {
	        return "Create Split";
	    }			
	},
	PROCESS_SPLIT {
	    public String toString() {
	        return "Edit Split";
	    }			
	},
	SCALE_IMPORTED_PHOTO_IN_SPLIT {
	    public String toString() {
	        return "Move and Scale imported photo in Split";
	    }			
	},
	PUBLISH_SPLIT {
	    public String toString() {
	        return "Publish Split";
	    }			
	},
	CHOOSE_FRAME {
	    public String toString() {
	        return "Choose Frame";
	    }			
	},
	INVITE_FOLLOWERS {
	    public String toString() {
	        return "Invite Followers";
	    }			
	},
	FOLLOW_FRIENDS {
	    public String toString() {
	        return "Follow Friends";
	    }			
	},
	SEARCH_USERS {
	    public String toString() {
	        return "Search Users";
	    }			
	},
	SIGNUP {
	    public String toString() {
	        return "Signup";
	    }			
	},
	SIGNIN {
	    public String toString() {
	        return "Signin";
	    }			
	},
	EDIT_PROFILE {
	    public String toString() {
	        return "Edit Profile";
	    }			
	},
	FORGOT_PASSWORD {
	    public String toString() {
	        return "Forgot password";
	    }			
	},
	WEB_VIEW {
	    public String toString() {
	        return "Web View";
	    }			
	},
	SETTINGS_PUSH_NOTIFICATION {
	    public String toString() {
	        return "Settings - push notifications";
	    }			
	},
	PIXPLIT_INFO {
	    public String toString() {
	        return "Pixplit Info";
	    }			
	},
	CHANGE_COVER {
	    public String toString() {
	        return "Change Cover";
	    }			
	},
	QUICK_GUIDE {
	    public String toString() {
	        return "Quick Guide";
	    }			
	},	
	INBOX {
	    public String toString() {
	        return "Conversations";
	    }			
	},
	CHAT {
	    public String toString() {
	        return "Chat";
	    }			
	},
	ADMIN_EUREKA_SIGNUP {
	    public String toString() {
	        return "Admin Eureka Signup";
	    }			
	}
}
