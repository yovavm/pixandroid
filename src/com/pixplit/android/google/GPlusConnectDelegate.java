package com.pixplit.android.google;


public interface GPlusConnectDelegate {
	public void onGPlusConnectFinished(GPlusProcessType processType);
	public void onGPlusConnectFailed(GPlusProcessType processType);
}