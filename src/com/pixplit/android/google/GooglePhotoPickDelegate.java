package com.pixplit.android.google;

import com.pixplit.android.data.GoogleImage;

public interface GooglePhotoPickDelegate {
	public void onPhotoPick(GoogleImage item);
}
