package com.pixplit.android.google;

import android.util.SparseArray;

public enum GPlusProcessType {
	GPLUS_PROCESS_IDLE				(0),
	GPLUS_PROCESS_AUTHENTICATION	(1),
	GPLUS_PROCESS_LOGOUT			(2),
	GPLUS_PROCESS_LINK_ACCOUNT		(3);
	
	private final int type;
	
	GPlusProcessType(int type) {
		this.type = type;
	}
	
	private static final SparseArray<GPlusProcessType> map = new SparseArray<GPlusProcessType>();
	
	static {
		for (GPlusProcessType processType : GPlusProcessType.values()) {
			map.put(processType.type, processType);
		}
	}
	
	public static GPlusProcessType getType(int intType) {
		return map.get(intType);
	}
	
	public static int getInt(GPlusProcessType enumType) {
		if (enumType != null) {
			return enumType.type;
		}
		return -1;
	}
}
