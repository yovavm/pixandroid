package com.pixplit.android.google;

public interface GooglePlusActivityInterface {
	public void signInWithGPlus();
	public void linkGPlusAccount(GPlusConnectDelegate delegate);
	public void logoutGPlus();
}
