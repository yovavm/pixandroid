package com.pixplit.android;

import java.util.Timer;
import java.util.TimerTask;

import com.pixplit.android.data.PixNewRecords;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.NewRecordsHttpRequest;
import com.pixplit.android.ui.activities.PixBaseActivity;

public class NotificationsManager implements RequestDelegate{
//	private static final String TAG = "NotificationsManager";
	private static Timer timer = null;
	private FetchNewRecordsTask fetchNewRecordsTask;
	private onNewNotificationsListener mDelegate = null;
	private PixBaseActivity activity;
	
	public interface onNewNotificationsListener {
		public void onNewUnreadConversations(int newUnreadConversationsNum);
		public void onNewNewsRecords(int newRecordsNum);
		public void onNewActions(int newActionsNum);
		public void onNewCompleted(int newCompletedNum);
		public void onError(PixError error);
	}
		
	public NotificationsManager(PixBaseActivity activity, onNewNotificationsListener onNewNotificationsListener) {
		this.mDelegate = onNewNotificationsListener;
		this.activity = activity;
	}
	
	public void reStart() {
		stop();
		start(0);
	}
	
	public void onResume() {
		this.start(PixDefs.notificationTimerStartDelay);
	}
	
	public void onPause() {
		this.stop();
	}
	
	private void start(long delay) {
//		if (!UserInfo.isUserLogedin()) {
//			if (!Utils.getSharedPreferencesBoolean(PixDefs.WELCOME_NEWS_READ, false)) {
//				if (mDelegate != null) {
//					PixAppState.setNewNewsNumber(1);
//					mDelegate.onNewNewsRecords(1);
//				}
//				return;
//			}
//		}
		stop(); // stop any older timer instance if exist.
		timer = new Timer();
		fetchNewRecordsTask = new FetchNewRecordsTask();
		timer.schedule(fetchNewRecordsTask, delay, PixDefs.notificationTimerInterval);
	}
	
	private void stop() {
		if (timer != null) {
			timer.cancel();
			fetchNewRecordsTask = null;
			timer = null;
		}
	}
	
	public void onDestroy() {
		mDelegate = null;
	}
	
	@Override
	public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
		if (response != null && 
				response instanceof PixNewRecords && 
				PixAppState.UserInfo.isUserLogedin()) {
			PixNewRecords newRecords = (PixNewRecords)response;
			this.onNewNewsRecords(newRecords.getNotifications());
			this.onNewActions(newRecords.getActions());
			this.onNewCompleted(newRecords.getCompleted());
			this.onNewConversationsUnread(newRecords.getConversations());
		}
	}

	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		if (mDelegate != null) {
			mDelegate.onError(error);
		}
	}
	
	private void onNewNewsRecords(int newRecordsNum) {
		int oldRecordsNum = PixAppState.getNewNewsNumber();
		if (newRecordsNum != oldRecordsNum) {
			//Utils.pixLog(TAG, "got "+newRecordsNum+" new news records");
			PixAppState.setNewNewsNumber(newRecordsNum);
			if (mDelegate != null) {
				mDelegate.onNewNewsRecords(newRecordsNum);
			}
		}
	}
	
	private void onNewConversationsUnread(int unreadConversationsNum) {
		int oldUnreadNum = PixAppState.getNewConversationsNumber();
		if (unreadConversationsNum != oldUnreadNum) {
			//Utils.pixLog(TAG, "got "+unreadConversationsNum+" new unread conversations");
			PixAppState.setNewConversationsNumber(unreadConversationsNum);
			if (mDelegate != null) {
				mDelegate.onNewUnreadConversations(unreadConversationsNum);
			}
		}
	}
	
	private void onNewActions(int newActionsNum) {
		//Utils.pixLog(TAG, "got "+newActionsNum+" new actions records");
		if (mDelegate != null) {
			mDelegate.onNewActions(newActionsNum);
		}
	}

	private void onNewCompleted(int newCompletedNum) {
		//Utils.pixLog(TAG, "got "+newCompletedNum+" new completed records");
		if (mDelegate != null) {
			mDelegate.onNewCompleted(newCompletedNum);
		}
	}

	private class FetchNewRecordsTask extends TimerTask {
		@Override
		public void run() {
			if (PixAppState.UserInfo.isUserLogedin() && NotificationsManager.this.activity != null) {
				PixAppState.getState().getRequestQueue().add(new NewRecordsHttpRequest(NotificationsManager.this));
			}
		}
	}
}
