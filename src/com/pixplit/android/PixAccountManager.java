package com.pixplit.android;

import android.accounts.Account;
import android.accounts.AccountManager;

public class PixAccountManager {
//	private static final String TAG = "PixAccountManager";
	private static PixAccountManager instance = null;
	
	public static final int PIX_GOOGLE_ACCOUNT = 1;
	
	public static PixAccountManager getInstance() {
		if (instance == null) {
			instance = new PixAccountManager();
		}
		return instance;
	}
	
	public static String getFirstAcountName(int accountType) {
		AccountManager am = AccountManager.get(PixApp.getContext()); 
		String accountTypeUrl = getAccountByType(accountType);
		if (accountTypeUrl != null) {
			Account[] accounts = am.getAccountsByType(accountTypeUrl);
			for (Account account : accounts) {
				if (account != null) {
					//Utils.pixLog(TAG, "Got account. name:"+ account.name);
					// return the first account name
					return account.name;
				}
			}
		}
		return null;
	}
	
	public static String[] getAcountsNames(int accountType) {
		String [] accountsNames = null;
		AccountManager am = AccountManager.get(PixApp.getContext()); 
		String accountTypeUrl = getAccountByType(accountType);
		if (accountTypeUrl != null) {
			Account[] accounts = am.getAccountsByType(accountTypeUrl);
			if (accounts != null && accounts.length > 0) {
				accountsNames = new String[accounts.length];
				for (int i=0; i<accounts.length; i++) {
					if (accounts[i] != null) {
						accountsNames[i] = accounts[i].name;
					}
				}
			}
		}
		return accountsNames;
	}
	
	private static String getAccountByType(int accountType) {
		String type = null;
		switch (accountType) {
		case PIX_GOOGLE_ACCOUNT:
			type = "com.google";
			break;
		default:
			break;
		}
		return type;
	}
}
