package com.pixplit.android.globals;

import android.util.SparseArray;

public enum PixCompositionFramesetType {
	
	// === 1 Frame
	Pix1Frameset(100),
	
	// === 2 Frames
	Pix2VerticalFrameset(0),
	Pix2HorizontalFrameset(1),
	
	Pix2ThirdTopFrameset(230),
	Pix2ThirdRightFrameset(231),
	Pix2ThirdBottomFrameset(232),
	Pix2ThirdLeftFrameset(234),
	
	Pix2QuarterTopFrameset(240),
	Pix2QuarterRightFrameset(241),
	Pix2QuarterBottomFrameset(242),
	Pix2QuarterLeftFrameset(243), 
	    
	// === 3 Frames
	
	Pix3HorizontalFrameset(2),
	Pix3VerticalFrameset (301),
	
	Pix3HalfLeftFrameset(3), 
	Pix3HalfTopFrameset(321),
	Pix3HalfRightFrameset(322),
	Pix3HalfBottomFrameset(323),
	
	Pix3QuarterVerticalFrameset(340), 
	Pix3QuarterHorizontalFrameset(341),
	
	Pix3QuarterRightFrameset(342),
	Pix3QuarterLeftFrameset(343), 
	Pix3QuarterTopFrameset(344), 
	Pix3QuarterBottomFrameset(345),
	
	// === 4 Frameset
	
	Pix4Frameset(4),  
	Pix4VerticalFrameset(401), 
	Pix4HorizontalFrameset (402), 
	
	Pix4HalfBottomFrameset(420), 
	Pix4HalfLeftFrameset(421), 
	Pix4HalfTopFrameset(422), 
	Pix4HalfRightFrameset(423),
	
	Pix4ThirdBottomFrameset(430),
	Pix4ThirdLeftFrameset(431), 
	Pix4ThirdTopFrameset(432), 
	Pix4ThirdRightFrameset(433),
	
	Pix4SixthVerticalLeft(460), 
	Pix4SixthHorizontalRight(461),
	Pix4SixthVerticalRight(462), 
	Pix4SixthHorizontalLeft(463), 
	
	Pix4TwoSixthLeftFrameset(464),
	Pix4TwoSixthTopFrameset(465), 
	Pix4TwoSixthRightFrameset(466),
	Pix4TwoSixthBottomFrameset(467);
	
	private final int type;
	
	PixCompositionFramesetType(int type) {
		this.type = type;
	}
	
	private static final SparseArray<PixCompositionFramesetType> map = new SparseArray<PixCompositionFramesetType>();
	
	static {
		for (PixCompositionFramesetType frameType : PixCompositionFramesetType.values()) {
			map.put(frameType.type, frameType);
		}
	}
	
	public static PixCompositionFramesetType getType(int intType) {
		return map.get(intType);
	}
	
	public static int getInt(PixCompositionFramesetType enumType) {
		if (enumType != null) {
			return enumType.type;
		}
		return -1;
	}
	
}
