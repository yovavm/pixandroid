package com.pixplit.android.globals;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.pixplit.android.PixApp;
import com.pixplit.android.ui.ActivitiesControllerInterface;
import com.pixplit.android.util.Utils;
import com.urbanairship.push.PushManager;

public class PixPushNotificationsReceiver extends BroadcastReceiver {

//	private static final String TAG = "PixPushNotificationsReceiver";
	
	// push keys
	private static final String PUSH_KEY_TYPE = "type";
	private static final String PUSH_KEY_COMP_ID = "compId";
	private static final String PUSH_KEY_USERNAME = "username";
	private static final String PUSH_KEY_CONVERSATION_ID = "conversationId";
	private static final String PUSH_KEY_MESSAGE_ID = "messageId";

	// launch intent push keys
	public static final String INTENT_PUSH_KEY_ACTION = PixDefs.PACKAGE_PREFIX+ ".push." + PushManager.ACTION_NOTIFICATION_OPENED;
	public static final String INTENT_PUSH_KEY_TYPE = PixDefs.PACKAGE_PREFIX+ ".push." + PUSH_KEY_TYPE;
	public static final String INTENT_PUSH_KEY_COMP_ID = PixDefs.PACKAGE_PREFIX+ ".push." + PUSH_KEY_COMP_ID;
	public static final String INTENT_PUSH_KEY_USERNAME = PixDefs.PACKAGE_PREFIX+ ".push." + PUSH_KEY_USERNAME;
	public static final String INTENT_PUSH_KEY_CONVERSATION_ID = PixDefs.PACKAGE_PREFIX+ ".push." + PUSH_KEY_CONVERSATION_ID;
	public static final String INTENT_PUSH_KEY_MESSAGE_ID = PixDefs.PACKAGE_PREFIX+ ".push." + PUSH_KEY_MESSAGE_ID;
		
	ActivitiesControllerInterface mActivitiesController = null;
	
	@Override	
	public void onReceive(Context context, Intent intent) {
		this.initActivitiesController();
		//Utils.pixLog(TAG, "Received intent: " + intent.toString());
		String action = intent.getAction();

		if (action.equals(PushManager.ACTION_PUSH_RECEIVED)) {
//			int id = intent.getIntExtra(PushManager.EXTRA_NOTIFICATION_ID, 0);
			
			//Utils.pixLog(TAG, "Received push notification. Alert: " + intent.getStringExtra(PushManager.EXTRA_ALERT) + " [NotificationID="+id+"]");
			
			String type = getPushType(intent);
			if (Utils.isValidString(type) &&
					type.equalsIgnoreCase(PixDefs.PUSH_TYPE_PRIVATE_SPLIT)) {
				/* if the private split relevant activities are running, forward the incoming message 
				 * if they are not running mark the relevant activities for refresh (in case they are paused).
				 */
				handlePush(intent);
			}
		} else if (action.equals(PushManager.ACTION_NOTIFICATION_OPENED)) {
			//Utils.pixLog(TAG, "User clicked notification. Message: " + intent.getStringExtra(PushManager.EXTRA_ALERT));
			
			handlePush(intent);
		} else if (action.equals(PushManager.ACTION_REGISTRATION_FINISHED)) {
			//Utils.pixLog(TAG, "Registration complete. APID:" + intent.getStringExtra(PushManager.EXTRA_APID) + ". Valid: " + intent.getBooleanExtra(PushManager.EXTRA_REGISTRATION_VALID, false));
		}
	}
	
	public static String getPushType(Intent intent) {
		if (intent != null) {
			Bundle extras = intent.getExtras();
			return extras.getString(PUSH_KEY_TYPE);
		}
		return null;
	}
	
	/**
	  * Log the values sent in the payload's "extra" dictionary.
	  *
	  * @param intent A PushManager.ACTION_NOTIFICATION_OPENED or ACTION_PUSH_RECEIVED intent.
	  */
	private void handlePush(Intent intent) {
		this.initActivitiesController();
		if (mActivitiesController == null) return;
		
		if (intent != null) {
			String action = intent.getAction();
			Bundle extras = intent.getExtras();
			String type = getPushType(intent);
			
			if (Utils.isValidString(type)) {
				Intent launch = new Intent();
				launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
				
				if (type.equalsIgnoreCase(PixDefs.PUSH_TYPE_PRIVATE_SPLIT)) {
					Bundle args = new Bundle();
					String conversationId = extras.getString(PUSH_KEY_CONVERSATION_ID);
					if (Utils.isValidString(conversationId)) {
						args.putString(INTENT_PUSH_KEY_CONVERSATION_ID, conversationId);
					}
					String messageId = extras.getString(PUSH_KEY_MESSAGE_ID);
					if (Utils.isValidString(messageId)) {
						args.putString(INTENT_PUSH_KEY_MESSAGE_ID, messageId);
					}
					if (action.equals(PushManager.ACTION_PUSH_RECEIVED)) {
						// mark the inbox for refresh
						mActivitiesController.markForRefresh(null, true);
						// check if the relevant chat is open and active
						if (mActivitiesController.isActivityRunning(conversationId)) {
							// the relevant chat is active
							mActivitiesController.gotoChat(PixApp.getContext(), args, 
									Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						} else {
							// the relevant chat is not open, mark it for refresh in case it is in the back stack
							mActivitiesController.markForRefresh(conversationId, true);
							// check if the inbox is active
							if (mActivitiesController.isActivityRunning(
									ActivitiesControllerInterface.RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT)) {
								// the inbox is active.
								mActivitiesController.gotoInbox(PixApp.getContext(), 
										Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
							}
						}
						
					} else if (action.equals(PushManager.ACTION_NOTIFICATION_OPENED)) {
						mActivitiesController.gotoInbox(PixApp.getContext(), Intent.FLAG_ACTIVITY_NEW_TASK);
					}
				} else {
					String compId = extras.getString(PUSH_KEY_COMP_ID);
					String username = extras.getString(PUSH_KEY_USERNAME);

					if (Utils.isValidString(username)) {
						Bundle args = new Bundle();
						args.putString(PixDefs.FRAG_ARG_USERNAME, username);
						mActivitiesController.gotoUserProfile(PixApp.getContext(), args, Intent.FLAG_ACTIVITY_NEW_TASK);
					} else if (Utils.isValidString(compId)) {
						Bundle args = new Bundle();
						args.putString(PixDefs.FRAG_ARG_COMP_ID, compId);
						mActivitiesController.gotoSingleCompView(PixApp.getContext(), args, Intent.FLAG_ACTIVITY_NEW_TASK);
					}
					
				}
			} else {
	        	// check if it is Facebook incoming intent
	        	Uri target = intent.getData();
	        	if (target != null) {
	        	    // target is the deep-link Uri
	        		//Utils.pixLog(TAG, "----> incoming intent data:"+target);
	        		String path = target.toString();
	        		if (Utils.isValidString(path)) {
	        			String array[] = path.split("\\?");
	        			if (Utils.isValidString(array[0])) {
	        				int urlIndex = array[0].indexOf(PixDefs.pixSplitLandingPageUrlPrefix);
	        				if (urlIndex >= 0) {
	        					int compIdStartIndex = urlIndex + PixDefs.pixSplitLandingPageUrlPrefix.length();
	        					String compId = array[0].substring(compIdStartIndex);
	        					if (Utils.isValidString(compId)) {
	        						//Utils.pixLog(TAG, "----> incoming intent comp ID:"+compId);
	        						Bundle args = new Bundle();
	        						args.putString(PixDefs.FRAG_ARG_COMP_ID, compId);
	        						mActivitiesController.gotoSingleCompView(PixApp.getContext(), args, Intent.FLAG_ACTIVITY_NEW_TASK);
	        					}
	        				}
	        			}
	        		}
	        	}
			}
		}
	}
	
	private void initActivitiesController() {
		try {
	    	mActivitiesController = (ActivitiesControllerInterface)PixApp.getInstance();
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(PixApp.getInstance().toString() +" must implement ActivitiesControllerInterface");
	    }
	}
}