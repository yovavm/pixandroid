package com.pixplit.android.globals;

import java.util.Map;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.localytics.android.LocalyticsSession;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.util.SplitPreviewCompatibleDimensFinder;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.BitmapLruCache;
import com.pixplit.android.volley.PixImagesLoader;

public class PixAppState {
//	private static final String TAG = "PixAppState";
	private static PixAppState state;
	
	public static final int PIX_FONT_REGULAR = 0;
	public static final int PIX_FONT_BOLD = 1;
	
	// screen dimensions
	private static int frameWidth = 0;
	private static int frameHeight = 0;
	
	// split dimensions
	private static int splitPreivewWidth = 0;
	private static int splitPreviewHeight = 0;
		
	// time stamps
	private static String defaultTimestamp;
	public static final int NEWS_TIMESTAMP = 1;
	public static final int ACTIONS_TIMESTAMP = 2;
	public static final int COMPLETED_TIMESTAMP = 3;

	private RequestQueue mVolleyRequestQueue;
	private BitmapLruCache mBitmapCache;
	
	// localytics
	private static LocalyticsSession localyticsSession;

	///////////////// Kudos kit /////////////////
	public static final String KUDOS_APP_ID = "com.pixplit.android";
	// The application key from the developer console
	public static final String KUDOS_APP_KEY = "c749d7bb14";
	// The names of the in-app items associated with your application
	// You should use an empty array if you don't have in-app items to publish
	public static final String[] KUDOS_INAPP_IDS = new String[] {
	    "pix_cat_love_1", "pix_cat_love_2", "pix_cat_love_3"};
	// An array of feature strings; leave empty for now
	public static final String[] KUDOS_FEATURES = new String[] {};
	
	public static PixAppState getState() {
		if (state == null) {
			state = new PixAppState();
			state.init();
		}
		return state;
	}
	
	public RequestQueue getRequestQueue() {
		return mVolleyRequestQueue;
	}
	
	public BitmapLruCache getBitmapCache() {
		return mBitmapCache;
	}
	
	@TargetApi(13)
	@SuppressWarnings("deprecation")
	private void initFrameDimentions() {
		WindowManager wm = (WindowManager) PixApp.getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		if (display != null) {
			if (Utils.hasHoneycombMR2()) {
				Point size = new Point();
				display.getSize(size);
				frameWidth = size.x;
				frameHeight = size.y;
			} else {
				frameWidth = display.getWidth();  // deprecated
				frameHeight = display.getHeight();  // deprecated
			}
			
			if (frameWidth > frameHeight) {
				// replace width and height
				int width = frameWidth;
				frameWidth = frameHeight;
				frameHeight = width;
			}
		}
	}
	
	private void initSplitDimentions() {
		int imgMarging = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comp_img_margin);
		int layoutMarging = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comps_feed_horizontal_margin);
		int widthAvailable = frameWidth - 2*(imgMarging+layoutMarging);
		int splitMaxDisplayWidth = (int)(PixDefs.SPLIT_BITMAP_WIDTH * PixDefs.SPLIT_BITMAP_PREVIEW_SCALE_FACTOR);
		int splitPreferedPreviewDimen = Math.min(widthAvailable, splitMaxDisplayWidth);
		splitPreivewWidth = SplitPreviewCompatibleDimensFinder.getSupportedPreviewDimen(splitPreferedPreviewDimen);
		splitPreviewHeight = splitPreivewWidth;
	}
	
	private void init() {
		initFrameDimentions();
		initSplitDimentions();
		initTimestamp();
		initAppEventsLogging();
		mVolleyRequestQueue = Volley.newRequestQueue(PixApp.getContext());
		mBitmapCache = new BitmapLruCache();
		PixImagesLoader.init();
	}
	
	private void initAppEventsLogging() {
		localyticsSession = new LocalyticsSession(
                PixApp.getContext(),
                PixDefs.localyticsKey);
		localyticsSession.open();
	    localyticsSession.upload();
	}
	
	public static void resumeEventLogging() {
		if (localyticsSession != null) {
			localyticsSession.open();
		    localyticsSession.upload();
		}
	}
	
	public static void pauseEventLogging() {
		if (localyticsSession != null) {
			localyticsSession.close();
	        localyticsSession.upload();
		}
	}
	
	public static void logAppEvent(String screen, String event, Map <String,String> context) {
		if (localyticsSession != null &&
				Utils.isValidString(event) &&
				Utils.isValidString(screen)) {
			if (context != null) {
				localyticsSession.tagEvent(screen+" - "+event, context);
			} else {
				localyticsSession.tagEvent(screen+" - "+event);
			}
		}
	}
	
	private void initTimestamp() {
		defaultTimestamp = "1970-01-01 12:00:00";
	}
		
	public int getFrameWidth() {
		return frameWidth;
	}
	public int getFrameHeight() {
		return frameHeight;
	}
	public int getSplitPreivewWidth() {
		return splitPreivewWidth;
	}
	public int getSplitPreviewHeight() {
		return splitPreviewHeight;
	}
	
	public static void onLogout() {
		setNewNewsNumber(0);
		setTimestamp(NEWS_TIMESTAMP, null);
		setTimestamp(ACTIONS_TIMESTAMP, null);
		setTimestamp(COMPLETED_TIMESTAMP, null);
		
		UserInfo.onLogout();
	}
	
	public static boolean didTipDisplayed(String tipKey){
		return Utils.getSharedPreferencesBoolean(tipKey, false);
	}
	
	public static void setTipDisplayed(String tipKey, boolean displayed) {
		Utils.setSharedPreferencesBoolean(tipKey, displayed);
	}
		
	public static int getNewNewsNumber() {
		return Utils.getSharedPreferencesInt(PixDefs.NEW_NEWS_NUMBER, 0);
	}
	
	@SuppressLint("NewApi")
	public static void setNewNewsNumber(int newNewsNumber) {
		Utils.setSharedPreferencesInt(PixDefs.NEW_NEWS_NUMBER, newNewsNumber);
	}
	
	public static int getNewConversationsNumber() {
		return Utils.getSharedPreferencesInt(PixDefs.NEW_CONVERSATIONS_NUMBER, 0);
	}
	
	@SuppressLint("NewApi")
	public static void setNewConversationsNumber(int newConversationsNumber) {
		Utils.setSharedPreferencesInt(PixDefs.NEW_CONVERSATIONS_NUMBER, newConversationsNumber);
	}
	
	public static String getTimestamp(int timestampType) {
		return Utils.getSharedPreferencesString(getTimestampKey(timestampType), defaultTimestamp);
	}
	
	@SuppressLint("NewApi")
	public static void setTimestamp(int timestampType, String timestamp) {
		Utils.setSharedPreferencesString(getTimestampKey(timestampType), timestamp);
	}
	
	private static String getTimestampKey(int timestampType) {
		String key = null;
		switch(timestampType) {
		case NEWS_TIMESTAMP:
			key = PixDefs.NEWS_TIMESTAMP;
			break;
		case ACTIONS_TIMESTAMP:
			key = PixDefs.ACTION_TIMESTAMP;
			break;
		case COMPLETED_TIMESTAMP:
			key = PixDefs.COMPLETED_TIMESTAMP;
			break;
		default:
			break;
		}
		return key;
	}
	
	public static String getAppVersionNumber() {
		String version = "";
		PackageInfo pInfo;
		try {
			pInfo = PixApp.getContext().getPackageManager().getPackageInfo(PixApp.getContext().getPackageName(), 0);
			version = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}
		
	public static class UserInfo{
		static String adminToken;
		
		public static boolean isUserLogedin(){
			return getUserToken() != null;
		}
		
		public static String getUserToken(){
			return Utils.getSharedPreferencesString(PixDefs.SESSION_TOKEN, null);
		}
		
		public static void setUserToken(String userToken) {
			Utils.setSharedPreferencesString(PixDefs.SESSION_TOKEN, userToken);
		}
		
		public static String getAdminToken(){
			return adminToken;
		}
		
		public static void setAdminToken(String token) {
			adminToken = token;
		}
		
		public static boolean hasAdminToken() {
			return Utils.isValidString(UserInfo.getAdminToken());
		}

		public static String getUserName(){
			return Utils.getSharedPreferencesString(PixDefs.LOGED_IN_USER_NAME, null);
		}
		
		public static void setUserName(String userName) {
			Utils.setSharedPreferencesString(PixDefs.LOGED_IN_USER_NAME, userName);
		}
		
		public static String getUserFullName(){
			return Utils.getSharedPreferencesString(PixDefs.LOGED_IN_USER_FULL_NAME, null);
		}
		
		public static void setUserFullName(String userName) {
			Utils.setSharedPreferencesString(PixDefs.LOGED_IN_USER_FULL_NAME, userName);
		}

		public static String getFbUserName(){
			return Utils.getSharedPreferencesString(PixDefs.FB_USER_NAME, null);
		}
		
		public static void setFbUserName(String fbUserName) {
			Utils.setSharedPreferencesString(PixDefs.FB_USER_NAME, fbUserName);
		}
		
		public static String getFbUserId(){
			return Utils.getSharedPreferencesString(PixDefs.FB_USER_ID, null);
		}
		
		public static void setFbUserId(String fbUserId) {
			Utils.setSharedPreferencesString(PixDefs.FB_USER_ID, fbUserId);
		}
		
		public static void storeFbCredentials(PixUserInfo userInfo) {
			if (userInfo != null) {
				setFbUserName(userInfo.getFullName());
				setFbUserId(userInfo.getId());
			} else {
				setFbUserName(null);
				setFbUserId(null);
			}
		}
		
		public static void clearFbCredentials() {
			setFbUserName(null);
			setFbUserId(null);
		}
		
		public static void clearTwitterCredentials() {
			setTwitterUserName(null);
		}
		
		public static String getInstagramToken(){
			return Utils.getSharedPreferencesString(PixDefs.INSTAGRAM_TOKEN, null);
		}
		
		public static void setInstagramToken(String instaToken) {
			Utils.setSharedPreferencesString(PixDefs.INSTAGRAM_TOKEN, instaToken);
		}
		
		public static String getInstagramPermissions() {
			return Utils.getSharedPreferencesString(PixDefs.INSTAGRAM_PERMISSIONS, null);
		}
		
		public static void setInstagramPermissions(String permissions) {
			Utils.setSharedPreferencesString(PixDefs.INSTAGRAM_PERMISSIONS, permissions);
		}
		
		public static void addInstagramPermissions(String permissions) {
			String currentPerms = getInstagramPermissions();
			if (!Utils.isValidString(currentPerms)) {
				setInstagramPermissions(permissions);
			} else {
				String updatedPerms = currentPerms.concat("+"+permissions);
				setInstagramPermissions(updatedPerms);
			}
		}
		
		public static boolean hasInstagramPermissions(String permissions) {
			String currentPerms = getInstagramPermissions();
			if (!Utils.isValidString(currentPerms)) return false;
			if (!Utils.isValidString(permissions)) return true;
			Pattern p = Pattern.compile("[+]");
			String []newPermissions = TextUtils.split(permissions, p);
			String []currentPermissions = TextUtils.split(currentPerms, p);
			
			for (String neededPermission : newPermissions) {
	        	boolean havePermission = false;
	        	for (String permission : currentPermissions) {
	        		if (permission.equalsIgnoreCase(neededPermission)) {
	        			havePermission = true;
	        			break;
	        		}
	        	}
	        	if (!havePermission) {
	        		return false;
	        	}
	        }
			return true;
		}
		
		public static String getInstagramUsername(){
			return Utils.getSharedPreferencesString(PixDefs.INSTAGRAM_USERNAME, null);
		}
		
		public static void setInstagramUsername(String instaUsername) {
			Utils.setSharedPreferencesString(PixDefs.INSTAGRAM_USERNAME, instaUsername);
		}

		public static String getInstagramFullName(){
			return Utils.getSharedPreferencesString(PixDefs.INSTAGRAM_FULLNAME, null);
		}
		
		public static void setInstagramFullname(String instaFullName) {
			Utils.setSharedPreferencesString(PixDefs.INSTAGRAM_FULLNAME, instaFullName);
		}

		public static void clearInstagramUserInfo() {
			setInstagramToken(null);
			setInstagramFullname(null);
			setInstagramUsername(null);
			setInstagramPermissions(null);
			CookieSyncManager.createInstance(PixApp.getContext());
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.removeAllCookie();
		}
		
		public static boolean getPostLikesToFb() {
			return Utils.getSharedPreferencesBoolean(PixDefs.SETTINGS_POST_LIKES_TO_FB, PixDefs.settingsDefaultPostLikeToFb);
		}
		public static void setPostLikesToFb(boolean post) {
	    	Utils.setSharedPreferencesBoolean(PixDefs.SETTINGS_POST_LIKES_TO_FB, post);
	    }
		
		public static String craetePixToken(String input){
			return Utils.pixHash(PixApp.getContext().getResources().getString(R.string.app_id)+input);
		}
		
		public static String getGPlusUserName(){
			return Utils.getSharedPreferencesString(PixDefs.GPLUS_USER_NAME, null);
		}
		
		public static void setGPlusUserName(String gPlusUserName) {
			Utils.setSharedPreferencesString(PixDefs.GPLUS_USER_NAME, gPlusUserName);
		}
		
		public static String getGPlusUserId(){
			return Utils.getSharedPreferencesString(PixDefs.GPLUS_USER_ID, null);
		}
		
		public static void setGPlusUserId(String gPlusUserId) {
			Utils.setSharedPreferencesString(PixDefs.GPLUS_USER_ID, gPlusUserId);
		}
		
		public static boolean isGPlusLinked() {
			return Utils.isValidString(getGPlusUserId());
		}
		
		public static void storeGPlusCredentials(PixUserInfo userInfo) {
			if (userInfo != null) {
				setGPlusUserName(userInfo.getFullName());
				setGPlusUserId(userInfo.getId());
			} else {
				setGPlusUserName(null);
				setGPlusUserId(null);
			}
		}
		
		public static void clearGPlusCredentials() {
			setGPlusUserName(null);
			setGPlusUserId(null);
		}
		
		public static void onLogout() {
			setUserToken(null);
			setUserName(null);
			setUserFullName(null);
			if (PixApp.inAdminBuild()) {
				setAdminToken(null);
			}
			clearFbCredentials();
			clearGPlusCredentials();
			clearTwitterCredentials();
			clearUserSettings();
			clearInstagramUserInfo();
		}
		
		private static void clearUserSettings() {
			Utils.setSharedPreferencesBoolean(PixDefs.SETTINGS_POST_LIKES_TO_FB, PixDefs.settingsDefaultPostLikeToFb);
		}

		public static String getTwitterUserName() {
			return Utils.getSharedPreferencesString(PixDefs.TWITTER_USER_NAME, "");
		}
		
		public static void setTwitterUserName(String username) {
			Utils.setSharedPreferencesString(PixDefs.TWITTER_USER_NAME, username);
		}

	}
}
