package com.pixplit.android.globals;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.ui.elements.FrameBtnInfo;
import com.pixplit.android.util.Utils;




public class PixDefs {
	// package prefix
	public static final String PACKAGE_PREFIX = "com.pixplit";
	
	// cache parameters
	// assuming that old OS devices has less memory and therefore set the cache sizes smaller
	public static final float SPLIT_IMAGE_CACHE_MEM_PERCENT = Utils.hasHoneycomb() ? 0.07f : 0.05f; 
	public static final String SPLIT_IMAGE_CACHE_DIR = "splits";
	
	public static final float SPLIT_IMAGE_THUMB_CACHE_MEM_PERCENT = Utils.hasHoneycomb() ? 0.06f : 0.05f;
	public static final String SPLIT_IMAGE_THUMB_CACHE_DIR = "splits_thumbs";

	public static final float PROFILE_IMAGE_CACHE_MEM_PERCENT = Utils.hasHoneycomb() ? 0.07f : 0.05f;
	public static final String PROFILE_IMAGE_CACHE_DIR = "profile_thumbs";
	
	public static final float PRIVATE_SPLIT_IMAGE_CACHE_MEM_PERCENT = 0.05f;
    
	public static final int HTTP_CACHE_SIZE = 10 *  1024 *  1024; // 10MB
	public static final String HTTP_CACHE_DIR = "http";
	public static final int IO_BUFFER_SIZE = 8 *  1024;
	
	// start activity for result code types
	public static final int ACTIVITY_RESULT_CODE_PHOTO_PICKER = 1;
	public static final int ACTIVITY_RESULT_CODE_PURCHASE_ITEM = 2;
	public static final int ACTIVITY_RESULT_CODE_FOLLOW_FRIENDS = 3;
	public static final int ACTIVITY_RESULT_CODE_COMP_COMMENTS = 4;
	public static final int ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT = 5;
	public static final int ACTIVITY_RESULT_CODE_INVITE_FOLLOWERS = 6;
	public static final int ACTIVITY_RESULT_CODE_RESET_PASSWORD = 7;
	public static final int ACTIVITY_RESULT_CODE_EDIT_PROFILE = 8;
	public static final int ACTIVITY_RESULT_CODE_GOOGLE_SIGNIN = 9;
	public static final int ACTIVITY_RESULT_CODE_PICK_PHOTO = 10;

	// onActiviyResult intent extra keys
	public static final String INTENT_EXTRA_COMP_COMMENTS_NUM 			= "INTENT_EXTRA_COMP_COMMENTS_NUM";
	public static final String INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS 	= "INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS";
	public static final String INTENT_EXTRA_NEW_SPLIT_COMP_ID 			= "INTENT_EXTRA_NEW_SPLIT_COMP_ID";
	public static final String INTENT_EXTRA_NEW_SPLIT_PARENT_ID 		= "INTENT_EXTRA_NEW_SPLIT_PARENT_ID";
	public static final String INTENT_EXTRA_NEW_SPLIT_CONVERSATION_ID 	= "INTENT_EXTRA_NEW_SPLIT_CONVERSATION_ID";
	public static final String INTENT_EXTRA_INVITE_FOLLOWERS_LIST 		= "INTENT_EXTRA_INVITE_FOLLOWERS_LIST";
	public static final String INTENT_EXTRA_RESET_PASSWORD_SUCCESS 		= "INTENT_EXTRA_IRESET_PASSWORD_SUCCESS";
	public static final String INTENT_EXTRA_PROFILE_WAS_EDITED 			= "INTENT_EXTRA_PROFILE_WAS_EDITED";
	public static final String INTENT_EXTRA_PICKED_PHOTO_URL 			= "INTENT_EXTRA_PICKED_PHOTO_URL";
	
	// Shared preference keys
	public static String PIX_SHARED_PREF = "PIX_SHARED_PREF";
	public static String NEWS_TIMESTAMP = "NEWS_TIMESTAMP";
	public static String ACTION_TIMESTAMP = "ACTION_TIMESTAMP";
	public static String COMPLETED_TIMESTAMP = "COMPLETED_TIMESTAMP";
	public static String NEW_NEWS_NUMBER = "NEW_NEWS_NUMBER";
	public static String NEW_CONVERSATIONS_NUMBER = "NEW_CONVERSATIONS_NUMBER";
	public static String SESSION_TOKEN = "SESSION_TOKEN";
	public static String LOGED_IN_USER_NAME = "LOGED_IN_USER_NAME";	
	public static String LOGED_IN_USER_FULL_NAME = "LOGED_IN_USER_FULL_NAME";
	public static String FB_USER_NAME = "FB_USER_NAME";
	public static String FB_USER_ID = "FB_USER_ID";
	public static String QUICK_GUIDE_DISPLAYED = "QUICK_GUIDE_DISPLAYED";
	public static String PROFILE_TIP_DISPLAYED = "PROFILE_TIP_DISPLAYED";
	public static String MY_FEED_TIP_DISPLAYED = "MY_FEED_TIP_DISPLAYED";
	public static String PLAYGROUND_TIP_DISPLAYED = "PLAYGROUND_TIP_DISPLAYED";
	public static String CHAT_TIP_DISPLAYED = "CHAT_TIP_DISPLAYED";
	public static String TWITTER_TOKEN = "TWITTER_TOKEN";
	public static String TWITTER_TOKEN_SECRET = "TWITTER_TOKEN_SECRET";
	public static String TWITTER_USER_NAME = "TWITTER_USER_NAME";
	public static String USER_PURCHASED_PREFIX = "USER_PURCHASED_";
	public static String PURCHASE_ITEM_PRICE_PREFIX = "PURCHASE_ITEM_PRICE_";
	public static String MYFEED_SELECTED_TAB = "MYFEED_SELECTED_TAB";
	public static String COMP_ACTION_BAR_SCROLLED = "COMP_ACTION_BAR_SCROLLED";
	public static String INSTAGRAM_TOKEN = "INSTAGRAM_TOKEN";
	public static String INSTAGRAM_PERMISSIONS = "INSTAGRAM_PERMISSIONS";
	public static String INSTAGRAM_USERNAME = "INSTAGRAM_USERNAME";
	public static String INSTAGRAM_FULLNAME = "INSTAGRAM_FULLNAME";
	public static String GPLUS_USER_NAME = "GPLUS_USER_NAME";
	public static String GPLUS_USER_ID = "GPLUS_USER_ID";
	public static String WELCOME_NEWS_READ = "WELCOME_NEWS_READ";
	public static String SHARED_SPLIT_TO_INSTAGRAM = "SHARED_SPLIT_TO_INSTAGRAM";
	public static String SHARED_SPLIT_TO_TWITTER = "SHARED_SPLIT_TO_TWITTER";
	public static String SHARED_SPLIT_TO_FACEBOOK = "SHARED_SPLIT_TO_FACEBOOK";
	public static String FOLLOW_ON_INSTAGRAM_DISPLAYED = "FOLLOW_ON_INSTAGRAM_DISPLAYED";
	public static String FOLLOW_ON_TWITTER_DISPLAYED = "FOLLOW_ON_TWITTER_DISPLAYED";
	public static String FOLLOW_ON_FACEBOOK_DISPLAYED = "FOLLOW_ON_FACEBOOK_DISPLAYED";
	
	// Fragments arguments
	public static final String FRAG_ARG_SECURED = "FRAG_ARG_SECURED";
	public static final String FRAG_ARG_URL = "FRAG_ARG_URL";
	public static final String FRAG_ARG_TITLE = "FRAG_ARG_TITLE";
	public static final String FRAG_ARG_COMP_ID = "FRAG_ARG_COMP_ID";
	public static final String FRAG_ARG_USERNAME = "PRAG_ARG_USERNAME";
	public static final String FRAG_ARG_USERS_FRAG_TYPE = "PRAG_ARG_USERS_FRAG_TYPE";
	public static final String FRAG_ARG_USERS_CONTEXT = "PRAG_ARG_USERS_CONTEXT";
	public static final String FRAG_ARG_TAG = "PRAG_ARG_TAG";
	public static final String PRAG_ARG_MYFEED_SEGMENT_TYPE = "PRAG_ARG_MYFEED_SEGMENT_TYPE";
	public static final String FRAG_ARG_FB_FRIENDS_LIST = "FRAG_ARG_FB_FRIENDS_LIST";
	public static final String FRAG_ARG_IN_LOGIN_PROCESS = "FRAG_ARG_IN_LOGIN_PROCESS";
	public static final String FRAG_ARG_INVITEE_LIST = "FRAG_ARG_INVITEE_LIST";
	public static final String FRAG_ARG_COMP_FRAME_TYPE = "FRAG_ARG_COMP_TYPE";
	public static final String FRAG_ARG_CONVERSATION_ID = "FRAG_ARG_CONVERSATION_ID";
	public static final String FRAG_ARG_USERS_ACTION_TYPE_FOLLOW = "FRAG_ARG_USERS_ACTION_TYPE_FOLLOW";
	public static final String FRAG_ARG_USERS_ACTION_TYPE_MSG = "FRAG_ARG_USERS_ACTION_TYPE_MSG";
	public static final String FRAG_ARG_DISPLAY_RECONNECT_MSG = "FRAG_ARG_DISPLAY_RECONNECT_MSG";
	public static final String FRAG_ARG_ANIMATION_TYPE = "FRAG_ARG_ANIMATION_TYPE";
	public static final String FRAG_ARG_SPLIT_PREVIEW_DIMEN = "FRAG_ARG_SPLIT_PREVIEW_DIMEN";
	public static final String FRAG_ARG_PAGE_NUM = "FRAG_ARG_PAGE_NUM";
	public static final String FRAG_ARG_MAIN_MENU_LAUNCH = "FRAG_ARG_MAIN_MENU_LAUNCH";
	public static final String FRAG_ARG_GALLERY_COMP_INDEX = "FRAG_ARG_GALLERY_COMP_INDEX";
	public static final String FRAG_ARG_GALLERY_FEED_URL = "FRAG_ARG_GALLERY_FEED_URL";
	public static final String FRAG_ARG_GALLERY_TITLE = "FRAG_ARG_GALLERY_TITLE";
	public static final String FRAG_ARG_GALLERY_ALLOW_PAGING = "FRAG_ARG_GALLERY_ALLOW_PAGING";
	public static final String FRAG_ARG_GALLERY_NEXT_PAGE = "FRAG_ARG_GALLERY_NEXT_PAGE";
	public static final String FRAG_ARG_GALLERY_FEED_TYPE = "FRAG_ARG_GALLERY_FEED_TYPE";
	public static final String FRAG_ARG_GALLERY_INIT_WITH_COMPS = "FRAG_ARG_GALLERY_INIT_WITH_COMPS";
	public static final String FRAG_ARG_GALLERY_PROFILE_USERNAME = "FRAG_ARG_GALLERY_PROFILE_USERNAME";
	public static final String FRAG_ARG_PROFILE_FEED_TYPE = "FRAG_ARG_PROFILE_FEED_TYPE";
	public static final String FRAG_ARG_ALBUM_ID = "FRAG_ARG_ALBUM_ID";
	
	// Push notifications types
	public static final String PUSH_TYPE_LIKED = "liked";
	public static final String PUSH_TYPE_JOINED = "joined";
	public static final String PUSH_TYPE_COMMENTED = "commented";
	public static final String PUSH_TYPE_INVITED = "invited";
	public static final String PUSH_TYPE_FOLLOWED = "followed";
	public static final String PUSH_TYPE_FOLLOW_REQUESTED = "followRequested";
	public static final String PUSH_TYPE_FOLLOW_REQUEST_APPROVED = "followRequestApproved";
	public static final String PUSH_TYPE_FB_FRIEND_JOINED = "fbFriendJoined";
	public static final String PUSH_TYPE_PRIVATE_SPLIT = "privateSplit";
	
	public static final int TOOLBAR_BTN_ID_REFRESH = 123;
	public static final int TOOLBAR_BTN_ID_CHAT_USER = 124;
	public static final int TOOLBAR_BTN_ID_START_SPLIT = 125;
	public static final int TOOLBAR_BTN_ID_DONE = 126;
	
	// Shared preference settings
	public static String SETTINGS_POST_LIKES_TO_FB = "SETTINGS_POST_LIKES_TO_FB";
	
	// Settings default values
	public static boolean settingsDefaultPostLikeToFb = false;
	
	public static final int notificationTimerStartDelay = 10 * 1000;
	public static final int notificationTimerInterval = 60 * 1000;
	
	public static final boolean recycleBitmaps = (!Utils.hasHoneycomb());
	public static final boolean fetchBitmapsInBg = (Utils.hasHoneycomb());
	public static final boolean hasImageFilters = (Utils.hasIceCreamSandwich());
	
	// split settings
	public static final int splitMaxFramesNum = 4;
	public static final int SPLIT_BITMAP_RELATIVE_BASE_WIDTH = 308;
	public static final int SPLIT_BITMAP_RELATIVE_BASE_HIGHT = 308;
	public static final int SPLIT_BITMAP_WIDTH = SPLIT_BITMAP_RELATIVE_BASE_WIDTH * 2; // 616
	public static final int SPLIT_BITMAP_HIGHT = SPLIT_BITMAP_RELATIVE_BASE_HIGHT * 2; // 616
	public static final int PROFILE_BITMAP_SCALE_WIDTH = 300;
	public static final int PROFILE_BITMAP_SCALE_HIGHT = 300;
	
	public static final int SPLIT_BITMAP_MIN_SCALE_WIDTH = (int) (SPLIT_BITMAP_WIDTH * 1.5);
	public static final int SPLIT_BITMAP_MIN_SCALE_HIGHT = (int) (SPLIT_BITMAP_HIGHT * 1.5);
	
	public static final double SPLIT_BITMAP_PREVIEW_SCALE_FACTOR = 1.7; // scale preview up to 170%
	public static final int SPLIT_MAX_PREVIEW_SCALE = (int)(SPLIT_BITMAP_WIDTH * SPLIT_BITMAP_PREVIEW_SCALE_FACTOR);
	
	public static final int SPLIT_THUMB_SCALE_WIDTH = 300;
	public static final int SPLIT_THUMB_SCALE_HIGHT = 300;
	
	public static final int defaultActionId = -1;
	public static final String defaultCompId = "-1";
	public static final String defaultRootId = "-1";
	public static final String defaultParentId = "-1";
	
	public static final int parsedIntFallback = -1;
	public static final String parsedStringFallback = "";
	
	// Http request context
	public static final String REQ_CTX_PROFILE_SEGMENT =  "REQ_CTX_PROFILE_SEGMENT";
	public static final String REQ_CTX_COMP_LIKED =  "REQ_CTX_COMP_LIKED";
	public static final String REQ_CTX_COMP_ADD_COMMENT =  "REQ_CTX_COMP_COMMENTS";		
	public static final String REQ_CTX_USER_FOLLOW_STATUS =  "REQ_CTX_USER_FOLLOW_STATUS";
	public static final String REQ_CTX_FEED_FETCH_TYPE = "REQ_CTX_FEED_FETCH_TYPE";
	public static final String REQ_CTX_NEW_COMP_TYPE =  "REQ_CTX_NEW_COMP_TYPE";
	public static final String REQ_CTX_COMP_ID =  "REQ_CTX_COMP_ID";
	public static final String REQ_CTX_NEW_COMP_SHARE_BY_EMAIL =  "REQ_CTX_NEW_COMP_SHARE_BY_EMAIL";
	public static final String REQ_CTX_NEW_COMP_PARENT_ID =  "REQ_CTX_NEW_COMP_PARENT_ID";
	public static final String REQ_CTX_NEW_COMP_SHARE_TO_FB =  "REQ_CTX_NEW_COMP_SHARE_TO_FB";
	
	// Http request context options
	public static final int REQ_CTX_NEW_TEMP_COMP = 1;
	public static final int REQ_CTX_NEW_FINAL_COMP = 2;
	public static final int REQ_CTX_NEW_PRIVATE_COMP = 3;
	
	// Facebook API
	public static final String facebookRequestPathShareLike = "me/og.likes";
	public static final String facebookRequestPathGetPhotos = "me/photos";
	public static final String facebookRequestPostNewComp = "me/pixplit:create";
	public static final String facebookPixplitUserId = "334871473233367";
	public static final String facebookPixplitUserUrl = "https://www.facebook.com/pixplit";
	public static final String pixSplitLandingPageUrlPrefix = "http://split.pixplit.com/";
	public static final String pixSplitImageUrlPrefix = "http://pixplit-srv.appspot.com/image/";
	
	// Twitter API
	public static final String twitterCustomerKey = "FzHCBr6oLMdzcOBeKWAohQ";
//	public static final String twitterCustomerSecret = "2nmDsLQO9ptwGC2TFe7QDSmFyEbCGBqoAzqwSeMBvM";
	public static final String twitterPixplitUserId = "552874658";
	public static final String twitterPixplitPageUrl = "https://twitter.com/pixplit";
	
	public static final String facebookPackageName = "com.facebook.katana";
	public static final String twitterPackageName = "com.twitter.android";
	public static final String instagramPackageName = "com.instagram.android";
	
	public static final int defaultColorScheme = R.color.black;
	public static final String defaultColorSchemeString = "000,000,000";
	
	// create split default tags to chose from
	public static String[] defaultChooseTags = {PixApp.getContext().getString(R.string.people), 
		PixApp.getContext().getString(R.string.place), 
		PixApp.getContext().getString(R.string.thing), 
		PixApp.getContext().getString(R.string.me), 
		PixApp.getContext().getString(R.string.nature), 
		PixApp.getContext().getString(R.string.art), 
		PixApp.getContext().getString(R.string.food), 
		PixApp.getContext().getString(R.string.animals)};
	
	public static final String PROFILE_DEFAULT_PHOTO = "default_avatar";
	public static final float PROFILE_COVER_PHOTO_RATIO = 1.48148f;
	
	public static final String PIX_FEEDBACK_EMAIL = "support@pixplit.com";
	
	public static final int MAX_TAGS_TO_DISPLAY = 5;
	
	public static final int[] likeHeartsRes = {
								R.drawable.like_heart_red, 
								R.drawable.like_heart_blue, 
								R.drawable.like_heart_green,
								R.drawable.like_heart_pink, 
								R.drawable.like_heart_yellow};

	public static final  FrameBtnInfo[] defaultSplitFrameBtns = {
		new FrameBtnInfo(R.drawable.frame_2horizontal_btn_selector, PixCompositionFramesetType.Pix2HorizontalFrameset),
		new FrameBtnInfo(R.drawable.frame_2vertical_btn_selector, PixCompositionFramesetType.Pix2VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_2third_right_btn_selector, PixCompositionFramesetType.Pix2ThirdRightFrameset),
		new FrameBtnInfo(R.drawable.frame_3vertical_btn_selector, PixCompositionFramesetType.Pix3VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_3horizontall_btn_selector, PixCompositionFramesetType.Pix3HorizontalFrameset),
		new FrameBtnInfo(R.drawable.frame_3half_left_btn_selector, PixCompositionFramesetType.Pix3HalfLeftFrameset),
		new FrameBtnInfo(R.drawable.frame_4vertical_btn_selector, PixCompositionFramesetType.Pix4VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_4_btn_selector, PixCompositionFramesetType.Pix4Frameset),
		new FrameBtnInfo(R.drawable.frame_4sixth_horizontal_left_btn_selector, PixCompositionFramesetType.Pix4SixthHorizontalLeft)};

	public static final  FrameBtnInfo[] privateSplitFrameBtns = {
		new FrameBtnInfo(R.drawable.frame_1full_btn_selector, PixCompositionFramesetType.Pix1Frameset),
		new FrameBtnInfo(R.drawable.frame_2horizontal_btn_selector, PixCompositionFramesetType.Pix2HorizontalFrameset),
		new FrameBtnInfo(R.drawable.frame_2vertical_btn_selector, PixCompositionFramesetType.Pix2VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_2third_right_btn_selector, PixCompositionFramesetType.Pix2ThirdRightFrameset),
		new FrameBtnInfo(R.drawable.frame_3vertical_btn_selector, PixCompositionFramesetType.Pix3VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_3horizontall_btn_selector, PixCompositionFramesetType.Pix3HorizontalFrameset),
		new FrameBtnInfo(R.drawable.frame_3half_left_btn_selector, PixCompositionFramesetType.Pix3HalfLeftFrameset),
		new FrameBtnInfo(R.drawable.frame_4vertical_btn_selector, PixCompositionFramesetType.Pix4VerticalFrameset),
		new FrameBtnInfo(R.drawable.frame_4_btn_selector, PixCompositionFramesetType.Pix4Frameset),
		new FrameBtnInfo(R.drawable.frame_4sixth_horizontal_left_btn_selector, PixCompositionFramesetType.Pix4SixthHorizontalLeft)};

	// dynamic views Ids
	public static final int emptyFeedViewId = 200;
	
	//Localytics
	public static final String localyticsProductionKey = "826c43a50c55a3491bcc403-3dc73298-a5b8-11e1-3b9c-00ef75f32667";
	public static final String localyticsTestKey = "5c0412f73dd19e72ecb6a3b-dd4f1868-3165-11e2-647b-00ef75f32667";
	public static final String localyticsKey = localyticsProductionKey;
	
	// ========================================= //
	// NEW EVENT TAGS, SCREENS, PARAMS 
	// ========================================= //

	// == PARAMS : Used to provide a well defined context
	// ================================================== //
	public final static String APP_PARAM_TAG_NAME                  = "Tag Name";
	public final static String APP_PARAM_FRAME_NAME                = "Frame Name";
	public final static String APP_PARAM_FILTER_NAME               = "Filter Name";
	public final static String APP_PARAM_CAMERA_SOURCE             = "Cammera Source"; // galley, camera
	public final static String APP_PARAM_WITH_PHOTO                = "With Photo"; // 0,1
	public final static String APP_PARAM_POST_LIKES_TO_FACEBOOK    = "Post Likes to Facebook"; // 0,1
	public final static String APP_PARAM_BUBBLE_OPENED             = "Bubble opened"; // 0,1
	public final static String APP_PARAM_USED_GEO_LOCATION         = "Used Geo-Location";// 0,1
	public final static String APP_PARAM_SHARED_WITH_FACEBOOK      = "Shared with Facebook"; // 0,1
	public final static String APP_PARAM_SHARED_WITH_TWITTER       = "Shared with Twitter"; // 0,1
	public final static String APP_PARAM_SHARED_BY_MAIL            = "Shared by Mail"; // 0,1
	public final static String APP_PARAM_SHARED_PRESSED_ANDROID_ONLY = "Share pressed (Android Only)";
	public final static String APP_PARAM_EXPORT_TO_INSTAGRAM_PRESSED     = "Export to Instagram pressed";
	public final static String APP_PARAM_COVER_PHOTO_ID            = "Cover Photo id";
	public final static String APP_PARMA_USERS_COUNT               = "Users count";

	public final static String APP_PARAM_CAMERA_SOURCE_GALLERY     = "Gallery";
	public final static String APP_PARAM_CAMERA_SOURCE_CAMERA      = "Camera";

	public final static String APP_PARAM_RATE_SHOW_RICH_VIEW       = "Show rich view";
	public final static String APP_PARAM_RATE_USE_COUNT            = "Uses count";
	public final static String APP_PARAM_RATE_EVENTS_COUNT         = "Significant events count";
	public final static String APP_PARAM_MSG_TYPE                  = "Message Type";
	
	public final static String APP_PARAM_MSG_TYPE_TEXT             = "text";
	public final static String APP_PARAM_MSG_TYPE_SPLIT            = "split";

	// == SCREENS: Used to provide name of screen where event is being tagged
	// ====================================================================== //

	public final static String APP_SCREEN_QUICK_START              = "Quick Start";
	public final static String APP_SCREEN_ANY_FEED                 = "Any Feed";
	public final static String APP_SCREEN_CONNECT_MENU             = "Connect Menu";
	public final static String APP_SCREEN_LOGIN                    = "Login";
	public final static String APP_SCREEN_FORGOT_PASSWORD          = "Forgot Password";
	public final static String APP_SCREEN_SIGN_UP                  = "Sign Up";
	public final static String APP_SCREEN_MAIN_MENU                = "Main Menu";
	public final static String APP_SCREEN_SEARCH_USERS             = "Search Users";
	public final static String APP_SCREEN_NEWS                     = "News";
	public final static String APP_SCREEN_FEATURED                 = "Featured";
	public final static String APP_SCREEN_PROFILE                  = "Profile";
	public final static String APP_SCREEN_SETTINGS                 = "Settings";
	public final static String APP_SCREEN_SETTINGS_FACEBOOK        = "Settings-Facebook";
	public final static String APP_SCREEN_SETTINGS_INSTAGRAM       = "Settings-Instagram";
	public final static String APP_SCREEN_SETTINGS_GPLUS           = "Settings-Google-Plus";
	public final static String APP_SCREEN_SETTINGS_TWITTER         = "Settings-Twitter";
	public final static String APP_SCREEN_SETTINGS_INFO            = "Settings-Info";
	public final static String APP_SCREEN_EDIT_PROFILE             = "Edit Profile";
	public final static String APP_SCREEN_SPLIT_ACTIONS            = "Split Actions";
	public final static String APP_SCREEN_SPLIT_OPTIONS            = "Split Options";
	public final static String APP_SCREEN_SEARCH_BY_TAG            = "Search by Tag";
	public final static String APP_SCREEN_SPLIT_COMMENTS           = "Split Comments";
	public final static String APP_SCREEN_SPLIT_STARTED            = "Split Started";
	public final static String APP_SCREEN_SPLIT_CUSTOMIZATION      = "Split Customization";
	public final static String APP_SCREEN_SPLIT_CREATION           = "Split Creation";
	public final static String APP_SCREEN_MY_FEED                  = "My Feed";
	public final static String APP_SCREEN_NEW_VERSION_ALERT        = "New Version Alert";
	public final static String APP_SCREEN_USERS_LIST               = "Users List";
	public final static String APP_SCREEN_FACEBOOK_TEASER          = "Find Friends Teaser";
	public final static String APP_SCREEN_FIND_FRIENDS             = "Find Friends";
	public final static String APP_SCREEN_INVITE_FB_FRIENDS        = "Invite FB Friends";
	public final static String APP_SCREEN_RATE_US_POPUP            = "Rate Us Popup";
	public final static String APP_SCREEN_PRIVATE_CHAT             = "Private Chat";
	public final static String APP_SCREEN_SEARCH_IMAGE             = "Search Image";

	// == EVENTS: Used to provide name of event which is being tagged
	// ============================================================== //

	// App Events
	public final static String APP_EVENT_GET_STARTED_PRESSES           = "Get Started pressed";
	public final static String APP_EVENT_CONNECT_WITH_FACEBOOK_PRESSED = "Connect with Facebook pressed";
	public final static String APP_EVENT_SUCCESSFUL_FACEBOOK_CONNECT   = "Successful Facebook Connect";
	public final static String APP_EVENT_CONNECT_WITH_GPLUS_PRESSED    = "Connect with Google pressed";
	public final static String APP_EVENT_SUCCESSFUL_GPLUS_CONNECT      = "Successful Google Connect";
	public final static String APP_EVENT_CONNECT_WITH_EMAIL_PRESSED    = "Connect with your Email pressed";
	public final static String APP_EVENT_SIGN_UP_PRESSED               = "Sign Up pressed";
	public final static String APP_EVENT_LOGIN_PRESSED                 = "Login pressed";
	public final static String APP_EVENT_SIGN_IN_PRESSED               = "Sign In pressed";
	public final static String APP_EVENT_SUCCESSFUL_LOGIN              = "Successful Login";
	public final static String APP_EVENT_FORGOT_PASSWORD_PRESSED       = "Forgot Password pressed";
	public final static String APP_EVENT_SEND_PRESSED                  = "Send pressed";
	public final static String APP_EVENT_DONE_PRESSED                  = "Done pressed";
	public final static String APP_EVENT_ACCOUNT_CREATED               = "Account Created";
	public final static String APP_EVENT_EUREKA_ACCOUNT_CREATED        = "Eureka Account Created";
	public final static String APP_EVENT_CANCEL_PRESSED                = "Cancel pressed";
	public final static String APP_EVENT_MY_FEED_PRESSED               = "My Feed pressed";
	public final static String APP_EVENT_PROFILE_PRESSED               = "Profile pressed";
	public final static String APP_EVENT_NEW_SPLIT_PRESSED             = "New Split pressed";
	public final static String APP_EVENT_NEWS_PRESSED                  = "News pressed";
	public final static String APP_EVENT_POPULAR_PRESSED               = "Popular pressed";
	public final static String APP_EVENT_FEATURED_PRESSED              = "Featured pressed";
	public final static String APP_EVENT_INBOX_PRESSED                 = "Inbox pressed";
	public final static String APP_EVENT_SETTINGS_PRESSED              = "Settings pressed";
	public final static String APP_EVENT_INVITE_FB_FRIENDS_PRESSED     = "Invite Friends pressed";
	public final static String APP_EVENT_SEARCH_USERS_PRESSED          = "Search Users pressed";
	public final static String APP_EVENT_SEARCH_PRESSED                = "Search pressed";
	public final static String APP_EVENT_FB_FRIENDS_FOLLOWED           = "Follow Users button pressed";
	public final static String APP_EVENT_FB_FRIENDS_INVITED            = "Invite button pressed";
	public final static String APP_EVENT_IMPORT_INSTAGRAM_PHOTO_PRESSED = "Import From Instagram pressed";
	public final static String APP_EVENT_IMPORT_FACEBOOK_PHOTO_PRESSED = "Import From Facebook pressed";
	public final static String APP_EVENT_IMPORT_GOOGLE_PHOTO_PRESSED   = "Import From Google pressed";
	public final static String APP_EVENT_SUCCESSFUL_INSTAGRAM_IMPORT   = "Successful Import From Instagram";
	public final static String APP_EVENT_SUCCESSFUL_FACEBOOK_IMPORT    = "Successful Import From Facebook";
	public final static String APP_EVENT_SUCCESSFUL_GOOGLE_IMPORT      = "Successful Import From Google";
	public final static String APP_EVENT_FOLLOW_ON_INSTAGRAM_PRESSED   = "Follow on Instagram pressed";
	public final static String APP_EVENT_FOLLOW_ON_TWITTER_PRESSED     = "Follow on Twitter pressed";
	public final static String APP_EVENT_FOLLOW_ON_FACEBOOK_PRESSED    = "Follow on Facebook pressed";
	public final static String APP_EVENT_SUCCESSFUL_FOLLOW_ON_INSTAGRAM= "Successful Follow on Instagram";
	
	public final static String APP_EVENT_NEWS_ITEM_SPLIT_PRESSED       = "News Item Split pressed";
	public final static String APP_EVENT_NEWS_ITEM_PROFILE_PRESSED     = "News Item Profile pressed";
	public final static String APP_EVENT_VIEW_LISTING_PRESSED          = "View Listing pressed";
	public final static String APP_EVENT_COMPLETED_TAB_PRESSED         = "Completed tab pressed";
	public final static String APP_EVENT_NONCOMPLETED_TAB_PRESSED      = "Non Completed tab pressed";
	public final static String APP_EVENT_LIKED_TAB_PRESSED             = "Liked tab pressed";
	public final static String APP_EVENT_CHANGED_COVER_PHOTO           = "Changed cover photo";
	public final static String APP_EVENT_FOLLOW_BUTTON_PRESSED         = "Follow button pressed";
	public final static String APP_EVENT_FOLLOWING_BUTTON_PRESSED      = "Unfollow button pressed";
	public final static String APP_EVENT_FOLLOWING_COUNTER_PRESSED     = "Following counter pressed";
	public final static String APP_EVENT_FOLLOWERS_COUNTER_PRESSED     = "Followers counter pressed";
	
	public final static String APP_EVENT_FIND_FACEBOOK_FRIENDS_PRESSED = "Find Facebook Friends button pressed";
	public final static String APP_EVENT_SKIP_FACEBOOK_FRIENDS_PRESSED = "Skip button pressed";
	public final static String APP_EVENT_PUSH_NOTIFICATIONS_PRESSED    = "Push Notifications pressed";
	public final static String APP_EVENT_EDIT_PROFILE_PRESSED          = "Edit Profile pressed";
	public final static String APP_EVENT_LOGOUT_PRESSED                = "Logout pressed";
	public final static String APP_EVENT_FACEBOOK_PRESSED              = "Facebook pressed";
	public final static String APP_EVENT_INSTAGRAM_PRESSED             = "Instagram pressed";
	public final static String APP_EVENT_GOOGLE_PRESSED                = "Google pressed";
	public final static String APP_EVENT_TWITTER_PRESSED               = "Twitter pressed";
	public final static String APP_EVENT_POST_LIKES_TO_FB_PRESSED      = "Post Likes to Facebook pressed";
	public final static String APP_EVENT_FACEBOOK_LINKED               = "Facebook linked";
	public final static String APP_EVENT_UNLINK_FACEBOOK_PRESSED       = "Unlink Facebook";
	public final static String APP_EVENT_UNLINK_INSTAGRAM_PRESSED      = "Unlink Instagram";
	public final static String APP_EVENT_UNLINK_GPLUS_PRESSED          = "Unlink Google Plus";
	public final static String APP_EVENT_TWITTER_LINKED                = "Twitter linked";
	public final static String APP_EVENT_UNLINK_TWITTER_PRESSED        = "Unlink Twitter";
	public final static String APP_EVENT_TELL_A_FRIEND_PRESSED         = "Tell a Friend pressed";
	public final static String APP_EVENT_RATE_ON_APPSTORE_PRESSED      = "Rate Now button pressed";
	public final static String APP_EVENT_DONT_RATE_ON_APPSTORE_PRESSED = "No button pressed";
	public final static String APP_EVENT_REMIND_TO_RATE_PRESSED        = "Remind Later button pressed";
	public final static String APP_EVENT_SHOW_RATING_SCREEN            = "Show rate us screen by Main Window";
	public final static String APP_EVENT_SEND_FEEDBACK_PRESSED         = "Send Feedback pressed";
	public final static String APP_EVENT_INFO_PRESSED                  = "Info pressed";
	public final static String APP_EVENT_ABOUT_PIXPLIT_PRESSED         = "About Pixplit pressed";
	public final static String APP_EVENT_PRIVACY_POLICY_PRESSED        = "Privacy Policy pressed";
	public final static String APP_EVENT_TERMS_OF_SERVICE_PRESSED      = "Terms of Service pressed";
	public final static String APP_EVENT_LIBRARIES_PRESSED             = "Libraries pressed";
	
	public final static String APP_EVENT_SAVE_PRESSED                  = "Save pressed";
//	public final static String APP_EVENT_CANCEL_PRESSED                = "Cancel pressed";
	public final static String APP_EVENT_JOINED_OPEN_SPLIT_PRESSED     = "Joined an open Split pressed";
	public final static String APP_EVENT_LIKE_PRESSED                  = "Like pressed";
	public final static String APP_EVENT_DISLIKE_PRESSED               = "Dislike pressed";
	public final static String APP_EVENT_LIKE_DOUBLE_TAP_PRESSED       = "Like double tap pressed";
	public final static String APP_EVENT_COMMENTS_PRESSED              = "Comments pressed";
	public final static String APP_EVENT_RELATED_PRESSED               = "Related pressed";
	public final static String APP_EVENT_PARTICIPANT_AVATAR_PRESSED    = "Participant avatar pressed";
	public final static String APP_EVENT_PARTICIPANT_BUBBLE_PRESSED    = "Participant bubble pressed";
	public final static String APP_EVENT_LIKERS_LIST_PRESSED           = "Likers list pressed";
	
	public final static String APP_EVENT_SHARE_TO_FACEBOOK_PRESSED     = "Share to Facebook pressed";
	public final static String APP_EVENT_SHARE_TO_TWITTER_PRESSED      = "Share to Twitter pressed";
	public final static String APP_EVENT_SHARE_BY_EMAIL_PRESSED        = "Share by Email pressed";
	public final static String APP_EVENT_SAVE_TO_LIBRARY_PRESSED       = "Save to Library pressed";
	public final static String APP_EVENT_RESPLIT_PRESSED               = "Resplit pressed";
	public final static String APP_EVENT_DELETE_SPLIT_PRESSED          = "Delete Split pressed";
	public final static String APP_EVENT_FLAG_SPLIT_PRESSED            = "Flag Split pressed";
	
	public final static String APP_EVENT_SEARCH_DONE                   = "Search done";
	public final static String APP_EVENT_COMMENT_ADDED                 = "Comment added";
	public final static String APP_EVENT_COMMENT_DELETED               = "Comment deleted";
	public final static String APP_EVENT_FRAME_SELECTED                = "Frame Selected";
	public final static String APP_EVENT_PHOTO_ADDED                   = "Photo Added";
	public final static String APP_EVENT_FILTER_APPLIED                = "Filter applied";
	public final static String APP_EVENT_BORDER_APPLIED                = "Border applied";
	public final static String APP_EVENT_SPLIT_COMPLETED               = "Split Completed";
	public final static String APP_EVENT_SPLIT_CREATION_CANCELED       = "Split Creation Canceled";
	
	public final static String APP_EVENT_COMPLETED_BUTTON_PRESSED      = "Completed button pressed";
	public final static String APP_EVENT_PLAYGROUND_BUTTON_PRESSED     = "Playground button pressed";
	public final static String APP_EVENT_USER_AGREED_TO_UPDATE         = "User agreed to update";
	public final static String APP_EVENT_USER_DECLINED_TO_UPDATE       = "User declined to update";
	public final static String APP_EVENT_SPLIT_IMPRESSION              = "Split Impression";
	
	public final static String APP_EVENT_MORE_FRAMES_PRESSED           = "More Frames pressed";
	public final static String APP_EVENT_MORE_FRAMES_BUY_NOW_PRESSED   = "More Frames - Buy Now pressed";
	
	public final static String APP_EVENT_MSG_INITIATED_FROM_PROFILE    = "Message initiated from profile";
	public final static String APP_EVENT_MSG_INITIATED_FROM_SEARCH     = "Message initiated from search";
	public final static String APP_EVENT_MSG_SENT                      = "Message Sent";
	public final static String APP_EVENT_HIDE_CONVERSATION             = "Hide Conversation";
	public final static String APP_EVENT_DELETE_PRIVATE_MSG            = "Delete Private Message";

	 
	
}
