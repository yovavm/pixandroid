package com.pixplit.android.globals;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.util.Utils;

public class PixLocationServices {
	private static LocationManager mLocationManager;
	private static Location mLastLocation = null;
	
	private static final int ONE_SECOND = 1000;
    
    // enable/disable location services
	public static boolean enableLocationServices(final Context context, boolean enable) {
		if (enable && context != null) {
			// Get a reference to the LocationManager object.
	        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	        if (mLocationManager != null) {
		        final boolean gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	
		        if (!gpsEnabled) {
		            // Build an alert dialog here that requests that the user enable
		            // the location services, then when the user clicks the "OK" button,
		            // call enableLocationSettings()
		            new PixConfirmationDialog(context, new ConfirmationDialogDelegate() {
						public void onConfirm() {
							enableLocationSettings(context);
						}
						public void onDecline() {}
					},
					PixApp.getContext().getResources().getString(R.string.location_services_disabled),
					PixApp.getContext().getResources().getString(R.string.you_currently_have_all_location_services_for_this_device_disabled), 
					PixApp.getContext().getResources().getString(R.string.enable), null).show();
		            return false;
		        }
		        
		        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
		        	try {
			            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, ONE_SECOND, 0, listener);
			            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, ONE_SECOND, 0, listener);
		        	} catch (Exception e) {
		        		// may fail on some devices in case the requested service is missing
		        		//Utils.pixLog("PixLocationServices", "requestLocationUpdates failed:"+e);
		        		e.printStackTrace();
		        	}
		            return true;
		        } else {
		        	Utils.showMessage(PixApp.getContext().getResources().getString(R.string.location_services_disabled), true);
		        }
	        }
		} else {
			if (mLocationManager != null) {
				mLocationManager.removeUpdates(listener);
			}
			mLastLocation = null;
		}
		return false;
	}
	
	// get current location
	public static String getLocation() {
		if (mLastLocation != null) {
			return String.format("%f,%f", mLastLocation.getLatitude(), mLastLocation.getLongitude());
		}
		return null;
	}
    
    private static final LocationListener listener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            // A new location update is received. 
        	mLastLocation = location;
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        	
        }
    };
    
    // Method to launch Settings
    private static void enableLocationSettings(Context context) {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(settingsIntent);
    }
}
