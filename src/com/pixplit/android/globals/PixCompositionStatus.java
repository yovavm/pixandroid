package com.pixplit.android.globals;

import android.util.SparseArray;

public enum PixCompositionStatus {
		PixStatusNew (1),
	    PixStatusOpen(2),
	    PixStatusDone(3);
		
		private final int status;
		PixCompositionStatus(int status) {
			this.status = status;
		}
		private static final SparseArray<PixCompositionStatus> map = new SparseArray<PixCompositionStatus>();
		
		static {
			for (PixCompositionStatus compStatus : PixCompositionStatus.values()) {
				map.put(compStatus.status, compStatus);
			}
		}
		
		public static PixCompositionStatus getStatus(int intStatus) {
			return map.get(intStatus);
		}
		
		public static int getInt(PixCompositionStatus enumStatus) {
			if (enumStatus != null) {
				return enumStatus.status;
			}
			return -1;
		}
	}