package com.pixplit.android.globals;

import android.os.Bundle;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.util.Utils;

public class PixCompositionType{
//  	private static final String TAG = "PixCompositionType";
	
	private PixCompositionFramesetType framesetType;
	private PixCompositionComponent[] components;
	private int maxComponentsForType;
	int numOfExistingComponents;
	private PixCompositionStatus compositionStatus;
	
	private PixCompositionType() {}  // empty constructor for serialization purpose
	
	public PixCompositionType(PixCompositionFramesetType framesetType, PixComposition comp) {
		this.framesetType = framesetType;
		this.compositionStatus = PixCompositionStatus.PixStatusNew;
		this.numOfExistingComponents = 0;
		initComponents();
		if (this.components != null) {
			this.maxComponentsForType = this.components.length;
		} else {
			this.maxComponentsForType = 0;
		}
		initWithComp(comp);
	}
	
	private static final String PIX_COMP_TYPE_KEY_PIX_COMP_TYPE = "PIX_COMP_TYPE_KEY_PIX_COMP_TYPE";
   	private static final String PIX_COMP_TYPE_KEY_FRAMESET_TYPE = "PIX_COMP_TYPE_KEY_FRAMESET_TYPE";
   	private static final String PIX_COMP_TYPE_KEY_MAX_COMPONENT = "PIX_COMP_TYPE_KEY_MAX_COMPONENT";
   	private static final String PIX_COMP_TYPE_KEY_EXIST_COMPONENT_NUM = "PIX_COMP_TYPE_KEY_EXIST_COMPONENT_NUM";
   	private static final String PIX_COMP_TYPE_KEY_STATUS = "PIX_COMP_TYPE_KEY_STATUS";
   	
   	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(PIX_COMP_TYPE_KEY_PIX_COMP_TYPE, true);
   		if (this.framesetType != null) {
   			outBundle.putInt(PIX_COMP_TYPE_KEY_FRAMESET_TYPE, PixCompositionFramesetType.getInt(this.framesetType));
   		}
   		outBundle.putInt(PIX_COMP_TYPE_KEY_MAX_COMPONENT, this.maxComponentsForType);
   		outBundle.putInt(PIX_COMP_TYPE_KEY_EXIST_COMPONENT_NUM, this.numOfExistingComponents);
   		if (this.components != null) {
   			for (int i=0; i < this.components.length; i++) {
   				if (this.components[i] != null) {
   					this.components[i].serialize(outBundle, String.format("COMPONENT_%s", i));
   				}
   			}
   		}
   		if (this.compositionStatus != null) {
   			outBundle.putInt(PIX_COMP_TYPE_KEY_STATUS, PixCompositionStatus.getInt(this.compositionStatus));
   		}
   		
   		
   	}
   	public static boolean hasCompositionType(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(PIX_COMP_TYPE_KEY_PIX_COMP_TYPE);
   	}
   	
   	public static PixCompositionType deserialize(Bundle inBundle) {
   		if (!hasCompositionType(inBundle)) return null;
   		PixCompositionType compositionType = new PixCompositionType();
   		compositionType.framesetType = PixCompositionFramesetType.getType
   				(inBundle.getInt(PIX_COMP_TYPE_KEY_FRAMESET_TYPE));
   		compositionType.maxComponentsForType = inBundle.getInt(PIX_COMP_TYPE_KEY_MAX_COMPONENT);
   		compositionType.numOfExistingComponents = inBundle.getInt(PIX_COMP_TYPE_KEY_EXIST_COMPONENT_NUM);
   		
   		if (compositionType.maxComponentsForType > 0) {
   			compositionType.components = new PixCompositionComponent[compositionType.maxComponentsForType];
	   		for (int i=0; i < compositionType.maxComponentsForType; i++) {
	   			compositionType.components[i] = PixCompositionComponent.deserialize(inBundle, String.format("COMPONENT_%s", i));
	   		}
   		}
   		compositionType.compositionStatus = PixCompositionStatus.getStatus(inBundle.getInt(PIX_COMP_TYPE_KEY_STATUS));
   		return compositionType;
   	}
   	
	public int getMaxComponentsForType() {
		return this.maxComponentsForType;
	}
	
	public int getNumOfExistingComponents() {
		return numOfExistingComponents;
	}
	
	public PixCompositionStatus getState() {
		return compositionStatus;
	}
	
	public PixCompositionFramesetType getCompFramesetType() {
		return this.framesetType;
	}
	
	public PixCompositionComponent getComponentWithIndex(int index) {
		if (this.components != null &&
				index > 0 && 
				index <= this.maxComponentsForType) {
			return this.components[index-1];
		}
		return null;
	}
	
	private void initComponents() {
		if (framesetType == null) {
			//Utils.pixLog(TAG, "in init components. no framesetType. aborting!");
			return;
		}
		
		switch (this.framesetType) {
		case Pix1Frameset:
			components = new PixCompositionComponent[1];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 308), 1);
			break;
		case Pix2VerticalFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 308), 2);
			break;
		case Pix2HorizontalFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 154), 2);
			break;
		case Pix2QuarterBottomFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 231), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 231, 308, 77), 2);
			break;
		case Pix2QuarterLeftFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 77, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(77, 0, 231, 308), 2);
			break;
		case Pix2QuarterRightFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 231, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(231, 0, 77, 308), 2);
			break;
		case Pix2QuarterTopFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 77), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 77, 308, 231), 2);
			break;
		case Pix2ThirdBottomFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 206), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 206, 308, 102), 2);
			break;
		case Pix2ThirdLeftFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 102, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(102, 0, 206, 308), 2);
			break;
		case Pix2ThirdRightFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 206, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(206, 0, 102, 308), 2);
			break;
		case Pix2ThirdTopFrameset:
			components = new PixCompositionComponent[2];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 102), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 102, 308, 206), 2);
			break;
		case Pix3HalfBottomFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 154), 3);
			break;
		case Pix3HalfLeftFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 154, 154, 154), 3);
			break;
		case Pix3HalfRightFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 154, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 308), 3);
			break;
		case Pix3HalfTopFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 154, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 154, 154, 154), 3);
			break;
		case Pix3HorizontalFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 308, 102), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 308, 103), 3);
			break;
		case Pix3QuarterBottomFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 77), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 231, 308, 77), 3);
			break;
		case Pix3QuarterHorizontalFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 77), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 77, 308, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 231, 308, 77), 3);
			break;
		case Pix3QuarterLeftFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 77, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(77, 0, 77, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 308), 3);
			break;
		case Pix3QuarterRightFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 77, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(231, 0, 77, 308), 3);
			break;
		case Pix3QuarterTopFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 77), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 77, 308, 77), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 154), 3);
			break;
		case Pix3QuarterVerticalFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 77, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(77, 0, 154, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(231, 0, 77, 308), 3);
			break;
		case Pix3VerticalFrameset:
			components = new PixCompositionComponent[3];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 102, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 308), 3);
			break;
		case Pix4Frameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 154, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 154, 154, 154), 4);
			break;
		case Pix4HalfBottomFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 102, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 154), 4);
			break;
		case Pix4HalfLeftFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 103), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 103, 154, 102), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 205, 154, 103), 4);
			break;
		case Pix4HalfRightFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 154, 102), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 154, 103), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 308), 4);
			break;
		case Pix4HalfTopFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 103, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 154, 102, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 154, 103, 154), 4);
			break;
		case Pix4HorizontalFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 77), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 77, 308, 77), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 308, 77), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 231, 308, 77), 4);
			break;
		case Pix4SixthHorizontalLeft:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 205), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 154, 205), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 205, 154, 103), 4);
			break;
		case Pix4SixthHorizontalRight:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 205), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 103), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 154, 103), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 103, 154, 205), 4);
			break;
		case Pix4SixthVerticalLeft:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 205, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 205, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 154, 103, 154), 4);
			break;
		case Pix4SixthVerticalRight:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 205, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 103, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 154, 205, 154), 4);
			break;
		case Pix4ThirdBottomFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 154, 102), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 205), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 308, 103), 4);
			break;
		case Pix4ThirdLeftFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 102, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 154, 205, 154), 4);
			break;
		case Pix4ThirdRightFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 205, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 103, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 154, 102, 154), 4);
			break;
		case Pix4ThirdTopFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 154, 205), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 103, 154, 102), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 205, 154, 103), 4);
			break;
		case Pix4TwoSixthBottomFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 308, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 308, 102), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 154, 103), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 205, 154, 103), 4);
			break;
		case Pix4TwoSixthLeftFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 154), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 154, 103, 154), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 102, 308), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 308), 4);
			break;
		case Pix4TwoSixthRightFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 103, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(103, 0, 102, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 0, 103, 154), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(205, 154, 103, 154), 4);
			break;
		case Pix4TwoSixthTopFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 154, 103), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 154, 103), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 103, 308, 102), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 205, 308, 103), 4);
			break;
		case Pix4VerticalFrameset:
			components = new PixCompositionComponent[4];
			components[0] = new PixCompositionComponent(Utils.createRectFromDimentions(0, 0, 77, 308), 1);
			components[1] = new PixCompositionComponent(Utils.createRectFromDimentions(77, 0, 77, 308), 2);
			components[2] = new PixCompositionComponent(Utils.createRectFromDimentions(154, 0, 77, 308), 3);
			components[3] = new PixCompositionComponent(Utils.createRectFromDimentions(231, 0, 77, 308), 4);
			break;
		default:
			break;
		
		}
	}
	
	private void initWithComp(PixComposition comp) {
		if (comp != null) {
			if (comp.getParticipantsCount() > 0) {
				this.numOfExistingComponents = comp.getParticipantsCount();
				// verification
				if (this.numOfExistingComponents < 1 || this.numOfExistingComponents >= this.maxComponentsForType) {
					this.compositionStatus = PixCompositionStatus.PixStatusDone;
					//Utils.pixLog(TAG, "invalid number of participants:"+ this.numOfExistingComponents+" where max allowed:"+ this.maxComponentsForType);
				}
			} else {
				this.compositionStatus = PixCompositionStatus.PixStatusDone;
				//Utils.pixLog(TAG, "no participant count on composition");
			}
			
			if (this.compositionStatus != PixCompositionStatus.PixStatusDone) {
				
				this.compositionStatus = PixCompositionStatus.PixStatusOpen;
				
				int validParticipantsNum = 0;
				if (comp.getParticipants() != null) {
					for (PixParticipant participant : comp.getParticipants()) {
						int position = participant.getIndex() - 1;
						if (position >= 0 && position < this.maxComponentsForType) {
							if (this.components[position] != null) {
								this.components[position].setParticipant(participant);
								validParticipantsNum++;
							}
						}
					}
				}
				
				if (validParticipantsNum != this.numOfExistingComponents) {
					//Utils.pixLog(TAG, "There are "+ (this.numOfExistingComponents-validParticipantsNum)+" invalid participants");
					this.numOfExistingComponents = validParticipantsNum;
				}
			}
		} 
	}
}
