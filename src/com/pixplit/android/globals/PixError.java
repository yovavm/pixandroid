package com.pixplit.android.globals;

public class PixError extends Error {
	public static final int PIX_SERVER_ERROR_MISSING_PARAMETER = 1001;
	public static final int PIX_SERVER_ERROR_USER_DOESNOT_EXIST = 1002;
	public static final int PIX_SERVER_ERROR_NOT_AUTHENTICATED = 1005;
	public static final int PIX_SERVER_ERROR_COMPOSITION_NOT_EXIST = 1011;
	public static final int PIX_SERVER_ERROR_INVALID_CLIENT = 1100;
	public static final int PIX_SERVER_ERROR_INVALID_CREDENTIALS = 1101;
	public static final int PIX_SERVER_ERROR_INVALID_EMAIL = 1102;
	public static final int PIX_SERVER_ERROR_EMAIL_EXIST = 1103;
	public static final int PIX_SERVER_ERROR_EMAIL_DOESNOT_EXIST = 1104;
	public static final int PIX_SERVER_ERROR_USERNAME_EXIST = 1105;
	public static final int PIX_SERVER_ERROR_ADMIN_NOT_AUTHENTICATED = 1205;
	
	
	public static final int PixErrorCodeInvalidRequest = 2001;
	public static final int PixErrorCodeInvalidRootObject = 2002;
	public static final int PixErrorCodeInvalidResponseCode = 2003;
	public static final int PixErrorCodeInvalidResponseData = 2004;
	public static final int PixErrorCodeInvalidAccountData = 2005;
	public static final int PixErrorCodeInvalidImageData = 2006;
	public static final int PixErrorCodeInvalidShortUrl = 2007;
	public static final int PixErrorCodeInvalidCommands = 2008;
	public static final int PixErrorCodeNetworkFailure = 2009;
	public static final int PixErrorInAppBillingFailure = 2010;
	
	
	private int code;

	public PixError(int code) {
		super();
		this.code = code;
	}
	public PixError(int code, String errorText) {
		super(errorText);
		this.code = code;
	}
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		switch (this.code) {
		case PIX_SERVER_ERROR_MISSING_PARAMETER:
			return "PIX_SERVER_ERROR_MISSING_PARAMETER";
		case PIX_SERVER_ERROR_USER_DOESNOT_EXIST:
			return "PIX_SERVER_ERROR_USER_DOESNOT_EXIST";
		case PIX_SERVER_ERROR_NOT_AUTHENTICATED:
			return "PIX_SERVER_ERROR_NOT_AUTHENTICATED";
		case PIX_SERVER_ERROR_COMPOSITION_NOT_EXIST:
			return "PIX_SERVER_ERROR_COMPOSITION_NOT_EXIST";
		case PIX_SERVER_ERROR_INVALID_CLIENT:
			return "PIX_SERVER_ERROR_INVALID_CLIENT";
		case PIX_SERVER_ERROR_INVALID_CREDENTIALS:
			return "PIX_SERVER_ERROR_INVALID_CREDENTIALS";
		case PIX_SERVER_ERROR_INVALID_EMAIL:
			return "PIX_SERVER_ERROR_INVALID_EMAIL";
		case PIX_SERVER_ERROR_EMAIL_EXIST:
			return "PIX_SERVER_ERROR_EMAIL_EXIST";
		case PIX_SERVER_ERROR_EMAIL_DOESNOT_EXIST:
			return "PIX_SERVER_ERROR_EMAIL_DOESNOT_EXIST";
		case PIX_SERVER_ERROR_USERNAME_EXIST:
			return "PIX_SERVER_ERROR_USERNAME_EXIST";
		case PixErrorCodeInvalidRequest:
			return "PixErrorCodeInvalidRequest";
		case PixErrorCodeInvalidRootObject:
			return "PixErrorCodeInvalidRootObject";
		case PixErrorCodeInvalidResponseCode:
			return "PixErrorCodeInvalidResponseCode";
		case PixErrorCodeInvalidResponseData:
			return "PixErrorCodeInvalidResponseData";
		case PixErrorCodeInvalidAccountData:
			return "PixErrorCodeInvalidAccountData";
		case PixErrorCodeInvalidImageData:
			return "PixErrorCodeInvalidImageData";
		case PixErrorCodeInvalidShortUrl:
			return "PixErrorCodeInvalidShortUrl";
		case PixErrorCodeInvalidCommands:
			return "PixErrorCodeInvalidCommands";
		case PixErrorInAppBillingFailure:
			return "PixErrorInAppBillingFailure";
		default:
			return "PixError";
		}
	}
	
}
