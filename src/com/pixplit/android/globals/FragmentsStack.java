package com.pixplit.android.globals;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.pixplit.android.PixApp;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.ui.PixBaseFragment;

public class FragmentsStack {
//		private static final String TAG = "FragmentsStack";
		private final int MAX_STACK_SIZE = 9;
		private ArrayList<BackStackEntry> mStack = new ArrayList<BackStackEntry>();
		private FragmentManager mFragmentManager;
		
		private static final String FRAGMENT_STACK_SIZE_KEY = "Fragment Stack size";
		private static final String FRAGMENT_KEY = "Fragment Stack entry %d";
		private static final String FRAGMENT_ANIM_KEY = "Fragment Stack entry %d exit animation";
		
		private FragmentsStackDelegate mDelegate;
		private int mFragmentsContainer;
		public interface FragmentsStackDelegate {
			public void setFragmentToolbarLogo(PixBaseFragment fragment, int stackIndex);
		}
				
		class BackStackEntry {
			BackStackEntry(PixBaseFragment fragment, int exitAnim) {
				this.fragment = fragment;
				this.exitAnim = exitAnim;
				this.fromPreviousSession = false;
			}
			PixBaseFragment fragment;
			int exitAnim;
			boolean fromPreviousSession;
		}
		
		public FragmentsStack(FragmentManager fragmentManager, FragmentsStackDelegate delegate, int fragmentsContainer, Bundle savedInstanceState) {
			this.mFragmentManager = fragmentManager;
			this.mDelegate = delegate;
			this.mFragmentsContainer = fragmentsContainer;
			if (savedInstanceState != null) {
				this.onRestoreInstanceState(savedInstanceState);
			}
		}
		
		private void clearStateFromBundle(Bundle outState) {
			// clear previous saved state from the bundle
			for (int i=0; i < this.MAX_STACK_SIZE; i++) {
				outState.remove(String.format(FRAGMENT_KEY, i));
				outState.remove(String.format(FRAGMENT_ANIM_KEY, i));
			}
			outState.remove(FRAGMENT_STACK_SIZE_KEY);
		}
		
		public void onSaveInstanceState(Bundle outState) {
			//Utils.pixLog(TAG, "==> called onSaveInstanceState");
//			this.dump("onSaveInstanceState");
			
			if (outState == null || mFragmentManager == null) return;
			
			this.clearStateFromBundle(outState);
			
			// save the stack state in order to retrieve it on restore.
			outState.putInt(FRAGMENT_STACK_SIZE_KEY, mStack.size());
			for (int i = 0; i < mStack.size(); i++) {
				// store a reference for the fragment and its exit animation
				BackStackEntry entry = mStack.get(i);
				if (entry != null && entry.fragment != null) {
					try {
						mFragmentManager.putFragment(outState, 
								String.format(FRAGMENT_KEY, i), entry.fragment);
						outState.putInt(String.format(FRAGMENT_ANIM_KEY, i), entry.exitAnim);
						//Utils.pixLog(TAG, "==> saved fragment: "+entry.fragment.toString());
					} catch (Exception e) {
						// Fragment may not currently be in the FragmentManager
						//Utils.pixLog(TAG, "onSaveInstanceState: failed to save fragment: "+e);
			        	e.printStackTrace();
			        }
				}
			}
		}

		public void onRestoreInstanceState(Bundle savedInstanceState) {
			//Utils.pixLog(TAG, "==> called onRestoreInstanceState");
			if (savedInstanceState == null || mFragmentManager == null) return;
			
			int stackSize = savedInstanceState.getInt(FRAGMENT_STACK_SIZE_KEY, 0);
			
			// clear previous state from the fragment manager
			while (mFragmentManager.popBackStackImmediate()){}
			
			for (int i = 0; i < stackSize; i++) {
				final Fragment fragment = mFragmentManager.getFragment(savedInstanceState, 
						String.format(FRAGMENT_KEY, i));
				int exitAnim = savedInstanceState.getInt(String.format(FRAGMENT_ANIM_KEY,i), 0);
				if (fragment != null && 
						fragment instanceof PixBaseFragment) {
					//Utils.pixLog(TAG, "==> restoring fragment: "+fragment.toString());
					mStack.add(new BackStackEntry((PixBaseFragment)fragment, exitAnim));
				}
			}
			
			/*
			 *  reset the saved state bundle in order not to restore it twice on the same flow -
			 *  restore is called both from onCreate and from onRestore..
			 */
			this.clearStateFromBundle(savedInstanceState);
//			this.dump("onRestoreInstanceState");
		}

		private void executePendingTransactions() {
			if (mFragmentManager != null) {
				try {
					mFragmentManager.executePendingTransactions();
				} catch (Exception e) {
					// ignore cases where there are no pending transactions to execute
					//Utils.pixLog(TAG, "executePendingTransactions failed: " + e);
					e.printStackTrace();
				}
			}
		}
		private void commitTransaction(FragmentTransaction ft) {
			if (ft != null) {
				try {
					this.executePendingTransactions();
					ft.commit();
				} catch (Exception e) {
					// ignore cases where the commit fails or no pending transactions to execute
					//Utils.pixLog(TAG, "commitTransaction failed: " + e);
					e.printStackTrace();
				}
			}
		}
		
		public void pushStackFragment(PixBaseFragment fragment, int pushInAnim, int popOutAnim){
			if (!UserInfo.isUserLogedin() && fragment.isSecured()) {
				//Utils.pixLog(TAG, "User not loged in. can't access secure view. dismiss request");
			} else {
				//Utils.pixLog(TAG, "pushStackFragment: "+fragment);
				FragmentTransaction ft = mFragmentManager.beginTransaction(); 
				if (mDelegate != null) {
					mDelegate.setFragmentToolbarLogo(fragment, mStack.size());
				}
				PixBaseFragment prevTop = peekStack();
				ft.setCustomAnimations(pushInAnim, popOutAnim);
				ft.add(mFragmentsContainer, fragment); 
				mStack.add(new BackStackEntry(fragment, popOutAnim));
				this.commitTransaction(ft);
				
		    	if (mStack.size() > MAX_STACK_SIZE) {
		    		this.removeFragment(2);
				}
		    	this.onFragmentEnterBackStack(prevTop);
			}
		}
		
		public void popOutFragment(boolean withAnimation){
			if (mStack.size() > 1) {
				BackStackEntry topEntry = mStack.remove(mStack.size()-1);
				if (topEntry != null) {
					FragmentTransaction ft = mFragmentManager.beginTransaction();
					if (withAnimation) {
						ft.setCustomAnimations(0, topEntry.exitAnim);
					}
					ft.remove(topEntry.fragment);
					this.commitTransaction(ft);
				}
				
				PixBaseFragment newTop = peekStack();
				if (newTop != null) {
					onFragmentExitBackStack(newTop);
				}
			}
		}
		
		public PixBaseFragment peekStack() {
			if (mStack.size() > 0) {
				return getStackEntry(mStack.size()-1);
			}
			return null;
		}
		
		public PixBaseFragment getStackEntry(int index) {
			if (index >= 0 && mStack.size() > index) {
				if (mStack.get(index) != null)
					return mStack.get(index).fragment;
			}
			return null;
		}
		
		public void replaceTopFragment(PixBaseFragment fragment, int pushInAnim, int popOutAnim){
			if (mStack.size() > 0 && fragment != null) {
				// remove the top fragment
				BackStackEntry replacedEntry = mStack.remove(mStack.size()-1);
				//Utils.pixLog(TAG, "replacing entry:" + replacedEntry.fragment);
				FragmentTransaction ft = mFragmentManager.beginTransaction();
				ft.remove(replacedEntry.fragment);
				// add the new top fragment
				if (mDelegate != null) {
					mDelegate.setFragmentToolbarLogo(fragment, mStack.size());
				}
				ft.setCustomAnimations(0, popOutAnim);
				ft.add(mFragmentsContainer, fragment); 
				mStack.add(new BackStackEntry(fragment, popOutAnim));
				this.commitTransaction(ft);
			}
		}
		
		private void removeFragment(int index) {
			if (mStack.size() > index) {
				BackStackEntry removedEntry = mStack.remove(index);
				//Utils.pixLog(TAG, "removing entry:" + removedEntry.fragment);
				FragmentTransaction ft = mFragmentManager.beginTransaction();
				ft.remove(removedEntry.fragment);
				this.commitTransaction(ft);
			}
		}
		
		public void clearFragmentStack() {
			int removeFragsNum = mStack.size() - 1;
			for (int i=0; i<removeFragsNum; i++) {
				this.popOutFragment(true);
			}
			this.executePendingTransactions();
		}
							
		private void onFragmentEnterBackStack(final PixBaseFragment fragment){
			if (fragment != null) {
				//Utils.pixLog(TAG, "--> " + fragment.toString()+" enter back stack");
//				this.dump("onFragmentEnterBackStack");

				PixApp.hideKeypad(fragment.getView());
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						View fragView = fragment.getView();
						if (fragView != null) {
							// verify that the fragment is not the top one (due to changes in the stack through the delay)
							if (fragment != peekStack()) {
								fragView.setVisibility(View.GONE);
								//Utils.pixLog(TAG, fragment.toString()+" -- setting visibility to gone");
							}
						}
					}
				}, 900);
			} 
		}
		
		private void onFragmentExitBackStack(PixBaseFragment fragment){
			if (fragment != null) {
				//Utils.pixLog(TAG, "<-- " + fragment.toString()+" exit back stack");
				PixApp.hideKeypad(fragment.getView());
				View fragView = fragment.getView();
				if (fragView != null) {
					fragView.setVisibility(View.VISIBLE);
					//Utils.pixLog(TAG, fragment.toString()+" -- setting visibility to visible");
				}
			} 
		}

		public void onNewNewsRecords(int newRecordsNum) {
			if (mDelegate != null) {
				int stackSize = mStack.size();
				for (int i=1; i<stackSize; i++) {
					mDelegate.setFragmentToolbarLogo(mStack.get(i).fragment, i);
				}
			}
		}
		
		public int getSize() {
			return mStack.size();
		}
		
//		public void dump(String context) {
//			int stackSize = mStack.size();
//			//Utils.pixLog(TAG, "=========== dump stack ========= (context:"+context+", size:"+stackSize+")");
//			for (int i=0; i<stackSize; i++) {
//				PixBaseFragment fragment = null;
//				BackStackEntry entry = mStack.get(i);
//				if (entry != null) {
//					fragment = entry.fragment;
//				}
//				//Utils.pixLog(TAG, "Entry "+i+":"+ ((fragment != null) ? fragment : "fragment is null"));
//			}
//			
//			mFragmentManager.dump("== mFragmentManager dump", null, 
//				     new PrintWriter(System.out, true), null);
//		}
		
		public void setPreviousSessionSnapshot() {
			if (mStack.size() > 0) {
				BackStackEntry topEntry = mStack.get(mStack.size()-1);
				if (topEntry != null) {
					topEntry.fromPreviousSession = true;
				}
			}
		}
		
		public boolean currentSessionHasEntries() {
			if (mStack.size() > 1) {
				BackStackEntry prevEntry = mStack.get(mStack.size()-2);
				if (prevEntry != null) {
					return !prevEntry.fromPreviousSession;
				}
			}
			return false;
		}
	}