package com.pixplit.android.globals;

import android.graphics.Rect;
import android.os.Bundle;

import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.util.Utils;

public class PixCompositionComponent {
	private int index;
	private Rect rect;
	private PixParticipant participant = null;
	
	private PixCompositionComponent() {}   // empty constructor for serialization purpose
	
	public PixCompositionComponent(Rect rect, int index) {
		this.rect = rect;
		this.index = index;
	}
	
   	private static final String PIX_COMP_COMPONENT_KEY_PIX_COMPONENT = "PIX_COMP_COMPONENT_%s_KEY_PIX_COMPONENT";
   	private static final String PIX_COMP_COMPONENT_KEY_INDEX = "PIX_COMP_COMPONENT_%s_KEY_INDEX";
   	private static final String PIX_COMP_COMPONENT_KEY_RECT = "PIX_COMP_COMPONENT_%s_KEY_RECT";
   	
	public void serialize(Bundle outBundle, String postfix) {
   		if (outBundle == null) return;
   		// indicates that the bundle holds a composition component with this postfix
   		outBundle.putBoolean(String.format(PIX_COMP_COMPONENT_KEY_PIX_COMPONENT, postfix), true); 
   		outBundle.putInt(String.format(PIX_COMP_COMPONENT_KEY_INDEX, postfix), this.index);
   		if (this.rect != null) {
   			outBundle.putString(String.format(PIX_COMP_COMPONENT_KEY_RECT, postfix), this.rect.flattenToString());
   		}
   		if (this.participant != null) {
   			this.participant.serialize(outBundle, postfix);
   		}
	}
	
	public static boolean hasCompositionComponent(Bundle inBundle, String postfix) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(String.format(PIX_COMP_COMPONENT_KEY_PIX_COMPONENT, postfix));
   	}
   	
   	public static PixCompositionComponent deserialize(Bundle inBundle, String postfix) {
   		if (!hasCompositionComponent(inBundle, postfix)) return null;
   		PixCompositionComponent compComponent = new PixCompositionComponent();
   		compComponent.index = inBundle.getInt(String.format(PIX_COMP_COMPONENT_KEY_INDEX, postfix));
   		String rectString = inBundle.getString(String.format(PIX_COMP_COMPONENT_KEY_RECT, postfix));
   		if (Utils.isValidString(rectString)) {
   			compComponent.rect = Rect.unflattenFromString(rectString);
   		}
   		compComponent.participant = PixParticipant.deserialize(inBundle, postfix);
   		return compComponent;
   	}
   	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public Rect getRect() {
		return rect;
	}
	public void setRect(Rect rect) {
		this.rect = rect;
	}
	public PixParticipant getParticipant() {
		return participant;
	}
	public void setParticipant(PixParticipant participant) {
		this.participant = participant;
	}
}
