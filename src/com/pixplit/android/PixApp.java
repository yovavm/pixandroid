package com.pixplit.android;

import java.util.ArrayList;
import java.util.Hashtable;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixPushNotificationsReceiver;
import com.pixplit.android.ui.ActivitiesControllerInterface;
import com.pixplit.android.ui.MyCompletedFragment;
import com.pixplit.android.ui.MyPlaygroundFragment;
import com.pixplit.android.ui.PopularFragment;
import com.pixplit.android.ui.activities.AdminAccountsActivity;
import com.pixplit.android.ui.activities.AdminEurekaSignUpActivity;
import com.pixplit.android.ui.activities.AdminPopularCandidatesActivity;
import com.pixplit.android.ui.activities.AdminPopularFullActivity;
import com.pixplit.android.ui.activities.AdminSettingsActivity;
import com.pixplit.android.ui.activities.AdminSuggestedFullActivity;
import com.pixplit.android.ui.activities.ChallengesActivity;
import com.pixplit.android.ui.activities.ChatActivity;
import com.pixplit.android.ui.activities.CommentsActivity;
import com.pixplit.android.ui.activities.CreateNewSplitActivity;
import com.pixplit.android.ui.activities.EditProfileActivity;
import com.pixplit.android.ui.activities.FbPickPhotoActivity;
import com.pixplit.android.ui.activities.FollowFriendsActivity;
import com.pixplit.android.ui.activities.ForgotPasswordActivity;
import com.pixplit.android.ui.activities.GalleryFeedActivity;
import com.pixplit.android.ui.activities.GoogleSearchImageActivity;
import com.pixplit.android.ui.activities.InboxActivity;
import com.pixplit.android.ui.activities.InstagramActivity;
import com.pixplit.android.ui.activities.InviteFollowersActivity;
import com.pixplit.android.ui.activities.MyFeedActivity;
import com.pixplit.android.ui.activities.NewsActivity;
import com.pixplit.android.ui.activities.PixInfoActivity;
import com.pixplit.android.ui.activities.PixWebViewActivity;
import com.pixplit.android.ui.activities.PopularActivity;
import com.pixplit.android.ui.activities.ProcessNewSplitActivity;
import com.pixplit.android.ui.activities.ProfileActivity;
import com.pixplit.android.ui.activities.ProfileFeedsPagerActivity;
import com.pixplit.android.ui.activities.PublishNewSplitActivity;
import com.pixplit.android.ui.activities.QuickGuidePagerActivity;
import com.pixplit.android.ui.activities.RelatedActivity;
import com.pixplit.android.ui.activities.ScalePhotoInSplitActivity;
import com.pixplit.android.ui.activities.SearchUsersActivity;
import com.pixplit.android.ui.activities.SettingsActivity;
import com.pixplit.android.ui.activities.SettingsFbActivity;
import com.pixplit.android.ui.activities.SettingsNotificationsActivity;
import com.pixplit.android.ui.activities.SignInActivity;
import com.pixplit.android.ui.activities.SignUpActivity;
import com.pixplit.android.ui.activities.SingleCompActivity;
import com.pixplit.android.ui.activities.ThemeActivity;
import com.pixplit.android.ui.activities.UsersListActivity;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import com.urbanairship.push.CustomPushNotificationBuilder;
import com.urbanairship.push.PushManager;

public class PixApp extends Application implements ActivitiesControllerInterface{
//	private static final String TAG = "PixApp";
	private static PixApp pixApp;
	private static Context context;
	
	// build parameters - move to Ant.
	private static final boolean isDebug = false;
	private static final boolean adminBuild = false;

	private boolean checkAppVersion = true;
	
	public static final String PACKAGE_NAME = "com.pixplit.android";
	
	// Returns the application instance
	public static PixApp getInstance() {
		return pixApp; 
	}
	
	@Override
	public final void onCreate() {
		super.onCreate();
		pixApp = this;
		initApp();
	}
	
	@Override
	public void onLowMemory() {
		//Utils.pixLog(TAG, "onLowMemory called!!");
	}
	
	public static boolean inDebug(){
		return isDebug;
	}
	
	public static boolean inAdminBuild(){
		return adminBuild;
	}
	
	private void initApp() {
		PixApp.setContext(this);
		//Utils.pixLog(TAG, "frameWidth:"+PixAppState.getState().getFrameWidth()+", frameHeight:"+PixAppState.getState().getFrameHeight());
		initPushNotifications();
		needRefreshContexts = new Hashtable<String, Boolean>();
		runningActivitiesContexts = new Hashtable<String, Boolean>();
		initializeTypefaces();
//		// for testing the supported preview dimensions (with respect to split parts).
//		SplitPreviewCompatibleDimensFinder.checkPreviewScalesCompatability();
	}
	
	private void initPushNotifications() {
		AirshipConfigOptions options = AirshipConfigOptions.loadDefaultOptions(this);
		options.gcmSender = "830918200397";
		options.transport = "gcm";
		// development
        options.inProduction = true;
        options.developmentAppKey = "2o_0T88bSI2Wa6efBRUIDA";
        options.developmentAppSecret = "XEihDAc-RP-B8hzZL9YesA";
        options.productionAppKey = "eN44C2WjSkeP-KiL8Igdyg";
        options.productionAppSecret = "f5k-iSLxSZK30QDUTcttcQ";
        
        UAirship.takeOff(this, options);
        setCustomPushNotifications();
        PushManager.shared().setIntentReceiver(PixPushNotificationsReceiver.class);
	}
	
	public static void enablePushNotifications() {
    	PushManager.init();
		PushManager.shared().setIntentReceiver(PixPushNotificationsReceiver.class);
		if (Utils.isValidString(PixAppState.UserInfo.getUserName())) {
			try {
				// this might fail on some devices - IllegalArgumentException: 
				// Unknown URL content://com.pixplit.android.urbanairship.provider/preferences
				PushManager.shared().setAlias(PixAppState.UserInfo.getUserName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
        
        PixApp.getInstance().setCustomPushNotifications();
        PushManager.enablePush();
    }
    
	public void setCustomPushNotifications() {
		CustomPushNotificationBuilder nb = new CustomPushNotificationBuilder();
        
        nb.statusBarIconDrawableId = R.drawable.ic_stat_notify;
        nb.layout = R.layout.custom_push_notification_layout; // The layout resource to use
        nb.layoutIconDrawableId = R.drawable.ic_launcher; // The icon you want to display
        nb.layoutIconId = R.id.icon; // The icon's layout 'id'
        nb.layoutSubjectId = R.id.subject; // The id for the 'subject' field
        nb.layoutMessageId = R.id.message; // The id for the 'message' field
        //set this ID to a value > 0 if you want a new notification to replace the previous one
        nb.constantNotificationId = 123;        
        // Set the custom notification builder
        PushManager.shared().setNotificationBuilder(nb);
	}
	
	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		PixApp.context = context;
	}
	
	public static void hideKeypad(View v) {
		if (v != null) {
			final InputMethodManager inputMethodManager = (InputMethodManager)PixApp.getContext()
	    			.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
		}
	}
	
	public static void showKeyboard(EditText editText, ResultReceiver resultReceiver) {
		if (editText != null) {
			editText.setFocusableInTouchMode(true);
			editText.requestFocus();
			
			final InputMethodManager inputMethodManager = (InputMethodManager) PixApp.getContext()
		            .getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT, resultReceiver);				
		}
	}
	
	// activities controller
	// activities running state logic
	private Hashtable<String, Boolean> runningActivitiesContexts;
	@Override
	public void markActivityIsRunning(String context, boolean running) {
		if (runningActivitiesContexts != null && context != null) {
			runningActivitiesContexts.put(context, Boolean.valueOf(running));
		}
	}

	@Override
	public boolean isActivityRunning(String context) {
		if (runningActivitiesContexts != null && context != null) {
			Boolean isRunning = runningActivitiesContexts.get(context);
			if (isRunning != null) {
				return isRunning.booleanValue();
			}
		}
		return false;
	}
	
	// refresh activities logic
	private boolean needRefresh = false; // general refresh bit (used in inbox for example)
	private Hashtable<String, Boolean> needRefreshContexts;
	
	@Override
	public void markForRefresh(String context, boolean refresh) {
		if (context != null && needRefreshContexts != null) {
			needRefreshContexts.put(context, Boolean.valueOf(refresh));
		}
		needRefresh = true;
	}
	
	@Override
	public boolean needRefresh(String context) {
		boolean ret = false;
		if (context != null && needRefreshContexts != null) {
			Boolean bool = needRefreshContexts.get(context);
			if (bool != null) {
				ret = bool.booleanValue();
				// remove the refresh context as refresh event has been delivered
				needRefreshContexts.remove(context);
			}
		} else {
			ret = needRefresh;
			// reset the need refresh as it was already read.
			needRefresh = false;
		}
		return ret;
	}
	///////////////////////////////////////////////////////
	//  start activities methods
	///////////////////////////////////////////////////////
    @Override
	public void gotoPopularRequest(final Activity activity, Bundle args){
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, PopularActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
	}
    
    @Override
	public void gotoCompComments(final Activity activity, Bundle args){
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, CommentsActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_COMP_COMMENTS);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}

    @Override
	public void gotoCompRelated(final Activity activity, Bundle args) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, RelatedActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }

    @Override
	public void gotoSingleCompView(final Context context, Bundle args, int flags) {
    	if (context == null) return;
    	Intent startActivityIntent = new Intent(context, SingleCompActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		((Activity)context).overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    	}
    }
    
    @Override
	public void gotoGalleryFeed(final Context context, Bundle args, int flags, ArrayList<PixComposition> comps) {
    	if (context == null) return;
		PixApp.setGalleryFeedComps(comps);
    	Intent startActivityIntent = new Intent(context, GalleryFeedActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		((Activity)context).overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    	}
    }
    
    @Override
	public void gotoProfileGalleryFeed(final Context context, Bundle args, int flags, ArrayList<PixComposition> comps) {
    	if (context == null) return;
		PixApp.setGalleryFeedComps(comps);
    	Intent startActivityIntent = new Intent(context, ProfileFeedsPagerActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		((Activity)context).overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    	}
    }

    @Override
	public void gotoCreateNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage) {
    	if (activity == null) return;
    	CreateNewSplitActivity.setJoinedCompImage(joinedCompImage);
    	Intent startActivityIntent = new Intent(activity, CreateNewSplitActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT);
    	// Override transitions: we don't want the normal window animation in addition
        activity.overridePendingTransition(0, 0);
    }
    
    @Override
	public void gotoScalePhotoInSplit(final Activity activity, Bundle args, PixBitmapHolder joinedComp, PixBitmapHolder activePartBitmap) {
    	if (activity == null) return;
    	ScalePhotoInSplitActivity.setJoinedCompImage(joinedComp);
    	ScalePhotoInSplitActivity.setActivePartOrigBitmap(activePartBitmap);
    	Intent startActivityIntent = new Intent(activity, ScalePhotoInSplitActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT);
    	// Override transitions: we don't want the normal window animation in addition
        activity.overridePendingTransition(0, 0);
    }
    
    @Override
	public void gotoProcessNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage, PixBitmapHolder activePartBitmap) {
    	if (activity == null) return;
    	ProcessNewSplitActivity.setJoinedCompImage(joinedCompImage);
    	ProcessNewSplitActivity.setActivePartOrigBitmap(activePartBitmap);
    	Intent startActivityIntent = new Intent(activity, ProcessNewSplitActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT);
    	// Override transitions: we don't want the normal window animation in addition
        activity.overridePendingTransition(0, 0);
    }
    
    @Override
	public void gotoPublishNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage, PixBitmapHolder activePartBitmap) {
    	if (activity == null) return;
    	PublishNewSplitActivity.setJoinedCompImage(joinedCompImage);
    	PublishNewSplitActivity.setActivePartOrigBitmap(activePartBitmap);
    	Intent startActivityIntent = new Intent(activity, PublishNewSplitActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT);
    	// Override transitions: we don't want the normal window animation in addition
        activity.overridePendingTransition(0, 0);
    }
    
    @Override
	public void gotoInviteFollowers(final Activity activity, Bundle args) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, InviteFollowersActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_INVITE_FOLLOWERS);
    	activity.overridePendingTransition(R.anim.slide_bottom_top, R.anim.hold_in_place);
    }
    
    @Override
	public void gotoMyFeed(final Activity activity, int anim) {
    	if (activity == null) return;
    	Intent startActivityIntent;
		startActivityIntent = new Intent(activity, MyFeedActivity.class);
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
    }
    
    @Override
	public void gotoUserProfile(final Context context, Bundle args, int flags) {
    	if (context == null) return;
    	boolean menuLaunch = false;
    	if (args != null) {
    		menuLaunch = args.getBoolean(PixDefs.FRAG_ARG_MAIN_MENU_LAUNCH, false);
    	}
    	Intent startActivityIntent = new Intent(context, ProfileActivity.class);
//    	if (menuLaunch) {
//    		startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    	}
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		if (menuLaunch) {
    			((Activity)context).overridePendingTransition(0, R.anim.activity_fade_out);
    		} else {
        		((Activity)context).overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    		}
    	}
    }
    
    @Override
	public void gotoFollowFriends(final Activity activity, Bundle args) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, FollowFriendsActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }
    
    @Override
	public void gotoSearchUsers(final Activity activity, Bundle args) {
    	if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, SearchUsersActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }
    
    @Override
	public void gotoQuickGuide(final Activity activity) {
    	if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, QuickGuidePagerActivity.class);
		startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(0, 0);
	}
    
    @Override
	public void gotoSignUp(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, SignUpActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_bottom_top, R.anim.hold_in_place);
    }
    
    @Override
	public void gotoSignIn(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, SignInActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_bottom_top, R.anim.hold_in_place);
    }
    
    @Override
	public void gotoSettings(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, SettingsActivity.class);
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_bottom_top, R.anim.activity_fade_out);
    }
    
    @Override
	public void gotoSettingsNotifications(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, SettingsNotificationsActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }

    @Override
	public void gotoSettingsFacebook(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, SettingsFbActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }
    
    @Override
	public void gotoAdminSettings(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, AdminSettingsActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }

    @Override
	public void gotoResetPassword(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, ForgotPasswordActivity.class);
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_RESET_PASSWORD);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);    	
    }
    
    @Override
	public void gotoNews(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, NewsActivity.class);
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
    }
    
    @Override
    public void gotoPickInstagramPhoto(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, InstagramActivity.class);
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_PICK_PHOTO);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
    }
    
    @Override
    public void gotoPickFacebookPhoto(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, FbPickPhotoActivity.class);
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_PICK_PHOTO);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
    }
    
    @Override
    public void gotoPickGooglePhoto(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, GoogleSearchImageActivity.class);
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_PICK_PHOTO);
    	activity.overridePendingTransition(0, R.anim.activity_fade_out);
    }
    
    @Override
	public void gotoInfo(final Activity activity) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, PixInfoActivity.class);
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }
    
    @Override
	public void gotoWebView(final Activity activity, Bundle args) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, PixWebViewActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivity(startActivityIntent);
    	activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    }
    
    @Override
	public void gotoInbox(final Context context, int flags) {
    	if (context == null) return;
    	Intent startActivityIntent = new Intent(context, InboxActivity.class);
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		((Activity)context).overridePendingTransition(0, R.anim.activity_fade_out);
    	}
    }

	@Override
	public void gotoChat(final Context context, Bundle args, int flags) {
		if (context == null) return;
		Intent startActivityIntent = new Intent(context, ChatActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
    	if (flags != 0) {
    		startActivityIntent.setFlags(flags);
    	}
    	context.startActivity(startActivityIntent);
    	if (context instanceof Activity) {
    		((Activity)context).overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    	}
	}
	
	@Override
	public void gotoUsersList(final Activity activity, Bundle args) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, UsersListActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}

	@Override
	public void gotoThemeFeed(Activity activity, Bundle args) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, ThemeActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoChallengesFeed(final Activity activity) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, ChallengesActivity.class);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}

	@Override
	public void gotoAdminPopularFull(Activity activity) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, AdminPopularFullActivity.class);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoAdminSuggestedFull(Activity activity) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, AdminSuggestedFullActivity.class);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoAdminAddEurekaAccount(Activity activity) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, AdminEurekaSignUpActivity.class);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoAdminEurekaAccounts(Activity activity) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, AdminAccountsActivity.class);
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoAdminPopularCandidates(Activity activity, Bundle args) {
		if (activity == null) return;
		Intent startActivityIntent = new Intent(activity, AdminPopularCandidatesActivity.class);
    	if (args != null) {
    		startActivityIntent.putExtras(args);
    	}
		activity.startActivity(startActivityIntent);
		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
	}
	
	@Override
	public void gotoEditProfile(final Activity activity, Bundle args) {
    	if (activity == null) return;
    	Intent startActivityIntent = new Intent(activity, EditProfileActivity.class);
    	int animType = ActivitiesControllerInterface.PIX_ANIMATE_NO_ANIMATION;
    	if (args != null) {
    		animType = args.getInt(PixDefs.FRAG_ARG_ANIMATION_TYPE);
    		startActivityIntent.putExtras(args);
    	}
    	activity.startActivityForResult(startActivityIntent, PixDefs.ACTIVITY_RESULT_CODE_EDIT_PROFILE);
		if (animType == ActivitiesControllerInterface.PIX_ANIMATE_SLIDE_IN_BOTTOM_UP) {
			activity.overridePendingTransition(R.anim.slide_bottom_top, R.anim.hold_in_place);
    	} else {
    		// default animation
    		activity.overridePendingTransition(R.anim.slide_right_to_left, R.anim.push_to_stack);
    	}    	
    }

	// retained fragments
	static PopularFragment popularFragment = null;
	static MyCompletedFragment myCompletedFragment = null;
	static MyPlaygroundFragment myPlaygroundFragment = null;
	public static PopularFragment getPopularFragment(Bundle args) {
		if (popularFragment == null) {
			popularFragment = PopularFragment.newInstance(args);
		}
		return popularFragment;
	}
	public static MyCompletedFragment getMyCompletedFragment() {
		if (myCompletedFragment == null) {
			myCompletedFragment = MyCompletedFragment.newInstance();
		}
		return myCompletedFragment;
	}
	public static MyPlaygroundFragment getMyPlaygroundFragment() {
		if (myPlaygroundFragment == null) {
			myPlaygroundFragment = MyPlaygroundFragment.newInstance();
		}
		return myPlaygroundFragment;
	}

	public boolean isCheckAppVersion() {
		return checkAppVersion;
	}

	public void setCheckAppVersion(boolean checkAppVersion) {
		this.checkAppVersion = checkAppVersion;
	}
	
	public static class Fonts {
        public static Typeface RobotoLight;
        public static Typeface RobotoRegular;
        public static Typeface RobotoMedium;
        public static Typeface RobotoBold;
        public static Typeface PixFont;
        public static Typeface PixFontMedium;
        public static Typeface PixFontBold;
    }

    private void initializeTypefaces(){
        Fonts.RobotoLight = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        Fonts.RobotoRegular = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Fonts.RobotoMedium = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Fonts.RobotoBold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        if (Utils.isHdpi() || Utils.isMdpi()) {
        	Fonts.PixFont = Fonts.RobotoRegular;
        } else {
        	Fonts.PixFont = Fonts.RobotoLight;
        }
        Fonts.PixFontMedium = Fonts.RobotoMedium;
        Fonts.PixFontBold = Fonts.RobotoBold;
    }

    // the gallery feed comps are used by GalleryFeed (faster vs. serialization)
	private static ArrayList<PixComposition> galleryFeedComps;
    public static ArrayList<PixComposition> getGalleryFeedComps() {
		return galleryFeedComps;
	}
	public static void setGalleryFeedComps(ArrayList<PixComposition> comps) {
		PixApp.galleryFeedComps = comps;
	}
}
