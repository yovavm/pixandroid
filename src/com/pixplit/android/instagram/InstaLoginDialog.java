package com.pixplit.android.instagram;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.InstagramUserInfo;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.InstagramUserInfoHttpRequest;
import com.pixplit.android.util.Utils;

public class InstaLoginDialog extends Dialog implements InstaAuthWebViewListener {

	private LinearLayout mLayout;
	private TextView mTitle;
	private WebView mWebView;
	private InstaAuthListener mAuthDialogListener;
	private String mScope;
	
	private static String url = 
			PixHttpAPI.INSTAGRAM_AUTH_URL + "?" + 
			PixHttpAPI.INSTAGRAM_CLIENT_ID_PARAM +  PixHttpAPI.INSTAGRAM_CLIENT_ID_VAL + "&" + 
			PixHttpAPI.INSTAGRAM_REDIRECT_URI_PARAM + PixHttpAPI.INSTAGRAM_REDIRECT_URI_VAL + "&" + 
			PixHttpAPI.INSTAGRAM_RESPONSE_TYPE;
	
	public interface InstaAuthListener {
		public void onComplete();
		public void onError(String error);
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		mLayout = new LinearLayout(getContext());
		mLayout.setOrientation(LinearLayout.VERTICAL);
				
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mTitle = new TextView(getContext());
		mTitle.setText(PixApp.getContext().getString(R.string.instagram_login));
		mTitle.setTextColor(Color.WHITE);
		mTitle.setTypeface(Typeface.DEFAULT_BOLD);
		mTitle.setBackgroundColor(Color.BLACK);
		mTitle.setPadding(10, 10, 10, 10);
		mTitle.setMinHeight(50);
		mLayout.addView(mTitle);
		
		mWebView = new WebView(getContext());
		mWebView.setVerticalScrollBarEnabled(true);
		mWebView.setHorizontalScrollBarEnabled(true);
		mWebView.setWebViewClient(new InstaAuthWebViewClient(getContext(), this));
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setSavePassword(false);
		mWebView.clearHistory();
		mWebView.clearView();
		mWebView.clearCache(true);
		
		if (Utils.isValidString(mScope)) {
			url = url.concat("&"+String.format(PixHttpAPI.INSTAGRAM_SCOPE, mScope));
		}
		
		mWebView.loadUrl(url);
		mWebView.setMinimumHeight(300);
		mWebView.setBackgroundResource(R.drawable.splash);
		int windowHeight = PixAppState.getState().getFrameHeight();
		int dialogHeight = (int)(windowHeight * .75f);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, dialogHeight);
		mLayout.addView(mWebView, params);
		
		params = new LayoutParams(LayoutParams.MATCH_PARENT, dialogHeight);
		addContentView(mLayout, params);
	}
	
	public InstaLoginDialog(Context context, InstaAuthListener listener) {
		super(context);
		this.mAuthDialogListener = listener;
	}
	
	public InstaLoginDialog(Context context, InstaAuthListener listener, String scope) {
		this(context, listener);
		this.mScope = scope;
	}

	@Override
	public void onComplete(String token) {
		storeIntaUserInfo(token);
	}

	@Override
	public void onError(String error) {
		this.dismiss();
		if (mAuthDialogListener != null) {
			mAuthDialogListener.onError(error);
		}
	}

	@Override
	public void updateTitle(String title) {
		if (Utils.isValidString(title) &&
				mTitle != null) {
			mTitle.setText(title);
		}
	}
	
	private void storeIntaUserInfo(String token) {
		UserInfo.setInstagramToken(token);
		if (Utils.isValidString(mScope)) {
			UserInfo.addInstagramPermissions(mScope);
		}
		RequestQueue queue = PixAppState.getState().getRequestQueue();
		queue.add(new InstagramUserInfoHttpRequest(new RequestDelegate() {
			
			@Override
			public void requestFinishedWithError(BaseVolleyHttpRequest request,
					PixError error) {
				InstaLoginDialog.this.dismiss();
				if (mAuthDialogListener != null) {
					mAuthDialogListener.onError(
							(error != null) ? error.getMessage() : "Failed to fetch Instagram user info");
				}
			}
			
			@Override
			public void requestFinished(BaseVolleyHttpRequest request,
					PixObject response) {
				if (response != null && 
						response instanceof InstagramUserInfo) {
					InstagramUserInfo userInfo = (InstagramUserInfo)response;
					if (userInfo != null) {
						UserInfo.setInstagramFullname(userInfo.getFullName());
						UserInfo.setInstagramUsername(userInfo.getUserName());
					}
				}
				InstaLoginDialog.this.dismiss();
				if (mAuthDialogListener != null) {
					mAuthDialogListener.onComplete();
				}
			}
		}));
	}
}
