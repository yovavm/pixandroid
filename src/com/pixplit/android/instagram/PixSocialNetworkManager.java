package com.pixplit.android.instagram;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.pixplit.android.PixShareTypeE;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.FollowUsDialog;
import com.pixplit.android.ui.elements.FollowUsDialog.FollowUsDelegate;
import com.pixplit.android.util.Utils;

public class PixSocialNetworkManager {

	public static void displayFollowUsIfNeeded(final Context context) {
		if (shouldDisplayFollowOnInstagram()) {
			// display "follow us on instagram" pop-up.
			new FollowUsDialog(context, 
					new FollowUsDelegate() {
						
						@Override
						public void onFollowUsClick() {
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_ANY_FEED, 
									PixDefs.APP_EVENT_FOLLOW_ON_INSTAGRAM_PRESSED, 
									null);

							InstagramSessionManager.followOnInstaRequest(context);
						}
					}, 
					R.drawable.instagram_connect_btn_selector, 
					context.getString(R.string.follow_us_on_instagram)).show();
			Utils.setSharedPreferencesBoolean(PixDefs.FOLLOW_ON_INSTAGRAM_DISPLAYED, true);
			return;
		} else if (shouldDisplayFollowOnTwitter()) {
			// display "follow us on twitter" pop-up.
			new FollowUsDialog(context, 
					new FollowUsDelegate() {
						
						@Override
						public void onFollowUsClick() {
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_ANY_FEED, 
									PixDefs.APP_EVENT_FOLLOW_ON_TWITTER_PRESSED, 
									null);
							followUsOnTwitter(context);
						}
					}, 
					R.drawable.twitter_connect_btn_selector, 
					context.getString(R.string.follow_us_on_twitter)).show();
			Utils.setSharedPreferencesBoolean(PixDefs.FOLLOW_ON_TWITTER_DISPLAYED, true);
		} else if (shouldDisplayFollowOnFacebook()){
			// display "follow us on facebook" pop-up.
			new FollowUsDialog(context, 
					new FollowUsDelegate() {
						
						@Override
						public void onFollowUsClick() {
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_ANY_FEED, 
									PixDefs.APP_EVENT_FOLLOW_ON_FACEBOOK_PRESSED, 
									null);
							followUsOnFacebook(context);
						}
					}, 
					R.drawable.facebook_connect_btn_selector, 
					context.getString(R.string.follow_us_on_facebook)).show();
			Utils.setSharedPreferencesBoolean(PixDefs.FOLLOW_ON_FACEBOOK_DISPLAYED, true);
		}
	}
	
	// follow pixplit on twitter is done opening twitter app(if installed)/web on pixplit page
	private static void followUsOnTwitter(Context context) {
		Intent intent = null;
		try {
		    // get the Twitter app if possible
			context.getPackageManager().getPackageInfo(PixDefs.twitterPackageName, 0);
		    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id="+PixDefs.twitterPixplitUserId));
		    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} catch (Exception e) {
		    // no Twitter app, revert to browser
		    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PixDefs.twitterPixplitPageUrl));
		}
		try {
			context.startActivity(intent);
		} catch (Exception e) {
			// failed
		}
	}
	
	// follow pixplit on facebook is done opening twitter app(if installed)/web on pixplit page
	private static void followUsOnFacebook(Context context) {
		Intent intent = null;
		try {
		    // get the facebook app if possible
		    context.getPackageManager().getPackageInfo(PixDefs.facebookPackageName, 0);
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/"+PixDefs.facebookPixplitUserId));
		    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} catch (Exception e) {
		    // no Facebook app, revert to browser
		    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PixDefs.facebookPixplitUserUrl));
		}
		try {
			context.startActivity(intent);
		} catch (Exception e) {
			// failed
		}
	}

	public static void shareToClick(PixShareTypeE shareType) {
		switch (shareType) {
		case pixShareToFacebook:
			Utils.setSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_FACEBOOK, true);
			break;
		case pixShareToInstagram:
			Utils.setSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_INSTAGRAM, true);
			break;
		case pixShareToTwitter:
			Utils.setSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_TWITTER, true);
			break;
		default:
			break;
		}
	}
	
	private static boolean shouldDisplayFollowOnInstagram() {
		boolean followOnInstagramDisplayed = Utils.getSharedPreferencesBoolean(PixDefs.FOLLOW_ON_INSTAGRAM_DISPLAYED, false);
		// display the pop-up only once
		if (followOnInstagramDisplayed) return false;
		// display if shared to instagram
		boolean sharedToInstagram = Utils.getSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_INSTAGRAM, false);
		if (sharedToInstagram) return true;
		// display if user has linked instagram account
		if (UserInfo.getInstagramToken() != null) return true;
		return false;
	}
	
	private static boolean shouldDisplayFollowOnTwitter() {
		boolean followOnTwitterDisplayed = Utils.getSharedPreferencesBoolean(PixDefs.FOLLOW_ON_TWITTER_DISPLAYED, false);
		// display the pop-up only once
		if (followOnTwitterDisplayed) return false;
		// display if shared to twitter
		boolean sharedToTwitter = Utils.getSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_TWITTER, false);
		if (sharedToTwitter) return true;
		return false;
	}
	
	private static boolean shouldDisplayFollowOnFacebook() {
		boolean followOnFacebookDisplayed = Utils.getSharedPreferencesBoolean(PixDefs.FOLLOW_ON_FACEBOOK_DISPLAYED, false);
		// display the pop-up only once
		if (followOnFacebookDisplayed) return false;
		// display if shared to instagram
		boolean sharedToFacebook = Utils.getSharedPreferencesBoolean(PixDefs.SHARED_SPLIT_TO_FACEBOOK, false);
		if (sharedToFacebook) return true;
		// display if user has linked Facebook account
		if (UserInfo.getFbUserId() != null) return true;
		return false;
	}
}
