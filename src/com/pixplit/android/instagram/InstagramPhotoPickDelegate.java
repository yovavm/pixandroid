package com.pixplit.android.instagram;

import com.pixplit.android.data.InstagramItem;

public interface InstagramPhotoPickDelegate {
	public void onPhotoPick(InstagramItem item);
}
