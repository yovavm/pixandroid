package com.pixplit.android.instagram;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.InstagramFollowPixplitHttpRequest;
import com.pixplit.android.instagram.InstaLoginDialog.InstaAuthListener;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;

public class InstagramSessionManager {
	
	public static void followOnInstaRequest(final Context context) {
		if (context == null) return;
		
		if (Utils.isValidString(UserInfo.getInstagramToken()) &&
				UserInfo.hasInstagramPermissions(PixHttpAPI.INSTAGRAM_SCOPE_FOLLOW)) {
			followOnInsta(context);
		} else {
			// authenticate with instagram first
			new InstaLoginDialog(context, new InstaAuthListener() {
				
				@Override
				public void onError(String error) {
					String failed = PixApp.getContext().getString(R.string.failed);
					PixProgressIndicator.showMessage(context, failed, true);
					PixProgressIndicator.hideMessageWithIndication(false, failed);
				}
				
				@Override
				public void onComplete() {
					followOnInsta(context);
				}
			},
			PixHttpAPI.INSTAGRAM_SCOPE_FOLLOW).show();
		}
	}
	
	private static void followOnInsta(final Context context) {
		RequestQueue queue = PixAppState.getState().getRequestQueue();
		PixProgressIndicator.showMessage(context, context.getString(R.string.follow_btn), true);
		queue.add(new InstagramFollowPixplitHttpRequest(new RequestDelegate() {
			
			@Override
			public void requestFinishedWithError(BaseVolleyHttpRequest request,
					PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, context.getString(R.string.failed));
			}
			
			@Override
			public void requestFinished(BaseVolleyHttpRequest request,
					PixObject response) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_ANY_FEED, 
						PixDefs.APP_EVENT_SUCCESSFUL_FOLLOW_ON_INSTAGRAM, 
						null);

				PixProgressIndicator.hideMessageWithIndication(true, context.getString(R.string.follow_btn));
			}
		}));
	}
}
