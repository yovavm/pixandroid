package com.pixplit.android.instagram;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;

public class InstaAuthWebViewClient extends WebViewClient {
	InstaAuthWebViewListener mListener;
	Context mContext;
	
	private final static String callbackUrl = PixHttpAPI.INSTAGRAM_REDIRECT_URI_VAL;
	private final static String tokenParam = PixHttpAPI.INSTAGRAM_TOKEN_PARAM;
	
	public InstaAuthWebViewClient(Context context, InstaAuthWebViewListener listener) {
		super();
		mContext = context;
		mListener = listener;
	}
	
	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {

		if (url.startsWith(callbackUrl)) {
			int tokenIndex = url.indexOf(tokenParam);
			if (tokenIndex > -1) {
				String token = url.substring(tokenIndex+tokenParam.length());
				if (Utils.isValidString(token)) {
					if (mListener != null) {
						mListener.onComplete(token);
					}
					PixProgressIndicator.hideMessageWithIndication(true, 
							PixApp.getContext().getString(R.string.done));
					return true;
				}
			}
			if (mListener != null) {
				mListener.onError("Missing/Invalid token on redirect URL");
			}
			PixProgressIndicator.hideMessage();
			return true;
		}
		return false;
	}

	@Override
	public void onPageFinished(WebView webView, String url) {
		super.onPageFinished(webView, url);
		String title = webView.getTitle();
		if (mListener != null) {
			mListener.updateTitle(title);
		}
		PixProgressIndicator.hideMessage();
	}

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		super.onPageStarted(view, url, favicon);
		PixProgressIndicator.showMessage(mContext, PixApp.getContext().getString(R.string.loading), true);
	}

	@Override
	public void onReceivedError(WebView view, int errorCode,
			String description, String failingUrl) {
		super.onReceivedError(view, errorCode, description, failingUrl);
		PixProgressIndicator.hideMessage();
		if (mListener != null) {
			mListener.onError(description);
		}
	}
}
