package com.pixplit.android.instagram;

public interface InstaAuthWebViewListener {
	public void onComplete(String token);
	public void onError(String error);
	public void updateTitle(String title);
}
