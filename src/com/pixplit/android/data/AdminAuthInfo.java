package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class AdminAuthInfo extends PixObject{
	private String adminToken;
	
	public AdminAuthInfo(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String adminToken = jsonObject.optString("adminToken", PixDefs.parsedStringFallback);
			if (adminToken.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			this.setAdminToken(adminToken);
			return true;
		}
		return false;
	}

	public String getAdminToken() {
		return adminToken;
	}

	public void setAdminToken(String adminToken) {
		this.adminToken = adminToken;
	}
}
