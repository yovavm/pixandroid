
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;


public class PixAccount{
	private String username;
	private String fullName;
	private int maxCampaigns;
	private int campaigns;
	
	public static PixAccount parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			int campaigns = jsonObject.optInt("campaigns", 0);
			int maxCampaigns = jsonObject.optInt("maxCampaigns", 0);
			
			// validations are done successfully. create the comment
			PixAccount eurekaAccount = new PixAccount();
			eurekaAccount.setUsername(username);
			eurekaAccount.setMaxCampaigns(maxCampaigns);
			eurekaAccount.setCampaigns(campaigns);
			eurekaAccount.setFullName(fullName);
			return eurekaAccount;
		}
		return null;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getMaxCampaigns() {
		return maxCampaigns;
	}

	public void setMaxCampaigns(int maxCampaigns) {
		this.maxCampaigns = maxCampaigns;
	}

	public int getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(int campaigns) {
		this.campaigns = campaigns;
	}
	
}
