package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixDeleteComp extends PixObject{
	private String compId;
	
	public PixDeleteComp(int code, String message) {
		super(code, message);
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String compId = jsonObject.optString("compId", PixDefs.parsedStringFallback);
			this.setCompId(compId);
			return true;
		}
		return false;
	}
	
	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}
}
