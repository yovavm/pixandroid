
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class FacebookPhoto{
	private String image;
	private String imageThumb;
	
	public static FacebookPhoto parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String thumbUrl = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(thumbUrl)) {
				return null;
			}
			String imageUrl = jsonObject.optString("source", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(imageUrl)) {
				return null;
			}
			
			// validations are done successfully. create the comment
			FacebookPhoto fbPhoto = new FacebookPhoto();
			fbPhoto.setImage(imageUrl);
			fbPhoto.setImageThumb(thumbUrl);
			return fbPhoto;
		}
		return null;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageThumb() {
		return imageThumb;
	}

	public void setImageThumb(String imageThumb) {
		this.imageThumb = imageThumb;
	}
	
}
