
package com.pixplit.android.data;

import org.json.JSONObject;

import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class PixChatUser{
	private String username;
	private String fullName;
	private String picture;


	public PixChatUser() {
		
	}
	
	public PixChatUser(PixProfile profile) {
		if (profile != null) {
			username = profile.getUsername();
			fullName = profile.getFullName();
			picture = profile.getPicture();
		}
	}
	
	public PixChatUser(PixUser pixUser) {
		if (pixUser != null) {
			username = pixUser.getUsername();
			fullName = pixUser.getFullName();
			picture = pixUser.getPicture();
		}
	}

	public static PixChatUser parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			
			PixChatUser user = new PixChatUser();
			user.setUsername(username);
			user.setFullName(fullName);
			user.setPicture(picture);
			return user;
		}
		return null;
	}
	
	public static final String PIX_CHAT_PARTICIPANT_KEY_EXIST = "PIX_CHAT_PARTICIPANT_%s_KEY_EXIST";
   	public static final String PIX_CHAT_PARTICIPANT_KEY_FULL_NAME = "PIX_CHAT_PARTICIPANT_%s_KEY_FULL_NAME";
   	public static final String PIX_CHAT_PARTICIPANT_KEY_PICTURE = "PIX_CHAT_PARTICIPANT_%s_KEY_PICTURE";
   	public static final String PIX_CHAT_PARTICIPANT_KEY_USER_NAME = "PIX_CHAT_PARTICIPANT_%s_KEY_USER_NAME";

   	public void serialize(Bundle outBundle, String postfix) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(String.format(PIX_CHAT_PARTICIPANT_KEY_EXIST, postfix), true);
   		if (Utils.isValidString(this.fullName)) {
   			outBundle.putString(String.format(PIX_CHAT_PARTICIPANT_KEY_FULL_NAME, postfix), this.fullName);
   		}
   		if (Utils.isValidString(this.picture)) {
   			outBundle.putString(String.format(PIX_CHAT_PARTICIPANT_KEY_PICTURE, postfix), this.picture);
   		}
   		if (Utils.isValidString(this.username)) {
   			outBundle.putString(String.format(PIX_CHAT_PARTICIPANT_KEY_USER_NAME, postfix), this.username);
   		}   		
   	}
   	
   	public static boolean hasChatParticipant(Bundle inBundle, String postfix) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(String.format(PIX_CHAT_PARTICIPANT_KEY_EXIST, postfix));
   	}
   	public static PixChatUser deserialize(Bundle inBundle, String postfix) {
   		if (!hasChatParticipant(inBundle, postfix)) return null;
   		PixChatUser chatUser = new PixChatUser();
   		chatUser.setFullName(inBundle.getString(String.format(PIX_CHAT_PARTICIPANT_KEY_FULL_NAME, postfix)));
   		chatUser.setPicture(inBundle.getString(String.format(PIX_CHAT_PARTICIPANT_KEY_PICTURE, postfix)));
   		chatUser.setUsername(inBundle.getString(String.format(PIX_CHAT_PARTICIPANT_KEY_USER_NAME, postfix)));
   		return chatUser;
   	}
   	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}		
}
