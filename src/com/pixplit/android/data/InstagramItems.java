package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class InstagramItems extends PixObjectsList<InstagramItem>{

	public InstagramItems(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if (super.parseJsonObject(jsonObject)) {
				// get the pagination (if exists) and use it as a next cursor
				JSONObject pagination = jsonObject.optJSONObject("pagination");
				if (pagination != null) {
					String nextUrl = pagination.optString("next_url", PixDefs.parsedStringFallback);
					if (Utils.isValidString(nextUrl)) {
						setNext(nextUrl);
					}
				}
				
				JSONArray items = jsonObject.optJSONArray("data");
				if (items != null && items.length() > 0) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						InstagramItem instaItem = InstagramItem.parseInstance(item);
						
						if (instaItem != null) {
							this.items.add(instaItem);
						}
					}
					return true;
				}
			}
		}
		return false;
	}
}