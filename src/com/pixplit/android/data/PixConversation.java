package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;

public class PixConversation extends PixObjectsList<PixChatMessage>{
//	private static final String TAG = "PixConversation";
	
	public static final String CONVERSATION_ID_NOT_FOUND = "-1";
	
	private String conversationId;
	private ArrayList<PixChatMessage> messages;
	private ArrayList<PixChatUser> users;
	
	public PixConversation(int code, String message) {
		super(code, message);
		messages = new ArrayList<PixChatMessage>();
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		boolean parseOk = false;
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				this.conversationId = jsonObject.optString("conversationId", PixDefs.parsedStringFallback);
				if (conversationId.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
					//Utils.pixLog(TAG, "invalid conversation id");
					return false;
				}
				
				JSONArray messagesArray = jsonObject.optJSONArray("messages");
				if (messagesArray != null) {
					for (int i=0; i<messagesArray.length(); i++) {
						JSONObject item = messagesArray.optJSONObject(i);
						PixChatMessage msg = PixChatMessage.parseInstance(item);
						if (msg != null) {
							this.messages.add(msg);
						}
					}
				}
				JSONArray usersArray = jsonObject.optJSONArray("users");
				users = new ArrayList<PixChatUser>();
				if (usersArray != null) {
					for (int i=0; i<usersArray.length(); i++) {
						JSONObject item = usersArray.optJSONObject(i);
						PixChatUser user = PixChatUser.parseInstance(item);
						if (user != null) {
							if (!user.getUsername().equalsIgnoreCase(PixAppState.UserInfo.getUserName())) {
								users.add(user);
							}
						}
					}
				}
				parseOk = true;
			}
		}
		
		return parseOk;
	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String id) {
		this.conversationId = id;
	}

	public ArrayList<PixChatMessage> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<PixChatMessage> messages) {
		this.messages = messages;
	}

	public ArrayList<PixChatUser> getUsers() {
		return users;
	}
}
