
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class FacebookAlbum{
	private String id;
	private String name;
	private String cover;
	private int size;
	
	public static FacebookAlbum parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(id)) {
				return null;
			}
			String name = jsonObject.optString("name", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(name)) {
				return null;
			}
			String cover = jsonObject.optString("cover_photo", PixDefs.parsedStringFallback);
			int size = jsonObject.optInt("count", -1);
			if (size <= 0) {
				return null;
			}

			FacebookAlbum fbAlbum = new FacebookAlbum();
			fbAlbum.setId(id);
			fbAlbum.setName(name);
			fbAlbum.setCover(cover);
			fbAlbum.setSize(size);
			return fbAlbum;
		}
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	
}
