package com.pixplit.android.data;

import org.json.JSONObject;

public class PixProfileCompositions extends PixCompositions{
	private PixProfile profile;
	
	public PixProfileCompositions(int code, String message) {
		super(code, message);
		// TODO Auto-generated constructor stub
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				JSONObject profileJson = jsonObject.optJSONObject("profile");
				PixProfile profile = PixProfile.parseInstance(profileJson);
				if (profile != null) {
					this.setProfile(profile);
					return true;
				}
			}
		}
		return false;
	}

	public PixProfile getProfile() {
		return profile;
	}

	public void setProfile(PixProfile profile) {
		this.profile = profile;
	}
}