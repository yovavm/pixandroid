package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixNotifications extends PixObjectsList<PixNotification>{

	public PixNotifications(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				
				JSONArray items = jsonObject.optJSONArray("notifications");
				
				if (items != null && (items.length() == this.getCount())) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixNotification notifications = PixNotification.parseInstance(item);
						
						if (notifications != null) {
							this.items.add(notifications);
						}
					}
					return true;
				}
			}
		}
		return false;
	}
}