
package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONObject;


public class PixObjectsList<T> extends PixObject{
	private int count;
   	private String next;
   	protected ArrayList<T> items;
   	
	public PixObjectsList(int code, String message) {
		super(code, message);
		items = new ArrayList<T>();
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			this.next = jsonObject.optString("next", null);
			this.count = jsonObject.optInt("count", 0);
			return true;
		}
		return false;
	}
	
    public int getCount(){
		return this.count;
	}
	
	public String getNext(){
		return this.next;
	}
	
	public void setNext(String next) {
		this.next = next;
	}

	public ArrayList<T> getItems() {
		return items;
	}	
}