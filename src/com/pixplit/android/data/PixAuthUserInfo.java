package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixAuthUserInfo extends PixObject{
	private String username;
	private String fullName;
	private String displayName;
	private String sessionToken;
	
	public PixAuthUserInfo(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			String displayName = jsonObject.optString("displayName", PixDefs.parsedStringFallback);
			String sessionToken = jsonObject.optString("sessionToken", PixDefs.parsedStringFallback);
			if (sessionToken.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			
			this.setUsername(username);
			this.setFullName(fullName);
			this.setDisplayName(displayName);
			this.setSessionToken(sessionToken);
			return true;
		}
		return false;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

}
