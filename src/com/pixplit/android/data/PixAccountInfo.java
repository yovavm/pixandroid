package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixAccountInfo extends PixObject{
	private String fullName;
	private String displayName;
	private String email;
	private String picture;
	private int pixAccount;
	
	public PixAccountInfo(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			String displayName = jsonObject.optString("displayName", PixDefs.parsedStringFallback);
			if (displayName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			String email = jsonObject.optString("email", PixDefs.parsedStringFallback);
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			int pixAccount = jsonObject.optInt("pixAccount", PixDefs.parsedIntFallback);
			// done parsing successfully
			this.setFullName(fullName);
			this.setDisplayName(displayName);
			this.setEmail(email);
			this.setPicture(picture);
			this.setPixAccount(pixAccount);
			return true;
		}
		return false;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getPixAccount() {
		return pixAccount;
	}
	public void setPixAccount(int pixAccount) {
		this.pixAccount = pixAccount;
	}

}
