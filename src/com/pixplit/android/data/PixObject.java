
package com.pixplit.android.data;


public class PixObject{
	
	public PixObject(int code, String message) {
		this.status = code;
		this.message = message;
	}
	
   	private String message;
   	private int status;
   	
 	public String getMessage(){
		return this.message;
	}
 	public int getStatus(){
		return this.status;
	}
}
