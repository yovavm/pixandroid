package com.pixplit.android.data;

import android.graphics.Bitmap;

public class PixEditAccountInfo {
	private String fullName;
	private String email;
	private int delimg;
	private Bitmap img;
	private String pwd;
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getDelimg() {
		return delimg;
	}
	public void setDelimg(int delimg) {
		this.delimg = delimg;
	}
	public Bitmap getImg() {
		return img;
	}
	public void setImg(Bitmap img) {
		this.img = img;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
