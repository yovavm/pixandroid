package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixCompositions extends PixObjectsList<PixComposition>{
	private ArrayList<PixComposition> featured;
	
	public PixCompositions(int code, String message) {
		super(code, message);
		this.featured = new ArrayList<PixComposition>();
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				
				// look for 'featured' block - optional
				JSONArray featuredItems = jsonObject.optJSONArray("featured");
				if (featuredItems != null) {
					for (int i=0; i<featuredItems.length(); i++) {
						JSONObject featuredItem = featuredItems.optJSONObject(i);
						PixComposition composition = PixComposition.parseInstance(featuredItem);
						if (composition != null) {
							composition.setFeatured(true);
							this.featured.add(composition);
						}
					}
				}
				
				JSONArray items = jsonObject.optJSONArray("comps");
				if (items != null && (items.length() == this.getCount())) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixComposition composition = PixComposition.parseInstance(item);
						
						if (composition != null) {
							this.items.add(composition);
						}
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasFeatured() {
		return featured != null && featured.size() > 0;
	}
	public ArrayList<PixComposition> getFeatured() {
		return this.featured;
	}
}