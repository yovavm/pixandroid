
package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixAccounts extends PixObjectsList<PixAccount>{
	private int count;

	public PixAccounts(int code, String message) {
		super(code, message);
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				count = jsonObject.optInt("count", 0);
				JSONArray items = jsonObject.optJSONArray("accounts");
				this.setCount(count);
				if (items != null && (items.length() == this.getCount())) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixAccount account = PixAccount.parseInstance(item);
						
						if (account != null) {
							this.items.add(account);
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	
 	public int getCount(){
		return this.count;
	}
	public void setCount(int count){
		this.count = count;
	}
}
