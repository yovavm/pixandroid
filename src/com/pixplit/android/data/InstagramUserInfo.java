package com.pixplit.android.data;

import org.json.JSONObject;

public class InstagramUserInfo extends PixObject{
	private String userName;
	private String fullName;
	
	public InstagramUserInfo(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			JSONObject data = jsonObject.optJSONObject("data");
			if (data == null) return false;
			
			this.setUserName(data.optString("username", ""));
			this.setFullName(data.optString("full_name", ""));
			return true;
		}
		return false;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}