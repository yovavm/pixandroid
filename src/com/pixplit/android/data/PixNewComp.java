package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixNewComp extends PixObject{
	private String compId;
	private String users;
	
	public PixNewComp(int code, String message) {
		super(code, message);
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String compId = jsonObject.optString("compId", PixDefs.parsedStringFallback);
			if (compId.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			String users = jsonObject.optString("users", PixDefs.parsedStringFallback);
			
			this.setCompId(compId);
			this.setUsers(users);
			return true;
		}
		return false;
	}
	
	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}
	
}
