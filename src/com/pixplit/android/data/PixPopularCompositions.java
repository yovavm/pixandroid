package com.pixplit.android.data;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pixplit.android.util.Utils;

public class PixPopularCompositions extends PixCompositions{
	private HashMap<String, Boolean> active;
	
	public PixPopularCompositions(int code, String message) {
		super(code, message);
		active = new HashMap<String, Boolean>();
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				JSONArray activeJson = jsonObject.optJSONArray("active");
				if (activeJson != null) {
					for (int i=0; i<activeJson.length(); i++) {
						String compId = null;
						try {
							compId = activeJson.getString(i);
						} catch (JSONException e) {
							e.printStackTrace();
							// failed to parse active comps.
							return false;
						}
						if (Utils.isValidString(compId)) {
							active.put(compId, true);
						}
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public HashMap<String, Boolean> getActive() {
		return active;
	}
}