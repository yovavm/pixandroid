
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class PixComment{
	private String username;
	private String fullName;
	private String picture;
	private String date;
	private String comment;
	private String id;
	
	public static PixComment parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			if (picture.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String date = jsonObject.optString("date", PixDefs.parsedStringFallback);
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			String comment = jsonObject.optString("comment", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(comment)) {
				return null;
			}
			
			// validations are done successfully. create the comment
			PixComment pixComment = new PixComment();
			pixComment.setUsername(username);
			pixComment.setFullName(fullName);
			pixComment.setPicture(picture);
			pixComment.setDate(date);
			pixComment.setComment(comment);
			pixComment.setId(id);
			return pixComment;
		}
		return null;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
