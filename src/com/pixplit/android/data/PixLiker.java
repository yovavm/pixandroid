
package com.pixplit.android.data;

import org.json.JSONObject;

import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class PixLiker{
   	private String fullName;
   	private String username;

   	public static PixLiker parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			
			// validations are done successfully. create the liker
			PixLiker liker = new PixLiker();
			liker.setFullName(fullName);
			liker.setUsername(username);
			return liker;
		}
		return null;
   	}
   	
   	public static final String PIX_LIKER_KEY_EXIST = "PIX_LIKER_KEY_EXIST";
   	public static final String PIX_LIKER_KEY_FULL_NAME = "PIX_LIKER_KEY_FULL_NAME";
   	public static final String PIX_LIKER_KEY_USER_NAME = "PIX_LIKER_KEY_USER_NAME";
   	
   	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(PIX_LIKER_KEY_EXIST, true);
   		if (Utils.isValidString(this.fullName)) {
   			outBundle.putString(PIX_LIKER_KEY_FULL_NAME, this.fullName);
   		}
   		if (Utils.isValidString(this.username)) {
   			outBundle.putString(PIX_LIKER_KEY_USER_NAME, this.username);
   		}
   	}
   	public static boolean hasLiker(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(PIX_LIKER_KEY_EXIST);
   	}
   	public static PixLiker deserialize(Bundle inBundle) {
   		if (!hasLiker(inBundle)) return null;
   		PixLiker liker = new PixLiker();
   		liker.setFullName(inBundle.getString(PIX_LIKER_KEY_FULL_NAME));
   		liker.setUsername(inBundle.getString(PIX_LIKER_KEY_USER_NAME));
   		return liker;
   	}
   	
 	public String getFullName(){
		return this.fullName;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}
 	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}
}
