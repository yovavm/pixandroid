package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixConversations extends PixObjectsList<PixConversationPreview> {
	private int conversationsCount;
   	private ArrayList<PixConversationPreview> conversations;
   	private JSONObject unreadStatuses;
   	
	public PixConversations(int code, String message) {
		super(code, message);
		conversations = new ArrayList<PixConversationPreview>();
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				this.conversationsCount = jsonObject.optInt("conversationsCount", 0);
				this.unreadStatuses = jsonObject.optJSONObject("unreadStatuses");
				
				JSONArray conversations = jsonObject.optJSONArray("conversations");
				
				if (conversations != null) {
					for (int i=0; i<conversations.length(); i++) {
						JSONObject item = conversations.optJSONObject(i);
						PixConversationPreview conversation = PixConversationPreview.parseInstance(item);
						if (conversation != null) {
							boolean read = true;
							if (this.unreadStatuses != null) {
								int unread = unreadStatuses.optInt(conversation.getId(), 0);
								if (unread == 1) {
									read = false;
								}
							}
							conversation.setReadStatus(read);
							this.conversations.add(conversation);
						}
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public int getConversationsCount() {
		return conversationsCount;
	}
	
	public ArrayList<PixConversationPreview> getConversations() {
		return conversations;
	}

}
