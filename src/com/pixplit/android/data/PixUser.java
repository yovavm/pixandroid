
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;


public class PixUser{
	private String username;
	private String fullName;
	private String picture;
	private int followStatus;
	private boolean selected = false; // default;

	public static final int FOLLOW_STATUS_LOGGED_IN_USER = -1;
	public static final int FOLLOW_STATUS_NOT_FOLLOWING = 0;
	public static final int FOLLOW_STATUS_FOLLOWING = 1;

	public PixUser() {
		
	}
	
	public PixUser(PixProfile profile) {
		if (profile != null) {
			username = profile.getUsername();
			fullName = profile.getFullName();
			picture = profile.getPicture();
			followStatus = profile.getIsFollowing();
		}
	}

	public static PixUser parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			int followStatus = jsonObject.optInt("followStatus", 0);
			
			PixUser user = new PixUser();
			user.setUsername(username);
			user.setFullName(fullName);
			user.setPicture(picture);
			user.setFollowStatus(followStatus);
			return user;
		}
		return null;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getFollowStatus() {
		return followStatus;
	}
	public void setFollowStatus(int followStatus) {
		this.followStatus = followStatus;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
		
}
