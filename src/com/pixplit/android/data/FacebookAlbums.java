package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class FacebookAlbums extends PixObjectsList<FacebookAlbum>{

	public FacebookAlbums(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if (super.parseJsonObject(jsonObject)) {
				// get the pagination (if exists) and use it as a next cursor
				JSONObject paging = jsonObject.optJSONObject("paging");
				if (paging != null) {
					String nextUrl = paging.optString("next", PixDefs.parsedStringFallback);
					if (Utils.isValidString(nextUrl)) {
						setNext(nextUrl);
					}
				}
				
				JSONArray items = jsonObject.optJSONArray("data");
				if (items != null && items.length() > 0) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						FacebookAlbum fbAlbum = FacebookAlbum.parseInstance(item);
						
						if (fbAlbum != null) {
							this.items.add(fbAlbum);
						}
					}
				}
				return true;
			}
		}
		return false;
	}
}