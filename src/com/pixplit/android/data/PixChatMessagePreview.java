package com.pixplit.android.data;

import java.text.ParseException;
import java.util.Date;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;

public class PixChatMessagePreview {
//	private static final String TAG = "PixChatMessage";
	
	private String id;
	private int type;
	private String text;
	private String sender;
	private PixComposition comp;
	private Date date; // message date
	
	public static PixChatMessagePreview parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			if (id.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid message id");
				return null;
			}
			Date date = null;
			String dateStr = jsonObject.optString("date", PixDefs.parsedStringFallback);
			if (Utils.isValidString(dateStr)) {
				dateStr = dateStr.replace(' ', '+');
				try {
					date = PixDateFormat.getPixDateInstance().parse(dateStr);
				} catch (ParseException e) {
					e.printStackTrace();
					date = null;
				}
			}
			int type = jsonObject.optInt("type", PixDefs.parsedIntFallback);
			String text = null;
			switch (type) {
			case PixChatMessage.PIX_MESSAGE_TYPE_TEXT:
				text = jsonObject.optString("text", PixDefs.parsedStringFallback);
				if (text.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
					//Utils.pixLog(TAG, "invalid/missing text in text message");
					return null;
				}
				break;
			case PixChatMessage.PIX_MESSAGE_TYPE_COMP:
				// do nothing. not composition in message preview
				break;
			default:
				//Utils.pixLog(TAG, "invalid message type");
				return null;
			}

			String sender = jsonObject.optString("sender", PixDefs.parsedStringFallback);
			if (sender.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid/missing lastMessageBy");
				return null;
			}
			
			// validations are done successfully. create the conversation
			PixChatMessagePreview conversation = new PixChatMessagePreview();
			conversation.setId(id);
			
			conversation.setDate(date);
			conversation.setType(type);
			if (type == PixChatMessage.PIX_MESSAGE_TYPE_TEXT) {
				conversation.setText(text);
			}
			conversation.setSender(sender);
			return conversation;
		}
		
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public PixComposition getComp() {
		return comp;
	}

	public void setComp(PixComposition comp) {
		this.comp = comp;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
