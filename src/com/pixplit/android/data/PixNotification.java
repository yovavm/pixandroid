
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class PixNotification{
	public static final int NOTIFICATION_STATE_UNKNOWN = 0;
	public static final int NOTIFICATION_IS_NEW = 1;
	public static final int NOTIFICATION_IS_OLD = 2;
	
	private String id;
	private String notificationType;
	private String username;
	private String fullName;
	private String picture;
	private String insertDate;
	private String date;
	private String comment;
	private String compId;
	private String compUrl;
	
	private int state = NOTIFICATION_STATE_UNKNOWN;
	
	public static PixNotification parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			if (id.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String notificationType = jsonObject.optString("notificationType", PixDefs.parsedStringFallback);
			if (notificationType.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			String insertDate = jsonObject.optString("insertDate", PixDefs.parsedStringFallback);
			if (insertDate.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String date = jsonObject.optString("date", PixDefs.parsedStringFallback);
			if (date.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String comment = jsonObject.optString("comment", PixDefs.parsedStringFallback);
			if (Utils.isValidString(comment) && 
					!Utils.isValidString(comment)) {
				// the notification had a comment and the comment contained emoji only, 
				// remove this notification
				return null;
			}
			String compId = jsonObject.optString("compId", PixDefs.parsedStringFallback);
			String compUrl = jsonObject.optString("compUrl", PixDefs.parsedStringFallback);
			
			// validations are done successfully. create the notification
			PixNotification notification = new PixNotification();
			notification.setId(id);
			notification.setNotificationType(notificationType);
			notification.setUsername(username);
			notification.setFullName(fullName);
			notification.setPicture(picture);
			notification.setInsertDate(insertDate);
			notification.setDate(date);
			notification.setComment(comment);
			notification.setCompId(compId);
			notification.setCompUrl(compUrl);
			return notification;
		}
		return null;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getCompId() {
		return compId;
	}
	public void setCompId(String compId) {
		this.compId = compId;
	}
	public String getCompUrl() {
		return compUrl;
	}
	public void setCompUrl(String compUrl) {
		this.compUrl = compUrl;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
}
