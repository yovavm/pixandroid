package com.pixplit.android.data;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;

import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class PixCompositionMetadata {

   	private int actionId;
   	private String rootId;
   	private String parentId;
   	private PixCompositionFramesetType compType;
   	private boolean isCompleted;
   	private int newComponentIndex;
   	private String title;
   	private String tags;
   	private String location;
   	private boolean hasBorder;
   	private String reservedCompId;
   	private String subtitle;
   	private String colorScheme;
   	private Bitmap compositionImage;
   	
   	private String inviteesList;
   	private String shareToEmailAddress;
   	private boolean shareWithFacebook;
   	private boolean shareWithTwitter;
   	
   	private PixPrivateCompositionMetadata privateCompMetadata;
   	
   	public PixCompositionMetadata() {}  // empty constructor for serialization purpose
   	
   	public PixCompositionMetadata(PixCompositionFramesetType compType, PixComposition joinedComp) {
   		this.setCompType(compType);
   		this.newComponentIndex = 0;
   		this.location = null;
   		this.reservedCompId = PixDefs.defaultCompId;
   		this.setCompleted(false);
   		this.privateCompMetadata = null;
   		if (joinedComp != null) {
   			this.setActionId(joinedComp.getActionId());
   			this.setRootId(joinedComp.getRootId());
   			this.setParentId(joinedComp.getCompId());
   			this.setTitle(joinedComp.getTitle());
   			if (joinedComp.getTags() != null) {
   				this.setTags(TextUtils.join(",",joinedComp.getTags()));
   			}
   			if (joinedComp.getHasBorder() == 1) {
   	   			this.setHasBorder(true);   				
   			} else {
   				this.setHasBorder(false);
   			}
   			this.subtitle = joinedComp.getSubtitle();
   			this.colorScheme = joinedComp.getColorScheme();
   		} else {
   			this.setActionId(PixDefs.defaultActionId);
   			this.setRootId(PixDefs.defaultRootId);
   			this.setParentId(PixDefs.defaultParentId);
   			this.setTitle(null);
   			this.setTags("");
   			this.setHasBorder(false);
   			this.subtitle = "";
   			this.colorScheme = PixDefs.defaultColorSchemeString;
   		}
   	}

   	///////////  Serialization stuff  ////////////////////
	private static final String COMP_METADATA_KEY_COMP_METADATA = "COMP_METADATA_KEY_COMP_METADATA";
   	private static final String COMP_METADATA_KEY_ACTION_ID = "COMP_METADATA_KEY_ACTION_ID";
	private static final String COMP_METADATA_KEY_ROOT_ID = "COMP_METADATA_KEY_ROOT_ID";
	private static final String COMP_METADATA_KEY_PARENT_ID = "COMP_METADATA_KEY_PARENT_ID";
	private static final String COMP_METADATA_KEY_FRAMESET_TYPE = "COMP_METADATA_KEY_FRAMESET_TYPE";
   	private static final String COMP_METADATA_KEY_IS_COMPLETED = "COMP_METADATA_KEY_IS_COMPLETED";
   	private static final String COMP_METADATA_KEY_NEW_PART_INDEX = "COMP_METADATA_KEY_NEW_PART_INDEX";
	private static final String COMP_METADATA_KEY_TITLE = "COMP_METADATA_KEY_TITLE";
	private static final String COMP_METADATA_TAGS = "COMP_METADATA_TAGS";
	private static final String COMP_METADATA_LOCATION = "COMP_METADATA_LOCATION";
   	private static final String COMP_METADATA_KEY_HAS_BORDER = "COMP_METADATA_KEY_HAS_BORDER";
	private static final String COMP_METADATA_RESERVED_ID = "COMP_METADATA_RESERVED_ID";
	private static final String COMP_METADATA_INVITEE = "COMP_METADATA_INVITEE";
	private static final String COMP_METADATA_KEY_SHARE_EMAIL = "COMP_METADATA_KEY_SHARE_EMAIL";
	private static final String COMP_METADATA_KEY_SHARE_FB = "COMP_METADATA_KEY_SHARE_FB";
	private static final String COMP_METADATA_KEY_SHARE_TWITTER = "COMP_METADATA_KEY_SHARE_TWITTER";
   	private static final String COMP_METADATA_KEY_IMAGE_PATH = "COMP_METADATA_KEY_IMAGE_PATH";
   	private static final String COMP_METADATA_KEY_SUBTITLE = "COMP_METADATA_KEY_SUBTITLE";
   	private static final String COMP_METADATA_KEY_COLOR_SCHEME = "COMP_METADATA_KEY_COLOR_SCHEME";

   	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(COMP_METADATA_KEY_COMP_METADATA, true); // indicates that the bundle holds a composition
   		outBundle.putInt(COMP_METADATA_KEY_ACTION_ID, this.actionId);
   		if (Utils.isValidString(this.rootId)) {
   			outBundle.putString(COMP_METADATA_KEY_ROOT_ID, this.rootId);
   		}
   		if (Utils.isValidString(this.parentId)) {
   			outBundle.putString(COMP_METADATA_KEY_PARENT_ID, this.parentId);
   		}
   		if (this.compType != null) {
   			outBundle.putInt(COMP_METADATA_KEY_FRAMESET_TYPE, PixCompositionFramesetType.getInt(this.compType));
   		}
   		outBundle.putBoolean(COMP_METADATA_KEY_IS_COMPLETED, this.isCompleted);
   		outBundle.putInt(COMP_METADATA_KEY_NEW_PART_INDEX, this.newComponentIndex);
   		if (Utils.isValidString(this.title)) {
   			outBundle.putString(COMP_METADATA_KEY_TITLE, this.title);
   		}
   		if (Utils.isValidString(this.tags)) {
   			outBundle.putString(COMP_METADATA_TAGS, this.tags);
   		}
   		if (Utils.isValidString(this.location)) {
   			outBundle.putString(COMP_METADATA_LOCATION, this.location);
   		}
   		outBundle.putBoolean(COMP_METADATA_KEY_HAS_BORDER, this.hasBorder);
   		if (Utils.isValidString(this.reservedCompId)) {
   			outBundle.putString(COMP_METADATA_RESERVED_ID, this.reservedCompId);
   		}
   		if (Utils.isValidString(this.inviteesList)) {
   			outBundle.putString(COMP_METADATA_INVITEE, this.inviteesList);
   		}
   		if (Utils.isValidString(this.shareToEmailAddress)) {
   			outBundle.putString(COMP_METADATA_KEY_SHARE_EMAIL, this.shareToEmailAddress);
   		}
		outBundle.putBoolean(COMP_METADATA_KEY_SHARE_FB, this.shareWithFacebook);
		outBundle.putBoolean(COMP_METADATA_KEY_SHARE_TWITTER, this.shareWithTwitter);
		if (this.compositionImage != null) {
   			String filePath = Utils.storeBitmapInTmpFile(this.compositionImage, 
   					COMP_METADATA_KEY_IMAGE_PATH);
   			if (Utils.isValidString(filePath)) {
   				outBundle.putString(COMP_METADATA_KEY_IMAGE_PATH, filePath);
   			}
   		}
		if (this.privateCompMetadata != null) {
			this.privateCompMetadata.serialize(outBundle);
		}
		if (Utils.isValidString(this.subtitle)) {
   			outBundle.putString(COMP_METADATA_KEY_SUBTITLE, this.subtitle);
   		}
		if (Utils.isValidString(this.colorScheme)) {
   			outBundle.putString(COMP_METADATA_KEY_COLOR_SCHEME, this.colorScheme);
   		}
   	}
   	
   	public static boolean hasCompMetadata(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(COMP_METADATA_KEY_COMP_METADATA);
   	}
   	
   	public static PixCompositionMetadata deserialize(Bundle inBundle) {
   		if (!hasCompMetadata(inBundle)) return null;
   		PixCompositionMetadata compMetadata = new PixCompositionMetadata();
   		compMetadata.setActionId(inBundle.getInt(COMP_METADATA_KEY_ACTION_ID));
   		compMetadata.setRootId(inBundle.getString(COMP_METADATA_KEY_ROOT_ID));
   		compMetadata.setParentId(inBundle.getString(COMP_METADATA_KEY_PARENT_ID));
   		compMetadata.compType = PixCompositionFramesetType.getType
   				(inBundle.getInt(COMP_METADATA_KEY_FRAMESET_TYPE));
   		compMetadata.setCompleted(inBundle.getBoolean(COMP_METADATA_KEY_IS_COMPLETED));
   		compMetadata.setNewComponentIndex(inBundle.getInt(COMP_METADATA_KEY_NEW_PART_INDEX));
   		compMetadata.setTitle(inBundle.getString(COMP_METADATA_KEY_TITLE));
   		compMetadata.setTags(inBundle.getString(COMP_METADATA_TAGS));
   		compMetadata.setLocation(inBundle.getString(COMP_METADATA_LOCATION));
   		compMetadata.setHasBorder(inBundle.getBoolean(COMP_METADATA_KEY_HAS_BORDER));
   		compMetadata.setReservedCompId(inBundle.getString(COMP_METADATA_RESERVED_ID));
   		compMetadata.setInviteesList(inBundle.getString(COMP_METADATA_INVITEE));
   		compMetadata.setShareToEmailAddress(inBundle.getString(COMP_METADATA_KEY_SHARE_EMAIL));
   		compMetadata.setShareWithFacebook(inBundle.getBoolean(COMP_METADATA_KEY_SHARE_FB));
   		compMetadata.setShareWithTwitter(inBundle.getBoolean(COMP_METADATA_KEY_SHARE_TWITTER));
   		String filePath = inBundle.getString(COMP_METADATA_KEY_IMAGE_PATH);
   		if (Utils.isValidString(filePath)) {
   			compMetadata.compositionImage = Utils.getBitmapFromFile(filePath, 0, 0);
   			if (compMetadata.compositionImage != null) {
   				Utils.deleteBitmapFile(filePath);
   			}
   		}
   		compMetadata.setPrivateCompMetadata(PixPrivateCompositionMetadata.deserialize(inBundle));
   		compMetadata.setSubtitle(inBundle.getString(COMP_METADATA_KEY_SUBTITLE));
   		compMetadata.setColorScheme(inBundle.getString(COMP_METADATA_KEY_COLOR_SCHEME));
   		return compMetadata;
   	}
	//////////////////////////////////
	
	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String getRootId() {
		return rootId;
	}

	public void setRootId(String rootId) {
		this.rootId = rootId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public PixCompositionFramesetType getCompType() {
		return compType;
	}

	public void setCompType(PixCompositionFramesetType compType) {
		this.compType = compType;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public int getNewComponentIndex() {
		return newComponentIndex;
	}

	public void setNewComponentIndex(int newComponentIndex) {
		this.newComponentIndex = newComponentIndex;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean hasBorder() {
		return hasBorder;
	}

	public void setHasBorder(boolean hasBorder) {
		this.hasBorder = hasBorder;
	}

	public String getReservedCompId() {
		return reservedCompId;
	}

	public void setReservedCompId(String reservedCompId) {
		this.reservedCompId = reservedCompId;
	}

	public Bitmap getCompositionImage() {
		return compositionImage;
	}

	public void setCompositionImage(Bitmap compositionImage) {
		this.compositionImage = compositionImage;
	}

	public String getInviteesList() {
		return inviteesList;
	}

	public void setInviteesList(String inviteesList) {
		this.inviteesList = inviteesList;
	}

	public String getShareToEmailAddress() {
		return shareToEmailAddress;
	}

	public void setShareToEmailAddress(String shareToEmailAddress) {
		this.shareToEmailAddress = shareToEmailAddress;
	}

	public boolean getShareWithFacebook() {
		return shareWithFacebook;
	}

	public void setShareWithFacebook(boolean shareWithFacebook) {
		this.shareWithFacebook = shareWithFacebook;
	}

	public boolean getShareWithTwitter() {
		return shareWithTwitter;
	}

	public void setShareWithTwitter(boolean shareWithTwitter) {
		this.shareWithTwitter = shareWithTwitter;
	}

	public PixPrivateCompositionMetadata getPrivateCompMetadata() {
		return privateCompMetadata;
	}

	public void setPrivateCompMetadata(PixPrivateCompositionMetadata privateCompMetadata) {
		this.privateCompMetadata = privateCompMetadata;
	}
	
	public boolean isPrivate() {
		return (privateCompMetadata != null);
	}
	
	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getColorScheme() {
		return colorScheme;
	}

	public void setColorScheme(String colorScheme) {
		this.colorScheme = colorScheme;
	}

}
