package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;

public class PixConversationPreview {
//	private static final String TAG = "PixConversationPreview";
	private String id;
	private PixChatMessagePreview lastMessage;
	private ArrayList<PixChatUser> users;
	private boolean readStatus;
	
	public static PixConversationPreview parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			if (id.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid conversation id");
				return null;
			}
			
			JSONObject lastMessageObj = jsonObject.optJSONObject("lastMessage");
			if (lastMessageObj == null) {
				//Utils.pixLog(TAG, "last message info is missing in conversation:"+ id);
				return null;
			}
			PixChatMessagePreview lastMessage = PixChatMessagePreview.parseInstance(lastMessageObj);
			if (lastMessage == null) {
				//Utils.pixLog(TAG, "invalid/missing last message in conversation:"+ id);
				return null;
			}			
			int usersCount = jsonObject.optInt("usersCount", PixDefs.parsedIntFallback);
			
			JSONArray usersArray = jsonObject.optJSONArray("users");
			if (usersArray == null) {
				//Utils.pixLog(TAG, "invalid/missing users in conversation:"+ id);
				return null;
			}
			ArrayList<PixChatUser> users = new ArrayList<PixChatUser>();
			for (int i=0; i<usersArray.length(); i++) {
				JSONObject item = usersArray.optJSONObject(i);
				PixChatUser user = PixChatUser.parseInstance(item);
				if (user != null) {
					if (user.getUsername().equalsIgnoreCase(PixAppState.UserInfo.getUserName())) {
						if (usersCount != PixDefs.parsedIntFallback) {
							usersCount--; // remove self from count as self user is not added to the list
						}
					} else {
						users.add(user);
					}
				}
			}
			if (users.size() == 0) {
				//Utils.pixLog(TAG, "no users in conversation in conversation:"+ id);
				return null;
			}
			
			// verify the number of users against the count
			if (usersCount == PixDefs.parsedIntFallback ||
					usersCount != users.size()) {
				//Utils.pixLog(TAG, String.format("invalid/wrong users num. usersCount:%d, users num:%d", usersCount, users.size()));
				return null;
			}
			// validations are done successfully. create the conversation
			PixConversationPreview conversation = new PixConversationPreview();
			conversation.setId(id);
			conversation.setUsers(users);
			conversation.setLastMessage(lastMessage);
			return conversation;
		}
		
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<PixChatUser> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<PixChatUser> users) {
		this.users = users;
	}

	public boolean isRead() {
		return readStatus;
	}

	public void setReadStatus(boolean readStatus) {
		this.readStatus = readStatus;
	}

	public PixChatMessagePreview getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(PixChatMessagePreview lastMessage) {
		this.lastMessage = lastMessage;
	}

	
}
