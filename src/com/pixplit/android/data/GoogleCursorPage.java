
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class GoogleCursorPage{
	private String start;
	
	public static GoogleCursorPage parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String start = jsonObject.optString("start", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(start)) {
				return null;
			}

			GoogleCursorPage cursorPage = new GoogleCursorPage();
			cursorPage.setStart(start);
			return cursorPage;
		}
		return null;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
}
