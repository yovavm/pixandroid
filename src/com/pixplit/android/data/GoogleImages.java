package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class GoogleImages extends PixObjectsList<GoogleImage>{
	public GoogleImages(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if (super.parseJsonObject(jsonObject)) {
				JSONObject cursorJson = jsonObject.optJSONObject("cursor");
				if (cursorJson != null) {
					GoogleCursor cursor = GoogleCursor.parseInstance(cursorJson);
					if (cursor != null) {
						String nextStart = cursor.getNextStart();
						setNext(nextStart);
					}
				}
				
				JSONArray items = jsonObject.optJSONArray("results");
				if (items != null) {
					if (items.length() > 0) {
						for (int i=0; i<items.length(); i++) {
							JSONObject item = items.optJSONObject(i);
							GoogleImage googleImage = GoogleImage.parseInstance(item);
							
							if (googleImage != null) {
								this.items.add(googleImage);
							}
						}
					}
					return true;
				}
			}
		}
		return false;
	}
}