package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixProfile{
	private String username;
	private String displayName;
	private String fullName;
	private String picture;
	private int coverId;
	private int isPrivate;
	private int splits;
	private int following;
	private int followers;
	private int isFollowing;
	
	public static PixProfile parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String displayName = jsonObject.optString("displayName", PixDefs.parsedStringFallback);
			if (displayName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			if (fullName.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			if (picture.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			int coverId = jsonObject.optInt("coverId", 1);
			int isPrivate = jsonObject.optInt("isPrivate", PixDefs.parsedIntFallback);
			int splits = jsonObject.optInt("splits", 0);
			int following = jsonObject.optInt("following", 0);
			int followers = jsonObject.optInt("followers", 0);
			int isFollowing = jsonObject.optInt("isFollowing", PixDefs.parsedIntFallback);
			
			// validations are done successfully. create the profile
			PixProfile profile = new PixProfile();
			profile.setUsername(username);
			profile.setDisplayName(displayName);
			profile.setFullName(fullName);
			profile.setPicture(picture);
			profile.setCoverId(coverId);
			profile.setIsPrivate(isPrivate);
			profile.setSplits(splits);
			profile.setFollowing(following);
			profile.setFollowers(followers);
			profile.setIsFollowing(isFollowing);
			return profile;
		}
		return null;
	}
	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}
	public String getDisplayName(){
		return this.displayName;
	}
	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}
	public String getFullName(){
		return this.fullName;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}
	public String getPicture(){
		return this.picture;
	}
	public void setPicture(String picture){
		this.picture = picture;
	}
	public int getCoverId(){
		return this.coverId;
	}
	public void setCoverId(int coverId){
		this.coverId = coverId;
	}
	public int getIsPrivate(){
		return this.isPrivate;
	}
	public void setIsPrivate(int isPrivate){
		this.isPrivate = isPrivate;
	}
	public int getSplits(){
		return this.splits;
	}
	public void setSplits(int splits){
		this.splits = splits;
	}
	public int getFollowing(){
		return this.following;
	}
	public void setFollowing(int following){
		this.following = following;
	}
	public int getFollowers(){
		return this.followers;
	}
	public void setFollowers(int followers){
		this.followers = followers;
	}
	public int getIsFollowing(){
		return this.isFollowing;
	}
	public void setIsFollowing(int isFollowing){
		this.isFollowing = isFollowing;
	}
}
