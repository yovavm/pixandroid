package com.pixplit.android.data;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import org.json.JSONObject;

import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;

public class PixChatMessage {
//	private static final String TAG = "PixChatMessage";
	
	public static final int PIX_MESSAGE_TYPE_TEXT = 1000;
	public static final int PIX_MESSAGE_TYPE_COMP = 1001;
	
	public static final int PIX_MESSAGE_SENT_STATE_NONE = 0;
	public static final int PIX_MESSAGE_SENT_STATE_SENDING = 1;
	public static final int PIX_MESSAGE_SENT_STATE_SUCCESS = 2;
	public static final int PIX_MESSAGE_SENT_STATE_FAIL = 3;
	
	private int sentState = PIX_MESSAGE_SENT_STATE_NONE;
	private String id;
	private int type;
	private String text;
	private String sender;
	private PixComposition comp;
	private Date date; // message date
	private Date nextDate;  // next message date - needed for display

	public static PixChatMessage parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String id = jsonObject.optString("id", PixDefs.parsedStringFallback);
			if (id.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid message id");
				return null;
			}
			Date date = null;
			String dateStr = jsonObject.optString("date", PixDefs.parsedStringFallback);
			if (Utils.isValidString(dateStr)) {
				try {
					date = PixDateFormat.getPixDateInstance().parse(dateStr);
				} catch (ParseException e) {
					e.printStackTrace();
					date = null;
				}
			}
			int type = jsonObject.optInt("type", PixDefs.parsedIntFallback);
			String text = null;
			PixComposition composition = null;
			switch (type) {
			case PIX_MESSAGE_TYPE_TEXT:
				text = jsonObject.optString("text", PixDefs.parsedStringFallback);
				if (text.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
					//Utils.pixLog(TAG, "invalid/missing text in text message");
					return null;
				}
				break;
			case PIX_MESSAGE_TYPE_COMP:
				JSONObject compObj = jsonObject.optJSONObject("comp");
				if (compObj == null) {
					//Utils.pixLog(TAG, "invalid/missing composition in comp message");
					return null;
				}
				composition = PixComposition.parseInstance(compObj);
				if (composition == null) {
					//Utils.pixLog(TAG, "failed to parse composition in comp message");
					return null;
				}
				break;
			default:
				//Utils.pixLog(TAG, "invalid message type");
				return null;
			}

			String sender = jsonObject.optString("sender", PixDefs.parsedStringFallback);
			if (sender.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid/missing lastMessageBy");
				return null;
			}
			
			// validations are done successfully. create the conversation
			PixChatMessage conversation = new PixChatMessage();
			conversation.setId(id);
			
			conversation.setDate(date);
			conversation.setNextDate(null); //default
			conversation.setType(type);
			if (type == PIX_MESSAGE_TYPE_TEXT) {
				conversation.setText(text);
			} else if (type == PIX_MESSAGE_TYPE_COMP) {
				conversation.setComp(composition);
			}
			conversation.setSender(sender);
			return conversation;
		}
		
		return null;
	}

	public static final String PIX_CHAT_MSG_KEY_EXIST = "PIX_CHAT_MSG_KEY_EXIST";
   	public static final String PIX_CHAT_MSG_KEY_DATE = "PIX_CHAT_MSG_KEY_DATE";
   	public static final String PIX_CHAT_MSG_KEY_NEXT_DATE = "PIX_CHAT_MSG_KEY_NEXT_DATE";
   	public static final String PIX_CHAT_MSG_KEY_ID = "PIX_CHAT_MSG_KEY_ID";
   	public static final String PIX_CHAT_MSG_KEY_TYPE = "PIX_CHAT_MSG_KEY_TYPE";
   	public static final String PIX_CHAT_MSG_KEY_SENT_STATE = "PIX_CHAT_MSG_KEY_SENT_STATE";
   	public static final String PIX_CHAT_MSG_KEY_SENDER = "PIX_CHAT_MSG_KEY_SENDER";
   	
   	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(PIX_CHAT_MSG_KEY_EXIST, true);
   		if (this.date != null) {
   			outBundle.putSerializable(PIX_CHAT_MSG_KEY_DATE , this.date);
   		}
   		if (this.nextDate != null) {
   			outBundle.putSerializable(PIX_CHAT_MSG_KEY_NEXT_DATE , this.nextDate);
   		}
   		if (Utils.isValidString(this.id)) {
   			outBundle.putString(PIX_CHAT_MSG_KEY_ID, this.id);
   		}
		outBundle.putInt(PIX_CHAT_MSG_KEY_TYPE, this.type);
		outBundle.putInt(PIX_CHAT_MSG_KEY_SENT_STATE, this.sentState);
   		if (Utils.isValidString(this.sender)) {
   			outBundle.putString(PIX_CHAT_MSG_KEY_SENDER, this.sender);
   		}
   		if (this.comp != null) {
   			this.comp.serialize(outBundle);
   		}
   	}
   	
   	public static boolean hasChatMessage(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(PIX_CHAT_MSG_KEY_EXIST);
   	}
   	public static PixChatMessage deserialize(Bundle inBundle) {
   		if (!hasChatMessage(inBundle)) return null;
   		PixChatMessage chatMessage = new PixChatMessage();
   		
   		Serializable dateSerializable = inBundle.getSerializable(PIX_CHAT_MSG_KEY_DATE);
   		if (dateSerializable instanceof Date) {
   			chatMessage.setDate((Date)dateSerializable);
   		}
   		Serializable nextDateSerializable = inBundle.getSerializable(PIX_CHAT_MSG_KEY_NEXT_DATE);
   		if (nextDateSerializable instanceof Date) {
   			chatMessage.setNextDate((Date)nextDateSerializable);
   		}
   		chatMessage.setId(inBundle.getString(PIX_CHAT_MSG_KEY_ID));
   		chatMessage.setType(inBundle.getInt(PIX_CHAT_MSG_KEY_TYPE));
   		chatMessage.setSentState(inBundle.getInt(PIX_CHAT_MSG_KEY_SENT_STATE));
   		chatMessage.setSender(inBundle.getString(PIX_CHAT_MSG_KEY_SENDER));
   		chatMessage.setComp(PixComposition.deserialize(inBundle));   		
   		return chatMessage;
   	}
   	
   	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public PixComposition getComp() {
		return comp;
	}

	public void setComp(PixComposition comp) {
		this.comp = comp;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public int getSentState() {
		return sentState;
	}

	public void setSentState(int sentState) {
		this.sentState = sentState;
	}
}
