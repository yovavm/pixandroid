
package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixUsers extends PixObjectsList<PixUser>{

	public PixUsers(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				
				JSONArray items = jsonObject.optJSONArray("users");
				
				if (items != null && (items.length() == this.getCount())) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixUser user = PixUser.parseInstance(item);
						
						if (user != null) {
							this.items.add(user);
						}
					}
					return true;
				}
			}
		}
		return false;
	}

}
