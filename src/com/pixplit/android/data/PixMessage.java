package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixMessage extends PixObject{
	private String pixMessage;
	
	public PixMessage(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			// parse straight to a setting objects - no strict validation on values
			this.setPixMessage(jsonObject.optString("message", PixDefs.parsedStringFallback));
			return true;
		}
		return false;
	}

	public String getPixMessage() {
		return pixMessage;
	}

	public void setPixMessage(String pixMessage) {
		this.pixMessage = pixMessage;
	}
}
