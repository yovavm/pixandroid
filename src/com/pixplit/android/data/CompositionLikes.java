package com.pixplit.android.data;

import org.json.JSONObject;

public class CompositionLikes extends PixObject{
	private int count;
   	private int liked;
   	private PixLiker liker;
   	
	public CompositionLikes(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			this.count = jsonObject.optInt("count", 0);
			this.liked = jsonObject.optInt("liked", 0);
			this.liker = PixLiker.parseInstance(jsonObject.optJSONObject("liker"));
			return true;
		}
		return false;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getLiked() {
		return liked;
	}
	public void setLiked(int liked) {
		this.liked = liked;
	}
	public PixLiker getLiker() {
		return liker;
	}
	public void setLiker(PixLiker liker) {
		this.liker = liker;
	}
}
