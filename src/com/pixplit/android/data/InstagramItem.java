
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;


public class InstagramItem{
	private String type;
	private String image;
	private String imageThumb;
	
	public static InstagramItem parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String type = jsonObject.optString("type", PixDefs.parsedStringFallback);
			if (type.equalsIgnoreCase(PixDefs.parsedStringFallback) ||
					!type.equalsIgnoreCase("image") /* parse only image type */) {
				return null;
			}
			JSONObject images = jsonObject.optJSONObject("images");
			if (images == null) {
				return null;
			}
			JSONObject thumbnail = images.optJSONObject("thumbnail");
			String thumbUrl = null;
			if (thumbnail != null) {
				thumbUrl = thumbnail.optString("url", PixDefs.parsedStringFallback);
			}
			JSONObject image = images.optJSONObject("standard_resolution");
			String imageUrl = null;
			if (image != null) {
				imageUrl = image.optString("url", PixDefs.parsedStringFallback);
			}
			
			// validations are done successfully. create the comment
			InstagramItem instagramItem = new InstagramItem();
			instagramItem.setType(type);
			instagramItem.setImage(imageUrl);
			instagramItem.setImageThumb(thumbUrl);
			return instagramItem;
		}
		return null;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageThumb() {
		return imageThumb;
	}

	public void setImageThumb(String imageThumb) {
		this.imageThumb = imageThumb;
	}
	
}
