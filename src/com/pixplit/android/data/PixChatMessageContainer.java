package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixChatMessageContainer extends PixObject{
//	private static final String TAG = "PixChatMessageContainer";
	
	private String conversationId;
	private PixChatMessage chatMessage;
	
	public PixChatMessageContainer(int code, String message) {
		super(code, message);
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		boolean parseOk = false;
		if (jsonObject != null) {
			this.conversationId = jsonObject.optString("conversationId", PixDefs.parsedStringFallback);
			if (conversationId.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				//Utils.pixLog(TAG, "invalid conversation id");
				return false;
			}
			
			this.chatMessage = null;
			JSONObject messageObj = jsonObject.optJSONObject("message");
			if (messageObj != null) {
				chatMessage = PixChatMessage.parseInstance(messageObj);
			}
			if (chatMessage == null) {
				//Utils.pixLog(TAG, "invalid/missing message");
				return false;
			}
			parseOk = true;
		}
		return parseOk;
	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String id) {
		this.conversationId = id;
	}

	public PixChatMessage getChatMessage() {
		return chatMessage;
	}


}
