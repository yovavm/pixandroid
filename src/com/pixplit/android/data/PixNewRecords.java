package com.pixplit.android.data;

import org.json.JSONObject;

public class PixNewRecords extends PixObject{
	private int notifications;
	private int actions;
	private int completed;
	private int conversations;
	
	public PixNewRecords(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			int notifications = jsonObject.optInt("notifications", 0);
			int actions = jsonObject.optInt("actions", 0);
			int completed = jsonObject.optInt("completed", 0);
			int conversations = jsonObject.optInt("conversations", 0);
			
			this.setNotifications(notifications);
			this.setActions(actions);
			this.setCompleted(completed);
			this.setConversations(conversations);
			return true;
		}
		return false;
	}
	
	public int getNotifications() {
		return notifications;
	}
	public void setNotifications(int notifications) {
		this.notifications = notifications;
	}
	public int getActions() {
		return actions;
	}
	public void setActions(int actions) {
		this.actions = actions;
	}
	public int getCompleted() {
		return completed;
	}
	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public int getConversations() {
		return conversations;
	}

	public void setConversations(int conversations) {
		this.conversations = conversations;
	}
}
