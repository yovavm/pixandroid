
package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixChallenges extends PixObjectsList<PixChallenge>{
	public PixChallenges(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				
				JSONArray items = jsonObject.optJSONArray("challenges");
				
				if (items != null) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixChallenge challenge = PixChallenge.parseInstance(item);
						
						if (challenge != null) {
							this.items.add(challenge);
						}
					}
					return true;
				}
			}
		}
		return false;
	}
}
