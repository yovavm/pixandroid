package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixEmailValidation extends PixObject{
	private String email;
	
	public PixEmailValidation(int code, String message) {
		super(code, message);
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			String email = jsonObject.optString("email", PixDefs.parsedStringFallback);
			if (email.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return false;
			}
			
			this.setEmail(email);
			return true;
		}
		return false;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
