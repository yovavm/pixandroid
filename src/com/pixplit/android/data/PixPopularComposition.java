
package com.pixplit.android.data;


public class PixPopularComposition extends PixComposition{
	private boolean active;
	
	public PixPopularComposition(PixComposition comp, boolean active) {
		super(comp);
		this.active = active;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
