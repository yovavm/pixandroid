
package com.pixplit.android.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class PixComposition{
	public static final int COMP_IS_NOT_COMPLETED = 0;
	public static final int COMP_IS_COMPLETED = 1;
	
	// empty constructor
	public PixComposition() {}
	
	// copy constructor
	public PixComposition(PixComposition comp) {
		if (comp == null) return;
		this.setActionId(comp.getActionId());
		this.setCompId(comp.getCompId());
		this.setCompType(comp.getCompType());
		this.setCreated(comp.getCreated());
		this.setHasBorder(comp.getHasBorder());
		this.setImage(comp.getImage());
		this.setInsertDate(comp.getInsertDate());
		this.setIsCompleted(comp.getIsCompleted());
		this.setIsRoot(comp.getIsRoot());
		this.setLiker(comp.getLiker());
		this.setLikesCount(comp.getLikesCount());
		this.setParentId(comp.getParentId());
		this.setParticipants(comp.getParticipants());
		this.setParticipantsCount(comp.getParticipantsCount());
		this.setRootId(comp.getRootId());
		this.setTagsCount(comp.getTagsCount());
		this.setTitle(comp.getTitle());
		this.setTotalComments(comp.getTotalComments());
		this.setLiked(comp.getLiked());
		this.setTags(comp.getTags());
		this.setSubtitle(comp.getSubtitle());
		this.setColorScheme(comp.getColorScheme());
		this.setFeatured(comp.isFeatured());
	}

	public static PixComposition parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String compId = jsonObject.optString("compId", PixDefs.parsedStringFallback);
			if (compId.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			int actionId = jsonObject.optInt("actionId", PixDefs.parsedIntFallback);
			int compType = jsonObject.optInt("compType", PixDefs.parsedIntFallback);
			if (compType == PixDefs.parsedIntFallback) {
				return null;
			}
			int created = jsonObject.optInt("created", PixDefs.parsedIntFallback);
			int hasBorder = jsonObject.optInt("hasBorder", PixDefs.parsedIntFallback);
			
			String image = jsonObject.optString("image", PixDefs.parsedStringFallback);
			if (image.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			
			String insertDate = jsonObject.optString("insertDate", PixDefs.parsedStringFallback);
			int isCompleted = jsonObject.optInt("isCompleted", PixDefs.parsedIntFallback);
			int isRoot = jsonObject.optInt("isRoot", PixDefs.parsedIntFallback);
			int likesCount = jsonObject.optInt("likesCount", 0);
			
			String parentId = jsonObject.optString("parentId", PixDefs.parsedStringFallback);
			if (parentId.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			
			JSONObject likerJson = jsonObject.optJSONObject("liker");
			PixLiker liker = PixLiker.parseInstance(likerJson);
			
			int participantsCount = jsonObject.optInt("participantsCount", 0);
			JSONArray participantsJson = jsonObject.optJSONArray("participants");
			ArrayList<PixParticipant> participants = new ArrayList<PixParticipant>();
			if (participantsJson != null && participantsCount > 0) {
				for (int i=0; i < participantsJson.length(); i++) {
					PixParticipant participant = PixParticipant.parseInstance(participantsJson.optJSONObject(i));
					if (participant != null) {
						participants.add(participant);
					}
				}
			}
			
			String rootId = jsonObject.optString("rootId", PixDefs.parsedStringFallback);
			int tagsCount = jsonObject.optInt("tagsCount", 0);
			String title = jsonObject.optString("title", PixDefs.parsedStringFallback);
			int totalComments = jsonObject.optInt("totalComments", 0);
			int liked = jsonObject.optInt("liked", 0);
			
			JSONArray tagsJson = jsonObject.optJSONArray("tags");
			ArrayList<String> tags = new ArrayList<String>();
			if (tagsJson != null && tagsCount > 0) {
				for (int i=0; i < tagsJson.length(); i++) {
					String tag = tagsJson.optString(i, PixDefs.parsedStringFallback);
					if (!tag.equalsIgnoreCase(PixDefs.parsedStringFallback) &&
							isTagAllowed(tag)) {
						tags.add(tag);
					}
				}
			}
			String subtitle = jsonObject.optString("subTitle", PixDefs.parsedStringFallback);
			String colorSchema = jsonObject.optString("colorScheme", PixDefs.defaultColorSchemeString);
			
			// validations are done successfully. create the composition
			PixComposition comp = new PixComposition();
			comp.setActionId(actionId);
			comp.setCompId(compId);
			comp.setCompType(compType);
			comp.setCreated(created);
			comp.setHasBorder(hasBorder);
			comp.setImage(image);
			comp.setInsertDate(insertDate);
			comp.setIsCompleted(isCompleted);
			comp.setIsRoot(isRoot);
			comp.setLiker(liker);
			comp.setLikesCount(likesCount);
			comp.setParentId(parentId);
			comp.setParticipants(participants);
			comp.setParticipantsCount(participantsCount);
			comp.setRootId(rootId);
			comp.setTagsCount(tagsCount);
			comp.setTitle(title);
			comp.setTotalComments(totalComments);
			comp.setLiked(liked);
			comp.setTags(tags);
			comp.setSubtitle(subtitle);
			comp.setColorScheme(colorSchema);
			comp.setFeatured(false); // default
			return comp;	
		}
		return null;
	}
	
   	private int actionId;
   	private String compId;
   	private int compType;
   	private int created;
   	private int hasBorder;
   	private String image;
   	private String insertDate;
   	private int isCompleted;
   	private int isRoot;
   	private PixLiker liker;
   	private int likesCount;
   	private String parentId;
   	private List<PixParticipant> participants;
   	private int participantsCount;
   	private String rootId;
   	private int tagsCount;
   	private String title;
   	private int totalComments;
   	private int liked;
   	private List<String> tags;
   	private String subtitle;
   	private String colorScheme;
   	private boolean isFeatured;
   	
   	public static final String PIX_COMPOSITION_KEY_PIX_COMPOSITION = "PIX_COMPOSITION_KEY_PIX_COMPOSITION";
   	public static final String PIX_COMPOSITION_KEY_ACTION_ID = "PIX_COMPOSITION_KEY_ACTION_ID";
   	public static final String PIX_COMPOSITION_KEY_COMP_ID = "PIX_COMPOSITION_KEY_COMP_ID";
   	public static final String PIX_COMPOSITION_KEY_COMP_TYPE = "PIX_COMPOSITION_KEY_COMP_TYPE";
   	public static final String PIX_COMPOSITION_KEY_CREATED = "PIX_COMPOSITION_KEY_CREATED";
   	public static final String PIX_COMPOSITION_KEY_HAS_BORDER = "PIX_COMPOSITION_KEY_HAS_BORDER";
   	public static final String PIX_COMPOSITION_KEY_IMAGE = "PIX_COMPOSITION_KEY_IMAGE";
   	public static final String PIX_COMPOSITION_KEY_INSERT_DATE = "PIX_COMPOSITION_KEY_INSERT_DATE";
   	public static final String PIX_COMPOSITION_KEY_IS_COMPLETED = "PIX_COMPOSITION_KEY_IS_COMPLETED";
   	public static final String PIX_COMPOSITION_KEY_IS_ROOT = "PIX_COMPOSITION_KEY_IS_ROOT";
	public static final String PIX_COMPOSITION_KEY_LIKES_COUNT = "PIX_COMPOSITION_KEY_LIKES_COUNT";
	public static final String PIX_COMPOSITION_KEY_PARENT_ID = "PIX_COMPOSITION_KEY_PARENT_ID";
	public static final String PIX_COMPOSITION_KEY_PARTICIPANT_COUNT = "PIX_COMPOSITION_KEY_PARTICIPANT_COUNT";
	public static final String PIX_COMPOSITION_KEY_ROOT_ID = "PIX_COMPOSITION_KEY_ROOT_ID";
	public static final String PIX_COMPOSITION_KEY_TAGS_COUNT = "PIX_COMPOSITION_KEY_TAGS_COUNT";
	public static final String PIX_COMPOSITION_KEY_TITLE = "PIX_COMPOSITION_KEY_TITLE";
	public static final String PIX_COMPOSITION_KEY_TOTAL_COMMENTS = "PIX_COMPOSITION_KEY_TOTAL_COMMENTS";
	public static final String PIX_COMPOSITION_KEY_LIKED = "PIX_COMPOSITION_KEY_LIKED";
	public static final String PIX_COMPOSITION_TAGS = "PIX_COMPOSITION_TAGS";
	public static final String PIX_COMPOSITION_SUBTITLE = "PIX_COMPOSITION_SUBTITLE";
	public static final String PIX_COMPOSITION_COLOR_SCHEME = "PIX_COMPOSITION_COLOR_SCHEME";
	
   	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(PIX_COMPOSITION_KEY_PIX_COMPOSITION, true); // indicates that the bundle holds a composition
   		outBundle.putInt(PIX_COMPOSITION_KEY_ACTION_ID, this.actionId);
   		if (Utils.isValidString(this.compId)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_COMP_ID, this.compId);
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_COMP_TYPE, this.compType);
   		outBundle.putInt(PIX_COMPOSITION_KEY_CREATED, this.created);
   		outBundle.putInt(PIX_COMPOSITION_KEY_HAS_BORDER, this.hasBorder);
   		if (Utils.isValidString(this.image)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_IMAGE, this.image);
   		}
   		if (Utils.isValidString(this.insertDate)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_INSERT_DATE, this.insertDate);
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_IS_COMPLETED, this.isCompleted);
   		outBundle.putInt(PIX_COMPOSITION_KEY_IS_ROOT, this.isRoot);
   		if (this.liker != null) {
   			this.liker.serialize(outBundle);
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_LIKES_COUNT, this.likesCount);
   		if (Utils.isValidString(this.parentId)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_PARENT_ID, this.parentId);
   		}
   		if (this.participants != null) {
   			int participantIndex = 0;
   			for (PixParticipant participant : this.participants) {
				if (participant != null) {
					participant.serialize(outBundle, String.format("%d", participantIndex));
					participantIndex++;
				}
			}
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_PARTICIPANT_COUNT, this.participantsCount);
   		if (Utils.isValidString(this.rootId)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_ROOT_ID, this.rootId);
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_TAGS_COUNT, this.tagsCount);
   		if (Utils.isValidString(this.title)) {
   			outBundle.putString(PIX_COMPOSITION_KEY_TITLE, this.title);
   		}
   		if (Utils.isValidString(this.subtitle)) {
   			outBundle.putString(PIX_COMPOSITION_SUBTITLE, this.subtitle);
   		}
   		if (Utils.isValidString(this.colorScheme)) {
   			outBundle.putString(PIX_COMPOSITION_COLOR_SCHEME, this.colorScheme);
   		}
   		outBundle.putInt(PIX_COMPOSITION_KEY_TOTAL_COMMENTS, this.totalComments);
   		outBundle.putInt(PIX_COMPOSITION_KEY_LIKED, this.liked);
   		if (this.tags != null) {
   			outBundle.putString(PIX_COMPOSITION_TAGS, TextUtils.join(",", this.tags));
   		}
   		
   	}
   	
   	public static boolean hasComposition(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(PIX_COMPOSITION_KEY_PIX_COMPOSITION);
   	}
   	
   	public static PixComposition deserialize(Bundle inBundle) {
   		if (!hasComposition(inBundle)) return null;
   		PixComposition composition = new PixComposition();
   		composition.setActionId(inBundle.getInt(PIX_COMPOSITION_KEY_ACTION_ID));
   		composition.setCompId(inBundle.getString(PIX_COMPOSITION_KEY_COMP_ID));
   		composition.setCompType(inBundle.getInt(PIX_COMPOSITION_KEY_COMP_TYPE, -1));
   		composition.setCreated(inBundle.getInt(PIX_COMPOSITION_KEY_CREATED));
   		composition.setHasBorder(inBundle.getInt(PIX_COMPOSITION_KEY_HAS_BORDER));
   		composition.setImage(inBundle.getString(PIX_COMPOSITION_KEY_IMAGE));
   		composition.setInsertDate(inBundle.getString(PIX_COMPOSITION_KEY_INSERT_DATE));
   		composition.setIsCompleted(inBundle.getInt(PIX_COMPOSITION_KEY_IS_COMPLETED));
   		composition.setIsRoot(inBundle.getInt(PIX_COMPOSITION_KEY_IS_ROOT));
   		composition.setLiker(PixLiker.deserialize(inBundle));
   		composition.setLikesCount(inBundle.getInt(PIX_COMPOSITION_KEY_LIKES_COUNT));
   		composition.setParentId(inBundle.getString(PIX_COMPOSITION_KEY_PARENT_ID));
   		int participantCount = inBundle.getInt(PIX_COMPOSITION_KEY_PARTICIPANT_COUNT);
   		composition.setParticipantsCount(participantCount);
   		if (participantCount > 0) {
   			ArrayList<PixParticipant> participants = new ArrayList<PixParticipant>(); 
   			for (int i=0; i < composition.getParticipantsCount(); i++) {
   				PixParticipant participant = PixParticipant.deserialize(inBundle, String.format("%d", i));
   				if (participant != null) {
   					participants.add(participant);
   				}
   			}
   			composition.setParticipants(participants);
   		}
   		composition.setRootId(inBundle.getString(PIX_COMPOSITION_KEY_ROOT_ID));
   		composition.setTagsCount(inBundle.getInt(PIX_COMPOSITION_KEY_TAGS_COUNT));
   		composition.setTitle(inBundle.getString(PIX_COMPOSITION_KEY_TITLE));
   		composition.setSubtitle(inBundle.getString(PIX_COMPOSITION_SUBTITLE));
   		composition.setColorScheme(inBundle.getString(PIX_COMPOSITION_COLOR_SCHEME, PixDefs.defaultColorSchemeString));
   		composition.setTotalComments(inBundle.getInt(PIX_COMPOSITION_KEY_TOTAL_COMMENTS));
   		composition.setLiked(inBundle.getInt(PIX_COMPOSITION_KEY_LIKED));
   		String tags = inBundle.getString(PIX_COMPOSITION_TAGS);
   		if (Utils.isValidString(tags)) {
   			Pattern p = Pattern.compile("[ ,#.]");
			String[] tagsArray = TextUtils.split(tags, p);
			if (tagsArray.length > 0) {
				composition.setTags(Arrays.asList(tagsArray));
			}
   		}
   		return composition;
   	}
   	
 	public int getActionId(){
		return this.actionId;
	}
	public void setActionId(int actionId){
		this.actionId = actionId;
	}
 	public String getCompId(){
		return this.compId;
	}
	public void setCompId(String compId){
		this.compId = compId;
	}
 	public int getCompType(){
		return this.compType;
	}
	public void setCompType(int compType){
		this.compType = compType;
	}
 	public int getCreated(){
		return this.created;
	}
	public void setCreated(int created){
		this.created = created;
	}
 	public int getHasBorder(){
		return this.hasBorder;
	}
	public void setHasBorder(int hasBorder){
		this.hasBorder = hasBorder;
	}
 	public String getImage(){
		return this.image;
	}
	public void setImage(String image){
		this.image = image;
	}
 	public String getInsertDate(){
		return this.insertDate;
	}
	public void setInsertDate(String insertDate){
		this.insertDate = insertDate;
	}
 	public int getIsCompleted(){
		return this.isCompleted;
	}
	public void setIsCompleted(int isCompleted){
		this.isCompleted = isCompleted;
	}
 	public int getIsRoot(){
		return this.isRoot;
	}
	public void setIsRoot(int isRoot){
		this.isRoot = isRoot;
	}
 	public PixLiker getLiker(){
		return this.liker;
	}
	public void setLiker(PixLiker liker){
		this.liker = liker;
	}
 	public int getLikesCount(){
		return this.likesCount;
	}
	public void setLikesCount(int likesCount){
		this.likesCount = likesCount;
	}
 	public String getParentId(){
		return this.parentId;
	}
	public void setParentId(String parentId){
		this.parentId = parentId;
	}
 	public List<PixParticipant> getParticipants(){
		return this.participants;
	}
	public void setParticipants(List<PixParticipant> participants){
		this.participants = participants;
	}
 	public int getParticipantsCount(){
		return this.participantsCount;
	}
	public void setParticipantsCount(int participantsCount){
		this.participantsCount = participantsCount;
	}
 	public String getRootId(){
		return this.rootId;
	}
	public void setRootId(String rootId){
		this.rootId = rootId;
	}
 	public int getTagsCount(){
		return this.tagsCount;
	}
	public void setTagsCount(int tagsCount){
		this.tagsCount = tagsCount;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
 	public int getTotalComments(){
		return this.totalComments;
	}
	public void setTotalComments(int totalComments){
		this.totalComments = totalComments;
	}
	public int getLiked() {
		return liked;
	}
	public void setLiked(int liked) {
		this.liked = liked;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public static boolean isJoinSplitAllow(PixComposition comp) {
		// validations
		if (comp == null) {
			return false;
		}
		
		// check that the composition is not completed
		if (comp.getIsCompleted() != PixComposition.COMP_IS_NOT_COMPLETED) {
			return false;
		}
		
		// validate composition type
		if (PixCompositionFramesetType.getType(comp.getCompType()) == null) {
			return false;
		}
		
		// otherwise join split allowed
		return true;
	}
	
	public static boolean isResplitAllow(PixComposition comp) {
		if (comp == null) return false;
		List<PixParticipant> participants = comp.getParticipants();
		if (participants == null || participants.size() < 2) return false;
		PixParticipant rootParticipant = comp.getParticipants().get(0);
		if (rootParticipant == null) return false;
		String rootUserName = rootParticipant.getUsername();
		if (!Utils.isValidString(rootUserName)) return false;
		if (rootUserName.equalsIgnoreCase(UserInfo.getUserName())) return false;
		if (!Utils.isValidString(comp.getRootId())) return false;
		return true;
	}
	
	private static boolean isTagAllowed(String tag) {
		if (Utils.isValidString(tag) &&
				!tag.equalsIgnoreCase("null") &&
				!tag.equalsIgnoreCase("!") &&
				!tag.equalsIgnoreCase("..")) {
			return true;
		}
		return false;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getColorScheme() {
		return colorScheme;
	}

	public void setColorScheme(String colorSchema) {
		this.colorScheme = colorSchema;
	}

	public boolean isFeatured() {
		return isFeatured;
	}

	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
}
