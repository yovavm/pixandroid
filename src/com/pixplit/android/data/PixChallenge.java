
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;


public class PixChallenge{
	private String tag;
	private String banner;
	private String title;
	
	public static PixChallenge parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String tag = jsonObject.optString("tag", PixDefs.parsedStringFallback);
			if (tag.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String banner = jsonObject.optString("banner", PixDefs.parsedStringFallback);
			if (banner.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			String title = jsonObject.optString("title", PixDefs.parsedStringFallback);
			if (title.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			
			// validations are done successfully. create the challenge
			PixChallenge challenge = new PixChallenge();
			challenge.setTag(tag);
			challenge.setBanner(banner);
			challenge.setTitle(title);
			return challenge;
		}
		
		return null;
	}
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getBanner() {
		return banner;
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
