package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixNotificationsSettings extends PixObject{
	private int liked;
	private int commented;
	private int invited;
	private int joined;
	private int followed;
	private int fbFriendJoined;
	private int system;
	
	public PixNotificationsSettings(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			// parse straight to a setting objects - no strict validation on values
			this.setLiked(jsonObject.optInt("liked", PixDefs.parsedIntFallback));
			this.setCommented(jsonObject.optInt("commented", PixDefs.parsedIntFallback));
			this.setInvited(jsonObject.optInt("invited", PixDefs.parsedIntFallback));
			this.setJoined(jsonObject.optInt("joined", PixDefs.parsedIntFallback));
			this.setFollowed(jsonObject.optInt("followed", PixDefs.parsedIntFallback));
			this.setFbFriendJoined(jsonObject.optInt("fbFriendJoined", PixDefs.parsedIntFallback));
			this.setSystem(jsonObject.optInt("system", PixDefs.parsedIntFallback));
			return true;
		}
		return false;
	}
	
	public int getLiked() {
		return liked;
	}
	public void setLiked(int liked) {
		this.liked = liked;
	}

	public int getCommented() {
		return commented;
	}

	public void setCommented(int commented) {
		this.commented = commented;
	}

	public int getInvited() {
		return invited;
	}

	public void setInvited(int invited) {
		this.invited = invited;
	}

	public int getJoined() {
		return joined;
	}

	public void setJoined(int joined) {
		this.joined = joined;
	}

	public int getFollowed() {
		return followed;
	}

	public void setFollowed(int followed) {
		this.followed = followed;
	}

	public int getFbFriendJoined() {
		return fbFriendJoined;
	}

	public void setFbFriendJoined(int fbFriendJoined) {
		this.fbFriendJoined = fbFriendJoined;
	}

	public int getSystem() {
		return system;
	}

	public void setSystem(int system) {
		this.system = system;
	}

}
