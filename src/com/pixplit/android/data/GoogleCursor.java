
package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


public class GoogleCursor{
	private int currentPageIndex;
	private ArrayList<GoogleCursorPage> pages;
	
	public static GoogleCursor parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			int currentPageIndex = jsonObject.optInt("currentPageIndex", -1);
			if (currentPageIndex < 0) {
				return null;
			}
			JSONArray pagesJson = jsonObject.optJSONArray("pages");
			if (pagesJson == null || pagesJson.length() <= 0) {
				return null;
			}
			ArrayList<GoogleCursorPage> pages = new ArrayList<GoogleCursorPage>();
			for (int i=0; i<pagesJson.length(); i++) {
				JSONObject item = pagesJson.optJSONObject(i);
				GoogleCursorPage page = GoogleCursorPage.parseInstance(item);
				if (page != null) {
					pages.add(page);
				}
			}
			
			GoogleCursor cursor = new GoogleCursor();
			cursor.setCurrentPageIndex(currentPageIndex);
			cursor.setPages(pages);
			return cursor;
		}
		return null;
	}

	public int getCurrentPageIndex() {
		return currentPageIndex;
	}

	public void setCurrentPageIndex(int currentPageIndex) {
		this.currentPageIndex = currentPageIndex;
	}

	public ArrayList<GoogleCursorPage> getPages() {
		return pages;
	}

	public void setPages(ArrayList<GoogleCursorPage> pages) {
		this.pages = pages;
	}
	
	public String getNextStart() {
		if (this.pages == null) return null;
		if (this.currentPageIndex < 0 || this.currentPageIndex+1 >= this.pages.size()) return null;
		GoogleCursorPage page = this.pages.get(currentPageIndex+1);
		if (page == null) return null;
		return page.getStart();
	}
}
