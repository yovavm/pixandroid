package com.pixplit.android.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;


public class PixFriendsFollowStatus  extends PixObject{
	private int notifyFBFriends;
	private int count;
	private ArrayList<PixUser> friends; // FB friends who have Pixplit linked
	private int suggestedCount;
	private ArrayList<PixUser> suggested;
	
	public PixFriendsFollowStatus(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			this.setNotifyFBFriends(jsonObject.optInt("notifyFBFriends", PixDefs.parsedIntFallback));
			this.setCount(jsonObject.optInt("count", PixDefs.parsedIntFallback));
			this.setSuggestedCount(jsonObject.optInt("suggestedCount", PixDefs.parsedIntFallback));
			
			JSONArray friendsJson = jsonObject.optJSONArray("friends");
			friends = new ArrayList<PixUser>();
			if (friendsJson != null && this.count > 0) {
				for (int i=0; i < friendsJson.length(); i++) {
					PixUser user = PixUser.parseInstance(friendsJson.optJSONObject(i));
					if (user != null) {
						friends.add(user);
					}
				}
			}

			JSONArray suggestedJson = jsonObject.optJSONArray("suggested");
			suggested = new ArrayList<PixUser>();
			if (suggestedJson != null && this.suggestedCount > 0) {
				for (int i=0; i < suggestedJson.length(); i++) {
					PixUser user = PixUser.parseInstance(suggestedJson.optJSONObject(i));
					if (user != null) {
						suggested.add(user);
					}
				}
			}
			
			return true;
		}
		return false;
	}

	public int getNotifyFBFriends() {
		return notifyFBFriends;
	}

	public void setNotifyFBFriends(int notifyFBFriends) {
		this.notifyFBFriends = notifyFBFriends;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSuggestedCount() {
		return suggestedCount;
	}

	public void setSuggestedCount(int suggestedCount) {
		this.suggestedCount = suggestedCount;
	}

	public ArrayList<PixUser> getSuggested() {
		return suggested;
	}

	public ArrayList<PixUser> getFriends() {
		return friends;
	}
}
