
package com.pixplit.android.data;

import java.text.ParseException;
import java.util.Date;

import org.json.JSONObject;

import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;


public class PixParticipant{
   	private String date;
   	private String displayName;
   	private String fullName;
   	private int index;
   	private String location;
   	private String picture;
   	private String username;
   	private String formatedDate;

   	public static PixParticipant parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String dateStr = jsonObject.optString("date", PixDefs.parsedStringFallback);
			String formatedDate = null;
			if (!dateStr.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				try {
					Date date = PixDateFormat.getPixDateInstance().parse(dateStr);  
	    		    formatedDate = PixDateFormat.getPixDateInstance().humanFormat(date); 
	    		}catch (ParseException e) {  
	    		    e.printStackTrace();
	    		    formatedDate = null;
	    		}
			}
			
			String displayName = jsonObject.optString("displayName", PixDefs.parsedStringFallback);
			String fullName = jsonObject.optString("fullName", PixDefs.parsedStringFallback);
			int index = jsonObject.optInt("index", PixDefs.parsedIntFallback);
			if (index == PixDefs.parsedIntFallback) {
				return null;
			}
			String location = jsonObject.optString("location", PixDefs.parsedStringFallback);
			String picture = jsonObject.optString("picture", PixDefs.parsedStringFallback);
			String username = jsonObject.optString("username", PixDefs.parsedStringFallback);
			if (username.equalsIgnoreCase(PixDefs.parsedStringFallback)) {
				return null;
			}
			
			// validations are done successfully. create the participant
			PixParticipant participant = new PixParticipant();
			participant.setDate(dateStr);
			participant.setDisplayName(displayName);
			participant.setFullName(fullName);
			participant.setIndex(index);
			participant.setLocation(location);
			participant.setPicture(picture);
			participant.setUsername(username);
			if (Utils.isValidString(formatedDate)) {
				participant.setFormatedDate(formatedDate);
			}
			return participant;
		}
		return null;
   	}
   	
   	public static final String PIX_PARTICIPANT_KEY_EXIST = "PIX_PARTICIPANT_%s_KEY_EXIST";
   	public static final String PIX_PARTICIPANT_KEY_DATE = "PIX_PARTICIPANT_%s_KEY_DATE";
   	public static final String PIX_PARTICIPANT_KEY_DISPLAY_NAME = "PIX_PARTICIPANT_%s_KEY_DISPLAY_NAME";
   	public static final String PIX_PARTICIPANT_KEY_FULL_NAME = "PIX_PARTICIPANT_%s_KEY_FULL_NAME";
   	public static final String PIX_PARTICIPANT_KEY_INDEX = "PIX_PARTICIPANT_%s_KEY_INDEX";
   	public static final String PIX_PARTICIPANT_KEY_LOCATION = "PIX_PARTICIPANT_%s_KEY_LOCATION";
   	public static final String PIX_PARTICIPANT_KEY_PICTURE = "PIX_PARTICIPANT_%s_KEY_PICTURE";
   	public static final String PIX_PARTICIPANT_KEY_USER_NAME = "PIX_PARTICIPANT_%s_KEY_USER_NAME";
   	public static final String PIX_PARTICIPANT_KEY_FORMATED_DATE = "PIX_PARTICIPANT_%s_KEY_FORMATED_DATE";
   	
   	public void serialize(Bundle outBundle, String postfix) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(String.format(PIX_PARTICIPANT_KEY_EXIST, postfix), true);
   		if (Utils.isValidString(this.date)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_DATE, postfix) , this.date);
   		}
   		if (Utils.isValidString(this.displayName)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_DISPLAY_NAME, postfix), this.displayName);
   		}
   		if (Utils.isValidString(this.fullName)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_FULL_NAME, postfix), this.fullName);
   		}
   		outBundle.putInt(String.format(PIX_PARTICIPANT_KEY_INDEX, postfix), this.index);
   		if (Utils.isValidString(this.location)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_LOCATION, postfix), this.location);
   		}
   		if (Utils.isValidString(this.picture)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_PICTURE, postfix), this.picture);
   		}
   		if (Utils.isValidString(this.username)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_USER_NAME, postfix), this.username);
   		}
   		if (Utils.isValidString(this.formatedDate)) {
   			outBundle.putString(String.format(PIX_PARTICIPANT_KEY_FORMATED_DATE, postfix), this.formatedDate);
   		}
   		
   	}
   	
   	public static boolean hasParticipant(Bundle inBundle, String postfix) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(String.format(PIX_PARTICIPANT_KEY_EXIST, postfix));
   	}
   	public static PixParticipant deserialize(Bundle inBundle, String postfix) {
   		if (!hasParticipant(inBundle, postfix)) return null;
   		PixParticipant participant = new PixParticipant();
   		participant.setDate(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_DATE, postfix)));
   		participant.setDisplayName(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_DISPLAY_NAME, postfix)));
   		participant.setFullName(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_FULL_NAME, postfix)));
   		participant.setIndex(inBundle.getInt(String.format(PIX_PARTICIPANT_KEY_INDEX, postfix)));
   		participant.setLocation(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_LOCATION, postfix)));
   		participant.setPicture(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_PICTURE, postfix)));
   		participant.setUsername(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_USER_NAME, postfix)));
   		participant.setFormatedDate(inBundle.getString(String.format(PIX_PARTICIPANT_KEY_FORMATED_DATE, postfix)));
   		return participant;
   	}
 	public String getDate(){
		return this.date;
	}
	public void setDate(String date){
		this.date = date;
	}
 	public String getDisplayName(){
		return this.displayName;
	}
	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}
 	public String getFullName(){
		return this.fullName;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}
 	public int getIndex(){
		return this.index;
	}
	public void setIndex(int index){
		this.index = index;
	}
 	public String getLocation(){
		return this.location;
	}
	public void setLocation(String location){
		this.location = location;
	}
 	public String getPicture(){
		return this.picture;
	}
	public void setPicture(String picture){
		this.picture = picture;
	}
 	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}

	public String getFormatedDate() {
		return formatedDate;
	}

	public void setFormatedDate(String formatedDate) {
		this.formatedDate = formatedDate;
	}
}
