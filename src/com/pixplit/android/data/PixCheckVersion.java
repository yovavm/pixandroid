package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;

public class PixCheckVersion extends PixObject{
	private int mandatory;
	private String title;
	private String laterButton;
	private String upgradeButton;
	private String message;
	
	public PixCheckVersion(int code, String message) {
		super(code, message);
	}
	
	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			// parse straight to a setting objects - no strict validation on values
			this.setMandatory(jsonObject.optInt("mandatory", PixDefs.parsedIntFallback));
			this.setTitle(jsonObject.optString("title", PixDefs.parsedStringFallback));
			this.setLaterButton(jsonObject.optString("laterButton", PixDefs.parsedStringFallback));
			this.setUpgradeButton(jsonObject.optString("upgradeButton", PixDefs.parsedStringFallback));
			this.setPopupMessage(jsonObject.optString("message", PixDefs.parsedStringFallback));
			
			return true;
		}
		return false;
	}

	public int getMandatory() {
		return mandatory;
	}

	public void setMandatory(int mandatory) {
		this.mandatory = mandatory;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLaterButton() {
		return laterButton;
	}

	public void setLaterButton(String laterButton) {
		this.laterButton = laterButton;
	}

	public String getUpgradeButton() {
		return upgradeButton;
	}

	public void setUpgradeButton(String upgradeButton) {
		this.upgradeButton = upgradeButton;
	}

	public String getPopupMessage() {
		return message;
	}

	public void setPopupMessage(String message) {
		this.message = message;
	}

}
