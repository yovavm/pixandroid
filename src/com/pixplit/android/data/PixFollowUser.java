package com.pixplit.android.data;

public class PixFollowUser extends PixUser{
	private boolean isFeatured;

	public PixFollowUser(PixUser user, boolean featured) {
		this.setUsername(user.getUsername());
		this.setFullName(user.getFullName());
		this.setPicture(user.getPicture());
		this.setFollowStatus(user.getFollowStatus());
		this.setSelected(user.isSelected());
		this.isFeatured = featured;
	}
	
	public boolean isFeatured() {
		return isFeatured;
	}

	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
}
