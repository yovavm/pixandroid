
package com.pixplit.android.data;

import org.json.JSONObject;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;


public class GoogleImage{
	private String url;
	private String thumbUrl;
	
	public static GoogleImage parseInstance(JSONObject jsonObject) {
		if (jsonObject != null) {
			String url = jsonObject.optString("url", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(url)) {
				return null;
			}
			String thumbUrl = jsonObject.optString("tbUrl", PixDefs.parsedStringFallback);
			if (!Utils.isValidString(thumbUrl)) {
				return null;
			}

			GoogleImage gImage = new GoogleImage();
			gImage.setUrl(url);
			gImage.setThumbUrl(thumbUrl);
			return gImage;
		}
		return null;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}	
}
