
package com.pixplit.android.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class PixComments extends PixObjectsList<PixComment>{
	private int totalComments;

	public PixComments(int code, String message) {
		super(code, message);
		// TODO Auto-generated constructor stub
	}

	public boolean parseJsonObject(JSONObject jsonObject) {
		if (jsonObject != null) {
			if(super.parseJsonObject(jsonObject)) {
				
				totalComments = jsonObject.optInt("totalComments", 0);
				
				JSONArray items = jsonObject.optJSONArray("comments");
				
				if (items != null && (items.length() == this.getCount())) {
					for (int i=0; i<items.length(); i++) {
						JSONObject item = items.optJSONObject(i);
						PixComment comment = PixComment.parseInstance(item);
						
						if (comment != null) {
							this.items.add(comment);
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	
 	public int getTotalComments(){
		return this.totalComments;
	}
	public void setTotalComments(int totalComments){
		this.totalComments = totalComments;
	}
}
