package com.pixplit.android.data;

import android.os.Bundle;

import com.pixplit.android.util.Utils;

public class PixPrivateCompositionMetadata {
	private String conversationId;
	private String recipient;
	private String parentMsgId;
	
	public PixPrivateCompositionMetadata() {}  // empty constructor for serialization purpose
	
	public PixPrivateCompositionMetadata(String conversationId, String recipient, String msgId) {
		this.conversationId = conversationId;
		this.setParentMsgId(msgId);
		this.setRecipient(recipient);
	}
	
	///////////  Serialization stuff  ////////////////////
	public static final String PRIVATE_COMP_METADATA_KEY_PRIVATE_COMP_EXIST = "PRIVATE_COMP_METADATA_KEY_PRIVATE_COMP_EXIST";
	public static final String PRIVATE_COMP_METADATA_KEY_CONVERSATION_ID 	= "PRIVATE_COMP_METADATA_KEY_CONVERSATION_ID";
	public static final String PRIVATE_COMP_METADATA_KEY_RECIPIENT 			= "PRIVATE_COMP_METADATA_KEY_RECIPIENT";
	public static final String PRIVATE_COMP_METADATA_KEY_PARENT_MSG_ID 		= "PRIVATE_COMP_METADATA_KEY_PARENT_MSG_ID";
	
	public void serialize(Bundle outBundle) {
   		if (outBundle == null) return;
   		outBundle.putBoolean(PRIVATE_COMP_METADATA_KEY_PRIVATE_COMP_EXIST, true); // indicates that the bundle holds a composition
   		if (Utils.isValidString(this.conversationId)) {
   			outBundle.putString(PRIVATE_COMP_METADATA_KEY_CONVERSATION_ID, this.conversationId);
   		}
   		if (Utils.isValidString(this.recipient)) {
   			outBundle.putString(PRIVATE_COMP_METADATA_KEY_RECIPIENT, this.recipient);
   		}
   		if (Utils.isValidString(this.parentMsgId)) {
   			outBundle.putString(PRIVATE_COMP_METADATA_KEY_PARENT_MSG_ID, this.parentMsgId);
   		}
	}
	
	public static boolean hasPrivateCompMetadata(Bundle inBundle) {
   		if (inBundle == null) return false;
   		return inBundle.getBoolean(PRIVATE_COMP_METADATA_KEY_PRIVATE_COMP_EXIST);
   	}
   	
	public static PixPrivateCompositionMetadata deserialize(Bundle inBundle) {
   		if (!hasPrivateCompMetadata(inBundle)) return null;
   		PixPrivateCompositionMetadata privateCompMetadata = new PixPrivateCompositionMetadata();
   		privateCompMetadata.setConversationId(inBundle.getString(PRIVATE_COMP_METADATA_KEY_CONVERSATION_ID));
   		privateCompMetadata.setRecipient(inBundle.getString(PRIVATE_COMP_METADATA_KEY_RECIPIENT));
   		privateCompMetadata.setParentMsgId(inBundle.getString(PRIVATE_COMP_METADATA_KEY_PARENT_MSG_ID));
   		return privateCompMetadata;
   	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}
	
	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getParentMsgId() {
		return parentMsgId;
	}

	public void setParentMsgId(String parentMsgId) {
		this.parentMsgId = parentMsgId;
	}
}
