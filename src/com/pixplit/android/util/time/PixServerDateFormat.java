package com.pixplit.android.util.time;

import java.text.SimpleDateFormat;

public class PixServerDateFormat extends SimpleDateFormat{
	private static PixServerDateFormat instance = null;
	
	public static PixServerDateFormat getPixDateInstance() {
		if (instance == null) {
			instance = new PixServerDateFormat("yyyy-MM-d HH:mm:ss");
		}
		return instance;
	}
	
	private PixServerDateFormat(String string) {
		super(string);
	}
}
