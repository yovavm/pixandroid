package com.pixplit.android.util.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android.text.format.DateFormat;

import com.pixplit.android.PixApp;
import com.pixplit.android.util.Utils;

public class PixConversationDateFormat extends SimpleDateFormat{
	private static PixConversationDateFormat instance = null;
	
	public static PixConversationDateFormat getPixDateInstance() {
		if (instance == null) {
			instance = new PixConversationDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");
		}
		return instance;
	}
	
	private PixConversationDateFormat(String string) {
		super(string);
	}

	public String getTimestamp() {
		String timestamp = "";
		if (instance != null) {
			timestamp = instance.format(new Date());
		}
		return timestamp;
	}
	
	public String timeFormat(Date date) {
		String humanFormat = null;
		
		if (date != null) {
			
		    Calendar calender = getCalendar();
		    calender.setTime(date);

		    humanFormat = DateFormat.getTimeFormat(PixApp.getContext()).format(date);
		    
		}
		return humanFormat;
	}
	
	public String mediumDateFormat(Date date) {
		String humanFormat = null;
		
		if (date != null) {
			humanFormat = DateFormat.getMediumDateFormat(PixApp.getContext()).format(date);
		}
		return humanFormat;
	}
	
	public String humanFormat(Date date) {
		int minutes;
		int hours;
		int days;
			
		String humanFormat = null;
		boolean displayTime = false;
		boolean displayDay = false;
		
		if (date != null) {
			long diffInMs = Utils.getTimeInMillis() - date.getTime();
			int diffInSeconds = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs);

			minutes = diffInSeconds/60;
		    if (minutes < 60) {
		    	displayTime = true;
		    } else {
		    	// hours
		    	hours = minutes / 60;
			    if (hours > 0 && hours < 24) {
			    	displayTime = true;
			    } else {
		    		// days
		    		days = hours / 24;
		    		if (days > 0 && days < 8) {
		    			displayDay = true;
				    }	    		
			    }
		    }

		    Calendar calender = getCalendar();
		    calender.setTime(date);
		    if (displayTime) {
		    	humanFormat = DateFormat.getTimeFormat(PixApp.getContext()).format(date);
		    } else {
		    	if (displayDay) {
		    		humanFormat = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
		    	} else {
		    		humanFormat = DateFormat.getDateFormat(PixApp.getContext()).format(date);
		    	}
		    }
		}
		return humanFormat;
	}
	
}
