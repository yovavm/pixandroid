package com.pixplit.android.util.viewholders;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixAccount;
import com.pixplit.android.util.Utils;

public class AdminAccountViewHolder extends PixViewHolder{
	
	private LinearLayout accountLayout;
	private TextView accountName;
	private TextView campaigns;
	private TextView maxCampaigns;
	
	Drawable accountColor = PixApp.getContext().getResources().getDrawable(R.drawable.comment_color_selector);
	
	public interface AccountViewDelegate {
		public void onAccountClick(PixAccount account);
		public void onAccountLongClick(PixAccount account);
	}
	
	public AdminAccountViewHolder(View v) {
		accountLayout = (LinearLayout) v.findViewById(R.id.accountLayout);
        accountName = (TextView) v.findViewById(R.id.accountName);
        campaigns = (TextView) v.findViewById(R.id.accountCampaigns);
        maxCampaigns = (TextView) v.findViewById(R.id.accountMaxCampaigns);
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void init(final AccountViewDelegate mDelegate) {
		// set the click listeners 
        if (accountLayout != null){
        	accountLayout.setOnClickListener(new OnViewHolderClickListener(this) {
                public void onClick(View v, PixViewHolder viewHolder) {
                	AdminAccountViewHolder mvh = (AdminAccountViewHolder) viewHolder;
                	PixAccount account = (PixAccount) mvh.data;
                	if (mDelegate != null) {
                		mDelegate.onAccountClick(account);
                	}
                }
            });
        	accountLayout.setOnLongClickListener(new OnViewHolderLongClickListener(this) {
				@Override
				public void onLongClick(View v, PixViewHolder viewHolder) {
					AdminAccountViewHolder mvh = (AdminAccountViewHolder) viewHolder;
					PixAccount account = (PixAccount) mvh.data;
                    // the image had been clicked, call the event listener
                    mDelegate.onAccountLongClick(account);
				}
			});
        	if (Utils.hasJellyBean()) {
        		accountLayout.setBackground(accountColor);
        	} else {
        		accountLayout.setBackgroundDrawable(accountColor);
        	}
        }        
	}

	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
		PixAccount account = (PixAccount)data; 
	    
	    if (accountName != null) {
	    	if (Utils.isValidString(account.getFullName())) {
		    	accountName.setText(account.getFullName());
		    } else {
		    	accountName.setText("--No name--");
		    }
		}
	    
	    if (campaigns != null) {
	    	campaigns.setText(Integer.valueOf(account.getCampaigns()).toString());
	    }
	    
	    if (maxCampaigns != null) {
	    	maxCampaigns.setText(Integer.valueOf(account.getMaxCampaigns()).toString());
	    }
	}
}
