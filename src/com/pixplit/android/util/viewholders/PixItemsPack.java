package com.pixplit.android.util.viewholders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PixItemsPack<T> {
	private ArrayList<T> mCompsPack;
	private int packSize;
	
	public PixItemsPack(int packSize, Iterator<T> iter){
		mCompsPack = new ArrayList<T>();
		this.packSize = packSize;
		
		for (int i=0; i<packSize; i++){
			if (iter.hasNext()){
				mCompsPack.set(i, iter.next());
			} else {
				mCompsPack.set(i, null);
			}
		}
	}
	
	public T getItem(int position){
		T response = null;
		if (position < packSize) {
			response = mCompsPack.get(position);
		}
		return response;
	}
	
	public int getPackSize() {
		return packSize;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList<PixItemsPack<Object>> createItemsPackList(List<Object> items, int packSize){
		ArrayList<PixItemsPack<Object>> mPackList = new ArrayList<PixItemsPack<Object>>();
		
		if (items != null){
			Iterator<?> iter = items.iterator();
			
			while (iter.hasNext()){
				mPackList.add(((PixItemsPack<Object>)(new PixItemsPack(packSize, iter))));
			}
		}
		return mPackList;
	}
}