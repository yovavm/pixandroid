package com.pixplit.android.util.viewholders;

import java.util.Random;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminAddToPopularHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.CompFlagHttpRequest;
import com.pixplit.android.http.request.ViewAckHttpRequest;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;
import com.pixplit.android.ui.elements.CompActionsBar;
import com.pixplit.android.ui.elements.CompActionsBar.CompActionsBarDelegate;
import com.pixplit.android.ui.elements.CompOverlay;
import com.pixplit.android.ui.elements.DoubleTapImageView;
import com.pixplit.android.ui.elements.DoubleTapImageView.DoubleTapImageViewListener;
import com.pixplit.android.ui.elements.FlowLayout;
import com.pixplit.android.ui.elements.LikesBar;
import com.pixplit.android.ui.elements.LikesBar.LikesBarDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.elements.PixTextView;
import com.pixplit.android.ui.elements.TagsContainer;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.viewholders.ParticipantViewController.ParticipantViewDelegate;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class PixCompositionViewHolder extends PixViewHolder implements CompActionsBarDelegate, 
																		ParticipantViewDelegate, 
																		LikesBarDelegate{
		
	private static final int MAX_PARTICIPANTS_NUM = 4;
	private int [] participantCellId = {R.id.lastContributerCell, R.id.participant3Cell, 
									 R.id.participant2Cell, R.id.participant1Cell};
	
	ParticipantViewController[] participants = new ParticipantViewController[MAX_PARTICIPANTS_NUM];
	private View participantsSecondRow;
	private RelativeLayout compImageFrame;
	private DoubleTapImageView compImage;
	private ImageContainer compImageContainer;
	private CompBitmapFetchHolderDelegate compImageFetcherDelegate;
		
	private PixBaseListDelegate mListDelegate;
	private TextView title;	
	private TextView subtitle;
	private TagsContainer tagsContainer;
	private LikesBar likesBar;

	protected CompActionsBar actionsBar;
	private PixTextView adminAddToPopularBtn;
	private CompOverlay overlay;
	
	private TextView footer;
	
	private static final String saveToLibStr = PixApp.getContext().getResources().getString(R.string.save_to_library);
	private static final int blackColor = PixApp.getContext().getResources().getColor(R.color.black);
	
	public PixCompositionViewHolder(View v, PixBaseListDelegate listDelegate) {
		mListDelegate = listDelegate;
		
		actionsBar = (CompActionsBar) v.findViewById(R.id.comp_actions_bar);
	    compImage = (DoubleTapImageView) v.findViewById(R.id.comp_img);

        likesBar = new LikesBar((FlowLayout)v.findViewById(R.id.likes_bar), this);
        tagsContainer = new TagsContainer((FlowLayout)v.findViewById(R.id.comp_tags_placeholder), listDelegate);
        compImageFrame = (RelativeLayout)v.findViewById(R.id.compFrame);
        title = (TextView)v.findViewById(R.id.compTitle);
        subtitle = (TextView)v.findViewById(R.id.compSubTitle);
		
        for (int i=0; i < MAX_PARTICIPANTS_NUM; i++) {
        	participants[i] = new ParticipantViewController(this);
        	LinearLayout cellLayout = (LinearLayout) v.findViewById(participantCellId[i]);
        	participants[i].init(cellLayout);
        }
        participantsSecondRow = v.findViewById(R.id.participants_row2);
        
        footer = (TextView)v.findViewById(R.id.comp_footer);
	}
	
	public void init() {
		// set the click listeners 
        if (compImage != null){
        	compImage.getLayoutParams().height = PixAppState.getState().getSplitPreviewHeight();
        	compImage.getLayoutParams().width = PixAppState.getState().getSplitPreivewWidth();
        }
                
        if (actionsBar != null) {
			actionsBar.setDelegate(this);
		}
        
        overlay = new CompOverlay(compImageFrame, mListDelegate);
        
        for(int i=0; i < MAX_PARTICIPANTS_NUM; i++) {
    		participants[i].setParticipant(null);
    	}
        
        compImageFetcherDelegate = new CompBitmapFetchHolderDelegate(this);
        
        if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				adminAddToPopularBtn = createAddToPopularBtn();
			}
        }
//        addRelatedBtn();
    }
	
	@Override
	public void onNewData(Object data) {
	    final PixComposition comp = (PixComposition)data; 
	    actionsBar.setEnabled(false);
	    
	    String url = comp.getImage();
	    if (compImageContainer != null) compImageContainer.cancelRequest();
	    compImageContainer = PixImagesLoader.loadImageWithDelegate(url, 
    			compImage, 0, R.drawable.reload_image, Priority.HIGH, true, compImageFetcherDelegate);
	    
	    if (PixComposition.isJoinSplitAllow(comp)) {
	    	compImage.highlightOnPress(true);
	    } else {
	    	compImage.highlightOnPress(false);
	    }
	    
	    if (actionsBar != null) {
			actionsBar.bindComposition(comp);
		}
	    
	    if (title != null) {
	    	String compTitle = comp.getTitle();
	    	if (Utils.isValidString(compTitle)) {
	    		title.setText(compTitle);
	    		String colorScheme = comp.getColorScheme();
	    		if (Utils.isValidString(colorScheme)) {
	    			title.setTextColor(Utils.rgb2Color(colorScheme));
	    		} else {
	    			// default is black
	    			title.setTextColor(blackColor);
	    		}
	    		title.setVisibility(View.VISIBLE);
	    	} else {
	    		title.setVisibility(View.GONE);
	    	}
	    }
	    
	    if (subtitle != null) {
	    	String compSubTitle = comp.getSubtitle();
	    	if (Utils.isValidString(compSubTitle)) {
	    		subtitle.setText(compSubTitle);
	    		subtitle.setVisibility(View.VISIBLE);
	    	} else {
	    		subtitle.setVisibility(View.GONE);
	    	}
	    }
	    
	    if (footer != null &&
	    		footer.getVisibility() != View.VISIBLE) {
	    	// handle the case where the last cell is recycled
	    	footer.setVisibility(View.VISIBLE);
	    }
	    
	    if (comp.getParticipants() != null) {
	    	int participantCount = comp.getParticipants().size();
	    	
	    	int profileCell = 0;
	    	for(int i=0; i<participantCount; i++) {
	    		PixParticipant participant = comp.getParticipants().get(i);
	    		participants[profileCell].setParticipant(participant);
	    		profileCell++;
	    	}
	    	for(;profileCell < MAX_PARTICIPANTS_NUM; profileCell++) {
	    		participants[profileCell].setParticipant(null);
	    	}
	    	if (participantsSecondRow != null) {
		    	if (participantCount <= 2) {
		    		participantsSecondRow.setVisibility(View.GONE);
		    	} else {
		    		participantsSecondRow.setVisibility(View.VISIBLE);
		    	}
	    	}
	    }
	    
		tagsContainer.setTags(comp.getTags());
    	
		likesBar.setLikers(comp);
        
	    overlay.bindComposition(comp);
	    
	    if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				if (comp.getIsCompleted() == PixComposition.COMP_IS_COMPLETED) {
					if (adminAddToPopularBtn == null) {
						// can happen when the view holder was created before admin had signed in.
						adminAddToPopularBtn = createAddToPopularBtn();
					}
					if (adminAddToPopularBtn != null) {
						adminAddToPopularBtn.setVisibility(View.VISIBLE);
					}
				} else {
					if (adminAddToPopularBtn != null) {
						adminAddToPopularBtn.setVisibility(View.GONE);
					}
				}
			}
	    }
    }
	
	@Override
	public void onEnterRecycleBin() {    	
		if (compImageContainer != null) {
			compImageContainer.cancelRequest();
			compImageContainer = null;
		}

		for (int i=0; i<MAX_PARTICIPANTS_NUM; i++) {
			ImageContainer imageContainer = participants[i].getImageContainer();
			if (imageContainer != null) {
				imageContainer.cancelRequest();
			}
			participants[i].setParticipant(null);
		}

		compImage.setImageBitmapHolder(null);
	    compImage.setOnDoubleTapListener(null);
	    
	    overlay.removeOverlay();
    	likesBar.clearLikers();
    	tagsContainer.clearTags();
    	data = null;
    }
	
	public void resetLikers() {
		if (likesBar != null) {
			likesBar.resetLikers((PixComposition)data);
		}
	}
	
	private boolean allowDoubleTapToLike() {
		if (this.data != null) {
			PixComposition comp = (PixComposition)data;
			if (comp != null &&
					comp.getIsCompleted() == PixComposition.COMP_IS_COMPLETED) {
				return true;
			}
		}
		return false;
	}
	
	private void onCompImageDoubleTap() {
		if (mListDelegate != null && !mListDelegate.verifyUser()) return;
		if (allowDoubleTapToLike()) {
			final PixComposition comp = (PixComposition)data;
			if (comp != null && comp.getLiked() != 1) {
				if (actionsBar != null) {
					actionsBar.setLikeState(true);
				}
				updateCompLiked(true);
			}
			if (compImageFrame != null) {
				final ImageView likeOverlay = new ImageView(compImageFrame.getContext());
				Random rand = new Random();
				int min = 0, max = PixDefs.likeHeartsRes.length - 1;
				// nextInt is normally exclusive of the top value,
				// so add 1 to make it inclusive
				int randomNum = rand.nextInt(max - min + 1) + min;
				
				likeOverlay.setBackgroundResource(PixDefs.likeHeartsRes[randomNum]);
				
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
							RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.CENTER_IN_PARENT);
				Animation popInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.expand_in);
				final Animation popOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.fade_out);
				if (popInAnimation != null) {
					likeOverlay.setAnimation(popInAnimation);
				}
				compImageFrame.addView(likeOverlay, params);
				new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
					@Override
					public void run() {
						if (likeOverlay != null) {
							likeOverlay.setAnimation(popOutAnimation);
							likeOverlay.setVisibility(View.GONE);
						}
					}
				}, 150);
				
			}
		}
	}
	
	public Bitmap getCompImage() {
		Bitmap bitmap  = null;
		if (compImage != null) {
			Drawable drawable = compImage.getDrawable();
			if (drawable != null) {
				try {
					bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
				} catch (Exception e){
					//Utils.pixLog("getCompImage", "createBitmap failed");
					e.printStackTrace();
				}
				if (bitmap != null) {
				    Canvas canvas = new Canvas(bitmap); 
				    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
				    drawable.draw(canvas);
				}
			}
		}
		return bitmap;
	}
	
	public String getCompCreator() {
		String compCreator = "";
		if (participants[0] != null && 
				participants[0].getName() != null) {
			compCreator = participants[0].getName();
		}
		return compCreator;
	}

	@Override
	public void handleLastCellFooter() {
		if (footer != null) {
    		footer.setVisibility(View.GONE);
		}
	}

	@Override
	public void onLikeBtnClick(boolean liked) {
		updateCompLiked(liked);
	}
	
	private void updateCompLiked(boolean liked) {
		final PixComposition comp = (PixComposition)data;
		if (comp == null) return;
		
		int likesCount = comp.getLikesCount();
		if (liked) {
			comp.setLiked(1);
			comp.setLikesCount(likesCount+1);
        } else {
        	comp.setLiked(0);
        	comp.setLikesCount(likesCount-1);
        }
        resetLikers();
        
        if (mListDelegate != null) {
        	mListDelegate.onCompLikeClick(comp, liked);
        }
	}

	@Override
	public void onCommentBtnClick(CompCommentsUpdateDelegate commentsUpdateDelegate) {
		final PixComposition comp = (PixComposition)data;
		if (mListDelegate != null) {
			mListDelegate.onCompCommentClick(comp, commentsUpdateDelegate);
		}
	}

	@Override
	public void onSaveClick(View v) {
		final PixComposition comp = (PixComposition)data;
		boolean saved = false;
		PixProgressIndicator.showMessage(v.getContext(), saveToLibStr, true);

		if (comp != null) {
			try {
				Bitmap compImage = this.getCompImage();
				if (compImage != null && Utils.isValidString(comp.getCompId())) {
					Utils.storeBitmapInFile(compImage, "pixplit_"+comp.getCompId()+".jpg");
					saved = true;
				}
			} catch (Exception e) {
				//Utils.pixLog("saveImageToLib", "Failed"+e);
				e.printStackTrace();
			}
		}
		if (saved) {
			PixProgressIndicator.hideMessageWithIndication(true, saveToLibStr);
		} else {
			PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getResources().getString(R.string.failed));
		}
	}

	@Override
	public void onDeleteClick(String compId) {
		if (mListDelegate != null) {
			mListDelegate.deleteCompRequest(compId);
		}
	}
	
	@Override
	public void onResplitClick() {
		final PixComposition comp = (PixComposition)data;
		if (mListDelegate != null) {
			mListDelegate.onCompResplitClick(comp);
		}
	}
	
	@Override
	public void onFlagCompClick(View v) {
		if (mListDelegate != null && !mListDelegate.verifyUser()) return;
		final PixComposition comp = (PixComposition)data;
		PixProgressIndicator.showMessage(v.getContext(), 
				PixApp.getContext().getResources().getString(R.string.sending), true);
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_EVENT_FLAG_SPLIT_PRESSED, null);
		new CompFlagHttpRequest(new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getResources().getString(R.string.failed));
			}
			
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getResources().getString(R.string.flagged));
			}
		}, comp).send();
	}
	
	@Override
	public void onTagsClick() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_PARTICIPANT_AVATAR_PRESSED, null);
    	
    	if (overlay != null) {
    		if (overlay.isOverlayVisible()) {
    			overlay.hideOverlay();
    		} else {
    			overlay.displayOverlay();
    		}
    	}
	}

	@Override
	public void onJoinClick() {
		final PixComposition comp = (PixComposition)data;
		if (mListDelegate != null) {
			mListDelegate.onCompJoinClick(comp);
		}
	}

	@Override
	public void onParticipantClick(PixParticipant participant) {
		if (mListDelegate != null && participant != null) {
			mListDelegate.onProfileClick(participant.getUsername());
		}
		
	}

	@Override
	public void onSingleLikerClick(String liker) {
		if (mListDelegate != null &&
				Utils.isValidString(liker)) {
			mListDelegate.onProfileClick(liker);
		}
	}

	@Override
	public void onLikersClick() {
		final PixComposition comp = (PixComposition)data;
		if (mListDelegate != null) {
			mListDelegate.onCompLikersClick(comp);
		}
	}
	
	@Override
	public boolean verifyUser() {
		if (mListDelegate != null) {
			return mListDelegate.verifyUser();
		}
		return false;
	}
	
	private class CompBitmapFetchHolderDelegate extends BitmapFetchHolderDelegate {

		public CompBitmapFetchHolderDelegate(PixViewHolder holder) {
			super(holder);
		}
		
		@Override
		public void onBitmapFetchFinished(PixViewHolder viewHolder, String url, 
				PixBitmapHolder bitmapHolder) {
			PixCompositionViewHolder mvh = (PixCompositionViewHolder) viewHolder;
			PixComposition comp = (PixComposition) mvh.data;
			
			if (bitmapHolder != null && bitmapHolder.getBitmap() != null) {
				// set the touch listener on the image
				compImage.setOnDoubleTapListener(
						new DoubleTapImageViewListener(PixCompositionViewHolder.this) {
							@Override
							public void onDoubleTap(PixViewHolder viewHolder) {
								onCompImageDoubleTap();
							}
							
							@Override
							public void onClick(PixViewHolder viewHolder) {
								PixCompositionViewHolder mvh = (PixCompositionViewHolder) viewHolder;
								PixComposition comp = (PixComposition) mvh.data;
								// the image had been clicked, call the event listener
								mListDelegate.onCompImageClick(comp);
							}
						});
				actionsBar.setEnabled(true);
				// in case this is a featured comp, send ack to server to inform on a featured view.
				if (comp.isFeatured()) {
					new ViewAckHttpRequest(comp).send();
				}
			} 
		}
		
		@Override
		public void onBitmapFetchFailed(PixViewHolder viewHolder) {
			compImage.setOnDoubleTapListener(
					new DoubleTapImageViewListener(PixCompositionViewHolder.this) {
						@Override
						public void onDoubleTap(PixViewHolder viewHolder) {}
						
						@Override
						public void onClick(PixViewHolder viewHolder) {
							PixCompositionViewHolder mvh = (PixCompositionViewHolder) viewHolder;
							PixComposition comp = (PixComposition) mvh.data;
							// the image had been clicked, call the event listener
							String url = comp.getImage();
							PixImagesLoader.loadImageWithDelegate(url, 
		    						compImage, 0, R.drawable.reload_image, Priority.HIGH, true, compImageFetcherDelegate);
						}
					});
			actionsBar.setEnabled(true); // in case the image had failed to load, need to be able to flag/delete it.
		}
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	protected PixTextView createAddToPopularBtn() {
		if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				final float btnTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
				final Drawable btnBg = PixApp.getContext().getResources().getDrawable(R.drawable.comp_actions_bar_btn_selector);
				final ColorStateList btnTextcolorSelector = 
						PixApp.getContext().getResources().getColorStateList(R.color.comp_actions_bar_text_color_selector);
				
				PixTextView adminAddCompToPopularBtn = new PixTextView(mListDelegate.getContext());
				adminAddCompToPopularBtn.setGravity(Gravity.CENTER);
				adminAddCompToPopularBtn.setText(PixApp.getContext().getResources().getString(R.string.admin_add_to_popular));
				adminAddCompToPopularBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
				if (Utils.hasJellyBean()) {
					adminAddCompToPopularBtn.setBackground(btnBg);
		        } else {
		        	adminAddCompToPopularBtn.setBackgroundDrawable(btnBg);
		        }
				adminAddCompToPopularBtn.setTextColor(btnTextcolorSelector);
				actionsBar.addActionToBar(adminAddCompToPopularBtn, new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixComposition comp = (PixComposition) data;
						PixProgressIndicator.showMessage(mListDelegate.getContext(), 
								PixApp.getContext().getResources().getString(R.string.admin_adding_to_popular), true);
						new AdminAddToPopularHttpRequest(new RequestDelegate() {
							@Override
							public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
								PixProgressIndicator.hideMessageWithIndication(false, 
										PixApp.getContext().getResources().getString(R.string.failed));
							}
							@Override
							public void requestFinished(BaseHttpRequest request, PixObject response) {
								PixProgressIndicator.hideMessageWithIndication(true, 
										PixApp.getContext().getResources().getString(R.string.admin_added_to_popular));
							}
						}, comp.getCompId()).send();
					}
				}, 2, 5);
				return adminAddCompToPopularBtn;
			}
		}
		return null;
	}
	
//	@SuppressLint("NewApi")
//	@SuppressWarnings("deprecation")
//	protected PixTextView addRelatedBtn() {
//		final float btnTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
//		final Drawable btnBg = PixApp.getContext().getResources().getDrawable(R.drawable.comp_actions_bar_btn_selector);
//		final ColorStateList btnTextcolorSelector = 
//				PixApp.getContext().getResources().getColorStateList(R.color.comp_actions_bar_text_color_selector);
//		
//		PixTextView relatedBtn = new PixTextView(mListDelegate.getContext());
//		relatedBtn.setGravity(Gravity.CENTER);
//		relatedBtn.setText(PixApp.getContext().getResources().getString(R.string.related));
//		relatedBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
//		if (Utils.hasJellyBean()) {
//			relatedBtn.setBackground(btnBg);
//        } else {
//        	relatedBtn.setBackgroundDrawable(btnBg);
//        }
//		relatedBtn.setTextColor(btnTextcolorSelector);
//		actionsBar.addActionToBar(relatedBtn, new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				PixComposition comp = (PixComposition) data;
//				if (mListDelegate != null) {
//					mListDelegate.onCompRelatedClick(comp);
//				}
//			}
//		}, 2, 2);
//		return relatedBtn;
//	}
}
