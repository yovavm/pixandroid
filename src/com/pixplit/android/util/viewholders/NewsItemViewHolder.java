package com.pixplit.android.util.viewholders;

import java.text.ParseException;
import java.util.Date;

import android.view.View;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixNotification;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class NewsItemViewHolder extends PixViewHolder{
	NewsProviderDelegate mNewsDelegate;
	PixBaseListDelegate mListDelegate;
	
	private static final String notificationTypeLikedStr = "liked";
	private static final String notificationTypeCommentedStr = "commented";
	private static final String notificationTypeFollowedStr = "followed";
	private static final String notificationTypeJoinedStr = "joined";
	private static final String notificationTypeFbFriendJoinedStr = "fbFriendJoined";
	private static final String notificationTypeInvitedStr = "invited";
	
	private static final int notificationTypeUnknown = -1;
	private static final int notificationTypeLiked = 0;
	private static final int notificationTypeCommented = 1;
	private static final int notificationTypeFollowed = 2;
	private static final int notificationTypeJoined = 3;
	private static final int notificationTypeFbFriendJoined = 4;
	private static final int notificationTypeInvited = 5;
	
	private static final String notificationTypeLikedBody = PixApp.getContext().getResources().getString(R.string.liked_your_split);
	private static final String notificationTypeCommentedBody = PixApp.getContext().getResources().getString(R.string.commented_on_your_split);
	private static final String notificationTypeFollowedBody = PixApp.getContext().getResources().getString(R.string.started_following_you);
	private static final String notificationTypeJoinedBody = PixApp.getContext().getResources().getString(R.string.joined_your_split);
	private static final String notificationTypeFbFriendJoinedBody = PixApp.getContext().getResources().getString(R.string.facebook_friend_joins);
	private static final String notificationTypeInvitedBody = PixApp.getContext().getResources().getString(R.string.invited_you_to_a_split);
	
	private RelativeLayout newsItem;
	private PixImageView newsItemPic;
	private ImageContainer newsItemPicContainer;
	private TextView newsItemTitle;
	private TextView newsItemBody;
	private TextView newsItemTime;
	
	public interface NewsProviderDelegate {
		public boolean isItemNew(String itemInsertDate);
	}
	
	public NewsItemViewHolder(View v) {
		newsItem = (RelativeLayout) v.findViewById(R.id.newsItem);
		newsItemPic = (PixImageView) v.findViewById(R.id.newsItemPic);
        newsItemTitle = (TextView) v.findViewById(R.id.newsItemTitle);
        newsItemBody = (TextView) v.findViewById(R.id.newsItemBody);
        newsItemTime = (TextView) v.findViewById(R.id.newsItemTime);
    }
	
	public void init(final PixBaseListDelegate mListDelegate, NewsProviderDelegate newsDelegate) {
		this.mNewsDelegate = newsDelegate;
		this.mListDelegate = mListDelegate;
		// set the click listeners 
		if (newsItem != null) {
			newsItem.setOnClickListener(new OnViewHolderClickListener(this) {

                public void onClick(View v, PixViewHolder viewHolder) {
                	NewsItemViewHolder mvh = (NewsItemViewHolder) viewHolder;
                	PixNotification notification = (PixNotification) mvh.data;
                	onNewsItemClick(notification);
            	}
            });
        }
        // set the click listeners 
        if (newsItemTitle != null){
        	newsItemTitle.setOnClickListener(new OnViewHolderClickListener(this) {
                public void onClick(View v, PixViewHolder viewHolder) {
                	NewsItemViewHolder mvh = (NewsItemViewHolder) viewHolder;
                	PixNotification notification = (PixNotification) mvh.data;
                    // the image had been clicked, call the event listener
                	PixAppState.logAppEvent(PixDefs.APP_SCREEN_NEWS, PixDefs.APP_EVENT_NEWS_ITEM_PROFILE_PRESSED, null);
                    mListDelegate.onProfileClick(notification.getUsername());
                    
                    setViewAsOld(notification);
                }
            });
        }
	}

	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	PixNotification notification = (PixNotification)data; 
    	Date itemDate = null;
    	int notificationType = getNotificationType(notification);
	    
    	setViewReadState(notification);
    	
    	if (notificationType == notificationTypeFollowed ||
    		notificationType == notificationTypeFbFriendJoined) {
    		newsItemPicContainer = PixImagesLoader.loadImage(notification.getPicture(), newsItemPic, 
    												0, 0, Priority.HIGH, false);
    	} else {
    		newsItemPicContainer = PixImagesLoader.loadImage(notification.getCompUrl(), newsItemPic, 
					0, 0, Priority.HIGH, false);
    	}
	    
    	if (newsItemTime != null) {
	    	try {
	    		itemDate = PixDateFormat.getPixDateInstance().parse(notification.getDate());
	    		newsItemTime.setText(PixDateFormat.getPixDateInstance().humanFormat(itemDate));
    		}catch (ParseException e) {
    		    e.printStackTrace();  
    		}
	    }
    	
	    if (newsItemTitle != null) {
	    	if (Utils.isValidString(notification.getFullName())) {
		    	newsItemTitle.setText(notification.getFullName());
		    } 
		}
	    
	    if (newsItemBody != null) {
		    if (Utils.isValidString(notification.getFullName())) {
		    	newsItemBody.setText(getNotificationBody(notificationType));
		    }
	    }
	}
	
	private int getNotificationType(PixNotification notification) {
		int type = notificationTypeUnknown;
		if (notification != null && notification.getNotificationType() != null) {
    		if (notification.getNotificationType().equalsIgnoreCase(notificationTypeFollowedStr)) {
    			type = notificationTypeFollowed;
    		} else if (notification.getNotificationType().equalsIgnoreCase(notificationTypeLikedStr)) {
    			type = notificationTypeLiked;
    		} else if (notification.getNotificationType().equalsIgnoreCase(notificationTypeJoinedStr)) {
    			type = notificationTypeJoined;
    		} else if (notification.getNotificationType().equalsIgnoreCase(notificationTypeCommentedStr)) {
    			type = notificationTypeCommented;
    		} else if (notification.getNotificationType().equalsIgnoreCase(notificationTypeFbFriendJoinedStr)) {
    			type = notificationTypeFbFriendJoined;
    		} else if (notification.getNotificationType().equalsIgnoreCase(notificationTypeInvitedStr)) {
    			type = notificationTypeInvited;
    		}
		}
		return type;
	}
	
	private void onNewsItemClick(PixNotification notification) {
		if (notification != null) {
			int notificationType = this.getNotificationType(notification);
			if ((notificationType == notificationTypeFollowed) ||
	    		(notificationType == notificationTypeFbFriendJoined))	{
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_NEWS, PixDefs.APP_EVENT_NEWS_ITEM_PROFILE_PRESSED, null);
	    		mListDelegate.onProfileClick(notification.getUsername());
	    	} else {
	    		PixAppState.logAppEvent(PixDefs.APP_SCREEN_NEWS, PixDefs.APP_EVENT_NEWS_ITEM_SPLIT_PRESSED, null);
				mListDelegate.onCompImageClick(notification.getCompId());
			}
	    	setViewAsOld(notification);
		}
	}
	
	private String getNotificationBody(int notificationType) {
		String body = "";
		switch(notificationType) {
		case notificationTypeFollowed: 
			body = notificationTypeFollowedBody;
			break;
		case notificationTypeLiked:
			body = notificationTypeLikedBody;
			break;
		case notificationTypeJoined:
			body = notificationTypeJoinedBody;
			break;
		case notificationTypeCommented:
			body = notificationTypeCommentedBody;
			break;
		case notificationTypeFbFriendJoined:
			body = notificationTypeFbFriendJoinedBody;
			break;
		case notificationTypeInvited:
			body = notificationTypeInvitedBody;
			break;
		default:
			break;
		}
		return body;
	}
	
	private void setViewReadState(PixNotification notification) {
		 if (notification != null) {
			 ViewParent layout = newsItemTitle.getParent();
		    	if (layout instanceof RelativeLayout) {
		    		int notificationReadState = notification.getState();
		    		if (notificationReadState == PixNotification.NOTIFICATION_STATE_UNKNOWN) {
		    			notificationReadState = checkNotificationReadState(notification);
		    		}
		    		
		    		if (notificationReadState == PixNotification.NOTIFICATION_IS_NEW) {
		    			setViewAsNew(notification);
		    		} else {
		    			setViewAsOld(notification);
		    		}
		    	}
		 }
	}
	private int checkNotificationReadState(PixNotification notification) {
		int notificationReadState;
		
		if (mNewsDelegate.isItemNew(notification.getInsertDate())) {
			notificationReadState = PixNotification.NOTIFICATION_IS_NEW;
		} else {
			notificationReadState = PixNotification.NOTIFICATION_IS_OLD;
		}
		return notificationReadState;
	}
	
	private void setViewAsNew(PixNotification notification) {
		if (newsItem != null) {
			newsItem.setBackgroundResource(R.drawable.news_new_item_btn_selector);
		}
		notification.setState(PixNotification.NOTIFICATION_IS_NEW);
	}
	
	private void setViewAsOld(PixNotification notification) {
		if (newsItem != null) {
			newsItem.setBackgroundResource(R.drawable.news_old_item_btn_selector);
		}
		notification.setState(PixNotification.NOTIFICATION_IS_OLD);
	}
	
	@Override
    public void onEnterRecycleBin() {
    	if (newsItemPicContainer != null) newsItemPicContainer.cancelRequest();
    	newsItemPic.setImageBitmapHolder(null);
    }
}