package com.pixplit.android.util.viewholders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.pixplit.android.data.FacebookPhoto;

public class FbItemsPack {
	private FacebookPhoto[] mItemsPack;
	private int packSize;
	
	public FbItemsPack(int packSize, Iterator<FacebookPhoto> iter){
		mItemsPack = new FacebookPhoto[packSize];
		this.packSize = packSize;
		
		for (int i=0; i<packSize; i++){
			if (iter.hasNext()){
				mItemsPack[i] = iter.next();
			} else {
				mItemsPack[i] = null;
			}
		}
	}
	
	public FacebookPhoto getItem(int position){
		FacebookPhoto response = null;
		if (position < packSize) {
			response = mItemsPack[position];
		}
		return response;
	}
	
	public int getPackSize() {
		return packSize;
	}
	
	public static ArrayList<FbItemsPack> createCompsPackList(List<FacebookPhoto> items, int packSize){
		ArrayList<FbItemsPack> mPackList = new ArrayList<FbItemsPack>();
		
		if (items != null){
			Iterator<FacebookPhoto> iter = items.iterator();
			
			while (iter.hasNext()){
				mPackList.add(new FbItemsPack(packSize, iter));
			}
		}
		return mPackList;
	}
}