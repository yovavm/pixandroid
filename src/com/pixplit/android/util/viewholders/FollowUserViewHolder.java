package com.pixplit.android.util.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixFollowUser;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class FollowUserViewHolder extends PixViewHolder{
	public RelativeLayout viewLayout = null;
	public PixImageView userPic;
	private ImageContainer userPicContainer;
	public TextView userName;
	public TextView userStatus;
	public ImageView followStatus;
	
	public FollowUserViewHolder(View v) {
		userPic = (PixImageView) v.findViewById(R.id.userPic);
        userName = (TextView) v.findViewById(R.id.userName);
        userStatus = (TextView) v.findViewById(R.id.userStatus);
        followStatus = (ImageView) v.findViewById(R.id.followStatus);
        if (v instanceof RelativeLayout) {
        	viewLayout = (RelativeLayout)v;
        } 
    }
	
	public void init(final PixBaseListDelegate mListDelegate) {
		// set the click listeners 
		if (viewLayout != null) {
			viewLayout.setOnClickListener(new UserOnClickListener(this) {
				@Override
				public void onClick(PixFollowUser user, FollowUserViewHolder viewHolder) {
					user.setSelected(!user.isSelected());
					viewHolder.updateSelectedView(user.isSelected());
				}
			});
		}
		
		if (followStatus != null){
        	followStatus.setBackgroundResource(R.drawable.check_off);
        }
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
		PixFollowUser pixUser = (PixFollowUser)data; 
		if (viewLayout != null) {
			viewLayout.setBackgroundResource(R.drawable.android_blue_btn_selector);
		}
	    if (pixUser != null) {
	    	userPicContainer = PixImagesLoader.loadImage(pixUser.getPicture(), userPic, 
	    												0, 0, Priority.HIGH, false);
	    	
		    if (userName != null) {
		    	if (Utils.isValidString(pixUser.getFullName())) {
			    	this.userName.setText(pixUser.getFullName());
			    }
		    }
		    
		    if (userStatus != null) {
		    	if (pixUser.isFeatured()) {
		    		this.userStatus.setText(R.string.featured);
		    		this.userStatus.setVisibility(View.VISIBLE);
		    	} else {
		    		this.userStatus.setVisibility(View.INVISIBLE);
		    	}
		    }
		    
		    updateSelectedView(pixUser.isSelected());
	    }
	}
		
	public void updateSelectedView(boolean isSelected) {
		if (followStatus != null) {
	    	if (isSelected){
	    		followStatus.setBackgroundResource(R.drawable.check_on);
	    	} else {
	    		followStatus.setBackgroundResource(R.drawable.check_off);
	    	}
	    }
	}

	@Override
    public void onEnterRecycleBin() {
    	if (userPicContainer != null) userPicContainer.cancelRequest();
    	userPic.setImageBitmapHolder(null);
    }
	
	private abstract static class UserOnClickListener extends OnViewHolderClickListener {

		public UserOnClickListener(FollowUserViewHolder holder) {
			super(holder);
		}

		@Override
		public void onClick(View v, PixViewHolder viewHolder) {
			FollowUserViewHolder mvh = (FollowUserViewHolder) viewHolder;
			PixFollowUser user = (PixFollowUser) mvh.data;
        	onClick(user, mvh);
        }
		
		public abstract void onClick(PixFollowUser user, FollowUserViewHolder viewHolder);
	}
}
