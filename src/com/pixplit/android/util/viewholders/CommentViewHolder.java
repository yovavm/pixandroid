package com.pixplit.android.util.viewholders;

import java.text.ParseException;
import java.util.Date;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComment;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixDateFormat;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class CommentViewHolder extends PixViewHolder{
	
	private RelativeLayout commentLayout;
	private PixImageView commentPic;
	private ImageContainer commentPicContainer;
	private TextView commenterName;
	private TextView comment;
	private TextView commentTime;
	
	Drawable commentColor = PixApp.getContext().getResources().getDrawable(R.drawable.comment_color_selector);
	
	public interface CommentViewDelegate {
		public void onCommentClick(PixComment comment);
		public void onCommentLongClick(PixComment comment);
	}
	
	public CommentViewHolder(View v) {
		commentLayout = (RelativeLayout) v.findViewById(R.id.commentLayout);
		commentPic = (PixImageView) v.findViewById(R.id.commentPic);
        commenterName = (TextView) v.findViewById(R.id.commenterName);
        comment = (TextView) v.findViewById(R.id.comment);
        commentTime = (TextView) v.findViewById(R.id.commentTime);
        
	}
	
	@SuppressWarnings("deprecation")
	public void init(final CommentViewDelegate mDelegate) {
		// set the click listeners 
        if (commentLayout != null){
        	commentLayout.setOnClickListener(new OnViewHolderClickListener(this) {
                public void onClick(View v, PixViewHolder viewHolder) {
                	CommentViewHolder mvh = (CommentViewHolder) viewHolder;
                	PixComment comment = (PixComment) mvh.data;
                    // the image had been clicked, call the event listener
                	if (mDelegate != null) {
                		mDelegate.onCommentClick(comment);
                	}
                }
            });
        	commentLayout.setOnLongClickListener(new OnViewHolderLongClickListener(this) {
				@Override
				public void onLongClick(View v, PixViewHolder viewHolder) {
					CommentViewHolder mvh = (CommentViewHolder) viewHolder;
                	PixComment comment = (PixComment) mvh.data;
                    // the image had been clicked, call the event listener
                    mDelegate.onCommentLongClick(comment);
				}
			});
        	if (Utils.hasJellyBean()) {
        		commentLayout.setBackground(commentColor);
        	} else {
        		commentLayout.setBackgroundDrawable(commentColor);
        	}
        }        
	}

	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	PixComment pixComment = (PixComment)data; 
	    
    	commentPicContainer = PixImagesLoader.loadImage(pixComment.getPicture(), commentPic, 
    													0, 0, Priority.HIGH, false);
	    
	    if (commenterName != null) {
	    	if (Utils.isValidString(pixComment.getFullName())) {
		    	commenterName.setVisibility(View.VISIBLE);
		    	commenterName.setText(pixComment.getFullName());
		    } else {
		    	commenterName.setVisibility(View.GONE);
		    }
		}
	    
	    if (comment != null) {
		    if (Utils.isValidString(pixComment.getFullName())) {
		    	comment.setVisibility(View.VISIBLE);
		    	comment.setText(pixComment.getComment());
		    } else {
		    	comment.setVisibility(View.GONE);
		    }
	    }
	    
	    if (commentTime != null) {
		    if (Utils.isValidString(pixComment.getFullName())) {
		    	commentTime.setVisibility(View.VISIBLE);
		    	try {
		    		Date date = PixDateFormat.getPixDateInstance().parse(pixComment.getDate());
		    		commentTime.setText(PixDateFormat.getPixDateInstance().humanFormat(date));
	    		}catch (ParseException e) {
	    		    e.printStackTrace();  
	    		}
		    } else {
		    	commentTime.setVisibility(View.GONE);
		    }
	    }
		
	}
	
	@Override
    public void onEnterRecycleBin() {
    	if (commentPicContainer != null) commentPicContainer.cancelRequest();
    	commentPic.setImageBitmapHolder(null);
    }
}
