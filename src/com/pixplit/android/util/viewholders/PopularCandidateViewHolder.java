package com.pixplit.android.util.viewholders;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminApprovePopularHttpRequest;
import com.pixplit.android.http.request.AdminRemovePopularCandidateHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.ui.elements.PixTextView;
import com.pixplit.android.util.Utils;

public class PopularCandidateViewHolder extends PixCompositionViewHolder{
	Context context;
	PopularCandidateViewDelegate mDelegate;
	
	private static final float btnTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
	private static final Drawable addBtnBg = PixApp.getContext().getResources().getDrawable(R.drawable.comp_actions_bar_btn_selector);
	private static final Drawable discardBtnBg = PixApp.getContext().getResources().getDrawable(R.drawable.comp_actions_bar_btn_selector);
	private static final ColorStateList colorSelector = PixApp.getContext().getResources().getColorStateList(R.color.comp_actions_bar_text_color_selector);
	
	public interface PopularCandidateViewDelegate {
		public void onCandidateApproved(String compId);
		public void onCandidateDiscard(String compId);
		public void onAdminNotAuthenticated();
	}
	
	public PopularCandidateViewHolder(Context context, View v, PixBaseListDelegate listDelegate,
								PopularCandidateViewDelegate delegate) {
		super(v, listDelegate);
		this.context = context;
		this.mDelegate = delegate;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void init() {
		super.init();
		if (this.actionsBar != null) {
			PixTextView addToPopular = new PixTextView(context);
			
			addToPopular.setText("Approve Candidate");
			addToPopular.setGravity(Gravity.CENTER);
			addToPopular.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
			if (Utils.hasJellyBean()) {
				addToPopular.setBackground(addBtnBg);
	        } else {
	        	addToPopular.setBackgroundDrawable(addBtnBg);
	        }
			addToPopular.setTextColor(colorSelector);
			
			this.actionsBar.addActionToBar(addToPopular, new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (data == null || !(data instanceof PixComposition)) return;
					final PixComposition comp = (PixComposition)data;
					new AdminApprovePopularHttpRequest(new RequestDelegate() {
						@Override
						public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
							if (mDelegate != null) {
								mDelegate.onAdminNotAuthenticated();
							}
						}
						
						@Override
						public void requestFinished(BaseHttpRequest request, PixObject response) {}
					}, comp.getCompId()).send();
					if (mDelegate != null) {
						mDelegate.onCandidateApproved(comp.getCompId());
					}
				}
			}, 2, 0);
			
			PixTextView removeFromPopular = new PixTextView(context);
			
			removeFromPopular.setText("Discard Candidate");
			removeFromPopular.setGravity(Gravity.CENTER);
			removeFromPopular.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
			if (Utils.hasJellyBean()) {
				removeFromPopular.setBackground(discardBtnBg);
	        } else {
	        	removeFromPopular.setBackgroundDrawable(discardBtnBg);
	        }
			removeFromPopular.setTextColor(colorSelector);
			
			this.actionsBar.addActionToBar(removeFromPopular, new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (data == null || !(data instanceof PixComposition)) return;
					final PixComposition comp = (PixComposition)data;
					new AdminRemovePopularCandidateHttpRequest(new RequestDelegate() {
						@Override
						public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
							if (mDelegate != null) {
								mDelegate.onAdminNotAuthenticated();
							}
						}
						
						@Override
						public void requestFinished(BaseHttpRequest request, PixObject response) {}
					}, comp.getCompId()).send();
					if (mDelegate != null) {
						mDelegate.onCandidateDiscard(comp.getCompId());
					}
				}
			}, 2, 1);
		}
	}
	
	@Override
	protected PixTextView createAddToPopularBtn() {
		return null; // no need to add the default add-to-popular button.
	}
}
