package com.pixplit.android.util.viewholders;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class ParticipantViewController {
	ParticipantViewDelegate mDelegate;
	ParticipantBitmapFetcherDelegate mBitmapFetcherDelegate;
	
	// data
	private PixParticipant mParticipant;
	
	// views
	private View mCell;
	private PixImageView mImage;
	private ImageView mOverlay;
	private ImageContainer mImageContainer;
	private TextView mName;
	private TextView mTime;
	
	public interface ParticipantViewDelegate {
		public void onParticipantClick(PixParticipant participant);
	}
	
	public ParticipantViewController(ParticipantViewDelegate delegate) {
		mDelegate = delegate;
		mBitmapFetcherDelegate = new ParticipantBitmapFetcherDelegate();
	}
	
	public ImageContainer getImageContainer() {
		return mImageContainer;
	}
	
	private void onProfileClick() {
		if (mDelegate != null) {
			mDelegate.onParticipantClick(mParticipant);
		}
	}

	public void init(View v) {
		if (v == null) return;
		
		mCell = v;
		FrameLayout profileFrame = (FrameLayout) v.findViewById(R.id.participantImage);
        if (profileFrame != null) {
        	mImage = (PixImageView)profileFrame.findViewById(R.id.profilePic);
        	mOverlay = (ImageView)profileFrame.findViewById(R.id.profileOverlay);
        }
        mName = (TextView) v.findViewById(R.id.participantName);
        mTime = (TextView) v.findViewById(R.id.participantTime);

        mCell.setVisibility(View.GONE);
	}
	
	public void setParticipant(PixParticipant participant) {
		if (mParticipant == participant) return;
		mParticipant = participant;
		
		mOverlay.setImageResource(R.drawable.feed_profile_pic_placeholder);
		
		if (mParticipant != null) {
			if (Utils.isValidString(mParticipant.getPicture())) {
				mImageContainer = 
						PixImagesLoader.loadImage(mParticipant.getPicture(), 
								mBitmapFetcherDelegate, Priority.HIGH);
			}
			if (Utils.isValidString(mParticipant.getFullName())) {
				mName.setText(mParticipant.getFullName());
			}
			if (Utils.isValidString(mParticipant.getFormatedDate())) {
				mTime.setText(mParticipant.getFormatedDate());
			}
			mCell.setVisibility(View.VISIBLE);
		} else {
			// no participant to display. hide this view
			mCell.setVisibility(View.GONE);
			// release bitmap holder
			mImage.setImageBitmapHolder(null);
		}
	}

	public String getName() {
		if (mParticipant != null) {
			return mParticipant.getFullName();
		}
		return null;
	}
	
	private class ParticipantBitmapFetcherDelegate implements BitmapFetcherDelegate {

		@Override
		public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapHolder) {
			mImage.setImageBitmapHolder(bitmapHolder);
			mOverlay.setImageResource(R.drawable.feed_profile_pic_overlay);
			mCell.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onProfileClick();
				}
			});
		}
		@Override
		public void onBitmapFetchFailed() {}
	}

}
