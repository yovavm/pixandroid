package com.pixplit.android.util.viewholders;

import android.view.View;

import com.pixplit.android.R;

public class SuggestedUserViewHolder extends UserViewHolder{
	
	public SuggestedUserViewHolder(View v, int userActionType, UserActionsDelegate userViewDelegate) {
		super(v, userActionType, userViewDelegate);
    }
	
	@Override
	protected void initUserActionBtn() {
		if (userAction != null) {
    		userAction.setBackgroundResource(R.drawable.profile_remove_suggested_btn_selector);
        }
	}
	
}
