package com.pixplit.android.util.viewholders;

import android.view.View;

public abstract class OnViewHolderLongClickListener implements View.OnLongClickListener {
	

    private PixViewHolder mViewHolder;

    /**
     * @param holder The holder of the clickable item
     */
    public OnViewHolderLongClickListener(PixViewHolder holder) {
        mViewHolder = holder;
    }

    // delegates the long click event
    @Override
    public boolean onLongClick(View v) {
        onLongClick(v, mViewHolder);
		return true;
    }

    /**
     * Implement your click behavior here
     * @param v  The clicked view.
     */
    public abstract void onLongClick(View v, PixViewHolder viewHolder);

}
