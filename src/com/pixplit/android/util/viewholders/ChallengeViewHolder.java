package com.pixplit.android.util.viewholders;

import java.util.HashMap;

import android.view.View;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixChallenge;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class ChallengeViewHolder extends PixViewHolder{
	
	private final PixImageView challengeImage;
	private ImageContainer challengeImageContainer;
	
	public ChallengeViewHolder(View v) {
		challengeImage = (PixImageView) v.findViewById(R.id.challengeImage);
	}
	
	public void init(final PixBaseListDelegate mListDelegate) {
		// set the click listeners 
        if (challengeImage != null){
        	challengeImage.setOnClickListener(new OnViewHolderClickListener(this) {

                public void onClick(View v, PixViewHolder viewHolder) {
                	ChallengeViewHolder mvh = (ChallengeViewHolder) viewHolder;
                	PixChallenge challenge = (PixChallenge) mvh.data;
                    // the image had been clicked, call the event listener
                	HashMap<String, String> context = new HashMap<String, String>();
            		context.put(PixDefs.APP_PARAM_TAG_NAME, challenge.getTag());
                	PixAppState.logAppEvent(PixDefs.APP_SCREEN_FEATURED, PixDefs.APP_EVENT_VIEW_LISTING_PRESSED, context);
                	
                    mListDelegate.onTagClick(challenge.getTag());
                }
            });
        }
    }

	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	PixChallenge pixChallenge = (PixChallenge)data; 
    	challengeImageContainer = PixImagesLoader.loadImage(pixChallenge.getBanner(), challengeImage, 0, 0, Priority.HIGH, false);
	}
	
	@Override
    public void onEnterRecycleBin() {
    	if (challengeImageContainer != null) challengeImageContainer.cancelRequest();
    	challengeImage.setImageBitmapHolder(null);
    }
	
}
