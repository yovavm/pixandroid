package com.pixplit.android.util.viewholders;

import android.view.View;
import android.view.View.OnClickListener;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.FacebookAlbum;
import com.pixplit.android.data.FacebookPhoto;
import com.pixplit.android.data.FacebookPhotos;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.fb.FbAlbumPickDelegate;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.FbPhotosHttpRequest;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.PixMediumTextView;
import com.pixplit.android.ui.elements.PixTextView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class FbAlbumViewHolder extends PixViewHolder{
	private View albumLayout;
	private PixImageView image;
	private PixMediumTextView name;
	private PixTextView size;
	private ImageContainer imagesContainer;
	
	private static final String xPhotos = PixApp.getContext().getResources().getString(R.string.x_photos);
	
	public FbAlbumViewHolder(View v) {
		albumLayout = v;
		if (albumLayout == null) return;
		
		this.image = (PixImageView)v.findViewById(R.id.albumCover);
		this.name = (PixMediumTextView)v.findViewById(R.id.albumName);
		this.size = (PixTextView)v.findViewById(R.id.albumSize);
	}
	
	public void init(final FbAlbumPickDelegate mDelegate) {
		if (albumLayout != null) {
			albumLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null &&
							data != null &&
							data instanceof FacebookAlbum) {
						mDelegate.onAlbumPick((FacebookAlbum)data);
					}
				}
			});
		}
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
		final FacebookAlbum album = (FacebookAlbum)data; 
        if (album == null) return;
        if (albumLayout != null) {
			albumLayout.setBackgroundResource(R.drawable.android_blue_btn_selector);
        }
        final String albumId = album.getId();
    	if (image != null && Utils.isValidString(albumId)) {
    		// fetch the list of pictures in the album and select the first one as a cover
    		FbPhotosHttpRequest request = new FbPhotosHttpRequest(new RequestDelegate() {
					
					@Override
					public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {}
					
					@Override
					public void requestFinished(BaseVolleyHttpRequest request,
							PixObject response) {
						if (response != null && response instanceof FacebookPhotos) {
							FacebookPhotos items = (FacebookPhotos)response;
							if (items.getItems() == null || items.getItems().size() <= 0) return;
							FacebookPhoto firstPhoto = items.getItems().get(0);
							if (firstPhoto == null) return;
							String firstPhotoThumb = firstPhoto.getImageThumb();
							if (!Utils.isValidString(firstPhotoThumb)) return;
							imagesContainer = PixImagesLoader.loadImage(
									firstPhotoThumb, 
									image, 0, 0, Priority.HIGH, false);
						}
					}
				}, 
				String.format(PixHttpAPI.FACEBOOK_ALBUM_PHOTOS_URL, albumId), 
				Priority.HIGH);
    		PixAppState.getState().getRequestQueue().add(request);
    	}
    	if (name != null) {
    		name.setText(album.getName());
    	}
    	if (size != null) {
    		size.setText(String.format(xPhotos, album.getSize()));
    	}
	}
	
	@Override
    public void onEnterRecycleBin() {
		if (imagesContainer != null) {
			imagesContainer.cancelRequest();
			imagesContainer = null;
		}
		image.setImageBitmapHolder(null);
	}
}
