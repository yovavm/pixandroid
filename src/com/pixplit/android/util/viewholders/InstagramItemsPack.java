package com.pixplit.android.util.viewholders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.pixplit.android.data.InstagramItem;

public class InstagramItemsPack {
	private InstagramItem[] mItemsPack;
	private int packSize;
	
	public InstagramItemsPack(int packSize, Iterator<InstagramItem> iter){
		mItemsPack = new InstagramItem[packSize];
		this.packSize = packSize;
		
		for (int i=0; i<packSize; i++){
			if (iter.hasNext()){
				mItemsPack[i] = iter.next();
			} else {
				mItemsPack[i] = null;
			}
		}
	}
	
	public InstagramItem getItem(int position){
		InstagramItem response = null;
		if (position < packSize) {
			response = mItemsPack[position];
		}
		return response;
	}
	
	public int getPackSize() {
		return packSize;
	}
	
	public static ArrayList<InstagramItemsPack> createItemsPackList(List<InstagramItem> items, int packSize){
		ArrayList<InstagramItemsPack> mPackList = new ArrayList<InstagramItemsPack>();
		
		if (items != null){
			Iterator<InstagramItem> iter = items.iterator();
			
			while (iter.hasNext()){
				mPackList.add(new InstagramItemsPack(packSize, iter));
			}
		}
		return mPackList;
	}
}