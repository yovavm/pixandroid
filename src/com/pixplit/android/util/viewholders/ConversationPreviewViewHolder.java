package com.pixplit.android.util.viewholders;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.data.PixChatMessagePreview;
import com.pixplit.android.data.PixChatUser;
import com.pixplit.android.data.PixConversationPreview;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixConversationDateFormat;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class ConversationPreviewViewHolder extends PixViewHolder{
	private ConversationPreviewDelegate mDelegate;
	
	private RelativeLayout conversationPreview;
	private PixImageView userPic;
	private ImageContainer userPicContainer;
	private TextView userName;
	private TextView messagePreview;
	private TextView messageTime;
	private ImageView splitMsgIcon;
	private ImageView msgUnreadIcon;
	
	Drawable readColor = PixApp.getContext().getResources().getDrawable(R.drawable.conversation_preview_read_color_selector);
	Drawable unreadColor = PixApp.getContext().getResources().getDrawable(R.drawable.conversation_preview_unread_color_selector);
	public interface ConversationPreviewDelegate {
		public void onConversationClick(PixConversationPreview conversation);
		public void onConversationLongPress(PixConversationPreview conversation);
	}
	
	public ConversationPreviewViewHolder(View v, ConversationPreviewDelegate delegate) {
		this.mDelegate = delegate;
		
		if (v != null) {
			conversationPreview = (RelativeLayout)v.findViewById(R.id.conversationPreview);
			userPic = (PixImageView) v.findViewById(R.id.userPic);
	        userName = (TextView) v.findViewById(R.id.userName);
	        messagePreview = (TextView) v.findViewById(R.id.messagePreview);
	        messageTime = (TextView) v.findViewById(R.id.messageTime);
	        splitMsgIcon = (ImageView) v.findViewById(R.id.splitMsgIcon);
	        msgUnreadIcon = (ImageView) v.findViewById(R.id.msgUnreadIcon);
		}
        
	}
	
	public void init(final PixBaseListDelegate mListDelegate) {
		if (conversationPreview != null) {
			conversationPreview.setOnClickListener(new OnViewHolderClickListener(this) {
				@SuppressWarnings("deprecation")
				@Override
				public void onClick(View v, PixViewHolder viewHolder) {
					ConversationPreviewViewHolder mvh = (ConversationPreviewViewHolder) viewHolder;
                	if (mvh == null || mvh.data == null) return;
                	
                	PixConversationPreview conversation = (PixConversationPreview) mvh.data;
                	conversation.setReadStatus(true);
                	if (conversationPreview != null) {
                		if (Utils.hasJellyBean()) {
                			conversationPreview.setBackground(readColor);
                		} else {
                			conversationPreview.setBackgroundDrawable(readColor);
                		}
                		if (msgUnreadIcon != null) {
            				msgUnreadIcon.setVisibility(View.GONE);
            			}
                	}
                	if (mDelegate != null) {
                		mDelegate.onConversationClick(conversation);
                	}
				}
			});
			
			conversationPreview.setOnLongClickListener(new OnViewHolderLongClickListener(this) {
				@Override
				public void onLongClick(View v, PixViewHolder viewHolder) {
					ConversationPreviewViewHolder mvh = (ConversationPreviewViewHolder) viewHolder;
                	if (mvh == null || mvh.data == null) return;
                	
                	PixConversationPreview conversation = (PixConversationPreview) mvh.data;
                	if (mDelegate != null) {
                		mDelegate.onConversationLongPress(conversation);
                	}
				}
			});
		} 
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
		if (data == null || conversationPreview == null) return;
		PixConversationPreview conversation = (PixConversationPreview)data; 
		
		if (conversation.isRead()) {
			if (Utils.hasJellyBean()) {
				conversationPreview.setBackground(readColor);
			} else {
				conversationPreview.setBackgroundDrawable(readColor);
			}
			if (msgUnreadIcon != null) {
				msgUnreadIcon.setVisibility(View.GONE);
			}
		} 
		else {
			if (Utils.hasJellyBean()) {
				conversationPreview.setBackground(unreadColor);
			} else {
				conversationPreview.setBackgroundDrawable(unreadColor);
			}
			if (msgUnreadIcon != null) {
				msgUnreadIcon.setVisibility(View.VISIBLE);
			}
		}
		
		PixChatUser user = null;
		if (conversation.getUsers() != null && 
				conversation.getUsers().size() > 0) {
			user = conversation.getUsers().get(0);
		}
		
		if (user == null) return;
		if (userPic != null) {
			userPicContainer = PixImagesLoader.loadImage(user.getPicture(), userPic, 0, 0, Priority.HIGH, false);
		}
	     
    	if (userName != null && Utils.isValidString(user.getFullName())) {
	    	userName.setText(user.getFullName());
		}

    	PixChatMessagePreview lastMessage = conversation.getLastMessage();
		if (lastMessage == null) return;
	    if (messagePreview != null) {
	    	if (lastMessage.getType() == PixChatMessage.PIX_MESSAGE_TYPE_TEXT) {
			    if (Utils.isValidString(lastMessage.getText())) {
			    	messagePreview.setText(lastMessage.getText());
			    }
			    if (splitMsgIcon != null) {
    				splitMsgIcon.setVisibility(View.GONE);
    			}
	    	} else {
	    		if (lastMessage.getType() == PixChatMessage.PIX_MESSAGE_TYPE_COMP) {
	    			// logged-in user is the sender
	    			messagePreview.setText(PixApp.getContext().getString(R.string.private_split));
	    			if (splitMsgIcon != null) {
	    				splitMsgIcon.setVisibility(View.VISIBLE);
	    			}
	    		}
	    	}
	    }
	    
	    if (messageTime != null) {
	    	if (lastMessage.getDate() != null) {
		    	String dateHumanFormat = PixConversationDateFormat.getPixDateInstance().humanFormat(lastMessage.getDate());
	    		if (Utils.isValidString(dateHumanFormat)) {
	    			messageTime.setText(dateHumanFormat);
	    		} else {
	    			messageTime.setText("");
	    		}
	    	}
	    }
	}
	
	@Override
    public void onEnterRecycleBin() {
		if (userPicContainer != null) userPicContainer.cancelRequest();
		userPic.setImageBitmapHolder(null);
    }
}
