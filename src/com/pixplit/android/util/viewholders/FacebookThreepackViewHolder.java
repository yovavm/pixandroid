package com.pixplit.android.util.viewholders;

import android.view.View;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.FacebookPhoto;
import com.pixplit.android.fb.FbPhotoPickDelegate;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class FacebookThreepackViewHolder extends PixViewHolder{
	
	private PixImageView[] images;
	private ImageContainer[] imagesContainers;
	private int[] imagesIds = {R.id.image1, R.id.image2, R.id.image3};
	private final static int COMPS_PACK_SIZE = 3;
	
	public FacebookThreepackViewHolder(View v) {
		images = new PixImageView[COMPS_PACK_SIZE];
		imagesContainers = new ImageContainer[COMPS_PACK_SIZE];
		for (int i = 0; i<COMPS_PACK_SIZE; i++) {
			images[i] = (PixImageView) v.findViewById(imagesIds[i]);
	    	if (images[i] != null) {
    			images[i].highlightOnPress(true);
	    	}
		}
	}
	
	public void init(final FbPhotoPickDelegate mDelegate) {
		for (int i = 0; i<COMPS_PACK_SIZE; i++) {
			if (images[i] != null) {
				// calculate frame dimensions
				int rowPadding = PixApp.getContext().getResources()
						.getDimensionPixelSize(R.dimen.profile_split_row_horizental_margin) * 2;
				int cellMargin = PixApp.getContext().getResources()
						.getDimensionPixelSize(R.dimen.comps_grid_spacing) * 2;
				int imageFrameDimen = (PixAppState.getState().getFrameWidth() - rowPadding - (cellMargin*COMPS_PACK_SIZE))/COMPS_PACK_SIZE;
				android.view.ViewGroup.LayoutParams params = images[i].getLayoutParams();
				params.width = imageFrameDimen;
				params.height = imageFrameDimen;
				
				images[i].setVisibility(View.VISIBLE);
				images[i].getLayoutParams().width = imageFrameDimen;
				images[i].getLayoutParams().height = imageFrameDimen;
				images[i].setOnClickListener(new OnFbThreepackClickListener(this, i, mDelegate));
	        }
	    }		
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
		FbItemsPack itemsThreepack = (FbItemsPack)data; 
        
    	for (int i = 0; i<COMPS_PACK_SIZE; i++){
    		if (images[i] != null) {
				if (itemsThreepack.getItem(i) != null) {
					images[i].setVisibility(View.VISIBLE);
					String thumbUrl = itemsThreepack.getItem(i).getImageThumb();
					if (thumbUrl != null) {
						imagesContainers[i] = PixImagesLoader.loadImage(thumbUrl, 
		    						images[i], 0, 0, Priority.HIGH, false);
					}
	    		} else {
	    			images[i].setVisibility(View.INVISIBLE);
	    		}
	    	}
    	}
	}
	
	@Override
    public void onEnterRecycleBin() {
    	for (int i = 0; i<COMPS_PACK_SIZE; i++){
    		if (images[i] != null) {
    			images[i].setVisibility(View.VISIBLE);
    			if (imagesContainers[i] != null) imagesContainers[i].cancelRequest();
    			images[i].setImageBitmapHolder(null);
    		}
    	}	
	}

	private static class OnFbThreepackClickListener implements View.OnClickListener {

        private FacebookThreepackViewHolder mViewHolder;
        private int itemIndex;
        private FbPhotoPickDelegate mDelegate;

        public OnFbThreepackClickListener(FacebookThreepackViewHolder holder, int index, FbPhotoPickDelegate listDelegate) {
                mViewHolder = holder;
                itemIndex = index;
                this.mDelegate = listDelegate;
        }

        // delegates the click event
        public void onClick(View v) {
                onClick(v, mViewHolder);
        }

        /**
         * Implement your click behavior here
         * @param v  The clicked view.
         */
        public void onClick(View v, FacebookThreepackViewHolder viewHolder) {
        	FacebookPhoto item = ((FbItemsPack) viewHolder.data).getItem(itemIndex);
            // the image had been clicked, call the event listener
            mDelegate.onPhotoPick(item);
        }
    };
}
