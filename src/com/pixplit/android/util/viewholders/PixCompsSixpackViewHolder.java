package com.pixplit.android.util.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class PixCompsSixpackViewHolder extends PixViewHolder{
	
	private RelativeLayout[] compFrames;
	private PixImageView[] compsImages;
	private ImageContainer[] compsImagesContainers;
	private int[] compsImagesIds = {R.id.comp1, R.id.comp2, R.id.comp3, R.id.comp4, R.id.comp5, R.id.comp6};
	private final int compsNum = 6;
	private TextView footer;
	
	public PixCompsSixpackViewHolder(View v) {
		compFrames = new RelativeLayout[compsNum];
		compsImages = new PixImageView[compsNum];
		compsImagesContainers = new ImageContainer[compsNum];
		for (int i = 0; i<compsNum; i++) {
			compFrames[i] = (RelativeLayout) v.findViewById(compsImagesIds[i]);
	    	if (compFrames[i] != null) {
	    		compsImages[i] = (PixImageView)compFrames[i].findViewById(R.id.compPic);
	    		if (compsImages[i] != null) {
	    			compsImages[i].highlightOnPress(true);
	    		}
	    	}
		}
		footer = (TextView)v.findViewById(R.id.cell_footer);
	}

	public void init(final PixBaseListDelegate mListDelegate) {
		for (int i = 0; i<compsNum; i++) {
			if (compFrames[i] != null && compsImages[i] != null) {
				// calculate frame dimensions
				int compsGripSpacing = PixApp.getContext().getResources()
						.getDimensionPixelSize(R.dimen.comps_grid_spacing);
				int rowPadding = compsGripSpacing * 2;
				int cellMargin = compsGripSpacing * 2;
				int thumbImageFrameDimen = (PixAppState.getState().getFrameWidth() - rowPadding - (cellMargin*3))/3;
				int imageFrameDimen;
				
				if (i == 0) {
					imageFrameDimen = 2*thumbImageFrameDimen + cellMargin;
				} else {
					imageFrameDimen = thumbImageFrameDimen;
				}
				android.view.ViewGroup.LayoutParams params = compFrames[i].getLayoutParams();
				params.width = imageFrameDimen;
				params.height = imageFrameDimen;
				compFrames[i].setVisibility(View.VISIBLE);
				compsImages[i].getLayoutParams().width = imageFrameDimen;
				compsImages[i].getLayoutParams().height = imageFrameDimen;
				compsImages[i].setOnClickListener(new OnPixCompsSixpackClickListener(this, i, mListDelegate) {
				});
	        }
	    }		
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	PixCompsPack compSixpack = (PixCompsPack)data; 
        
    	for (int i = 0; i<compsNum; i++){
    		if (compSixpack.getComp(i) != null && compsImages[i] != null) {
    			if (i == 0) {
	    			compsImagesContainers[i] = PixImagesLoader.loadImage(compSixpack.getComp(i).getImage(), 
	    						compsImages[i], 0, 0, Priority.HIGH, false);
    			} else {
    				String url = compSixpack.getComp(i).getImage();
					if (url != null) {
						url = url.concat(PixHttpAPI.IMAGE_SIZE_THUMBNAIL);
						compsImagesContainers[i] = PixImagesLoader.loadImage(url, 
	    						compsImages[i], 0, 0, Priority.HIGH, false);
					}
    			}
    		}
    	}
	}
	
	@Override
    public void onEnterRecycleBin() {
    	for (int i = 0; i<compsNum; i++){
			if (compsImagesContainers[i] != null) compsImagesContainers[i].cancelRequest();
			compsImages[i].setImageBitmapHolder(null);
    	}	
    	if (footer != null &&
	    		footer.getVisibility() == View.VISIBLE) {
	    	// handle the case where the last cell is recycled
	    	footer.setVisibility(View.GONE);
	    }
	}
	
	@Override
	public void handleLastCellFooter() {
		if (footer != null) {
    		footer.setVisibility(View.VISIBLE);
		}
	}
	
	private static class OnPixCompsSixpackClickListener implements View.OnClickListener {

        private PixCompsSixpackViewHolder mViewHolder;
        private int compIndex;
        private PixBaseListDelegate mListDelegate;

        public OnPixCompsSixpackClickListener(PixCompsSixpackViewHolder holder, int index, PixBaseListDelegate listDelegate) {
                mViewHolder = holder;
                compIndex = index;
                this.mListDelegate = listDelegate;
        }

        // delegates the click event
        public void onClick(View v) {
                onClick(v, mViewHolder);
        }

        /**
         * Implement your click behavior here
         * @param v  The clicked view.
         */
        public void onClick(View v, PixCompsSixpackViewHolder viewHolder) {
        	PixComposition comp = ((PixCompsPack) viewHolder.data).getComp(compIndex);
            // the image had been clicked, call the event listener
            mListDelegate.onCompImageClick(comp); 
        }
    };
}
