package com.pixplit.android.util.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.ui.elements.AnimatedImageView;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.time.PixConversationDateFormat;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class ChatMessageViewHolder extends PixViewHolder{
	private ChatMessageViewDelegate mDelegate;
	
	// date
	private RelativeLayout dateTimestamp;
	private TextView dateText;
	// name
	private RelativeLayout senderName;
	private TextView senderNameText;
	private ImageView nameLeftDivider;
	private ImageView nameRightDivider;
	// message
	private TextView msgText;
	//comp
	private RelativeLayout msgCompFrame;
	private PixImageView compImg;
	private BitmapFetchHolderDelegate compImageFetcherDelegate;
	private ImageContainer compImgContainer;
	//time
	private TextView msgTime;
	// send animation
	private AnimatedImageView msgSentIndication;
	
	int msgTextTopMargins = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.conversation_chat_text_top_margin);
	int msgTimeHorizontalMargins = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.conversation_chat_time_horizontal_margin);
	int msgTimeTopMargins = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.conversation_chat_time_top_margin);
	int msgTimeBottomMargins = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.conversation_chat_time_bottom_margin);
	int comps_feed_horizontal_margin = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comps_feed_horizontal_margin);
	int chat_msg_sent_indication_dimen = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.chat_msg_sent_indication_dimen);
	
	// alignment params
	RelativeLayout.LayoutParams alignLeftParams;
	RelativeLayout.LayoutParams alignRightParams;
	
	public interface ChatMessageViewDelegate {
		public String getUserDisplayName(String username);
		public void reSendTextMsg(PixChatMessage chatMsg);
		public void onMsgCompClick(PixChatMessage chatMsg);
		public void onMsgLongPress(PixChatMessage chatMsg);
	}
	
	public ChatMessageViewHolder(View v, ChatMessageViewDelegate delegate) {
		this.mDelegate = delegate;
		if (v != null) {
			// date
			dateTimestamp = (RelativeLayout) v.findViewById(R.id.dateTimestamp_ref);
			if (dateTimestamp != null) {
				dateText = (TextView) dateTimestamp.findViewById(R.id.dateText);
			}
			// message
			msgText = (TextView) v.findViewById(R.id.msgText);
			if (msgText != null) {
				msgText.setOnLongClickListener(new OnViewHolderLongClickListener(this) {
					
					@Override
					public void onLongClick(View v, PixViewHolder viewHolder) {
						if (viewHolder instanceof ChatMessageViewHolder) {
							ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)viewHolder;
		                    final PixChatMessage msg = (PixChatMessage) chatMessageViewHolder.data;
		                    if (mDelegate != null) {
		                    	mDelegate.onMsgLongPress(msg);
		                    }
						}
					}
				});
			}
			
			//comp
			msgCompFrame = (RelativeLayout) v.findViewById(R.id.msgCompFrame);
			if (msgCompFrame != null) {
				compImg = (PixImageView) msgCompFrame.findViewById(R.id.msg_comp_img);
				if (compImg != null) {
					compImg.getLayoutParams().height = PixAppState.getState().getSplitPreviewHeight();
					compImg.getLayoutParams().width = PixAppState.getState().getSplitPreivewWidth();
				}
				// name
				senderName = (RelativeLayout) msgCompFrame.findViewById(R.id.senderName);
				if (senderName != null) {
					senderNameText = (TextView) senderName.findViewById(R.id.senderNameText);
					nameLeftDivider = (ImageView) senderName.findViewById(R.id.nameLeftDivider);
					nameRightDivider = (ImageView) senderName.findViewById(R.id.nameRightDivider);
				}
				
			}
			//time
			msgTime = (TextView) v.findViewById(R.id.msgTime);
			
			// send animation
			msgSentIndication = (AnimatedImageView) v.findViewById(R.id.msgSentIndication);
			if (msgSentIndication != null) {
				msgSentIndication.setOnClickListener(new OnViewHolderClickListener(this) {
					@Override
					public void onClick(View v, PixViewHolder viewHolder) {
						if (viewHolder instanceof ChatMessageViewHolder) {
							ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)viewHolder;
		                    final PixChatMessage msg = (PixChatMessage) chatMessageViewHolder.data;
		                    if (msg == null || v == null) return;
		                    if (msg.getSentState() == PixChatMessage.PIX_MESSAGE_SENT_STATE_FAIL) {
		                    	// open a dialog suggesting to send the message again
		                    	new PixConfirmationDialog(v.getContext(), 
		                    			new ConfirmationDialogDelegate() {
											@Override
											public void onDecline() {}
											@Override
											public void onConfirm() {
												if (mDelegate != null) {
													mDelegate.reSendTextMsg(msg);
												}
											}
										}, 
										PixApp.getContext().getResources().getString(R.string.your_message_was_not_sent), 
										null, 
										PixApp.getContext().getResources().getString(R.string.try_again), 
										PixApp.getContext().getResources().getString(R.string.cancel)).show();
		                    }
						}
					}
				});
			}
		}
	}
	
	public void init(final PixBaseListDelegate mListDelegate) {
		alignLeftParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		alignLeftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		alignLeftParams.setMargins(0, msgTextTopMargins, msgTimeHorizontalMargins + chat_msg_sent_indication_dimen, 0);
		
		alignRightParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		alignRightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		alignRightParams.setMargins(msgTimeHorizontalMargins + chat_msg_sent_indication_dimen, msgTextTopMargins, 0, 0);
		
		compImageFetcherDelegate = new BitmapFetchHolderDelegate(this) {
			@Override
			public void onBitmapFetchFinished(PixViewHolder viewHolder, String url, 
					PixBitmapHolder bitmapHolder) {
				if (bitmapHolder != null && bitmapHolder.getBitmap() != null) {
					compImg.setOnClickListener(new OnViewHolderClickListener(ChatMessageViewHolder.this) {
						@Override
						public void onClick(View v, PixViewHolder viewHolder) {
							if (viewHolder instanceof ChatMessageViewHolder) {
								ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)viewHolder;
			                    final PixChatMessage msg = (PixChatMessage) chatMessageViewHolder.data;
			                    if (mDelegate != null) {
			                    	mDelegate.onMsgCompClick(msg);
			                    }
							}
						}
					});
					compImg.setOnLongClickListener(new OnViewHolderLongClickListener(ChatMessageViewHolder.this) {
						@Override
						public void onLongClick(View v, PixViewHolder viewHolder) {
							if (viewHolder instanceof ChatMessageViewHolder) {
								ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)viewHolder;
			                    final PixChatMessage msg = (PixChatMessage) chatMessageViewHolder.data;
			                    if (mDelegate != null) {
			                    	mDelegate.onMsgLongPress(msg);
			                    }
							}
						}
					});
				}
				
			}
			
			@Override
			public void onBitmapFetchFailed(PixViewHolder viewHolder) {
				
				compImg.setOnClickListener(new OnViewHolderClickListener(ChatMessageViewHolder.this) {
						@Override
						public void onClick(View v, PixViewHolder viewHolder) {
							if (viewHolder instanceof ChatMessageViewHolder) {
								ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)viewHolder;
			                    final PixChatMessage chatMsg = (PixChatMessage) chatMessageViewHolder.data;
			                    PixComposition comp = chatMsg.getComp();
			        			if (comp != null) {
			        				String compImageUrl = comp.getImage();
			        				PixImagesLoader.loadPrivateImage(compImageUrl, compImg, 0, 
			        						R.drawable.reload_image, Priority.HIGH, true, compImageFetcherDelegate);
			        			}
							}
						}
					});
			}
		};
	}

	@Override
	protected void onNewData(Object data) {
		if (data == null) return;
		
		// Binding the holder keeps our data up to date.
    	PixChatMessage chatMsg = (PixChatMessage)data; 
	    
    	// check who is the sender
    	boolean loggedInUserIsSender = false;
    	String loggedInUser = PixAppState.UserInfo.getUserName();
    	String sender = chatMsg.getSender();
    	if (Utils.isValidString(sender) &&
    			Utils.isValidString(loggedInUser) &&
    			sender.equalsIgnoreCase(loggedInUser)) {
    		loggedInUserIsSender = true;
    	}
    	
    	// check message type
    	int messageType = chatMsg.getType();
    	if (messageType == PixChatMessage.PIX_MESSAGE_TYPE_TEXT) {
    		if (msgText != null) {
    			String message = chatMsg.getText();
    			if (Utils.isValidString(message)) {
    				msgText.setText(message);
    			}
    			msgText.setVisibility(View.VISIBLE);
    			if (loggedInUserIsSender) {
    				msgText.setBackgroundResource(R.drawable.msg_chat_bubble_grey_selector);
    				msgText.setLayoutParams(alignRightParams);
    			} else {
    				msgText.setBackgroundResource(R.drawable.msg_chat_bubble_blue);
    				msgText.setLayoutParams(alignLeftParams);
    			}
    		}
    		if (msgCompFrame != null) {
    			msgCompFrame.setVisibility(View.GONE);
    		}
    		if (senderName != null) {
    			senderName.setVisibility(View.GONE);
    		}
    	} else if (messageType == PixChatMessage.PIX_MESSAGE_TYPE_COMP) {
    		if (msgText != null) {
    			msgText.setVisibility(View.GONE);
    		}
    		if (msgCompFrame != null) {
    			msgCompFrame.setVisibility(View.VISIBLE);
    			
    			PixComposition comp = chatMsg.getComp();
    			if (comp != null) {
    				String compImageUrl = comp.getImage();
    				if (Utils.isValidString(compImageUrl) && compImg != null) {
    					compImgContainer = PixImagesLoader.loadPrivateImage(compImageUrl, compImg, 0, R.drawable.reload_image, 
    							Priority.HIGH, true, compImageFetcherDelegate);
    				}
    				
    				if (senderName != null && senderNameText != null) {
    					senderName.setVisibility(View.VISIBLE);
    					senderName.setPadding(0, msgTimeTopMargins, 0, msgTimeBottomMargins);
    					if (mDelegate != null) {
    						String userDiaplayName = mDelegate.getUserDisplayName(sender);
    						
    						RelativeLayout.LayoutParams senderNameParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							if (Utils.isValidString(userDiaplayName)) {
    							senderNameText.setText(userDiaplayName);
    						}
    						if (loggedInUserIsSender) {
    							if (nameLeftDivider != null) {
    								nameLeftDivider.setVisibility(View.VISIBLE);
    							}
    							if (nameRightDivider != null) {
    								nameRightDivider.setVisibility(View.GONE);
    							}
    							senderNameParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    							senderNameParams.setMargins(msgTimeHorizontalMargins, 
    														msgTimeTopMargins, 
    														msgTimeHorizontalMargins - comps_feed_horizontal_margin, 
    														msgTimeBottomMargins);
    							senderNameText.setLayoutParams(senderNameParams);
    						} else {
    							if (nameLeftDivider != null) {
    								nameLeftDivider.setVisibility(View.GONE);
    							}
    							if (nameRightDivider != null) {
    								nameRightDivider.setVisibility(View.VISIBLE);
    							}
    							senderNameParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
    							senderNameParams.setMargins(msgTimeHorizontalMargins - comps_feed_horizontal_margin,
										msgTimeTopMargins, 
										msgTimeHorizontalMargins, 
										msgTimeBottomMargins);
    							senderNameText.setLayoutParams(senderNameParams);
    						}
    					}
    				}
    			}
    		}
    	}
    	
    	// set the time
    	if (msgTime != null) {
			if (chatMsg.getDate() != null) {
	    		String timeHumanFormat = PixConversationDateFormat.getPixDateInstance().timeFormat(chatMsg.getDate());
	    		if (Utils.isValidString(timeHumanFormat)) {
	    			msgTime.setText(timeHumanFormat);
	    			
	    			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    			if (chatMsg.getType() == PixChatMessage.PIX_MESSAGE_TYPE_TEXT) {
	    				params.addRule(RelativeLayout.BELOW, R.id.msgTextBox);
	    			} else if (chatMsg.getType() == PixChatMessage.PIX_MESSAGE_TYPE_COMP) {
	    				params.addRule(RelativeLayout.BELOW, R.id.msgCompFrame);
	    			}
	    			params.setMargins(msgTimeHorizontalMargins, msgTimeTopMargins, msgTimeHorizontalMargins, msgTimeBottomMargins);
	    			if (loggedInUserIsSender) {
	    				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	    				msgTime.setLayoutParams(params);
	    			} else {
	    				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	    				msgTime.setLayoutParams(params);
	    			}
	    			msgTime.setVisibility(View.VISIBLE);
	    		} else {
	    			msgTime.setVisibility(View.GONE);
	    		}
    		} else {
    		    msgTime.setVisibility(View.GONE);
    		}
		}
    	
    	// tell whether to add the date
    	if (dateText != null) {
    		if (chatMsg.getNextDate() != null) {
    			String dateHumanFormat = PixConversationDateFormat.getPixDateInstance().mediumDateFormat(chatMsg.getNextDate());
    			if (Utils.isValidString(dateHumanFormat)) {
	    			dateText.setText(dateHumanFormat);
	    			dateTimestamp.setVisibility(View.VISIBLE);
    			} else {
        			dateTimestamp.setVisibility(View.GONE);
        		}
    		} else {
    			dateTimestamp.setVisibility(View.GONE);
    		}
    	}
    
    	// sending indication
    	if (msgSentIndication != null) {
    		boolean displayIndication = false;
	    	switch (chatMsg.getSentState()) {
			case PixChatMessage.PIX_MESSAGE_SENT_STATE_FAIL:
				msgSentIndication.setBackgroundResource(R.drawable.msg_failed_selector);
				displayIndication = true;
				break;
			default:
				displayIndication = false;
				break;
			}
	    	if (displayIndication) {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				if (loggedInUserIsSender) {
					params.addRule(RelativeLayout.ALIGN_LEFT);
				} else {
					params.addRule(RelativeLayout.ALIGN_RIGHT);
				}
				params.addRule(RelativeLayout.CENTER_VERTICAL);
				msgSentIndication.setLayoutParams(params);
	    		msgSentIndication.setVisibility(View.VISIBLE);
	    	} else {
	    		msgSentIndication.setVisibility(View.GONE);
	    	}
    	}
	}
	
	@Override
    public void onEnterRecycleBin() {
		if (compImgContainer != null) compImgContainer.cancelRequest();
		compImg.setImageBitmapHolder(null);
		compImg.setOnClickListener(null);
		compImg.setOnLongClickListener(null);
    }
}
