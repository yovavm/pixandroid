package com.pixplit.android.util.viewholders;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixPopularComposition;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminAddToPopularHttpRequest;
import com.pixplit.android.http.request.AdminRemoveFromPopularHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.ui.elements.PixTextView;
import com.pixplit.android.util.Utils;

public class PopularCompViewHolder extends PixCompositionViewHolder{
	Context context;
	PixTextView activateCompBtn;
	private DeactivatePopularComp deactivateListener;
	private ActivatePopularComp activateListener;
	private PopularCompHolderDelegate mDelegate;
	
	private static final float btnTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
	private static final Drawable btnBg = PixApp.getContext().getResources().getDrawable(R.drawable.comp_actions_bar_btn_selector);
	private static final ColorStateList btnTextcolorSelector = PixApp.getContext().getResources().getColorStateList(R.color.comp_actions_bar_text_color_selector);
	private static final String removePopularString = PixApp.getContext().getResources().getString(R.string.admin_remove_popular);
	private static final String activatePopularString = PixApp.getContext().getResources().getString(R.string.admin_activate_popular);
	
	public interface PopularCompHolderDelegate {
		public void onAdminNotAuthenticated();
		public void onPopularDeactivateClick(PixPopularComposition comp);
	}
	
	public PopularCompViewHolder(Context context, View v, PixBaseListDelegate listDelegate, PopularCompHolderDelegate delegate) {
		super(v, listDelegate);
		this.context = context;
		this.mDelegate = delegate;
	}
		
	@SuppressWarnings("deprecation")
	@Override
	public void init() {
		super.init();
		deactivateListener = new DeactivatePopularComp();
		activateListener = new ActivatePopularComp();
		
		activateCompBtn = new PixTextView(context);
		activateCompBtn.setGravity(Gravity.CENTER);
		activateCompBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
		if (Utils.hasJellyBean()) {
			activateCompBtn.setBackground(btnBg);
        } else {
        	activateCompBtn.setBackgroundDrawable(btnBg);
        }
		activateCompBtn.setTextColor(btnTextcolorSelector);
		
		this.actionsBar.addActionToBar(activateCompBtn, null, 2, 0);
	}
	
	@Override
	public void onNewData(Object data) {
	    super.onNewData(data);
	    final PixPopularComposition popularComp = (PixPopularComposition)data;
		
	    if (popularComp.isActive()) {
	    	// popular comp is active, add "remove from popular" button.
	    	activateCompBtn.setText(removePopularString);
	    	activateCompBtn.setOnClickListener(deactivateListener);
	    } else {
	    	// popular comp is not active, add "add to popular" button.
	    	activateCompBtn.setText(activatePopularString);
	    	activateCompBtn.setOnClickListener(activateListener);
	    }
	}
	
	private class DeactivatePopularComp implements OnClickListener {
		@Override
		public void onClick(View v) {
			final PixPopularComposition popularComp = (PixPopularComposition)data;
			new AdminRemoveFromPopularHttpRequest(new RequestDelegate() {
				@Override
				public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
					if (error != null && 
							error.getCode() == PixError.PIX_SERVER_ERROR_ADMIN_NOT_AUTHENTICATED) {
						// admin is not authenticated
						if (mDelegate != null) {
							mDelegate.onAdminNotAuthenticated();
						}
					}
				}
				@Override
				public void requestFinished(BaseHttpRequest request, PixObject response) {}
			}, popularComp.getCompId()).send();
	    	activateCompBtn.setText(activatePopularString);
	    	activateCompBtn.setOnClickListener(activateListener);
	    	popularComp.setActive(false);
	    	if (mDelegate != null) {
	    		mDelegate.onPopularDeactivateClick(popularComp);
	    	}
		}
	}
	
	private class ActivatePopularComp implements OnClickListener {
		@Override
		public void onClick(View v) {
			final PixPopularComposition popularComp = (PixPopularComposition)data;
			new AdminAddToPopularHttpRequest(new RequestDelegate() {
				@Override
				public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
					if (error != null && 
							error.getCode() == PixError.PIX_SERVER_ERROR_ADMIN_NOT_AUTHENTICATED) {
						// admin is not authenticated
						if (mDelegate != null) {
							mDelegate.onAdminNotAuthenticated();
						}
					}
				}
				@Override
				public void requestFinished(BaseHttpRequest request, PixObject response) {}
			}, popularComp.getCompId()).send();
	    	activateCompBtn.setText(removePopularString);
	    	activateCompBtn.setOnClickListener(deactivateListener);
	    	popularComp.setActive(true);
		}
	}
	
	@Override
	protected PixTextView createAddToPopularBtn() {
		return null; // no need to add the default add-to-popular button.
	}
}
