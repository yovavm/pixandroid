package com.pixplit.android.util.viewholders;

import android.view.View;

/**
 * The click listener base class.
 */
public abstract class OnViewHolderClickListener implements View.OnClickListener {

    private PixViewHolder mViewHolder;

    /**
     * @param holder The holder of the clickable item
     */
    public OnViewHolderClickListener(PixViewHolder holder) {
        mViewHolder = holder;
    }

    // delegates the click event
    @Override
    public void onClick(View v) {
        onClick(v, mViewHolder);
    }

    /**
     * Implement your click behavior here
     * @param v  The clicked view.
     */
    public abstract void onClick(View v, PixViewHolder viewHolder);
};