package com.pixplit.android.util.viewholders;

import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;

public abstract class BitmapFetchHolderDelegate implements BitmapFetcherDelegate{

	private PixViewHolder mViewHolder;
	
	/**
     * @param holder The holder of the bitmap being fetched
     */
    public BitmapFetchHolderDelegate(PixViewHolder holder) {
        mViewHolder = holder;
    }
    
	@Override
	public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapHolder) {
		onBitmapFetchFinished(mViewHolder, url, bitmapHolder);
	}

	@Override
	public void onBitmapFetchFailed() {
		onBitmapFetchFailed(mViewHolder);
	}
	
	public abstract void onBitmapFetchFinished(PixViewHolder viewHolder, String url, PixBitmapHolder bitmap);

	public abstract void onBitmapFetchFailed(PixViewHolder viewHolder);

}
