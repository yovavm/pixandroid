package com.pixplit.android.util.viewholders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.pixplit.android.data.GoogleImage;

public class GoogleImagesPack {
	private GoogleImage[] mItemsPack;
	private int packSize;
	
	public GoogleImagesPack(int packSize, Iterator<GoogleImage> iter){
		mItemsPack = new GoogleImage[packSize];
		this.packSize = packSize;
		
		for (int i=0; i<packSize; i++){
			if (iter.hasNext()){
				mItemsPack[i] = iter.next();
			} else {
				mItemsPack[i] = null;
			}
		}
	}
	
	public GoogleImage getItem(int position){
		GoogleImage response = null;
		if (position < packSize) {
			response = mItemsPack[position];
		}
		return response;
	}
	
	public int getPackSize() {
		return packSize;
	}
	
	public static ArrayList<GoogleImagesPack> createItemsPackList(List<GoogleImage> items, int packSize){
		ArrayList<GoogleImagesPack> mPackList = new ArrayList<GoogleImagesPack>();
		
		if (items != null){
			Iterator<GoogleImage> iter = items.iterator();
			
			while (iter.hasNext()){
				mPackList.add(new GoogleImagesPack(packSize, iter));
			}
		}
		return mPackList;
	}
}