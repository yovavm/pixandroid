package com.pixplit.android.util.viewholders;

import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class PixCompsThreepackViewHolder extends PixViewHolder{
	
	private PixImageView[] compsImages;
	private ImageContainer[] compsImagesContainers;
	private RelativeLayout[] compFrames;
	private int[] compsImagesIds = {R.id.comp1, R.id.comp2, R.id.comp3};
	private final static int COMPS_PACK_SIZE = 3;
	
	public PixCompsThreepackViewHolder(View v) {
		compsImages = new PixImageView[COMPS_PACK_SIZE];
		compFrames = new RelativeLayout[COMPS_PACK_SIZE];
		compsImagesContainers = new ImageContainer[COMPS_PACK_SIZE];
		for (int i = 0; i<COMPS_PACK_SIZE; i++) {
			compFrames[i] = (RelativeLayout) v.findViewById(compsImagesIds[i]);
	    	if (compFrames[i] != null) {
	    		compsImages[i] = (PixImageView)compFrames[i].findViewById(R.id.compPic);
	    		if (compsImages[i] != null) {
	    			compsImages[i].highlightOnPress(true);
	    		}
	    	}
		}
	}
	
	public void init(final PixBaseListDelegate mListDelegate) {
		for (int i = 0; i<COMPS_PACK_SIZE; i++) {
			if (compFrames[i] != null && compsImages[i] != null) {
				// calculate frame dimensions
				int rowPadding = PixApp.getContext().getResources()
						.getDimensionPixelSize(R.dimen.profile_split_row_horizental_margin) * 2;
				int cellMargin = PixApp.getContext().getResources()
						.getDimensionPixelSize(R.dimen.comps_grid_spacing) * 2;
				int imageFrameDimen = (PixAppState.getState().getFrameWidth() - rowPadding - (cellMargin*COMPS_PACK_SIZE))/COMPS_PACK_SIZE;
				android.view.ViewGroup.LayoutParams params = compFrames[i].getLayoutParams();
				params.width = imageFrameDimen;
				params.height = imageFrameDimen;
				
				compFrames[i].setVisibility(View.VISIBLE);
				compsImages[i].getLayoutParams().width = imageFrameDimen;
				compsImages[i].getLayoutParams().height = imageFrameDimen;
				compsImages[i].setOnClickListener(new OnPixCompsThreepackClickListener(this, i, mListDelegate));
	        }
	    }		
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	PixCompsPack compThreepack = (PixCompsPack)data; 
        
    	for (int i = 0; i<COMPS_PACK_SIZE; i++){
    		if (compFrames[i] != null && compsImages[i] != null) {
				if (compThreepack.getComp(i) != null) {
					compFrames[i].setVisibility(View.VISIBLE);
					String url = compThreepack.getComp(i).getImage();
					if (url != null) {
						url = url.concat(PixHttpAPI.IMAGE_SIZE_THUMBNAIL);
						compsImagesContainers[i] = PixImagesLoader.loadImage(url, 
		    						compsImages[i], 0, 0, Priority.HIGH, false);
					}
	    		} else {
	    			compFrames[i].setVisibility(View.INVISIBLE);
	    		}
	    	}
    	}
	}
	
	@Override
    public void onEnterRecycleBin() {
    	for (int i = 0; i<COMPS_PACK_SIZE; i++){
    		if (compFrames[i] != null) {
    			compFrames[i].setVisibility(View.VISIBLE);
    			if (compsImagesContainers[i] != null) compsImagesContainers[i].cancelRequest();
    			compsImages[i].setImageBitmapHolder(null);
    		}
    	}	
	}

	private static class OnPixCompsThreepackClickListener implements View.OnClickListener {

        private PixCompsThreepackViewHolder mViewHolder;
        private int compIndex;
        private PixBaseListDelegate mListDelegate;

        public OnPixCompsThreepackClickListener(PixCompsThreepackViewHolder holder, int index, PixBaseListDelegate listDelegate) {
                mViewHolder = holder;
                compIndex = index;
                this.mListDelegate = listDelegate;
        }

        // delegates the click event
        public void onClick(View v) {
                onClick(v, mViewHolder);
        }

        /**
         * Implement your click behavior here
         * @param v  The clicked view.
         */
        public void onClick(View v, PixCompsThreepackViewHolder viewHolder) {
        	PixComposition comp = ((PixCompsPack) viewHolder.data).getComp(compIndex);
            // the image had been clicked, call the event listener
            mListDelegate.onCompImageClick(comp); 
        }
    };
}
