package com.pixplit.android.util.viewholders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.pixplit.android.data.PixComposition;

public class PixCompsPack {
	private PixComposition[] mCompsPack;
	private int packSize;
	
	public PixCompsPack(int packSize, Iterator<PixComposition> iter){
		mCompsPack = new PixComposition[packSize];
		this.packSize = packSize;
		
		for (int i=0; i<packSize; i++){
			if (iter.hasNext()){
				mCompsPack[i] = iter.next();
			} else {
				mCompsPack[i] = null;
			}
		}
	}
	
	public PixComposition getComp(int position){
		PixComposition response = null;
		if (position < packSize) {
			response = mCompsPack[position];
		}
		return response;
	}
	
	public int getPackSize() {
		return packSize;
	}
	
	public static ArrayList<PixCompsPack> createCompsPackList(List<PixComposition> comps, int packSize){
		ArrayList<PixCompsPack> mPackList = new ArrayList<PixCompsPack>();
		
		if (comps != null){
			Iterator<PixComposition> iter = comps.iterator();
			
			while (iter.hasNext()){
				mPackList.add(new PixCompsPack(packSize, iter));
			}
		}
		return mPackList;
	}
}