package com.pixplit.android.util.viewholders;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.adapters.UsersListAdapter;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.UserFollowBtn;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.PixImagesLoader;
import com.pixplit.android.volley.PixVolleyImageLoader.ImageContainer;

public class UserViewHolder extends PixViewHolder{
	private RelativeLayout userLayout;
	private PixImageView userPic;
	private ImageContainer userPicContainer;
	private TextView userName;
	private UserFollowBtn userFollowStatus;
	protected ImageView userAction;
	int userActionType;
	UserActionsDelegate userViewDelegate;
	
	public interface UserActionsDelegate {
		public void onUserClick(PixUser pixUser);
	}
	
	public UserViewHolder(View v, int userActionType, UserActionsDelegate userViewDelegate) {
		userLayout = (RelativeLayout)v.findViewById(R.id.user_layout);
		userPic = (PixImageView) v.findViewById(R.id.userPic);
        userName = (TextView) v.findViewById(R.id.userName);
        userFollowStatus = (UserFollowBtn) v.findViewById(R.id.followStatus);
        userAction = (ImageView)v.findViewById(R.id.userAction);
        this.userActionType = userActionType;
        this.userViewDelegate = userViewDelegate;
    }
	
	public void init(final PixBaseListDelegate mListDelegate) {
		// set the click listeners 
		userLayout.setOnClickListener(new OnViewHolderClickListener(this) {
			@Override
			public void onClick(View v, PixViewHolder viewHolder) {
				UserViewHolder mvh = (UserViewHolder) viewHolder;
	        	PixUser user = (PixUser) mvh.data;
	        	mListDelegate.onProfileClick(user.getUsername());
			}
		});
		
        if (userFollowStatus != null){
        	if (userActionType == UsersListAdapter.USERS_LIST_ACTION_FOLLOW) {
        		userFollowStatus.init(mListDelegate, R.drawable.following_btn, R.drawable.follow_btn);
        	} else {
        		userFollowStatus.setVisibility(View.GONE);
        	}
        }
        initUserActionBtn();
	}

	protected void initUserActionBtn() {
		if (userAction != null) {
        	if (userActionType == UsersListAdapter.USERS_LIST_ACTION_FOLLOW) {
        		userAction.setVisibility(View.GONE);
        	} else {
        		userAction.setBackgroundResource(R.drawable.profile_message_btn_selector); // default
        	}
        }
	}
	
	@Override
	protected void onNewData(Object data) {
		// Binding the holder keeps our data up to date.
    	final PixUser pixUser = (PixUser)data; 
	    userPicContainer = PixImagesLoader.loadImage(pixUser.getPicture(), userPic, 0, 0, Priority.HIGH, false);
	    if (userLayout != null) {
	    	userLayout.setBackgroundResource(R.drawable.android_blue_btn_selector);
	    }
	    if (userName != null) {
	    	if (Utils.isValidString(pixUser.getFullName())) {
		    	userName.setText(pixUser.getFullName());
		    }
	    }
	    
	    if (userActionType == UsersListAdapter.USERS_LIST_ACTION_FOLLOW) {
		    if (userFollowStatus != null) {
		    	userFollowStatus.bindUser(pixUser, false);
		    }
	    }  else {
	    	if (userAction != null) {
	    		if (PixUser.FOLLOW_STATUS_LOGGED_IN_USER == pixUser.getFollowStatus()) {
	    			userAction.setVisibility(View.GONE);
	    		} else {
	    			userAction.setVisibility(View.VISIBLE);
	    			userAction.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if (userViewDelegate != null) {
								userViewDelegate.onUserClick(pixUser);
							}
						}
					});
	    		}
	    	}
	    }
	}
	
	@Override
    public void onEnterRecycleBin() {
	    if (userPicContainer != null) userPicContainer.cancelRequest();
	    userPic.setImageBitmapHolder(null);
    }
}
