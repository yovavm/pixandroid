package com.pixplit.android.util.viewholders;



public abstract class PixViewHolder{
	public Object data;
	
	public final void bindData(Object data) {
		this.data = data;
		if (data != null) {
			onNewData(data);
		}
	}
	
	protected abstract void onNewData(Object data);
	
	// override by sub-classes to release resources
	public void onEnterRecycleBin() {}

	// override by sub-classes 
	public void onExitRecycleBin() {}

	// override by sub-classes
	public void handleLastCellFooter() {}

}
