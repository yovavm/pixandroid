package com.pixplit.android.util;

import com.pixplit.android.globals.PixDefs;

public class SplitPreviewCompatibleDimensFinder {
//	private static final String TAG = "SplitPreviewCompatibleDimensFinder";
	
	private static final int MIN_PREVIEW_DIMEN = 280;
	private static final int MAX_PREVIEW_DIMEN = PixDefs.SPLIT_MAX_PREVIEW_SCALE;
	private static boolean[] competibleDimens = new boolean[MAX_PREVIEW_DIMEN-MIN_PREVIEW_DIMEN+1];
	private static String compatDimenStr = null; 

	// this compatPreviewDimens was calculated by the checkPreviewScalesCompatability bellow.
	public static final int [] compatPreviewDimens = {284, 296, 308, 320, 332, 344, 356, 368, 380, 388, 392, 
													  400,404,412,416,424,428,436,440,448,452,460,464,472,476,484,488,496,
													  500,508,512,520,524,532,536,544,556,568,580,592,
													  604,616,628,640,652,664,676,688,696,
													  700,708,712,720,724,732,736,744,748,756,760,768,772,780,784,792,796,
													  804,808,816,820,828,832,840,844,852,864,876,888,
													  900,912,924, 936, 948, 960, 972, 984, 996, 
													  1004, 1008, 1016, 1020, 1028, 1032, 1040, 1044};
	
	// returns the largest supported dimension which is equal to or smaller of the input preferedDimen
	public static int getSupportedPreviewDimen(int preferedDimen) {
		int i=0;
		for (i=0;i<compatPreviewDimens.length-1;i++){
			if (compatPreviewDimens[i] <= preferedDimen &&
					compatPreviewDimens[i+1] > preferedDimen) {
				break;
			}
		}
		int selectedPreviewDimen = compatPreviewDimens[i];
//		Utils.pixLog(TAG, "selectedPreviewDimen:"+selectedPreviewDimen);
		return selectedPreviewDimen;
	}
	
	public static void checkPreviewScalesCompatability() {
		for (int splitPreviewDimen = MIN_PREVIEW_DIMEN; splitPreviewDimen <= MAX_PREVIEW_DIMEN; splitPreviewDimen++) {
			competibleDimens[splitPreviewDimen-MIN_PREVIEW_DIMEN] = true;
			if (!checkPreviewCompatability(splitPreviewDimen)) {
				//Utils.pixLog(TAG, "checkPreviewScalesCompatability FAILS for preview dimension:"+splitPreviewDimen);
				return;
			}
		}
		
		// check largest gap
		int largestGap = 0;
		int lastCompat = 0;
		for (int i=0; i<competibleDimens.length; i++) {
			if (competibleDimens[i]) {
				int gap = i-lastCompat;
				if (gap > largestGap) {
					largestGap = gap;
				}
				lastCompat = i;
				int comptDimen = (i+MIN_PREVIEW_DIMEN);
//				Utils.pixLog(TAG, "competible dimen found! DIMEN:"+comptDimen);
				if (compatDimenStr == null) {
					compatDimenStr = "{" + comptDimen;
				} else {
					compatDimenStr = compatDimenStr.concat(","+comptDimen);
				}
			}
		}
		compatDimenStr = compatDimenStr.concat("}");
		//Utils.pixLog(TAG, "larget gap:"+largestGap);
		//Utils.pixLog(TAG, "competible dimens list:"+compatDimenStr);
	}
	
	private final static int [][] COMBINATIONS = {
			{308,0,0,0},
			{154,154,0,0},
			{231,77,0,0},
			{206,102,0,0},
			{103,102,103,0},
			{154,77,77,0},
			{77,77,77,77},
			{103,205,0,0}
	};
	private final static int [][] COMBINATIONS_X_VALUE = {
		{0,0,0,0},
		{0,154,0,0},
		{0,231,0,0},
		{0,206,0,0},
		{0,103,205,0},
		{0,154,231,0},
		{0,77,154,231},
		{0,103,0,0}
	};
	
	public static boolean checkPreviewCompatability(int splitPreviewDimen) {
		float scale = ((float)PixDefs.SPLIT_BITMAP_RELATIVE_BASE_WIDTH) / splitPreviewDimen;
		
		for (int i=0; i<COMBINATIONS.length; i++) {
			if (!checkCombination(COMBINATIONS[i], COMBINATIONS_X_VALUE[i],splitPreviewDimen, scale)) {
				//Utils.pixLog(TAG, "checkPreviewCompatability FAILS for scale:"+scale+" and combination:" +"{"+COMBINATIONS[i][0]+","+COMBINATIONS[i][1]+","+COMBINATIONS[i][2]+","+COMBINATIONS[i][3]+"}");
				return false;
			}
		}
		return true;
	}
	static int MAX_PARTS = 4;
	private static boolean checkCombination(int[] combination, int[] combinationXvals, int splitPreviewDimen, float scale) {
		int partsDimen [] = new int[MAX_PARTS];
		int partsStartPosition [] = new int[MAX_PARTS];
		int partsDimenSum = 0;
		for (int i=0;i<MAX_PARTS;i++) {
			partsDimen[i] = getPreviewDimen(combination[i], scale);
			partsStartPosition[i] = getPreviewXposition(combinationXvals[i], scale);
			partsDimenSum += partsDimen[i];
		}
		if (partsDimenSum != splitPreviewDimen) {
			//Utils.pixLog(TAG, "partsDimenSum("+partsDimenSum+") != splitPreviewDimen("+splitPreviewDimen+")");
			//Utils.pixLog(TAG, "combination:{"+combination[0]+","+combination[1]+","+combination[2]+","+combination[3]+"}");
			//Utils.pixLog(TAG, "partsDimen:{"+partsDimen[0]+","+partsDimen[1]+","+partsDimen[2]+","+partsDimen[3]+"}");
			competibleDimens[splitPreviewDimen - MIN_PREVIEW_DIMEN] = false;
		} else {
			int sumX = 0;
			for (int i=0;i<MAX_PARTS;i++) {
				if (partsDimen[i] > 0) {
					if (partsStartPosition[i] != sumX) {
						//Utils.pixLog(TAG, "partsStartPosition[i]("+partsStartPosition[i]+") != sumX("+sumX+")");
						//Utils.pixLog(TAG, "combination:{"+combination[0]+","+combination[1]+","+combination[2]+","+combination[3]+"}");
						//Utils.pixLog(TAG, "partsDimen:{"+partsDimen[0]+","+partsDimen[1]+","+partsDimen[2]+","+partsDimen[3]+"}");
						competibleDimens[splitPreviewDimen - MIN_PREVIEW_DIMEN] = false;
					}
				}
				sumX += partsDimen[i];
			}
		}
		
		return true;
	}
	
	private static int getPreviewDimen(int origDimen, float scale) {
		int previewDimen = (int) Math.round((origDimen)/scale);
		return previewDimen;
	}
	private static int getPreviewXposition(int origDimen, float scale) {
		int previewDimen = (int) Math.round((origDimen)/scale);
		return previewDimen;
	}
}

