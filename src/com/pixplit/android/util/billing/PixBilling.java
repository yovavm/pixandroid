package com.pixplit.android.util.billing;

import java.util.ArrayList;

import android.app.Activity;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.util.Utils;


public class PixBilling {
//	private static final String TAG = "PixBilling";
	
	// TODO: hide the string by constructing it using XOR and other strings manipulations
	private final static String MII = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjZ18r1OHVkwamStJXwmMOTjwH1C6YrBVnFdDtUjupUFSyJyd4Hwvo75s/cwSbxqeI4hm/7Fz6QlEC6Y8O";
	private final static String RVX = "rVXWtka7Mp4xPYtyYGrx0E2owGtaqwN1oBvTiT9CIvHiCUlmtWN7pbj23KzdIbIuGJXRN9g3LFE0lVN7MAuNZagFJ2W8EMOSUvceSjzCX+HWQfSrneRXZxMUTpmhN";
	private final static String SLU = "sLUB5hhUD7QECZMivBan++yRt+YqY22fVfkHTESxoVrYFw86wS2eV/DCSr/ynPfC7QS+T7fDl8oHieGiAXE2a17I9qikXiYjbf2hUsqZ0G8yC/rRJfHAy8NSEt1fV";
	private final static String VKD = "VKDLI8QQzpwIDAQAB";
	public final static String base64EncodedPublicKey = MII + RVX + SLU + VKD;
	
	// SKUs for our products: non-consumable and consumable
    public static final String SKU_EXTRA_FRAMES = "frames_01";  
	
    // Listener that's called when we finish querying the items and subscriptions we own
    public static IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            //Utils.pixLog(TAG, "Query inventory finished.");
            if (result.isFailure()) {
                //Utils.pixLog(TAG, "Failed to query inventory: " + result);
                return;
            }

            //Utils.pixLog(TAG, "Query inventory was successful.");
            
            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */
            if (inventory != null) {
	            Purchase extraFramesPurchase = inventory.getPurchase(SKU_EXTRA_FRAMES);
	            boolean purchasedExtraFrames = false;
	            if (extraFramesPurchase != null) {
	            	purchasedExtraFrames = true;//verifyDeveloperPayload(extraFramesPurchase);
	            }
	            //Utils.pixLog(TAG, "User " + (purchasedExtraFrames ? "have" : "don't have") + "extra frames");
	            UserPurchases.setUserPurchased(SKU_EXTRA_FRAMES, purchasedExtraFrames);
	            
	            storeItemsPrices(inventory);
            }
        }
    };
	
    private static void storeItemsPrices(Inventory inventory) {
    	SkuDetails  purchaseItemDetails = inventory.getSkuDetails(SKU_EXTRA_FRAMES);
    	if (purchaseItemDetails != null) {
    		setItemPrice(SKU_EXTRA_FRAMES, purchaseItemDetails.mPrice);
    	}
    }
    
    public static void queryInventoryAsync(IabHelper mBillingHelper) {
    	if (mBillingHelper != null) {
    		ArrayList<String> additionalSkuList = new ArrayList<String>();
    		additionalSkuList.add(SKU_EXTRA_FRAMES);
    		try {
    			// query for purchased items
    			mBillingHelper.queryInventoryAsync(true, additionalSkuList, PixBilling.mGotInventoryListener);
    		} catch (Exception e) {
        		//Utils.pixLog(TAG, "Exception while trying to query inventory:"+e);
        		e.printStackTrace();
        	}
		}
		
    }
    
    public static IabHelper initInAppBilling() {
    	final IabHelper mBillingHelper = new IabHelper(PixApp.getContext(), PixBilling.base64EncodedPublicKey);
        
        // enable debug logging (for a production application, you should set this to false).
        mBillingHelper.enableDebugLogging(PixApp.inDebug());
        
        mBillingHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                //Utils.pixLog(TAG, "Setup finished.");
                if (result == null) {
                	//Utils.pixLog(TAG, "Problem setting up in-app billing (result is null)");
                    return;
                }
                if (!result.isSuccess()) {
                    //Utils.pixLog(TAG, "Problem setting up in-app billing: " + result);
                    return;
                }

                // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
                //Utils.pixLog(TAG, "Setup successful. Querying inventory.");
                PixBilling.queryInventoryAsync(mBillingHelper);
            }
        });
        
        return mBillingHelper;
	}
    
    public static String getPurchaseHash(String itemSku) {
    	String username = PixAppState.UserInfo.getUserName();
    	if (!Utils.isValidString(username) || !Utils.isValidString(itemSku)) {
    		return null;
    	}
    	return Utils.pixHash(PixApp.getContext().getResources().getString(R.string.app_id) + 
    			username + itemSku);
    }
    
    /** Verifies the developer payload of a purchase. */
    public static boolean verifyDeveloperPayload(Purchase p) {
    	if (p == null) return false;
    	String payload = p.getDeveloperPayload();
    	String itemSku = p.getSku();
    	if (!Utils.isValidString(payload)) {
    		return false;
    	}
    	String purchaseHash = getPurchaseHash(itemSku);
    	
    	return (purchaseHash != null &&
    			purchaseHash.equalsIgnoreCase(payload));
	}
    
    public interface BuyInAppDelegate {
    	public void buyRequestFinished(String itemSku);
    	public void buyRequestFailed(String itemSku, PixError error);
    }
    
    public static void buyInApp(Activity activity, IabHelper billingHelper, String itemSku, BuyInAppDelegate callback) {
		if (activity == null || billingHelper == null || !Utils.isValidString(itemSku)) return;
		// verification that the item is not purchased already
		if (PixBilling.UserPurchases.isUserPurchased(itemSku)) {
			//Utils.pixLog(TAG, "User already purchased " + itemSku);
			if (callback != null) {
				callback.buyRequestFinished(itemSku);
			}
			return;
		}
		
    	String payload = PixBilling.getPurchaseHash(itemSku); 
        
    	try {
	        billingHelper.launchPurchaseFlow(activity, 
	        		itemSku, 
	        		PixDefs.ACTIVITY_RESULT_CODE_PURCHASE_ITEM, 
	                new PixOnIabPurchaseFinishedListener(itemSku, callback), 
	                payload);
    	} catch (Exception e) {
    		//Utils.pixLog(TAG, "Exception while trying to purchase item:"+itemSku);
    		e.printStackTrace();
			if (callback != null) {
				callback.buyRequestFailed(itemSku, new PixError(PixError.PixErrorInAppBillingFailure));
			}
    	}

	}
    
    private static void setItemPrice(String itemSku, String price) {
		Utils.setSharedPreferencesString(PixDefs.PURCHASE_ITEM_PRICE_PREFIX + itemSku, price);
	}
	
	public static String getItemPrice(String itemSku){
		return Utils.getSharedPreferencesString(PixDefs.PURCHASE_ITEM_PRICE_PREFIX + itemSku, "");
	}
	
    public static class UserPurchases {
    	public static void setUserPurchased(String itemSku, boolean purchased) {
			Utils.setSharedPreferencesBoolean(PixDefs.USER_PURCHASED_PREFIX + itemSku, purchased);
		}
		
		public static boolean isUserPurchased(String itemSku){
			return Utils.getSharedPreferencesBoolean(PixDefs.USER_PURCHASED_PREFIX + itemSku, false);
		}
		
		public static void clearUserBillingInfo() {
			Utils.setSharedPreferencesBoolean(PixDefs.USER_PURCHASED_PREFIX + SKU_EXTRA_FRAMES, false);
		}
    }

	public static void onLogout() {
		UserPurchases.clearUserBillingInfo();
	}
}
