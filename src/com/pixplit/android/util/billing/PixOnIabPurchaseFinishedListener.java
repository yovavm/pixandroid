package com.pixplit.android.util.billing;

import com.pixplit.android.globals.PixError;
import com.pixplit.android.util.billing.PixBilling.BuyInAppDelegate;

// Callback for when a purchase is finished
public class PixOnIabPurchaseFinishedListener implements IabHelper.OnIabPurchaseFinishedListener {
//	private static final String TAG = "PixOnIabPurchaseFinishedListener";
	
	BuyInAppDelegate mCallback;
	String mItemSku;
	
	public PixOnIabPurchaseFinishedListener(String itemSku, BuyInAppDelegate callback) {
		this.mCallback = callback;
		this.mItemSku = itemSku;
	}
	
	@Override
	public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
		//Utils.pixLog(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
		if (result == null) {
			//Utils.pixLog(TAG, "Error purchasing. no result");
			if (this.mCallback != null) {
				this.mCallback.buyRequestFailed(this.mItemSku, 
						new PixError(PixError.PixErrorInAppBillingFailure));
			}
			return;
		}
		if (result.isFailure()) {
			//Utils.pixLog(TAG, "Error purchasing: " + result);
			if (this.mCallback != null) {
				this.mCallback.buyRequestFailed(this.mItemSku, 
						new PixError(PixError.PixErrorInAppBillingFailure, result.toString()));
			}
			return;
		}
		if (!PixBilling.verifyDeveloperPayload(purchase)) {
			//Utils.pixLog(TAG, "Error purchasing. Authenticity verification failed.");
			if (this.mCallback != null) {
				this.mCallback.buyRequestFailed(this.mItemSku, 
						new PixError(PixError.PixErrorInAppBillingFailure, result.toString()));
			}
			return;
		}
		//Utils.pixLog(TAG, "Purchase successful:" + this.mItemSku);
		// store purchase info
		PixBilling.UserPurchases.setUserPurchased(this.mItemSku, true);
		
		if (this.mCallback != null) {
			this.mCallback.buyRequestFinished(this.mItemSku);
		}
		
	}		
		
}
