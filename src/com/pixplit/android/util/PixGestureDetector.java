package com.pixplit.android.util;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public abstract class PixGestureDetector extends SimpleOnGestureListener {
	private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            // right to left swipe
//            //Utils.pixLog("on fling", String.format("e1.getX() - e2.getX():%f, e2.getX() - e1.getX():%f.", e1.getX() - e2.getX(), e2.getX() - e1.getX()));
            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	onSwipeLeft();
            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	onSwipeRight();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    


	@Override
    public boolean onDown(MotionEvent e) {
        return true; 
    }
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		onSingleTap(e);
    	return super.onSingleTapConfirmed(e);
	}
	
	@Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return super.onDoubleTapEvent(e);
    }
	
    // event when double tap occurs
    @Override
    public boolean onDoubleTap(MotionEvent e) {
    	onDoubleTapEvnt(e);
    	return super.onDoubleTap(e);
    }
	
    public abstract void onSwipeLeft();
    public abstract void onSwipeRight();
    public abstract void onDoubleTapEvnt(MotionEvent e);
    public abstract void onSingleTap(MotionEvent e);
    

}

