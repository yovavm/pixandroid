package com.pixplit.android.util.bitmap;

import android.graphics.Bitmap;

import com.pixplit.android.globals.PixDefs;

public class PixBitmapHolder {
//	private static final String TAG = "PixBitmapDrawable";
	private int referenceCount = 0;
	private Bitmap mBitmap;
	
	public PixBitmapHolder(Bitmap bitmap) {
		mBitmap = bitmap;
    }
	
	public Bitmap getBitmap() {
		return mBitmap;
	}
	
	public synchronized void useBitmap(final String context) {
		if (mBitmap != null) {
			this.referenceCount++;
//			//Utils.pixLog(TAG, String.format("$$ increase reference to:%d, by:"+context+", for bitmap:"+bitmap, this.referenceCount));
		}
	}
	
	public synchronized void releaseBitmap(final String context) {
		if (mBitmap != null) {
			this.referenceCount--;
//			//Utils.pixLog(TAG, String.format("$$ decrease reference to:%d, by:"+context+", for bitmap:"+bitmap, this.referenceCount));
			if (this.referenceCount <=0) {
				if (PixDefs.recycleBitmaps && !mBitmap.isRecycled()) {
//					//Utils.pixLog(TAG, "$$ recycle bitmap:"+bitmap);
					mBitmap.recycle();
					mBitmap = null;
				}
			}
		}
	}
}
