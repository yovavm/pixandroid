package com.pixplit.android.util.bitmap.filters;

import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.R;
import com.pixplit.android.capture.SplitBorderType;
import com.pixplit.android.ui.elements.PixBorder;
import com.pixplit.android.util.bitmap.GLToolbox;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.bitmap.TextureRenderer;

public class PixFilterPreview extends FrameLayout implements GLSurfaceView.Renderer{
	PixBitmapHolder origBitmapHolder;
	PixBitmapHolder filteredBitmapHolder;
	
	// GLSurfaceView.Renderer implementation
    private GLSurfaceView mEffectView;
    private int[] mTextures = new int[2];
    private EffectContext mEffectContext;
    private Effect mEffect;
    private Effect mEffectBrightness;
    private Effect mEffectSaturation;
    private Effect mEffectContrast;
    private TextureRenderer mTexRenderer = new TextureRenderer();
    private int mImageWidth;
    private int mImageHeight;
    private boolean mInitialized = false;
    int mCurrentEffect;
    boolean mEffectHasParams = false;
    private ImageView mEffectViewDefaultBorder = null;
    private PixBorder mEffectViewBorder = null;
    private int width, height;
    private boolean storeFilteredBitmap = false;
    
    
    public static final int EFFECT_NONE 			= 0;
    public static final int EFFECT_AUTO_FIX 		= 1;
    public static final int EFFECT_BLACKWHITE 		= 2;
    public static final int EFFECT_CROSSPROCESS		= 3;
    public static final int EFFECT_GRAYSCALE 		= 4;
    public static final int EFFECT_LOMOISH 			= 5;
    public static final int EFFECT_SEPIA 			= 6;
    public static final int EFFECT_TEMPERTURE 		= 7;
    public static final int EFFECT_TINT	 			= 8;
    public static final int EFFECT_BRIGHTNESS	    = 9;
    public static final int EFFECT_SATURATE	    	= 10;
    public static final int EFFECT_CONTRAST	    	= 11;
    
    private static float EFFECT_PARAM_BRIGHTNESS = .0f;
    private static float EFFECT_PARAM_SATURATION = .5f;
    private static float EFFECT_PARAM_CONTRAST = .0f;
    private static int EFFECT_PARAM_COLOR = Color.MAGENTA;
    
	public PixFilterPreview(Context context, PixBitmapHolder bitmapHolder, int width, int height) {
		super(context);
		this.setOrigBitmap(bitmapHolder);
		this.setFilteredBitmap(bitmapHolder);
		this.width = width;
		this.height = height;
		
		mEffectView = new GLSurfaceView(context);
		LayoutParams params = new LayoutParams(this.width, this.height);
		addView(mEffectView, params);
		mEffectView.setEGLContextClientVersion(2);
        mEffectView.setRenderer(this);
        mEffectView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	public PixFilterPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
    
	private void setOrigBitmap(PixBitmapHolder origBitmap) {
    	if (origBitmap != null) {
    		origBitmap.useBitmap("");
    	}
    	if (origBitmapHolder != null) {
    		origBitmapHolder.releaseBitmap("");
    	}
    	origBitmapHolder = origBitmap;
    }
	private void setFilteredBitmap(PixBitmapHolder filterBitmap) {
    	if (filterBitmap != null) {
    		filterBitmap.useBitmap("");
    	}
    	if (filteredBitmapHolder != null) {
    		filteredBitmapHolder.releaseBitmap("");
    	}
    	filteredBitmapHolder = filterBitmap;
    }
    
    public void setCurrentEffect(int effect) {
        mCurrentEffect = effect;
    }
        
    private void loadTextures() {
        // Generate textures
        GLES20.glGenTextures(2, mTextures, 0);

        // Load input bitmap
        if (this.origBitmapHolder == null) return;
        
        Bitmap origBitmap = this.origBitmapHolder.getBitmap();
        if (origBitmap == null) return;
        
        mImageWidth = origBitmap.getWidth();
        mImageHeight = origBitmap.getHeight();
        mTexRenderer.updateTextureSize(mImageWidth, mImageHeight);

        // Upload to texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, origBitmap, 0);

        // Set texture parameters
        GLToolbox.initTexParams();
        
    }
    
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void initEffect() {
        EffectFactory effectFactory = mEffectContext.getFactory();
        if (mEffect != null) {
            mEffect.release();
        }
        /**
         * Initialize the correct effect based on the selected menu/action item
         */
        switch (mCurrentEffect) {
            case EFFECT_NONE:
            	mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BRIGHTNESS);
            	 mEffect.setParameter("brightness", 1.0f);
                break;
            case EFFECT_AUTO_FIX:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_AUTOFIX);
//                mEffect.setParameter("scale", EFFECT_PARAM_SCALE);
                mEffect.setParameter("scale", .5f);
                break;
            case EFFECT_BLACKWHITE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BLACKWHITE);
                mEffect.setParameter("black", .1f);
                mEffect.setParameter("white", .7f);
                break;                
            case EFFECT_SEPIA:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SEPIA);
                break;
            case EFFECT_TEMPERTURE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TEMPERATURE);
//                mEffect.setParameter("scale", EFFECT_PARAM_SCALE);
                mEffect.setParameter("scale", .9f);
                break;
            case EFFECT_TINT:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TINT);
                mEffect.setParameter("tint", EFFECT_PARAM_COLOR);
                break;
            case EFFECT_CROSSPROCESS:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_CROSSPROCESS);
                break;
            case EFFECT_GRAYSCALE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_GRAYSCALE);
                break;
            case EFFECT_LOMOISH:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_LOMOISH);
                break;
            default:
                break;
        }
    }
    
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void initEffectParams() {
        EffectFactory effectFactory = mEffectContext.getFactory();
        if (mEffectBrightness != null) {
        	mEffectBrightness.release();
        }
        if (mEffectSaturation != null) {
        	mEffectSaturation.release();
        }
        if (mEffectContrast != null) {
        	mEffectContrast.release();
        }
        
        mEffectBrightness = effectFactory.createEffect(EffectFactory.EFFECT_BRIGHTNESS);
        mEffectBrightness.setParameter("brightness", (float)((EFFECT_PARAM_BRIGHTNESS*2)+1));
            
        mEffectSaturation = effectFactory.createEffect(EffectFactory.EFFECT_SATURATE);
        mEffectSaturation.setParameter("scale", (float)((EFFECT_PARAM_SATURATION*2)-1));
        
	
    	mEffectContrast = effectFactory.createEffect(EffectFactory.EFFECT_CONTRAST);
    	mEffectContrast.setParameter("contrast", (float)((EFFECT_PARAM_CONTRAST*2)+1));
    }
    
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void applyEffect() {
    	if (mEffect != null) {
    		mEffect.apply(mTextures[0], mImageWidth, mImageHeight, mTextures[1]);
    	}
    }
    
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void applyEffectParams() {
    	if (mEffectBrightness != null) {
    		mEffectBrightness.apply(mTextures[1], mImageWidth, mImageHeight, mTextures[1]);
        }
        if (mEffectSaturation != null) {
        	mEffectSaturation.apply(mTextures[1], mImageWidth, mImageHeight, mTextures[1]);
        }
        if (mEffectContrast != null) {
        	mEffectContrast.apply(mTextures[1], mImageWidth, mImageHeight, mTextures[1]);
        }
    }
    
    public void applyFilterParam() {
    	this.storeFilteredBitmap = true;
    	this.mEffectHasParams = true;
		if (this.origBitmapHolder != null) {
			mEffectView.requestRender();
		}
    }
    
    public void applyFilter(int filter) {
    	this.storeFilteredBitmap = true;
		if (this.origBitmapHolder != null) {
			setCurrentEffect(filter);
	        mEffectView.requestRender();
		}
    }
    
    public void setEffectParamScale(int effectParam, float scale) {
    	switch (effectParam) {
    	case EFFECT_BRIGHTNESS:
    		EFFECT_PARAM_BRIGHTNESS = scale;
    		break;
    	case EFFECT_SATURATE:
    		EFFECT_PARAM_SATURATION = scale;
    		break;
    	case EFFECT_CONTRAST:
    		EFFECT_PARAM_CONTRAST = scale;
    		break;
    	default:
    		break;
    	}
    	if (this.origBitmapHolder != null) {
	        mEffectView.requestRender();
		}
    }
    
    public void setEffectParamColor(int color) {
    	EFFECT_PARAM_COLOR = color;
		if (this.origBitmapHolder != null) {
	        mEffectView.requestRender();
		}
    }
    
    private void renderResult() {
        mTexRenderer.renderTexture(mTextures[1]);
    }
    
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if (mTexRenderer != null) {
            mTexRenderer.updateViewSize(width, height);
        }
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onDrawFrame(final GL10 gl) {
		if (!mInitialized) {
            //Only need to do this once
            mEffectContext = EffectContext.createWithCurrentGlContext();
            mTexRenderer.init();
            loadTextures();
            mInitialized = true;
        }
        initEffect();
        applyEffect();
        if (mEffectHasParams) {
        	// apply effect parameter
        	initEffectParams();
        	applyEffectParams();
        }
        
        renderResult();
        if (storeFilteredBitmap) {
        	storeFilteredPartBitmap(gl);
        }
	}
	
	private void storeFilteredPartBitmap(GL10 gl) {
		
	    int w = this.width;
        int h = this.height;
		
        int b[]=new int[w*h];
        int bt[]=new int[w*h];
        IntBuffer ib=IntBuffer.wrap(b);
        ib.position(0);
        gl.glReadPixels(0, 0, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, ib);

        //  OpenGL bitmap is incompatible with Android bitmap and so, some correction need.
        for(int i=0; i<h; i++) {         
            for(int j=0; j<w; j++) {
                int pix=b[i*w+j];
                int pb=(pix>>16)&0xff;
                int pr=(pix<<16)&0x00ff0000;
                int pix1=(pix&0xff00ff00) | pr | pb;
                bt[(h-i-1)*w+j]=pix1;
            }
        }
        Bitmap filteredBitmap = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
        this.setFilteredBitmap(new PixBitmapHolder(filteredBitmap));
	}
	
	public void updateBorder(SplitBorderType borderType) {
		// handle the border over the GLSurface View (for display purpose only)
		switch (borderType) {
		case BORDER_TYPE_WIDE:
			if (mEffectViewBorder == null) {
				mEffectViewBorder = new PixBorder(getContext(), 
						getContext().getResources().getDimensionPixelOffset(R.dimen.pix_split_border_width));
				mEffectViewBorder.setBackgroundColor(getResources().getColor(R.color.transparent));
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.width, 
						this.height);
				this.addView(mEffectViewBorder, params);
			} else {
				mEffectViewBorder.setVisibility(View.VISIBLE);
			}
			break;
			
		case BORDER_TYPE_DEFAULT:
			if (mEffectViewBorder != null) {
				mEffectViewBorder.setVisibility(View.GONE);
			}
			// add default border
			if (mEffectViewDefaultBorder == null) {
				mEffectViewDefaultBorder = new ImageView(getContext());
	    		mEffectViewDefaultBorder.setBackgroundResource(R.drawable.capture_slot_gray);
				mEffectViewDefaultBorder.setVisibility(View.VISIBLE);
				RelativeLayout.LayoutParams defaultBorderParams = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT, 
						RelativeLayout.LayoutParams.MATCH_PARENT);
				addView(mEffectViewDefaultBorder, defaultBorderParams);
			}
		case BORDER_TYPE_NONE:
			if (mEffectViewDefaultBorder != null) {
				mEffectViewDefaultBorder.setVisibility(View.GONE);
			}
			if (mEffectViewBorder != null) {
				mEffectViewBorder.setVisibility(View.GONE);
			}
			break;
		default:
			break;
		}
	}

	public void onPause() {
		if (mEffectView != null) {
			mEffectView.onPause();
		}
		mInitialized = false;// should re-initialize when resume
	}

	public void onResume() {
		if (mEffectView != null) {
			mEffectView.onResume();
		}
	}

	public PixBitmapHolder getFilteredBitmap() {
		return this.filteredBitmapHolder;
	}
	
    @Override
    protected void finalize() throws Throwable {
    	// release bitmaps
    	this.setOrigBitmap(null);
    	this.setFilteredBitmap(null);
        super.finalize();
    }
}

