package com.pixplit.android.util.bitmap;

import java.util.Random;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

public class PixBitmapUtils {
//	private static final String TAG = "PixBitmapUtils";
	private static final int MAX_HW_TEXTURE_DIMEN = 2048;

	private static final int[] exportOverlays = {R.drawable.export_overlay1,
			  R.drawable.export_overlay2,
			  R.drawable.export_overlay3,
			  R.drawable.export_overlay4,
			  R.drawable.export_overlay5,
			  R.drawable.export_overlay6};

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
    	if (bitmap == null) return null;
    	
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
    
    public static Bitmap cropCenterSquare(Bitmap bitmap) {
    	if (bitmap == null) return null;
    	
    	int origWidth = bitmap.getWidth();
    	int origHeight = bitmap.getHeight();
    	
    	if (origWidth == origHeight) return bitmap;
    	
    	int squareDimen = Math.min(origWidth, origHeight);
    	int cropX, cropY;
    	if (origWidth > origHeight) {
    		cropX = (origWidth - squareDimen) / 2;
    		cropY = 0;
    	} else {
    		cropX = 0;
    		cropY = (origHeight - squareDimen) / 2;
    	}
    	
    	return Bitmap.createBitmap(bitmap, cropX, cropY, squareDimen, squareDimen);
    }
    
    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options An options object with out* params already populated (run through a decode*
     *            method with inJustDecodeBounds==true
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger
            // inSampleSize).
            // Anything more than 2x the requested pixels we'll sample down further.
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
            
            // handle openGL bitmap dimensions limit. note that this depends on the device.
            // we use the minimum limit assumption.
            // see http://stackoverflow.com/questions/7428996/hw-accelerated-activity-how-to-get-opengl-texture-size-limit
            int dimenLimit = MAX_HW_TEXTURE_DIMEN;

            // verify that the bitmap is not over the limit and if so scale down
            while (dimenLimit > 0 && height / inSampleSize > dimenLimit) {
            	inSampleSize++;
            }
            while (dimenLimit > 0 && width / inSampleSize > dimenLimit) {
            	inSampleSize++;
            }
        }
        return inSampleSize;
    }
    
    public static Bitmap addExportOverlay(Bitmap compBitmap) {
		Bitmap exportBitmap = compBitmap.copy(compBitmap.getConfig(), true);
		Canvas canvas = new Canvas(exportBitmap);
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		Bitmap overlayBitmap = BitmapFactory.decodeResource(PixApp.getContext().getResources(), getRandomExportOverlay(), options);
		int overlayWidth = overlayBitmap.getWidth();
		int overlayHeight = overlayBitmap.getHeight();
		int overlayLeft = exportBitmap.getWidth() - overlayWidth;
		int overlayTop = exportBitmap.getHeight() - overlayHeight;
		overlayBitmap.setDensity(canvas.getDensity());
		canvas.drawBitmap(overlayBitmap, overlayLeft, overlayTop, null);
		return exportBitmap;
	}
	
	private static int getRandomExportOverlay() {
		Random rand = new Random();
		int min = 0, max = exportOverlays.length - 1;
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomOverlay = (rand.nextInt(max - min + 1) + min)%exportOverlays.length;
		return exportOverlays[randomOverlay];
	}
}
