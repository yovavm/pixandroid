package com.pixplit.android.util;

import java.util.concurrent.atomic.AtomicInteger;

public class PixIdGenerator {

    private static PixIdGenerator INSTANCE = new PixIdGenerator();

    private AtomicInteger seq;

    private PixIdGenerator() {
        seq = new AtomicInteger((int)Math.random());
    }

    public static int getUniqueId() {
        return INSTANCE.seq.incrementAndGet();
    }
}