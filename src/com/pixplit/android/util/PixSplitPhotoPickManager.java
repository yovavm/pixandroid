package com.pixplit.android.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Request.Priority;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.PixBaseActivity;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;

public class PixSplitPhotoPickManager extends PixPhotoPickManager {

	public PixSplitPhotoPickManager(PixBaseActivity activity) {
		super(activity);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (PixDefs.ACTIVITY_RESULT_CODE_PICK_PHOTO == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null) {
        			// photo picked successfully
        			String imageUrl = extras.getString(PixDefs.INTENT_EXTRA_PICKED_PHOTO_URL);
        			if (Utils.isValidString(imageUrl)) {
        				PixImagesLoader.loadImage(imageUrl, 
        						new BitmapFetcherDelegate() {
									@Override
									public void onBitmapFetchFinished(String url, PixBitmapHolder holder) {
										if (holder != null) {
											pickedPhotoBitmap = holder.getBitmap();
										} else {
											pickedPhotoBitmap = null;
										}
										deliverPhotoPickResult();
									}
									
									@Override
									public void onBitmapFetchFailed() {
										pickedPhotoBitmap = null;
										deliverPhotoPickResult();
									}
								}, Priority.IMMEDIATE);
        				// image is being load. mPhotoDelegate will receive the response
        				return;
        			}
        		}
        	}
        	pickedPhotoBitmap = null;
			deliverPhotoPickResult();
        } else {
        	super.onActivityResult(requestCode, resultCode, data);
        }
	}

}
