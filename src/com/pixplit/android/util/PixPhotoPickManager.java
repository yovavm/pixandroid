package com.pixplit.android.util;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.PixBaseActivity;

public class PixPhotoPickManager {

//	private static final String TAG = "PixPhotoPickManager";
	
	private PhotoFetcherDelegate mPhotoPickerDelegate = null;
	protected Bitmap pickedPhotoBitmap = null;
	private PixBaseActivity mActivity;
	private Uri tempPhotoUri;
	private static final String PHOTO_PICK_TEMP_PHOTO_URI_KEY = "PHOTO_PICK_TEMP_PHOTO_URI_KEY";
	
	public interface PhotoFetcherDelegate {
		public void onPhotoFetchSuccess(Bitmap selectedImage);
		public void onPhotoFetchFail();
	}
	
	public PixPhotoPickManager(PixBaseActivity activity) {
		mActivity = activity;
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (PixDefs.ACTIVITY_RESULT_CODE_PHOTO_PICKER == requestCode) {
			if (resultCode == Activity.RESULT_OK) {
				Uri selectedImageUri = null;
				if (this.tempPhotoUri != null) {
					selectedImageUri = this.tempPhotoUri;
				} else if (data != null) {
					selectedImageUri = data.getData();
				}
				if (selectedImageUri != null) {
					pickedPhotoBitmap = Utils.getBitmap(selectedImageUri);
					// delete the temp photo if needed
					this.deleteTempPhoto();
	            }
			}
			if (pickedPhotoBitmap != null) {
	        	//Utils.pixLog(TAG, "Picked photo successfully");
	        } else {
	        	//Utils.pixLog(TAG, "Failed to picked photo");
	        	if (mPhotoPickerDelegate != null) {
	        		mPhotoPickerDelegate.onPhotoFetchFail();
	        	}
	        }
		}
	}
	
	private void deleteTempPhoto() {
		if (this.tempPhotoUri != null) {
			//TODO: delete the temp photo
			this.tempPhotoUri = null;
		}
	}

	public void deliverPhotoPickResult() {
    	if (mPhotoPickerDelegate != null) {
        	if (pickedPhotoBitmap != null) {
            	mPhotoPickerDelegate.onPhotoFetchSuccess(pickedPhotoBitmap);
            	pickedPhotoBitmap = null;
            } else {
            	mPhotoPickerDelegate.onPhotoFetchFail();
            }
        }
    }
	
	public void registerPhotoPickerDelegate(PhotoFetcherDelegate delegate) {
		mPhotoPickerDelegate = delegate;
	}

	public void unregisterPhotoPickerDelegate() {
		mPhotoPickerDelegate = null;
	}

    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	if (savedInstanceState != null && 
    			this.tempPhotoUri == null) {
    		this.tempPhotoUri = savedInstanceState.getParcelable(PHOTO_PICK_TEMP_PHOTO_URI_KEY);
    	}
    }
    
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	if (savedInstanceState != null &&
    			this.tempPhotoUri != null) {
    		savedInstanceState.putParcelable(PHOTO_PICK_TEMP_PHOTO_URI_KEY, 
    				this.tempPhotoUri);
    	}
    }
    
	public void photoPickRequest() {
		if (mActivity == null) return;
		Intent photoPickerIntent = new Intent();
		photoPickerIntent.setType("image/*"); // to pick only images
		photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
		try {
			mActivity.startActivityForResult(photoPickerIntent, PixDefs.ACTIVITY_RESULT_CODE_PHOTO_PICKER);
		} catch (Exception e) {
			// may fail in phones which don't have a gallery
			//Utils.pixLog(TAG, "Failed to pick photo."+e);
			e.printStackTrace();
		}
	}
	
	public void photoCaptureRequest() {
		if (mActivity == null) return;
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		try {
			
			File f = Utils.createImageFile();
			if (f != null) {
				this.tempPhotoUri = Uri.fromFile(f);
				if (this.tempPhotoUri != null) {
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoUri);
				}
			}
			mActivity.startActivityForResult(takePictureIntent, PixDefs.ACTIVITY_RESULT_CODE_PHOTO_PICKER);
		} catch (Exception e) {
			//Utils.pixLog(TAG, "Failed to capture photo."+e);
			e.printStackTrace();
		}
	}


}
