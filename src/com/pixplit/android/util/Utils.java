package com.pixplit.android.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Spanned;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.bitmap.PixBitmapUtils;

public class Utils {
  	private static Time time;
	private Utils() {};

	public static boolean isMdpi() {
		float density = PixApp.getContext().getResources().getDisplayMetrics().density;
		return (density == 1.0f);
	}
	public static boolean isHdpi() {
		float density = PixApp.getContext().getResources().getDisplayMetrics().density;
		return (density == 1.5f);
	}
    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed behavior.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasGingerbreadMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }
    
    public static boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    public static boolean hasIceCreamSandwich() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }
    
    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }
	
	public static boolean isValidString(String str) {
		boolean valid = false;
		
		if (str != null && str.length() > 0){
			valid = true;
		}
		return valid;
	}
	
	public static String getFirstWord(String input) {
		String word = "";
		if (input != null) {
			int i = input.indexOf(' ');
			if (i>0) {
				word = input.substring(0, i);
			} else {
				word = input;
			}
		}
		return word;
	}
	
	public static String addCommasToString(String str) {
	    if(str.length() < 4){
	        return str;
	    }
	    return addCommasToString(str.substring(0, str.length() - 3)) + "," + str.substring(str.length() - 3, str.length());
	}
	
	
	public static void pixLog(String tag, String msg) {
		if (PixApp.inDebug()) {
			Log.d(tag, msg);
		}
	}
	
	public static void setTimeToNow(){
		if (time == null) {
			time = new Time(Time.getCurrentTimezone());
		}
		time.setToNow();
	}
	
	public static long getTimeInMillis(){
		setTimeToNow();
		return time.toMillis(true);
	}
	// Date format
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm", Locale.getDefault());
	private static String timeStampPrefix = "Last Update: ";
	
	public String getTimeStamp(){
    	return timeStampPrefix + dateFormat.format(new Date());
    }
	
	public static void unbindDrawables(View view) {
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            if (!AdapterView.class.isInstance(view)) {
                ((ViewGroup) view).removeAllViews();
            }
        } else {
            Drawable bmp = view.getBackground();
            if (bmp == null && ImageView.class.isInstance(view)) bmp = ((ImageView) view).getDrawable();
            if (bmp != null) {
                bmp.setCallback(null);
            }
        }
    }
	
	public static String getSharedPreferencesString(String key, String defaultValue){
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		return pixSharedPreferences.getString(key, defaultValue);
	}
	
	@SuppressLint("NewApi")
	public static void setSharedPreferencesString(String key, String value) {
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor edit = pixSharedPreferences.edit();
		edit.putString(key, value);
		if (Utils.hasGingerbread()) {
			edit.apply();
		} else {
			edit.commit();
		}
	}
	
	public static Boolean getSharedPreferencesBoolean(String key, boolean defaultValue){
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		return pixSharedPreferences.getBoolean(key, defaultValue);
	}
	
	@SuppressLint("NewApi")
	public static void setSharedPreferencesBoolean(String key, Boolean value) {
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor edit = pixSharedPreferences.edit();
		edit.putBoolean(key, value);
		if (Utils.hasGingerbread()) {
			edit.apply();
		} else {
			edit.commit();
		}
	}
	
	public static int getSharedPreferencesInt(String key, int defaultValue) {
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		return pixSharedPreferences.getInt(key, defaultValue);
	}
	
	@SuppressLint("NewApi")
	public static void setSharedPreferencesInt(String key, int value) {
		SharedPreferences pixSharedPreferences = PixApp.getContext().getSharedPreferences(PixDefs.PIX_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor edit = pixSharedPreferences.edit();
		edit.putInt(key, value);
		if (Utils.hasGingerbread()) {
			edit.apply();
		} else {
			edit.commit();
		}
	}
	
	private static byte[] hash(String input) {
		MessageDigest digest=null;
	    try {
	    	digest = MessageDigest.getInstance("SHA-256");
	    } catch (Exception e1) {
	        // Should never happen for SHA-256
	        e1.printStackTrace();
	    }
	    digest.reset();
	    return digest.digest(input.getBytes());
	}
	
	public static String bin2hex(byte[] data) {
	    return String.format("%0" + (data.length*2) + "x", new BigInteger(1, data));
	}

	public static String pixHash(String input) {
		return bin2hex(hash(input));
	}
	
	public static Rect createRectFromDimentions(int x, int y, int w, int h) {
		return new Rect(x, y, x+w, y+h);
	}
	
	public final static boolean isValidEmail(CharSequence target) {
	    if (target == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	    }
	}

	// Bitmap stuff
	public static Bitmap loadBitmapFromView(View v, int w, int h) {
		if (w > 0 && h > 0 && v != null) {
			Bitmap b = null;
			try {
				b = Bitmap.createBitmap( w, h, Bitmap.Config.ARGB_8888);                
			} catch (Exception e){
				//Utils.pixLog("loadBitmapFromView", "createBitmap failed");
				e.printStackTrace();
			}
			if (b != null) {
				Canvas c = new Canvas(b);
				v.layout(0, 0, w-1, h-1);
				v.draw(c);
			}
			return b;
		}
		return null;
	}
	
	public static Bitmap rotateBitmap(Bitmap original, int degrees) {
		if (original != null) {
	    	Matrix matrix = new Matrix();
	    	matrix.postRotate(degrees);
	    	return Bitmap.createBitmap(original, 0, 0, 
	    	                              original.getWidth(), original.getHeight(), 
	    	                              matrix, false);
		}
		return null;
    }
	
	public static Bitmap flipBitmap(Bitmap src, boolean horizontal)
	{
	    Matrix m = new Matrix();
	    if (horizontal) {
	    	m.preScale(-1, 1);
	    } else {
	    	// vertical
	    	m.preScale(1, -1);
	    }
	    Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), m, false);
	    return dst;
	}
	
	public static Bitmap getBitmap(Uri uri) {

        Bitmap outputImage = null;
        if (uri != null) {
	        try {
	        	Bitmap orgImage = null;
	        	final BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inJustDecodeBounds = true;
		        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        options.inPreferQualityOverSpeed = true;
		        BitmapFactory.decodeStream(PixApp.getContext().getContentResolver().openInputStream(uri), null, options);
		        // Calculate inSampleSize
		        options.inSampleSize = PixBitmapUtils.calculateInSampleSize(options, 
		        		PixDefs.SPLIT_BITMAP_MIN_SCALE_WIDTH, PixDefs.SPLIT_BITMAP_MIN_SCALE_HIGHT);		        
		        // Decode bitmap with inSampleSize set
		        options.inJustDecodeBounds = false;
	            orgImage = BitmapFactory.decodeStream(PixApp.getContext().getContentResolver().openInputStream(uri), null, options);
	            outputImage = handleBitmapOrientation(orgImage, uri);
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
        }
        return outputImage;
    }
	
	private static Bitmap handleBitmapOrientation(Bitmap orgImage, Uri bitmapUri) {
		Bitmap outputImage = orgImage;
		if (orgImage != null && bitmapUri != null) {
			int rotationInDegrees = getOrientationFromMediaStore(bitmapUri);
			if (rotationInDegrees == 0) {
				// try looking up the exif information as well
				rotationInDegrees = getOrientationFromExif(bitmapUri);
			}
			
			if (rotationInDegrees != 0) {
	        	outputImage = Utils.rotateBitmap(orgImage, rotationInDegrees);
	        } else {
	        	outputImage = orgImage;
	        }
		}
		return outputImage;
	}
	
	// works for gallery images
	public static int getOrientationFromMediaStore(Uri photoUri) {
		int rotationInDegrees = 0;
		if (photoUri != null) {
		    Cursor cursor = PixApp.getContext().getContentResolver().query(photoUri,
		            new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
		            null, null, null);
		    if (cursor != null) {
			    try {
			        if (cursor != null && cursor.moveToFirst()) {
			        	rotationInDegrees = cursor.getInt(0);
			        }
			    } catch (Exception e) {
		        	e.printStackTrace();
		        } finally {
		        	if (cursor != null) {
		        		cursor.close();
		        	}
			    } 
		    }
		}
		return rotationInDegrees;
	}
	
	// works for non gallery images
	private static int getOrientationFromExif(Uri photoUri) {   
		int rotationInDegrees = 0;
		ExifInterface exif = null;
		if (photoUri != null) {
			try {
				exif = new ExifInterface(photoUri.getPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (exif != null) {
		        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 
		        					ExifInterface.ORIENTATION_NORMAL);
		        
		        if (rotation == ExifInterface.ORIENTATION_ROTATE_90) { 
		        	rotationInDegrees = 90; 
		        } else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) {
		        	rotationInDegrees = 180; 
		        } else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) {  
		        	rotationInDegrees = 270; 
		        }
			}
		}
        return rotationInDegrees;    
    }
    
	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";
	
	public static File createImageFile() {
	    File image = null;
		try {
		    // Create an image file name
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
			String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
			
			image = (new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					imageFileName + JPEG_FILE_SUFFIX));
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return image;
	}
	
	public static void showMessage(String message, boolean top) {
		Toast messageToast = Toast.makeText(PixApp.getContext(), message, Toast.LENGTH_LONG);
		if (top) {
			messageToast.setGravity(Gravity.TOP, 0, PixApp.getContext().getResources().getDimensionPixelOffset(R.dimen.toast_message_top_margin));
		}
		messageToast.show();
	}
	
	public static void sendMail(String mailTo, String subject, Spanned body) {
		String mailToField = "mailto:";
		if (isValidString(mailTo)) {
			mailToField = mailToField.concat(mailTo);
		}
    	final Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(mailToField));
    	sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
    	if (body != null) {
    		sendIntent.putExtra(Intent.EXTRA_TEXT, body);
    	}
    	sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	PixApp.getContext().startActivity(Intent.createChooser(sendIntent, "Send mail...").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

	public static int countCharOccurrences(String haystack, char needle) {
	    int count = 0;
	    for (int i=0; i < haystack.length(); i++) {
	        if (haystack.charAt(i) == needle) {
	             count++;
	        }
	    }
	    return count;
	}
	
	public static void gotoPlayStore(Context context) {
		if (context != null) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_RATE_ON_APPSTORE_PRESSED, null);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id="+PixApp.getContext().getPackageName()));
			try {
				context.startActivity(intent);
			} catch (Exception e) {
				// may fail in case google play is not on the device
			}
		}
	}
	
	private static File getTempFile(String fileName) {
	    File file = null;
	    try {
//	        String fileName = Uri.parse(url).getLastPathSegment();
	        file = File.createTempFile(fileName, null, PixApp.getContext().getCacheDir());
	    } catch (IOException e) {
	        // Error while creating file
	    }
	    return file;
	}
	
	private static File getFile(String fileName) {
	    File file = null;
	    try {
	    	File path = Environment.getExternalStoragePublicDirectory(
	                Environment.DIRECTORY_PICTURES + "/Pixplit");
	    	file = new File(path, fileName);
	    	path.mkdirs();
	    } catch (Exception e) {
	        // Error while creating file
	    }
	    return file;
	}
	
	public static String storeBitmapInTmpFile(Bitmap bitmap, String fileName) {
		return storeBitmapInFile(bitmap, fileName, true);
	}
	
	public static String storeBitmapInFile(Bitmap bitmap, String fileName) {
		return storeBitmapInFile(bitmap, fileName, false);
	}
	
	public static String storeBitmapInFile(Bitmap bitmap, String fileName, boolean temp) {
		String filePath = null;
		File file = null;
		if (Utils.isValidString(fileName) && bitmap != null) {
			if (temp) {
				file = getTempFile(fileName);
			} else {
				file = getFile(fileName);
			}
		}
		if (file != null) {
			FileOutputStream outStream = null;
			try {
				outStream = new FileOutputStream(file);
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				filePath = file.getPath();
				//Utils.pixLog("storeBitmapInFile", "filePath is:"+filePath);
			} catch (Exception e) {
			       e.printStackTrace();
			} finally {
				if (outStream != null) {
					try {
						outStream.flush();
						outStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (!temp) {
			// Tell the media scanner about the new file so that it is
	        // immediately available to the user.
			MediaScannerConnection.scanFile(PixApp.getContext(),
	                new String[] { file.toString() }, null, null);
			
		}
		return filePath;
	}
	
	public static Bitmap getBitmapFromFile(String filePath, int reqWidht, int reqHeight) {
		Bitmap bitmap = null;
		if (Utils.isValidString(filePath)) {
			//Utils.pixLog("getBitmapFromFile", "filePath is:"+filePath);
			if (reqWidht > 0 && reqHeight > 0) {
				final BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inJustDecodeBounds = true;
		        BitmapFactory.decodeFile(filePath, options);
		        // Calculate inSampleSize
		        options.inSampleSize = PixBitmapUtils.calculateInSampleSize(options, reqWidht, reqHeight);		        
		        // Decode bitmap with inSampleSize set
		        options.inJustDecodeBounds = false;
		        bitmap = BitmapFactory.decodeFile(filePath, options);
			} else {
				bitmap = BitmapFactory.decodeFile(filePath);
			}
		}
		return bitmap;
	}
	
	public static void deleteBitmapFile(String filePath) {
		if (isValidString(filePath)) {
			//Utils.pixLog("deleteBitmapFile", "filePath is:"+filePath);
			try {
				PixApp.getContext().deleteFile(new File(filePath).getName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Bitmap scaleBitmap(Bitmap source, int reqWidht, int reqHeight) {
		Bitmap scaledBitmap = null;		
		int width = source.getWidth();
		int height = source.getHeight();
		float scaleWidth = ((float)reqWidht) / width;
		float scaleHeight = ((float)reqHeight) / height;
		// in order to keep the original aspect ratio use one scale
		float scale = Math.min(scaleWidth, scaleHeight);
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scale, scale);
		// RECREATE THE NEW BITMAP
		try {
			scaledBitmap = Bitmap.createBitmap(source, 0, 0, width, height, matrix, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return scaledBitmap;
	}
	
	public static int clamp(int x, int min, int max) {
        if (x > max) return max;
        if (x < min) return min;
        return x;
    }
	
	/*
	 * This method use PackageManager Class to check for instagram package.
	 * */
	public static boolean isAppInstalled(String appPackage) {

		boolean app_installed = false;
		try {
			ApplicationInfo info = PixApp.getContext().getPackageManager().getApplicationInfo(appPackage, 0);
			if (info != null) {
				app_installed = true;
			}
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}
	
	public static boolean isNumeric(String str) {  
	  try {  
	    @SuppressWarnings("unused")
		int d = Integer.parseInt(str);
	  } catch(NumberFormatException nfe) {  
	    return false;  
	  }  
	  return true;  
	}
	
//	/**
//	 * 
//	 * @param colorHexStr e.g. "#FFFFFF"
//	 * @return rgb color format e.g. "244,244,244"
//	 */
//	@SuppressLint("DefaultLocale")
//	public static String hex2Rgb(String colorHexStr) {
//		String rgb = PixDefs.defaultColorSchemeString;
//		// validations
//		if (colorHexStr == null ||
//				colorHexStr.charAt(0) != '#' ||
//				colorHexStr.length() != 7) return rgb;
//		try {
//		    rgb = String.format("%03d,%03d,%03d", 
//		            Integer.valueOf( colorHexStr.substring( 1, 3 ), 16 ),
//		            Integer.valueOf( colorHexStr.substring( 3, 5 ), 16 ),
//		            Integer.valueOf( colorHexStr.substring( 5, 7 ), 16 ) );
//		} catch (Exception e) {
//			// malformed hex format, return the default color scheme
//		}
//	    return rgb;
//	}
	
	@SuppressLint("DefaultLocale")
	public static int rgb2Color(String colorRgbStr) {
		int color = PixDefs.defaultColorScheme;
		try {
			int rEnd = colorRgbStr.indexOf(',');
			int gStart = rEnd+1;
			int gEnd = colorRgbStr.indexOf(',', gStart+1);
			int bStart = gEnd+1;
			int r = Integer.valueOf(colorRgbStr.substring( 0, rEnd ));
			int g = Integer.valueOf(colorRgbStr.substring( gStart, gEnd ));
			int b = Integer.valueOf(colorRgbStr.substring( bStart));
			String hex = String.format("#%02X%02X%02X", r, g, b);
			color = Color.parseColor(hex);
		} catch (Exception e) {
			// malformed rgb format, return the default color scheme
		}
	    return color;
	}
	
	@SuppressLint("DefaultLocale")
	public static String color2Rgb(int color) {
		String rgb = PixDefs.defaultColorSchemeString;
		try {
			rgb = String.format("%03d,%03d,%03d", 
					(color >> 16) & 0xFF, 
					(color >> 8) & 0xFF,
					color & 0xFF);
		} catch (Exception e) {
			// malformed color int, return the default color scheme
		}
		return rgb;
	}
}
