package com.pixplit.android.util.appreciation;

import android.content.Context;

public class AppreciationManager {

	// The kudosKit object (from package com.ironsrc.kudoskit)
//	private KudosKit mKudosKit = null;
	
	public void onStart() {
//    	this.mKudosKit.onStart();
    }
    
	public void onStop() {
//    	this.mKudosKit.onStop();
    }
    
	public void onDestroy() {
//    	this.mKudosKit.onDestroy();
    }
	
	public void onEvent(String eventName) {
//		this.mKudosKit.startRightspot(eventName);
	}
	
	public static AppreciationManager getAppreciationManager(Context context) {
		AppreciationManager appreciationManager = new AppreciationManager();
//		appreciationManager.mKudosKit = new KudosKit(context, PixAppState.KUDOS_APP_ID, PixAppState.KUDOS_APP_KEY);
//		appreciationManager.mKudosKit.setup(PixAppState.KUDOS_FEATURES, PixAppState.KUDOS_INAPP_IDS);
		return appreciationManager;
	}
}
