package com.pixplit.android.fb;

import com.facebook.Request.Callback;
import com.facebook.Response;


public class FbPostRequestHandler extends FbBaseRequestHandler implements Callback{

	public FbPostRequestHandler(FbRequestHandlerDelegate delegate) {
		super(delegate);
	}

	@Override
	public void onCompleted(Response response) {
		if (mDelegate != null) {
			if (response.getError() != null) {
				mDelegate.onRequestHandlerFailed(this, new Error(response.getError().getErrorMessage()));
			} else {
				mDelegate.onRequestHandlerFinished(this);
			}
		}
		
	}

}
