package com.pixplit.android.fb;

import com.pixplit.android.data.FacebookAlbum;

public interface FbAlbumPickDelegate {
	public void onAlbumPick(FacebookAlbum album);
}
