package com.pixplit.android.fb;

import com.pixplit.android.data.FacebookPhoto;

public interface FbPhotoPickDelegate {
	public void onPhotoPick(FacebookPhoto photo);
}
