package com.pixplit.android.fb;



public class FbBaseRequestHandler {

	FbRequestHandlerDelegate mDelegate;
	private FbProcessType processType;
	private FbRequestHandlerType type;
	
	public interface FbRequestHandlerDelegate {
		public void onRequestHandlerFinished(FbBaseRequestHandler handler);
		public void onRequestHandlerFailed(FbBaseRequestHandler handler, Error error);
	}
	
	public FbBaseRequestHandler(FbRequestHandlerDelegate delegate) {
		mDelegate = delegate;
	}
	
	public void requestFailed(Error error) {
		if (mDelegate != null) {
			mDelegate.onRequestHandlerFailed(this, error);
		}
	}

	public FbProcessType getProcessType() {
		return processType;
	}

	public void setProcessType(FbProcessType processType) {
		this.processType = processType;
	}

	public FbRequestHandlerType getType() {
		return type;
	}

	public void setType(FbRequestHandlerType type) {
		this.type = type;
	}

	public static enum FbRequestHandlerType {
		FB_AUTHENTICATE_REQUEST_HANDLER
	}
}
