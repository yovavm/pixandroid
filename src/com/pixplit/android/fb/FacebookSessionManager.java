package com.pixplit.android.fb;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.Request;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.util.Utils;

public class FacebookSessionManager implements Session.StatusCallback{
//	private static final String TAG = "FacebookSessionManager";
	private UiLifecycleHelper mFbUiHelper;
	FbSessionDelegate sessionDelegate;
	
	public static final List<String> PUBLISH_PERMISSIONS = Arrays.asList("publish_actions");
	public static final List<String> FETCH_PHOTOS_PERMISSIONS = Arrays.asList("user_friends", "basic_info", "user_photos");
	
	public interface FbSessionDelegate {
		public void onOpenSessionFinished();
		public void onOpenSessionTokenUpdate();
		public void onOpenSessionFailed(Error error);
	}
			
	public boolean isOpenForPublish() {
		return this.isOpenForPublish(Session.getActiveSession());
	}
	
	public boolean isOpenForFetchPhotos() {
	    return this.isOpenForFetchPhotos(Session.getActiveSession());
	}
	
	private boolean isOpenWithPermissions(Session session, List<String> neededPermissions) {
		if (neededPermissions == null || session == null) return true;
	    // Check for publish permissions    
        List<String> permissions = session.getPermissions();
        if (permissions == null) return false;
        for (String neededPermission : neededPermissions) {
        	boolean havePermission = false;
        	for (String permission : permissions) {
        		if (permission.equalsIgnoreCase(neededPermission)) {
        			havePermission = true;
        			break;
        		}
        	}
        	if (!havePermission) {
        		return false;
        	}
        }
        return true;
	}
	
	private boolean isOpenForPublish(Session session) {
		return isOpenWithPermissions(session, PUBLISH_PERMISSIONS);
	}
	
	private boolean isOpenForFetchPhotos(Session session) {
		return isOpenWithPermissions(session, FETCH_PHOTOS_PERMISSIONS);
	}
	
	public void openSessionWithPublishPermissions(Activity activity) {
		Session session = Session.getActiveSession();
		if (session != null) {
			if (session.isOpened()) {
				if (this.isOpenForPublish(session)) {
					// validation - session is already open for publish
					if (this.sessionDelegate != null) {
						this.sessionDelegate.onOpenSessionFinished();
					}
					return;
				}
				
				//Utils.pixLog(TAG, "requesting publish permissions on an open session");
				Session.NewPermissionsRequest newPermissionsRequest = new Session
		                .NewPermissionsRequest(activity, PUBLISH_PERMISSIONS);
				newPermissionsRequest.setCallback(this);
				try {
					session.requestNewPublishPermissions(newPermissionsRequest);
				} catch (Exception e) {
					/*
					 *  may cause exception in case an attempt was made to request new permissions 
					 *  for a session that has a pending request.
					 */
					//Utils.pixLog(TAG, "got exception calling requestNewPublishPermissions: "+e);
					if (this.sessionDelegate != null) {
						this.sessionDelegate.onOpenSessionFailed(
								new Error("got exception calling requestNewPublishPermissions: "+e));
					}
					return;
				}
			} else {
				Session.OpenRequest openForPublishRequest = new Session.OpenRequest(activity);
				openForPublishRequest.setCallback(this);
				openForPublishRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
				openForPublishRequest.setPermissions(PUBLISH_PERMISSIONS);
				openForPublishRequest.setRequestCode(Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE);
				try {
					session.openForPublish(openForPublishRequest);
				} catch (Exception e) {
					//Utils.pixLog(TAG, "got exception calling openForPublish: "+e);
					if (this.sessionDelegate != null) {
						this.sessionDelegate.onOpenSessionFailed(new Error("got exception calling openForPublish: "+e));
					}
					return;
				}
			}
		} else {
			//Utils.pixLog(TAG, "can't ask for publish permission without having basic persmissions before");
			if (this.sessionDelegate != null) {
				this.sessionDelegate.onOpenSessionFailed(new Error("can't ask for publish permission without having basic persmissions before"));
			}
			return;
		}
		
		return;
	}
	
	public void openSession(Activity activity, List<String> permissions) {
		Session session = Session.getActiveSession();
		
		if (session != null) {
			//Utils.pixLog(TAG, "active session state:"+session.getState().toString());
			if (session.isOpened()) {
				if (permissions == null ||
						isOpenWithPermissions(session, permissions)) {
					return;
				}
			}
			session.close();
		}
		session = Session.openActiveSession(activity, true, this);
		
		if (session == null || session.isClosed()) {
			//Utils.pixLog(TAG, "open new session returned invalid session");
			if (this.sessionDelegate != null) {
				this.sessionDelegate.onOpenSessionFailed(null);
			}
			return;
		}
		
		if (!session.isOpened()) {
			OpenRequest openRequest = new OpenRequest(activity);
			openRequest.setCallback(this);
			if (permissions != null) {
				openRequest.setPermissions(permissions);
			}
			session.openForRead(openRequest);
		} else {
			// check if the open session has the requested permissions
			boolean havePermissions = isOpenWithPermissions(session, permissions);
			if (!havePermissions) {
				requestNewReadPermissions(session, activity, permissions);
			}
		}
	}
	
	private void requestNewReadPermissions(Session session, Activity activity, List<String> permissions) {
		if (session == null) return;
		NewPermissionsRequest newPermissionsRequest = new NewPermissionsRequest(activity, permissions);
		newPermissionsRequest.setCallback(this);
		try {
			session.requestNewReadPermissions(newPermissionsRequest);
		} catch (Exception e) {
			/*
			 *  may cause exception in case an attempt was made to request new permissions 
			 *  for a session that has a pending request.
			 */
			//Utils.pixLog(TAG, "got exception calling requestNewPublishPermissions: "+e);
			if (this.sessionDelegate != null) {
				this.sessionDelegate.onOpenSessionFailed(
						new Error("got exception calling requestNewPublishPermissions: "+e));
			}
			return;
		}
		
	}
	
	public void logout() {
		Session session = Session.getActiveSession();
		if (session != null) {
			session.close();
		}
	}
	
	@Override
	public void call(Session session, SessionState state, Exception exception) {
		//Utils.pixLog(TAG, "Called on state:"+state.toString() + ((exception!=null)?(" with exception:"+exception):""));
		switch (state) {
		case CLOSED:
			break;
		case CLOSED_LOGIN_FAILED:
			break;
		case CREATED:
			break;
		case CREATED_TOKEN_LOADED:
			break;
		case OPENED:
			if (this.sessionDelegate != null) {
				this.sessionDelegate.onOpenSessionFinished();
	    	}
			break;
		case OPENED_TOKEN_UPDATED:
			if (this.sessionDelegate != null) {
				this.sessionDelegate.onOpenSessionTokenUpdate();
			}
			break;
		case OPENING:
			break;
		default:
			break;
		}
	}

	public void onCreate(Activity activity, Bundle savedInstanceState, FbSessionDelegate delegate) {
		//Utils.pixLog(TAG, "==> FB onCreate called. activity:"+activity.toString());
		this.sessionDelegate = delegate;
		mFbUiHelper = new UiLifecycleHelper(activity, this);
		mFbUiHelper.onCreate(savedInstanceState);
		
	}

	public void onResume() {
		mFbUiHelper.onResume();
		
	}

	public void onPause() {
		mFbUiHelper.onPause();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mFbUiHelper.onActivityResult(requestCode, resultCode, data);
	}

	public void onDestroy() {
		mFbUiHelper.onDestroy();
	}

	public void onSaveInstanceState(Bundle outState) {
		mFbUiHelper.onSaveInstanceState(outState);
	}
	
	public void fetchUserInfo(FbAuthenticationRequestHandler requestDelegate) {
		if (requestDelegate != null) {
			Session session = Session.getActiveSession();
			if (session != null) {
				if (session.isOpened()) {
					// make request to the /me API
					Request.newMeRequest(session, requestDelegate).executeAsync();
				} else {
					requestDelegate.requestFailed(new Error("Pix: FB session is not open"));
				}
			} else {
				requestDelegate.requestFailed(new Error("Pix: no active session"));
			}
		} else {
			//Utils.pixLog(TAG, "Fetch user info with no request handler. abort request");
		}
	}
	
	public void postRequest(FbPostRequestHandler requestDelegate, String graphPath, GraphObject graphObject) {
		
		Session session = Session.getActiveSession();
		if (session != null) {
			if (session.isOpened()) {
				Request.newPostRequest(session, graphPath, graphObject, requestDelegate).executeAsync();
			} else {
				if (requestDelegate != null) {
					requestDelegate.requestFailed(new Error("Pix: FB session is not open"));
				}
			}
		} else {
			if (requestDelegate != null) {
				requestDelegate.requestFailed(new Error("Pix: no active session"));
			}
		}
	
	}
	
	public boolean hasOpenSession(List<String> permissions) {
		Session session = Session.getActiveSession();
		if (session == null) return false;
		boolean isOpen = session.isOpened();
		if (!isOpen) return false;
		if (permissions != null &&
				!isOpenWithPermissions(session, permissions)) {
			return false;
		}
		return true; 
	}
	
	public boolean isLinked() {
		return (this.hasOpenSession(null) && 
    			Utils.isValidString(PixAppState.UserInfo.getFbUserName()));
	}
	
	public Session getActiveSession() {
		return Session.getActiveSession();
	}
	
	public static String getAccessToken() {
		Session session = Session.getActiveSession();
		if (session != null) {
			return session.getAccessToken();
		}
		return null;
	}
	
	
}
