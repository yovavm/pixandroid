package com.pixplit.android.fb;

import java.util.List;

public interface FbActivityInterface {
	public void authenticateWithFb();
	public void authorizePublishToFb(FbProcessType processType, FbConnectDelegate connectDelegate);
	public boolean isFbLinked();
	public boolean isFbOpenForPublish();
	public void logoutFb();
	public void authenticateWithFbRequest(FbProcessType processType, FbConnectDelegate connectDelegate, List<String> permissions);
	public void postLikesToFb(String compId);
	public void shareNewCompInFb(String compId, String users);
	public void inviteFbFriends();
	public void showGetStarted();
	public boolean verifyUser();
}
