package com.pixplit.android.fb;

public interface FbConnectDelegate {
	public void onFbConnectFinished(FbProcessType processType);
	public void onFbConnectFailed(FbProcessType processType);
}