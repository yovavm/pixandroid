package com.pixplit.android.fb;

import android.util.SparseArray;

public enum FbProcessType {
	FB_PROCESS_AUTHENTICATION	(1),
	FB_PROCESS_AUTHORIZE_PUBLISH(2),
	FB_PROCESS_LINK_ACCOUNT		(3),
	FB_PROCESS_LIKES			(4),
	FB_PROCESS_FIND_FRIENDS		(5),
	FB_PROCESS_PICK_PHOTO		(6),
	FB_PROCESS_UPDATE_PERMISSIONS(7);
	
	private final int type;
	
	FbProcessType(int type) {
		this.type = type;
	}
	
	private static final SparseArray<FbProcessType> map = new SparseArray<FbProcessType>();
	
	static {
		for (FbProcessType processType : FbProcessType.values()) {
			map.put(processType.type, processType);
		}
	}
	
	public static FbProcessType getType(int intType) {
		return map.get(intType);
	}
	
	public static int getInt(FbProcessType enumType) {
		if (enumType != null) {
			return enumType.type;
		}
		return -1;
	}
}
