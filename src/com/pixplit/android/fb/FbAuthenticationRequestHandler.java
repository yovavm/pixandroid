package com.pixplit.android.fb;

import com.facebook.FacebookRequestError;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.data.PixUserInfo;

public class FbAuthenticationRequestHandler extends FbBaseRequestHandler implements GraphUserCallback{

	private PixUserInfo userInfo;
	private FbConnectDelegate mConnectionDelegate;
	
	public FbAuthenticationRequestHandler(FbRequestHandlerDelegate delegate) {
		super(delegate);
		setType(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER);
	}
	
	public FbConnectDelegate getConnectionDelegate() {
		return mConnectionDelegate;
	}

	public void setConnectionDelegate(FbConnectDelegate mConnectionDelegate) {
		this.mConnectionDelegate = mConnectionDelegate;
	}

	@Override
	public void onCompleted(GraphUser user, Response response) {
		userInfo = createPixUser(user);
    	
    	if (userInfo != null) {
    		PixAppState.UserInfo.storeFbCredentials(userInfo);
    		if (mDelegate != null) {
    			mDelegate.onRequestHandlerFinished(this);
    		}
    	}
    	else {
    		if	(response != null) {
    			FacebookRequestError fbError = response.getError();
    			if (fbError != null) {
    				this.requestFailed(new Error(fbError.getErrorMessage()));
    			}
    			else {
    				this.requestFailed(null);
    			}
    		}
    		PixAppState.UserInfo.clearFbCredentials();
      	}
	}
	
	public PixUserInfo getUserInfo() {
		return userInfo;
	}
	
	private PixUserInfo createPixUser(GraphUser user) {
		PixUserInfo pixUserInfo = null;
		if (user != null) {
			pixUserInfo = new PixUserInfo();
			pixUserInfo.setFullName(user.getName());
			pixUserInfo.setId(user.getId());
			pixUserInfo.setUserName(user.getUsername());
		}
		return pixUserInfo;
	}
}
