package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.InviteFollowersListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.data.PixUsers;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.UsersFeedHttpRequest;
import com.pixplit.android.ui.elements.SearchUsers;


public class InviteFollowersFragment extends PixListFragment<PixUser>{

	SearchUsers mSearchUsers;
	String inviteeList = null; // previously selected invitee list. needed in order to initialize the current list properly.
	
	public static InviteFollowersFragment newInstance(Bundle args) {
		InviteFollowersFragment fragment = new InviteFollowersFragment();
		Bundle b = new Bundle();
		if (args != null) {
			b.putAll(args);
        }
		b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
    	fragment.setArguments(args);
        return fragment;
	}
			
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setAllowPaging(true);
        
        Bundle args = getArguments();
        if (args != null) {
        	inviteeList = args.getString(PixDefs.FRAG_ARG_INVITEE_LIST);
        }
        setFragmentType(EFragmentType.INVITE_FOLLOWERS);
        setRequestUrl(PixHttpAPI.USER_FOLLOWERS_URL_PREFIX + PixAppState.UserInfo.getUserName());
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.users_layout, container, false);
		this.addSearchUsersHeader();
		return mFragmentView;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		if (mToolbarController != null) {
			mToolbarController.addToolbarTextBtn(
	    			R.drawable.done_btn_selector, 
					PixDefs.TOOLBAR_BTN_ID_DONE,
	        		new OnClickListener() {
						@Override
						public void onClick(View v) {
							String users = getSelectedUsers();
							// return the list result to the calling activity
					    	if (getActivity() != null) {
								Intent resultIntent = new Intent();
								resultIntent.putExtra(PixDefs.INTENT_EXTRA_INVITE_FOLLOWERS_LIST, users);
								getActivity().setResult(Activity.RESULT_OK, resultIntent);
								getActivity().finish();
					    	}
						}
					}, 
					PixApp.getContext().getResources().getString(R.string.done));
		}
	}
	
	@Override
    public boolean onToolbarHomePressIntercepted() {
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }
	
	private String getSelectedUsers() {
		String users = null;
		// create the list of selected users
		ArrayList<String> selectedUsers = ((InviteFollowersListAdapter)mAdapter).getSelectedUsersList();
		if (selectedUsers != null) {
			StringBuffer invitedUsersStrBuf = new StringBuffer();
			for (String user : selectedUsers) {
				if (invitedUsersStrBuf.length() > 0) {
					invitedUsersStrBuf.append(',');
				}
				invitedUsersStrBuf.append(user);
			}
			users = invitedUsersStrBuf.toString();
		}
		return users;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixUsers) {
			PixUsers users = (PixUsers)response;
			ArrayList<PixUser> usersFeed = users.getItems();
			// update the previously invited users
			if (usersFeed != null && inviteeList != null) {
				for (PixUser user : usersFeed) {
					if (inviteeList.contains(user.getUsername())) {
						user.setSelected(true);
					}
				}
			}
			updateAdapterData(usersFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
    	return new UsersFeedHttpRequest(this, getRequestUrl());
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	InviteFollowersListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new InviteFollowersListAdapter(getActivity(), this, R.layout.invite_user_layout);
        	mSearchUsers.assignAdapter(adapter);
    	}
    	return adapter;
	}
    
    protected void addSearchUsersHeader() {
    	mSearchUsers = new SearchUsers(getActivity());
		if (mSearchUsers != null && 
				mFragmentView instanceof LinearLayout) {			
			((LinearLayout)mFragmentView).addView(mSearchUsers, 0);
		}
    }
}
