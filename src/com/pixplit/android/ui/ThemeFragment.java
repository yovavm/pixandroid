package com.pixplit.android.ui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.protocol.HTTP;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public class ThemeFragment extends PixFeedFragment{
	String themeTag;
	
	public static ThemeFragment newInstance(Bundle args) {
		ThemeFragment fragment = new ThemeFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle args = getArguments();
        if (args != null) {
        	themeTag = args.getString(PixDefs.FRAG_ARG_TAG);
        }
        setAllowPaging(true);
        setFragmentType(EFragmentType.RELATED);
        if (Utils.isValidString(themeTag)) {
        	String encodedThemeTag = themeTag;
        	try {
				encodedThemeTag = URLEncoder.encode(themeTag, HTTP.UTF_8);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        setRequestUrl(PixHttpAPI.THEME_FEED_URL_PREFIX + encodedThemeTag);
	        if (mToolbarController != null) {
	        	mToolbarController.setToolBarTitle("#"+themeTag);
	        }
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.theme_feed_layout, container, false);
		
		return mFragmentView;
	}
}
