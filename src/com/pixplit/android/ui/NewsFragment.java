package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.adapters.NewsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixNotification;
import com.pixplit.android.data.PixNotifications;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.NotificationsFeedHttpRequest;
import com.pixplit.android.http.request.ResetBadgeCountHttpRequest;
import com.pixplit.android.util.Utils;


public class NewsFragment extends PixListFragment<PixNotification>{

	public static NewsFragment newInstance() {
		NewsFragment fragment = new NewsFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRefreshBtn(true);
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.NEWS);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.news_layout, container, false);
//		if (!UserInfo.isUserLogedin()) {
//			this.addWelcomeMessage();
//		}
		return mFragmentView;
	}
	
//	private void addWelcomeMessage() {
//		PixTextView welcomeMsg = new PixTextView(getContext());
//		welcomeMsg.setText("    Welcome to Pixplit motherfucker. click here to start (-:");
//		welcomeMsg.setGravity(Gravity.CENTER_VERTICAL);
//		welcomeMsg.setTextColor(PixApp.getContext().getResources().getColor(R.color.black));
//		welcomeMsg.setBackgroundResource(R.drawable.android_blue_btn_selector);
//		welcomeMsg.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (mFbActivity != null) {
//					mFbActivity.showGetStarted();
//				}
//			}
//		});
//		if (mFragmentView != null) {
//			ViewGroup layout = (ViewGroup) mFragmentView.findViewById(R.id.feedLayout);
//			if (layout != null && layout instanceof LinearLayout) {
//				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 100);
//				((LinearLayout)layout).addView(welcomeMsg, 0, params);
//			}
//		}
//		// turn off the new news item - as we displayed the welcome message
//		Utils.setSharedPreferencesBoolean(PixDefs.WELCOME_NEWS_READ, true);
//		PixAppState.setNewNewsNumber(0);
//	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixNotifications) {
			PixNotifications notifications = (PixNotifications)response;
			ArrayList<PixNotification> notificationFeed = notifications.getItems();
			if (this.isFirstPage(requestContext)) {
				((NewsListAdapter)mAdapter).resetLastNewsTimestamp();
				updateNewRecordsTimestamp(notificationFeed);
			}
			updateAdapterData(notificationFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	if (UserInfo.isUserLogedin()) {
    		return new NotificationsFeedHttpRequest(this);
    	} else {
    		// don't send a request to the server as the user hasn't signed in
    		return null;
    	}
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	NewsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new NewsListAdapter(getActivity(), this);
    	}
    	return adapter;
	}
    
    @Override
    public void onCompImageClick(String compId) {
    	if (!Utils.isValidString(compId)) return;
    	Bundle args = new Bundle();
		args.putString(PixDefs.FRAG_ARG_COMP_ID, compId);
		if (mActivitiesController != null) {
			mActivitiesController.gotoSingleCompView(getActivity(), args, 0);
		}
    }
    
    private void updateNewRecordsTimestamp(ArrayList<PixNotification> notificationFeed) {
    	if (notificationFeed != null && notificationFeed.size() > 0) {
    		PixNotification newestNotification = notificationFeed.get(0);
    		if (newestNotification != null && newestNotification.getInsertDate() != null) {
    			PixAppState.setTimestamp(PixAppState.NEWS_TIMESTAMP, newestNotification.getInsertDate());
    			// reset the badge count (not currently used on android, but for iOS competability
    			new ResetBadgeCountHttpRequest().send();
    			// make the notification manager aware on the update without waiting to next pull
    			if (mActivity != null) {
    				mActivity.pullNewRecords();
    			}
    		}
    	}
    }
}
