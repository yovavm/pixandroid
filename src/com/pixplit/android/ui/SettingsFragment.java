package com.pixplit.android.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.fb.FbConnectDelegate;
import com.pixplit.android.fb.FbProcessType;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.google.GPlusConnectDelegate;
import com.pixplit.android.google.GPlusProcessType;
import com.pixplit.android.http.request.AdminLogoutHttpRequest;
import com.pixplit.android.instagram.InstaLoginDialog;
import com.pixplit.android.instagram.InstaLoginDialog.InstaAuthListener;
import com.pixplit.android.ui.elements.AdminMenuBtn;
import com.pixplit.android.ui.elements.AdminMenuBtn.AdminMenuBtnDelegate;
import com.pixplit.android.ui.elements.AdminSignInDialog;
import com.pixplit.android.ui.elements.AdminSignInDialog.AdminSignInDialogDelegate;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.elements.ShareToBtn;
import com.pixplit.android.ui.elements.ShareToBtn.ShareToBtnDelegate;
import com.pixplit.android.ui.fragments.PixFbBaseFragment;
import com.pixplit.android.util.Utils;


public class SettingsFragment extends PixFbBaseFragment implements ShareToBtnDelegate, FbConnectDelegate{
	@SuppressWarnings("unused")
	private static final String TAG = "SettingsFragment";
	
	private static final String failed = PixApp.getContext().getString(R.string.failed);
	
	ShareToBtn shareToFb = null;
	ShareToBtn shareToInsta = null;
	ShareToBtn shareToGPlus = null;
	
	public static SettingsFragment newInstance() {
		SettingsFragment fragment = new SettingsFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.SETTINGS);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.settings_layout, container, false);
    	
    	// log out
    	TextView logoutBtn = (TextView)mFragmentView.findViewById(R.id.logoutBtn);
        if (logoutBtn != null) {
    		logoutBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (getActivity() != null) {
						new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
							public void onConfirm() {
								if (mActivity != null) {
									PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_LOGOUT_PRESSED, null);
									mActivity.finishFragment();
									mActivity.logoutRequest(false);
								}
							}
							public void onDecline() {}
						}, 
						PixApp.getContext().getResources().getString(R.string.logout),
						PixApp.getContext().getResources().getString(R.string.are_you_sure), 
						PixApp.getContext().getResources().getString(R.string.logout), null).show();
					}
				};
			});
    	}
        
        // Find friends
        View findFriendsBtn = mFragmentView.findViewById(R.id.findFriendsBtn);
        if (findFriendsBtn != null) {
        	findFriendsBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_FIND_FACEBOOK_FRIENDS_PRESSED, null);
					onFindFriendsBtnClicked();
				}
			});
        }

        // Push notifications
        View pushNotificationsBtn = mFragmentView.findViewById(R.id.pushNotificationsBtn);
        if (pushNotificationsBtn != null) {
        	pushNotificationsBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_PUSH_NOTIFICATIONS_PRESSED, null);
					if (mActivitiesController != null) {
						mActivitiesController.gotoSettingsNotifications(getActivity());
					}
				}
			});
        }
        
        // edit profile
        View editProfileBtn = mFragmentView.findViewById(R.id.editProfileBtn);
        if (editProfileBtn != null) {
        	editProfileBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_EDIT_PROFILE_PRESSED, null);
					if (mActivitiesController != null) {
						Bundle args = new Bundle();
						args.putInt(PixDefs.FRAG_ARG_ANIMATION_TYPE, 
								ActivitiesControllerInterface.PIX_ANIMATE_SLIDE_IN_RIGHT_TO_LEFT);
						mActivitiesController.gotoEditProfile(getActivity(), args);
					}
				}
			});
        }
        
        // share to FB
        shareToFb = (ShareToBtn)mFragmentView.findViewById(R.id.shareToFacebook);
        if (shareToFb != null) {
        	shareToFb.setDelegate(this, PixApp.getContext().getResources().getString(R.string.facebook));
        	if (mFbActivity != null) {
            	shareToFb.updateView(mFbActivity.isFbLinked(), PixAppState.UserInfo.getFbUserName());
        	}
        }
        
        // share to Instagram
        shareToInsta = (ShareToBtn)mFragmentView.findViewById(R.id.shareToInstagram);
        if (shareToInsta != null) {
        	shareToInsta.setDelegate(
        			new ShareToBtnDelegate() {
						@Override
						public void onShareToCheckboxClicked() {
							onShareToInstaBtnClicked();
						}
						
						@Override
						public void onShareToBtnClicked() {
							onShareToInstaBtnClicked();
						}
					}, 
        			PixApp.getContext().getResources().getString(R.string.instagram));
        	if (PixAppState.UserInfo.getInstagramToken() != null) {
        		String username = PixAppState.UserInfo.getInstagramFullName();
        		if (!Utils.isValidString(username)) {
        			username = PixAppState.UserInfo.getInstagramUsername();
        		}
        		shareToInsta.updateView(true, username);
        	} else {
        		shareToInsta.updateView(false, "");
        	}
        }
        
        // share to Google Plus
        shareToGPlus = (ShareToBtn)mFragmentView.findViewById(R.id.shareToGPlus);
        if (shareToGPlus != null) {
        	if (UserInfo.isGPlusLinked()) {
        		// only expose the GPlus link/unlink button when the user is linked to GPlus.
        		shareToGPlus.setVisibility(View.VISIBLE);
        	} else {
        		shareToGPlus.setVisibility(View.GONE);
        	}
        	shareToGPlus.setDelegate(
        			new ShareToBtnDelegate() {
						@Override
						public void onShareToCheckboxClicked() {
							onShareToGplusBtnClicked();
						}
						
						@Override
						public void onShareToBtnClicked() {
							onShareToGplusBtnClicked();
						}
					}, 
        			PixApp.getContext().getResources().getString(R.string.google));
        	if (PixAppState.UserInfo.getGPlusUserName() != null) {
        		String username = PixAppState.UserInfo.getGPlusUserName();
        		shareToGPlus.updateView(true, username);
        	} else {
        		shareToGPlus.updateView(false, "");
        	}
        }
        
        // tell a friend
        View tellFriendBtn = mFragmentView.findViewById(R.id.tellFriendBtn);
        if (tellFriendBtn != null) {
        	tellFriendBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_TELL_A_FRIEND_PRESSED, null);
					sendMailToFriend();
				}
			});
        }
        
        // Rate at google play
        View rateUsBtn = mFragmentView.findViewById(R.id.rateUsBtn);
        if (rateUsBtn != null) {
        	rateUsBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (getActivity() != null) {
						Utils.gotoPlayStore(getActivity());
					}
				}
			});
        }
        
        // Pixplit Info
        View infoBtn = mFragmentView.findViewById(R.id.infoBtn);
        if (infoBtn != null) {
        	infoBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_INFO_PRESSED, null);
					if (mActivitiesController != null) {
						mActivitiesController.gotoInfo(getActivity());
					}
				}
			});
        }
        
        // send feedback
        View sendFeedbackBtn = mFragmentView.findViewById(R.id.sendFeedbackBtn);
        if (sendFeedbackBtn != null) {
        	sendFeedbackBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_SEND_FEEDBACK_PRESSED, null);
					sendFeedback();
				}
			});
        }
        
        if (PixApp.inAdminBuild()) {
        	LinearLayout layout = (LinearLayout)mFragmentView.findViewById(R.id.settingsLayout);
        	if (layout != null) {
        		final AdminMenuBtn adminBtn = new AdminMenuBtn(getActivity());
        		if (adminBtn != null) {
        			adminBtn.updateView(UserInfo.hasAdminToken());
        		}
        		adminBtn.setDelegate(new AdminMenuBtnDelegate() {
					@Override
					public void onAdminMenuCheckboxClicked() {
						if (UserInfo.hasAdminToken()) {
							// admin is signed in. check weather to log admin out.
							new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
								@Override
								public void onDecline() {
									// do nothing
								}
								@Override
								public void onConfirm() {
									// log out admin
									new AdminLogoutHttpRequest().send();
									UserInfo.setAdminToken(null);
									adminBtn.updateView(false);
								}
							}, 
							PixApp.getContext().getResources().getString(R.string.logout_admin),
							PixApp.getContext().getResources().getString(R.string.are_you_sure), 
							PixApp.getContext().getResources().getString(R.string.logout), null).show();
						} else {
							// admin is not sign-in. try to sign in admin.
							new AdminSignInDialog(getActivity(), new AdminSignInDialogDelegate() {
								@Override
								public void onSignIn() {
									adminBtn.updateView(true);
									if (mActivitiesController != null) {
										mActivitiesController.gotoAdminSettings(getActivity());
									}
								}
							}).show();
						}
						
					}
					@Override
					public void onAdminMenuBtnClicked() {
						if (UserInfo.hasAdminToken()) {
							// admin is signed in.
							if (mActivitiesController != null) {
								mActivitiesController.gotoAdminSettings(getActivity());
							}
						} else {
							// admin is not sign-in. try to sign in admin.
							new AdminSignInDialog(getActivity(), new AdminSignInDialogDelegate() {
								@Override
								public void onSignIn() {
									adminBtn.updateView(true);
									if (mActivitiesController != null) {
										mActivitiesController.gotoAdminSettings(getActivity());
									}
								}
							}).show();
						}
						
					}
				});
        		layout.addView(adminBtn);
        	}
        }
    	return mFragmentView;
    }

    private void onShareToInstaBtnClicked() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_INSTAGRAM_PRESSED, null);
		if (PixAppState.UserInfo.getInstagramToken() != null) {
			new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
				public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_INSTAGRAM, 
							PixDefs.APP_EVENT_UNLINK_INSTAGRAM_PRESSED, null);
					PixAppState.UserInfo.clearInstagramUserInfo();
					if (shareToInsta != null) {
						shareToInsta.updateView(false, "");
					}
				}
				public void onDecline() {}
			},
			PixApp.getContext().getResources().getString(R.string.unlink_your_instagram_account),
			null, 
			PixApp.getContext().getResources().getString(R.string.ok), null).show();
		} else {
			// not linked to instagram account. link it.
			// authenticate with instagram first
			new InstaLoginDialog(getActivity(), new InstaAuthListener() {
				
				@Override
				public void onError(String error) {
					PixProgressIndicator.showMessage(getActivity(), failed, true);
					PixProgressIndicator.hideMessageWithIndication(false, failed);
					UserInfo.clearInstagramUserInfo();
				}
				
				@Override
				public void onComplete() {
					if (shareToInsta != null) {
						String username = PixAppState.UserInfo.getInstagramFullName();
		        		if (!Utils.isValidString(username)) {
		        			username = PixAppState.UserInfo.getInstagramUsername();
		        		}
		        		shareToInsta.updateView(true, username);
					}
				}
			}).show();
		}
    }
    
    private void onShareToGplusBtnClicked() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_GOOGLE_PRESSED, null);
		if (PixAppState.UserInfo.getGPlusUserName() != null) {
			new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
				public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_GPLUS, 
							PixDefs.APP_EVENT_UNLINK_GPLUS_PRESSED, null);
					if (mGPlusActivity != null) {
						mGPlusActivity.logoutGPlus();
					}
					if (shareToGPlus != null) {
						shareToGPlus.updateView(false, "");
					}
				}
				public void onDecline() {}
			},
			PixApp.getContext().getResources().getString(R.string.unlink_your_gplus_account),
			null, 
			PixApp.getContext().getResources().getString(R.string.ok), null).show();
		} else {
			// not linked to Google account. link it.
			// authenticate with Google first
			if (mGPlusActivity != null) {
				mGPlusActivity.linkGPlusAccount(new GPlusConnectDelegate() {
					@Override
					public void onGPlusConnectFinished(GPlusProcessType processType) {
						if (shareToGPlus != null) {
							String username = PixAppState.UserInfo.getGPlusUserName();
							shareToGPlus.updateView(true, username);
						}
					}
					
					@Override
					public void onGPlusConnectFailed(GPlusProcessType processType) {
						PixProgressIndicator.showMessage(getActivity(), failed, true);
						PixProgressIndicator.hideMessageWithIndication(false, failed);
						UserInfo.clearGPlusCredentials();
					}
				});
			}
		}
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
    
    private void sendMailToFriend() {    	
    	Utils.sendMail("", 
    			PixApp.getContext().getResources().getString(R.string.join_me_on_pixplit),
    			Html.fromHtml(new StringBuilder()
        	    .append(PixApp.getContext().getResources().getString(R.string.join_me_on_pixplit_body))
        	    .append("<br>www.pixplit.com/download")
        	    .toString()));
    }
    
    private void sendFeedback() {
    	String body = PixApp.getContext().getResources().getString(R.string.pixplit_feedback);
    	String version = PixAppState.getAppVersionNumber();
    	if (Utils.isValidString(version)) {
    		body = body.concat(String.format(" (Android v. %s)", version));
    	}
    	Utils.sendMail(PixDefs.PIX_FEEDBACK_EMAIL, body, null);
    }
    
    private void onFindFriendsBtnClicked() {
    	if (mFbActivity != null) {
	    	if (mFbActivity.isFbLinked()) {
	    		if (getActivity() != null) {
		    		Bundle args = new Bundle();
		    		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, false);
					if (mActivitiesController != null) {
						mActivitiesController.gotoFollowFriends(getActivity(), args);
					}
	    		}
	    	} else {
				PixProgressIndicator.showMessage(getActivity(), 
						PixApp.getContext().getResources().getString(R.string.authenticating), true);
				mFbActivity.authenticateWithFbRequest(FbProcessType.FB_PROCESS_FIND_FRIENDS, this, null);
			}
    	}
    }
    
	@Override
	public void onShareToCheckboxClicked() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_FACEBOOK_PRESSED, null);
		if (mFbActivity != null) {
			if (mFbActivity.isFbLinked() && getActivity() != null) {
				new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
					public void onConfirm() {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_FACEBOOK, PixDefs.APP_EVENT_UNLINK_FACEBOOK_PRESSED, null);
						if (mFbActivity != null) {
							mFbActivity.logoutFb();
						}
						PixAppState.UserInfo.clearFbCredentials();
						if (shareToFb != null) {
							shareToFb.updateView(false, "");
						}
					}
					public void onDecline() {}
				},
				PixApp.getContext().getResources().getString(R.string.unlink_your_facebook_account),
				null, 
				PixApp.getContext().getResources().getString(R.string.ok), null).show();
			} else {
				PixProgressIndicator.showMessage(getActivity(), 
						PixApp.getContext().getResources().getString(R.string.authenticating), true);
				mFbActivity.authenticateWithFbRequest(FbProcessType.FB_PROCESS_LINK_ACCOUNT, this, null);
			}
		}
		
	}

	@Override
	public void onShareToBtnClicked() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_FACEBOOK_PRESSED, null);
		if (mFbActivity != null) {
			if (mFbActivity.isFbLinked()) {
				if (mActivitiesController != null) {
					mActivitiesController.gotoSettingsFacebook(getActivity());
				}
			} else {
				PixProgressIndicator.showMessage(getActivity(), 
						PixApp.getContext().getResources().getString(R.string.authenticating), true);
				mFbActivity.authenticateWithFbRequest(FbProcessType.FB_PROCESS_LINK_ACCOUNT, this, null);
			}
		}
	}

	@Override
	public void onFbConnectFinished(FbProcessType processType) {
		if (processType == FbProcessType.FB_PROCESS_LINK_ACCOUNT) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS, PixDefs.APP_EVENT_FACEBOOK_LINKED, null);
			if (shareToFb != null) {
				shareToFb.updateView(true, PixAppState.UserInfo.getFbUserName());
			}
			// default facebook settings depends on the permissions we have so far
			if (mFbActivity != null &&
					mFbActivity.isFbOpenForPublish()) {
				PixAppState.UserInfo.setPostLikesToFb(true);
			}
		} else if (processType == FbProcessType.FB_PROCESS_FIND_FRIENDS) {
			if (shareToFb != null) {
				shareToFb.updateView(true, PixAppState.UserInfo.getFbUserName());
			}
			Bundle args = new Bundle();
    		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, false);
			if (mActivitiesController != null) {
				mActivitiesController.gotoFollowFriends(getActivity(), args);
			}
    	}
		PixProgressIndicator.hideMessage();
	}

	@Override
	public void onFbConnectFailed(FbProcessType processType) {
		if (processType == FbProcessType.FB_PROCESS_LINK_ACCOUNT) {
			shareToFb.updateView(false, "");
			PixProgressIndicator.hideMessageWithIndication(false, failed);
			return;
		} 
		if (processType == FbProcessType.FB_PROCESS_FIND_FRIENDS) {
			// as a best effort, go to follow friends view in order to follow suggested users only
			if (getActivity() != null) {
	    		Bundle args = new Bundle();
	    		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, false);
				if (mActivitiesController != null) {
					mActivitiesController.gotoFollowFriends(getActivity(), args);
				}
    		}
		}
		PixProgressIndicator.hideMessage();
	}    
}
