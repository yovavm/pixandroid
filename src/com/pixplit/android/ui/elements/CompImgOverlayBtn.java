package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.volley.PixImagesLoader;

public class CompImgOverlayBtn extends LinearLayout{

	PixImageView userPic;
	TextView userName;
	TextView creationLocation;
	
	final int userPicId = 1;
	
	public CompImgOverlayBtn(Context context, PixParticipant participant) {
		super(context);
		if (participant != null) {	
			setBackgroundResource(R.color.light_alpha_black);
			userPic = new PixImageView(getContext());
			PixImagesLoader.loadImage(participant.getPicture(), userPic, 0, 0, Priority.HIGH, false);
			
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(context.getResources().getDimensionPixelSize(R.dimen.profile_overlay_pic_size), 
					context.getResources().getDimensionPixelSize(R.dimen.profile_overlay_pic_size));
			int margins = context.getResources().getDimensionPixelSize(R.dimen.profile_overlay_pic_margin);
			params.setMargins(margins, margins, margins, margins);
			params.gravity = Gravity.CENTER_VERTICAL;
			userPic.setBackgroundResource(R.drawable.avatar_thumb);
			userPic.setId(userPicId);
			userPic.setScaleType(ScaleType.CENTER_CROP);
			addView(userPic, params);
			
			LinearLayout linlay = new LinearLayout(context);
			linlay.setOrientation(LinearLayout.VERTICAL);
			
			userName = new PixTextView(getContext());
			params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			params.setMargins(margins, margins, margins, 0);
			linlay.addView(userName, params);
			userName.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.pix_text_size_mini));
			userName.setTypeface(null, Typeface.BOLD);
			userName.setMaxLines(1);
			userName.setEllipsize(TextUtils.TruncateAt.END);
			userName.setTextColor(context.getResources().getColor(R.color.white));
			userName.setText(participant.getFullName());
			
			creationLocation = new PixTextView(getContext());
			params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			params.setMargins(margins, 0, margins, margins);
			linlay.addView(creationLocation, params);
			creationLocation.setText(participant.getLocation());
			creationLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.pix_text_size_mini));
			creationLocation.setTextColor(context.getResources().getColor(R.color.grey));
			creationLocation.setMaxLines(1);
			creationLocation.setEllipsize(TextUtils.TruncateAt.END);
			
			
			addView(linlay);
		}
	}
	
	public CompImgOverlayBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

}
