package com.pixplit.android.ui.elements;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;

public class CommentsBtn extends FrameLayout implements OnClickListener, CompCommentsUpdateDelegate{
	CommentsBtnDelegate mDelegate;
	int mCommentsNum;
	TextView mText;
	final static float textSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.feed_btns_text_size);
	final static float textLeftMargin = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.feed_comment_btn_left_margin);
	final static ColorStateList colorSelector = PixApp.getContext().getResources().getColorStateList(R.color.feed_buttons_color_selector);
	
	public interface CommentsBtnDelegate {
		public void onCommentsBtnClick(CompCommentsUpdateDelegate commentsUpdateDelegate);
	}
	
	public CommentsBtn(Context context) {
		super(context);
	}
	public CommentsBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void init(CommentsBtnDelegate delegate) {
		mDelegate = delegate;
		setOnClickListener(this);
		mText = new PixTextView(getContext());
		mText.setTextColor(colorSelector);
		mText.setBackgroundResource(R.color.transparent);
		mText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		mText.setMaxLines(1);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		params.setMargins((int)textLeftMargin, 0, 0, 0);
		addView(mText, params);
		setPadding(0, 0, 0, 0);
		mCommentsNum = 0;
	}
	
	public void bindData(int totalComments) {
		mCommentsNum = totalComments;
		this.updateCommentsText();
	}
		
	@Override
    public void onClick(View v) {
        if (mDelegate != null) {
        	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_COMMENTS_PRESSED, null);
        	mDelegate.onCommentsBtnClick(this);
        }
    }
	@Override
	public void onTotalCommentsUpdate(int totalComments) {
		mCommentsNum = totalComments;
		this.updateCommentsText();
	}
	
	private void updateCommentsText() {
		if (mCommentsNum > 0) {
			mText.setText(String.format("%s", mCommentsNum));
		} else {
			mText.setText("");
		}
	}
}