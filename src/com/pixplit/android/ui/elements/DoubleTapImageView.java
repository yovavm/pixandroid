package com.pixplit.android.ui.elements;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.pixplit.android.R;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class DoubleTapImageView extends PixImageView {

	GestureDetector gestureDetector;
	DoubleTapImageViewDelegate delegate;
	
	public interface DoubleTapImageViewDelegate {
		public void onDoubleTap();
		public void onSingleTap();
	}
	
	public static abstract class DoubleTapImageViewListener implements DoubleTapImageViewDelegate {
		private PixViewHolder mViewHolder;
		
		public DoubleTapImageViewListener(PixViewHolder holder) {
            mViewHolder = holder;
        }
		
		@Override
		public void onDoubleTap() {
			onDoubleTap(mViewHolder);
		}

		@Override
		public void onSingleTap() {
			onClick(mViewHolder);
		}
		
		public abstract void onClick(PixViewHolder viewHolder);
		public abstract void onDoubleTap(PixViewHolder viewHolder);
	}
	
	public DoubleTapImageView(Context context) {
		super(context);
		
		// creating new gesture detector
	    gestureDetector = new GestureDetector(context, new PixGestureListener());
	}

	public DoubleTapImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		// creating new gesture detector
	    gestureDetector = new GestureDetector(context, new PixGestureListener());
	}

	public DoubleTapImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		// creating new gesture detector
	    gestureDetector = new GestureDetector(context, new PixGestureListener());
	}

	public void setOnDoubleTapListener(DoubleTapImageViewDelegate delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		if (mHighlight) {
			switch(e.getAction()) {
//	        case MotionEvent.ACTION_DOWN:
//	        	setColorFilter(getContext().getResources().getColor(R.color.highlight));
//	            break;
	        case MotionEvent.ACTION_UP:
	        case MotionEvent.ACTION_CANCEL:
	        	clearColorFilter();
	            break;
	        default:
	        	break;
			}
		}
	    return gestureDetector.onTouchEvent(e);
	}
	
	private class PixGestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
        public boolean onDown(MotionEvent e) {
            return true; 
        }
		
		@Override
		public void onShowPress (MotionEvent e) {
			if (mHighlight) {
				setColorFilter(getContext().getResources().getColor(R.color.highlight));
			}
		}
		
		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			if (mHighlight) {
				setColorFilter(getContext().getResources().getColor(R.color.highlight));
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						clearColorFilter();
					}
				}, 250);
			}
			
			if (delegate != null) {
	    		delegate.onSingleTap();
	    	}
	    	return super.onSingleTapConfirmed(e);
		}
		
		@Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            return super.onDoubleTapEvent(e);
        }
		
	    // event when double tap occurs
	    @Override
	    public boolean onDoubleTap(MotionEvent e) {
	    	if (delegate != null) {
	    		delegate.onDoubleTap();
	    	}
	    	return super.onDoubleTap(e);
	    }
	}
}
