package com.pixplit.android.ui.elements;

import java.util.List;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixParticipant;

public class CompOverlay {

	private final static int[] overlayFrameIds = {1,2,3,4};
	private CompOverlayFrame[] overlayFrames;
	private PixCompositionFramesetType frameType;
	private PixComposition comp;
	private RelativeLayout parent;
	PixBaseListDelegate mDelegate;
	int splitWidth;
	int splitHeight;
	
	
	public CompOverlay(RelativeLayout parent, PixBaseListDelegate delegate) {
		this.parent = parent;
		this.mDelegate = delegate;
		overlayFrames = new CompOverlayFrame[PixDefs.splitMaxFramesNum];
		splitWidth = PixAppState.getState().getSplitPreivewWidth();
		splitHeight= PixAppState.getState().getSplitPreviewHeight();
		
	}
	
	public void bindComposition(PixComposition comp) {
		this.comp = comp;
		
		if (comp != null) {
			this.frameType = PixCompositionFramesetType.getType(comp.getCompType());
		} 
	}
	
	public void displayOverlay() {
		if (!hasOverlay()) {
			createOverlay();
		}
		for (int i = 0; i<PixDefs.splitMaxFramesNum; i++) {
			if (overlayFrames[i] != null) {
				overlayFrames[i].show();
			}
		}
	}
	
	public void hideOverlay() {
		for (int i = 0; i<PixDefs.splitMaxFramesNum; i++) {
			if (overlayFrames[i] != null) {
				overlayFrames[i].hide();
			}
		}
	}
	
	public void removeOverlay() {
		if (parent != null && hasOverlay()) {
			for (int i=0; i< PixDefs.splitMaxFramesNum; i++) {
				if (overlayFrames[i] != null) {
					parent.removeView(overlayFrames[i]);
					overlayFrames[i] = null;
				}
			}
		}
	}

	private void createOverlay() {
		if(frameType != null) {
			switch (frameType) {
			case Pix1Frameset:
				createPix1FramesetOverlay();
				break;
			case Pix2VerticalFrameset:
				createPix2VerticalFramesetOverlay();
				break;
			case Pix2HorizontalFrameset:
				createPix2HorizontalFramesetOverlay();
				break;
			case Pix2QuarterBottomFrameset:
				craetePix2QuarterBottomFramesetOverlay();
				break;
			case Pix2QuarterLeftFrameset:
				createPix2QuarterLeftFramesetOverlay();
				break;
			case Pix2QuarterRightFrameset:
				createPix2QuarterRightFramesetOverlay();
				break;
			case Pix2QuarterTopFrameset:
				createPix2QuarterTopFramesetOverlay();
				break;
			case Pix2ThirdBottomFrameset:
				createPix2ThirdBottomFramesetOverlay();
				break;
			case Pix2ThirdLeftFrameset:
				createPix2ThirdLeftFramesetOverlay();
				break;
			case Pix2ThirdRightFrameset:
				createPix2ThirdRightFramesetOverlay();
				break;
			case Pix2ThirdTopFrameset:
				createPix2ThirdTopFramesetOverlay();
				break;
			case Pix3HalfBottomFrameset:
				createPix3HalfBottomFramesetOverlay();
				break;
			case Pix3HalfLeftFrameset:
				createPix3HalfLeftFramesetOverlay();
				break;
			case Pix3HalfRightFrameset:
				createPix3HalfRightFramesetOverlay();
				break;
			case Pix3HalfTopFrameset:
				createPix3HalfTopFramesetOverlay();
				break;
			case Pix3HorizontalFrameset:
				createPix3HorizontalFramesetOverlay();
				break;
			case Pix3QuarterBottomFrameset:
				craetePix3QuarterBottomFramesetOverlay();
				break;
			case Pix3QuarterHorizontalFrameset:
				createPix3QuarterHorizontalFramesetOverlay();
				break;
			case Pix3QuarterLeftFrameset:
				createPix3QuarterLeftFramesetOverlay();
				break;
			case Pix3QuarterRightFrameset:
				createPix3QuarterRightFramesetOverlay();
				break;
			case Pix3QuarterTopFrameset:
				createPix3QuarterTopFramesetOverlay();
				break;
			case Pix3QuarterVerticalFrameset:
				createPix3QuarterVerticalFramesetOverlay();
				break;
			case Pix3VerticalFrameset:
				createPix3VerticalFramesetOverlay();
				break;
			case Pix4Frameset:
				createPix4FramesetOverlay();
				break;
			case Pix4HalfBottomFrameset:
				createPix4HalfBottomFramesetOverlay();
				break;
			case Pix4HalfLeftFrameset:
				createPix4HalfLeftFramesetOverlay();
				break;
			case Pix4HalfRightFrameset:
				createPix4HalfRightFramesetOverlay();
				break;
			case Pix4HalfTopFrameset:
				createPix4HalfTopFramesetOverlay();
				break;
			case Pix4HorizontalFrameset:
				createPix4HorizontalFramesetFrame();
				break;
			case Pix4SixthHorizontalLeft:
				createPix4SixthHorizontalLeftOverlay();
				break;
			case Pix4SixthHorizontalRight:
				createPix4SixthHorizontalRightOverlay();
				break;
			case Pix4SixthVerticalLeft:
				createPix4SixthVerticalLeftOverlay();
				break;
			case Pix4SixthVerticalRight:
				createPix4SixthVerticalRightOverlay();
				break;
			case Pix4ThirdBottomFrameset:
				createPix4ThirdBottomFramesetOverlay();
				break;
			case Pix4ThirdLeftFrameset:
				createPix4ThirdLeftFramesetOverlay();
				break;
			case Pix4ThirdRightFrameset:
				createPix4ThirdRightFramesetOverlay();
				break;
			case Pix4ThirdTopFrameset:
				createPix4ThirdTopFramesetOverlay();
				break;
			case Pix4TwoSixthBottomFrameset:
				createPix4TwoSixthBottomFramesetFrame();
				break;
			case Pix4TwoSixthLeftFrameset:
				createPix4TwoSixthLeftFramesetOverlay();
				break;
			case Pix4TwoSixthRightFrameset:
				createPix4TwoSixthRightFramesetOverlay();
				break;
			case Pix4TwoSixthTopFrameset:
				craetePix4TwoSixthTopFramesetOverlay();
				break;
			case Pix4VerticalFrameset:
				createPix4VerticalFramesetOverlay();
				break;
			default:
				break;
			
			}
			addOverlayBtns();
		}
    }
	
	private boolean hasOverlay() {
		boolean hasOverlay = overlayFrames[0]!=null;
		return hasOverlay;
	}
	
	public boolean isOverlayVisible() {
		boolean visible = false;
		if (parent != null) {
			for (int i=0; i<PixDefs.splitMaxFramesNum; i++) {
				CompOverlayFrame frame = (CompOverlayFrame)parent.findViewById(overlayFrameIds[i]);
				visible = ((frame != null) && frame.getVisibility() == View.VISIBLE);
				if (visible) {
					break;
				}
			}
		}
		return visible;
	}
	
	private CompOverlayFrame createFrame() {
		CompOverlayFrame frame = new CompOverlayFrame(PixApp.getContext());
		return frame;
	}
	
	private void addCenterBtn(FrameLayout frame, int index, final PixParticipant participant) {
		CompImgOverlayBtn centerBtn = new CompImgOverlayBtn(PixApp.getContext(), participant);
		centerBtn.setId(index);
		
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		centerBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_PARTICIPANT_BUBBLE_PRESSED, null);
				mDelegate.onProfileClick(participant.getUsername());
			}
		});
		
		frame.addView(centerBtn, params);
	}
	
	private void addOverlayBtns() {
		int participantCount = 0;
		if (this.comp != null) {
			participantCount = this.comp.getParticipantsCount();
			List<PixParticipant> participants = comp.getParticipants();
			if (participants != null && participants.size() == participantCount) {
				for (int i=0; i<participants.size(); i++) {
					if (participants.get(i) != null) {
						int participantIndex = participants.get(i).getIndex() - 1;
						if (participantIndex >=0 && 
								participantIndex < PixDefs.splitMaxFramesNum && 
								overlayFrames[participantIndex] != null) {
							addCenterBtn(overlayFrames[participantIndex], participantIndex, comp.getParticipants().get(i));
						}
					}
				}
			}
		}
	}
	
	private void createPix1FramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight);
		parent.addView(overlayFrames[0], relativeParams);
	}
	
	private void createPix2VerticalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2HorizontalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void craetePix2QuarterBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight*3/4);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2QuarterLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2QuarterRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2QuarterTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight*3/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2ThirdBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight*2/3);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2ThirdLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2ThirdRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix2ThirdTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.comp_img);
		relativeParams.addRule(RelativeLayout.ALIGN_TOP, R.id.comp_img);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight*2/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);
	
	}
	
	private void createPix3HorizontalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[1]);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void craetePix3QuarterBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[1]);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3QuarterHorizontalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[1]);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3QuarterLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3QuarterRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3QuarterTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[1]);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3QuarterVerticalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3VerticalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3HalfBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3HalfLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relativeParams.addRule(RelativeLayout.CENTER_VERTICAL);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3HalfRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.CENTER_VERTICAL);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix3HalfTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;
		
		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);
	}
	
	private void createPix4VerticalFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_LEFT, overlayFrameIds[0]);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4HorizontalFramesetFrame() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4FramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4HalfBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4HalfLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4HalfRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4HalfTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/2);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4SixthHorizontalLeftOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4SixthHorizontalRightOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4SixthVerticalLeftOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4SixthVerticalRightOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4ThirdBottomFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight*2/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4ThirdLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/2);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4ThirdRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*3/4, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4ThirdTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight*2/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4TwoSixthBottomFramesetFrame() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4TwoSixthLeftFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*2/3, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/4);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void createPix4TwoSixthRightFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth*2/3, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[0]);
		relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
	
	private void craetePix4TwoSixthTopFramesetOverlay() {
		RelativeLayout.LayoutParams relativeParams;

		overlayFrames[0] = createFrame();
		overlayFrames[0].setId(overlayFrameIds[0]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		parent.addView(overlayFrames[0], relativeParams);
	
		overlayFrames[1] = createFrame();
		overlayFrames[1].setId(overlayFrameIds[1]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth/2, splitHeight/3);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		parent.addView(overlayFrames[1], relativeParams);

		overlayFrames[2] = createFrame();
		overlayFrames[2].setId(overlayFrameIds[2]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[1]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[2], relativeParams);

		overlayFrames[3] = createFrame();
		overlayFrames[3].setId(overlayFrameIds[3]);
		relativeParams = new RelativeLayout.LayoutParams(splitWidth, splitHeight/3);
		relativeParams.addRule(RelativeLayout.BELOW, overlayFrameIds[2]);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		parent.addView(overlayFrames[3], relativeParams);
	}
}
