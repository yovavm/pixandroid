package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.util.Utils;

public class ProfileFollowBtn extends LinearLayout implements OnClickListener{
	int mFollowStatus;
	int mFollowingImgRes;
	int mFollowImgRes;
	ProfileFollowBtnDelegae mDelegate;
	PixUser mUser;
	TextView followStatus;
	TextView userName;
//	ProgressBar progressBar; TODO...
	
	public interface ProfileFollowBtnDelegae {
		public void onUserFollowClick(PixUser user);
		public boolean verifyUser();
	}
	
	public ProfileFollowBtn(Context context) {
		super(context);
	}
	public ProfileFollowBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void init(ProfileFollowBtnDelegae delegate, int followingImgRes, int followImgRes) {
		mDelegate = delegate;
		mFollowingImgRes = followingImgRes;
		mFollowImgRes = followImgRes;
		setOnClickListener(this);
	
		int padding = getContext().getResources().getDimensionPixelSize(R.dimen.profile_follow_btn_padding);
		int topPadding = getContext().getResources().getDimensionPixelSize(R.dimen.profile_follow_btn_top_padding);
		int nameTopPadding = getContext().getResources().getDimensionPixelSize(R.dimen.profile_follow_btn_name_top_margin);
		this.setPadding(padding, padding, padding, padding);
		this.setOrientation(LinearLayout.VERTICAL);
		this.setMinimumWidth(PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.profile_follow_btn_min_width));
		
		this.followStatus = new PixTextView(getContext());
		this.followStatus.setGravity(Gravity.CENTER);
		this.followStatus.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.transparent));
		this.followStatus.setTypeface(null, Typeface.BOLD);
		this.followStatus.setPadding(0, topPadding, 0, 0);
		this.addView(followStatus);
		
		this.userName = new PixTextView(getContext());
		this.userName.setTextColor(PixApp.getContext().getResources().getColor(R.color.whitish));
		this.userName.setGravity(Gravity.CENTER);
		this.userName.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.transparent));
		this.userName.setMaxLines(1);
		this.userName.setEllipsize(TruncateAt.END);
		this.userName.setMaxWidth(PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.profile_follow_btn_width));
		this.userName.setPadding(0, nameTopPadding, 0, 0);
		this.addView(userName);
		
		/*
		this.progressBar = new ProgressBar(getContext());
		this.progressBar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.pix_progress_bar));
		this.progressBar.setVisibility(View.INVISIBLE);
		this.progressBar.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
		this.progressBar.setIndeterminate(false);
		this.progressBar.set
		*/
	}
	
	public void bindUser(PixUser user) {
		mUser = user;
		
		if (mUser != null) {
			mFollowStatus = mUser.getFollowStatus();
			updateImage();
    	}
	}
		
	public void toggleState() {
		switch(mFollowStatus) {
		case PixUser.FOLLOW_STATUS_FOLLOWING:{
			mFollowStatus = PixUser.FOLLOW_STATUS_NOT_FOLLOWING;
			break;
		}
		case PixUser.FOLLOW_STATUS_NOT_FOLLOWING:{
			mFollowStatus = PixUser.FOLLOW_STATUS_FOLLOWING;
			break;
		}
		default:break;
		}
		updateImage();
	}
	
	private void updateImage() {
		switch(mFollowStatus) {
		case PixUser.FOLLOW_STATUS_FOLLOWING:{
			this.setVisibility(View.VISIBLE);
			this.setBackgroundResource(mFollowingImgRes);
			this.followStatus.setText(PixApp.getContext().getResources().getString(R.string.following_btn));
			this.followStatus.setTextColor(PixApp.getContext().getResources().getColor(R.color.yellow));
			break;
		}
		case PixUser.FOLLOW_STATUS_NOT_FOLLOWING:{
			this.setVisibility(View.VISIBLE);
			this.setBackgroundResource(mFollowImgRes);
			this.followStatus.setText(PixApp.getContext().getResources().getString(R.string.follow_btn));
			this.followStatus.setTextColor(PixApp.getContext().getResources().getColor(R.color.brown));
			break;
		}
		case PixUser.FOLLOW_STATUS_LOGGED_IN_USER:{
			setVisibility(View.GONE);
			break;
		}
		default:break;
		}
		if (userName != null && mUser != null) {
			this.userName.setText(Utils.getFirstWord(mUser.getFullName()));
		}
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate == null || !mDelegate.verifyUser()) return;
		toggleState();
		mDelegate.onUserFollowClick(mUser);
	}
}
