package com.pixplit.android.ui.elements;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixLiker;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class LikesBar {

	FlowLayout mLikesBarLayout;
	LikesBarDelegate melegate;
	TextView mLikesView;
	String mSingleLiker;
	SingleLikerClickListener mSingleLikerListener;
	MultiLikersClickListener mMultiLikersListener;
	
	final static int verticalMargin = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comps_feed_items_vertical_margin);
	final static int textColor = PixApp.getContext().getResources().getColor(R.color.brown);
	final static float textSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comp_likes_text_size);
	
	public interface LikesBarDelegate {
		public void onSingleLikerClick(String liker);
		public void onLikersClick();
	}
	
	public LikesBar(FlowLayout likesBarLayout, LikesBarDelegate delegate) {
		this.mLikesBarLayout = likesBarLayout;
		this.melegate = delegate;
		this.mSingleLikerListener = new SingleLikerClickListener();
		this.mMultiLikersListener = new MultiLikersClickListener();
		
		if (this.mLikesBarLayout != null) {
			this.mLikesView = new TextView(mLikesBarLayout.getContext());
			this.mLikesView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			this.mLikesView.setTextColor(textColor);
			mLikesBarLayout.addView(this.mLikesView);
			
			if (this.mLikesBarLayout.getParent() instanceof LinearLayout) {
	    		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
	    				PixAppState.getState().getSplitPreivewWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
	    		params.gravity = Gravity.CENTER_HORIZONTAL;
	    		params.setMargins(0, verticalMargin, 0, verticalMargin);
	    		this.mLikesBarLayout.setLayoutParams(params);
			}
			
			mLikesBarLayout.setVisibility(View.GONE);
    	}
	}
	
	public void resetLikers(PixComposition comp) {
		if (mLikesBarLayout != null) {
			setLikers(comp);
		}
	}
	
	public void setLikers(final PixComposition comp) {
		if (mLikesBarLayout == null) return;
		boolean showBar = false;
		int likesCount = 0;
		String likerUsername = null;
		
		if (comp != null) {
			likesCount = comp.getLikesCount();
			boolean liked = (comp.getLiked() == 1);
			
			if (likesCount > 0) {
				String likerName = null;
				PixLiker liker = comp.getLiker();
				if (liker != null) {
					likerName = liker.getFullName();
				}
				
				if (likesCount == 1) {
					if (liked) { // the logged in user is the only liker. don't show the bar.
						return;
					}
					if (Utils.isValidString(likerName)) {
						this.setText(String.format(PixApp.getContext().getString(R.string.user_likes_this_split), likerName));
						showBar = true;
						if (liker != null) {
							likerUsername = liker.getUsername();
						}
					}
				} else if (likesCount == 2) {
					if (liked) {
						this.setText(PixApp.getContext().getString(R.string.you_and_1_other_person_like_this_split));
						showBar = true;
					} else {
						if (Utils.isValidString(likerName)) {
							this.setText(String.format(
									PixApp.getContext().getString(R.string.user_and_one_other_person_like_this_split), likerName));
							showBar = true;
						}
					}
				} else {
					// more then 2 likers
					if (liked) {
						this.setText(String.format(
								PixApp.getContext().getString(R.string.you_and_num_others_like_this_split), 
								Integer.valueOf(likesCount-1)));
						showBar = true;
					} else {
						if (Utils.isValidString(likerName)) {
							this.setText(String.format(
									PixApp.getContext().getString(R.string.user_and_num_others_like_this_split),
									likerName,
									Integer.valueOf(likesCount-1)));
							showBar = true;
						} else {
							this.setText(String.format(
									PixApp.getContext().getString(R.string.num_people_like_this_split),
									Integer.valueOf(likesCount)));
							showBar = true;
						}
					}
				}
			}
		}
		
		if (showBar) {
			mLikesBarLayout.setVisibility(View.VISIBLE);
			
			// set on-click behavior
			if (likesCount == 1) {
				if (Utils.isValidString(likerUsername)) {
					mSingleLiker = likerUsername;
					mLikesBarLayout.setOnClickListener(mSingleLikerListener);
				}
			} else {
				mLikesBarLayout.setOnClickListener(mMultiLikersListener);
			}
		} else {
			mLikesBarLayout.setVisibility(View.GONE);
		}
	}
	
	public void clearLikers() {
		if (mLikesBarLayout != null) {
			mSingleLiker = null;
			mLikesBarLayout.setVisibility(View.GONE);
		}
	}
		
	private void setText(String text) {
		this.mLikesView.setText(text);
	}
	
	private class SingleLikerClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_LIKERS_LIST_PRESSED, null);
			if (melegate != null) {
				melegate.onSingleLikerClick(mSingleLiker);
			}
		}
	}
	
	private class MultiLikersClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_LIKERS_LIST_PRESSED, null);
			if (melegate != null) {
				melegate.onLikersClick();
			}
		}
	}
}
