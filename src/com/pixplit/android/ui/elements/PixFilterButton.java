package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class PixFilterButton extends ImageView implements View.OnClickListener {
	int mFilterType;
	PixFilterButtonDelegate mDelegate;
	
	public interface PixFilterButtonDelegate {
		public void onFilterClick(PixFilterButton filter, int type);
	}
	
	public PixFilterButton(Context context) {
		super(context);
		setOnClickListener(this);
	}
	
	public PixFilterButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnClickListener(this);
	}

	public void setType(int filterType) {
		mFilterType = filterType;
	}
	
	public void setDelegate(PixFilterButtonDelegate delegate) {
		mDelegate = delegate;
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onFilterClick(this, mFilterType);
		}
	}
}

