package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class UserFollowBtn extends PixTextView implements OnClickListener{
	int mFollowStatus;
	int mFollowingImgRes;
	int mFollowImgRes;
	PixBaseListDelegate mListDelegate;
	PixUser mUser;
	boolean setName;
	private static final String followingText = PixApp.getContext().getString(R.string.following_btn);
	private static final String followText = PixApp.getContext().getString(R.string.follow_btn);
	private static final int followingColor = PixApp.getContext().getResources().getColor(R.color.whitish);
	private static final int followColor = PixApp.getContext().getResources().getColor(R.color.brown);
	private static final int minWidth = (int) (PixAppState.getState().getFrameWidth()/4.5f);
	
	public UserFollowBtn(Context context) {
		super(context);
	}
	public UserFollowBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void init(PixBaseListDelegate listDelegate, int followingImgRes, int followImgRes) {
		mListDelegate = listDelegate;
		mFollowingImgRes = followingImgRes;
		mFollowImgRes = followImgRes;
		setOnClickListener(this);
		setTypeface(null, Typeface.BOLD);
		setGravity(Gravity.CENTER);
		setMinimumWidth(minWidth);
	}
	
	public void bindUser(PixUser user, boolean setName) {
		mUser = user;
		this.setName = setName;
		
		if (mUser != null) {
			mFollowStatus = mUser.getFollowStatus();
			updateImage();
    	}
	}
		
	public void toggleState() {
		switch(mFollowStatus) {
		case PixUser.FOLLOW_STATUS_FOLLOWING:{
			mFollowStatus = PixUser.FOLLOW_STATUS_NOT_FOLLOWING;
			break;
		}
		case PixUser.FOLLOW_STATUS_NOT_FOLLOWING:{
			mFollowStatus = PixUser.FOLLOW_STATUS_FOLLOWING;
			break;
		}
		default:break;
		}
		updateImage();
	}
	
	private void updateImage() {
		switch(mFollowStatus) {
		case PixUser.FOLLOW_STATUS_FOLLOWING:{
			this.setVisibility(View.VISIBLE);
			this.setBackgroundResource(mFollowingImgRes);
			this.setText(followingText);
			this.setTextColor(followingColor);
			break;
		}
		case PixUser.FOLLOW_STATUS_NOT_FOLLOWING:{
			this.setVisibility(View.VISIBLE);
			this.setBackgroundResource(mFollowImgRes);
			this.setText(followText);
			this.setTextColor(followColor);
			break;
		}
		case PixUser.FOLLOW_STATUS_LOGGED_IN_USER:{
			setVisibility(View.GONE);
			break;
		}
		default:break;
		}
		if (setName && mUser != null) {
			setText(Utils.getFirstWord(mUser.getFullName()));
		}
	}
	
	@Override
	public void onClick(View v) {
		if (mListDelegate == null || !mListDelegate.verifyUser()) return;
		if (mFollowStatus == PixUser.FOLLOW_STATUS_NOT_FOLLOWING) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_USERS_LIST, PixDefs.APP_EVENT_FOLLOW_BUTTON_PRESSED, null);
		} else {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_USERS_LIST, PixDefs.APP_EVENT_FOLLOWING_BUTTON_PRESSED, null);
		}
    	
		toggleState();
		mListDelegate.onUserFollowClick(mUser);
	}
	
}
