package com.pixplit.android.ui.elements;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.data.PixProfile;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.PixImagesLoader;

public class PixUserPictureDialog extends Dialog {

	public PixUserPictureDialog(Context context, PixProfile profile) {
		super(context, android.R.style.Theme);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_picture_dialog_layout);
		
		// center the dialog vertically as the title takes place although not used
		View topPanel = findViewById(android.R.id.title);
		if (topPanel != null) {
			topPanel.setVisibility(View.GONE);
		}
		
		init(profile);
	}

	private void init(final PixProfile profile) {
		// full screen dialog
		LayoutParams params = getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
		
		PixImageView userPicture = (PixImageView)findViewById(R.id.user_pic);
		if (userPicture != null && Utils.isValidString(profile.getPicture())) {
			PixImagesLoader.loadImage(profile.getPicture(), userPicture, 0, 0, Priority.IMMEDIATE, false);
		}
		FrameLayout userPicDialogLayout = (FrameLayout)findViewById(R.id.user_pic_dialog_layout);
		userPicDialogLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}
