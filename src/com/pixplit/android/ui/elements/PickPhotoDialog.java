package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.PixSplitPhotoPickerInterface;

public class PickPhotoDialog extends PixDialog {
	PixSplitPhotoPickerInterface mPhotoPicker;
	LinearLayout mLayout;
	
	private static final String facebook = PixApp.getContext().getString(R.string.facebook);
	private static final String instagram = PixApp.getContext().getString(R.string.instagram);
	private static final String gallery = PixApp.getContext().getString(R.string.gallery_lower);
//	private static final String google = PixApp.getContext().getString(R.string.google);
	
	public PickPhotoDialog(Context context, PixSplitPhotoPickerInterface photoPicker) {
		super(context, R.layout.pick_photo_dialog);
		mPhotoPicker = photoPicker;
		init();
	}

	private void init() {
		mLayout = (LinearLayout) findViewById(R.id.pick_photo_layout);
		if (mLayout == null) return;
		
		TextView instaBtn = new TextView(getContext());
		instaBtn.setText(instagram);
		instaBtn.setTextColor(PixApp.getContext().getResources().getColor(R.color.black));
		instaBtn.setBackgroundResource(R.drawable.confirmation_dialog_btn_selector);
		instaBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
						PixDefs.APP_EVENT_IMPORT_INSTAGRAM_PHOTO_PRESSED, 
						null);
				if (mPhotoPicker != null) {
					mPhotoPicker.photoPickFromInstaRequest();
				}
				dismiss();
			}
		});
		mLayout.addView(instaBtn);
		
		// Facebook can be accessed even when the app is not installed by a web view
		TextView fbBtn = new TextView(getContext());
		fbBtn.setText(facebook);
		fbBtn.setTextColor(PixApp.getContext().getResources().getColor(R.color.black));
		fbBtn.setBackgroundResource(R.drawable.confirmation_dialog_btn_selector);
		fbBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
						PixDefs.APP_EVENT_IMPORT_FACEBOOK_PHOTO_PRESSED, 
						null);

				if (mPhotoPicker != null) {
					mPhotoPicker.photoPickFromFacebookRequest();
				}
				dismiss();
			}
		});
		mLayout.addView(fbBtn);
		
//		// Google images search
//		TextView googleBtn = new TextView(getContext());
//		googleBtn.setText(google);
//		googleBtn.setTextColor(PixApp.getContext().getResources().getColor(R.color.black));
//		googleBtn.setBackgroundResource(R.drawable.confirmation_dialog_btn_selector);
//		googleBtn.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
//						PixDefs.APP_EVENT_IMPORT_GOOGLE_PHOTO_PRESSED, 
//						null);
//				if (mPhotoPicker != null) {
//					mPhotoPicker.photoPickFromGoogleRequest();
//				}
//				dismiss();
//			}
//		});
//		mLayout.addView(googleBtn);
		
		// the generic way
		TextView moreBtn = new TextView(getContext());
		moreBtn.setText(gallery);
		moreBtn.setTextColor(PixApp.getContext().getResources().getColor(R.color.black));
		moreBtn.setBackgroundResource(R.drawable.confirmation_dialog_btn_selector);
		moreBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mPhotoPicker != null) {
					mPhotoPicker.photoPickRequest();
				}
				dismiss();
			}
		});
		mLayout.addView(moreBtn);		
	}
}