package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class SearchBar extends RelativeLayout{
	SearchDelegate mDelegate;
	EditText searchText;
	
	public interface SearchDelegate {
		public void onSearchRequest(String searchText);
	}
	
	public SearchBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	protected void onFinishInflate () {
		super.onFinishInflate();
		init(getContext());
	}
	
	private void init(Context context) {
		searchText = (EditText)findViewById(R.id.searchText);
		if (searchText != null) {
			// init the search text
			searchText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			searchText.setInputType(EditorInfo.TYPE_CLASS_TEXT);
			searchText.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				    if(actionId == EditorInfo.IME_ACTION_SEARCH) {
						onSearchRequest();
						PixApp.hideKeypad(v);
						return true;
					}
					return false;
				}
			});
		}

		// search button
		TextView searchBtn = (TextView)findViewById(R.id.searchTextSend);
		if (searchBtn != null) {
			searchBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixApp.hideKeypad(v);
				    onSearchRequest();
				}
			});
		}
	}
	
	public void setDelegate(SearchDelegate delegate) {
		this.mDelegate = delegate;
	}
	
	private void onSearchRequest(){
		if (searchText == null || searchText.getText() == null) return;
		String text = searchText.getText().toString();
		if (!Utils.isValidString(text)) return;
		
		if (mDelegate != null) {
			mDelegate.onSearchRequest(text);
		}
	}	
}
