package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class CompLayout extends LinearLayout {

	public CompLayout(Context context) {
		super(context);
	}
	
	public CompLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public CompLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
    public void setPressed(boolean pressed)
    {
        // Do nothing here. Specifically, do not propagate this message along
        // to our children so they do not incorrectly display a pressed state
        // just because one of their ancestors got pressed.
    }

	

}
