package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class ColorsBar extends LinearLayout {
	
	Animation slideInAnimation;
	Animation slideOutAnimation;
	ColorsBarDelegate mDelegate;
	
	public interface ColorsBarDelegate {
		public void onNewColorSelected(int color);
	}
	
	public ColorsBar(Context context) {
		super(context);
		this.init();
	}
	public ColorsBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}

	private void init() {
		this.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.light_alpha_black));
		this.setOrientation(LinearLayout.HORIZONTAL);
		this.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.transparent));
		this.setPadding(10, 20, 10, 20);
		
		addColorToBar(Color.RED);
		addColorToBar(Color.GREEN);
		addColorToBar(Color.BLUE);
		addColorToBar(Color.YELLOW);
		addColorToBar(Color.CYAN);
		addColorToBar(Color.MAGENTA);
		
		this.slideInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.title_slide_in_anim);
		if (this.slideInAnimation != null) {
			this.slideInAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.VISIBLE);
				}
			});
		}
		this.slideOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.title_slide_out_anim);
		if (this.slideOutAnimation != null) {
			this.slideOutAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.INVISIBLE);
				}
			});
		}
	}
	
	private void addColorToBar(final int color) {
		ImageView colorBtn = new ImageView(getContext());
		colorBtn.setImageDrawable(new ColorDrawable(color));
		colorBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDelegate != null) {
					mDelegate.onNewColorSelected(color);
				}
			}
		});
		LayoutParams params = new LayoutParams(0, 60, 1);
		this.addView(colorBtn, params);
	}
	
	public void setDelegate(ColorsBarDelegate delegate) {
		mDelegate = delegate;
	}
	
	public void show() {
		if (this.getVisibility() == View.INVISIBLE &&
				this.slideInAnimation != null) {
			this.startAnimation(slideInAnimation);
		}
	}
	
	public void hide() {
		if (this.getVisibility() == View.VISIBLE &&
				this.slideOutAnimation != null) {
			this.startAnimation(slideOutAnimation);
		}
	}
}
