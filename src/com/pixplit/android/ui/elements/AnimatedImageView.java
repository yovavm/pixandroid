package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class AnimatedImageView extends ImageView {
 
	 public AnimatedImageView(Context context) {
		super(context);
	}
 
	public AnimatedImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
    public AnimatedImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	private void updateAnimationsState() {
        boolean running = getVisibility() == View.VISIBLE && hasWindowFocus();
        updateAnimationState(getDrawable(), running);
        updateAnimationState(getBackground(), running);
    }

    private void updateAnimationState(Drawable drawable, boolean running) {
        if(drawable instanceof AnimationDrawable) {
            AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
            if(running) {
                animationDrawable.start();
            } else {
                animationDrawable.stop();
            }
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        updateAnimationsState();
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        updateAnimationsState();
    }
    
    @Override 
    protected void onDraw(Canvas canvas){
    	super.onDraw(canvas);
    	updateAnimationsState();
    }
}
