package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class PixAlertDialog extends PixDialog {
	ConfirmationDialogDelegate mDelegate;
	
	public interface ConfirmationDialogDelegate {
		public void onConfirm();
	}
	
	public PixAlertDialog(Context context, ConfirmationDialogDelegate delegate, String title, String msgText, String confirmText) {
		super(context, R.layout.alert_dialog);
		mDelegate = delegate;
		init(title, msgText, confirmText);
	}
	
	private void init(String titleText, String msgText, String confirmText) {
		TextView title = (TextView)findViewById(R.id.alertTitle);
		if (title != null && Utils.isValidString(titleText)) {
			title.setText(titleText);
		}
		
		TextView message = (TextView)findViewById(R.id.alertMessage);
		if (message != null && Utils.isValidString(msgText)) {
			message.setText(msgText);
		}
		
		TextView logoutBtn = (TextView)findViewById(R.id.confirmBtn);
		if (logoutBtn != null) {
			logoutBtn.setText(confirmText);
			logoutBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null) {
						mDelegate.onConfirm();
					}
					dismiss();
				}
			});
		}
	}
	
	@Override
	public void show() {
		try {
			super.show();
		} catch (Exception e) {
        	// on some cases the dialog may no longer be attached to a window and therefore fail to show. 
        	e.printStackTrace();
        }
	}

	@Override
	public void dismiss() {
		try {
			super.dismiss();
		} catch (Exception e) {
        	// on some cases the dialog may no longer be attached to a window and therefore fail to dismiss. 
        	e.printStackTrace();
        }
	}
}
