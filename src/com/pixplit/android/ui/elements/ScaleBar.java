package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class ScaleBar extends FrameLayout implements OnSeekBarChangeListener {
	
	SeekBar mSeekBar;
	Animation slideInAnimation;
	Animation slideOutAnimation;
	ScaleBarDelegate mDelegate;
	private static final int SEEK_BAR_MAX_VAL = 100;
	
	public interface ScaleBarDelegate {
		// scale is value is between 0 to 1
		public void onNewScaleValue(float scale);
	}
	
	public ScaleBar(Context context) {
		super(context);
		this.init();
	}
	public ScaleBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}
	public ScaleBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init();
	}

	private void init() {
		this.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.transparent));
		
		mSeekBar = new SeekBar(getContext());
		this.mSeekBar.setBackgroundColor(getContext().getResources().getColor(R.color.light_alpha_black));
		this.mSeekBar.setPadding(30, 15, 30, 15);
		this.mSeekBar.setMax(SEEK_BAR_MAX_VAL);
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		params.setMargins(0, 0, 0, 0);
		this.addView(mSeekBar, params);
		
		mSeekBar.setOnSeekBarChangeListener(this);
		
		this.slideInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_in_from_top_anim);
		if (this.slideInAnimation != null) {
			this.slideInAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.VISIBLE);
				}
			});
		}
		this.slideOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_out_into_top_anim);
		if (this.slideOutAnimation != null) {
			this.slideOutAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.INVISIBLE);
				}
			});
		}
	}
	
	public void setScale(float scale) {
		if (scale < 0 || scale > 1) return;
		if (this.mSeekBar != null) {
			this.mSeekBar.setProgress((int)(scale*SEEK_BAR_MAX_VAL));
		}
	}
	
	public void setDelegate(ScaleBarDelegate delegate) {
		mDelegate = delegate;
	}
	
	public void show() {
		if (this.getVisibility() == View.INVISIBLE &&
				this.slideInAnimation != null) {
			this.startAnimation(slideInAnimation);
		}
	}
	
	public void hide() {
		if (this.getVisibility() == View.VISIBLE &&
				this.slideOutAnimation != null) {
			this.startAnimation(slideOutAnimation);
		}
	}
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (mDelegate != null) {
			mDelegate.onNewScaleValue(progress/(float)SEEK_BAR_MAX_VAL);
		}
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {}
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {}
}
