package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.R;

public class FollowUsDialog extends PixDialog {

	public interface FollowUsDelegate {
		public void onFollowUsClick();
	}
	
	public FollowUsDialog(Context context, final FollowUsDelegate delegate, int followBtnRes, String followBtnText) {
		super(context, R.layout.follow_us_dialog);
		
		TextView followUsBtn = (TextView)findViewById(R.id.followUsBtn);
		if (followUsBtn != null) {
			followUsBtn.setBackgroundResource(followBtnRes);
			followUsBtn.setText(followBtnText);
			followUsBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (delegate != null) {
						delegate.onFollowUsClick();
					}
					dismiss();
				}
			});
		}
	}

}
