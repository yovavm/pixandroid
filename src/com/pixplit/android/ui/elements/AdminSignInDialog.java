package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.AdminAuthInfo;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminSignInHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.util.Utils;

public class AdminSignInDialog extends PixDialog {
	AdminSignInDialogDelegate mDelegate;
	PixEditText passwordText;
	
	public interface AdminSignInDialogDelegate {
		public void onSignIn();
	}
	
	public AdminSignInDialog(Context context, AdminSignInDialogDelegate delegate) {
		super(context, R.layout.admin_sign_in_dialog);
		mDelegate = delegate;
		this.init();
	}
	
	private void init() {
		passwordText = (PixEditText)findViewById(R.id.adminPassword);
		
		TextView signinBtn = (TextView)findViewById(R.id.adminLoginBtn);
		if (signinBtn != null) {
			signinBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signInAdmin();
					dismiss();
				}
			});
		}
	}
	
	private void signInAdmin() {
		if (passwordText == null) return;
		String password = passwordText.getText().toString();
		if (!Utils.isValidString(password)) return;
		
    	PixProgressIndicator.showMessage(getContext(), 
				PixApp.getContext().getResources().getString(R.string.signing_up), true);
    	
		new AdminSignInHttpRequest(new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, 
						PixApp.getContext().getResources().getString(R.string.failed));
			}
			
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, 
						PixApp.getContext().getResources().getString(R.string.authenticated));
				if (response instanceof AdminAuthInfo) {
					UserInfo.setAdminToken(((AdminAuthInfo)response).getAdminToken());
				}
				if (mDelegate != null) {
					mDelegate.onSignIn();
				}
			}
		}, password).send();
	}
}
