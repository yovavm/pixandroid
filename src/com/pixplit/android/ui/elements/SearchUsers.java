package com.pixplit.android.ui.elements;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.adapters.PixFilterableUsersListAdapter;

public class SearchUsers extends RelativeLayout{
	PixFilterableUsersListAdapter adapter;
	EditText searchText;
	
	
	public SearchUsers(Context context) {
		super(context);
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
				getContext().getResources().getDimensionPixelOffset(R.dimen.user_search_bar_height));
		searchText = new EditText(context);
		searchText.setHint(R.string.enter_a_name);
		searchText.setBackgroundColor(getContext().getResources().getColor(R.color.whitish));
		searchText.setTextAppearance(context, R.style.DefaultTextStyle);
		searchText.setCursorVisible(true);
		addView(searchText, params);
		
		
		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getContext().getResources().getDimensionPixelOffset(R.dimen.pix_border_width));
		params.addRule(ALIGN_PARENT_BOTTOM);
		TextView bottomBorder = new PixTextView(context);
		bottomBorder.setBackgroundColor(getContext().getResources().getColor(R.color.black));
		bottomBorder.requestFocus();
		addView(bottomBorder, params);
		
		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
				getContext().getResources().getDimensionPixelOffset(R.dimen.user_search_bar_height)+getContext().getResources().getDimensionPixelOffset(R.dimen.pix_border_width));
		setLayoutParams(params);
	}
	
	public void assignAdapter(final PixFilterableUsersListAdapter adapter) {
		this.adapter = adapter;
		
		searchText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// do nothing
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				//do nothing
			}
			@Override
			public void afterTextChanged(Editable s) {
				adapter.getFilter().filter(s.toString());
			}
		});
	}
	
	public SearchUsers(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	

}
