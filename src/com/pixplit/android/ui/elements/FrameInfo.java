package com.pixplit.android.ui.elements;

import android.util.SparseArray;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixCompositionFramesetType;

public class FrameInfo {

	private static final SparseArray<FrameBtnInfo> map = new SparseArray<FrameBtnInfo>();
	
	static {
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix1Frameset), 
				new FrameBtnInfo(R.drawable.frame_1full_btn_selector, PixCompositionFramesetType.Pix1Frameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix2HorizontalFrameset), 
				new FrameBtnInfo(R.drawable.frame_2horizontal_btn_selector, PixCompositionFramesetType.Pix2HorizontalFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix2VerticalFrameset), 
				new FrameBtnInfo(R.drawable.frame_2vertical_btn_selector, PixCompositionFramesetType.Pix2VerticalFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix2ThirdRightFrameset), 
				new FrameBtnInfo(R.drawable.frame_2third_right_btn_selector, PixCompositionFramesetType.Pix2ThirdRightFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix3VerticalFrameset), 
				new FrameBtnInfo(R.drawable.frame_3vertical_btn_selector, PixCompositionFramesetType.Pix3VerticalFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix3HorizontalFrameset), 
				new FrameBtnInfo(R.drawable.frame_3horizontall_btn_selector, PixCompositionFramesetType.Pix3HorizontalFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix3HalfLeftFrameset), 
				new FrameBtnInfo(R.drawable.frame_3half_left_btn_selector, PixCompositionFramesetType.Pix3HalfLeftFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix4VerticalFrameset), 
				new FrameBtnInfo(R.drawable.frame_4vertical_btn_selector, PixCompositionFramesetType.Pix4VerticalFrameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix4Frameset), 
				new FrameBtnInfo(R.drawable.frame_4_btn_selector, PixCompositionFramesetType.Pix4Frameset));
		map.put(PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix4SixthHorizontalLeft), 
				new FrameBtnInfo(R.drawable.frame_4sixth_horizontal_left_btn_selector, PixCompositionFramesetType.Pix4SixthHorizontalLeft));
	}
	
	public static FrameBtnInfo getInfo(int intType) {
		return map.get(intType);
	}
}
