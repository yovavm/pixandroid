package com.pixplit.android.ui.elements;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.PixShareTypeE;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixParticipant;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.instagram.PixSocialNetworkManager;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;
import com.pixplit.android.ui.elements.CommentsBtn.CommentsBtnDelegate;
import com.pixplit.android.ui.elements.LikeBtn.LikeBtnDelegate;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.bitmap.PixBitmapUtils;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;

public class CompActionsBar extends HorizontalScrollView implements LikeBtnDelegate, CommentsBtnDelegate{
	
	LinearLayout mBarView;
	PixComposition mComp;
	CompActionsBarDelegate mDelegate;
	
	LikeBtn mLikeBtn;
	CommentsBtn mCommentsBtn;
	ImageView mShareInstagramBtn;
	ImageView mShareFacebookBtn;
	ImageView mShareTwitterBtn;
	ImageView mResplitBtn;
	ImageView mSaveBtn;
	ImageView mFlagBtn;
	ImageView mDeleteBtn;
	ImageView mTagsBtn;
	ImageView mShareBtn;
	ImageView mJoinBtn;
	
	private static boolean actionBarWasScrolled = false;
	private static final float ACTION_BAR_INIT_VISIBLE_UNITS = 7.55f;
	private static final float ACTION_BAR_VISIBLE_UNITS = 7.3f;
	private static float VISIBLE_UNITS_NUM = ACTION_BAR_INIT_VISIBLE_UNITS;
	
	{
		// run once at start up. in order to check if the action bar tool-tip has been displayed
		if (Utils.getSharedPreferencesBoolean(PixDefs.COMP_ACTION_BAR_SCROLLED, false)) {
			VISIBLE_UNITS_NUM = ACTION_BAR_VISIBLE_UNITS;
			actionBarWasScrolled = true;
		} else {
			VISIBLE_UNITS_NUM = ACTION_BAR_INIT_VISIBLE_UNITS;
		}
	}
	
	public interface CompActionsBarDelegate {
		public void onLikeBtnClick(boolean liked);
		public void onCommentBtnClick(CompCommentsUpdateDelegate commentsUpdateDelegate);
		public void onSaveClick(View v);
		public void onDeleteClick(String compId);
		public void onResplitClick();
		public void onTagsClick();
		public void onJoinClick();
		public void onFlagCompClick(View v);
		public boolean verifyUser();
	}
	
	public CompActionsBar(Context context) {
		super(context);
		init();
	}

	public CompActionsBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public CompActionsBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		setHorizontalScrollBarEnabled(false);
		setPadding(0, 0, 0, 0);
		
		LayoutInflater inflater = 
	              (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.comp_actions_bar_layout, this, true);
		mBarView = (LinearLayout)layout.findViewById(R.id.actionBarLayout);
		
		mLikeBtn = (LikeBtn) mBarView.findViewById(R.id.compLikeBtn);
		if (mLikeBtn != null) {
			mLikeBtn.init(this);
		}
		mCommentsBtn = (CommentsBtn) mBarView.findViewById(R.id.compCommentsBtn);
		if (mCommentsBtn != null) {
			mCommentsBtn.init(this);
		}
		mShareInstagramBtn = (ImageView) mBarView.findViewById(R.id.compShareInstagramBtn);
		if (mShareInstagramBtn != null) {
			if (Utils.isAppInstalled(PixDefs.instagramPackageName)) {
				mShareInstagramBtn.setVisibility(View.VISIBLE);
				mShareInstagramBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onShareToInstagramClick();
				    }
				});
			} else {
				mShareInstagramBtn.setVisibility(View.GONE);
			}
		}
		mShareFacebookBtn = (ImageView) mBarView.findViewById(R.id.compShareFacebookBtn);
		if (mShareFacebookBtn != null) {
			if (Utils.isAppInstalled(PixDefs.facebookPackageName)) {
				mShareFacebookBtn.setVisibility(View.VISIBLE);
				mShareFacebookBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_PARAM_SHARED_WITH_FACEBOOK, null);
						onShareToFacebookClick();		
				    }
				});
			} else {
				mShareFacebookBtn.setVisibility(View.GONE);
			}
		}
		mShareTwitterBtn = (ImageView) mBarView.findViewById(R.id.compShareTwitterBtn);
		if (mShareTwitterBtn != null) {
			if (Utils.isAppInstalled(PixDefs.twitterPackageName)) {
				mShareTwitterBtn.setVisibility(View.VISIBLE);
				mShareTwitterBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_PARAM_SHARED_WITH_TWITTER, null);
						onShareToTwitterClick();
				    }
				});
			} else {
				mShareTwitterBtn.setVisibility(View.GONE);
			}
		}
		mResplitBtn = (ImageView) mBarView.findViewById(R.id.compResplitBtn);
		if (mResplitBtn != null) {
			mResplitBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onResplitClick();
				}
			});
		}
		mSaveBtn = (ImageView) mBarView.findViewById(R.id.compSaveBtn);
		if (mSaveBtn != null) {
			mSaveBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_EVENT_SAVE_TO_LIBRARY_PRESSED, null);
					onSaveImageClick(v);				
			    }
			});
        }
		mFlagBtn = (ImageView) mBarView.findViewById(R.id.compFlagBtn);
		if (mFlagBtn != null) {
			mFlagBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onFlagCompClick(v);				
			    }
			});
        }
		mTagsBtn = (ImageView) mBarView.findViewById(R.id.compTagsBtn);
		if (mTagsBtn != null) {
			mTagsBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onTagsClick();				
			    }
			});
        }
		mShareBtn = (ImageView) mBarView.findViewById(R.id.compShareBtn);
		if (mShareBtn != null) {
			mShareBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onShareClick();				
			    }
			});
        }
		mDeleteBtn = (ImageView) mBarView.findViewById(R.id.compDeleteBtn);
		if (mDeleteBtn != null) {
			mDeleteBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onDeleteCompClick(v);
			    }
			});
		}
		mJoinBtn = (ImageView) mBarView.findViewById(R.id.compJoinBtn);
		if (mJoinBtn != null) {
			mJoinBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onJoinClick();
				}
			});
		}
	}
	
	public void setDelegate(CompActionsBarDelegate delegate) {
		mDelegate = delegate;
	}
	
	public void bindComposition(PixComposition comp) {
		mComp = comp;
		if (getScrollX() > 0) {
			fullScroll(View.FOCUS_LEFT);
		}
		if (mComp == null) return;
		
		if (mLikeBtn != null) {
			if (mComp.getIsCompleted() == PixComposition.COMP_IS_COMPLETED) {
				mLikeBtn.setVisibility(View.VISIBLE);
				mLikeBtn.bindData(mComp.getLiked() == 1);
			} else {
				mLikeBtn.setVisibility(View.GONE);
			}
		}
		
		if (mCommentsBtn != null) {
			if (mComp.getIsCompleted() == PixComposition.COMP_IS_COMPLETED) {
				mCommentsBtn.setVisibility(View.VISIBLE);
				mCommentsBtn.bindData(mComp.getTotalComments());
			} else {
				mCommentsBtn.setVisibility(View.GONE);
			}
		}
		
        if (mDeleteBtn != null) {
			if (allowCompDeletion()) {
				mDeleteBtn.setVisibility(View.VISIBLE);
			} else {
				mDeleteBtn.setVisibility(View.GONE);
			}
        }
        
        if (mResplitBtn != null) {
			if (PixComposition.isResplitAllow(mComp)) {
				mResplitBtn.setVisibility(View.VISIBLE);
			} else {
				mResplitBtn.setVisibility(View.GONE);
			}
		}
		
        if (mJoinBtn != null) {
        	if (PixComposition.isJoinSplitAllow(mComp)) {
        		mJoinBtn.setVisibility(View.VISIBLE);
        	} else {
        		mJoinBtn.setVisibility(View.GONE);
        	}
        }
        
        setDisplayDimens();
	}
	
	@Override
	protected void onScrollChanged(int x, int y, int oldx, int oldy) {
		super.onScrollChanged(x, y, oldx, oldy);
		// in case the scroll bar has been scrolled, can hide the scroll tip of showing 
		// the last visible item chopped in the middle.
		VISIBLE_UNITS_NUM = ACTION_BAR_VISIBLE_UNITS;
		
		if (!actionBarWasScrolled) {
			Utils.setSharedPreferencesBoolean(PixDefs.COMP_ACTION_BAR_SCROLLED, true);
			actionBarWasScrolled = true;
		}
	}
	
	private void setDisplayDimens() {
		View layout = getChildAt(0);
		if (layout != null && layout instanceof LinearLayout) {
			float unitWidth = PixAppState.getState().getFrameWidth() / VISIBLE_UNITS_NUM ;
			float totalVisibleItemsWeight = 0;
			LinearLayout mLayout = ((LinearLayout)layout);
			for(int i = 0; i < mLayout.getChildCount(); i++) {
		        View barItem = mLayout.getChildAt(i);
		        if (barItem != null &&
		        		barItem.getVisibility() == View.VISIBLE) {
		        	ViewGroup.LayoutParams params =  barItem.getLayoutParams();
		        	float itemWeight = 1;
		        	if (params != null && params instanceof android.widget.LinearLayout.LayoutParams) {
		        		itemWeight = ((android.widget.LinearLayout.LayoutParams)params).weight;
		        		totalVisibleItemsWeight += itemWeight;
		        	}
		        	barItem.getLayoutParams().width = (int) (unitWidth * itemWeight);
		        }
		    }

			mLayout.setWeightSum(totalVisibleItemsWeight);
			mLayout.setLayoutParams(new LayoutParams((int) (totalVisibleItemsWeight*unitWidth), LayoutParams.WRAP_CONTENT));
		}
	}
	
	public void setLikeState(boolean liked) {
		if (mLikeBtn != null) {
			mLikeBtn.setLike(liked);
		}
	}

	@Override
	public void onCompLikeClick(boolean liked) {
		if (mDelegate != null) {
			mDelegate.onLikeBtnClick(liked);
		}
		
	}

	@Override
	public void onCommentsBtnClick(CompCommentsUpdateDelegate commentsUpdateDelegate) {
		if (mDelegate != null) {
			mDelegate.onCommentBtnClick(commentsUpdateDelegate);
		}
	}
	
	private void onShareToInstagramClick() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_PARAM_EXPORT_TO_INSTAGRAM_PRESSED, null);
		PixSocialNetworkManager.shareToClick(PixShareTypeE.pixShareToInstagram);
		if (mComp != null && Utils.isValidString(mComp.getImage())) {
			// fetch the composition image
			PixImagesLoader.loadImage(mComp.getImage(), 
									new BitmapFetcherDelegate() {
										final String compId = mComp.getCompId();
										@Override
										public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapDrawable) {
											if (bitmapDrawable != null) {
												Bitmap bitmap = bitmapDrawable.getBitmap();
												if (bitmap != null && Utils.isValidString(compId)) {
													Intent sendIntent = new Intent();
													sendIntent.setAction(Intent.ACTION_SEND);
													sendIntent.setType("image/*");
													sendIntent.putExtra("InstagramCaption", "#pixplit");
													sendIntent.putExtra(Intent.EXTRA_TITLE, "#pixplit");
													sendIntent.putExtra(Intent.EXTRA_SUBJECT, "#pixplit");
													sendIntent.putExtra(Intent.EXTRA_TEXT, "#pixplit");
													sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
													
													// add export overlay
													Bitmap exportBitmap = PixBitmapUtils.addExportOverlay(bitmap);
													
													String path = Utils.storeBitmapInFile(exportBitmap, "pix_share_to_instagram_"+compId+".jpg");
													if (Utils.isValidString(path)) {
														Uri uri = Uri.fromFile(new File(path));
														if (uri != null) {
															sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
															sendIntent.setPackage(PixDefs.instagramPackageName);
															try {
																PixApp.getContext().startActivity(sendIntent);
															} catch (Exception e) {
																e.printStackTrace();
															}
														}
													}
													if (exportBitmap != null &&
															PixDefs.recycleBitmaps &&
															!exportBitmap.isRecycled()) {
														exportBitmap.recycle();
													}
												}
												bitmapDrawable.releaseBitmap("shareCompToInstagram");
											}
										}
										@Override
										public void onBitmapFetchFailed() {
											// Failed to fetch the split image. Do nothing - can't join.
										}
									}, Priority.HIGH);
		}
	}
	
	private void onShareToFacebookClick() {
		if (mComp != null && Utils.isValidString(mComp.getCompId())) {
			PixSocialNetworkManager.shareToClick(PixShareTypeE.pixShareToFacebook);
			String compId = mComp.getCompId();
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			
			String subject = String.format(PixApp.getContext().getResources().getString(R.string.share_main_subject_prefix),
											getCompCreator());
			
			String body = PixApp.getContext().getResources().getString(R.string.share_body_prefix) + 
				Html.fromHtml(new StringBuilder()
			    .append(String.format(PixApp.getContext().getResources().getString(R.string.share_body_postfix), compId))
			    .toString());
		    		
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			sendIntent.putExtra(Intent.EXTRA_TEXT, body);
			sendIntent.setType("text/plain");
			sendIntent.setPackage(PixDefs.facebookPackageName);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try {
				PixApp.getContext().startActivity(sendIntent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getCompCreator() {
		if (mComp == null) return null;
		
		if (mComp.getParticipants() != null) {
	    	int participantCount = mComp.getParticipants().size();
	    	if (participantCount > 0) {
	    		PixParticipant participant = mComp.getParticipants().get(participantCount-1);
				if (participant != null) {
		    		return (participant.getFullName());
		    	}
	    	}
		}
		return "";
	}
	
	private void onShareToTwitterClick() {
		if (mComp != null && Utils.isValidString(mComp.getCompId())) {
			PixSocialNetworkManager.shareToClick(PixShareTypeE.pixShareToTwitter);
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			
			String title = mComp.getTitle();
			String text = ((Utils.isValidString(title) ? (title + " ")  : "") + 
					"(@pixplit)\n" +
					PixDefs.pixSplitLandingPageUrlPrefix+mComp.getCompId());
			
			sendIntent.putExtra(Intent.EXTRA_TEXT, text);
			sendIntent.setType("text/plain");
			sendIntent.setPackage(PixDefs.twitterPackageName);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try {
				PixApp.getContext().startActivity(sendIntent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void onSaveImageClick(View v) {
		if (mDelegate != null) {
			mDelegate.onSaveClick(v);
		}
	}
	
	private void onFlagCompClick(View v) {
		if (mDelegate != null) {
			mDelegate.onFlagCompClick(v);
		}
	}
	
	private void onTagsClick() {
		if (mDelegate != null) {
			mDelegate.onTagsClick();
		}
	}
	
	private void onJoinClick() {
		if (mDelegate != null) {
			mDelegate.onJoinClick();
		}
	}
	
	private void onShareClick() {
		if (mComp != null && Utils.isValidString(mComp.getCompId())) {
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			
			String subject = String.format(PixApp.getContext().getResources().getString(R.string.share_main_subject_prefix),
										getCompCreator());
			
			String body = PixApp.getContext().getResources().getString(R.string.share_body_prefix) + 
				Html.fromHtml(new StringBuilder()
			    .append(String.format(PixApp.getContext().getResources().getString(R.string.share_body_postfix), mComp.getCompId()))
			    .toString());
		    		
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			sendIntent.putExtra(Intent.EXTRA_TEXT, body);
			sendIntent.setType("text/plain");
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PixApp.getContext().startActivity(Intent.createChooser(sendIntent, 
					PixApp.getContext().getResources().getString(R.string.share)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
		}
	}
	
	private boolean allowCompDeletion() {
		boolean allowDeletion = false;
		if (mComp != null && mComp.getParticipants() != null) {
			
			for (PixParticipant participant : mComp.getParticipants()) {
				if (participant != null && participant.getUsername() != null &&
						participant.getUsername().equalsIgnoreCase(PixAppState.UserInfo.getUserName())) {
					allowDeletion = true;
					break;
				}
			}
		}
		
		if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				allowDeletion = true;
			}
		}
		return allowDeletion;
	}
	
	private void onDeleteCompClick(View v) {
		Context context = v.getContext();
		if (context instanceof Activity) {
			if (((Activity) context).isFinishing()) {
				// activity is not running / is finishing. do nothing.
				return;
			}
		}

		if (mComp != null) {
			new PixConfirmationDialog(context, new ConfirmationDialogDelegate() {
				public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_EVENT_DELETE_SPLIT_PRESSED, null);
					if (mDelegate != null && mComp != null) {
						mDelegate.onDeleteClick(mComp.getCompId());
					}
				}
				public void onDecline() {}
			},
			PixApp.getContext().getResources().getString(R.string.confirm_deletion),
			PixApp.getContext().getResources().getString(R.string.any_split_created_from_this_split_will_also_be_deleted), 
			PixApp.getContext().getResources().getString(R.string.ok), null).show();
		}
	}
	
	private void onResplitClick() {
		if (mDelegate != null) {
			mDelegate.onResplitClick();
		}
	}
	
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (mBarView == null) return;
		for (int i=0; i<mBarView.getChildCount(); i++) {
			View child = mBarView.getChildAt(i);
			if (child != null) {
				child.setEnabled(enabled);
			}
		}
	}
	
	public void addActionToBar(View actionBtn, View.OnClickListener listener, int weight, int index) {
		if (mBarView == null || actionBtn == null) return;
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, weight);
		params.setMargins(0, 0, 1, 0);
		mBarView.addView(actionBtn, index, params);
		actionBtn.setOnClickListener(listener);
	}
	
	@Override
	public boolean verifyUser() {
		if (mDelegate != null) {
			return mDelegate.verifyUser();
		}
		return false;
	}
}
