package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.pixplit.android.PixApp.Fonts;

public class PixTextView extends TextView {
	
	public PixTextView(Context context) {
        super(context);
        init();
    }
	
    public PixTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PixTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setTypeface(Fonts.PixFont);
    }
}
