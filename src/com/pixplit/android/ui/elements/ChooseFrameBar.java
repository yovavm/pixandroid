package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.ui.elements.ChooseFrameClickListener.ChooseFrameDelegate;

public class ChooseFrameBar extends HorizontalScrollView{
	
	LinearLayout mListView;
	ChooseFrameDelegate mDelegate;
	Animation slideInAnimation;
	Animation slideOutAnimation;
	FrameBtnInfo[] mFrameBtns;
	
	public ChooseFrameBar(Context context, ChooseFrameDelegate delegate, FrameBtnInfo[] frameBtns) {
		super(context);
		mFrameBtns = frameBtns;
		
		mListView = new LinearLayout(context);
		mListView.setOrientation(LinearLayout.HORIZONTAL);
		mListView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		addView(mListView, params);
		
		setBackgroundResource(R.drawable.capture_filter_bar);
		setHorizontalScrollBarEnabled(false);
		setPadding(0, 0, 0, 0);
		this.mDelegate = delegate;
		
		init();
	}
	
	public ChooseFrameBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	private void init() {
		if (mListView == null) return;
		
		if (mFrameBtns != null) {
			mListView.setWeightSum(mFrameBtns.length);
			
			for (int i=0; i<mFrameBtns.length; i++) {

				ImageView frameBtn = new ImageView(getContext());
				frameBtn.setAdjustViewBounds(true);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 
						LinearLayout.LayoutParams.MATCH_PARENT, 1);
				
				frameBtn.setImageResource(mFrameBtns[i].getDrawableRes());
    			frameBtn.setOnClickListener(new ChooseFrameClickListener(mFrameBtns[i],
    					new ChooseFrameDelegate() {
							@Override
							public void onFrameChoosen(FrameBtnInfo frameInfo) {
								if (mDelegate != null) {
									mDelegate.onFrameChoosen(frameInfo);
									hide();
								}
							}
						}));
	    		mListView.addView(frameBtn, params);
	    	}
			
		}
		
		this.slideInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_right_to_left);
		if (this.slideInAnimation != null) {
			this.slideInAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.VISIBLE);
				}
			});
		}
		this.slideOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_left_to_right);
		if (this.slideOutAnimation != null) {
			this.slideOutAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.GONE);
				}
			});
		}
		this.setAnimation(slideInAnimation);
	}
	
	public void show() {
		if (this.getVisibility() == View.GONE &&
				this.slideInAnimation != null) {
			this.startAnimation(slideInAnimation);
		}
	}
	
	public void hide() {
		if (this.getVisibility() == View.VISIBLE &&
				this.slideOutAnimation != null) {
			this.startAnimation(slideOutAnimation);
		}
	}
}