package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.pixplit.android.PixApp.Fonts;

public class PixBoldTextView extends TextView {
	
	public PixBoldTextView(Context context) {
        super(context);
        init();
    }
	
    public PixBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PixBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
    	setTypeface(Fonts.PixFontBold);
    }
}
