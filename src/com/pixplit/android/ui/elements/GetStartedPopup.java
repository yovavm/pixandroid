package com.pixplit.android.ui.elements;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.ConnectWithEmailDialog;
import com.pixplit.android.ui.ConnectWithEmailDialog.ConnectWithEmailDialogDelegate;

public class GetStartedPopup implements ConnectWithEmailDialogDelegate{
	GetStartedPopupDelegate mDelegate = null;
	Context mContext;
	
	public interface GetStartedPopupDelegate {
		public void connectWithFacebookPressed();
		public void signUpWithEmailPressed();
		public void signInPressed();
		public void termsPressed();
		public void connectWithGPlusPressed();
	}
	
	public GetStartedPopup(Context context, GetStartedPopupDelegate delegate) {
		mContext = context;
		mDelegate = delegate;
	}
		
	public void show() {
		final PixDialog dialog = new PixDialog(mContext, R.layout.get_started_dialog_layout);
		
		// set the custom dialog components - text, image and button
		setOnClickHandlers(dialog);
		
		dialog.show();
	}
	
	private void setOnClickHandlers(final Dialog dialog) {
		if (dialog != null) {
			TextView connectWithFacebook = (TextView)dialog.findViewById(R.id.loginWithFacebook);
			if (connectWithFacebook != null) {
				connectWithFacebook.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_CONNECT_WITH_FACEBOOK_PRESSED, null);
						if (mDelegate != null) {
							mDelegate.connectWithFacebookPressed();
						}
						dialog.dismiss();
					}
				});
			}
			
			TextView connectWithGoogle = (TextView)dialog.findViewById(R.id.loginWithGoogle);
			if (connectWithGoogle != null) {
				connectWithGoogle.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_CONNECT_WITH_GPLUS_PRESSED, null);
						if (mDelegate != null) {
							mDelegate.connectWithGPlusPressed();
						}
						dialog.dismiss();
					}
				});
			}
			
			TextView connectWithEmail = (TextView)dialog.findViewById(R.id.loginWithEmail);
			if (connectWithEmail != null) {
				connectWithEmail.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_CONNECT_WITH_EMAIL_PRESSED, null);
						// show connect with email options dialog
						connectWithEmailRequest();
						dialog.dismiss();
					}
				});
			}
			
			TextView termsBtn = (TextView)dialog.findViewById(R.id.termsBtn);
			if (termsBtn != null) {
				termsBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_TERMS_OF_SERVICE_PRESSED, null);
						if (mDelegate != null) {
							mDelegate.termsPressed();
						}
						dialog.dismiss();
					}
				});
			}

		}
	}

	@Override
	public void onSignupRequest() {
		if (mDelegate != null) {
			mDelegate.signUpWithEmailPressed();
		}
	}

	@Override
	public void onSigninRequest() {
		if (mDelegate != null) {
			mDelegate.signInPressed();
		}
	}
	
	private void connectWithEmailRequest() {
		new ConnectWithEmailDialog(mContext, this).show();
	}
}
