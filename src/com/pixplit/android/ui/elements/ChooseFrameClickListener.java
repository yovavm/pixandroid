package com.pixplit.android.ui.elements;

import android.view.View;

public class ChooseFrameClickListener implements View.OnClickListener {
	
	FrameBtnInfo mFrameInfo;
	ChooseFrameDelegate mDelegate;
	
	public interface ChooseFrameDelegate {
		public void onFrameChoosen(FrameBtnInfo frameInfo);
	}
	
	public ChooseFrameClickListener(FrameBtnInfo frameInfo, ChooseFrameDelegate delegate) {
		this.mFrameInfo = frameInfo;
		this.mDelegate = delegate;
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onFrameChoosen(mFrameInfo);
		}
	}
}