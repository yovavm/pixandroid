package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;

import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.ProfileFeedsPagerActivity;

public class ProfileTabPageIndicator extends IconPageIndicator {

	
	public ProfileTabPageIndicator(Context context) {
		super(context);
	}
	
	public ProfileTabPageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onPageSelected(int pageIndex) {
		super.onPageSelected(pageIndex);
		
		switch (pageIndex) {
		case ProfileFeedsPagerActivity.FFED_TYPE_COMPLETED:
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_COMPLETED_TAB_PRESSED, null);
			break;
		case ProfileFeedsPagerActivity.FFED_TYPE_OPEN:
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_NONCOMPLETED_TAB_PRESSED, null);
			break;
		case ProfileFeedsPagerActivity.FFED_TYPE_LIKED:
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_LIKED_TAB_PRESSED, null);
			break;
		default:
			break;
		}
	}
}
