package com.pixplit.android.ui.elements;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager.LayoutParams;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;

public class PixDialog extends Dialog{
	
	int mLayoutResID;
	
	public PixDialog(Context context, int layoutResID) {
		super(context, R.style.PixAlertDialog);
		mLayoutResID = layoutResID;
		
		setContentView(mLayoutResID);
		
		// center the dialog vertically as the title takes place although not used
		View topPanel = findViewById(android.R.id.title);
		if (topPanel != null) {
			topPanel.setVisibility(View.GONE);
		}
		
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        LayoutParams params = getWindow().getAttributes();
        params.width = PixAppState.getState().getFrameWidth();
        params.height = LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }
	
}
