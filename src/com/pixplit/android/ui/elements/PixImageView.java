package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public class PixImageView extends ImageView {
	PixBitmapHolder mBitmapHolder;
	boolean mHighlight = false;
	
	public PixImageView(Context context) {
		super(context);
	}
	
	public PixImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public PixImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw (Canvas canvas) {
		if (PixDefs.recycleBitmaps) {
			// safety - verify that the image view is not trying to draw a recycled bitmap
			Drawable drawable = this.getDrawable();
			if (drawable != null &&
					drawable instanceof BitmapDrawable) {
			    BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
			    Bitmap bitmap = bitmapDrawable.getBitmap();
			    if (bitmap != null) {
			    	if (bitmap.isRecycled()) {
			    		//Utils.pixLog("PixImageView", "$$ onDraw: bitmap has been recycled");
			    		return;
			    	}
			    }
			}
		}
		try {
			super.onDraw(canvas);
		} catch (Error e) {
			//Utils.pixLog("PixImageView", "$$ error in onDraw");
			e.printStackTrace();
		} catch (Exception e) {
			//Utils.pixLog("PixImageView", "$$ exception in onDraw");
			e.printStackTrace();
		}
	}
	
	public void setImageBitmapHolder(PixBitmapHolder bitmapHolder) {
		if (mBitmapHolder != null) {
			if (mBitmapHolder != bitmapHolder) {
				mBitmapHolder.releaseBitmap("PixImageView: use on override bitmapHolder");
			}
		}
		
		Bitmap bitmap = null;
		if (bitmapHolder != null) {
			if (mBitmapHolder != bitmapHolder) {
				bitmapHolder.useBitmap("PixImageView: use on setImageBitmap");
			}
			bitmap = bitmapHolder.getBitmap();
		}
		
		mBitmapHolder = bitmapHolder;
		super.setImageBitmap(bitmap);		
	}
	
	@Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        // Will cause displayed bitmap wrapper to be 'free-able'
        setImageBitmapHolder(null);
    }
	
	public void highlightOnPress(boolean highlight) {
		this.mHighlight = highlight;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		if (mHighlight) {
			switch(e.getAction()) {
			case MotionEvent.ACTION_DOWN:
				setColorFilter(getContext().getResources().getColor(R.color.highlight));
				break;
	        case MotionEvent.ACTION_UP:
	        case MotionEvent.ACTION_CANCEL:
	        	clearColorFilter();
	            break;
	        default:
	        	break;
			}
		}
	    return super.onTouchEvent(e);
	}
}
