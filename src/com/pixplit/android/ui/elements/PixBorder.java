package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class PixBorder extends View{
	private float borderWidth = 0;
	private Paint paint;
    private int w;
    private int h;

    public PixBorder(Context context, float width) {
		super(context);
		this.borderWidth = width;
		paint = new Paint();
	}
    
    public PixBorder(Context context, AttributeSet attr) {
        super(context, attr);
        paint = new Paint();
    }
        
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.w = w;
        this.h = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
    	paint.setColor(PixApp.getContext().getResources().getColor(R.color.light_grey_bg));
        paint.setStrokeWidth(borderWidth);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(0, 0, w, h, paint);
        
    }
	
}
