package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class NewSplitsBar extends PixTextView implements View.OnClickListener{
	NewSplitsBarDelegate mDelegate;
	Animation slideInAnimation;
	Animation slideOutAnimation;
	boolean completed;
	String newSplitsText;
	String newSingleSplitText;
	
	public interface NewSplitsBarDelegate {
		public void onNewSplitsBarClick();
	}
	
	public NewSplitsBar(Context context, boolean completed, NewSplitsBarDelegate delegate) {
		super(context);
		this.mDelegate = delegate;
		this.completed = completed;
		if (this.completed) {
			newSplitsText = PixApp.getContext().getString(R.string.new_completed_splits);
			newSingleSplitText = PixApp.getContext().getString(R.string.new_completed_split);
		} else {
			newSplitsText = PixApp.getContext().getString(R.string.new_splits);
			newSingleSplitText = PixApp.getContext().getString(R.string.new_split);
		}
		this.init();
	}
	
	public NewSplitsBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}
	public NewSplitsBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init();
	}

	private void init() {
		this.setBackgroundResource(R.drawable.new_splits_bar_selector);
		this.setTextColor(getContext().getResources().getColor(R.color.whitish));
		this.setTypeface(null, Typeface.BOLD);
		this.setGravity(Gravity.CENTER);
		this.setOnClickListener(this);
		
		this.slideInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_in_top);
		if (this.slideInAnimation != null) {
			this.slideInAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.VISIBLE);
				}
			});
		}
		this.slideOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.slide_out_top);
		if (this.slideOutAnimation != null) {
			this.slideOutAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.GONE);
				}
			});
		}
		this.setAnimation(slideInAnimation);
		

	}
	
	public void show() {
		if (this.getVisibility() == View.GONE &&
				this.slideInAnimation != null) {
			this.startAnimation(slideInAnimation);
		}
	}
	
	public void hide() {
		if (this.getVisibility() == View.VISIBLE &&
				this.slideOutAnimation != null) {
			this.startAnimation(slideOutAnimation);
		}
	}
	
	public void setNewSplitsNumber(int newSplitNum) {
		if (newSplitNum > 0) {
			if (newSplitNum > 1) {
				if (newSplitNum > 10) {
					this.setText("10+ " + newSplitsText);
				} else {
					this.setText(newSplitNum + " " + newSplitsText);
				}
			} else {
				this.setText(newSplitNum + " " + newSingleSplitText);
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onNewSplitsBarClick();
		}		
	}

}
