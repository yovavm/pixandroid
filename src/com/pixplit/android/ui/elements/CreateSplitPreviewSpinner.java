package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class CreateSplitPreviewSpinner extends FrameLayout {

	int bgColor = PixApp.getContext().getResources().getColor(R.color.black);
	ImageView spinner;
	Animation spinnerAnim;
	
	public CreateSplitPreviewSpinner(Context context, int width, int height) {
		super(context);
		setBackgroundColor(bgColor);
		setLayoutParams(new LayoutParams(width, height));
		
		spinner = new ImageView(context);
		spinner.setBackgroundResource(R.drawable.spinner);
		LayoutParams spinnerLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		spinnerLayoutParams.gravity = Gravity.CENTER;
		addView(spinner, spinnerLayoutParams);
		
        spinnerAnim = AnimationUtils.loadAnimation(context, R.anim.spinning_slow_anim);
	}
	
	public CreateSplitPreviewSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void show() {
		setVisibility(View.VISIBLE);
		spinner.setAnimation(spinnerAnim);
	}
	
	public void hide() {
		setVisibility(View.GONE);
		spinner.setAnimation(null);
	}

}
