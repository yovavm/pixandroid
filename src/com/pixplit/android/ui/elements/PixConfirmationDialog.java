package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class PixConfirmationDialog extends PixDialog {
	ConfirmationDialogDelegate mDelegate;
	
	public interface ConfirmationDialogDelegate {
		public void onConfirm();
		public void onDecline();
	}
	
	public PixConfirmationDialog(Context context, ConfirmationDialogDelegate delegate, String title, String message, String confirmText, String cancelText) {
		super(context, R.layout.confirmation_dialog);
		mDelegate = delegate;
		this.init(title, message, confirmText, cancelText);
	}
	
	private void init(String titleText, String messageText, String confirmText, String cancelText) {
		TextView title = (TextView)findViewById(R.id.confirmationTitle);
		if (title != null && Utils.isValidString(titleText)) {
			title.setText(titleText);
		}
		
		TextView message = (TextView)findViewById(R.id.confirmationMessage);
		if (message != null) {
			if (Utils.isValidString(messageText)) {
				message.setText(messageText);
			} else {
				message.setVisibility(View.GONE);
			}
		}
		
		TextView cancelBtn = (TextView)findViewById(R.id.cancelBtn);
		if (cancelBtn != null) {
			if (Utils.isValidString(cancelText)) {
				cancelBtn.setText(cancelText);
			}
			cancelBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null) {
						mDelegate.onDecline();
					}
					dismiss();
				}
			});
		}
		
		TextView logoutBtn = (TextView)findViewById(R.id.confirmBtn);
		if (logoutBtn != null) {
			if (Utils.isValidString(confirmText)) {
				logoutBtn.setText(confirmText);
			}
			logoutBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null) {
						mDelegate.onConfirm();
					}
					dismiss();
				}
			});
		}
	}
}
