package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class ItemsChooser extends LinearLayout {
	String []items;
	ItemsChooserDelegate delegate;
	
	static final float btnTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_list_items_text_size);
	static final int btnTextColor = PixApp.getContext().getResources().getColor(R.color.black);
	static final int dividerColor = PixApp.getContext().getResources().getColor(R.color.divider_color);
	static final int dividerHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, 
											PixApp.getContext().getResources().getDisplayMetrics());
	static final int itemHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, 
			PixApp.getContext().getResources().getDisplayMetrics());
	
	public interface ItemsChooserDelegate {
		public void onItemChoosen(String itemText);
	}
	
	public ItemsChooser(Context context) {
		super(context);
		init();
	}
	public ItemsChooser(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void setDelegate(ItemsChooserDelegate delegate) {
		this.delegate = delegate;
	}
	
	private void init() {
		setBackgroundResource(R.drawable.signup_email_bg);
		setOrientation(LinearLayout.VERTICAL);
	}
	
	@SuppressWarnings("deprecation")
	public void setItems(final String []items) {
		this.items = items;
		if (items == null) return;
		
		for (int i=0; i<items.length; i++) {
			if (Utils.isValidString(items[i])) {
				if (i > 0) {
					// add a divider
					TextView divider = new TextView(getContext());
					divider.setBackgroundColor(dividerColor);
					divider.setPadding(0, 0, 0, 0);
					LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, dividerHeight);
					this.addView(divider, params);
				}
				PixTextView itemView = new PixTextView(getContext());
				itemView.setText(items[i]);
				itemView.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
				itemView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
				itemView.setTextColor(btnTextColor);
				itemView.setMinHeight(itemHeight);
				itemView.setPadding(20,0,65,0);
				if (Utils.hasJellyBean()) {
					itemView.setBackground(getResources().getDrawable(R.drawable.list_item_btn_selector));
		        } else {
		        	itemView.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_item_btn_selector));
		        }
				itemView.setOnClickListener(new OnListItemClickListener(i, new OnListItemClickDelegate() {
					@Override
					public void onItemClick(int itemIndex) {
						if (delegate != null) {
							delegate.onItemChoosen(items[itemIndex]);
						}
					}
				}));
				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 
						LayoutParams.WRAP_CONTENT);
				this.addView(itemView, params);
			}
		}
	}
	
	public void autoComplete(String prefix) {
		int childCount = getChildCount();
		if (childCount <= 0) return;
		boolean hasVisibleItems = false;
		boolean firstVisibleItem = true;
		for (int i=0; i<childCount; i=i+2) {
			View itemView = getChildAt(i);
			if (itemView != null &&
					itemView instanceof PixTextView) {
				String text = ((PixTextView)itemView).getText().toString();
				int visibility = View.GONE;
				if (text.startsWith(prefix)) {
					visibility = View.VISIBLE;
					hasVisibleItems = true;
				}
				itemView.setVisibility(visibility);
				if (i > 0) {
					// divider visibility
					View divider = getChildAt(i-1);
					if (firstVisibleItem) {
						divider.setVisibility(View.GONE);
					} else {
						divider.setVisibility(visibility);
					}
				}
				if (visibility == View.VISIBLE) {
					firstVisibleItem = false;
				}
			}
		}
		if (hasVisibleItems) {
			setVisibility(View.VISIBLE);
		} else {
			setVisibility(View.GONE);
		}
	}
	
	public void show() {
		this.setVisibility(View.VISIBLE);
	}
	
	public void hide() {
		this.setVisibility(View.GONE);
	}
	
	public interface OnListItemClickDelegate {
		public void onItemClick(int itemIndex);
	}
	
	private class OnListItemClickListener implements OnClickListener {
		int index;
		OnListItemClickDelegate delegate;
		
		public OnListItemClickListener(int index, OnListItemClickDelegate delegate) {
			this.index = index;
			this.delegate = delegate;
		}
		
		@Override
		public void onClick(View v) {
			if (this.delegate != null) {
				this.delegate.onItemClick(this.index);
			}
		}
	}
}
