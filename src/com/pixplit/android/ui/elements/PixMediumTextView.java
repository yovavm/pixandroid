package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.pixplit.android.PixApp.Fonts;

public class PixMediumTextView extends TextView {
	
	public PixMediumTextView(Context context) {
        super(context);
        init();
    }
	
    public PixMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PixMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        setTypeface(Fonts.PixFontMedium);
    }
}
