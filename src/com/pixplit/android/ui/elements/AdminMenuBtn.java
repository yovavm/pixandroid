package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class AdminMenuBtn extends RelativeLayout implements OnClickListener{
	AdminMenuBtnDelegate mDelegate = null;

	final static float titleSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
	final static int titleColor = PixApp.getContext().getResources().getColor(R.color.grey_dark);

	ImageView checkBox;
	PixTextView title;
	
	private static final int checkBoxId = 1;
	private static final int titleId = 2;
	
	public AdminMenuBtn(Context context) {
		super(context);
		init();
	}
	public AdminMenuBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	public AdminMenuBtn(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	public interface AdminMenuBtnDelegate {
		public void onAdminMenuCheckboxClicked();
		public void onAdminMenuBtnClicked();
	}
	
	public void setDelegate(AdminMenuBtnDelegate delegate) {
		mDelegate = delegate;
	}
	
	@SuppressWarnings("deprecation")
	public void init() {
		setOnClickListener(this);
		
		setPadding(getResources().getDimensionPixelSize(R.dimen.settings_item_left_padding), 
				getResources().getDimensionPixelSize(R.dimen.settings_btn_vertical_margin), 
				getResources().getDimensionPixelSize(R.dimen.settings_item_left_padding), 
				getResources().getDimensionPixelSize(R.dimen.settings_btn_vertical_margin));
		if (Utils.hasJellyBean()) {
			setBackground(getResources().getDrawable(R.drawable.setting_btn_selector));
        } else {
        	setBackgroundDrawable(getResources().getDrawable(R.drawable.setting_btn_selector));
        }

		
		checkBox = new ImageView(getContext());
		checkBox.setId(checkBoxId);
		checkBox.setBackgroundResource(R.drawable.check_off);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		int checkboxMargin = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.settings_item_horizontal_margin);
		params.setMargins(0, 0, checkboxMargin, 0);
		addView(checkBox, params);
		checkBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDelegate != null) {
					mDelegate.onAdminMenuCheckboxClicked();
				}
			}
		});

		title = new PixTextView(getContext());
		title.setId(titleId);
		title.setText(PixApp.getContext().getResources().getString(R.string.admin));
		title.setTextColor(titleColor);
		title.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleSize);
		title.setMinHeight(getResources().getDimensionPixelSize(R.dimen.settings_item_min_height));
		title.setBackgroundResource(R.color.transparent);
		title.setGravity(Gravity.CENTER_VERTICAL);
		params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);	
		params.addRule(RelativeLayout.RIGHT_OF, checkBoxId);
		addView(title, params);
	}
	
	public void updateView(boolean isAdminAuthenticated) {
		if (isAdminAuthenticated) {
			checkBox.setBackgroundResource(R.drawable.check_on);
		} else {
			checkBox.setBackgroundResource(R.drawable.check_off);
		}
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onAdminMenuBtnClicked();
		}
	}
}
