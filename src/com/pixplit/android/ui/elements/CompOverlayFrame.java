package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class CompOverlayFrame extends FrameLayout implements AnimationListener {
//	private static final String TAG = "CompOverlayFrame";
	
	public CompOverlayFrame(Context context) {
		super(context);
	}
	
	public CompOverlayFrame(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onAnimationStart(Animation animation) {
		//Utils.pixLog(TAG, "onAnimationStart");
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		//Utils.pixLog(TAG, "onAnimationEnd");
		setVisibility(View.INVISIBLE);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {}

	public void show() {
		setVisibility(View.VISIBLE);
		Animation animation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.popup_show);
		
		LayoutAnimationController animController = new LayoutAnimationController(animation);
		animController.setDelay(0);
		setLayoutAnimation(animController);
		startLayoutAnimation();
	}
	
	public void hide() {
		if (getChildCount() > 0) {
			Animation animation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.popup_hide);
			animation.setAnimationListener(this);
			LayoutAnimationController animController = new LayoutAnimationController(animation);
			animController.setDelay(0);
			setLayoutAnimation(animController);
			startLayoutAnimation();
		} else {
			// no animation is needed
			setVisibility(View.INVISIBLE);
		}
	}

}
