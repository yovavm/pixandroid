package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;

public class ShareToBtn extends RelativeLayout implements OnClickListener{
	ShareToBtnDelegate mDelegate = null;

	final static float titleSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
	final static int titleColor = PixApp.getContext().getResources().getColor(R.color.grey_dark);
	final static float nameSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_small);
	final static int nameColor = PixApp.getContext().getResources().getColor(R.color.black);

	ImageView checkBox;
	PixTextView title;
	PixTextView username;
	
	private static final int checkBoxId = 1;
	private static final int titleId = 2;
	
	public ShareToBtn(Context context) {
		super(context);
	}
	public ShareToBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public ShareToBtn(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public interface ShareToBtnDelegate {
		public void onShareToCheckboxClicked();
		public void onShareToBtnClicked();
	}
	
	public void setDelegate(ShareToBtnDelegate delegate, String titleText) {
		this.mDelegate = delegate;
		this.init(titleText);
	}
	
	public void init(String titleText) {
		setOnClickListener(this);
		
		checkBox = new ImageView(getContext());
		checkBox.setId(checkBoxId);
		checkBox.setBackgroundResource(R.drawable.check_off);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		int checkboxMargin = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.settings_item_horizontal_margin);
		params.setMargins(0, 0, checkboxMargin, 0);
		addView(checkBox, params);
		checkBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDelegate != null) {
					mDelegate.onShareToCheckboxClicked();
				}
			}
		});

		title = new PixTextView(getContext());
		title.setId(titleId);
		title.setText(titleText);
		title.setTextColor(titleColor);
		title.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleSize);
		
		title.setBackgroundResource(R.color.transparent);
		params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);	
		params.addRule(RelativeLayout.RIGHT_OF, checkBoxId);
		addView(title, params);
		
		username = new PixTextView(getContext());
		username.setTextAppearance(getContext(), R.style.SettingsItemExtraText);
		username.setTextColor(nameColor);
		username.setTextSize(TypedValue.COMPLEX_UNIT_PX, nameSize);
    
		username.setBackgroundResource(R.color.transparent);
		params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.BELOW, titleId);
		params.addRule(RelativeLayout.ALIGN_LEFT, titleId);
		addView(username, params);
	}
	
	public void updateView(boolean isLinked, String usernameText) {
		if (isLinked) {
			checkBox.setBackgroundResource(R.drawable.check_on);
			username.setText(usernameText);
			username.setVisibility(View.VISIBLE);
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.ALIGN_TOP, checkBoxId);	
			params.addRule(RelativeLayout.RIGHT_OF, checkBoxId);
			title.setLayoutParams(params);
			
		} else {
			checkBox.setBackgroundResource(R.drawable.check_off);
			username.setVisibility(View.INVISIBLE);
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.CENTER_VERTICAL);	
			params.addRule(RelativeLayout.RIGHT_OF, checkBoxId);
			title.setLayoutParams(params);
		}
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onShareToBtnClicked();
		}
	}
}
