package com.pixplit.android.ui.elements;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixplit.android.R;

public class PixProgressIndicator extends PixDialog {
	private static PixProgressIndicator indicator;
	
	TextView indicationMessage;
	ImageView spinner;
	Animation spinnerAnim;
	
	private PixProgressIndicator(Context context) {
		super(context, R.layout.pix_progress_indicator);
	}
	
	public static void showMessage(Context context, String message, boolean cancelable) {
		if (context != null) {
			if (indicator != null) {
				indicator.dismiss();
			}
			indicator = new PixProgressIndicator(context);
			indicator.init();
			indicator.setCancelable(cancelable);
			indicator.setMessage(message);
			indicator.show();
		}
	}
	
	public static void hideMessageWithIndication(boolean success, String indication) {
		if (indicator != null) {
			indicator.setMessage(indication);
			if (indicator.spinner != null) {
				indicator.spinner.setAnimation(null);
				if (success) {
					indicator.spinner.setBackgroundResource(R.drawable.alert_check);
				} else {
					indicator.spinner.setBackgroundResource(R.drawable.alert_x);
				}
			}
			final PixProgressIndicator currentIndicator = indicator;
			new Handler().postDelayed(new Runnable() {
		        public void run() {
		        	if (currentIndicator != null && indicator != null) {
		        		if (currentIndicator == indicator) {
		        			hideMessage();
		        		} else {
			        		currentIndicator.dismiss();		        			
		        		}
		        	}
		        }
		    }, 800);
		}
	}
	
	public static void hideMessage() {
		if (indicator != null) {
			indicator.dismiss();
			indicator = null;
		}
	}
	
	private void setMessage(String message) {
		if (indicationMessage != null) {
			indicationMessage.setText(message);
		}
	}
	
	private void init() {
		indicationMessage = (TextView)findViewById(R.id.indicationText);
		spinnerAnim = AnimationUtils.loadAnimation(getContext(), R.anim.spinning_anim);
		
		spinner = (ImageView)findViewById(R.id.indicationImage);
		
		// center the dialog vertically as the title takes place although not used
		View topPanel = findViewById(android.R.id.title);
		if (topPanel != null) {
			topPanel.setVisibility(View.GONE);
		}
	}
	
	@Override
    public void show() {
		try {
	        super.show();
	        if (spinner != null) {
	        	spinner.startAnimation(spinnerAnim);
	        }
		} catch (Exception e) {
        	// on some cases the dialog may no longer be attached to a window and therefore fail to show. 
        	e.printStackTrace();
        }
    }
	
	@Override
    public void dismiss() {
		 try {
			if (spinner != null) {
				spinner.setAnimation(null);
	        }
        	super.dismiss();
        } catch (Exception e) {
        	// on some cases the dialog may no longer be attached to a window and therefore fail to dismiss. 
        	e.printStackTrace();
        }
    }

	public static void onPause() {
		if (indicator != null) {
			// clear old indication
			indicator.dismiss();
			indicator = null;
		}
	}

}
