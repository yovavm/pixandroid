package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.pixplit.android.PixApp.Fonts;

public class PixEditText extends EditText {
	
	public PixEditText(Context context) {
        super(context);
        init();
    }
	
    public PixEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PixEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setTypeface(Fonts.PixFont);
    }
}
