package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;

import com.pixplit.android.R;

public class LikeBtn extends PixTextView implements OnClickListener{
	LikeBtnDelegate mDelegate;
	boolean mLikeStatus;
	
	public interface LikeBtnDelegate {
		public void onCompLikeClick(boolean liked);
		public boolean verifyUser();
	}
	
	public LikeBtn(Context context) {
		super(context);
	}
	public LikeBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void init(LikeBtnDelegate delegate) {
		setOnClickListener(this);
		mDelegate = delegate;
		mLikeStatus = false;
	}
	
	public void bindData(boolean likeStatus){
		mLikeStatus = likeStatus;
		updateView();
	}
	
	public void clearData() {
		mLikeStatus = false;
	}
	
	@Override
    public void onClick(View v) {
        if (mDelegate != null && 
        		mDelegate.verifyUser()) {
        	this.setLike(!mLikeStatus);
        	mDelegate.onCompLikeClick(mLikeStatus);
        }
    }
	
	private void updateView() {
		if (mLikeStatus) {
	    	setBackgroundResource(R.drawable.feed_like_btn_selected);
		} else {
			setBackgroundResource(R.drawable.feed_like_btn_selector);
		}
	}
	
	public void setLike(boolean liked) {
		if (mLikeStatus == liked) return;
		mLikeStatus = liked;
		updateView();
	}
	
}
