package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;

import com.pixplit.android.R;

public class FeedBarNotificationBadge extends PixTextView{

	public FeedBarNotificationBadge(Context context) {
		super(context);
		this.init();
	}
	public FeedBarNotificationBadge(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}
	public FeedBarNotificationBadge(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		this.init();
	}
	
	private void init() {
		this.setTextColor(getContext().getResources().getColor(R.color.whitish));
		this.setVisibility(View.GONE);
		this.setGravity(Gravity.CENTER);
		this.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.comps_feed_notification_badge_text_size));
		this.setBackgroundResource(R.drawable.feed_notification_badge);
	}
	
	public void show(int number) {
		this.setVisibility(View.VISIBLE);
		if (number > 99) {
			number = 99;
		}
		this.setText(String.format("%d", number));
	}
	
	public void hide() {
		this.setVisibility(View.GONE);
		this.setText("");
	}
}
