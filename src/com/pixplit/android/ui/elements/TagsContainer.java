package com.pixplit.android.ui.elements;

import java.util.List;

import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class TagsContainer {
	FlowLayout mTagsLayout;
	PixBaseListDelegate mListDelegate;
	List<String> tags;
	final static float textSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comp_likes_text_size);
	final static int textColor = PixApp.getContext().getResources().getColor(R.color.grey_dark);
	final static int textBg = PixApp.getContext().getResources().getColor(R.color.transparent);
	static final int VERTICAL_MARGIN = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.comps_feed_tags_vertical_margin);
	static final int tagPadding = (int)PixApp.getContext().getResources().getDimension(R.dimen.comp_tag_padding);
	private PixTextView[] tagsViews;
	
	public TagsContainer(FlowLayout tagsLayout, PixBaseListDelegate listDelegate) {
		this.mTagsLayout = tagsLayout;
		this.mListDelegate = listDelegate;
		
		if (this.mTagsLayout != null &&
				this.mTagsLayout.getParent() instanceof LinearLayout) {
    		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
    				LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    		params.setMargins(0, VERTICAL_MARGIN, 0, 0);
    		this.mTagsLayout.setLayoutParams(params);
    	}
		tagsViews = new PixTextView[PixDefs.MAX_TAGS_TO_DISPLAY];
		for (int i = 0; i<tagsViews.length; i++) {
			tagsViews[i] = new PixTextView(mTagsLayout.getContext());
			tagsViews[i].setTextColor(textColor);
			tagsViews[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			tagsViews[i].setBackgroundColor(textBg);

			tagsViews[i].setPadding(tagPadding, 0, tagPadding, 0);

			tagsViews[i].setOnClickListener(new onTagClickListener());
			mTagsLayout.addView(tagsViews[i]);
		}
	}
	
	public void setTags(List<String> tags) {
		if (mTagsLayout != null) {
			if (tags != null) {
				this.tags = tags;
				insertTags();
			} else {
				// clean the tags from old content
				for (int i = 0; i<tagsViews.length; i++) {
					tagsViews[i].setText("");
				}
				mTagsLayout.setVisibility(View.GONE);
			}
		}
	}
	
	private void insertTags() {
		if (mTagsLayout != null) {
			int currentTagView = 0;
			for (int i = 0; i < tags.size(); i++) {
				if (i >= PixDefs.MAX_TAGS_TO_DISPLAY) {
					break;
				}
				final String tag = tags.get(i);
				
				if (tag.startsWith("#")) {
					tagsViews[currentTagView].setText(tag);
				} else {
					tagsViews[currentTagView].setText("#"+tag);
				}
				tagsViews[currentTagView].setVisibility(View.VISIBLE);
				currentTagView++;
			}
			if (currentTagView > 0) {
				// at least one tag is in the container
				mTagsLayout.setVisibility(View.VISIBLE);
			}
			for (int i = currentTagView; i<tagsViews.length; i++) {
				tagsViews[currentTagView].setVisibility(View.INVISIBLE);
			}
		}
	}
	
	public void clearTags(){
		if (mTagsLayout != null) {
			// clean the tags from old content
			for (int i = 0; i<tagsViews.length; i++) {
				tagsViews[i].setText("");
			}
			mTagsLayout.setVisibility(View.GONE);
		}
	}
	
	private class onTagClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			if (v instanceof TextView) {
				TextView tagView = (TextView)v;
				if (tagView.getText() != null) {
					String tag = tagView.getText().toString();
					if (mListDelegate != null && 
							Utils.isValidString(tag)) {
						if (tag.startsWith("#")) {
							mListDelegate.onTagClick(tag.substring(1));
						} else {
							mListDelegate.onTagClick(tag);
						}
					}
				}
			}
		}
	}
}
