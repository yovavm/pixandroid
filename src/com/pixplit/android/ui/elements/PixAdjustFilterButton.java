package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class PixAdjustFilterButton extends ImageView implements View.OnClickListener {
	float mScale;
	int mAdjustType;
	PixAdjustFilterButtonDelegate mDelegate;
	
	public interface PixAdjustFilterButtonDelegate {
		public void onAdjustFilterClick(PixAdjustFilterButton adjustFilter, int type, float scale);
	}

	public PixAdjustFilterButton(Context context) {
		super(context);
		setOnClickListener(this);
	}
	
	public PixAdjustFilterButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnClickListener(this);
	}
	
	public void setScale(float scale) {
		mScale = scale;
	}
	
	public void setType(int filterType) {
		mAdjustType = filterType;
	}
	
	public int getType() {
		return mAdjustType;
	}
	
	public void setDelegate(PixAdjustFilterButtonDelegate delegate) {
		mDelegate = delegate;
	}
	
	@Override
	public void onClick(View v) {
		if (mDelegate != null) {
			mDelegate.onAdjustFilterClick(this, mAdjustType, mScale);
		}
	}
}
