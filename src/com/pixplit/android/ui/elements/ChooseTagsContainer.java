package com.pixplit.android.ui.elements;

import android.content.Context;
import android.view.View;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class ChooseTagsContainer {
	FlowLayout mTagsLayout;
	SelectableTextView[] tags;
	Context context;
//	private static final int tagBtnColor = PixApp.getContext().getResources().getColor(R.color.tag_btn_default);
//	private static final int tagBtnSelectedColor = PixApp.getContext().getResources().getColor(R.color.tag_btn_selected);
	private static final int tagTextColor = PixApp.getContext().getResources().getColor(R.color.brown);
	private static final int tagTextSelectedColor = PixApp.getContext().getResources().getColor(R.color.yellow);
	
	public ChooseTagsContainer(Context context, FlowLayout tagsLayout, String[] tags) {
		this.mTagsLayout = tagsLayout;
		this.context = context;
		
		if (mTagsLayout != null) {
			if (tags != null && tags.length > 0) {
				mTagsLayout.setVisibility(View.VISIBLE);
				insertTags(tags);
			} else {
				mTagsLayout.setVisibility(View.GONE);
			}
		}
	}
			
	private void insertTags(String[] tagsText) {
		if (mTagsLayout != null && tagsText != null) {
			this.tags = new SelectableTextView[tagsText.length];
			for (int i = 0; i < tagsText.length; i++) {
				final String tagStr = tagsText[i];
				if (Utils.isValidString(tagStr)) {
					tags[i] = new SelectableTextView(PixApp.getContext(), 
							R.color.tag_btn_selected, 
							R.color.tag_btn_default, 
							tagTextSelectedColor, 
							tagTextColor);
					tags[i].setText(tagStr);
					tags[i].setTextAppearance(this.context, R.style.TagText);
					tags[i].setPadding(15, 7, 15, 7);
					mTagsLayout.addView(tags[i]);
				}
			}
		}
	}
	
	public String getChoosenTags() {
		String choosenTags = null;
		if (tags != null) {
			for (int i = 0; i<tags.length; i++) {
				if (tags[i] != null && 
						tags[i].isSelected() && 
						tags[i].getText() != null &&
						Utils.isValidString(tags[i].getText().toString())) {
					if (choosenTags == null) {
						choosenTags = tags[i].getText().toString();
					} else {
						choosenTags = choosenTags.concat(","+tags[i].getText().toString());
					}
				}
			}
		}
		
		return choosenTags;
	}
}
