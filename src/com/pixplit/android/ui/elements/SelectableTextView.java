package com.pixplit.android.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;

public class SelectableTextView extends PixTextView implements OnClickListener{
	boolean selected = false;
	int selectedBgRes = 0;
	int notSelectedBgRes = 0;
	int selectedTextColor;
	int notSelectedTextColor;
	
	public SelectableTextView(Context context, int selectedRes, int notSelectedRes, int selectedTextColor, int notSelectedTextColor) {
		super(context);
		this.selectedBgRes = selectedRes;
		this.notSelectedBgRes = notSelectedRes;
		this.selectedTextColor = selectedTextColor;
		this.notSelectedTextColor = notSelectedTextColor;
		if (notSelectedBgRes > 0) {
			setBackgroundResource(notSelectedBgRes);
			if (notSelectedTextColor != 0) {
				setTextColor(notSelectedTextColor);
			}
		}
		setOnClickListener(this);
	}
	
	public SelectableTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public SelectableTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onClick(View v) {
		if (selected) {
			selected = false;
			if (notSelectedBgRes > 0) {
				setBackgroundResource(notSelectedBgRes);
				if (notSelectedTextColor != 0) {
					setTextColor(notSelectedTextColor);
				}
			}
		} else {
			selected = true;
			if (selectedBgRes > 0) {
				setBackgroundResource(selectedBgRes);
				if (selectedTextColor != 0) {
					setTextColor(selectedTextColor);
				}
			}
		}
	}
	
	public boolean isSelected() {
		return selected;
	}
}
