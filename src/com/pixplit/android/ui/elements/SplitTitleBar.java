package com.pixplit.android.ui.elements;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.util.Utils;

public class SplitTitleBar extends FrameLayout {
	
	PixMediumTextView titleText;
	Animation slideInAnimation;
	Animation slideOutAnimation;
	
	public SplitTitleBar(Context context, boolean completed) {
		super(context);
		this.init();
	}
	public SplitTitleBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}
	public SplitTitleBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init();
	}

	private void init() {
		this.setBackgroundColor(PixApp.getContext().getResources().getColor(R.color.light_alpha_black));

		this.titleText = new PixMediumTextView(getContext());
		this.titleText.setTextColor(getContext().getResources().getColor(R.color.white));
		this.titleText.setTypeface(null, Typeface.BOLD);
		this.titleText.setMaxLines(3);
		this.titleText.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
		this.titleText.setGravity(Gravity.CENTER);
		this.titleText.setPadding(5, 5, 5, 5);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(8, 0, 0, 0);
		this.addView(titleText, params);
		
		this.slideInAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.title_slide_in_anim);
		if (this.slideInAnimation != null) {
			this.slideInAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.VISIBLE);
				}
			});
		}
		this.slideOutAnimation = AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.title_slide_out_anim);
		if (this.slideOutAnimation != null) {
			this.slideOutAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					setVisibility(View.GONE);
				}
			});
		}
	}
	
	public void setTitle(String title) {
		if (this.titleText != null) {
			this.titleText.setText(title);
		}
	}
	
	public void show() {
		if (this.getVisibility() == View.GONE &&
				this.slideInAnimation != null) {
			this.startAnimation(slideInAnimation);
		}
	}
	
	public void hide() {
		if (this.getVisibility() == View.VISIBLE &&
				this.slideOutAnimation != null) {
			this.startAnimation(slideOutAnimation);
		}
	}
	
	public boolean hasTitle() {
		return (this.titleText != null &&
				this.titleText.getText() != null &&
				Utils.isValidString(this.titleText.getText().toString()));
	}
}
