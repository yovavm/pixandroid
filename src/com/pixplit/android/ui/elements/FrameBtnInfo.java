package com.pixplit.android.ui.elements;

import com.pixplit.android.globals.PixCompositionFramesetType;

public class FrameBtnInfo {
	private int mDrawableRes;
	private PixCompositionFramesetType mFrameType;
	public FrameBtnInfo(int drawableRes, PixCompositionFramesetType frameType) {
		this.mDrawableRes = drawableRes;
		this.mFrameType = frameType;
	}
	public int getDrawableRes() {
		return mDrawableRes;
	}
	public PixCompositionFramesetType getFrameType() {
		return mFrameType;
	}
}