package com.pixplit.android.ui;

import com.pixplit.android.util.billing.IabHelper;

public interface BillingHelperController {
	public IabHelper getIabHelper();
}
