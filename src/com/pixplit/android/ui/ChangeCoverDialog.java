package com.pixplit.android.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.UpdateCoverPicHttpRequest;

public class ChangeCoverDialog extends Dialog {
	ChangeCoverFragmentDelegate mDelegate;
	
	private static final int frameWidth = PixAppState.getState().getFrameWidth();
	private static final int coverImagePadding = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.change_cover_frames_padding);
	private static final int coverImageDimen = (frameWidth - 4*coverImagePadding)/3;
	public interface ChangeCoverFragmentDelegate {
		public void onCoverPhotoChoosen(int coverIndex);
	}
	private class ProfileCover {
		private int id;
		private int drawableRes;
		private ProfileCover(int id, int drawableRes) {
			this.id = id;
			this.drawableRes = drawableRes;
		}
		public int getId() {
			return id;
		}
		public int getDrawableRes() {
			return drawableRes;
		}
		
	}
	private ProfileCover[] profileCovers = {new ProfileCover(R.id.profileCover1, R.drawable.cover_profile1_tn),
											new ProfileCover(R.id.profileCover2, R.drawable.cover_profile2_tn),
											new ProfileCover(R.id.profileCover3, R.drawable.cover_profile3_tn),
											new ProfileCover(R.id.profileCover4, R.drawable.cover_profile4_tn),
											new ProfileCover(R.id.profileCover5, R.drawable.cover_profile5_tn),
											new ProfileCover(R.id.profileCover6, R.drawable.cover_profile6_tn),
											new ProfileCover(R.id.profileCover7, R.drawable.cover_profile7_tn),
											new ProfileCover(R.id.profileCover8, R.drawable.cover_profile8_tn),
											new ProfileCover(R.id.profileCover9, R.drawable.cover_profile9_tn),
											new ProfileCover(R.id.profileCover10, R.drawable.cover_profile10_tn),
											new ProfileCover(R.id.profileCover11, R.drawable.cover_profile11_tn),
											new ProfileCover(R.id.profileCover12, R.drawable.cover_profile12_tn)};
	
	public ChangeCoverDialog(Context context, ChangeCoverFragmentDelegate delegate) {
		super(context, R.style.PixTipDialog);
		this.mDelegate = delegate;
	    
		setContentView(R.layout.change_cover_layout);
       
		LayoutParams params = getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        
		// center the dialog vertically as the title takes place although not used
		View topPanel = findViewById(android.R.id.title);
		if (topPanel != null) {
			topPanel.setVisibility(View.GONE);
		}

        ImageView toolbarImage = (ImageView)this.findViewById(R.id.toolbarImage);
        if (toolbarImage != null) {
        	toolbarImage.setImageResource(R.drawable.close_x_btn);
        	toolbarImage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dismiss();
				}
			});
        }
        
        TextView toolbarTitle = (TextView)this.findViewById(R.id.toolbarTitle);
		if (toolbarTitle != null) {
			toolbarTitle.setText(R.string.change_cover);
			toolbarTitle.bringToFront();
		}
        
    	for (int i=0; i< profileCovers.length; i++) {
    		ImageView coverBtn = (ImageView)this.findViewById(profileCovers[i].getId());
    		if (coverBtn != null) {
    			coverBtn.getLayoutParams().width = coverImageDimen;
    			coverBtn.getLayoutParams().height = (int)(((float)coverImageDimen)/PixDefs.PROFILE_COVER_PHOTO_RATIO);
    			coverBtn.setImageResource(profileCovers[i].getDrawableRes());
    			coverBtn.setOnClickListener(new OnProfileCoverClickListener(i+1));
    		}
    	}
    }
    
    private class OnProfileCoverClickListener implements View.OnClickListener {
    	int coverIndex;
    	public OnProfileCoverClickListener(int coverIndex) {
    		this.coverIndex = coverIndex;
    	}
    	
		@Override
		public void onClick(View v) {
			mDelegate.onCoverPhotoChoosen(this.coverIndex);
			// update the server
			new UpdateCoverPicHttpRequest(null, this.coverIndex).send();
			dismiss();
		}
    }
}
