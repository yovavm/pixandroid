package com.pixplit.android.ui;


public interface ChatControllerInterface {
	public void registerChatDelegate(ChatControlDelegate delegate);
	public void unregisterChatDelegate();
}
