package com.pixplit.android.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter.PixBaseListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixObjectsList;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.UserFollowRequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.UserFollowHttpRequest;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;
import com.pixplit.android.ui.fragments.PixFbBaseFragment;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;

public abstract class PixListFragment<T> extends PixFbBaseFragment implements PixBaseListDelegate, 
																			ListView.RecyclerListener, 
																			RequestDelegate{

//	private static final String TAG = "PixListFragment";
	
	// Spinner
	View mSpinnerFrame = null;
	Animation spinnerAnim;
	
	// request attributes
	private String requestUrl = null;
	
	private ListView mListView = null;
	protected PixBaseListAdapter<?> mAdapter = null;
    protected View mHeader = null;
    
    // paging
    private String nextPage = "";
    private boolean allowPaging = false;
    
    // fetching type
    public static final int FEED_FETCH_TYPE_INVALID = 0;
    public static final int FEED_FETCH_TYPE_FIRST = 1;
    public static final int FEED_FETCH_TYPE_NEXT_PAGE = 2;
    public static final int FEED_FETCH_TYPE_REFRESH = 3;
    
    private boolean isLoading = false;
    
    // position restore
 	int firstVisibleItemIndex = 0;
 	int headerHeight = 0;
 	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spinnerAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.spinning_anim);
    }
    
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	initList();
		initSpinner();
    	if (mAdapter != null && !mAdapter.hasData()){
			startSpinner();
			fetchListData(FEED_FETCH_TYPE_FIRST);
		}
    }
    
    @Override
    public void onToolbarCenterClick() {
    	scrollToFirst();
    }
    
    private void initList() {
    	if (mFragmentView != null && getActivity() != null){
    		if (mListView == null) {
    			mListView = (ListView)mFragmentView.findViewById(R.id.feedsList);
    			if (mListView != null) {
	    			addHeaders();
	    			if (mAdapter == null) {
		    			mAdapter = createAdapter();
		    		}
		    		if (mAdapter != null){
						mListView.setAdapter(mAdapter);
					}
		    		
		    		mListView.setRecyclerListener(this);
	    		}
    			initNextPage();
	    	}
    	}
    }
        
    protected void initNextPage() {
    	nextPage = "";
    }
    
    public boolean pagingEnabled() {
    	return (allowPaging && nextPage != null);
    }
    
    public void loadNextPage() {
    	fetchListData(FEED_FETCH_TYPE_NEXT_PAGE);
    }
    
    protected boolean isFirstPage(Bundle requestContext) {
    	boolean isFirstPage = false;
    	if (requestContext != null) {
    		int feedFetchType = requestContext.getInt(PixDefs.REQ_CTX_FEED_FETCH_TYPE, FEED_FETCH_TYPE_INVALID);
    		if (feedFetchType == FEED_FETCH_TYPE_FIRST || feedFetchType == FEED_FETCH_TYPE_REFRESH) {
    			isFirstPage = true;
    		}
    	}
    	return isFirstPage;
    }
    
    public void onMovedToScrapHeap(View view) {
    	mAdapter.onHolderEnterRecycleBin(view);
    }
    
	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	
    protected void setAllowPaging(boolean allowPaging){
    	this.allowPaging = allowPaging;
    }
    
    protected String getNextPage(){
    	return nextPage;
    }
    
    protected void setNextPage(String nextPage) {
    	this.nextPage = nextPage;
    }
    
    public void refreshList() {
    	initNextPage();
		fetchListData(FEED_FETCH_TYPE_REFRESH);
    }
    
    protected boolean isRefreshRequest(Bundle requestContext) {
    	boolean isRefreshRequest = false;
    	if (requestContext != null) {
    		int feedFetchType = requestContext.getInt(PixDefs.REQ_CTX_FEED_FETCH_TYPE);
    		if (feedFetchType == FEED_FETCH_TYPE_REFRESH) {
    			isRefreshRequest = true;
    		}
    	}
    	return isRefreshRequest;
    }
    
    private void addHeaders() {
    	int hdrLayout = getHeaderLayout();
		if (hdrLayout != 0) {
			LayoutInflater li = (LayoutInflater)PixApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mHeader = li.inflate(hdrLayout, null);
			if (mHeader != null) {
				mListView.addHeaderView(mHeader);
			}
		}
    }

    // override by sub-class to insert list header 
    protected int getHeaderLayout() {
    	return 0; 
    }
        
    protected void updateAdapterData(ArrayList<?> data){
    	if (mAdapter != null) {
    		mAdapter.updateData(data);
    	}
    }
    
    protected void changeAdapterData(ArrayList<?> data){
    	if (mAdapter != null) {
    		mAdapter.changeData(data);
    	}
    }
    
    protected void setPosition(int position) {
      	if (mListView != null) {
    		mListView.setSelection(position);
    	}
    }
    
    private void fetchListData(int fetchType){
    	//Utils.pixLog(TAG, "fetching list");
    	if (!this.isLoading) {
    		BaseVolleyHttpRequest request = getRefreshRequest();
	    	if (request != null) {
	    		addNextParam(request);
	    		Bundle requestContext = request.getRequestContext();
	    		if (requestContext != null) {
	    			requestContext.putInt(PixDefs.REQ_CTX_FEED_FETCH_TYPE, fetchType);
	    		}
	    		this.isLoading = true;
		    	
	    		PixAppState.getState().getRequestQueue().add(request);
		    	
//		    	if (fetchType == FEED_FETCH_TYPE_REFRESH) {
		    		this.setInRefreshProcess(true);
//		    	}
	    	} else {
	    		// no request to send
	    		hideSpinner();
	    	}
    	}
    }
    
    // default next page behavior. can be override
    protected void addNextParam(BaseVolleyHttpRequest request) {
    	if (pagingEnabled() && Utils.isValidString(nextPage)) {
    		if (Utils.isNumeric(nextPage)) {
    			request.addParam(PixHttpAPI.PAGE_PARAM, nextPage);
    		} else {
    			// Cursor
    			request.addParam(PixHttpAPI.NEXT_PARAM, nextPage);
    		}
    	}
    }
    
    protected void scrollToFirst() {
		if (mListView != null &&
				mAdapter != null &&
				mAdapter.getCount() > 0) {
			if (mListView.getFirstVisiblePosition() > 5) {
				mListView.setSelection(5);
			}
			mListView.post(new Runnable() {
				@Override
				public void run() {
					mListView.smoothScrollBy(-10000, 2000);
				}
			});
		}
    }
	
	protected void jumpToLast() {
		if (mListView != null && 
				mAdapter != null &&
				mAdapter.getCount() > 0) {
			mListView.setSelection(mListView.getHeaderViewsCount() + mAdapter.getCount()-1);
		}
    }

    protected void scrollToLast() {
    	if (mListView != null && mAdapter != null) {
    		mListView.post(new Runnable() {
    	        @Override
    	        public void run() {
    	            // Select the last row so it will scroll into view...
    	        	mListView.smoothScrollToPosition(mListView.getHeaderViewsCount() + mAdapter.getCount() - 1);
    	        }
    	    });
    	}
    }
    
    
    protected void scrollToAfterHeader() {
    	int headersCount = mListView.getHeaderViewsCount();
		mListView.smoothScrollToPosition(headersCount);
    }

    // spinner
    protected void startSpinner(){
    	if (mSpinnerFrame != null){
    		mSpinnerFrame.setVisibility(View.VISIBLE);
    		View spinnerView = mSpinnerFrame.findViewById(R.id.spinner);
			if (spinnerView != null) {
				spinnerView.setVisibility(View.VISIBLE);
				spinnerView.startAnimation(spinnerAnim);
			}
    	}
	}
	
    private void initSpinner() {
    	// initialize the spinner
    	if (mFragmentView != null) {
            mSpinnerFrame = (View)mFragmentView.findViewById(R.id.spinnerFrame);
    	}
    	hideSpinner();
	}
    
    protected void hideSpinner(){
		if (mSpinnerFrame != null) {
			View spinnerView = mSpinnerFrame.findViewById(R.id.spinner);
			if (spinnerView != null) {
				spinnerView.setVisibility(View.GONE);
				spinnerView.setAnimation(null);
			}
        	mSpinnerFrame.setVisibility(View.GONE);
        }
	}
	
	protected abstract BaseVolleyHttpRequest getRefreshRequest();
	
	protected abstract PixBaseListAdapter<?> createAdapter();
	
	protected abstract void handleListResponse(PixObject response, Bundle requestContext);
	
	@Override
	public void onCompImageClick(final PixComposition comp) {
		this.onCompJoinClick(comp);
	}
	
	@Override
	public void onCompJoinClick(final PixComposition comp) {
		if (!this.verifyUser()) return;
		if (PixComposition.isJoinSplitAllow(comp)) {
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_JOINED_OPEN_SPLIT_PRESSED, null);
			
			final Bundle args = new Bundle();
			args.putInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, comp.getCompType());
			comp.serialize(args);
			
			// fetch the joined split image and start the create process
			PixImagesLoader.loadImage(comp.getImage(), 
									new BitmapFetcherDelegate() {
										@Override
										public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapHolder) {
											if (bitmapHolder != null) {
												if (mActivitiesController != null) {
													mActivitiesController.gotoCreateNewSplit(getActivity(), args, bitmapHolder);
												}
											}
										}
										@Override
										public void onBitmapFetchFailed() {
											// Failed to fetch the split image. Do nothing - can't join.
										}
									}, Priority.IMMEDIATE);
		}
	}
	
	@Override
	public void onCompImageClick(String compId) {
		// default is to do nothing
	}
	
	@Override
	public void onProfileClick(String username) {
		if (Utils.isValidString(username)) {
			Bundle args = new Bundle();
			args.putString(PixDefs.FRAG_ARG_USERNAME, username);
			if (mActivitiesController != null) {
				mActivitiesController.gotoUserProfile(getActivity(), args, 0);
			}
		}
	}
	
	@Override
	public void onCompLikeClick(PixComposition comp, boolean liked) {
    	// default is to do nothing
    }
    @Override
	public void onCompCommentClick(PixComposition comp, CompCommentsUpdateDelegate delegate) {
    	// default is to do nothing
    }
    @Override
    public void onCompRelatedClick(PixComposition comp) {
    	// default is to do nothing
    }
    @Override
    public void onCompResplitClick(PixComposition comp) {
    	// default is to do nothing
    }
    @Override
	public void deleteCompRequest(String compId) {
    	// default is to do nothing
    }
    @Override
    public void onTagClick(String tag) {
    	if (!Utils.isValidString(tag)) return;
    	HashMap<String, String> context = new HashMap<String, String>();
		context.put(PixDefs.APP_PARAM_TAG_NAME, tag);
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SEARCH_BY_TAG, PixDefs.APP_EVENT_SEARCH_DONE, context);
    	
    	Bundle args = new Bundle();
        args.putString(PixDefs.FRAG_ARG_TAG, tag);
    	if (mActivitiesController != null) {
    		mActivitiesController.gotoThemeFeed(getActivity(), args);
    	}
    }
    @Override
	public void onUserFollowClick(PixUser user) {
    	if (!this.verifyUser()) return;
    	if (user != null) {
			UserFollowRequestDelegate followDelegate = new UserFollowRequestDelegate(user);
			int followStatus = user.getFollowStatus();
			// verification - should not get here if user is the logged-in user
			if (followStatus != PixUser.FOLLOW_STATUS_LOGGED_IN_USER) {
				PixAppState.getState().getRequestQueue().add(new UserFollowHttpRequest(followDelegate, user, followStatus));
				// update the data without waiting for the server response 
				user.setFollowStatus((followStatus==PixUser.FOLLOW_STATUS_NOT_FOLLOWING) ? PixUser.FOLLOW_STATUS_FOLLOWING : PixUser.FOLLOW_STATUS_NOT_FOLLOWING);
			}
		
    	}
    }
    @Override
    public void onCompLikersClick(PixComposition comp) {
    	if (Utils.isValidString(comp.getCompId())) {
    		Bundle args = new Bundle();
            args.putInt(PixDefs.FRAG_ARG_USERS_FRAG_TYPE, UsersListFragment.FRAG_TYPE_LIKERS);
            args.putString(PixDefs.FRAG_ARG_USERS_CONTEXT, comp.getCompId());
            if (mActivitiesController != null) {
            	mActivitiesController.gotoUsersList(getActivity(), args);
            }
    	}
    }
    
    @Override
    public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
    	this.isLoading = false;
    	if (request == null) {
    		requestFinishedWithError(request, new PixError(PixError.PixErrorCodeInvalidRequest));
    	}
    	Bundle requestContext = request.getRequestContext();
    	boolean isRefreshRequest = isRefreshRequest(requestContext);
    	if (response != null && response instanceof PixObjectsList<?>) {
    		PixObjectsList<?> listResponse = (PixObjectsList<?>)response;
	    	// get the next page number/curser
	    	if (pagingEnabled() || isRefreshRequest){
	    		String responseNext = listResponse.getNext();
	    		if (responseNext == null || 
	    				responseNext.equalsIgnoreCase(nextPage) ||
	    				responseNext.equalsIgnoreCase("0")) {
	    			nextPage = null; // no next page
	    		} else {
	    			nextPage = responseNext;	    			
	    		}
	    	}
	    	if (isRefreshRequest){
	    		this.changeAdapterData(null);
	    	}
	    	this.handleListResponse(response, requestContext);
	    	if (isRefreshRequest){
	    		onRefreshRequestDone(requestContext);
	    	}
    		this.setInRefreshProcess(false);
	    	// the list might be invisible in case there was an error on last load
	    	if (mListView.getVisibility() == View.INVISIBLE) {
	    		mListView.setVisibility(View.VISIBLE);
	    	}
	    	
    	} else {
    		requestFinishedWithError(request, new PixError(PixError.PixErrorCodeInvalidResponseData));
    	}
    	
    	hideSpinner();
    }
    
    @Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
    	this.isLoading = false;
		//Utils.pixLog(TAG, "http request finished with errors");
		hideSpinner();
		if (mListView != null &&
				mListView.getVisibility() == View.VISIBLE &&
				mAdapter != null &&
				(mAdapter.getData() == null ||
				 mAdapter.getData().isEmpty())) {
			mListView.setVisibility(View.INVISIBLE);
		}
			
//		boolean isRefreshRequest = isRefreshRequest(request.getRequestContext());
//		if (isRefreshRequest){
			this.setInRefreshProcess(false);
//    	}
		if (mActivity != null) {
			mActivity.onError(error);
		}
	}
    
    protected void onRefreshRequestDone(Bundle requestContext) {
    	if (mListView != null && 
				mAdapter != null &&
				mAdapter.getCount() > 0) {
    		mListView.setSelection(0);
    	}
	}
    
    public void storeListPosition() {
    	if (mListView != null) {
	    	// save index and top position
	    	firstVisibleItemIndex = mListView.getFirstVisiblePosition();
	    	if (mHeader != null) {
    			headerHeight = mHeader.getHeight();
    		}
    	}
	}

    public void restoreListPosition(int itemsOffset) {
    	if (mListView != null) {
    		// restore
    		mListView.setSelectionFromTop(mListView.getHeaderViewsCount() + 
    				firstVisibleItemIndex + itemsOffset, headerHeight);
    	}
    }
    
	@Override
	public void onActionBtnRefreshPressed() {
		refreshList();		
	}
	
	@Override
	public Context getContext() {
		return getActivity();
	}

//	protected void removeListItem(final int index, Animation removalAnim) {
//		if (mListView == null || mListView.getChildCount() > index) return;
//		if (removalAnim != null) {
//			removalAnim.setAnimationListener(new AnimationListener() {
//				
//				@Override
//				public void onAnimationStart(Animation animation) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				@Override
//				public void onAnimationRepeat(Animation animation) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				@Override
//				public void onAnimationEnd(Animation animation) {
//					if (mAdapter != null) {
//						mAdapter.removeItemAtIndex(index);
//					}
//				}
//			});			
//			mListView.getChildAt(index).startAnimation(removalAnim);
//		}
//	}
}
