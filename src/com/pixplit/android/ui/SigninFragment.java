package com.pixplit.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixAccountManager;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.SignInHttpRequest;
import com.pixplit.android.ui.elements.PixAlertDialog;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.billing.PixBilling;


public class SigninFragment extends PixBaseFragment {
  	
	EditText emailText;
	ImageView emailIcon;
	EditText passwordText;
	ImageView passwordIcon;
	
	// validation stuff
	private static final int EMAIL_INVALID_MAIL = 1;
	
	private BillingHelperController mIabController;
	
	public static SigninFragment newInstance() {
		SigninFragment fragment = new SigninFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.SIGNIN);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.sign_in_layout, container, false);
    	if (mFragmentView != null) {
    		RelativeLayout enterEmail = (RelativeLayout)mFragmentView.findViewById(R.id.enterEmail);
    		if (enterEmail != null) {
    			emailText = (EditText)enterEmail.findViewById(R.id.emailText);
    			emailIcon = (ImageView)enterEmail.findViewById(R.id.emailIcon);
    			if (emailText != null) {
    				String accountEmail = PixAccountManager.getFirstAcountName(PixAccountManager.PIX_GOOGLE_ACCOUNT);
        			if (accountEmail != null) {
        				emailText.setText(accountEmail);
        			}
        			
    				emailText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				emailText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    				emailText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (emailIcon != null) {
								emailIcon.setBackgroundResource(R.drawable.account_email_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				emailText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								if (validateEmailAddress()) {
									passwordText.requestFocus();
								}
								return true;
							}
							return false;
						}
					});
    				emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								if (validateEmailAddress()) {
									if (passwordText != null) {
										// set focus on next field.
									}
								}
							}
						}
					});
    			}
    		}
    		
    		RelativeLayout enterPassword = (RelativeLayout)mFragmentView.findViewById(R.id.enterPassword);
    		if (enterPassword != null) {
    			passwordText = (EditText)enterPassword.findViewById(R.id.passwordText);
    			passwordIcon = (ImageView)enterPassword.findViewById(R.id.passwordIcon);
    			if (passwordText != null) {
    				passwordText.setImeOptions(EditorInfo.IME_ACTION_DONE);
    				passwordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
    				passwordText.setTransformationMethod(new PasswordTransformationMethod());
    				passwordText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (passwordIcon != null) {
								passwordIcon.setBackgroundResource(R.drawable.account_password_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				passwordText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_DONE) {
								if (validatePassword()) {
									signInRequest();
								}
								return true;
							}
							return false;
						}
					});
    				passwordText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validatePassword();
							}
						}
					});
    			}
    		}
    		
    		TextView forgotPasswordBtn = (TextView) mFragmentView.findViewById(R.id.forgotPasswordBtn);
    		if (forgotPasswordBtn != null) {
    			forgotPasswordBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_LOGIN, PixDefs.APP_EVENT_FORGOT_PASSWORD_PRESSED, null);
						forgotPasswordRequest();
					}
				});
    		}
    		
    		TextView createAccountBtn = (TextView) mFragmentView.findViewById(R.id.createAccountBtn);
    		if (createAccountBtn != null) {
    			createAccountBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						createAccountRequest();
					}
				});
    		}
    		
    		TextView signInBtn = (TextView)mFragmentView.findViewById(R.id.signInBtn);
    		if (signInBtn != null) {
    			signInBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_LOGIN, PixDefs.APP_EVENT_SIGN_IN_PRESSED, null);
						signInRequest();
					}
				});
    		}
    	}
    	return mFragmentView;
    }

    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mIabController = (BillingHelperController)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement BillingHelperController");
	    }
    }
    
    @Override
    public void onDetach () {
    	super.onDetach();
    	mIabController = null;
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_LOGIN, PixDefs.APP_EVENT_CANCEL_PRESSED, null);
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }
    
    private void forgotPasswordRequest() {
		if (mActivitiesController != null) {
			mActivitiesController.gotoResetPassword(getActivity());
		}
    }
    
    private void createAccountRequest() {
    	// start sing-up activity with no-history in order to close current activity
		if (mActivitiesController != null) {
			mActivitiesController.gotoSignUp(getActivity());
		}
    }
    
    private void signInRequest() {
    	if (accountDataIsValid()) {
    		PixUserInfo userInfo = new PixUserInfo();
    		userInfo.setEmail(getEmailText());
    		userInfo.setPassword(getPasswordText());
    		
    		this.signinAtPixServer(userInfo);
    	}
    }
    
    private void signinAtPixServer(PixUserInfo userInfo) {
    	PixProgressIndicator.showMessage(getActivity(), 
				PixApp.getContext().getResources().getString(R.string.signing_up), true);
		if (userInfo != null) {
			new SignInHttpRequest(
							new AuthRequestDelegate(getActivity(), AuthRequestDelegate.AUTH_PROCESS_TYPE_SIGNIN) {
							@Override
							public void onAuthFinished() {
								signinAtPixServerFinished();
							}
							@Override
							public void onAuthFailed() {
								signinAtPixServerFailed();
							}
						}
						, userInfo).send();
		} else {
			signinAtPixServerFailed();
		}
	}
    
    private void signinAtPixServerFinished() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_LOGIN, PixDefs.APP_EVENT_SUCCESSFUL_LOGIN, null);
		if (mIabController != null) {
			PixBilling.queryInventoryAsync(mIabController.getIabHelper());
		}
		PixProgressIndicator.hideMessageWithIndication(true, 
				PixApp.getContext().getResources().getString(R.string.authenticated));
		Bundle args = new Bundle();
		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, true);
		if (mActivitiesController != null) {
			if (mActivity != null) {
				mActivity.finishFragment();
			}
			mActivitiesController.gotoFollowFriends(getActivity(), args);
		}
	}
    
    private void signinAtPixServerFailed() {
		PixProgressIndicator.hideMessageWithIndication(false, 
				PixApp.getContext().getResources().getString(R.string.failed));
		if (getActivity() != null) {
			new PixAlertDialog(getActivity(), 
					null, 
					PixApp.getContext().getString(R.string.failed_to_log_in),
					PixApp.getContext().getString(R.string.invalid_email___password_please_try_again), 
					PixApp.getContext().getResources().getString(R.string.ok)).show();
		}
	}
    
    private boolean accountDataIsValid() {
    	// email
    	if (!validateEmailAddress()) {
    		return false;
    	}
    	
    	// password
    	if (!validatePassword()) {
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean validateEmailAddress() {
    	
    	String email = getEmailText();
    	if (Utils.isValidString(email)) {
    		if (!Utils.isValidEmail(email)) {
    			onInvalidEmail(EMAIL_INVALID_MAIL);
    			return false;
    		} else {
    			return true;
    		}
    	} else {
			onInvalidEmail(EMAIL_INVALID_MAIL);
    	}
    	
    	return false;
    }

    private boolean validatePassword() {
    	// the password is considered as not valid until validation is done
    	boolean passwordIsValid = false;
    	
    	String password = getPasswordText();
    	if (Utils.isValidString(password)) {
    		if (password.length() < 6) {
    			Utils.showMessage(PixApp.getContext().getResources().getString(R.string.password_must_be_at_least_6_characters), true);
    		} else {
	    		// password is valid
    			passwordIsValid = true;
    		}
    	} else {
    		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.password_must_be_at_least_6_characters), true);
    	}
    	
    	updatePasswordIcon(passwordIsValid);
    	return passwordIsValid;
    }
    
    private String getEmailText() {
    	if (emailText != null && emailText.getText() != null) {
    		return emailText.getText().toString();
    	}
    	return null;
    }
    
    private String getPasswordText() {
    	if (passwordText != null && passwordText.getText() != null) {
    		return passwordText.getText().toString();
    	}
    	return null;
    }
    
    private void onInvalidEmail(int invalidCode) {
    	if (invalidCode == EMAIL_INVALID_MAIL) {
    		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.please_enter_a_valid_email), true);
    	}
    	
    	updateMailIcon(false);
    }
    
    private void updateMailIcon(boolean valid) {
    	if (emailIcon != null) {
    		if (valid) {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon);
    		} else {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_red);
    		}
    	}
    }
    
    private void updatePasswordIcon(boolean valid) {
    	if (passwordIcon != null) {
    		if (valid) {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon);
    		} else {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon_red);
    		}
    	}
    }
	
}
