package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.UsersListAdapter;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.data.PixUsers;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.UsersFeedHttpRequest;
import com.pixplit.android.ui.elements.SearchUsers;


public class UsersListFragment extends PixListFragment<PixUsers>{

	public static final int FRAG_TYPE_FOLLOWING = 0;
	public static final int FRAG_TYPE_FOLLOWERS = 1;
	public static final int FRAG_TYPE_LIKERS = 2;
	
	int fragType;
	String usersContext;
	SearchUsers mSearchUsers;
	
	public static UsersListFragment newInstance(Bundle args) {
		UsersListFragment fragment = new UsersListFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	static UsersListFragment newInstance(int type, String usersContext) {
		UsersListFragment fragment = new UsersListFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        b.putInt(PixDefs.FRAG_ARG_USERS_FRAG_TYPE, type);
        b.putString(PixDefs.FRAG_ARG_USERS_CONTEXT, usersContext);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
        	fragType = args.getInt(PixDefs.FRAG_ARG_USERS_FRAG_TYPE);
        	usersContext = args.getString(PixDefs.FRAG_ARG_USERS_CONTEXT);
        }
        
        setAllowPaging(true);
        
        switch(fragType) {
        case FRAG_TYPE_FOLLOWING:{
            setFragmentType(EFragmentType.FOLLOWING);
            if (mToolbarController != null) {
            	mToolbarController.setToolBarTitle(PixApp.getContext().getString(R.string.following_caps));
            }
            setRequestUrl(PixHttpAPI.USER_FOLLOWING_URL_PREFIX + usersContext);
        	break;
        }
		case FRAG_TYPE_FOLLOWERS:{
            setFragmentType(EFragmentType.FOLLOWERS);
            if (mToolbarController != null) {
            	mToolbarController.setToolBarTitle(PixApp.getContext().getString(R.string.followers_caps));
            }
            setRequestUrl(PixHttpAPI.USER_FOLLOWERS_URL_PREFIX + usersContext);
        	break;
        }
		case FRAG_TYPE_LIKERS:{
            setFragmentType(EFragmentType.LIKERS);
            if (mToolbarController != null) {
            	mToolbarController.setToolBarTitle(PixApp.getContext().getString(R.string.likers_caps));
            }
            setRequestUrl(PixHttpAPI.COMP_LIKERS_URL_PREFIX + usersContext);
			break;
		}
		default: break;
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.users_layout, container, false);
		
		addSearchUsersHeader();
		
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixUsers) {
			PixUsers users = (PixUsers)response;
			
			ArrayList<PixUser> usersFeed = (ArrayList<PixUser>) users.getItems();
			updateAdapterData(usersFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	BaseVolleyHttpRequest request = new UsersFeedHttpRequest(this, getRequestUrl());
    	
		return request;
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	UsersListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new UsersListAdapter(getActivity(), this, R.layout.user_layout, UsersListAdapter.USERS_LIST_ACTION_FOLLOW, null);
        	mSearchUsers.assignAdapter(adapter);
    	}
    	return adapter;
	}
        
    protected void addSearchUsersHeader() {
    	if (getActivity() != null) {
	    	mSearchUsers = new SearchUsers(getActivity());
			if (mSearchUsers != null && 
					mFragmentView instanceof LinearLayout) {			
				((LinearLayout)mFragmentView).addView(mSearchUsers, 0);
			}
    	}
    }
}
