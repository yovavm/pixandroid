package com.pixplit.android.ui;

import android.app.Activity;

import com.pixplit.android.PixApp;
import com.pixplit.android.data.PixAuthUserInfo;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.ui.activities.PixBaseActivity;

public abstract class AuthRequestDelegate implements RequestDelegate{
	
	public static final int AUTH_PROCESS_TYPE_FB = 1;
	public static final int AUTH_PROCESS_TYPE_SIGNUP = 2;
	public static final int AUTH_PROCESS_TYPE_SIGNIN = 3;
	public static final int AUTH_PROCESS_TYPE_GPLUS = 4;
	
	int authProcessType;
	Activity mActivity;
	
	public AuthRequestDelegate(Activity activity, int authProcessType) {
		this.mActivity = activity;
		this.authProcessType = authProcessType;
	}
	
	@Override
	public void requestFinished(BaseHttpRequest request, PixObject response) {
		if (response != null && response instanceof PixAuthUserInfo) {
			// save the user information
			PixAuthUserInfo authUserInfo = (PixAuthUserInfo) response;
			PixAppState.UserInfo.setUserToken(authUserInfo.getSessionToken());
			PixAppState.UserInfo.setUserName(authUserInfo.getUsername());
			PixAppState.UserInfo.setUserFullName(authUserInfo.getFullName());
			// restart/init the push notifications
			PixApp.enablePushNotifications();
            
			this.onAuthFinished();
		} else {
			this.onAuthFailed();
		}
	}
	
	@Override
	public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
		this.onAuthFailed();
		if (mActivity != null && mActivity instanceof PixBaseActivity) {
			((PixBaseActivity)mActivity).onError(error);
		}
	}
	
	public abstract void onAuthFinished();
	public abstract void onAuthFailed();

}
