package com.pixplit.android.ui;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.ui.elements.PixDialog;
import com.pixplit.android.util.Utils;

public class AdminSetAccountMaxCampaigns extends PixDialog {

	SetMaxCampaignsDelegate mDelegate;
	
	public interface SetMaxCampaignsDelegate {
		public void onLimitSet(int limit);
	}
	
	public AdminSetAccountMaxCampaigns(Context context, SetMaxCampaignsDelegate delegate, int initLimit) {
		super(context, R.layout.admin_set_account_limit_dialog);
		this.mDelegate = delegate;
		
		LinearLayout setLimitLayout = (LinearLayout)findViewById(R.id.setLimitLayout);
		if (setLimitLayout != null) {
			setLimitLayout.setPadding(60, 60, 60, 60);
			
			final EditText limitView = (EditText)findViewById(R.id.accountCampaignsLimit);
			if (limitView != null) {
				limitView.setHint(Integer.valueOf(initLimit).toString());
			}
		
			TextView cancel = (TextView)findViewById(R.id.cancelNewLimit);
			if (cancel != null) {
				cancel.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dismiss();
					}
				});
			}
			
			TextView set = (TextView)findViewById(R.id.setNewLimit);
			if (set != null) {
				set.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (limitView == null || limitView.getText() == null) return;
						String newLimitString = limitView.getText().toString();
						if (!Utils.isValidString(newLimitString)) return;
						if (!Utils.isNumeric(newLimitString)) {
							Utils.showMessage("Limit must be a number!", true);
							return;
						}
						int newLimit = Integer.parseInt(newLimitString);
						if (mDelegate != null) {
							mDelegate.onLimitSet(newLimit);
							dismiss();
						}
					}
				});
			}
		
		}
	}
	
}
