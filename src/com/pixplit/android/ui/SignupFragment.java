package com.pixplit.android.ui;

import java.util.HashMap;
import java.util.regex.Pattern;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixAccountManager;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixEmailValidation;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.SignupHttpRequest;
import com.pixplit.android.http.request.ValidateEmailHttpRequest;
import com.pixplit.android.ui.AccountPhotoDialog.AccountPhotoDialogDelegate;
import com.pixplit.android.ui.activities.PixPhotoPickerInterface;
import com.pixplit.android.ui.elements.ItemsChooser;
import com.pixplit.android.ui.elements.ItemsChooser.ItemsChooserDelegate;
import com.pixplit.android.ui.elements.PixAlertDialog;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;
import com.pixplit.android.util.Utils;


public class SignupFragment extends PixBaseFragment implements AccountPhotoDialogDelegate, 
																PhotoFetcherDelegate{
//  	private static final String TAG = "SignupFragment";
	
	EditText emailText;
	ImageView emailIcon;
	View emailValidationSpinner;
	Animation spinnerAnim;
	RelativeLayout addPhoto;
	ImageView addPhotoBtn;
	EditText passwordText;
	ImageView passwordIcon;
	EditText fullnameText;
	ImageView fullnameIcon;
	Bitmap accountPhoto;
	ItemsChooser emailsChooser;
	String accountsEmail[];
	
	// validation stuff
	private static final int EMAIL_INVALID_MAIL = 1;
	private static final int EMAIL_INVALID_EXISTS = 2;
	String validatedEmail;
	
	// restore stuff
	private static final String PROFILE_PICTURE_TEMP_FILE_NAME = "profile_temp_file";
	private static final String EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH = "EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH";
	
	private PixPhotoPickerInterface mPhotoPickActivity;
	
	public static SignupFragment newInstance() {
		SignupFragment fragment = new SignupFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.SIGNUP);
        spinnerAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.spinning_anim);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.sign_up_layout, container, false);
    	if (mFragmentView != null) {
    		RelativeLayout enterEmail = (RelativeLayout)mFragmentView.findViewById(R.id.enterEmail);
    		if (enterEmail != null) {
    			emailText = (EditText)enterEmail.findViewById(R.id.emailText);
    			emailIcon = (ImageView)enterEmail.findViewById(R.id.emailIcon);
    			emailValidationSpinner = enterEmail.findViewById(R.id.emailValidationSpinner);
    			if (emailText != null) {
    				accountsEmail = PixAccountManager.getAcountsNames(PixAccountManager.PIX_GOOGLE_ACCOUNT);
        			if (accountsEmail != null && accountsEmail.length > 0) {
        				if (accountsEmail.length == 1) {
        					emailText.setText(accountsEmail[0]);
        				} else {
        					emailsChooser = new ItemsChooser(getActivity());
        					emailsChooser.setItems(accountsEmail);
        					emailsChooser.setDelegate(new ItemsChooserDelegate() {
								@Override
								public void onItemChoosen(String itemText) {
									if (emailText != null) {
										emailText.setText(itemText);
									}
									if (passwordText != null) {
										passwordText.requestFocus();
									}
								}
							});
        					LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, 
        														   LayoutParams.WRAP_CONTENT);
        					params.addRule(RelativeLayout.BELOW, R.id.enterEmail);
        					params.addRule(RelativeLayout.ALIGN_LEFT, R.id.enterEmail);
        					params.setMargins(110, -20, 0, 0);
        					((RelativeLayout)mFragmentView).addView(emailsChooser, params);
        				}
        			}
        			
    				emailText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				emailText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    				emailText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {
							if (s == null || emailsChooser == null || accountsEmail == null) return;
							emailsChooser.autoComplete(s.toString());
						}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (emailIcon != null) {
								emailIcon.setBackgroundResource(R.drawable.account_email_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				emailText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								validateEmailAddress();
								if (passwordText != null) {
									passwordText.requestFocus();
								}
								return true;
							}
							return false;
						}
					});
    				emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validateEmailAddress();
								if (emailsChooser != null) {
									emailsChooser.setVisibility(View.GONE);
								}
							} else {
								if (emailsChooser != null) {
									emailsChooser.setVisibility(View.VISIBLE);
								}
							}
						}
					});
    			}
    		}
    		
    		RelativeLayout enterPassword = (RelativeLayout)mFragmentView.findViewById(R.id.enterPassword);
    		if (enterPassword != null) {
    			passwordText = (EditText)enterPassword.findViewById(R.id.passwordText);
    			passwordIcon = (ImageView)enterPassword.findViewById(R.id.passwordIcon);
    			if (passwordText != null) {
    				passwordText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				passwordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
    				passwordText.setTransformationMethod(new PasswordTransformationMethod());
    				passwordText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (passwordIcon != null) {
								passwordIcon.setBackgroundResource(R.drawable.account_password_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				passwordText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								if (validatePassword()) {
									if (fullnameText != null) {
										fullnameText.requestFocus();
									}
								}
								return true;
							}
							return false;
						}
					});
    				passwordText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validatePassword();
							}
						}
					});
    			}
    		}
    		
    		RelativeLayout enterFullname = (RelativeLayout)mFragmentView.findViewById(R.id.enterFullname);
    		if (enterFullname != null) {
    			fullnameText = (EditText)enterFullname.findViewById(R.id.fullnameText);
    			fullnameIcon = (ImageView)enterFullname.findViewById(R.id.fullnameIcon);
    			if (fullnameText != null) {
    				fullnameText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				fullnameText.setInputType(EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    				fullnameText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (fullnameIcon != null) {
								fullnameIcon.setBackgroundResource(R.drawable.account_name_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				fullnameText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								if (validateFullname()) {
									if (addPhoto != null) {
										addPhoto.performClick();
									}
								}
								return true;
							}
							return false;
						}
					});
    				fullnameText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validateFullname();
							}
						}
					});
    			}
    			
    			addPhoto = (RelativeLayout) mFragmentView.findViewById(R.id.addPhoto);
    			if (addPhoto != null) {
    				addPhoto.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							onAccountPhotoClick();
						}
					});
					addPhotoBtn = (ImageView) addPhoto.findViewById(R.id.addPhotoBtn);
    				if (addPhotoBtn != null) {
    					if (savedInstanceState != null) {
    						String filePath = savedInstanceState.getString(EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH);
    						Bitmap storedBitmap = Utils.getBitmapFromFile(filePath, 
    								PixDefs.PROFILE_BITMAP_SCALE_WIDTH, PixDefs.PROFILE_BITMAP_SCALE_HIGHT);
    						if (storedBitmap != null) {
    							accountPhoto = storedBitmap;
    							addPhotoBtn.setImageBitmap(storedBitmap);
    							Utils.deleteBitmapFile(filePath);
    						}
    					}
    				}
    			}
    		}
    		
    		TextView createAccountBtn = (TextView)mFragmentView.findViewById(R.id.createAccountBtn);
    		if (createAccountBtn != null) {
    			createAccountBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						createAccountRequest();
					}
				});
    		}
    	}
    	return mFragmentView;
    }

    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mPhotoPickActivity = (PixPhotoPickerInterface)activity;
	    } catch (ClassCastException e) {
	    	//Utils.pixLog(TAG, activity.toString() +" must implement PixPhotoPickerInterface");
	    	throw new ClassCastException(activity.toString() +" must implement PixPhotoPickerInterface");
	    }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.registerPhotoPickerDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.unregisterPhotoPickerDelegate();
    	}
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SIGN_UP, PixDefs.APP_EVENT_CANCEL_PRESSED, null);
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }

    private void createAccountRequest() {
    	if (accountDataIsValid()) {
    		// send sign up request
    		PixUserInfo userInfo = new PixUserInfo();
    		userInfo.setFullName(getFullnameText());
    		userInfo.setEmail(getEmailText());
    		userInfo.setPassword(getPasswordText());
    		if (accountPhoto != null) {
    			Bitmap scaledAccountPhoto = Utils.scaleBitmap(accountPhoto, 
    					PixDefs.PROFILE_BITMAP_SCALE_WIDTH,
    					PixDefs.PROFILE_BITMAP_SCALE_HIGHT);
    			accountPhoto = scaledAccountPhoto;
    		}
    		userInfo.setUserPhoto(accountPhoto);

    		HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARAM_WITH_PHOTO, (accountPhoto != null)?"1":"0");
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_SIGN_UP, PixDefs.APP_EVENT_DONE_PRESSED, context);

			this.signupAtPixServer(userInfo);
    	} 
    }
    
    private boolean accountDataIsValid() {
    	// email
    	if (!validateEmailAddress()) {
    		return false;
    	}
    	
    	// password
    	if (!validatePassword()) {
    		return false;
    	}
    	
    	// full name
    	if (!validateFullname()) {
    		return false;
    	}
    	return true;
    }
    
    private boolean validateEmailAddress() {
    	
    	String email = getEmailText();
    	if (Utils.isValidString(email)) {
    		if (validatedEmail != null && email.equalsIgnoreCase(validatedEmail)) {
    			return true;
    		} else {
	    		if (!Utils.isValidEmail(email)) {
	    			onInvalidEmail(EMAIL_INVALID_MAIL);
	    			return false;
	    		}
	    		// check if the mail exists in the server
	    		if (emailValidationSpinner != null) {
	    			emailValidationSpinner.setVisibility(View.VISIBLE);
	    			emailValidationSpinner.startAnimation(spinnerAnim);
	    		}
	    		new ValidateEmailHttpRequest(email, new RequestDelegate() {
					@Override
					public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
						if (emailValidationSpinner != null) {
							emailValidationSpinner.setVisibility(View.GONE);
							emailValidationSpinner.setAnimation(null);
						}
						//Utils.pixLog(TAG, "validate email request failed");
						int errCode = -1;
						if (error != null) {
							errCode = error.getCode();
						}
						if (errCode == PixError.PIX_SERVER_ERROR_INVALID_EMAIL) {
							onInvalidEmail(EMAIL_INVALID_MAIL);
						} else if (errCode == PixError.PIX_SERVER_ERROR_EMAIL_EXIST) {
							onInvalidEmail(EMAIL_INVALID_EXISTS);
						} else {
							//Utils.pixLog(TAG, "Request finished with error:" + errCode);
							onInvalidEmail(EMAIL_INVALID_MAIL);
						}
					}
					@Override
					public void requestFinished(BaseHttpRequest request, PixObject response) {
						if (emailValidationSpinner != null) {
							emailValidationSpinner.setVisibility(View.GONE);
							emailValidationSpinner.setAnimation(null);
						}
						if (response != null && response instanceof PixEmailValidation) {
							PixEmailValidation validateEmailResponse = (PixEmailValidation)response; 
							// if we got here the email was validated in the server
							updateMailIcon(true);
							validatedEmail = validateEmailResponse.getEmail();
						}
					}
				}).send();
    		}
    	} else {
			onInvalidEmail(EMAIL_INVALID_MAIL);
    	}
    	
    	return false;
    }

    private boolean validatePassword() {
    	// the password is considered as not valid until validation is done
    	boolean passwordIsValid = false;
    	
    	String password = getPasswordText();
    	if (Utils.isValidString(password)) {
    		if (password.length() < 6) {
    			Utils.showMessage(PixApp.getContext().getResources().getString(R.string.password_must_be_at_least_6_characters), true);
    		} else {
	    		Pattern passwordPattern = Pattern.compile("^[a-zA-Z0-9]+$");
	    		if (passwordPattern.matcher(password).matches()) {
	    			// password is valid
	    			passwordIsValid = true;
	    		} else {
	    			Utils.showMessage(PixApp.getContext().getResources().getString(R.string.please_use_letters_a_z_or_numbers_0_9_), true);
	    		}
    		}
    	} else {
    		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.password_must_be_at_least_6_characters), true);
    	}
    	
    	updatePasswordIcon(passwordIsValid);
    	return passwordIsValid;
    }
    
    private boolean validateFullname() {
    	boolean fullnameIsValid = false;
    	
    	String fullname = getFullnameText();
    	if (Utils.isValidString(fullname)) {
    		fullnameIsValid = true;
    	} else {
			Utils.showMessage(PixApp.getContext().getResources().getString(R.string.please_fill_in_your_details), true);
		}
    	
    	updateFullnameIcon(fullnameIsValid);
    	return fullnameIsValid;
    }
    
    
    private String getEmailText() {
    	if (emailText != null && emailText.getText() != null) {
    		return emailText.getText().toString();
    	}
    	return null;
    }
    
    private String getPasswordText() {
    	if (passwordText != null && passwordText.getText() != null) {
    		return passwordText.getText().toString();
    	}
    	return null;
    }
    
    private String getFullnameText() {
    	if (fullnameText != null && fullnameText.getText() != null) {
    		return fullnameText.getText().toString();
    	}
    	return null;
    }
    
    private void onInvalidEmail(int invalidCode) {
    	validatedEmail = null;
    	if (invalidCode == EMAIL_INVALID_MAIL) {
    		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.please_enter_a_valid_email), true);
    	} 
    	if (invalidCode == EMAIL_INVALID_EXISTS) {
    		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.a_user_with_that_email_already_exists), true);
    	}
    	updateMailIcon(false);
    }
    
    private void updateMailIcon(boolean valid) {
    	if (emailIcon != null) {
    		if (valid) {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_green);
    		} else {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_red);
    		}
    	}
    }
    
    private void updatePasswordIcon(boolean valid) {
    	if (passwordIcon != null) {
    		if (valid) {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon_green);
    		} else {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon_red);
    		}
    	}
    }
    
    private void updateFullnameIcon(boolean valid) {
    	if (fullnameIcon != null) {
    		if (valid) {
    			fullnameIcon.setBackgroundResource(R.drawable.account_name_icon_green);
    		} else {
    			fullnameIcon.setBackgroundResource(R.drawable.account_name_icon_red);
    		}
    	}
    }
    
    private void onAccountPhotoClick() {
    	if (getActivity() != null) {
    		new AccountPhotoDialog(getActivity(), this).show();
    	}
	}

	// account photo delegate
	@Override
	public boolean hasAccountPhoto() {
		return (accountPhoto != null);
	}

	@Override
	public void takePhoto() {
		if (mPhotoPickActivity != null) {
			mPhotoPickActivity.photoCaptureRequest();
		}
	}

	@Override
	public void pickPhotoFromLib() {
		if (mPhotoPickActivity != null) {
			mPhotoPickActivity.photoPickRequest();
		}
	}

	@Override
	public void removePhoto() {
		accountPhoto = null;
		if (addPhotoBtn != null) {
			addPhotoBtn.setImageBitmap(null);
		}

	}

	// photo picker delegate
	@Override
	public void onPhotoFetchSuccess(Bitmap selectedImage) {
		if (selectedImage != null) {
			accountPhoto = selectedImage;
			if (addPhotoBtn != null) {
				addPhotoBtn.setScaleType(ScaleType.CENTER_CROP);
				addPhotoBtn.setImageBitmap(selectedImage);
			}
		} else {
			removePhoto();
		}
	}

	@Override
	public void onPhotoFetchFail() {
		// do nothing - failed to pick new photo
		
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
		if (outState != null) {
    		if (accountPhoto != null) {
    			// store the photo to a temp file
    			String filePath = Utils.storeBitmapInTmpFile(accountPhoto, PROFILE_PICTURE_TEMP_FILE_NAME);
    			outState.putString(EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH, filePath);
    		}
    	}
    	super.onSaveInstanceState(outState);        
	}
	
	public void signupAtPixServer(PixUserInfo userInfo) {
		PixProgressIndicator.showMessage(getActivity(), 
				PixApp.getContext().getResources().getString(R.string.signing_up), true);
		if (userInfo != null) {
			new SignupHttpRequest(
					new AuthRequestDelegate(getActivity(), AuthRequestDelegate.AUTH_PROCESS_TYPE_SIGNUP) {
						@Override
						public void onAuthFinished() {
							signupAtPixServerFinished();
						}
						@Override
						public void onAuthFailed() {
							signupAtPixServerFailed();
						}
					},
					userInfo).send();
		} else {
			signupAtPixServerFailed();
		}
	}
	
	private void signupAtPixServerFailed() {
		PixProgressIndicator.hideMessageWithIndication(false, 
				PixApp.getContext().getResources().getString(R.string.failed));
		if (getActivity() != null) {
			new PixAlertDialog(getActivity(), 
					null, 
					PixApp.getContext().getString(R.string.failed_to_sign_up),
					PixApp.getContext().getString(R.string.invalid_email___password_please_try_again), 
					PixApp.getContext().getString(R.string.ok)).show();
		}
	}
	
	private void signupAtPixServerFinished() {
		PixProgressIndicator.hideMessageWithIndication(true, 
				PixApp.getContext().getResources().getString(R.string.authenticated));
		
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SIGN_UP, PixDefs.APP_EVENT_ACCOUNT_CREATED, null);

		Bundle args = new Bundle();
		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, true);
		if (mActivitiesController != null) {
			if (mActivity != null) {
				mActivity.finishFragment();
			}
			mActivitiesController.gotoFollowFriends(getActivity(), args);
		}
	}

}
