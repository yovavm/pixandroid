package com.pixplit.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixAccountManager;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.ResetPasswordHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;


public class ForgotPasswordFragment extends PixBaseFragment{
	
	EditText emailText;
	ImageView emailIcon;
	
	// validation stuff
	private static final int EMAIL_INVALID_MAIL = 1;
	
	public static ForgotPasswordFragment newInstance() {
		ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.FORGOT_PASSWORD);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.forgot_password_layout, container, false);
    	if (mFragmentView != null) {
    		RelativeLayout enterEmail = (RelativeLayout)mFragmentView.findViewById(R.id.enterEmail);
    		if (enterEmail != null) {
    			emailText = (EditText)enterEmail.findViewById(R.id.emailText);
    			String accountEmail = PixAccountManager.getFirstAcountName(PixAccountManager.PIX_GOOGLE_ACCOUNT);
    			if (accountEmail != null) {
    				emailText.setText(accountEmail);
    			}
    			emailIcon = (ImageView)enterEmail.findViewById(R.id.emailIcon);
    			if (emailText != null) {
    				emailText.setImeOptions(EditorInfo.IME_ACTION_DONE);
    				emailText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    				emailText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (emailIcon != null) {
								emailIcon.setBackgroundResource(R.drawable.account_email_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				emailText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_DONE) {
								if (validateEmailAddress()) {
									resetPasswordRequest();
								}
								return true;
							}
							return false;
						}
					});
    				emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validateEmailAddress();
							}
						}
					});
    			}
    		}
    		
    		TextView doneBtn = (TextView)mFragmentView.findViewById(R.id.doneBtn);
    		if (doneBtn != null) {
    			doneBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_FORGOT_PASSWORD, PixDefs.APP_EVENT_SEND_PRESSED, null);
						resetPasswordRequest();
					}
				});
    		}
    	}
    	return mFragmentView;
    }

    private void resetPasswordRequest() {
    	if (validateEmailAddress()) {
    		PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getString(R.string.sending), true);
    		new ResetPasswordHttpRequest(new RequestDelegate() {
				@Override
				public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
					PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
					if (mActivity != null) {
						mActivity.onError(error);
					}
				}
				@Override
				public void requestFinished(BaseHttpRequest request, PixObject response) {
					onResetPassowrdRequestDone();
				}
			}, getEmailText()).send();
    		
    		
    	}
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_FORGOT_PASSWORD, PixDefs.APP_EVENT_CANCEL_PRESSED, null);
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }
    
    private void onResetPassowrdRequestDone() {
    	PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getString(R.string.sent));
    	
    	// return the list result to the calling activity
		Intent resultIntent = new Intent();
		resultIntent.putExtra(PixDefs.INTENT_EXTRA_RESET_PASSWORD_SUCCESS, true);
		if (getActivity() != null) {
			getActivity().setResult(Activity.RESULT_OK, resultIntent);
			getActivity().finish();
		}
    }
    
    private boolean validateEmailAddress() {
    	
    	String email = getEmailText();
    	if (Utils.isValidString(email)) {
    		if (!Utils.isValidEmail(email)) {
    			onInvalidEmail(EMAIL_INVALID_MAIL);
    			return false;
    		} else {
    			return true;
    		}
    	} else {
			onInvalidEmail(EMAIL_INVALID_MAIL);
    	}
    	
    	return false;
    }

    private String getEmailText() {
    	if (emailText != null && emailText.getText() != null) {
    		return emailText.getText().toString();
    	}
    	return null;
    }
    
    private void onInvalidEmail(int invalidCode) {
    	if (invalidCode == EMAIL_INVALID_MAIL) {
    		Utils.showMessage(PixApp.getContext().getString(R.string.please_enter_a_valid_email), true);
    	}
    	updateMailIcon(false);
    }
    
    private void updateMailIcon(boolean valid) {
    	if (emailIcon != null) {
    		if (valid) {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon);
    		} else {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_red);
    		}
    	}
    }
	
}
