package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;

public class SingleViewFragment extends PixFeedFragment{
	//private static final String TAG = "SingleViewFragment";
	String compId;
	
	public static SingleViewFragment newInstance(Bundle args) {
		SingleViewFragment fragment = new SingleViewFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
        	compId = args.getString(PixDefs.FRAG_ARG_COMP_ID);
        }
        
        setFragmentType(EFragmentType.SINGLE_VIEW);
        setRequestUrl(PixHttpAPI.SINGLE_COMP_URL_PREFIX + compId);
        
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.single_view_layout, container, false);
	  
		return mFragmentView;
    }
	
	@Override
	protected void onCompDeletion(String deletedCompId) {
		super.onCompDeletion(deletedCompId);
		// verify that the deleted composition is the one displayed
		if (Utils.isValidString(deletedCompId) &&
				Utils.isValidString(this.compId) &&
				this.compId.equalsIgnoreCase(deletedCompId)) {
			// the comp displayed was deleted, close this view
			if (getActivity() != null) {
				getActivity().finish();
			}
		}
	}
}
