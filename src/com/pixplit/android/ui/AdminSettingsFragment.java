package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;


public class AdminSettingsFragment extends PixBaseFragment {
	
	public static AdminSettingsFragment newInstance() {
		AdminSettingsFragment fragment = new AdminSettingsFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.ADMIN_SETTINGS);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.admin_settings_layout, container, false);
    	
    	View allPopularBtn = mFragmentView.findViewById(R.id.allPopularBtn);
    	if (allPopularBtn != null) {
    		allPopularBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivitiesController != null) {
						mActivitiesController.gotoAdminPopularFull(getActivity());
					}
				}
			});
    	}
    	
    	View popularCandidatesBtn = mFragmentView.findViewById(R.id.popularCandidatesBtn);
    	if (popularCandidatesBtn != null) {
    		popularCandidatesBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivitiesController != null) {
						mActivitiesController.gotoAdminPopularCandidates(getActivity(), null);
					}
				}
			});
    	}

    	View suggestedUsersBtn = mFragmentView.findViewById(R.id.suggestedUsersBtn);
    	if (suggestedUsersBtn != null) {
    		suggestedUsersBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivitiesController != null) {
						mActivitiesController.gotoAdminSuggestedFull(getActivity());
					}
				}
			});
    	}

    	View addEurekaAccountBtn = mFragmentView.findViewById(R.id.addEurekaAccountBtn);
    	if (addEurekaAccountBtn != null) {
    		addEurekaAccountBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivitiesController != null) {
						mActivitiesController.gotoAdminAddEurekaAccount(getActivity());
					}
				}
			});
    	}
    	
    	View eurekaAccountBtn = mFragmentView.findViewById(R.id.eurekaAccountBtn);
    	if (eurekaAccountBtn != null) {
    		eurekaAccountBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivitiesController != null) {
						mActivitiesController.gotoAdminEurekaAccounts(getActivity());
					}
				}
			});
    	}

    	return mFragmentView;
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
}
