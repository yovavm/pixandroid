package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;

public class RelatedFragment extends PixFeedFragment{
	String compId;
	
	public static RelatedFragment newInstance(Bundle args) {
		RelatedFragment fragment = new RelatedFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle args = getArguments();
        if (args != null) {
        	compId = args.getString(PixDefs.FRAG_ARG_COMP_ID);
        }
        
        setAllowPaging(true);
        setRequestUrl(PixHttpAPI.COMP_RELATED_URL_PREFIX + compId);
        setFragmentType(EFragmentType.RELATED);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.related_feed_layout, container, false);
		
		return mFragmentView;
	}
}
