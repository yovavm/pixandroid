package com.pixplit.android.ui;

import android.support.v4.widget.DrawerLayout.DrawerListener;

import com.pixplit.android.globals.PixError;

public interface PixActivityInterface {
	public void registerBaseActivityDelegate(BaseActivityDelegate delegate);
	public void unregisterBaseActivityDelegate(BaseActivityDelegate delegate);
	
	public void logoutRequest(boolean withReconnectMsg);
	public void onError(PixError error);
	public void pullNewRecords();
	public ActivitiesControllerInterface getActivitiesController();
	public void finishFragment();
	public void onAdminNotAuthenticatedError();
	
	public void closeDrawer(DrawerListener delegate);
	public void setDrawerListener(DrawerListener delegate);
	public void unsetDrawerListener(DrawerListener delegate);
}
