package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;

public class MyCompletedFragment extends MyFeedFragment{
	//private static final String TAG = "MyCompletedFragment";
	
	public static MyCompletedFragment newInstance() {
		MyCompletedFragment fragment = new MyCompletedFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setAllowPaging(true);
        setRequestUrl(PixHttpAPI.COMPLETED_FEED_URL);
        setFragmentType(EFragmentType.MY_COMPLETED);
        
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mFragmentView != null) {
			removeFragmentViewFromParent(); 
		} else {
			mFragmentView = inflater.inflate(R.layout.my_feeds_layout, container, false);
		}		
		return mFragmentView;
	}
		
//	private void showTipIfNeeded() {
//		
//		if (!PixAppState.didTipDisplayed(PixDefs.MY_FEED_TIP_DISPLAYED) && 
//				getActivity() != null) {
//			final Dialog tipDialog = new Dialog(getActivity(), R.style.PixTipDialog);
//			
//			if (tipDialog != null) {
//				tipDialog.setContentView(R.layout.myfeed_tip_layout);
//				TextView myfeedTipImage = (TextView)tipDialog.findViewById(R.id.myfeedTipImage);
//				if (myfeedTipImage != null) {
//					myfeedTipImage.setText(
//							PixApp.getContext().getResources().getString(R.string.your_feed_has_two_sections) +"\n\n"+ 
//							PixApp.getContext().getResources().getString(R.string.explore_both));
//					myfeedTipImage.setOnClickListener(new OnClickListener() {
//						@Override
//						public void onClick(View v) {
//							tipDialog.dismiss();
//						}
//					});
//									
//				}
//				tipDialog.show();
//			}
//			PixAppState.setTipDisplayed(PixDefs.MY_FEED_TIP_DISPLAYED, true);
//		}
//	}
}
