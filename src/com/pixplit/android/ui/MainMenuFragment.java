package com.pixplit.android.ui;

import java.util.Locale;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.fragments.PixFbBaseFragment;


public class MainMenuFragment extends PixFbBaseFragment implements DrawerListener {

	private View newsBtn;
	private View menuSettings;
	private TextView notificationsIcon;
	private TextView conversationsIcon;
	
	final static int squareDimen = (PixAppState.getState().getFrameHeight() / 10) - 
			PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.main_menu_button_margin);
	
	public static MainMenuFragment newInstance() {
		MainMenuFragment fragment = new MainMenuFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.MAIN_MENU);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.main_menu_layout, container, false);
    	
    	View menuLayoutBg = mFragmentView.findViewById(R.id.menuLayoutBg1);
    	if (menuLayoutBg != null) {
    		menuLayoutBg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivity != null) {
						mActivity.closeDrawer(null);
					}
				}
			});
    	}
    	menuLayoutBg = mFragmentView.findViewById(R.id.menuLayoutBg2);
    	if (menuLayoutBg != null) {
    		menuLayoutBg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivity != null) {
						mActivity.closeDrawer(null);
					}
				}
			});
    	}
    	menuLayoutBg = mFragmentView.findViewById(R.id.menuLayoutBg3);
    	if (menuLayoutBg != null) {
    		menuLayoutBg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActivity != null) {
						mActivity.closeDrawer(null);
					}
				}
			});
    	}
    	
    	// popular
    	View popular = mFragmentView.findViewById(R.id.popularButton);
        if (popular != null) {
	        popular.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_POPULAR_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (mActivitiesController != null) {
									mActivitiesController.gotoPopularRequest(getActivity(), null);
								}
							}});
					}
				}
			});
    	}
        
        // my feeds
    	View myFeeds = mFragmentView.findViewById(R.id.myFeedsButton);
        if (myFeeds != null) {
    		myFeeds.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_MY_FEED_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoMyFeed(getActivity(), 
												ActivitiesControllerInterface.PIX_ANIMATE_SLIDE_IN_RIGHT_TO_LEFT);
								}
							}});
					}
				}
			});
    	}
    	
        // profile
    	View myProfile = mFragmentView.findViewById(R.id.profileButton);
        if (myProfile != null) {
    		myProfile.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_PROFILE_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								Bundle args = new Bundle();
								args.putString(PixDefs.FRAG_ARG_USERNAME, UserInfo.getUserName());
								args.putBoolean(PixDefs.FRAG_ARG_MAIN_MENU_LAUNCH, true);
								if (mActivitiesController != null) {
									mActivitiesController.gotoUserProfile(getActivity(), args, 0);
								}
							}
						});
					}
				}
			});
    	}
    	
        // inbox
    	View inbox = mFragmentView.findViewById(R.id.inboxButton);
        if (inbox != null) {
    		inbox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_INBOX_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoInbox(getActivity(), 0);
								}
							}});
					}
				}
			});
    	}
    	
        // news
    	newsBtn = mFragmentView.findViewById(R.id.newsButton);
        if (newsBtn != null) {
    		newsBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_NEWS_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoNews(getActivity());
								}
							}});
					}
				}
			});
    	}
        notificationsIcon = (TextView)mFragmentView.findViewById(R.id.notificationIcon);
        if (notificationsIcon != null) {
        	notificationsIcon.getLayoutParams().width = squareDimen;
        	notificationsIcon.getLayoutParams().height = squareDimen;
        	notificationsIcon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_NEWS_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoNews(getActivity());
								}
							}});
					}
				}
			});
        	this.setNewsNotificationIcon(PixAppState.getNewNewsNumber());
        }

        // new unread conversations icon
        conversationsIcon = (TextView)mFragmentView.findViewById(R.id.conversationsIcon);
        if (conversationsIcon != null) {
        	conversationsIcon.getLayoutParams().width = squareDimen;
        	conversationsIcon.getLayoutParams().height = squareDimen;
        	conversationsIcon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_INBOX_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoInbox(getActivity(), 0);
								}
							}});
					}
				}
			});
        	this.setNewsConversationsIcon(PixAppState.getNewConversationsNumber());
        }

        // settings
        menuSettings = mFragmentView.findViewById(R.id.menuSettings);
        if (menuSettings != null) {
        	menuSettings.getLayoutParams().width = squareDimen;
        	menuSettings.getLayoutParams().height = squareDimen;
        	menuSettings.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_SETTINGS_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									mActivitiesController.gotoSettings(getActivity());
								}
							}});
					}
				}
			});
        }
        
        // search users
        View searchUsers = mFragmentView.findViewById(R.id.searchUsers);
        if (searchUsers != null) {
        	searchUsers.getLayoutParams().height = squareDimen;
        	searchUsers.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_SEARCH_USERS_PRESSED, null);
					final Bundle args = new Bundle();
					args.putBoolean(PixDefs.FRAG_ARG_USERS_ACTION_TYPE_FOLLOW, true);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (mActivitiesController != null) {
									mActivitiesController.gotoSearchUsers(getActivity(), args);
								}
							}});
					}
				}
			});
    	}
                
        View newSplitBtn = mFragmentView.findViewById(R.id.newSplitButtonLayout);
        if (newSplitBtn != null) {
        	newSplitBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_MAIN_MENU, PixDefs.APP_EVENT_NEW_SPLIT_PRESSED, null);
					if (mActivity != null) {
						mActivity.closeDrawer(new onDrawerCloseListener(){
							@Override
							public void onDrawerClosed() {
								if (!verifyUser()) return;
								if (mActivitiesController != null) {
									Bundle args = new Bundle();
									args.putInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, 
											PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix2HorizontalFrameset));
									mActivitiesController.gotoCreateNewSplit(getActivity(), args, null);
								}
							}});
					}
				}
			});
        }
        
    	return mFragmentView;
    }
    

    @Override
    public void onResume() {
    	super.onResume();
    	
    	if (mActivity != null) {
    		mActivity.setDrawerListener(this);
    	}
    	updateBadges();
    	
    	// on some scenarios, on some fragments, the keyboard opens on resume. close the drawer.
		if (mActivity != null) {
			mActivity.closeDrawer(null);
		}
    }
    
    private void updateBadges() {
    	// update the badges on numbers of new items
    	this.setNewsNotificationIcon(PixAppState.getNewNewsNumber());
    	this.setNewsConversationsIcon(PixAppState.getNewConversationsNumber());
    }
    
    @Override
    public void onNewNewsRecords(int newRecordsNum) {
    	super.onNewNewsRecords(newRecordsNum);
    	setNewsNotificationIcon(newRecordsNum);
    }
    
    @Override
    public void onNewUnreadConversations(int unreadConversationsNum) {
    	super.onNewUnreadConversations(unreadConversationsNum);
    	setNewsConversationsIcon(unreadConversationsNum);
    }

    private void setNewsNotificationIcon(int newRecordsNum) {
    	if (notificationsIcon != null) {
    		if (newRecordsNum > 0) {
    			String notificationNumString;
    			if (newRecordsNum < 100) {
    				notificationNumString = String.format(Locale.getDefault(), "%d", newRecordsNum);
    			} else {
    				notificationNumString = "99";
    			}
    			notificationsIcon.setVisibility(View.VISIBLE);
    			notificationsIcon.setText(notificationNumString);
    			notificationsIcon.setGravity(Gravity.CENTER);
    		} else {
    			notificationsIcon.setVisibility(View.GONE);
    		}
    	}
    }
    
    private void setNewsConversationsIcon(int unreadConversationsNum) {
    	if (conversationsIcon != null) {
    		if (unreadConversationsNum > 0) {
    			String conversationsNumString;
    			if (unreadConversationsNum < 100) {
    				conversationsNumString = String.format(Locale.getDefault(), "%d", unreadConversationsNum);
    			} else {
    				conversationsNumString = "99";
    			}
    			conversationsIcon.setVisibility(View.VISIBLE);
    			conversationsIcon.setText(conversationsNumString);
    			conversationsIcon.setGravity(Gravity.CENTER);
    		} else {
    			conversationsIcon.setVisibility(View.GONE);
    		}
    	}
    }
    
    private abstract class onDrawerCloseListener implements DrawerListener {

		@Override
		public void onDrawerClosed(View arg0) {
			if (mActivity != null) {
				mActivity.unsetDrawerListener(this);
			}
			this.onDrawerClosed();
		}

		@Override
		public void onDrawerOpened(View arg0) {}

		@Override
		public void onDrawerSlide(View arg0, float arg1) {}

		@Override
		public void onDrawerStateChanged(int arg0) {}
    	
		public abstract void onDrawerClosed();
    }

	@Override
	public void onDrawerClosed(View arg0) {}

	@Override
	public void onDrawerOpened(View arg0) {
		updateBadges();
	}

	@Override
	public void onDrawerSlide(View arg0, float arg1) {}

	@Override
	public void onDrawerStateChanged(int arg0) {}
}
