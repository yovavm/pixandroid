package com.pixplit.android.ui;

import java.util.HashMap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.fb.FbConnectDelegate;
import com.pixplit.android.fb.FbProcessType;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.fragments.PixFbBaseFragment;


public class SettingsShareToFbFragment extends PixFbBaseFragment implements FbConnectDelegate{
//  	private static final String TAG = "SettingsShareToFbFragment";
	
	ImageView postLikesCheckBox = null;
	
	public static SettingsShareToFbFragment newInstance() {
		SettingsShareToFbFragment fragment = new SettingsShareToFbFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.FB_SETTINGS);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.settings_facebook_sharing, container, false);
    	
    	RelativeLayout postLikes = (RelativeLayout)mFragmentView.findViewById(R.id.fbPostLikes);
    	if (postLikes != null) {
    		postLikes.setOnClickListener(onPostLikesToFbClickListener);
    		postLikesCheckBox = (ImageView)postLikes.findViewById(R.id.fbPostLikesCheckbox);
            if (postLikesCheckBox != null) {
        		postLikesCheckBox.setOnClickListener(onPostLikesToFbClickListener);
        		updatePostLikesCheckbox();
        	}
    	}
        
    	return mFragmentView;
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
    
    public void setPostLikesToFb(boolean post) {
    	if (post) {
    		if (mFbActivity != null) {
    			mFbActivity.authorizePublishToFb(FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH, this);
    		}
    	} else {
    		// setting post likes to false requires no interaction with facebook
        	PixAppState.UserInfo.setPostLikesToFb(false);
        	updatePostLikesCheckbox();
    	}
    }
    
    private void togglePostLikesToFb() {
    	boolean newPostValue;
    	if (PixAppState.UserInfo.getPostLikesToFb()) {
    		newPostValue = false;
    	} else {
    		newPostValue = true;
    	}
    	HashMap<String, String> context = new HashMap<String, String>();
		context.put(PixDefs.APP_PARAM_POST_LIKES_TO_FACEBOOK, (newPostValue)?"1":"0");
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_FACEBOOK, PixDefs.APP_EVENT_POST_LIKES_TO_FB_PRESSED, context);
    	setPostLikesToFb(newPostValue);
    }
    
    private void updatePostLikesCheckbox() {
    	if (postLikesCheckBox != null) {
	    	if (PixAppState.UserInfo.getPostLikesToFb()) {
	    		postLikesCheckBox.setBackgroundResource(R.drawable.check_on);
	    	} else {
	    		postLikesCheckBox.setBackgroundResource(R.drawable.check_off);
	    	}
    	}
    }
    
    private OnClickListener onPostLikesToFbClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			togglePostLikesToFb();
		}
	};

	@Override
	public void onFbConnectFinished(FbProcessType processType) {
		// validate the process type
		if (processType == FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH) {
			PixAppState.UserInfo.setPostLikesToFb(true);
        	updatePostLikesCheckbox();
		}
	}

	@Override
	public void onFbConnectFailed(FbProcessType processType) {
		//Utils.pixLog(TAG, "request to Facebook failed:"+processType);
	}
    
}
