package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;


public class InfoFragment extends PixBaseFragment{
	
	public static InfoFragment newInstance() {
		InfoFragment fragment = new InfoFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.PIXPLIT_INFO);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.info_layout, container, false);
    	if (mFragmentView != null) {
    		View aboutPixplitBtn = mFragmentView.findViewById(R.id.aboutPixplitBtn);
    		if (aboutPixplitBtn != null) {
    			aboutPixplitBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_INFO, PixDefs.APP_EVENT_ABOUT_PIXPLIT_PRESSED, null);
						if (mActivitiesController != null) {
							Bundle args = new Bundle();
							args.putString(PixDefs.FRAG_ARG_TITLE, PixApp.getContext().getString(R.string.about_pixplit));
							args.putString(PixDefs.FRAG_ARG_URL, PixHttpAPI.aboutUrl);
							mActivitiesController.gotoWebView(getActivity(), args);
						}
					}
				});
    		}
    		View termsOfServiceBtn = mFragmentView.findViewById(R.id.termsOfServiceBtn);
    		if (termsOfServiceBtn != null) {
    			termsOfServiceBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SETTINGS_INFO, PixDefs.APP_EVENT_TERMS_OF_SERVICE_PRESSED, null);
						if (mActivitiesController != null) {
							Bundle args = new Bundle();
							args.putString(PixDefs.FRAG_ARG_TITLE, PixApp.getContext().getString(R.string.terms_of_service));
							args.putString(PixDefs.FRAG_ARG_URL, PixHttpAPI.termsUrl);
							mActivitiesController.gotoWebView(getActivity(), args);
						}
					}
				});
    		}
    		TextView pixplitVersionText = (TextView)mFragmentView.findViewById(R.id.pixplitVersionText);
    		if (pixplitVersionText != null) {
    			String version = PixAppState.getAppVersionNumber();
    			if(Utils.isValidString(version)) {
    				pixplitVersionText.setText("Pixplit v"+version);
    			}
    		}
    	}
    	return mFragmentView;
    }

    @Override
    public boolean onToolbarHomePressIntercepted() {
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }
}
