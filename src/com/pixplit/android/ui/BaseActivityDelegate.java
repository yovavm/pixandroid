package com.pixplit.android.ui;


public interface BaseActivityDelegate {
	
	public boolean onBackPressed();
	// notifications
	public void onNewUnreadConversations(int newUnreadConversationsNum);
	public void onNewNewsRecords(int newRecordsNum);
	public void onNewActions(int newActionsNum);
	public void onNewCompleted(int newCompletedNum);
}
