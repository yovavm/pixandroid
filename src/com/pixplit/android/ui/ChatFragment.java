package com.pixplit.android.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.ConversationListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.data.PixChatMessageContainer;
import com.pixplit.android.data.PixChatUser;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixConversation;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixPrivateCompositionMetadata;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.ChatAddMessageHttpRequest;
import com.pixplit.android.http.request.ConversationDeleteMsgHttpRequest;
import com.pixplit.android.http.request.ConversationFeedHttpRequest;
import com.pixplit.android.http.request.ConversationMessageAckHttpRequest;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.util.PixIdGenerator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.time.PixConversationDateFormat;
import com.pixplit.android.util.viewholders.ChatMessageViewHolder.ChatMessageViewDelegate;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class ChatFragment extends PixListFragment<PixConversation> implements ChatMessageViewDelegate, 
																			  ChatControlDelegate {
//	private static final String TAG = "ChatFragment";
	
	private String conversationId;
	ArrayList<PixChatUser> conversationUsers;
	String recipient = null;
	Date oldestMessageDate = null;
	
	// add messsage bar
	ImageView startNewSplitBtn;
	ImageView sendMessageBtn;
	EditText addMsgText;
	
	// load previous chat messages button
	TextView loadPrevBtn = null;
	
	ChatControllerInterface mChatController;
		
	public static ChatFragment newInstance(Bundle args) {
		ChatFragment fragment = new ChatFragment();
        if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        	fragment.setArguments(args);
        }
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.CHAT);
        Bundle args = getArguments();
        if (args != null) {
        	this.conversationId = args.getString(PixDefs.FRAG_ARG_CONVERSATION_ID);
        	
        	this.conversationUsers = new ArrayList<PixChatUser>();
        	int userIndex = 0;
        	PixChatUser user = PixChatUser.deserialize(args, String.format("%d", userIndex));
        	while (user != null) {
        		this.conversationUsers.add(user);
        		userIndex++;
        		user = PixChatUser.deserialize(args, String.format("%d", userIndex));
        	}
        	updateRecipiants();
        }
        setUsersNamesInToolbar();
    }
	
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mChatController = (ChatControllerInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement ChatControllerInterface");
	    }
    }

    @Override
    public void onDetach () {
    	super.onDetach();
    	mChatController = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mChatController != null) {
    		mChatController.registerChatDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mChatController != null) {
    		mChatController.unregisterChatDelegate();
    	}
    }
    
   	@Override
	public void onPause() {
		super.onPause();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.conversationId, false);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.conversationId, false);
		}
	}
	
	private void updateRecipiants() {
		if (conversationUsers != null) {
			for (PixChatUser user : conversationUsers) {
				if (user != null &&
						Utils.isValidString(user.getUsername())) {
					if (recipient == null) {
        				recipient = user.getUsername();
        			} else {
        				recipient = recipient + "," + user.getUsername();
        			}
				}
			}
		}
	}
	
	private void setUsersNamesInToolbar() {
		if (conversationUsers != null &&
        		conversationUsers.size() > 0 &&
        		conversationUsers.get(0) != null) { 
        	final PixChatUser user = conversationUsers.get(0);
        	if (Utils.isValidString(user.getFullName())) {
        		if (mToolbarController != null) {
        			mToolbarController.setToolBarTitle(user.getFullName());
        		}
        	}
        }
	}
	
	private void setUsersImagesInToolbar() {
		if (conversationUsers != null &&
				conversationUsers.size() > 0 &&
				 conversationUsers.get(0) != null) { 
			 final PixChatUser user = conversationUsers.get(0);
			 if (mToolbarController != null) {
     			 final PixImageView userBtn = mToolbarController.addToolbarImageBtn(
     					 PixDefs.TOOLBAR_BTN_ID_CHAT_USER,
     					 new OnClickListener() {
							 @Override
							 public void onClick(View v) {
								 if (mActivitiesController != null) {
									 Bundle args = new Bundle();
									 args.putString(PixDefs.FRAG_ARG_USERNAME, user.getUsername());
									 mActivitiesController.gotoUserProfile(getActivity(), args, 0);
								 }
							 }
						});
     			if (userBtn != null) {
	   				 userBtn.setScaleType(ScaleType.CENTER_CROP);
	   				 userBtn.setImageResource(R.drawable.avatar_thumb);
	   				 PixImagesLoader.loadImage(user.getPicture(), 
							   						new BitmapFetcherDelegate() {
							   					 @Override
							   					 public void onBitmapFetchFinished(String url, 
							   							 						   PixBitmapHolder bitmapHolder) {
							   						 if (userBtn != null && bitmapHolder != null) {
							   							 userBtn.setImageBitmapHolder(bitmapHolder);
							   						 }
							   					 }
							   					 @Override
							   					 public void onBitmapFetchFailed() {}
							   				 }, Priority.HIGH);
	   			 }
			 }
		 }
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.chat_layout, container, false);
		updateAddMessageBar();
		
		return mFragmentView;
	}
	
	@Override
	 public void onStart() {
		 super.onStart();
		 // update the toolbar
		 setUsersImagesInToolbar();
		 
		 this.showTipIfNeeded();
	 }

	
	private void showTipIfNeeded() {
		if (!PixAppState.didTipDisplayed(PixDefs.CHAT_TIP_DISPLAYED) && getActivity() != null) {
			final Dialog tipDialog = new Dialog(getActivity(), R.style.PixAlertDialog);
			
			if (tipDialog != null) {
				tipDialog.setContentView(R.layout.chat_tip_layout);
				View tip = tipDialog.findViewById(R.id.chatTip);
				if (tip != null) {
					tip.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							tipDialog.dismiss();
						}
					});
				}
				tipDialog.show();
			}
			PixAppState.setTipDisplayed(PixDefs.CHAT_TIP_DISPLAYED, true);
		}
	}
	
	 @Override
	 public void onResume() {
		 super.onResume();
		 PixApp.hideKeypad(mFragmentView);
		 if (mActivitiesController != null &&
				mActivitiesController.needRefresh(this.conversationId)) {
			refreshList();
		}
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.conversationId, true);
		}
	 }
	 
	@Override
	public void onDestroyView() {
		PixApp.hideKeypad(mFragmentView);
		super.onDestroyView();
	}
    
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixConversation) {
			PixConversation conversation = (PixConversation)response;
			String id = conversation.getConversationId();
			if (Utils.isValidString(id) && !id.equalsIgnoreCase(PixConversation.CONVERSATION_ID_NOT_FOUND)) {
				this.conversationId = id;
				if (mActivitiesController != null) {
					mActivitiesController.markActivityIsRunning(this.conversationId, true);
				}
			}
			if (conversationUsers == null || conversationUsers.size() == 0) {
				/*
				 *  the conversation users are missing (may happen in case a push notification 
				 *  opened this screen), update the users and UI from the response.
				 */
				conversationUsers = conversation.getUsers();
				setUsersNamesInToolbar();
				if (mToolbarController != null) {
					// refresh the title with the users names
					mToolbarController.updateToolbarTitle(0);
				}
				setUsersImagesInToolbar(); // set user image on toolbar
				updateRecipiants();
				
			}
			ArrayList<PixChatMessage> messagesFeed = (ArrayList<PixChatMessage>) conversation.getMessages();
			
			// ack receipt to server
			if (messagesFeed != null && messagesFeed.size() > 0 ) {
				ackMessageToServer(messagesFeed.get(0));
    			if (mActivity != null) {
    				mActivity.pullNewRecords();
    			}
    		}
			
			int newMessagesNum = 0;
			if (messagesFeed != null) {
				Collections.reverse(messagesFeed);
				newMessagesNum = messagesFeed.size();
			}
			
			// update whether to display date timestamp on the items
			updateDateTimestamps(messagesFeed);
			
			// update the 'load previous messages' header
			updateHeader();
			
			updateAdapterData(messagesFeed);
			if (isFirstPage(requestContext)) {
				jumpToLast();
			} else {
				restoreListPosition(newMessagesNum);
			}
			if (loadPrevBtn != null) {
				loadPrevBtn.setBackgroundResource(R.drawable.wizard_btn_selector);
			}
		}
	}
	
	private void ackMessageToServer(PixChatMessage message) {
		if (message != null) {
			String msgId = message.getId();
			if (Utils.isValidString(msgId) &&
					Utils.isValidString(conversationId)) {
				new ConversationMessageAckHttpRequest(conversationId, msgId).send();
			}
		}
	}
	
    private void updateDateTimestamps(ArrayList<PixChatMessage> messagesFeed) {
    	if (messagesFeed != null) {
    		if (messagesFeed.size() > 0 && messagesFeed.get(0) != null) {
    			oldestMessageDate = messagesFeed.get(0).getDate();
    		} else {
    			oldestMessageDate = null;
    		}
    		for (int i=0; i < messagesFeed.size()-1; i++) {
    			PixChatMessage message = messagesFeed.get(i);
    			PixChatMessage nextMessage = messagesFeed.get(i+1);

    			if (message != null && nextMessage != null) {
        			if (!isMsgsDateEuqal(message, nextMessage)) {
        				Date nextDate = nextMessage.getDate();
        				message.setNextDate(nextDate);
        			}
        		}
    		}
    	} else {
			oldestMessageDate = null;
		}
	}

    private boolean isMsgsDateEuqal(PixChatMessage message1, PixChatMessage message2) {
    	boolean equal = false;
    	if (message1 != null && message2 != null) {
	    	Date date = message1.getDate();
			Date nextDate = message2.getDate();
			if (date != null && nextDate != null) {
				Calendar calender = PixConversationDateFormat.getPixDateInstance().getCalendar();
				calender.setTime(date);
				int dateInt = calender.get(Calendar.DATE);
				calender.setTime(nextDate);
				int nextDateInt = calender.get(Calendar.DATE);
				if (dateInt == nextDateInt) {
					equal = true;
				}
			}
    	}
		return equal;
    }
    
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
		BaseVolleyHttpRequest request = null;
		request = new ConversationFeedHttpRequest(this, conversationId, recipient);
		return request;
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	ConversationListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new ConversationListAdapter(getActivity(), this, R.layout.chat_item_layout, this);
    	}
    	return adapter;
	}

    public String getConversationId() {
    	return this.conversationId;
    }
    
    @Override
	protected int getHeaderLayout(){
		return R.layout.chat_header;
	}
    
    private void updateHeader(){
		if (mHeader != null) {
			View loadPrevBtnFrame =  mHeader.findViewById(R.id.loadPrevBtnFrame);
			
			if (Utils.isValidString(getNextPage())) {
				loadPrevBtnFrame.setVisibility(View.VISIBLE);
				
				if (loadPrevBtnFrame != null) {
					loadPrevBtn = (TextView)loadPrevBtnFrame.findViewById(R.id.loadPrevBtn);
					if (loadPrevBtn != null) {
						loadPrevBtn.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								v.setBackgroundResource(R.drawable.wizard_btn_selected);
								storeListPosition();
								loadNextPage();
							}
						});
					}
				}
			} else {
				loadPrevBtnFrame.setVisibility(View.GONE);
			}
			
			// date
			RelativeLayout headerDateTimestamp;
			TextView headerDateText;
			// date
			headerDateTimestamp = (RelativeLayout) mHeader.findViewById(R.id.dateTimestamp_ref);
			if (headerDateTimestamp != null) {
				headerDateText = (TextView) headerDateTimestamp.findViewById(R.id.dateText);
				if (headerDateText != null) {
		    		if (oldestMessageDate != null) {
		    			String dateHumanFormat = PixConversationDateFormat.getPixDateInstance().mediumDateFormat(oldestMessageDate);
		    			if (Utils.isValidString(dateHumanFormat)) {
			    			headerDateText.setText(dateHumanFormat);
			    			headerDateTimestamp.setVisibility(View.VISIBLE);
		    			}
		    		} else {
		    			headerDateTimestamp.setVisibility(View.GONE);
		    		}
		    	}
			}	
		}
    }
    
    @Override
	public String getUserDisplayName(String username) {
    	if (Utils.isValidString(username)) {
    		if (username.equalsIgnoreCase(PixAppState.UserInfo.getUserName())) {
    			return PixAppState.UserInfo.getUserFullName();
    		}
			if (this.conversationUsers != null) {
				for (PixChatUser user : this.conversationUsers) {
					if (user != null &&
							Utils.isValidString(user.getUsername()) &&
							username.equalsIgnoreCase(user.getUsername())) {
						return user.getFullName();
					}
				}
			}
    	}
		return null;
	}
    
    private void updateAddMessageBar() {
    	if (mFragmentView == null) return;
    	
    	startNewSplitBtn = (ImageView)mFragmentView.findViewById(R.id.startNewSplitBtn);
    	if (startNewSplitBtn != null) {
    		startNewSplitBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createPrivateSplit(PixCompositionFramesetType.Pix2HorizontalFrameset, null, null);
				}
			});
    	}
    	sendMessageBtn = (ImageView)mFragmentView.findViewById(R.id.addMsgSendBtn);
    	addMsgText = (EditText)mFragmentView.findViewById(R.id.addMsgText);
		    	
		addMsgText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					sendMessageBtn.setEnabled(true);
				} else {
					sendMessageBtn.setEnabled(false);
				}
			}
		});
		sendMessageBtn.setEnabled(false);
		sendMessageBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String text = null;
				if (addMsgText != null &&
						addMsgText.getText() != null) {
					text = addMsgText.getText().toString();
				}
				
				sendTextMessage(text);
				// clean ups
				addMsgText.setText("");
				PixApp.hideKeypad(mFragmentView);
			}
		});
    }

    RequestDelegate sendTextMessageRequestDelegate = new RequestDelegate() {
		@Override
		public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
			if (request != null) {
				Bundle requestContext = request.getRequestContext();
				if (requestContext != null) {
					PixChatMessage requestMsg = PixChatMessage.deserialize(requestContext);
					if (requestMsg != null) {
						updateMessageDataOnResponse(requestMsg.getId(), false, null);
						// finally, after updating the data, refresh the list to propagate the new info.
						if (mAdapter != null) {
							mAdapter.notifyDataSetChanged();
					    	jumpToLast();
						}
					}
				}
			}
			if (mActivity != null) {
				mActivity.onError(error);
			}
		}
		
		@Override
		public void requestFinished(BaseHttpRequest request, PixObject response) {
			if (request != null && response != null && response instanceof PixChatMessageContainer) {
				PixChatMessageContainer chatMsgContainer = (PixChatMessageContainer)response;
				// verification on conversation ID
				String responseConvId = chatMsgContainer.getConversationId();
				if (!Utils.isValidString(responseConvId)) {
					//Utils.pixLog(TAG, "invalid/missing response conversation ID");
					return;
				}
				if (!Utils.isValidString(conversationId) && 
						!responseConvId.equalsIgnoreCase(PixConversation.CONVERSATION_ID_NOT_FOUND)) {
					// set the conversation ID
					ChatFragment.this.conversationId = responseConvId;
					if (mActivitiesController != null) {
						mActivitiesController.markActivityIsRunning(ChatFragment.this.conversationId, true);
					}
				} else {
					if (!conversationId.equalsIgnoreCase(responseConvId)) {
						//Utils.pixLog(TAG, "conversation ID mismatch. chat id:"+ conversationId+", response id:"+responseConvId);
						return;
					}
				}
				Bundle requestContext = request.getRequestContext();
				if (requestContext != null) {
					PixChatMessage requestMsg = PixChatMessage.deserialize(requestContext);
					if (requestMsg != null) {
						updateMessageDataOnResponse(requestMsg.getId(), true, chatMsgContainer.getChatMessage());
					}
				}
				// finally, after updating the data, refresh the list to propagate the new info.
				if (mAdapter != null) {
					mAdapter.notifyDataSetChanged();
			    	jumpToLast();
				}
				
				if (mActivitiesController != null) {
					mActivitiesController.markForRefresh(null, true);
				}
			}
			
		}
	};
	
	private void updateMessageDataOnResponse(String tmpMsgId, boolean success, PixChatMessage responseMsg) {
		ArrayList<PixChatMessage> messagesFeed = null;
		if (mAdapter != null) {
			messagesFeed = (ArrayList<PixChatMessage>)mAdapter.getData();
		}
		if (messagesFeed == null || messagesFeed.size() == 0 || !Utils.isValidString(tmpMsgId)) return;
		
		for (PixChatMessage msg : messagesFeed) {
			if (msg != null && msg.getId() != null &&
					msg.getId().equalsIgnoreCase(tmpMsgId)) {
				if (success) {
					msg.setSentState(PixChatMessage.PIX_MESSAGE_SENT_STATE_SUCCESS);
					if (responseMsg != null) {
						msg.setId(responseMsg.getId());
						msg.setDate(responseMsg.getDate());
					}
				} else {
					msg.setSentState(PixChatMessage.PIX_MESSAGE_SENT_STATE_FAIL);
				}
				break;
			}
		}
	}
	
    public void sendTextMessage(String text) {
    	if (Utils.isValidString(text)) {
    		// create a chat message
    		PixChatMessage newChatMessage = new PixChatMessage();
    		newChatMessage.setText(text);
    		newChatMessage.setSender(UserInfo.getUserName());
    		newChatMessage.setType(PixChatMessage.PIX_MESSAGE_TYPE_TEXT);
    		newChatMessage.setDate(new Date());
    		int uniqueId = PixIdGenerator.getUniqueId();
    		String newMsgId = String.format(Locale.getDefault(), "Pix_ID_%d", uniqueId);
    		newChatMessage.setId(newMsgId);
    		newChatMessage.setSentState(PixChatMessage.PIX_MESSAGE_SENT_STATE_SENDING);

    		// add the new message to the chat list
    		addNewMessageToFeed(newChatMessage);
    		
			HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARAM_MSG_TYPE, PixDefs.APP_PARAM_MSG_TYPE_TEXT);
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_PRIVATE_CHAT, PixDefs.APP_EVENT_MSG_SENT, context);

    		// send the message to the server
    		sendNewTextMessageRequest(newChatMessage);
    	}
    }

    public void sendNewTextMessageRequest(PixChatMessage newChatMessage) {
    	if (newChatMessage == null || !Utils.isValidString(newChatMessage.getText())) return;
    	
    	ChatAddMessageHttpRequest addMessageHttpRequest = new ChatAddMessageHttpRequest(
				sendTextMessageRequestDelegate,
				conversationId, 
				recipient, 
				null,
				PixChatMessage.PIX_MESSAGE_TYPE_TEXT, 
				newChatMessage.getText(), 
				null);
		Bundle requestContext = addMessageHttpRequest.getRequestContext();
		if (requestContext != null) {
			newChatMessage.serialize(requestContext);
		}
		// send the message to the server
		addMessageHttpRequest.send();
    }
    
    public void reSendTextMsg(PixChatMessage chatMsg) {
    	if (chatMsg == null) return;
    	
    	chatMsg.setSentState(PixChatMessage.PIX_MESSAGE_SENT_STATE_SENDING);
    	if (mAdapter != null) {
			mAdapter.notifyDataSetChanged(); // reflect the 'sending' in the ui
	    }
		// send the message to the server
		sendNewTextMessageRequest(chatMsg);
    }
    
	public void addNewMessageToFeed(PixChatMessage chatMessage) {
		if (chatMessage == null) return;
		ArrayList<PixChatMessage> messagesFeed = null;
		if (mAdapter != null) {
			messagesFeed = (ArrayList<PixChatMessage>)mAdapter.getData();
		}
		if (messagesFeed == null || messagesFeed.size() == 0) {
			// first message in chat
			oldestMessageDate = chatMessage.getDate();
			updateHeader();
			
			messagesFeed = new ArrayList<PixChatMessage>();
		} else {
			// not first message in chat, no need to update the header
			PixChatMessage prevChatMessage = messagesFeed.get(messagesFeed.size()-1);
			if (prevChatMessage != null) {
				if (!isMsgsDateEuqal(prevChatMessage, chatMessage)) {
					Date nextDate = chatMessage.getDate();
					prevChatMessage.setNextDate(nextDate);
				}
			}
		}
		messagesFeed.add(chatMessage);
		changeAdapterData(messagesFeed);
		jumpToLast();
	}

	public void createPrivateSplit(PixCompositionFramesetType frameType, final PixComposition joinedComp, String msgId) {
		int privateCompFrameType = -1;
		
		if (frameType != null) {
			privateCompFrameType = PixCompositionFramesetType.getInt(frameType);
		} else if (joinedComp != null) {
			privateCompFrameType = joinedComp.getCompType();
		}
		
		if (privateCompFrameType != -1) {
			final Bundle args = new Bundle();
			args.putInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, privateCompFrameType);
			
			PixPrivateCompositionMetadata privateCompMetadata = new PixPrivateCompositionMetadata(conversationId, recipient, msgId);
			privateCompMetadata.serialize(args);
			
			if (joinedComp != null) {
				joinedComp.serialize(args);

				// fetch the joined private split image and start the create process
				PixImagesLoader.loadImage(joinedComp.getImage(), 
										new BitmapFetcherDelegate() {
											@Override
											public void onBitmapFetchFinished(String url, 
																			  PixBitmapHolder bitmapHolder) {
												if (bitmapHolder != null) {
													if (mActivitiesController != null) {
														mActivitiesController.gotoCreateNewSplit(getActivity(), args, bitmapHolder);
													}
												}
											}
											@Override
											public void onBitmapFetchFailed() {
												// Failed to fetch the split image. Do nothing - can't join.
											}
										}, 
										Priority.IMMEDIATE);
			} else {
				// start a new private split
				if (mActivitiesController != null) {
					mActivitiesController.gotoCreateNewSplit(getActivity(), args, null);
				}
			}
		}
	}
	
	@Override
	public void onMsgCompClick(PixChatMessage chatMsg) {
		if (chatMsg != null &&
				chatMsg.getType() == PixChatMessage.PIX_MESSAGE_TYPE_COMP) {
			PixComposition comp = chatMsg.getComp();
			if (PixComposition.isJoinSplitAllow(comp)) {
				createPrivateSplit(null, comp, chatMsg.getId());
			}
		}
	}
	
	@Override
	protected void onRefreshRequestDone(Bundle requestContext) {
		// no need to scroll to first/last - already done in response handling.
	}

	@Override
	public void onMsgLongPress(final PixChatMessage chatMsg) {
		if (chatMsg != null && 
				getActivity() != null) {
			String loggedInUser = PixAppState.UserInfo.getUserName();
	    	String sender = chatMsg.getSender();
			if (Utils.isValidString(sender) &&
	    			Utils.isValidString(loggedInUser) &&
	    			sender.equalsIgnoreCase(loggedInUser)) {
	    		
				new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
					public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_PRIVATE_CHAT, PixDefs.APP_EVENT_DELETE_PRIVATE_MSG, null);
						
						// send the deletion request to the server
						new ConversationDeleteMsgHttpRequest(conversationId, chatMsg.getId()).send();
						
						if (mAdapter != null && Utils.isValidString(chatMsg.getId())) {
							// delete the conversation from the list locally
							ArrayList<PixChatMessage> chatFeed = null;
							chatFeed = (ArrayList<PixChatMessage>) mAdapter.getData();
							if (chatFeed != null) {
								for (PixChatMessage chatFeedItem : chatFeed) {
									if (chatFeedItem != null && 
											Utils.isValidString(chatFeedItem.getId()) &&
											chatFeedItem.getId().equalsIgnoreCase(chatMsg.getId())) {
										chatFeed.remove(chatFeedItem);
										mAdapter.changeData(chatFeed);
										break;
									}
								}
							}
						}
						
					}
					public void onDecline() {}
				},
				PixApp.getContext().getResources().getString(R.string.confirm_deletion),
				PixApp.getContext().getResources().getString(R.string.are_you_sure), 
				PixApp.getContext().getResources().getString(R.string.ok), null).show();
			}
		}
	}

	// ChatControlDelegate
	@Override
	public void onNewMessageReceived(String conversationId) {
		if (Utils.isValidString(conversationId) &&
				Utils.isValidString(this.conversationId) &&
				conversationId.equalsIgnoreCase(this.conversationId)) {
			this.refreshList();
		}
	}
}
