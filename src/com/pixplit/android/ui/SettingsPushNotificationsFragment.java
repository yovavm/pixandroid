package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixNotificationsSettings;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.GetNotificationsSettingsHttpRequest;
import com.pixplit.android.http.request.SetNotificationsSettingsHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;


public class SettingsPushNotificationsFragment extends PixBaseFragment implements RequestDelegate{
	PixNotificationsSettings settings;
	
	ImageView likeCheckbox;
	ImageView joinCheckbox;
	ImageView invitesCheckbox;
	ImageView commentsCheckbox;
	ImageView followedCheckbox;
	ImageView fbFriendCheckbox;
	ImageView systemCheckbox;
	
	public static SettingsPushNotificationsFragment newInstance() {
		SettingsPushNotificationsFragment fragment = new SettingsPushNotificationsFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.SETTINGS_PUSH_NOTIFICATION);
        settings = null;
        this.getPushNotificationsSettings();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.settings_notifications, container, false);
    	if (mFragmentView != null) {
    		RelativeLayout likeBtn = (RelativeLayout)mFragmentView.findViewById(R.id.likeBtn);
    		if (likeBtn != null) {
    			likeCheckbox = (ImageView) likeBtn.findViewById(R.id.likeCheckbox);
    			likeBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getLiked() == 1) {
								settings.setLiked(0);
							} else {
								settings.setLiked(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout joinBtn = (RelativeLayout)mFragmentView.findViewById(R.id.joinBtn);
    		if (joinBtn != null) {
    			joinCheckbox = (ImageView) joinBtn.findViewById(R.id.joinCheckbox);
    			joinBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getJoined() == 1) {
								settings.setJoined(0);
							} else {
								settings.setJoined(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout invitesBtn = (RelativeLayout)mFragmentView.findViewById(R.id.invitesBtn);
    		if (invitesBtn != null) {
    			invitesCheckbox = (ImageView) invitesBtn.findViewById(R.id.invitesCheckbox);
    			invitesBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getInvited() == 1) {
								settings.setInvited(0);
							} else {
								settings.setInvited(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout commentsBtn = (RelativeLayout)mFragmentView.findViewById(R.id.commentsBtn);
    		if (commentsBtn != null) {
    			commentsCheckbox = (ImageView) commentsBtn.findViewById(R.id.commentsCheckbox);
    			commentsBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getCommented() == 1) {
								settings.setCommented(0);
							} else {
								settings.setCommented(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout followedBtn = (RelativeLayout)mFragmentView.findViewById(R.id.followedBtn);
    		if (followedBtn != null) {
    			followedCheckbox = (ImageView) followedBtn.findViewById(R.id.followedCheckbox);
    			followedBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getFollowed() == 1) {
								settings.setFollowed(0);
							} else {
								settings.setFollowed(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout fbFriendBtn = (RelativeLayout)mFragmentView.findViewById(R.id.fbFriendBtn);
    		if (fbFriendBtn != null) {
    			fbFriendCheckbox = (ImageView) fbFriendBtn.findViewById(R.id.fbFriendCheckbox);
    			fbFriendBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getFbFriendJoined() == 1) {
								settings.setFbFriendJoined(0);
							} else {
								settings.setFbFriendJoined(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		RelativeLayout systemBtn = (RelativeLayout)mFragmentView.findViewById(R.id.systemBtn);
    		if (systemBtn != null) {
    			systemCheckbox = (ImageView) systemBtn.findViewById(R.id.systemCheckbox);
    			systemBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (settings != null) {
							if (settings.getSystem() == 1) {
								settings.setSystem(0);
							} else {
								settings.setSystem(1);
							}
						}
						updateSettingsView();
					}
				});
    		}
    		
    	}
    	return mFragmentView;
    }

    private void getPushNotificationsSettings() {
    	PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getResources().getString(R.string.loading), true);
    	new GetNotificationsSettingsHttpRequest(this).send();
    }

    private void updateSettingsView() {
    	if (settings != null) {
	    	if (likeCheckbox != null) {
	    		if (settings.getLiked() == 1) {
	    			likeCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			likeCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (joinCheckbox != null) {
	    		if (settings.getJoined() == 1) {
	    			joinCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			joinCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (invitesCheckbox != null) {
	    		if (settings.getInvited() == 1) {
	    			invitesCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			invitesCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (commentsCheckbox != null) {
	    		if (settings.getCommented() == 1) {
	    			commentsCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			commentsCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (followedCheckbox != null) {
	    		if (settings.getFollowed() == 1) {
	    			followedCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			followedCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (fbFriendCheckbox != null) {
	    		if (settings.getFbFriendJoined() == 1) {
	    			fbFriendCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			fbFriendCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	if (systemCheckbox != null) {
	    		if (settings.getSystem() == 1) {
	    			systemCheckbox.setBackgroundResource(R.drawable.check_on);
	    		} else {
	    			systemCheckbox.setBackgroundResource(R.drawable.check_off);
	    		}
	    	}
	    	
    	}
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
    	// send the updated settings to the server
    	new SetNotificationsSettingsHttpRequest(null, this.settings).send();
    }
    
	@Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
	
	@Override
	public void requestFinished(BaseHttpRequest request, PixObject response) {
		if (response != null && response instanceof PixNotificationsSettings) {
			this.settings = (PixNotificationsSettings)response; 
			this.updateSettingsView();
		}
		PixProgressIndicator.hideMessage();
	}

	@Override
	public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
		PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getResources().getString(R.string.failed));
		if (mActivity != null) {
			mActivity.onError(error);
		}
	}
}
