package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.PopularCandidatesListAdapter;
import com.pixplit.android.adapters.PopularCandidatesListAdapter.PopularCandidatesListDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.util.Utils;

public class AdminPopularCandidatesFragment extends PixFeedFragment 
											implements PopularCandidatesListDelegate{
	
	public static AdminPopularCandidatesFragment newInstance(Bundle args) {
		AdminPopularCandidatesFragment fragment = new AdminPopularCandidatesFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        setFragmentType(EFragmentType.ADMIN_POPULAR_CANDIDATES);
        setRequestUrl(PixHttpAPI.ADMIN_POPULAR_CANDIDATES_URL);
        if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(PixApp.getContext().getResources().getString(R.string.admin_popular_candidates));
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.theme_feed_layout, container, false);
		return mFragmentView;
	}
	
	@Override
	protected PixBaseListAdapter<?> createAdapter() {
		PopularCandidatesListAdapter adapter = new PopularCandidatesListAdapter(getActivity(), 
				this, R.layout.composition_layout, this);
		return adapter;
	}
	
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
		BaseVolleyHttpRequest request = super.getRefreshRequest();
		if (request != null) {
			request.addAdminIdentity();
		}
		return request;
	}

	@Override
	public void onCandidateApproved(String compId) {
		this.removeCompositionFromFeed(compId);
	}

	@Override
	public void onCandidateDiscard(String compId) {
		this.removeCompositionFromFeed(compId);
	}
	
	@Override
	public void removeCompositionFromFeed(String compId) {
		if (Utils.isValidString(compId) && mAdapter != null) {
			ArrayList<PixComposition> compArray = (ArrayList<PixComposition>)mAdapter.getData();
			if (compArray != null) {
				for (int i=0; i < compArray.size(); i++) {
					PixComposition comp = compArray.get(i);
					if (comp != null && comp.getCompId() != null &&
							comp.getCompId().equalsIgnoreCase(compId)) {
						mAdapter.removeItemAtIndex(i);
//						TODO: add animation...
//						removeListItem(i, AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.popup_hide));
					}
				}
			}
		}
	}
	
	@Override
	public void onAdminNotAuthenticated() {
		if (mActivity != null) {
			mActivity.onAdminNotAuthenticatedError();
		}
	}
}
