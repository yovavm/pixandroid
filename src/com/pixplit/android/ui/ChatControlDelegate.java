package com.pixplit.android.ui;


public interface ChatControlDelegate {
	public void onNewMessageReceived(String conversationId);
}
