package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.ConversationsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixChatUser;
import com.pixplit.android.data.PixConversationPreview;
import com.pixplit.android.data.PixConversations;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.ConversationHideHttpRequest;
import com.pixplit.android.http.request.ConversationsFeedHttpRequest;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.ConversationPreviewViewHolder.ConversationPreviewDelegate;


public class InboxFragment extends PixListFragment<PixConversationPreview> implements ConversationPreviewDelegate{

	public static InboxFragment newInstance() {
		InboxFragment fragment = new InboxFragment();
		Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.INBOX);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.conversations_layout, container, false);
		
		if (mFragmentView != null) {
			ImageView newMsgBtn = (ImageView) mFragmentView.findViewById(R.id.newMsgBtn);
			if (newMsgBtn != null) {
				newMsgBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Bundle args = new Bundle();
						args.putBoolean(PixDefs.FRAG_ARG_USERS_ACTION_TYPE_MSG, true);
						if (mActivitiesController != null) {
							mActivitiesController.gotoSearchUsers(getActivity(), args);
						}
					}
				});
			}
		}
		return mFragmentView;
	}
	    
	@Override
    public void onResume() {
    	super.onResume();
		if (mActivitiesController != null &&
				mActivitiesController.needRefresh(null)) {
			refreshList();
		}
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(
					ActivitiesControllerInterface.RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT, true);
		}
    }

	@Override
    public void onPause() {
    	super.onPause();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(
					ActivitiesControllerInterface.RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT, false);
		}
	}
	
	@Override
    public void onStop() {
    	super.onStop();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(
					ActivitiesControllerInterface.RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT, false);
		}
	}
	
    @Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixConversations) {
			PixConversations conversations = (PixConversations)response;
			
			ArrayList<PixConversationPreview> conversationsFeed = conversations.getConversations();
			updateAdapterData(conversationsFeed);
			
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	BaseVolleyHttpRequest request = new ConversationsFeedHttpRequest(this);
    	
		return request;
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	ConversationsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new ConversationsListAdapter(getActivity(), this, R.layout.conversation_preview_layout, this);
    	}
    	return adapter;
	}

	@Override
	public void onConversationClick(PixConversationPreview conversation) {
		if (conversation != null) {
			// create the arguments bundle
        	Bundle args = new Bundle();
        	args.putString(PixDefs.FRAG_ARG_CONVERSATION_ID, conversation.getId());
        	ArrayList<PixChatUser> users = conversation.getUsers();
        	if (users != null) {
        		int index = 0;
        		for (PixChatUser user : users) {
        			if (user != null) {
        				user.serialize(args, String.format("%d", index));
        				index++;
        			}
        		}
        	}
			if (mActivitiesController != null) {
				mActivitiesController.gotoChat(getActivity(), args, 0);
			}
		}
	}

	@Override
	public void onConversationLongPress(final PixConversationPreview conversation) {
		if (conversation != null && 
				getActivity() != null) {
			new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
				public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_PRIVATE_CHAT, PixDefs.APP_EVENT_HIDE_CONVERSATION , null);
					
					// send the deletion request to the server
					new ConversationHideHttpRequest(conversation.getId()).send();
					
					if (mAdapter != null && Utils.isValidString(conversation.getId())) {
						// delete the conversation from the list locally
						ArrayList<PixConversationPreview> conversationsFeed = null;
						conversationsFeed = (ArrayList<PixConversationPreview>) mAdapter.getData();
						if (conversationsFeed != null) {
							for (PixConversationPreview convPreview : conversationsFeed) {
								if (convPreview != null && 
										Utils.isValidString(convPreview.getId()) &&
										convPreview.getId().equalsIgnoreCase(conversation.getId())) {
									conversationsFeed.remove(convPreview);
									mAdapter.changeData(conversationsFeed);
									break;
								}
							}
						}
					}
					
				}
				public void onDecline() {}
			},
			PixApp.getContext().getResources().getString(R.string.confirm_deletion),
			PixApp.getContext().getResources().getString(R.string.are_you_sure), 
			PixApp.getContext().getResources().getString(R.string.ok), null).show();
		}		
	}

}
