package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.InstagramPhotoListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.InstagramItem;
import com.pixplit.android.data.InstagramItems;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.InstagramFeedHttpRequest;
import com.pixplit.android.instagram.InstagramPhotoPickDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.viewholders.InstagramItemsPack;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class InstagramPickPhotoFragment extends PixListFragment<InstagramItem> implements InstagramPhotoPickDelegate{
	String instagramRefreshUrl;
	private static final String loading = PixApp.getContext().getString(R.string.loading);
	private static final String failed = PixApp.getContext().getString(R.string.failed);
	
	public static InstagramPickPhotoFragment newInstance() {
		InstagramPickPhotoFragment fragment = new InstagramPickPhotoFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.INSTAGRAM_PICK_PHOTO);
        instagramRefreshUrl = PixHttpAPI.INSTAGRAM_PHOTOS_URL;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.instagram_pick_photo_layout, container, false);
		
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof InstagramItems) {
			InstagramItems items = (InstagramItems)response;
			this.instagramRefreshUrl = items.getNext();
			ArrayList<InstagramItemsPack> parsedData = (ArrayList<InstagramItemsPack>) 
					InstagramItemsPack.createItemsPackList(items.getItems(), 3);
			updateAdapterData(parsedData);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new InstagramFeedHttpRequest(this, this.instagramRefreshUrl, Priority.HIGH);
	}
    
    @Override
    protected void addNextParam(BaseVolleyHttpRequest request) {
    	// next page is handled internally by Instagram url's mechanism
    }
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	InstagramPhotoListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new InstagramPhotoListAdapter(getActivity(), this, this);
    	}
    	return adapter;
	}

	@Override
	public void onPhotoPick(InstagramItem item) {
		if (getActivity() == null || item == null) return;
		final String imageUrl = item.getImage();
		if (!Utils.isValidString(imageUrl)) return;
		
		PixProgressIndicator.showMessage(getActivity(), loading, true);
		PixImagesLoader.loadImage(imageUrl, 
				new BitmapFetcherDelegate() {
					@Override
					public void onBitmapFetchFinished(String url, PixBitmapHolder holder) {
						if (getActivity() == null || !Utils.isValidString(imageUrl)) return;
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
								PixDefs.APP_EVENT_SUCCESSFUL_INSTAGRAM_IMPORT, 
								null);

						PixProgressIndicator.hideMessage();
						Intent resultIntent = new Intent();
						resultIntent.putExtra(PixDefs.INTENT_EXTRA_PICKED_PHOTO_URL, imageUrl);
						getActivity().setResult(Activity.RESULT_OK, resultIntent);
						getActivity().finish();	
					}
					
					@Override
					public void onBitmapFetchFailed() {
						PixProgressIndicator.hideMessageWithIndication(false, failed);
						if (getActivity() != null) {
							getActivity().finish();	
						}
					}
				}, Priority.IMMEDIATE);
	}
	
	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		super.requestFinishedWithError(request, error);
		UserInfo.clearInstagramUserInfo();
		if (mActivity != null) {
			mActivity.finishFragment();
		}
	}
}
