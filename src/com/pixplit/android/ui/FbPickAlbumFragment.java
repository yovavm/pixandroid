package com.pixplit.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.FbAlbumsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.FacebookAlbum;
import com.pixplit.android.data.FacebookAlbums;
import com.pixplit.android.data.PixNotification;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.fb.FbAlbumPickDelegate;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.FbAlbumsHttpRequest;
import com.pixplit.android.ui.activities.FbPickAlbumDelegate;


public class FbPickAlbumFragment extends PixListFragment<PixNotification> implements FbAlbumPickDelegate{
	String fbRefreshUrl;
	FbPickAlbumDelegate mDelegate;
	
	public static FbPickAlbumFragment newInstance() {
		FbPickAlbumFragment fragment = new FbPickAlbumFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.FACEBOOK_PICK_ALBUM);
        fbRefreshUrl = PixHttpAPI.FACEBOOK_ALBUMS_URL;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.fb_pick_photo_layout, container, false);
		return mFragmentView;
	}
	
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mDelegate = (FbPickAlbumDelegate)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement FbPickAlbumDelegate");
	    }
    }
    
    @Override
	public void onResume() {
		super.onResume();
		if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(PixApp.getContext().getResources().getString(R.string.select_photo));
        	mToolbarController.setToolBarCustomView(R.drawable.import_facebook);
        }
	}
    
    @Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof FacebookAlbums) {
			FacebookAlbums items = (FacebookAlbums)response;
			updateAdapterData(items.getItems());
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new FbAlbumsHttpRequest(this, this.fbRefreshUrl, Priority.HIGH);
	}
    
    @Override
    protected void addNextParam(BaseVolleyHttpRequest request) {
    	// next page is handled internally by facebook url's mechanism
    }
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	FbAlbumsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new FbAlbumsListAdapter(getActivity(), this, this);
    	}
    	return adapter;
	}

	@Override
	public void onAlbumPick(FacebookAlbum album) {
		if (mDelegate == null || album == null) return;
		mDelegate.onAlbumPick(album);
	}
	
	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		super.requestFinishedWithError(request, error);
		if (mActivity != null) {
			mActivity.finishFragment();
		}
	}
}
