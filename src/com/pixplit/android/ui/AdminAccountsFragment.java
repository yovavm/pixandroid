package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.adapters.AccountsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixAccount;
import com.pixplit.android.data.PixAccounts;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminEurekaAccountsHttpRequest;
import com.pixplit.android.http.request.AdminSetAccountLimitHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.ui.AdminSetAccountMaxCampaigns.SetMaxCampaignsDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.viewholders.AdminAccountViewHolder.AccountViewDelegate;


public class AdminAccountsFragment extends PixListFragment<PixAccount> implements AccountViewDelegate {
	
	public static AdminAccountsFragment newInstance(Bundle args) {
		AdminAccountsFragment fragment = new AdminAccountsFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        setFragmentType(EFragmentType.ACCOUNTS);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.related_feed_layout, container, false);
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixAccounts) {
			PixAccounts accounts = (PixAccounts)response;
			ArrayList<PixAccount> accountsFeed = (ArrayList<PixAccount>) accounts.getItems();
			this.updateAdapterData(accountsFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new AdminEurekaAccountsHttpRequest(this);
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	AccountsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new AccountsListAdapter(getActivity(), this, R.layout.admin_account_layout, this);
    	}
    	return adapter;
	}

	@Override
	public void onAccountClick(final PixAccount account) {
		if (account == null) return;
		final int currentLimit = account.getMaxCampaigns();
		new AdminSetAccountMaxCampaigns(getActivity(), 
				new SetMaxCampaignsDelegate() {
					@Override
					public void onLimitSet(int limit) {
						if (limit == currentLimit) return;
						setAccountLimit(account, limit);
					}
				}, 
				currentLimit).show();
		
	}
	
	private void setAccountLimit(PixAccount account, int limit) {
		account.setMaxCampaigns(limit);
		PixProgressIndicator.showMessage(getActivity(), getContext().getString(R.string.updating_limit), true);
		new AdminSetAccountLimitHttpRequest(new RequestDelegate() {
			
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, getContext().getString(R.string.failed));
			}
			
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, getContext().getString(R.string.done));
				refreshList();
			}
		}, account, limit).send();
	}

	@Override
	public void onAccountLongClick(PixAccount account) {
		// TODO Auto-generated method stub
		
	}
    
}
