package com.pixplit.android.ui;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.capture.PixCameraPreview;
import com.pixplit.android.capture.SplitBorderType;
import com.pixplit.android.capture.SplitPreviewFrame;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.data.PixPrivateCompositionMetadata;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixCompositionType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.PixSplitPhotoPickerInterface;
import com.pixplit.android.ui.elements.ChooseFrameBar;
import com.pixplit.android.ui.elements.ChooseFrameClickListener.ChooseFrameDelegate;
import com.pixplit.android.ui.elements.FrameBtnInfo;
import com.pixplit.android.ui.elements.FrameInfo;
import com.pixplit.android.ui.elements.PickPhotoDialog;
import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.bitmap.PixBitmapUtils;

public class CreateSplitFragment extends PixBaseFragment implements PhotoFetcherDelegate{
	private static final String TAG = "CreateSplitFragment";
	
	private Camera mCamera;
	private int cameraId;
    private PixCameraPreview mCameraPreview;
    private SplitPreviewFrame mSplitPreview;
    private final int previewFrameId = 777; // for luck (-:
    private PixCompositionMetadata compMetadata;
    
    private ImageView captureBtn;
    private ImageView flashModeBtn;
    private ImageView switchCameraBtn;
    private ImageView pickPhotoBtn;
    private ImageView focusTargetIndicator;
    PixCompositionType mCompType = null;
    
    private int mPreviewFrameDimen;
    private boolean previewWasInit;
    
    ChooseFrameBar framesBar;
    ViewGroup framesBarContainer;
    
    ImageView changeFrameBtn;
    
    // save state
    private static final String CREATE_FRAG_ACTIVE_PART_INDEX_KEY = "CREATE_FRAG_ACTIVE_PART_INDEX_KEY";
    int activePartIndex = 0;
    
    // join split composition
    PixComposition mJoinedComp = null;
    
    private PixSplitPhotoPickerInterface mPhotoPickActivity;
    
    private static PixBitmapHolder joinedCompImage;
    public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
    	if (joinedImage != null) {
    		joinedImage.useBitmap(TAG);
    	}
    	if (joinedCompImage != null) {
    		joinedCompImage.releaseBitmap(TAG);
    	}
    	joinedCompImage = joinedImage;
    }
    
	public static CreateSplitFragment newInstance(Bundle args) {
		CreateSplitFragment fragment = new CreateSplitFragment();
        if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
            fragment.setArguments(args);
        }
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.CREATE_SPLIT);
        
        if (savedInstanceState != null) {
        	// restore previous state
        	this.activePartIndex = savedInstanceState.getInt(CREATE_FRAG_ACTIVE_PART_INDEX_KEY, 0);
        	this.mCompType = PixCompositionType.deserialize(savedInstanceState);
        	this.compMetadata = PixCompositionMetadata.deserialize(savedInstanceState);
        	this.mJoinedComp = PixComposition.deserialize(savedInstanceState);
        } else {
        	// initiate from arguments
            Bundle args = this.getArguments();
	        if (args != null) {
	        	int frameIntType = args.getInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, -1);
	        	if (frameIntType >= 0) {
	        		this.mJoinedComp = PixComposition.deserialize(args);
	        		PixCompositionFramesetType frameType = PixCompositionFramesetType.getType(frameIntType);
	        		if (frameType != null) {
	        			this.mCompType = new PixCompositionType(frameType, this.mJoinedComp);
	        		}
	        		if (this.mCompType != null) {
	                	compMetadata = new PixCompositionMetadata(this.mCompType.getCompFramesetType(), mJoinedComp);
	                	compMetadata.setPrivateCompMetadata(PixPrivateCompositionMetadata.deserialize(args));
	                }
	        	}
	        }
        }
        
        if (Utils.hasGingerbread()) {
        	this.cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        } else {
        	this.cameraId = 0;
        }
    }
    
    @SuppressLint("NewApi")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.create_split_layout, container, false);
    	if (mFragmentView != null) {
    		// capture button
	        captureBtn = (ImageView) mFragmentView.findViewById(R.id.captureBtn);
	        if (captureBtn != null) {
	        	captureBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onCaptureButtonClick();
					}
				});
	        }
	        			
	        // switch camera button
	        switchCameraBtn = (ImageView)mFragmentView.findViewById(R.id.switchCameraBtn);
			if (switchCameraBtn != null) {
				switchCameraBtn.setVisibility(View.GONE); //default
				if (Utils.hasGingerbread()) {
		        	if (Camera.getNumberOfCameras() > 1) {
		        		// rotate camera is only visible if applicable 
		        		switchCameraBtn.setVisibility(View.VISIBLE);
		        		switchCameraBtn.setOnClickListener(new OnClickListener() {
		    				@Override
		    				public void onClick(View v) {
		    					rotateCamera();
		    				}
		    			});
		        	}
		        }
			}
			
	        // flash button
	        flashModeBtn = (ImageView)mFragmentView.findViewById(R.id.flashModeBtn);
			if (flashModeBtn != null) {
				flashModeBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						toggleCameraFlash();
					}
				});
			}
	        
			// pick photo from library button
			pickPhotoBtn = (ImageView)mFragmentView.findViewById(R.id.pickPhotoBtn);
			if (pickPhotoBtn != null) {
				pickPhotoBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mPhotoPickActivity != null) {
							new PickPhotoDialog(getActivity(), mPhotoPickActivity).show();
						}
					}
				});
			}
			
	        // add border to split
	        ImageView addBorder = (ImageView) mFragmentView.findViewById(R.id.addBorder);
	        framesBarContainer = (ViewGroup)mFragmentView.findViewById(R.id.createSplitBottomLeftBar);
	        if (addBorder != null) {
				if (mJoinedComp != null) {
		    		addBorder.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							onBorderBtnClick();
						}
					});
				} else {
					changeFrameBtn = addBorder;
					if (this.mCompType != null &&
							this.mCompType.getCompFramesetType() != null) {
						FrameBtnInfo frameInfo = FrameInfo.getInfo(
								PixCompositionFramesetType.getInt(this.mCompType.getCompFramesetType()));
						if (frameInfo != null) {
							changeFrameBtn.setImageResource(frameInfo.getDrawableRes());
						}
					}
					changeFrameBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							onChangeFrameClick();
						}
					});					
				}
	    	}
	        
	        focusTargetIndicator = (ImageView) mFragmentView.findViewById(R.id.focusTargetIndicator);
    	}
    	return mFragmentView;
    }
    
    @Override
	public void onResume() {
    	super.onResume();
    	if (mToolbarController != null) {
    		mToolbarController.setToolbarVisible(false);
    	}
    	// Delay the creation of the camera and surface in order to open the fragment faster
    	new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
			@Override
			public void run() {
				CreateSplitFragment.this.getCameraInstance(CreateSplitFragment.this.cameraId);
				if (mCamera != null) {
					try {
						mCamera.startPreview();
					} catch (Exception e) {
						//Utils.pixLog(TAG, "Error starting camera preview: " + e.getMessage());
					}
				}
			}
		}, 250);
        if (captureBtn != null) {
			captureBtn.setEnabled(true);
		}
        if (this.mSplitPreview != null) {
        	this.mSplitPreview.show();
        }
        // handle cases where joining a split and the joined image is missing - may happen in case 
        // the app was closed and reopened by the system while in create process
        if (mJoinedComp != null) {
    		if (joinedCompImage == null ||
    				joinedCompImage.getBitmap() == null) {
    			if (mActivity != null) {
    				mActivity.finishFragment();
    			}
    		}
        }
    }
    
    @Override
	public void onPause() {
        super.onPause();
        releaseCamera(); // release the camera immediately on pause event
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (outState != null) {
    		if (this.mSplitPreview != null) {
    			this.activePartIndex = this.mSplitPreview.getActivatePartIndex();
    		}
    		outState.putInt(CREATE_FRAG_ACTIVE_PART_INDEX_KEY, activePartIndex);
    		
    		if (this.mCompType != null) {
    			this.mCompType.serialize(outState);
        	}
        	if (this.compMetadata != null) {
        		this.compMetadata.serialize(outState);
        	}
        	if (this.mJoinedComp != null) {
        		this.mJoinedComp.serialize(outState);
        	}
    	}
    }
    
    private void onBorderBtnClick() {
		if (mSplitPreview != null && compMetadata != null) {
			compMetadata.setHasBorder(!compMetadata.hasBorder());
			mSplitPreview.setBorder(compMetadata.hasBorder() ? SplitBorderType.BORDER_TYPE_WIDE 
															 : SplitBorderType.BORDER_TYPE_DEFAULT);
		}
    }
    
    private void onChangeFrameClick() {
    	if (framesBar == null) {
    		framesBar = new ChooseFrameBar(getActivity(), 
							new ChooseFrameDelegate() {
								@Override
								public void onFrameChoosen(FrameBtnInfo frameInfo) {
									if (frameInfo == null) return;
									PixCompositionFramesetType frameType = frameInfo.getFrameType();
									if (frameType == null) return;
									// can not change frame on joined split
									if (mJoinedComp != null) return;
									
									// do nothing if existing frame chosen
									if (mCompType != null && 
											mCompType.getCompFramesetType() == frameType) return;
									
									// log event
									HashMap<String, String> context = new HashMap<String, String>();
						    		context.put(PixDefs.APP_PARAM_FRAME_NAME, frameType.name());
									PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_STARTED, PixDefs.APP_EVENT_FRAME_SELECTED, context);
									
									mCompType = new PixCompositionType(frameType, null);
									compMetadata.setCompType(frameType);
									SplitBorderType borderType = compMetadata.hasBorder() ? SplitBorderType.BORDER_TYPE_WIDE 
																						  : SplitBorderType.BORDER_TYPE_DEFAULT;
									SplitPreviewFrame newSplitPreview = new SplitPreviewFrame(getActivity(), 
											mPreviewFrameDimen, mPreviewFrameDimen, mCompType, borderType, null);
									setSplitPreview(newSplitPreview);
									
									if (changeFrameBtn != null) {
										changeFrameBtn.setImageResource(frameInfo.getDrawableRes());
									}
								}
							},
							(compMetadata.isPrivate()) ? PixDefs.privateSplitFrameBtns : PixDefs.defaultSplitFrameBtns);
			if (framesBarContainer != null) {
				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 
						LayoutParams.WRAP_CONTENT);
				framesBarContainer.addView(framesBar, params);
			}
		} else {
			if (framesBar.getVisibility() == View.VISIBLE) {
				framesBar.hide();
			} else {
				framesBar.show();
			}
		}
    }
    
    private void setSplitPreview(SplitPreviewFrame splitPreview) {
    	if (mFragmentView != null) {
    		// remove previous split preview instance
    		View currentSplitPreview = mFragmentView.findViewById(previewFrameId);
    		if (currentSplitPreview != null) {
    			((ViewGroup)mFragmentView).removeView(currentSplitPreview);
    		}
    		mSplitPreview = splitPreview;
	    	mSplitPreview.setId(previewFrameId);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mPreviewFrameDimen, mPreviewFrameDimen);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
			((ViewGroup)mFragmentView).addView(mSplitPreview, params);
    	}
    }
    
    private void onCaptureButtonClick() {
    	// get an image from the camera
		if (mCamera != null) {
			try {
				if (this.captureBtn != null) {
					this.captureBtn.setEnabled(false);
				}
				if (this.mSplitPreview != null) {
					this.mSplitPreview.hide();
				}
				mCamera.takePicture(shutterCallback, null, mPictureCallback);
			} catch (Exception e) {
				//Utils.pixLog(TAG, "take picture failed."+e);
				e.printStackTrace();
			}
		}
    }
    
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mPhotoPickActivity = (PixSplitPhotoPickerInterface)activity;
	    } catch (ClassCastException e) {
	    	//Utils.pixLog(TAG, activity.toString() +" must implement PixPhotoPickerInterface");
	    	throw new ClassCastException(activity.toString() +" must implement PixPhotoPickerInterface");
	    }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.registerPhotoPickerDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.unregisterPhotoPickerDelegate();
    	}
    }
    
    private void rotateCamera() {
    	if (this.cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
    		this.cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
		} else {
			this.cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
    	this.getCameraInstance(this.cameraId);
    }
    
    private void releaseCamera(){
        if (mCamera != null){
        	try {
                mCamera.stopPreview();
            } catch (Exception e){
              // ignore: tried to stop a non-existent preview
            }
            mCamera.release();        // release the camera for other applications
            mCamera = null;
            
            // remove the preview surface
            if (mFragmentView != null && mCameraPreview != null) {
            	((ViewGroup)mFragmentView).removeView(mCameraPreview);
	        }
        }
    }
    
	/** A safe way to get an instance of the Camera object. */
	@SuppressLint("NewApi")
	private void getCameraInstance(int cameraId){
	    if (mCamera != null) {
	    	this.releaseCamera();
	    }
	    try {
	    	// attempt to get a Camera instance
	    	if (Utils.hasGingerbread()) {
	    		mCamera = Camera.open(cameraId);
	    	} else {
	    		mCamera = Camera.open();
	    	}
	    	
	    	if (mCamera != null && getActivity() != null) {
	    		this.mPreviewFrameDimen = PixAppState.getState().getSplitPreivewWidth();
	    		this.initPreview();
	    		
				this.updateCameraFlashIcon();
				
				// remove old preview if exists
				if (mCameraPreview != null && mFragmentView != null) {
		        	((RelativeLayout)mFragmentView).removeView(mCameraPreview);
		        }
				// Create our Preview view and set it as the content of the fragment.
		        mCameraPreview = new PixCameraPreview(getActivity(), 
		        		mCamera, cameraId, this.mPreviewFrameDimen, focusTargetIndicator);
		        if (mFragmentView != null) {
		        	((RelativeLayout)mFragmentView).addView(mCameraPreview, 0);
		        }
	        }
	    }
	    catch (Exception e){
	        // Camera is not available (in use or does not exist)
	    }
	}
	
	private PictureCallback mPictureCallback = new PictureCallback() {

	    @TargetApi(10)
		@Override
	    public void onPictureTaken(final byte[] data, Camera camera) {
			Bitmap splitPartBmp = null;
	    	float scaleFactor = 1;
	    	// validations 
	    	if (compMetadata == null || mCompType == null || mSplitPreview == null) {
	    		//Utils.pixLog(TAG, "missing mandatory parameters!");
	    		return;
	    	}
	    	
	    	try { 
		    	// decode the captured bitmap
		    	// First decode with inJustDecodeBounds=true to check dimensions
		        final BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inJustDecodeBounds = true;
		        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        BitmapFactory.decodeByteArray(data, 0, data.length, options);

		        // Calculate inSampleSize
		        options.inSampleSize = PixBitmapUtils.calculateInSampleSize(options, 
		        		PixDefs.SPLIT_BITMAP_MIN_SCALE_WIDTH, PixDefs.SPLIT_BITMAP_MIN_SCALE_HIGHT);
		        
		        // Decode bitmap with inSampleSize set
		        options.inJustDecodeBounds = false;
		        options.inScaled = false;
		        if (Utils.hasGingerbreadMR1()) {
		        	options.inPreferQualityOverSpeed = true;
		        }
		        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        Bitmap origCapturedBmp = BitmapFactory.decodeByteArray(data, 0, data.length, options);
		        
		        Bitmap capturedBmp;
		        if (Utils.hasGingerbread() && cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
		        	Bitmap flippedBitmap = Utils.rotateBitmap(origCapturedBmp, 270);
		        	if (PixDefs.recycleBitmaps) {
			        	origCapturedBmp.recycle();
			        	origCapturedBmp = null;
		        	}
			        
		        	capturedBmp = Utils.flipBitmap(flippedBitmap, true);
		        	if (PixDefs.recycleBitmaps) {
			        	flippedBitmap.recycle();
			        	flippedBitmap = null;
		        	}
		        } else {
		        	capturedBmp = Utils.rotateBitmap(origCapturedBmp, 90);
		        	if (PixDefs.recycleBitmaps) {
			        	origCapturedBmp.recycle();
				        origCapturedBmp = null;
		        	}
		        }
		        
//			    		Utils.pixLog(TAG, "Bitmap dimensions. width:"+capturedBmp.getWidth()+",height:"+capturedBmp.getHeight());
	    		
//			    		Utils.pixLog(TAG, "mCameraPreview. left:" + mCameraPreview.getLeft() + 
//			    									", top:" + mCameraPreview.getTop() +
//			    									", width:" + mCameraPreview.getWidth() + 
//			    									", height:" + mCameraPreview.getHeight());
	    		
//			    		Utils.pixLog(TAG, "splitPreviewFrame locations. left:" + mSplitPreview.getLeft() +
//			    									", top:" + mSplitPreview.getTop() +
//			    									", width:"+mSplitPreview.getWidth() +
//			    									", height:"+mSplitPreview.getHeight());
		        
		        
	        
	    		Rect previewActiveRect = mSplitPreview.getActivePartDisplayRelativeRect();
	    		//Utils.pixLog(TAG, "previewActiveRect:"+ previewActiveRect.left+","+previewActiveRect.top+","+previewActiveRect.width()+","+previewActiveRect.height());
	    		
	    		// use same scale factor on both dimensions
	    		float heightScaleFactor = ((float) capturedBmp.getHeight()) / mCameraPreview.getHeight(); 
	    		float widthScaleFactor = ((float) capturedBmp.getWidth()) / mCameraPreview.getWidth();
	    		//Utils.pixLog(TAG, "heightScaleFactore:"+ heightScaleFactor+",widthScaleFactore:"+widthScaleFactor);
	    		scaleFactor = Math.min(widthScaleFactor, heightScaleFactor);
	    		int x = (int)Math.round((previewActiveRect.left + mSplitPreview.getLeft() - mCameraPreview.getLeft()) * scaleFactor);
	    		int y = (int)Math.round((previewActiveRect.top+mSplitPreview.getTop() - mCameraPreview.getTop()) * scaleFactor);
	    		int w = (int)Math.round(previewActiveRect.width() * scaleFactor);
	    		int h = (int)Math.round(previewActiveRect.height() * scaleFactor);
	
    			try {
		    		//Utils.pixLog(TAG, "cut bitmap:"+x+","+y+","+w+","+h);
	    			splitPartBmp = Bitmap.createBitmap(capturedBmp, x, y, w, h);
	    		} catch (Exception e){
	    			//Utils.pixLog(TAG, "createBitmap failed");
	    			e.printStackTrace();
	    			// should not continue as the part's bitmap was not created
	    			return;
	    		}
		    }
	    	catch (Exception e) { 
	    		//Utils.pixLog(TAG, "Got exception while creating the captured bitmap:"+e);
	    	}

	    	Bundle args = new Bundle();
	    	if (mCompType != null) {
	    		mCompType.serialize(args);
	    	}
	    	if (compMetadata != null) {
	    		compMetadata.setNewComponentIndex(mSplitPreview.getActivatePartIndex());
	    		compMetadata.serialize(args);
	    	}
			args.putInt(PixDefs.FRAG_ARG_SPLIT_PREVIEW_DIMEN, CreateSplitFragment.this.mPreviewFrameDimen);
	    	
	    	HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARAM_CAMERA_SOURCE, "camera");
        	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CUSTOMIZATION, PixDefs.APP_EVENT_PHOTO_ADDED, context);
        	
			if (mActivitiesController != null) {
				PixBitmapHolder splitPartBmpHolder = new PixBitmapHolder(splitPartBmp);
				mActivitiesController.gotoProcessNewSplit(getActivity(), args, joinedCompImage, splitPartBmpHolder);
			}
			return;
		}
	};

	private void initPreview() {
		if (this.previewWasInit) return;  
		// init the preview frame
		initPreviewFrame();
		this.previewWasInit = true;
	}
	
	private void initPreviewFrame() {
		if (getActivity() == null || 
				mFragmentView == null ||
				mCompType == null || 
				compMetadata == null) return;

		// camera preview
		SplitBorderType borderType = compMetadata.hasBorder() ? SplitBorderType.BORDER_TYPE_WIDE : SplitBorderType.BORDER_TYPE_DEFAULT;
		SplitPreviewFrame splitPreviewFrame  = new SplitPreviewFrame(getActivity(), 
				this.mPreviewFrameDimen, 
				this.mPreviewFrameDimen, 
				this.mCompType, 
				borderType, 
				(joinedCompImage != null) ? joinedCompImage.getBitmap() : null);
		if (this.activePartIndex > 0) {
			// the fragment is re-created and there is saved state of the active part to init
			splitPreviewFrame.activateSplitPart(this.activePartIndex);
		}
		this.setSplitPreview(splitPreviewFrame);
		
		// surround the preview frame with black
		int backgroundColor = R.color.create_process_bg;
		ImageView blackRectFrameLayout = new ImageView(getActivity());
		blackRectFrameLayout.setClickable(true); // in order to catch tap to focus events outside of the visible frame
		blackRectFrameLayout.setBackgroundResource(backgroundColor);
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.LEFT_OF, previewFrameId);
		params.addRule(RelativeLayout.BELOW, R.id.createSplitTopBar);
		params.addRule(RelativeLayout.ABOVE, R.id.createSplitBottomBar);
		((ViewGroup)mFragmentView).addView(blackRectFrameLayout, params);
		
		blackRectFrameLayout = new ImageView(getActivity());
		blackRectFrameLayout.setClickable(true); // in order to catch tap to focus events outside of the visible frame
		blackRectFrameLayout.setBackgroundResource(backgroundColor);
		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.RIGHT_OF, previewFrameId);
		params.addRule(RelativeLayout.BELOW, R.id.createSplitTopBar);
		params.addRule(RelativeLayout.ABOVE, R.id.createSplitBottomBar);
		((ViewGroup)mFragmentView).addView(blackRectFrameLayout, params);
		
		blackRectFrameLayout = new ImageView(getActivity());
		blackRectFrameLayout.setClickable(true); // in order to catch tap to focus events outside of the visible frame
		blackRectFrameLayout.setBackgroundResource(backgroundColor);
		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, R.id.createSplitTopBar);
		params.addRule(RelativeLayout.ABOVE, previewFrameId);
		((ViewGroup)mFragmentView).addView(blackRectFrameLayout, params);
    	
		blackRectFrameLayout = new ImageView(getActivity());
		blackRectFrameLayout.setClickable(true); // in order to catch tap to focus events outside of the visible frame
		blackRectFrameLayout.setBackgroundResource(backgroundColor);
		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.BELOW, previewFrameId);
		params.addRule(RelativeLayout.ABOVE, R.id.createSplitBottomBar);
		((ViewGroup)mFragmentView).addView(blackRectFrameLayout, params);
	
	}
	private void toggleCameraFlash() {
		if (mCamera != null) {
			try {
				Camera.Parameters cp = mCamera.getParameters();
				String currentfalshMode = cp.getFlashMode();
				if (currentfalshMode != null) {
					String nextFlashMode;
					if (currentfalshMode.equals(Camera.Parameters.FLASH_MODE_ON)) {
						nextFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
					} else if (currentfalshMode.equals(Camera.Parameters.FLASH_MODE_AUTO)) {
						nextFlashMode = Camera.Parameters.FLASH_MODE_OFF;
					} else {
						nextFlashMode = Camera.Parameters.FLASH_MODE_ON;
					}
					
				    cp.setFlashMode(nextFlashMode);
			    	mCamera.setParameters(cp);
				}
			} catch (Exception e) {
		    	//Utils.pixLog(TAG, "failed to set flasg parameters. "+e);
		    	e.printStackTrace();
		    }
		}
		updateCameraFlashIcon();
	}
	
	private void updateCameraFlashIcon() {
		if (flashModeBtn != null && mCamera != null) {
			try {
				Camera.Parameters cp = mCamera.getParameters();
				String currentfalshMode = cp.getFlashMode();
				if (currentfalshMode != null) {
					if (currentfalshMode.equals(Camera.Parameters.FLASH_MODE_ON)) {
						flashModeBtn.setBackgroundResource(R.drawable.capture_flash_btn_selector);
					} else if (currentfalshMode.equals(Camera.Parameters.FLASH_MODE_AUTO)) {
						flashModeBtn.setBackgroundResource(R.drawable.capture_flash_auto_btn_selector);
					} else {
						flashModeBtn.setBackgroundResource(R.drawable.capture_flash_off_btn_selector);
					}
				}
			} catch (Exception e) {
		    	//Utils.pixLog(TAG, "failed to set parameters. "+e);
		    	e.printStackTrace();
		    }
		}
	}
		
//	private void pickPhotoFromLibrary() {
//		if (mPhotoPickActivity != null) {
//			mPhotoPickActivity.photoPickRequest();
//		}
//	}
	
	@Override
	public void onPhotoFetchSuccess(final Bitmap selectedImage) {
		if (this.mSplitPreview != null) {
			this.mSplitPreview.hide();
		}
		HashMap<String, String> context = new HashMap<String, String>();
		context.put(PixDefs.APP_PARAM_CAMERA_SOURCE, "gallery");
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CUSTOMIZATION, PixDefs.APP_EVENT_PHOTO_ADDED, context);
    	
    	Bundle args = new Bundle();
    	if (mCompType != null) {
    		mCompType.serialize(args);
    	}
    	if (compMetadata != null) {
			compMetadata.setNewComponentIndex(activePartIndex);
			compMetadata.serialize(args);
			args.putInt(PixDefs.FRAG_ARG_SPLIT_PREVIEW_DIMEN, mPreviewFrameDimen);
    	}
		if (mActivitiesController != null) {
			PixBitmapHolder selectedImageHolder = new PixBitmapHolder(selectedImage);
			mActivitiesController.gotoScalePhotoInSplit(getActivity(), args, joinedCompImage, selectedImageHolder);
		}
	}

	@Override
	public void onPhotoFetchFail() {
		// Do nothing, the view is already populated by the camera
	}
	
	private final ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        	if (getActivity() != null) {
	            AudioManager mgr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
	            if (mgr != null) {
	            	mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
	            }
        	}
        }
    };
    
	@Override
	public boolean onBackPressed() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, PixDefs.APP_EVENT_CANCEL_PRESSED, null);
		return false; // don't consume the back event
	}    
}
