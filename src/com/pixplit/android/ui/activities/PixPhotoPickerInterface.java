package com.pixplit.android.ui.activities;

import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;


public interface PixPhotoPickerInterface {
	public void registerPhotoPickerDelegate(PhotoFetcherDelegate delegate);
	public void unregisterPhotoPickerDelegate();
	public void photoPickRequest();
	public void photoCaptureRequest();
}
