package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.ActivitiesControllerInterface;
import com.pixplit.android.ui.fragments.ProfileFragment;


public class ProfileActivity extends PixCompFeedActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.updateToolbar();

        if (savedInstanceState == null) {
	        Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	ProfileFragment fragment = ProfileFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
        
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (PixDefs.ACTIVITY_RESULT_CODE_EDIT_PROFILE == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_PROFILE_WAS_EDITED, false)) {
        			if (mActivitiesController != null) {
        				// mark this (my) profile activity for refresh as the data had been changed.
        				mActivitiesController.markForRefresh(ActivitiesControllerInterface.REFRESH_CONTEXT_PROFILE_EDITED, true);
        			}
        		}
        	}
        }
	}
}
