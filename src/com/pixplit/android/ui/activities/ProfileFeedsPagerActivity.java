package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.ui.GalleryFeedFragment;
import com.pixplit.android.ui.elements.ProfileTabPageIndicator;
import com.viewpagerindicator.IconPagerAdapter;


public class ProfileFeedsPagerActivity extends PixCompFeedActivity {
	
	private static final int PROFILE_TABS_NUM = 3;
	public static final int FFED_TYPE_COMPLETED = 0;
	public static final int FFED_TYPE_OPEN = 1;
	public static final int FFED_TYPE_LIKED = 2;
	
	private ViewPager mPager;
    private ProfileTabsAdapter mAdapter;
    private ProfileTabPageIndicator mIndicator;
    
    private String username;
    private int initFeed = FFED_TYPE_COMPLETED; // default
    
    private GalleryFeedFragment[] profileFeedFragments;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_gallery_feed_layout);
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        if (args != null) {
	        	initFeed = args.getInt(PixDefs.FRAG_ARG_GALLERY_FEED_TYPE);
	        	username = args.getString(PixDefs.FRAG_ARG_GALLERY_PROFILE_USERNAME);
	        	String title = args.getString(PixDefs.FRAG_ARG_GALLERY_TITLE);
	        	setToolBarTitle(title);
	        	
	        	Bundle defaultArgs = new Bundle(); // used for not-current tabs
	        	defaultArgs.putInt(PixDefs.FRAG_ARG_GALLERY_COMP_INDEX, 0);
	        	defaultArgs.putString(PixDefs.FRAG_ARG_GALLERY_TITLE, title);
	        	defaultArgs.putString(PixDefs.FRAG_ARG_GALLERY_NEXT_PAGE, "1");
	        	defaultArgs.putBoolean(PixDefs.FRAG_ARG_GALLERY_ALLOW_PAGING, true);
	        	defaultArgs.putBoolean(PixDefs.FRAG_ARG_GALLERY_INIT_WITH_COMPS, false);
	        	
	        	// create the tabs fragments
	        	profileFeedFragments = new GalleryFeedFragment[PROFILE_TABS_NUM];
	        	for (int i=0; i<PROFILE_TABS_NUM; i++) {
	        		if (i == initFeed) {
	        			profileFeedFragments[i] = GalleryFeedFragment.newInstance(args);
	        		} else {
	        			switch (i) {
						case FFED_TYPE_COMPLETED:
							defaultArgs.putString(PixDefs.FRAG_ARG_GALLERY_FEED_URL, PixHttpAPI.USER_COMPLETED_FEED_URL + username);
							break;
						case FFED_TYPE_OPEN:
							defaultArgs.putString(PixDefs.FRAG_ARG_GALLERY_FEED_URL, PixHttpAPI.USER_NONCOMPLETED_FEED_URL + username);
							break;
						case FFED_TYPE_LIKED:
							defaultArgs.putString(PixDefs.FRAG_ARG_GALLERY_FEED_URL, PixHttpAPI.USER_LIKED_FEED_URL + username);
							break;
						default:
							break;
						}
	        			profileFeedFragments[i] = GalleryFeedFragment.newInstance(defaultArgs);
	        		}
	        	}
	        }
        }
        
        mAdapter = new ProfileTabsAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        if (mPager != null) {
        	mPager.setAdapter(mAdapter);
        	mPager.setOffscreenPageLimit(PROFILE_TABS_NUM); // don't destroy the fragments
        }
        
        mIndicator = (ProfileTabPageIndicator)findViewById(R.id.indicator);
        if (mIndicator != null) {
        	mIndicator.setViewPager(mPager);
        }
        
        if (mPager != null) {
        	mPager.setCurrentItem(initFeed);
        }
    }
	
	class ProfileTabsAdapter extends FragmentPagerAdapter implements IconPagerAdapter{
        public ProfileTabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	return profileFeedFragments[position];
        }
        
        @Override
        public CharSequence getPageTitle(int position) {
        	String title = "";
        	switch (position) {
    		case FFED_TYPE_COMPLETED:
    			title = "Completed";
    			break;
    		case FFED_TYPE_OPEN:
    			title = "Open";
    			break;
    		case FFED_TYPE_LIKED:
    			title = "Liked";
    			break;
    		default:
    			break;
    		}
            return title;
        }

        @Override
        public int getCount() {
            return PROFILE_TABS_NUM;
        }

		@Override
		public int getIconResId(int index) {
			int iconRes = 0;
			switch (index) {
    		case FFED_TYPE_COMPLETED:
    			iconRes = R.drawable.profile_completed_btn_selector;
    			break;
    		case FFED_TYPE_OPEN:
    			iconRes = R.drawable.profile_open_btn_selector;
    			break;
    		case FFED_TYPE_LIKED:
    			iconRes = R.drawable.profile_liked_btn_selector;
    			break;
    		default:
    			break;
    		}
			return iconRes;
		}
    }
}