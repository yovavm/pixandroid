package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.os.Handler;

import com.bugsense.trace.BugSenseHandler;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.ActivitiesControllerInterface;

public class SplashActivity extends PixBaseActivity {
	private final static int SPLASH_DELAY = 1000;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity_layout);
        setToolbarVisible(false);
    
        BugSenseHandler.initAndStartSession(this, "f40ae0ba");
        if (UserInfo.isUserLogedin()) {
    		// init the push notifications
    		PixApp.enablePushNotifications();
    	} 
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (!PixAppState.didTipDisplayed(PixDefs.QUICK_GUIDE_DISPLAYED)) {
			if (mActivitiesController != null) {
				mActivitiesController.gotoQuickGuide(this);
			}
			PixAppState.setTipDisplayed(PixDefs.QUICK_GUIDE_DISPLAYED, true);
		} else {			
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if (UserInfo.isUserLogedin()) {
						if (mActivitiesController != null) {
							mActivitiesController.gotoMyFeed(SplashActivity.this, 
									ActivitiesControllerInterface.PIX_ANIMATE_SYSTEM_DEFAULT_ANIMATION);
			        	} 
					} else {
		        		if (mActivitiesController != null) {
			    			mActivitiesController.gotoPopularRequest(SplashActivity.this, null);
			    		}
		        	}
					
				}
			}, SPLASH_DELAY);
		}
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
