package com.pixplit.android.ui.activities;

import com.pixplit.android.ui.PixCompFeedDelegate;

public interface PixCompFeedControllerInterface {
	public void registerCompFeedDelegate(PixCompFeedDelegate delegate);
	public void unregisterCompFeedDelegate(PixCompFeedDelegate delegate);
	
}
