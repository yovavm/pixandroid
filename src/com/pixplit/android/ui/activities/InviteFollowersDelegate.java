package com.pixplit.android.ui.activities;

public interface InviteFollowersDelegate {
	public void onInviteFollowersResult(String users);
}
