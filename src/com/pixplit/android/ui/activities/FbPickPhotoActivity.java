package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.data.FacebookAlbum;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.FbPickAlbumFragment;
import com.pixplit.android.ui.FbPickPhotoFragment;
import com.pixplit.android.ui.elements.PixBoldTextView;


public class FbPickPhotoActivity extends PixFbBaseActivity implements FbPickAlbumDelegate {
	PixBoldTextView title;
	ImageView logo;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_photo_activity);
        logo = (ImageView)findViewById(R.id.importLogo);
        title = (PixBoldTextView)findViewById(R.id.importTitle);
        
        if (savedInstanceState == null) {
        	if (mFragmentManager != null) {
        		if (mFragmentManager.findFragmentByTag(EFragmentType.FACEBOOK_PICK_ALBUM.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		FbPickAlbumFragment fragment = FbPickAlbumFragment.newInstance();
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.FACEBOOK_PICK_ALBUM.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when finish
    }
	
	@Override
	public void onAlbumPick(FacebookAlbum album) {
		if (album == null) return;
		
		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
		Bundle args = new Bundle();
		args.putString(PixDefs.FRAG_ARG_ALBUM_ID, album.getId());
		FbPickPhotoFragment fragment = FbPickPhotoFragment.newInstance(args);
		ft.setTransition(FragmentTransaction. TRANSIT_FRAGMENT_OPEN);
		ft.add(R.id.fragment_container, fragment, EFragmentType.FACEBOOK_PICK_PHOTO.toString());
		ft.addToBackStack(null);
    	ft.commit();
	}
	
	@Override
	public void setToolBarTitle(String title) {
		if (this.title != null) {
        	this.title.setText(title);
        }
	}
	
	@Override
	public void setToolBarCustomView(int resId) {
		if (this.logo != null) {
			this.logo.setImageResource(resId);
        }
	}
}
