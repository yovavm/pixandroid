package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.PublishNewSplitFragment;
import com.pixplit.android.util.bitmap.PixBitmapHolder;


public class PublishNewSplitActivity extends PixFbBaseActivity implements InviteFollowersDelegateInterface{
	
	InviteFollowersDelegate mInviteFollowersDelegate;
	
	public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
		PublishNewSplitFragment.setJoinedCompImage(joinedImage);
    }
	public static void setActivePartOrigBitmap(PixBitmapHolder partBitmap) {
		PublishNewSplitFragment.setActivePartOrigBitmap(partBitmap);
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.updateToolbar();
        this.setFetchNotifications(false);

        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	if (mFragmentManager.findFragmentByTag(EFragmentType.PUBLISH_SPLIT.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		PublishNewSplitFragment publishFragment = PublishNewSplitFragment.newInstance(args);
		        	ft.add(R.id.fragment_container, publishFragment, EFragmentType.PUBLISH_SPLIT.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if (PixDefs.ACTIVITY_RESULT_CODE_INVITE_FOLLOWERS == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle resultBundle = data.getExtras();
        		if (mInviteFollowersDelegate != null) {
        			mInviteFollowersDelegate.onInviteFollowersResult(resultBundle.getString(PixDefs.INTENT_EXTRA_INVITE_FOLLOWERS_LIST));
            	}
        	}
        }
	}
	
	@Override
    public void finish() {
        super.finish();
        // override transitions to skip the standard window animations
        overridePendingTransition(0, 0);
    }

	@Override
	public void registerInviteFollowersDelegate(InviteFollowersDelegate delegate) {
		mInviteFollowersDelegate = delegate;
	}

	@Override
	public void unregisterInviteFollowersDelegate() {
		mInviteFollowersDelegate = null;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// release the bitmaps static references
		PublishNewSplitFragment.setJoinedCompImage(null);
		PublishNewSplitFragment.setActivePartOrigBitmap(null);
	}
}
