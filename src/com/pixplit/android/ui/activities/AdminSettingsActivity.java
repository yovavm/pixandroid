package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.AdminSettingsFragment;

public class AdminSettingsActivity extends PixBaseActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolBarTitle(getApplicationContext().getString(R.string.admin_settings));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	AdminSettingsFragment fragment = AdminSettingsFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
    }
}
