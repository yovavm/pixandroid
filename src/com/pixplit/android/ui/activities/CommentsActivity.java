package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.CommentsFragment;


public class CommentsActivity extends PixFbBaseActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.setToolBarTitle(getApplicationContext().getString(R.string.comments_caps));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	CommentsFragment fragment = CommentsFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
        
	}
}
