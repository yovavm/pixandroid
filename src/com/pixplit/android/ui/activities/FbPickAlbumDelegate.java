package com.pixplit.android.ui.activities;

import com.pixplit.android.data.FacebookAlbum;

public interface FbPickAlbumDelegate {
	public void onAlbumPick(FacebookAlbum album);
}
