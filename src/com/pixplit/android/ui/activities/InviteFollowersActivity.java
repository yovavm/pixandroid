package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.InviteFollowersFragment;

public class InviteFollowersActivity extends PixFbBaseActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolBarTitle(getApplicationContext().getString(R.string.invite_followers));
        this.updateToolbar();
        this.setFetchNotifications(false);
        
        Bundle args = null;
        Intent callingIntent = getIntent();
        if (callingIntent != null) {
        	args = callingIntent.getExtras();
        }
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	InviteFollowersFragment inviteFollowersFragment = InviteFollowersFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, inviteFollowersFragment);
	        	ft.commit();
	        }
        }
    }
	
	@Override
    public void finish() {
    	super.finish();
    	this.overridePendingTransition(R.anim.hold_in_place, R.anim.slide_top_bottom);
    }
}
