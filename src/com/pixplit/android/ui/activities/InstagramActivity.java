package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.InstagramPickPhotoFragment;
import com.pixplit.android.ui.elements.PixBoldTextView;


public class InstagramActivity extends PixFbBaseActivity {
	PixBoldTextView title;
	ImageView logo;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_photo_activity);
        logo = (ImageView)findViewById(R.id.importLogo);
        if (logo != null) {
        	logo.setBackgroundResource(R.drawable.import_instagram);
        }
        title = (PixBoldTextView)findViewById(R.id.importTitle);
        if (title != null) {
        	title.setText(getString(R.string.select_photo));
        }
        
        if (savedInstanceState == null) {
        	if (mFragmentManager != null) {
        		if (mFragmentManager.findFragmentByTag(EFragmentType.INSTAGRAM_PICK_PHOTO.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		InstagramPickPhotoFragment fragment = InstagramPickPhotoFragment.newInstance();
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.INSTAGRAM_PICK_PHOTO.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
