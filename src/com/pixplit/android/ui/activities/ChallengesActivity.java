package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.ChallengesFragment;


public class ChallengesActivity extends PixCompFeedActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.updateToolbar();

    	if (mFragmentManager != null) {
        	if (mFragmentManager.findFragmentByTag(EFragmentType.CHALLENGES.toString()) == null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	ChallengesFragment fragment = ChallengesFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment, EFragmentType.CHALLENGES.toString());
	        	ft.commit();
        	}
        }
	}
}
