package com.pixplit.android.ui.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.BaseActivityDelegate;
import com.pixplit.android.ui.MyCompletedFragment;
import com.pixplit.android.ui.MyFeedControlDelegate;
import com.pixplit.android.ui.MyFeedControllerInterface;
import com.pixplit.android.ui.MyPlaygroundFragment;
import com.pixplit.android.ui.PixCompFeedDelegate;
import com.pixplit.android.ui.elements.TabPageIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.appreciation.AppreciationManager;
import com.sbstrm.appirater.Appirater;


public class MyFeedActivity extends PixCompFeedActivity implements MyFeedControllerInterface{

	private static final int MY_FEED_TABS_NUM = 2;
	private static final int PLAYGOUND_TAB_INDEX = 0;
	private static final int COMPLETED_TAB_INDEX = 1;
	
    ArrayList<MyFeedControlDelegate> mMyFeedDelegates;

    private ViewPager mPager;
    private FragmentPagerAdapter mAdapter;
    private TabPageIndicator mIndicator;
    
    // Appreciation manager
    private AppreciationManager mAppreciationManager = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_feed_tabs_layout);
        this.setToolBarTitle(getApplicationContext().getString(R.string.my_feed));
        this.updateToolbar();
        this.mAppreciationManager = AppreciationManager.getAppreciationManager(this);
        
        mMyFeedDelegates = new ArrayList<MyFeedControlDelegate>();
        
        mAdapter = new MyFeedTabsAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        if (mPager != null) {
        	mPager.setAdapter(mAdapter);
        	
        }
        
        mIndicator = (TabPageIndicator)findViewById(R.id.indicator);
        if (mIndicator != null) {
        	mIndicator.setViewPager(mPager);
        	mIndicator.setOnPageChangeListener(new OnPageChangeListener() {
    			
        		@Override
        		public void onPageSelected(int pageIndex) {
        			Utils.setSharedPreferencesInt(PixDefs.MYFEED_SELECTED_TAB, pageIndex);
        			if (mMyFeedDelegates != null) {
        				for (MyFeedControlDelegate delegate : mMyFeedDelegates) {
        					if (delegate != null) {
        						if (pageIndex == COMPLETED_TAB_INDEX &&
        								delegate instanceof MyCompletedFragment) {
        							delegate.onFeedTabSelected();
        							return;
        						}
        						if (pageIndex == PLAYGOUND_TAB_INDEX &&
        								delegate instanceof MyPlaygroundFragment) {
        							delegate.onFeedTabSelected();
        							return;
        						}
        					}
        				}
        			}
        		}
    			@Override
    			public void onPageScrolled(int arg0, float arg1, int arg2) {}
    			@Override
    			public void onPageScrollStateChanged(int arg0) {}
    		});
        }
        
        if (mPager != null) {
    		mPager.setCurrentItem(Utils.getSharedPreferencesInt(PixDefs.MYFEED_SELECTED_TAB, 
    									COMPLETED_TAB_INDEX));
    	}
        
        Appirater.appLaunched(this);
    }
	

    @Override
	public void onStart() {
    	super.onStart();
    	this.mAppreciationManager.onStart();
    }
    
    @Override
	public void onStop() {
    	super.onStop();
    	this.mAppreciationManager.onStop();
    }
    
    @Override
	public void onDestroy() {
    	super.onDestroy();
    	this.mAppreciationManager.onDestroy();
    }
    
    @Override
	public void onResume() {
		super.onResume();
		if (PixApp.getInstance().isCheckAppVersion()) {
			this.checkAppVersionAtServer();
			PixApp.getInstance().setCheckAppVersion(false); // should happen once only
		}
//		PixSocialNetworkManager.displayFollowUsIfNeeded(this);
		this.mAppreciationManager.onEvent("App Open");
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT:
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			String parentId = extras.getString(PixDefs.INTENT_EXTRA_NEW_SPLIT_PARENT_ID);
        			if (Utils.isValidString(parentId) &&
        					mCompFeedDelegates != null) {
        				for (PixCompFeedDelegate delegate : mCompFeedDelegates) {
        					if (delegate != null) {
        						// remove the parent composition from the feed
        						delegate.removeCompositionFromFeed(parentId);
        					}
        				}
        			}
        		}
        	}
        	break;
        default:
			break;
		}
        super.onActivityResult(requestCode, resultCode, data);
	}
		
	// notifications
	@Override
	public void onNewCompleted(int newCompletedNum) {
		super.onNewCompleted(newCompletedNum);
		// pass the event to my-feeds in order to display 'new items' bar
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewCompleted(newCompletedNum);
				}
			}
		}
	}

	@Override
	public void onNewActions(int newActionsNum) {
		super.onNewActions(newActionsNum);
		// pass the event to my-feeds in order to display 'new items' bar
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewActions(newActionsNum);
				}
			}
		}
	}

	@Override
	public void registerMyFeedDelegate(MyFeedControlDelegate delegate) {
		if (mMyFeedDelegates != null) {
			mMyFeedDelegates.add(delegate);
		}
	}

	@Override
	public void unregisterMyFeedDelegate(MyFeedControlDelegate delegate) {
		if (mMyFeedDelegates != null) {
			mMyFeedDelegates.remove(delegate);
		}
	}
	
	class MyFeedTabsAdapter extends FragmentPagerAdapter {
        public MyFeedTabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	if (position == PLAYGOUND_TAB_INDEX) {
        		return PixApp.getMyPlaygroundFragment();
        	} else if (position == COMPLETED_TAB_INDEX) {
        		return PixApp.getMyCompletedFragment();
        	}
            return null;
        }
        
        @Override
        public CharSequence getPageTitle(int position) {
        	if (position == PLAYGOUND_TAB_INDEX) {
        		return PixApp.getContext().getText(R.string.studio);
        	} else if (position == COMPLETED_TAB_INDEX) {
        		return PixApp.getContext().getText(R.string.gallery);
        	}
            return "";
        }

        @Override
        public int getCount() {
            return MY_FEED_TABS_NUM;
        }
    }
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
