package com.pixplit.android.ui.activities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplit.android.NotificationsManager;
import com.pixplit.android.NotificationsManager.onNewNotificationsListener;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixCheckVersion;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.CheckVersionHttpRequest;
import com.pixplit.android.http.request.LogoutHttpRequest;
import com.pixplit.android.ui.ActivitiesControllerInterface;
import com.pixplit.android.ui.BaseActivityDelegate;
import com.pixplit.android.ui.PixActivityInterface;
import com.pixplit.android.ui.ToolBarDelegateInterface;
import com.pixplit.android.ui.elements.PixAlertDialog;
import com.pixplit.android.ui.elements.PixAlertDialog.ConfirmationDialogDelegate;
import com.pixplit.android.ui.elements.PixBoldTextView;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.PixTextView;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.billing.PixBilling;
import com.urbanairship.push.PushManager;

@SuppressLint("Registered")
public class PixBaseActivity extends FragmentActivity implements PixActivityInterface,
																	ToolBarDelegateInterface,
																	onNewNotificationsListener, 
																	DrawerListener {
//	private static final String TAG = "PixBaseActivity";

	protected FragmentManager mFragmentManager;
	private NotificationsManager mNotificationsManager = null;
	private boolean fetchNotifications = true;
	ArrayList<ToolBarDelegate> mActionBarDelegates;
	ActivitiesControllerInterface mActivitiesController;
	protected ArrayList<BaseActivityDelegate> mBaseActivityDelegates;
		
	// toolbar properties
	private boolean showToolbar = true;
	private int toolbarTitleRes = 0;
	private String toolbarTitleText = null;
	private int toolbarTitleTextId = 1234; 
	private int toolbarLogoRes = 0;
	private RelativeLayout mToolbar = null;
	private static final int titleTextColor = PixApp.getContext().getResources().getColor(R.color.black);
	final static float titleTextSize = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.pix_text_size_medium);
	
	// drawer
	DrawerLayout mDrawer;
	ArrayList<DrawerListener> mDrawerListeners;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        try {
        	mActivitiesController = (ActivitiesControllerInterface)getApplication();
	    } catch (ClassCastException e) {
	    	throw new ClassCastException("Application must implement ActivitiesControllerInterface");
	    }
        this.mBaseActivityDelegates = new ArrayList<BaseActivityDelegate>();
        this.mActionBarDelegates = new ArrayList<ToolBarDelegate>();
        this.mDrawerListeners = new ArrayList<DrawerListener>();
        this.mFragmentManager = getSupportFragmentManager();
        
		mNotificationsManager = new NotificationsManager(this, this);
	}
	
	@Override
    public void onResume() {
        super.onResume();
        this.updateToolbar();
        this.updateToolbarLogo();
        this.initDrawer();
		if (mNotificationsManager != null && this.fetchNotifications) {
        	mNotificationsManager.onResume();
        }
		PixAppState.resumeEventLogging();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.toString(), true);
		}
	}
	
	@Override
	protected void onSaveInstanceState (Bundle savedInstanceState) {
		try {
			super.onSaveInstanceState(savedInstanceState);
		} catch (Exception e) {
			// may fail in some cases. i.e.: "Failure saving state: active My Completed has cleared index: -1"
			// failure on this stage is ok as the activity will fall back to default state
		}
	}
	
	@Override
	protected void onRestoreInstanceState (Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		try {
        	mActivitiesController = (ActivitiesControllerInterface)getApplication();
	    } catch (ClassCastException e) {
	    	throw new ClassCastException("Application must implement ActivitiesControllerInterface");
	    }
	}
	
	@Override
    public void onPause() {
        super.onPause();
        if (mNotificationsManager != null && this.fetchNotifications) {
        	mNotificationsManager.onPause();
        }
        PixAppState.pauseEventLogging();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.toString(), false);
		}
    }
	
	@Override
	public void onStop() {
		super.onStop();
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.toString(), false);
		}
	}
	
	@Override
    public void onDestroy() {
        super.onDestroy();
        if (mNotificationsManager != null) {
        	mNotificationsManager.onDestroy();
        }
		if (mActivitiesController != null) {
			mActivitiesController.markActivityIsRunning(this.toString(), false);
		}
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if (PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			// clean the new split flag so that the single view will be opened only once
        			Intent resultIntent = new Intent();
        			setResult(Activity.RESULT_OK, resultIntent);
        			// split creation finished successfully, open the split in single view
        			Bundle args = new Bundle();
        			String compId = extras.getString(PixDefs.INTENT_EXTRA_NEW_SPLIT_COMP_ID);
    				args.putString(PixDefs.FRAG_ARG_COMP_ID, compId);
    				if (mActivitiesController != null) {
    					mActivitiesController.gotoSingleCompView(this, args, 0);
    				}
        		}
        	}
        }
	}
    
    public void onError(PixError error) {
    	//Utils.pixLog(TAG, "Got error!");
    	if (error != null) {
    		//Utils.pixLog(TAG, error.toString()+"(code:"+error.getCode()+")");	
    		switch (error.getCode()) {
    		case PixError.PIX_SERVER_ERROR_NOT_AUTHENTICATED:
			case PixError.PIX_SERVER_ERROR_INVALID_CLIENT:
				this.logoutRequest(true);
				break;
			case PixError.PixErrorCodeNetworkFailure:
				Utils.showMessage(getApplicationContext().getResources().getString(R.string.error) + ". " +
								  getApplicationContext().getResources().getString(R.string.cannot_show_data), true);
				break;
			default:
				break;
			}
        }
    }
    
    @Override
	public void logoutRequest(boolean withGetstarted){
    	// the logout process should happen only once
    	if (UserInfo.isUserLogedin()) {
			//Utils.pixLog(TAG, "Logout request");
    		new LogoutHttpRequest().send();
			PixAppState.onLogout();
			PixBilling.onLogout();
			// pix server
			// push notifications
			PushManager.shared().setAlias("");
	        PushManager.disablePush();
    	}
        if (mActivitiesController != null) {
        	Bundle args = new Bundle();
        	if (withGetstarted) {
        		this.showGetStarted();
        	} else {
        		mActivitiesController.gotoPopularRequest(this, args);
        	}
        }
	}
    
    // override to provide get start through FB/Google
    public void showGetStarted() {
    	// base behavior is to go to popular feed and display reconnect...
        if (mActivitiesController != null) {
        	Bundle args = new Bundle();
        	args.putBoolean(PixDefs.FRAG_ARG_DISPLAY_RECONNECT_MSG, true);
        	mActivitiesController.gotoPopularRequest(this, args);
        }
    }

    @Override
    public void pullNewRecords() {
		if (mNotificationsManager != null && this.fetchNotifications) {
			mNotificationsManager.reStart();
		}
	}
    
	@Override
	public void onNewUnreadConversations(int newUnreadConversationsNum) {
		updateToolbarLogo();
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewUnreadConversations(newUnreadConversationsNum);
				}
			}
		}
	}

	@Override
	public void onNewNewsRecords(int newRecordsNum) {
		updateToolbarLogo();
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewNewsRecords(newRecordsNum);
				}
			}
		}
	}

	@Override
	public void onNewActions(int newActionsNum) {
		// override by sub-classes
	}

	@Override
	public void onNewCompleted(int newCompletedNum) {
		// override by sub-classes
	}

	@Override
	public void registerToolBarDelegate(ToolBarDelegate delegate) {
		if (mActionBarDelegates != null) {
			mActionBarDelegates.add(delegate);
		}
	}

	@Override
	public void unregisterToolBarDelegate(ToolBarDelegate delegate) {
		if (mActionBarDelegates != null) {
			mActionBarDelegates.remove(delegate);
		}
	}

	@Override
	public ActivitiesControllerInterface getActivitiesController() {
		return mActivitiesController;
	}

	@Override
	public void finishFragment() {
		// in small screen devices where each activity runs one fragment at a time, finish the activity.
		finish();
	}

	@Override
	public void registerBaseActivityDelegate(BaseActivityDelegate delegate) {
		if (mBaseActivityDelegates != null) {
			mBaseActivityDelegates.add(delegate);
		}
	}

	@Override
	public void unregisterBaseActivityDelegate(BaseActivityDelegate delegate) {
		if (mBaseActivityDelegates != null) {
			mBaseActivityDelegates.remove(delegate);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null && delegate.onBackPressed()) {
					// the delegate intercepted the back press event
					return;
				}
			}
		}
		super.onBackPressed();
	}
	
	protected final void checkAppVersionAtServer() {
		if (Utils.isValidString(PixAppState.getAppVersionNumber())) {
			PixAppState.getState().getRequestQueue().add(
			    	new CheckVersionHttpRequest(
			    			new BaseVolleyHttpRequest.RequestDelegate() {
								
								public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
									onError(error);	
								}
								
								public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
									if (response != null && response instanceof PixCheckVersion) {
										onVersionUpdateRequired((PixCheckVersion)response);
									}
								}
							}));
		}
    }
    
    private void onVersionUpdateRequired(PixCheckVersion versionCheck) {
    	int mandatory = versionCheck.getMandatory();
		if (mandatory != PixDefs.parsedIntFallback) {
			// there is a new version - popup a notification
			if (mandatory == 1) {
				new PixAlertDialog(
						this, 
						new ConfirmationDialogDelegate() {
							
							public void onConfirm() {
								Utils.gotoPlayStore(PixBaseActivity.this);
								onMandatoryVersionConfirmed();
							}
						},
						versionCheck.getTitle(), 
						versionCheck.getPopupMessage(), 
						versionCheck.getUpgradeButton()).show();
			} else {
				// not mandatory
				new PixConfirmationDialog(
						this,
						new PixConfirmationDialog.ConfirmationDialogDelegate() {
							
							public void onConfirm() {
								PixAppState.logAppEvent(PixDefs.APP_SCREEN_NEW_VERSION_ALERT, PixDefs.APP_EVENT_USER_AGREED_TO_UPDATE, null);
								Utils.gotoPlayStore(PixBaseActivity.this);
							}
							
							public void onDecline() {
								PixAppState.logAppEvent(PixDefs.APP_SCREEN_NEW_VERSION_ALERT, PixDefs.APP_EVENT_USER_DECLINED_TO_UPDATE, null);
							}
						}, 
						versionCheck.getTitle(),
						versionCheck.getPopupMessage(), 
						versionCheck.getUpgradeButton(),
						versionCheck.getLaterButton()).show();
			}
			
		}
    }
    
    private void onMandatoryVersionConfirmed() {
    	this.finish();
    }
    
    @Override
    public void finish() {
    	super.finish();
    	if (usePixFinishAnimation()) {
    		this.overridePendingTransition(R.anim.pop_from_stack, R.anim.slide_left_to_right);
    	}
    }
    
    /*
     *  override by sub-class that doesn't wish to use default (pixplit) animation,
     *  but rather prefer the system (android) default.
     */
    protected boolean usePixFinishAnimation() {
    	return true;
    }
    
    
    // Toolbar methods
    public void setToolbarVisible(boolean visible){
    	this.showToolbar = visible;
    }
    
    @Override
    public void setToolBarCustomView(int resId) {
    	this.toolbarTitleRes = resId;
    }
    
    @Override
    public void setToolBarTitle(String title) {
    	this.toolbarTitleText = title;  	
	}
    
    @Override
    public void updateToolbarTitle(int resId) {
    	setToolBarCustomView(resId);
    	
    	if (mToolbar != null) {
    		if (toolbarTitleRes != 0) {
    			ImageView toolbarTitle = (ImageView)mToolbar.findViewById(R.id.toolbarTitle);
        		if (toolbarTitle != null) {
        			toolbarTitle.setImageResource(toolbarTitleRes);
        			toolbarTitle.bringToFront();
        		}
    		} else if (toolbarTitleText != null) {
    			ImageView toolbarTitle = (ImageView)mToolbar.findViewById(R.id.toolbarTitle);
        		if (toolbarTitle != null) {
        			TextView titleText = (TextView)mToolbar.findViewById(toolbarTitleTextId);
        			if (titleText == null) {
	        			titleText = new PixBoldTextView(this);
	        			titleText.setTextColor(titleTextColor);
	        			titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize);
	        			titleText.setId(toolbarTitleTextId);
	        			titleText.setBackgroundResource(getResources().getColor(R.color.transparent));
	        			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
	        					RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
	        			params.addRule(RelativeLayout.ALIGN_LEFT, R.id.toolbarTitle);
	        			params.addRule(RelativeLayout.CENTER_VERTICAL);
	        			mToolbar.addView(titleText, params);
        			}
        			titleText.setText(toolbarTitleText);
        		}
    		}
    	}	
    }
    
    @Override
    public PixImageView addToolbarImageBtn(int toolbarBtnId, OnClickListener onClickListener) {
    	PixImageView toolbarBtn = null;
    	if (mToolbar != null) {
    		View btn = mToolbar.findViewById(toolbarBtnId);
	    	if (btn != null) {
	    		if (btn instanceof PixImageView) {
	    			return (PixImageView)btn;
	    		} else {
		    		// current implementation supports one toolbar button and it already exists
		    		return null;
	    		}
	    	}
	    	toolbarBtn = new PixImageView(this);
	    	if (onClickListener != null) {
	    		toolbarBtn.setOnClickListener(onClickListener);
	    	}
	    	toolbarBtn.setId(toolbarBtnId);
	    	int imageDimen = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.toolbar_btn_image_max_width);
	    	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(imageDimen, imageDimen);
	    	params.addRule(RelativeLayout.ABOVE, R.id.toolbarBottom);
	    	params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	    	params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
	    	mToolbar.addView(toolbarBtn, params);
    	}
		return toolbarBtn;
    }
    
    @Override
    public TextView addToolbarTextBtn(int bgResId, int toolbarBtnId, OnClickListener onClickListener, String btnText) {
    	TextView toolbarBtn = null;
    	if (mToolbar != null) {
    		View btn = mToolbar.findViewById(toolbarBtnId);
	    	if (btn != null) {
	    		if (btn instanceof TextView) {
		    		return ((TextView)btn);
	    		} else {
		    		// current implementation supports one toolbar button and it already exists
		    		return null;
	    		}
	    	}
	    	int toolbarBtnDimen = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.toolbar_btn_dimen);
	    	toolbarBtn = new PixTextView(this);
	    	toolbarBtn.setBackgroundResource(bgResId);
	    	if (onClickListener != null) {
	    		toolbarBtn.setOnClickListener(onClickListener);
	    	}
	    	toolbarBtn.setId(toolbarBtnId);
	    	toolbarBtn.setMinimumWidth(toolbarBtnDimen);
	    	if (btnText != null) {
	    		toolbarBtn.setText(btnText);
	    		toolbarBtn.setGravity(Gravity.CENTER);
	    		toolbarBtn.setTextAppearance(this, R.style.ToolbarBtnText);
	    	}
	    	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
	    	params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	    	mToolbar.addView(toolbarBtn, params);
    	}
		return toolbarBtn;
    }
    
    @Override
    public void addToolbarCustomView(View view, RelativeLayout.LayoutParams params) {
    	if (view == null || params == null) return;
    	View activityLayout = findViewById(R.id.activityLayout);
    	if (activityLayout == null ||
    			!(activityLayout instanceof RelativeLayout)) return;
    	
    	((RelativeLayout)activityLayout).addView(view, params);
    }
    
    public void updateToolbarLogo() {
		if (PixAppState.getNewConversationsNumber() > 0){
			this.toolbarLogoRes = R.drawable.menu_btn_inbox_selector;
		} else {
			if (PixAppState.getNewNewsNumber() > 0){
				this.toolbarLogoRes = R.drawable.menu_btn_news_selector;
			} else {
				this.toolbarLogoRes = R.drawable.menu_btn_selector;
			}		
		}
    	
    	if (mToolbar != null && this.toolbarLogoRes != 0) {
    		View toolbarImage = mToolbar.findViewById(R.id.toolbarImage);
    		if (toolbarImage != null) {
    			toolbarImage.setBackgroundResource(this.toolbarLogoRes);
    		}
    	}
    }
    
    protected void updateToolbar(){
    	mToolbar = (RelativeLayout)findViewById(R.id.toolbar);
		if (mToolbar != null){
			if (showToolbar) {
    			View toolbarImage = mToolbar.findViewById(R.id.toolbarImage);
	    		toolbarImage.setBackgroundResource(toolbarLogoRes);
	    		// Set onClick methods for the toolbar
	    		toolbarImage.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onToolbarHomePress();
					}
				});
	    		
	    		updateToolbarTitle(toolbarTitleRes);
	    		View toolbarCenter = mToolbar.findViewById(R.id.toolbarCenter);
	    		toolbarCenter.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mActionBarDelegates != null) {
							for (ToolBarDelegate delegate : mActionBarDelegates) {
								if (delegate != null) {
									delegate.onToolbarCenterClick();
								}
							}
						}
					}
				});
	    		
	    		View toolbarTitle = mToolbar.findViewById(R.id.toolbarTitle);
	    		toolbarTitle.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mActionBarDelegates != null) {
							for (ToolBarDelegate delegate : mActionBarDelegates) {
								if (delegate != null) {
									delegate.onToolbarCenterClick();
								}
							}
						}
					}
				});
	    	} else {
	    		// hide the toolbar
	    		mToolbar.setVisibility(View.GONE);
	    	}
    	}
	}

    private void onToolbarHomePress() {
    	if (mActionBarDelegates != null) {
			for (ToolBarDelegate delegate : mActionBarDelegates) {
				if (delegate != null) {
					// give a chance for the delegate to handle this event
					if (delegate.onToolbarHomePressIntercepted()) {
						// intercepted
						return;
					}
				}
			}
		}
		// event not handled by the delegates, do the default
		if (mDrawer != null) {
			mDrawer.openDrawer(Gravity.LEFT);
		}
    }
    
    // sets whether or not the notification manager should pull notifications from the servers
	public void setFetchNotifications(boolean fetchNotifications) {
		this.fetchNotifications = fetchNotifications;
	}

	// Drawer menu
	private void initDrawer() {
		View drawerView = findViewById(R.id.drawer_layout);
		if (drawerView != null && drawerView instanceof DrawerLayout) {
			mDrawer = (DrawerLayout)drawerView;
			mDrawer.setDrawerListener(this);
		}
	}
	
	@Override
	public void setDrawerListener(DrawerListener delegate) {
		mDrawerListeners.add(delegate);
	}
	
	@Override
	public void unsetDrawerListener(DrawerListener delegate) {
		mDrawerListeners.remove(delegate);
	}
	
	@Override
	public void closeDrawer(DrawerListener delegate) {
		if (mDrawer != null) {
			if (delegate != null) {
				setDrawerListener(delegate);
			}
			mDrawer.closeDrawers();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDrawerClosed(View arg0) {
		// clone the listeners array as due to the following calls some listeners may remove themselves from the list.
		ArrayList<DrawerListener> listeners = (ArrayList<DrawerListener>) mDrawerListeners.clone();
		for (DrawerListener listener : listeners) {
			if (listener != null) {
				listener.onDrawerClosed(arg0);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDrawerOpened(View arg0) {
		// clone the listeners array as due to the following calls some listeners may remove themselves from the list.
		ArrayList<DrawerListener> listeners = (ArrayList<DrawerListener>) mDrawerListeners.clone();
		for (DrawerListener listener : listeners) {
			if (listener != null) {
				listener.onDrawerOpened(arg0);
			}
		}
	}

	@Override
	public void onDrawerSlide(View arg0, float arg1) {}

	@Override
	public void onDrawerStateChanged(int arg0) {
		PixApp.hideKeypad(getWindow().getDecorView().findViewById(android.R.id.content));
	}

	@Override
	public void onAdminNotAuthenticatedError() {
		// in case admin is not authenticated (as indicated by the server), close this activity
		if (mActivitiesController != null) {
			UserInfo.setAdminToken(null);
			Utils.showMessage(this
					.getString(
							R.string.admin_is_not_authenticate), false);
			mActivitiesController.gotoMyFeed(this, 
					ActivitiesControllerInterface.PIX_ANIMATE_SYSTEM_DEFAULT_ANIMATION);
		}
	}
}
