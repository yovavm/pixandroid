package com.pixplit.android.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.AgeRange;
import com.pixplit.android.R;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.google.GPlusConnectDelegate;
import com.pixplit.android.google.GPlusProcessType;
import com.pixplit.android.google.GooglePlusActivityInterface;
import com.pixplit.android.http.request.PixGPlusAuthHttpRequest;
import com.pixplit.android.ui.AuthRequestDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;

public class GPlusBaseActivity extends PixBaseActivity implements PlusClient.ConnectionCallbacks, 
																  PlusClient.OnConnectionFailedListener,
																  //PlusClient.OnAccessRevokedListener, 
																  GooglePlusActivityInterface {
	private PlusClient mPlusClient;
	private GPlusProcessType processType;	
	private GPlusConnectDelegate mConnectDelegate;
	
    @Override
    public void onStop() {
    	super.onStop();
    	if (mPlusClient != null &&
    			mPlusClient.isConnected()) {
    		mPlusClient.disconnect();
    	}
    }
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
        case PixDefs.ACTIVITY_RESULT_CODE_GOOGLE_SIGNIN:
        	if (resultCode == RESULT_OK && 
        			mPlusClient != null &&
        			!mPlusClient.isConnected() && 
        			!mPlusClient.isConnecting()) {
        		// This time, connect should succeed.
        		mPlusClient.connect();
        	} else {
        		// hide the authenticating message
        		PixProgressIndicator.hideMessage();
        	}
			break;
			
        default:
			break;
		}
    }
    
	private void gPlusAuthWithPixServer(PixUserInfo userInfo) {
		PixProgressIndicator.showMessage(this, getApplicationContext().getResources().getString(R.string.authenticating), true);
		if (userInfo != null) {
			userInfo.setToken(PixAppState.UserInfo.craetePixToken(userInfo.getUserName()));
			new PixGPlusAuthHttpRequest(gPlusAuthRequestDelegate, userInfo).send();
		} else {
			authenticateWithGPlusFailed();
		}
	}

	private AuthRequestDelegate gPlusAuthRequestDelegate = new AuthRequestDelegate(this, AuthRequestDelegate.AUTH_PROCESS_TYPE_GPLUS) {
		@Override
		public void onAuthFinished() {
			authenticateWithGPlusFinished();
		}
		@Override
		public void onAuthFailed() {
			authenticateWithGPlusFailed();
		}
	};
	
	protected void authenticateWithGPlusFinished() {
		PixProgressIndicator.hideMessageWithIndication(true, 
				getApplicationContext().getResources().getString(R.string.authenticated));

		PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, 
				PixDefs.APP_EVENT_SUCCESSFUL_GPLUS_CONNECT, 
				null);
		
		Bundle args = new Bundle();
		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, true);
		finish();
		if (mActivitiesController != null) {
			mActivitiesController.gotoFollowFriends(this, args);
		}
	}
	
	private void authenticateWithGPlusFailed() {
		UserInfo.clearGPlusCredentials();
		PixProgressIndicator.hideMessageWithIndication(false, 
				getApplicationContext().getResources().getString(R.string.failed));
	}

	@Override
	public void logoutRequest(boolean withReconnectMsg){
    	this.logoutGPlus();
		super.logoutRequest(withReconnectMsg);
    }

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// clear the process type as each process handling should happen only once
		GPlusProcessType currentProcessType = this.processType;
		this.processType = GPlusProcessType.GPLUS_PROCESS_IDLE;
		
		switch (currentProcessType) {
		case GPLUS_PROCESS_AUTHENTICATION:
		case GPLUS_PROCESS_LINK_ACCOUNT:
			try {
				this.processType = currentProcessType; // try to resolve - second attempt
	            result.startResolutionForResult(this, PixDefs.ACTIVITY_RESULT_CODE_GOOGLE_SIGNIN);
	            return;
	        } catch (IntentSender.SendIntentException e) {
	            // Failed to connect
	        	this.authenticateWithGPlusFailed();
	        }
			break;
		case GPLUS_PROCESS_LOGOUT:
			// if on logout process, the failure indicates that the user has not logged in with GPlus. do nothing.
			break;
		default:
			this.authenticateWithGPlusFailed();
			break;
		}
		
		if (this.mConnectDelegate != null) {
			this.mConnectDelegate.onGPlusConnectFailed(currentProcessType);
			this.mConnectDelegate = null; // the delegate received its response
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// clear the process type as each process handling should happen only once
		GPlusProcessType currentProcessType = this.processType;
		this.processType = GPlusProcessType.GPLUS_PROCESS_IDLE;
		
		// verify that the Plus client is connected (bug fix for scenarios on field..).
		if (mPlusClient == null || !mPlusClient.isConnected()) {
			if (this.mConnectDelegate != null) {
				this.mConnectDelegate.onGPlusConnectFailed(currentProcessType);
				this.mConnectDelegate = null; // the delegate received its response
			}
			return;
		}
		
		Person signedInPerson = mPlusClient.getCurrentPerson();
		if (signedInPerson == null) {
			// error
			authenticateWithGPlusFailed();
			return;
		}
		
		PixUserInfo pixUserInfo = createPixUser(signedInPerson);
		UserInfo.storeGPlusCredentials(pixUserInfo);
		
		switch (currentProcessType) {
		case GPLUS_PROCESS_AUTHENTICATION:
			this.gPlusAuthWithPixServer(pixUserInfo);
			break;
		case GPLUS_PROCESS_LOGOUT:
			logoutGPlus();
			break;
		case GPLUS_PROCESS_LINK_ACCOUNT:
			break;
		default:
			break;
		}
		
		if (this.mConnectDelegate != null) {
			this.mConnectDelegate.onGPlusConnectFinished(currentProcessType);
			this.mConnectDelegate = null; // the delegate received its response
		}
    }

	private PixUserInfo createPixUser(Person signedInPerson) {
		PixUserInfo pixUserInfo = new PixUserInfo();
		pixUserInfo.setFullName(signedInPerson.getDisplayName());
		pixUserInfo.setId(signedInPerson.getId());
		pixUserInfo.setGender((signedInPerson.getGender() == Person.Gender.MALE) ? "male" : "female");
		pixUserInfo.setLocale(signedInPerson.getCurrentLocation());
		pixUserInfo.setUserName(signedInPerson.getId());
		Person.Image image = signedInPerson.getImage();
		if (image != null) {
			pixUserInfo.setImageUrl(image.getUrl());
		}
		AgeRange ageRange = signedInPerson.getAgeRange();
		if (ageRange != null) {
			pixUserInfo.setAgeRange(String.format("%d-%d", ageRange.getMin(), ageRange.getMax()));
		}
		return pixUserInfo;
	}
	
	@Override
	public void onDisconnected() {}

	private boolean hasGooglePlayServices() {
		int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (available != ConnectionResult.SUCCESS) {
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(available, this, 0);
			if (errorDialog != null) {
				errorDialog.show();
			}
            return false;
        }
		return true;
	}
	
	/////////  GooglePlusActivityInterface  //////////////////
	@Override
	public void signInWithGPlus() {
		if (!hasGooglePlayServices()) return; // no delegate to inform
				
		PixProgressIndicator.showMessage(this, getApplicationContext().getResources().getString(R.string.authenticating), true);
		this.processType = GPlusProcessType.GPLUS_PROCESS_AUTHENTICATION;
		mPlusClient = new PlusClient.Builder(this, this, this).build();
		mPlusClient.connect();
	}

	@Override
	public void linkGPlusAccount(GPlusConnectDelegate delegate) {
		if (!hasGooglePlayServices()) {
			if (delegate != null) {
				delegate.onGPlusConnectFailed(GPlusProcessType.GPLUS_PROCESS_LINK_ACCOUNT);
			}
			return;
		}
		this.mConnectDelegate = delegate;
		this.processType = GPlusProcessType.GPLUS_PROCESS_LINK_ACCOUNT;
		mPlusClient = new PlusClient.Builder(this, this, this).build();
		mPlusClient.connect();
	}
	
	@Override
	public void logoutGPlus() {
		UserInfo.clearGPlusCredentials();
		if (mPlusClient == null) {
			/* we have not signed-in with GPlus in this session. Check if the user has 
			 * signed with GPlus and if so clear his credentials */
			mPlusClient = new PlusClient.Builder(this, this, this).build();
			this.processType = GPlusProcessType.GPLUS_PROCESS_LOGOUT;
			mPlusClient.connect();
		} else {
			if (mPlusClient.isConnected()) {
				mPlusClient.clearDefaultAccount();
				mPlusClient.disconnect();
			}
		}
	}
}