package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.SettingsFragment;

public class SettingsActivity extends PixFbBaseActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolBarTitle(getApplicationContext().getString(R.string.settings_caps));
        
        this.updateToolbar();
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	SettingsFragment fragment = SettingsFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
    }
	
	@Override
    public void finish() {
    	super.finish();
    	this.overridePendingTransition(R.anim.hold_in_place, R.anim.slide_top_bottom);
    }
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the slide down animation to run when app is done
    }
}
