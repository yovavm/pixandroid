package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.BillingHelperController;
import com.pixplit.android.ui.SigninFragment;
import com.pixplit.android.ui.elements.PixAlertDialog;
import com.pixplit.android.util.billing.IabHelper;

public class SignInActivity extends PixBaseActivity  implements BillingHelperController{
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolBarTitle(getApplicationContext().getString(R.string.sign_in));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	SigninFragment fragment = SigninFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        switch (requestCode) {
        case PixDefs.ACTIVITY_RESULT_CODE_RESET_PASSWORD:
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle resultBundle = data.getExtras();
        		if (resultBundle.getBoolean(PixDefs.INTENT_EXTRA_RESET_PASSWORD_SUCCESS, false)) {
    				new PixAlertDialog(this, 
    		    			null, 
    		    			PixApp.getContext().getString(R.string.reset_password),
    		    			PixApp.getContext().getString(
    		    			R.string.email_sent_please_check_your_inbox_for_an_email_from_us_and_follow_the_instructions_inside), 
    		    			PixApp.getContext().getString(R.string.ok)).show();
    	    	}
        	}
        	break;
        default:
			break;
		}
	}
		
	@Override
    public void onDestroy() {
        super.onDestroy();
	}

	// Billing
	@Override
	public IabHelper getIabHelper() {
//		return mBillingHelper;
		return null;
	}

	@Override
    public void finish() {
    	super.finish();
    	this.overridePendingTransition(R.anim.hold_in_place, R.anim.slide_top_bottom);
    }
}
