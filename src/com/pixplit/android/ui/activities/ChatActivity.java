package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixPushNotificationsReceiver;
import com.pixplit.android.ui.ChatControlDelegate;
import com.pixplit.android.ui.ChatControllerInterface;
import com.pixplit.android.ui.ChatFragment;


public class ChatActivity extends PixFbBaseActivity implements ChatControllerInterface{
	
	ChatControlDelegate mChatDelegate;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.updateToolbar();
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	ChatFragment fragment = ChatFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
        
	}
		
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			// split creation finished successfully, refresh the feed to display it.
        			if (mActivitiesController != null) {
        				mActivitiesController.markForRefresh(
        						extras.getString(PixDefs.INTENT_EXTRA_NEW_SPLIT_CONVERSATION_ID),
        						true);
        			}
        		}
        	}
        	/* on chat receiving result from new split creation, override 
        	 * the default behaviour (of openning a single comp view).
        	 */
        	return;
        }
        super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
    public void onNewIntent(Intent intent) {
    	super.onNewIntent(intent);
		if (intent != null) {
			// forward the intent info to the chat delegate
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String conversationId = extras.getString(PixPushNotificationsReceiver.INTENT_PUSH_KEY_CONVERSATION_ID);
				if (mChatDelegate != null) {
					mChatDelegate.onNewMessageReceived(conversationId);
				}
			}
		}
	}
	
	@Override
	public void registerChatDelegate(ChatControlDelegate delegate) {
		mChatDelegate = delegate;
	}

	@Override
	public void unregisterChatDelegate() {
		mChatDelegate = null;
	}
}