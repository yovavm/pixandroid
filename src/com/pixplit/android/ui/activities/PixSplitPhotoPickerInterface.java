package com.pixplit.android.ui.activities;



public interface PixSplitPhotoPickerInterface extends PixPhotoPickerInterface{
	public void photoPickFromInstaRequest();
	public void photoPickFromFacebookRequest();
	public void photoPickFromGoogleRequest();
}
