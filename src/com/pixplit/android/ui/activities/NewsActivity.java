package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.NewsFragment;


public class NewsActivity extends PixFbBaseActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.setToolBarTitle(getApplicationContext().getString(R.string.news));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
        	if (mFragmentManager != null) {
        		if (mFragmentManager.findFragmentByTag(EFragmentType.NEWS.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		NewsFragment fragment = NewsFragment.newInstance();
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.NEWS.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
