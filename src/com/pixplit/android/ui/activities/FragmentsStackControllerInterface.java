package com.pixplit.android.ui.activities;

import com.pixplit.android.ui.PixBaseFragment;

public interface FragmentsStackControllerInterface {
	public void pushStackFragment(PixBaseFragment fragment, int pushInAnim, int popOutAnim);
	public void popOutFragment(boolean withAnimation);
}

// fragments stack control
// in onCreate: mFragmentsStack = new FragmentsStack(mFragmentManager, null, R.id.fragment_container, savedInstanceState);
//@Override
//public void pushStackFragment(PixBaseFragment fragment, int pushInAnim,
//		int popOutAnim) {
//	if (mFragmentsStack != null) {
//		mFragmentsStack.pushStackFragment(fragment, pushInAnim, popOutAnim);
//	}
//}
//
//@Override
//public void popOutFragment(boolean withAnimation) {
//	if (mFragmentsStack != null) {
//		mFragmentsStack.popOutFragment(withAnimation);
//	}
//}
//
//@Override
//public void onBackPressed() {
//	if (mFragmentsStack.getSize() == 1) {
//		super.onBackPressed();
//	} else {
//		mFragmentsStack.popOutFragment(true);
//	}
//}
