package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.ProcessNewSplitFragment;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public class ProcessNewSplitActivity extends PixBaseActivity{
	
	public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
		ProcessNewSplitFragment.setJoinedCompImage(joinedImage);
    }
    public static void setActivePartOrigBitmap(PixBitmapHolder partBitmap) {
    	ProcessNewSplitFragment.setActivePartOrigBitmap(partBitmap);
    }
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolbarVisible(false);
        this.updateToolbar();
        this.setFetchNotifications(false);
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	if (mFragmentManager.findFragmentByTag(EFragmentType.PROCESS_SPLIT.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		ProcessNewSplitFragment fragment = ProcessNewSplitFragment.newInstance(args);
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.PROCESS_SPLIT.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}

	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			// pass on the data that the split creation process was done successful.
        			setResult(Activity.RESULT_OK, data);
        			
        			// release the bitmaps static references
        			releaseBitmaps();
        			
        			// in case the split creation was finished successfully, finish up.
        			finish();
        			return;
        		}
        	}
        }
        super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
    public void finish() {
        super.finish();
        // override transitions to skip the standard window animations
        overridePendingTransition(0, 0);
    }
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// release the bitmaps static references
		releaseBitmaps();
	}
	
	private void releaseBitmaps() {
		ProcessNewSplitFragment.setJoinedCompImage(null);
		ProcessNewSplitFragment.setActivePartOrigBitmap(null);
	}
}
