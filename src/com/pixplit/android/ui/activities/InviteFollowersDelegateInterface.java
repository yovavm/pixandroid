package com.pixplit.android.ui.activities;

public interface InviteFollowersDelegateInterface {
	public void registerInviteFollowersDelegate(InviteFollowersDelegate delegate);
	public void unregisterInviteFollowersDelegate();
}
