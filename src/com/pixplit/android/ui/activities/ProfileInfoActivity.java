package com.pixplit.android.ui.activities;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.ShadowLayout;
import com.pixplit.android.util.Utils;
import com.pixplit.android.volley.PixImagesLoader;

@SuppressLint("NewApi")
public class ProfileInfoActivity extends PixBaseActivity {
	
	static float sAnimatorScale = 1;
	
    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    private static final int ANIM_DURATION = 220;

    ColorDrawable mBackground;
    int mLeftDelta;
    int mTopDelta;
    float mWidthScale;
    float mHeightScale;
    private PixImageView userImageView;
    private FrameLayout mTopLevelLayout;
    private ShadowLayout mShadowLayout;
    private int mOriginalOrientation;
    private int thumbnailTop;
    private int thumbnailLeft;
    private int thumbnailWidth;
    private int thumbnailHeight;
    
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info_activity_layout);
        this.updateToolbar();
        userImageView = (PixImageView) findViewById(R.id.user_pic);
        mTopLevelLayout = (FrameLayout) findViewById(R.id.user_pic_dialog_layout);
        mShadowLayout = (ShadowLayout) findViewById(R.id.shadowLayout);
        
        // Retrieve the data we need for the picture/description to display and
        // the thumbnail to animate it from
        thumbnailTop = 0;
        thumbnailLeft = 0;
        thumbnailWidth = 0;
        thumbnailHeight = 0;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
        	String userPicture = bundle.getString(PixApp.PACKAGE_NAME + ".userPicture");
            if (userImageView != null && Utils.isValidString(userPicture)) {
            	PixImagesLoader.loadImage(userPicture, userImageView, 0, 0, Priority.IMMEDIATE, false);
    		}
            thumbnailTop = bundle.getInt(PixApp.PACKAGE_NAME + ".top");
            thumbnailLeft = bundle.getInt(PixApp.PACKAGE_NAME + ".left");
            thumbnailWidth = bundle.getInt(PixApp.PACKAGE_NAME + ".width");
            thumbnailHeight = bundle.getInt(PixApp.PACKAGE_NAME + ".height");
            mOriginalOrientation = bundle.getInt(PixApp.PACKAGE_NAME + ".orientation");
        }
		
        if (mTopLevelLayout != null) {
        	mTopLevelLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					exitActivity();
				}
			});
        }
        
                
        mBackground = new ColorDrawable(Color.BLACK);
        if (Utils.hasJellyBean()) {
        	mTopLevelLayout.setBackground(mBackground);
        } else {
        	mTopLevelLayout.setBackgroundDrawable(mBackground);
        }

        // Only run the animation if we're coming from the parent activity, not if
        // we're recreated automatically by the window manager (e.g., device rotation)
        if (savedInstanceState == null) {
            ViewTreeObserver observer = userImageView.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                
                @Override
                public boolean onPreDraw() {
                    userImageView.getViewTreeObserver().removeOnPreDrawListener(this);

                    // Figure out where the thumbnail and full size versions are, relative
                    // to the screen and each other
                    int[] screenLocation = new int[2];
                    userImageView.getLocationOnScreen(screenLocation);
                    mLeftDelta = thumbnailLeft - screenLocation[0];
                    mTopDelta = thumbnailTop - screenLocation[1];
                    
                    // Scale factors to make the large version the same size as the thumbnail
                    mWidthScale = (float) thumbnailWidth / userImageView.getWidth();
                    mHeightScale = (float) thumbnailHeight / userImageView.getHeight();
    
                    runEnterAnimation();
                    
                    return true;
                }
            });
        }
    }

    /**
     * The enter animation scales the picture in from its previous thumbnail
     * size/location, colorizing it in parallel. In parallel, the background of the
     * activity is fading in. When the pictue is in place, the text description
     * drops down.
     */
    public void runEnterAnimation() {
        final long duration = (long) (ANIM_DURATION * sAnimatorScale);
        
        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        userImageView.setPivotX(0);
        userImageView.setPivotY(0);
        userImageView.setScaleX(mWidthScale);
        userImageView.setScaleY(mHeightScale);
        userImageView.setTranslationX(mLeftDelta);
        userImageView.setTranslationY(mTopDelta);
        
        // Animate scale and translation to go from thumbnail to full size
        userImageView.animate().setDuration(duration).
                scaleX(1).scaleY(1).
                translationX(0).translationY(0).
                setInterpolator(sDecelerator);
        
        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0, 255);
        bgAnim.setDuration(duration);
        bgAnim.start();
        
        // Animate a drop-shadow of the image
        ObjectAnimator shadowAnim = ObjectAnimator.ofFloat(mShadowLayout, "shadowDepth", 0, 1);
        shadowAnim.setDuration(duration);
        shadowAnim.start();
    }
    
    /**
     * The exit animation is basically a reverse of the enter animation, except that if
     * the orientation has changed we simply scale the picture back into the center of
     * the screen.
     * 
     */
    public void runExitAnimation() {
        final long duration = (long) (ANIM_DURATION * sAnimatorScale);

        // No need to set initial values for the reverse animation; the image is at the
        // starting size/location that we want to start from. Just animate to the
        // thumbnail size/location that we retrieved earlier 
        
        // Caveat: configuration change invalidates thumbnail positions; just animate
        // the scale around the center. Also, fade it out since it won't match up with
        // whatever's actually in the center
        final boolean fadeOut;
        if (getResources().getConfiguration().orientation != mOriginalOrientation) {
            userImageView.setPivotX(userImageView.getWidth() / 2);
            userImageView.setPivotY(userImageView.getHeight() / 2);
            mLeftDelta = 0;
            mTopDelta = 0;
            fadeOut = true;
        } else {
            fadeOut = false;
        }


        // Animate image back to thumbnail size/location
        userImageView.animate().setDuration(duration).
                scaleX(mWidthScale).scaleY(mHeightScale).
                translationX(mLeftDelta).translationY(mTopDelta);
        if (fadeOut) {
            userImageView.animate().alpha(0);
        }
        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0);
        bgAnim.setDuration(duration);
        bgAnim.start();

        // Animate the shadow of the image
        ObjectAnimator shadowAnim = ObjectAnimator.ofFloat(mShadowLayout,
                "shadowDepth", 1, 0);
        shadowAnim.setDuration(duration);
        shadowAnim.start();
    }    

    /**
     * Overriding this method allows us to run our exit animation first, then exiting
     * the activity when it is complete.
     */
    @Override
    public void onBackPressed() {
        exitActivity();
    }

    private void exitActivity() {
    	runExitAnimation();
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
                // *Now* go ahead and exit the activity
                finish();
			}
		}, ANIM_DURATION-30);
	}

	@Override
    public void finish() {
        super.finish();
        
        // override transitions to skip the standard window animations
        overridePendingTransition(0, 0);
    }
}
