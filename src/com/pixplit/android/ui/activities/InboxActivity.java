package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.InboxFragment;


public class InboxActivity extends PixFbBaseActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.setToolBarTitle(getApplicationContext().getString(R.string.inbox));
        this.updateToolbar();

        if (savedInstanceState == null) {
        	if (mFragmentManager != null) {
        		if (mFragmentManager.findFragmentByTag(EFragmentType.INBOX.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		InboxFragment fragment = InboxFragment.newInstance();
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.INBOX.toString());
		        	ft.commit();
	        	}
	        }
        }
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
