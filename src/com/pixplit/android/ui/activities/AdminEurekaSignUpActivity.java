package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.AdminEurekaSignupFragment;
import com.pixplit.android.util.PixPhotoPickManager;
import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;

public class AdminEurekaSignUpActivity extends PixBaseActivity implements PixPhotoPickerInterface {
	
	PixPhotoPickManager photoPickManager;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolBarTitle(getApplicationContext().getString(R.string.create_a_eureka_account));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	AdminEurekaSignupFragment fragment = AdminEurekaSignupFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPhotoPickManager().onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
    protected void onResumeFragments() {
		super.onResumeFragments();
		getPhotoPickManager().deliverPhotoPickResult();
	}

	@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
    	getPhotoPickManager().onSaveInstanceState(savedInstanceState);
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	getPhotoPickManager().onRestoreInstanceState(savedInstanceState);
    }
    
	@Override
	public void registerPhotoPickerDelegate(PhotoFetcherDelegate delegate) {
		getPhotoPickManager().registerPhotoPickerDelegate(delegate);
	}

	@Override
	public void unregisterPhotoPickerDelegate() {
		getPhotoPickManager().unregisterPhotoPickerDelegate();
	}

	@Override
	public void photoPickRequest() {
		getPhotoPickManager().photoPickRequest();
	}

	@Override
	public void photoCaptureRequest() {
		getPhotoPickManager().photoCaptureRequest();
	}
	
	private PixPhotoPickManager getPhotoPickManager() {
		if (photoPickManager == null) {
			photoPickManager = new PixPhotoPickManager(this);
		}
		return photoPickManager;
	}
	
	@Override
    public void finish() {
    	super.finish();
    }
}
