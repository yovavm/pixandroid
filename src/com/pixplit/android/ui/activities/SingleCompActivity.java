package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.SingleViewFragment;


public class SingleCompActivity extends PixCompFeedActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.setToolBarTitle(getApplicationContext().getString(R.string.pixplit_caps));
        this.updateToolbar();
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	SingleViewFragment fragment = SingleViewFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
        
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			// pass on the data that the split creation process was done successful.
        			setResult(Activity.RESULT_OK, data);
        			// this single view activity just got back after creating a new split.
            		// as this is possible only when displaying an open composition to join,
            		// close this open split as it is no longer relevant
        			finish();
        			// call supper in order to apply default - open single view..
        			super.onActivityResult(requestCode, resultCode, data);
        			return;
        		}
        	}
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
