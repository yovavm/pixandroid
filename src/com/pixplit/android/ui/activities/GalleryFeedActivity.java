package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.GalleryFeedFragment;


public class GalleryFeedActivity extends PixCompFeedActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        
	        if (mFragmentManager != null) {
	        	if (mFragmentManager.findFragmentByTag(EFragmentType.GALLERY_VIEW.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		GalleryFeedFragment fragment = GalleryFeedFragment.newInstance(args);
	        		ft.add(R.id.fragment_container, fragment, EFragmentType.GALLERY_VIEW.toString());
	        		ft.commit();
	        	}
	        }
        }
    }
}