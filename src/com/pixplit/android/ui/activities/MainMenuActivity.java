package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.ui.BaseActivityDelegate;
import com.pixplit.android.ui.MainMenuFragment;

public class MainMenuActivity extends PixBaseActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        setToolbarVisible(false);
        this.updateToolbar();
        
        if (mFragmentManager != null) {
        	if (mFragmentManager.findFragmentByTag(EFragmentType.MAIN_MENU.toString()) == null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	MainMenuFragment fragment = MainMenuFragment.newInstance();
	        	ft.add(R.id.fragment_container, fragment, EFragmentType.MAIN_MENU.toString());
	        	ft.commit();
        	}
        }
	}
	
	// notifications
	@Override
	public void onNewUnreadConversations(int newUnreadConversationsNum) {
		super.onNewUnreadConversations(newUnreadConversationsNum);
		// pass the event to the main-menu fragment to update badges
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewUnreadConversations(newUnreadConversationsNum);
				}
			}
		}
	}

	@Override
	public void onNewNewsRecords(int newRecordsNum) {
		super.onNewNewsRecords(newRecordsNum);
		// pass the event to the main-menu fragment to update badges
		if (mBaseActivityDelegates != null) {
			for (BaseActivityDelegate delegate : mBaseActivityDelegates) {
				if (delegate != null) {
					delegate.onNewNewsRecords(newRecordsNum);
				}
			}
		}
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
    	return false; // allow the default system animation to run when app is done
    }
}
