package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.PopularFragment;
import com.pixplit.android.ui.elements.PixAlertDialog;


public class PopularActivity extends PixCompFeedActivity {
	boolean displayReconnectMsg = false;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity_with_drawer);
        this.setToolBarTitle(getApplicationContext().getString(R.string.popular));
        this.updateToolbar();

        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	            if (args != null) {
	            	displayReconnectMsg = args.getBoolean(PixDefs.FRAG_ARG_DISPLAY_RECONNECT_MSG, false);
	            }
	        }
	        
	        if (mFragmentManager != null) {
	        	if (mFragmentManager.findFragmentByTag(EFragmentType.POPULAR.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        		PopularFragment fragment = PixApp.getPopularFragment(args);
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.POPULAR.toString());
		        	ft.commit();
	        	}
	        }
        }
	}
	
	@Override
    protected boolean usePixFinishAnimation() {
		return false; // allow the default system animation to run when app is done
    }
	
	@Override
	public void onStart() {
		super.onStart();
		
		if (displayReconnectMsg) {
			new PixAlertDialog(this, 
					null, 
					getApplicationContext().getResources().getString(R.string.reconnect_your_account),
					getApplicationContext().getResources().getString(R.string.please_log_in_using_your_account), 
					getApplicationContext().getResources().getString(R.string.ok)).show();
			displayReconnectMsg = false;
		}
	}
}
