package com.pixplit.android.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.instagram.InstaLoginDialog;
import com.pixplit.android.instagram.InstaLoginDialog.InstaAuthListener;
import com.pixplit.android.ui.CreateSplitFragment;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.PixPhotoPickManager;
import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;
import com.pixplit.android.util.PixSplitPhotoPickManager;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;


public class CreateNewSplitActivity extends PixFbBaseActivity implements PixSplitPhotoPickerInterface{
	
	PixPhotoPickManager photoPickManager;
	PhotoFetcherDelegate mPhotoDelegate;
	
	public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
		CreateSplitFragment.setJoinedCompImage(joinedImage);
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        this.setToolbarVisible(false);
        this.updateToolbar();
        this.setFetchNotifications(false);
        
        if (savedInstanceState == null) {
        	Bundle args = null;
	        Intent callingIntent = getIntent();
	        if (callingIntent != null) {
	        	args = callingIntent.getExtras();
	        }
	        if (mFragmentManager != null) {
	        	if (mFragmentManager.findFragmentByTag(EFragmentType.CREATE_SPLIT.toString()) == null) {
	        		FragmentTransaction ft = mFragmentManager.beginTransaction(); 
		        	CreateSplitFragment fragment = CreateSplitFragment.newInstance(args);
		        	ft.add(R.id.fragment_container, fragment, EFragmentType.CREATE_SPLIT.toString());
		        	ft.commit();
	        	}
	        }
        }
        
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getPhotoPickManager().onActivityResult(requestCode, resultCode, data);
        
        if (PixDefs.ACTIVITY_RESULT_CODE_CREATE_NEW_SPLIT == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null &&
        				extras.getBoolean(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, false)) {
        			// pass on the data that the split creation process was done successful.
        			setResult(Activity.RESULT_OK, data);

        			// release the bitmaps static references
        			releaseBitmaps();
        			
        			// in case the split creation was finished successfully, finish up.
        			finish();
        			return;
        		}
        	}
        }
        super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
    protected void onResumeFragments() {
		super.onResumeFragments();
		getPhotoPickManager().deliverPhotoPickResult();
	}

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
    	getPhotoPickManager().onSaveInstanceState(savedInstanceState);
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	getPhotoPickManager().onRestoreInstanceState(savedInstanceState);
    }
        
	@Override
	public void registerPhotoPickerDelegate(PhotoFetcherDelegate delegate) {
		getPhotoPickManager().registerPhotoPickerDelegate(delegate);
	}

	@Override
	public void unregisterPhotoPickerDelegate() {
		getPhotoPickManager().unregisterPhotoPickerDelegate();
	}

	@Override
	public void photoPickRequest() {
		getPhotoPickManager().photoPickRequest();
	}

	@Override
	public void photoCaptureRequest() {
		getPhotoPickManager().photoCaptureRequest();
	}
	
	private PixPhotoPickManager getPhotoPickManager() {
		if (photoPickManager == null) {
			photoPickManager = new PixSplitPhotoPickManager(this);
		}
		return photoPickManager;
	}
	
	@Override
    public void finish() {
    	super.finish();
    	this.overridePendingTransition(0, 0);
    }
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// release the bitmaps static references
		releaseBitmaps();
	}
	
	private void releaseBitmaps() {
		CreateSplitFragment.setJoinedCompImage(null);
	}

	@Override
	public void photoPickFromInstaRequest() {
		if (Utils.isValidString(UserInfo.getInstagramToken())) {
			pickPhotoFromInsta();
		} else {
			// authenticate with instagram first
			new InstaLoginDialog(this, new InstaAuthListener() {
				
				@Override
				public void onError(String error) {
					String failed = PixApp.getContext().getString(R.string.failed);
					PixProgressIndicator.showMessage(CreateNewSplitActivity.this, failed, true);
					PixProgressIndicator.hideMessageWithIndication(false, failed);
					UserInfo.clearInstagramUserInfo();
				}
				
				@Override
				public void onComplete() {
					pickPhotoFromInsta();
				}
			}).show();
		}
	}
	
	private void pickPhotoFromInsta() {
		if (mActivitiesController != null) {
			mActivitiesController.gotoPickInstagramPhoto(this);
		}
	}
	
	@Override
	public void photoPickFromFacebookRequest() {
		PixProgressIndicator.showMessage(this, getString(R.string.authenticating), true);
		this.fBRequestPhotoPick(new FbPhotoPickDelegate() {
			
			@Override
			public void pickPhotoFromFacebookFailure() {
				PixProgressIndicator.hideMessageWithIndication(false, getString(R.string.failed));
			}
			
			@Override
			public void pickPhotoFromFacebook() {
				PixProgressIndicator.hideMessage();
				if (mActivitiesController != null) {
					mActivitiesController.gotoPickFacebookPhoto(CreateNewSplitActivity.this);
				}
			}
		});
	}
	
	@Override
	public void photoPickFromGoogleRequest() {
		if (mActivitiesController != null) {
			mActivitiesController.gotoPickGooglePhoto(this);
		}
	}
}
