package com.pixplit.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.pixplit.android.R;
import com.pixplit.android.ui.FollowFriendsFragment;

public class FollowFriendsActivity extends PixFbBaseActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        setToolBarTitle(getApplicationContext().getString(R.string.follow_your_friends));
        this.updateToolbar();
        this.setFetchNotifications(false);
        
        Bundle args = null;
        Intent callingIntent = getIntent();
        if (callingIntent != null) {
        	args = callingIntent.getExtras();
        }
        
        if (savedInstanceState == null) {
	        if (mFragmentManager != null) {
	        	FragmentTransaction ft = mFragmentManager.beginTransaction(); 
	        	FollowFriendsFragment fragment = FollowFriendsFragment.newInstance(args);
	        	ft.add(R.id.fragment_container, fragment);
	        	ft.commit();
	        }
        }
    }
}
