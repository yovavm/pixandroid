package com.pixplit.android.ui.activities;


public interface ToolBarDelegate {
	/**
	 * @return true in case the event was handled/intercepted by the method, 
	 * false otherwise
	 */
	public boolean onToolbarHomePressIntercepted();
	public void onToolbarCenterClick();
	
}
