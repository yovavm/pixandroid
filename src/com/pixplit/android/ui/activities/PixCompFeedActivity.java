package com.pixplit.android.ui.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.PixCompFeedDelegate;

public class PixCompFeedActivity extends PixFbBaseActivity implements PixCompFeedControllerInterface{

	ArrayList<PixCompFeedDelegate> mCompFeedDelegates;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompFeedDelegates = new ArrayList<PixCompFeedDelegate>();
	}
	
	@Override
	public void registerCompFeedDelegate(PixCompFeedDelegate delegate) {
		if (mCompFeedDelegates != null) {
			mCompFeedDelegates.add(delegate);
		}
	}

	@Override
	public void unregisterCompFeedDelegate(PixCompFeedDelegate delegate) {
		if (mCompFeedDelegates != null) {
			mCompFeedDelegates.remove(delegate);
		}
	}

	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (PixDefs.ACTIVITY_RESULT_CODE_COMP_COMMENTS == requestCode) {
        	if (resultCode == Activity.RESULT_OK && data != null) {
        		Bundle extras = data.getExtras();
        		if (extras != null) {
        			int compCommentsNum = extras.getInt(PixDefs.INTENT_EXTRA_COMP_COMMENTS_NUM, -1);
        			if (compCommentsNum >= 0 && mCompFeedDelegates != null) {
        				for (PixCompFeedDelegate delegate : mCompFeedDelegates) {
        					if (delegate != null) {
        						delegate.onTotalCommentsUpdate(compCommentsNum);
        					}
        				}
        			}
        		}
        	}
        }
	}
}
