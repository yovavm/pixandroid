package com.pixplit.android.ui.activities;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.facebook.FacebookException;
import com.facebook.model.GraphObject;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.facebook.widget.WebDialog.RequestsDialogBuilder;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.fb.FacebookSessionManager;
import com.pixplit.android.fb.FacebookSessionManager.FbSessionDelegate;
import com.pixplit.android.fb.FbActivityInterface;
import com.pixplit.android.fb.FbAuthenticationRequestHandler;
import com.pixplit.android.fb.FbBaseRequestHandler;
import com.pixplit.android.fb.FbBaseRequestHandler.FbRequestHandlerDelegate;
import com.pixplit.android.fb.FbBaseRequestHandler.FbRequestHandlerType;
import com.pixplit.android.fb.FbConnectDelegate;
import com.pixplit.android.fb.FbPostRequestHandler;
import com.pixplit.android.fb.FbProcessType;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.PixFbAuthHttpRequest;
import com.pixplit.android.ui.AuthRequestDelegate;
import com.pixplit.android.ui.BillingHelperController;
import com.pixplit.android.ui.PixBaseFragment;
import com.pixplit.android.ui.elements.GetStartedPopup;
import com.pixplit.android.ui.elements.GetStartedPopup.GetStartedPopupDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.billing.IabHelper;

public class PixFbBaseActivity extends GPlusBaseActivity implements FbSessionDelegate, 
																				 FbRequestHandlerDelegate,
																				 FbActivityInterface,
																				 BillingHelperController{
//	private static final String TAG = "PixFbConnectActivity";
	
	// FB
	FacebookSessionManager mFbSessionManager;
	Hashtable<String, FbBaseRequestHandler> fbRequestHandlers = null;
	// FB request handlers
	private static final String REQUEST_HANDLERS_MAP_KEY = "REQUEST_HANDLERS_MAP_KEY %d";
	private static final String REQUEST_HANDLERS_NUM_KEY = "REQUEST_HANDLERS_NUM_KEY";
	private static final String REQUEST_HANDLER_TYPE_KEY = "REQUEST_HANDLERS %d Type";
	private static final String REQUEST_HANDLER_CONN_DELEGATE_KEY = "REQUEST_HANDLER_CONN_DELEGATE %d ";
	private static final String REQUEST_HANDLER_PROCESS_TYPE_KEY = "REQUEST_HANDLER_PROCESS_TYPE %d ";
	
	private static final int REQUEST_HANDLER_TYPE_BASE = 1;
	private static final int REQUEST_HANDLER_TYPE_AUTHENTICATION = 2;
	private static final int REQUEST_HANDLER_TYPE_POST = 3;
	
//	IabHelper mBillingHelper;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mFbSessionManager = new FacebookSessionManager();
        if (mFbSessionManager != null) {
        	mFbSessionManager.onCreate(this, savedInstanceState, this);
        }
        if (fbRequestHandlers == null) {
        	fbRequestHandlers = new Hashtable<String, FbBaseRequestHandler>();
        }
	}
	
	@Override
    public void onResume() {
        super.onResume();
        
        if (mFbSessionManager != null) {
        	mFbSessionManager.onResume();
        }
	}
	
	@Override
    public void onPause() {
        super.onPause();
     
        if (mFbSessionManager != null) {
        	mFbSessionManager.onPause();
        }
    }
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mFbSessionManager != null) {
        	mFbSessionManager.onActivityResult(requestCode, resultCode, data);
        }
        
        switch (requestCode) {
        case PixDefs.ACTIVITY_RESULT_CODE_PURCHASE_ITEM:
//			if (mBillingHelper != null) {
//				try {
//					mBillingHelper.handleActivityResult(requestCode, resultCode, data);
//				} catch (Exception e) {
//					//Utils.pixLog(TAG, "Exception while calling IAB handleActivityResult:"+e);
//	        		e.printStackTrace();
//				}
//			}
			break;
        default:
			break;
		}
    
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mFbSessionManager != null) {
        	mFbSessionManager.onDestroy();
        }
//        if (mBillingHelper != null) {
//        	try {
//        		mBillingHelper.dispose();
//        	} catch (Exception e) {
//        		//Utils.pixLog(TAG, "Exception while trying to dispose IAB:"+e);
//        		e.printStackTrace();
//        	}
//        }
//        mBillingHelper = null;
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	if (mFbSessionManager != null) {
    		mFbSessionManager.onSaveInstanceState(outState);
    	}
    	this.saveRequestHandlersState(outState);
    	super.onSaveInstanceState(outState);
    }
    
    @Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	if (savedInstanceState != null) {
    		this.restoreRequestHandlersState(savedInstanceState);
    	}
    }
    
    @Override
	public void onRequestHandlerFinished(FbBaseRequestHandler handler) {
		PixProgressIndicator.hideMessage();
		if (handler != null) {
			// start by removing the handler - as the following may start a new handler
			fbRequestHandlers.remove(handler.getType().toString());
			if (handler.getType() == FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER) {
				FbAuthenticationRequestHandler authRequestHandler = (FbAuthenticationRequestHandler)handler;
				if (authRequestHandler.getProcessType() == FbProcessType.FB_PROCESS_AUTHENTICATION) {
					// connection delegate is null, next if will not happen in this case
					//Utils.pixLog(TAG, "==> FB onRequestHandlerFinished. calling fbAuthWithPixServer.");
					fbAuthWithPixServer(authRequestHandler.getUserInfo());
				} 
				
				FbConnectDelegate connectionDelegate = authRequestHandler.getConnectionDelegate();
				if (connectionDelegate != null) {
					
					if (authRequestHandler.getProcessType() == FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH) {
						// check the permissions and return appropriate answer 
						if (mFbSessionManager != null &&
								mFbSessionManager.isOpenForPublish()) {
							// set the facebook default publish values
							PixAppState.UserInfo.setPostLikesToFb(true);
							connectionDelegate.onFbConnectFinished(authRequestHandler.getProcessType());
						} else {
							connectionDelegate.onFbConnectFailed(authRequestHandler.getProcessType());
						}
					} else {
						connectionDelegate.onFbConnectFinished(authRequestHandler.getProcessType());
					}
				}
			}
		}
	}

	@Override
	public void onRequestHandlerFailed(FbBaseRequestHandler handler, Error error) {
		PixProgressIndicator.hideMessageWithIndication(false, 
				getApplicationContext().getResources().getString(R.string.failed));
		if (handler != null) {
			fbRequestHandlers.remove(handler.getType().toString());
			FbConnectDelegate connectionDelegate = ((FbAuthenticationRequestHandler)handler).getConnectionDelegate();
			if (connectionDelegate != null) {
				connectionDelegate.onFbConnectFailed(handler.getProcessType());
			}
		}
	}
	
	@Override
	public void onOpenSessionFinished() {
		//Utils.pixLog(TAG, "==> FB onOpenSessionFinished called. activity:"+this.toString());
		FbAuthenticationRequestHandler requestHandler = 
				(FbAuthenticationRequestHandler)fbRequestHandlers.get(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
		if (requestHandler != null && 
				requestHandler.getProcessType() == FbProcessType.FB_PROCESS_UPDATE_PERMISSIONS) {
			// do nothing on open, wait for session token update
			return;
		}
		if (mFbSessionManager != null) {
			mFbSessionManager.fetchUserInfo(requestHandler);
		}
	}

	@Override
	public void onOpenSessionTokenUpdate() {
		// token update may interest publish authorization request handlers
		FbAuthenticationRequestHandler requestHandler = (FbAuthenticationRequestHandler)fbRequestHandlers.get(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
		if (requestHandler != null) {
			FbConnectDelegate connectionDelegate = requestHandler.getConnectionDelegate();
			if (connectionDelegate != null) {
				if (requestHandler.getProcessType() == FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH) {
					// remove the publish authorization request handler
					fbRequestHandlers.remove(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
					// check the permissions and return appropriate answer 
					if (mFbSessionManager != null &&
							mFbSessionManager.isOpenForPublish()) {
						// set the facebook default publish values
						PixAppState.UserInfo.setPostLikesToFb(true);
						connectionDelegate.onFbConnectFinished(requestHandler.getProcessType());
					} else {
						connectionDelegate.onFbConnectFailed(requestHandler.getProcessType());
					}
				} else if (requestHandler.getProcessType() == FbProcessType.FB_PROCESS_UPDATE_PERMISSIONS) {
					fbRequestHandlers.remove(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
					connectionDelegate.onFbConnectFinished(requestHandler.getProcessType());
				}
			}
		}
	}
	
	@Override
	public void onOpenSessionFailed(Error error) {
		// open session request failed, remove the request handler (if exist) that sent the request
		FbAuthenticationRequestHandler requestHandler = (FbAuthenticationRequestHandler)fbRequestHandlers.get(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
		if (requestHandler != null) {
			fbRequestHandlers.remove(FbRequestHandlerType.FB_AUTHENTICATE_REQUEST_HANDLER.toString());
			FbConnectDelegate connectionDelegate = requestHandler.getConnectionDelegate();
			if (connectionDelegate != null) {
				connectionDelegate.onFbConnectFailed(requestHandler.getProcessType());
			}
		}
	}    
	
	@Override
	public void authenticateWithFb() {
		PixProgressIndicator.showMessage(this, getApplicationContext().getResources().getString(R.string.authenticating), true);
		if (PixAppState.UserInfo.isUserLogedin()) {
			this.authenticateWithFbFinished();
		} else {
			this.authenticateWithFbRequest(FbProcessType.FB_PROCESS_AUTHENTICATION, null, null);
		}
	}
	
	@Override
	public void authorizePublishToFb(FbProcessType processType, FbConnectDelegate connectDelegate) {
		if (mFbSessionManager != null) {
			if (mFbSessionManager.isOpenForPublish()) {
				//Utils.pixLog(TAG, "FB session is already open for publish");
				if (connectDelegate != null) {
					connectDelegate.onFbConnectFinished(processType);
				}
			} else {
				FbAuthenticationRequestHandler requestHandler = new FbAuthenticationRequestHandler(this);
				requestHandler.setProcessType(processType);
				requestHandler.setConnectionDelegate(connectDelegate);
				fbRequestHandlers.put(requestHandler.getType().toString(), requestHandler);
				
				mFbSessionManager.openSessionWithPublishPermissions(this);
			}
		} else {
			if (connectDelegate != null) {
				connectDelegate.onFbConnectFailed(processType);
			}
		}
	}

	protected void authenticateWithFbFinished() {
		PixProgressIndicator.hideMessageWithIndication(true, 
				getApplicationContext().getResources().getString(R.string.authenticated));
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, 
				PixDefs.APP_EVENT_SUCCESSFUL_FACEBOOK_CONNECT, 
				null);
//		PixBilling.queryInventoryAsync(mBillingHelper);
		
		Bundle args = new Bundle();
		args.putBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS, true);
		finish();
		if (mActivitiesController != null) {
			mActivitiesController.gotoFollowFriends(this, args);
		}
	}
	
	private void authenticateWithFbFailed() {
		PixProgressIndicator.hideMessageWithIndication(false, 
				getApplicationContext().getResources().getString(R.string.failed));
	}

	private void fbAuthWithPixServer(PixUserInfo userInfo) {
		PixProgressIndicator.showMessage(this, getApplicationContext().getResources().getString(R.string.authenticating), true);
		if (userInfo != null) {
			userInfo.setToken(PixAppState.UserInfo.craetePixToken(userInfo.getUserName()));
			new PixFbAuthHttpRequest(fbAuthRequestDelegate, userInfo).send();
		} else {
			authenticateWithFbFailed();
		}
	}

	private AuthRequestDelegate fbAuthRequestDelegate = new AuthRequestDelegate(this, AuthRequestDelegate.AUTH_PROCESS_TYPE_FB) {
		@Override
		public void onAuthFinished() {
			authenticateWithFbFinished();
		}
		@Override
		public void onAuthFailed() {
			authenticateWithFbFailed();
		}
	};
	
	@Override
	public boolean isFbLinked() {
		if (mFbSessionManager != null) {
			return mFbSessionManager.isLinked();
		}
		return false;
	}
	
	@Override
	public boolean isFbOpenForPublish() {
		if (mFbSessionManager != null) {
			return mFbSessionManager.isOpenForPublish();
		}
		return false;
	}

	@Override
	public void postLikesToFb(String compId) {
		if (Utils.isValidString(compId) && 
				mFbSessionManager != null &&
				mFbSessionManager.isOpenForPublish() &&
				PixAppState.UserInfo.getPostLikesToFb()) {
			GraphObject graphObject = GraphObject.Factory.create();
			graphObject.setProperty("object", PixDefs.pixSplitLandingPageUrlPrefix+compId);
			mFbSessionManager.postRequest(null, PixDefs.facebookRequestPathShareLike, graphObject);
		}
	}

	@Override
	public void logoutFb() {
		if (mFbSessionManager != null) {
			mFbSessionManager.logout();
		}
	}

    @Override
	public void logoutRequest(boolean withReconnectMsg){
		super.logoutRequest(withReconnectMsg);
		this.logoutFb();
    }
    
    public GetStartedPopupDelegate mGetStartedDelegate = new GetStartedPopupDelegate() {
		@Override
		public void signUpWithEmailPressed() {
			if (mActivitiesController != null) {
				mActivitiesController.gotoSignUp(PixFbBaseActivity.this);
			}
		}
		@Override
		public void signInPressed() {
			if (mActivitiesController != null) {
				mActivitiesController.gotoSignIn(PixFbBaseActivity.this);
			}
		}
		@Override
		public void connectWithFacebookPressed() {
			PixFbBaseActivity.this.authenticateWithFb();
		}
		
		@Override
		public void termsPressed() {
			if (mActivitiesController != null) {
				Bundle args = new Bundle();
				args.putString(PixDefs.FRAG_ARG_TITLE, PixApp.getContext().getString(R.string.terms_of_service));
				args.putString(PixDefs.FRAG_ARG_URL, PixHttpAPI.termsUrl);
				mActivitiesController.gotoWebView(PixFbBaseActivity.this, args);
			}
		}
		
		@Override
		public void connectWithGPlusPressed() {
			PixFbBaseActivity.this.signInWithGPlus();
		}
    };
    
    @Override
    public void showGetStarted() {
    	if (mActivitiesController != null &&
    			mActivitiesController.isActivityRunning(this.toString())) {
    		new GetStartedPopup(this, mGetStartedDelegate).show();
    	}
    }
    
    @Override
    public boolean verifyUser() {
		if (!UserInfo.isUserLogedin()) {
			this.showGetStarted();
			return false;
		}
		return true;
    }
    
	@Override
	public void authenticateWithFbRequest(FbProcessType processType, FbConnectDelegate connectDelegate, List<String> permissions){
		FbAuthenticationRequestHandler requestHandler = new FbAuthenticationRequestHandler(this);
		requestHandler.setProcessType(processType);
		requestHandler.setConnectionDelegate(connectDelegate);
		fbRequestHandlers.put(requestHandler.getType().toString(), requestHandler);
		if (mFbSessionManager != null) {
			if (mFbSessionManager.hasOpenSession(permissions)) {
				mFbSessionManager.fetchUserInfo(requestHandler);
			} else {
				mFbSessionManager.openSession(this, permissions);
			}
		}
	}

	@Override
	public void shareNewCompInFb(String compId, String users) {
		if (Utils.isValidString(compId) && 
				mFbSessionManager != null &&
				mFbSessionManager.isOpenForPublish()) {
			GraphObject graphObject = GraphObject.Factory.create();
			graphObject.setProperty("split", PixDefs.pixSplitLandingPageUrlPrefix+compId);
			graphObject.setProperty("image[0][url]", PixDefs.pixSplitImageUrlPrefix+compId);
			graphObject.setProperty("image[0][user_generated]", "true");
			if (Utils.isValidString(users)) {
				graphObject.setProperty("tags", users);
			}
			mFbSessionManager.postRequest(null, PixDefs.facebookRequestPostNewComp, graphObject);
		}
	}
	
	private void saveRequestHandlersState(Bundle outState) {
    	if (outState == null) return;
    	Enumeration<String> keys = fbRequestHandlers.keys();
    	int handlerIndex = 0;
    	while(keys.hasMoreElements() ) {
    		String key = keys.nextElement();
    		FbBaseRequestHandler handler = fbRequestHandlers.get(key);
    		if (handler != null) {
    			int handlerType;
    			if (handler instanceof FbAuthenticationRequestHandler) {
    				handlerType = REQUEST_HANDLER_TYPE_AUTHENTICATION;
    				FbAuthenticationRequestHandler authRequestHandler = (FbAuthenticationRequestHandler)handler;
    				if (authRequestHandler.getConnectionDelegate() != null &&
    						mFragmentManager != null &&
    						authRequestHandler.getConnectionDelegate() instanceof PixBaseFragment) {
    					try {
    						mFragmentManager.putFragment(outState, 
    							String.format(REQUEST_HANDLER_CONN_DELEGATE_KEY, handlerIndex), 
    							(PixBaseFragment)authRequestHandler.getConnectionDelegate());
    					} catch (Exception e) {
    						// on some cases FB call backs are not called and therefore not remove from the hashmap
    						//Utils.pixLog(TAG, "Failed to store FB connection delegate:"+e);
    					}
    				}
    			} else if (handler instanceof FbPostRequestHandler) {
    				handlerType = REQUEST_HANDLER_TYPE_POST;
    			} else {
    				handlerType = REQUEST_HANDLER_TYPE_BASE;
    			}
    			outState.putInt(String.format(REQUEST_HANDLER_TYPE_KEY, handlerIndex), handlerType);
    			outState.putString(String.format(REQUEST_HANDLERS_MAP_KEY, handlerIndex), key);
    			if (handler.getProcessType() != null) {
    				outState.putInt(String.format(REQUEST_HANDLER_PROCESS_TYPE_KEY, handlerIndex), FbProcessType.getInt(handler.getProcessType()));
    			}
    			handlerIndex++;
    		}
    	}
    	outState.putInt(REQUEST_HANDLERS_NUM_KEY, handlerIndex);
    }
    
    private void restoreRequestHandlersState(Bundle savedInstanceState) {
    	if (savedInstanceState == null) return;
    	
    	int handlersNum = savedInstanceState.getInt(REQUEST_HANDLERS_NUM_KEY);
    	for (int i=0; i<handlersNum; i++) {
    		int handlerType = savedInstanceState.getInt(String.format(REQUEST_HANDLER_TYPE_KEY, i));
    		if (handlerType > 0) {
    			FbBaseRequestHandler requestHandler = null;
    			int processType = savedInstanceState.getInt(String.format(REQUEST_HANDLER_PROCESS_TYPE_KEY, i));
    			FbConnectDelegate connectDelegate = null;
    			// create the request handler
    			if (handlerType == REQUEST_HANDLER_TYPE_AUTHENTICATION) {
    				requestHandler = new FbAuthenticationRequestHandler(this);
    				// try to fetch the connection delegate (if exists, not mandatory)
    				Fragment fragment = mFragmentManager.getFragment(savedInstanceState, String.format(REQUEST_HANDLER_CONN_DELEGATE_KEY, i));
    				if (fragment != null &&
    						fragment instanceof FbConnectDelegate) {
    					connectDelegate = (FbConnectDelegate)fragment;
    					((FbAuthenticationRequestHandler)requestHandler).setConnectionDelegate(connectDelegate);
    				}
    			} else if (handlerType == REQUEST_HANDLER_TYPE_POST) {
    				requestHandler = new FbPostRequestHandler(this);
    			} else {
    				requestHandler = new FbBaseRequestHandler(this);
    			}
    			
    			if (requestHandler != null) {
	    			if (processType > 0) {
						requestHandler.setProcessType(FbProcessType.getType(processType));
					}
	    			String requestHandlerKey = savedInstanceState.getString(String.format(REQUEST_HANDLERS_MAP_KEY, i));
	    			
	    			// add the request handler to the hash map
	    			if (fbRequestHandlers != null &&
	    					Utils.isValidString(requestHandlerKey)) {
	    				fbRequestHandlers.put(requestHandlerKey, requestHandler);
	    			}
    			}
    		}
    	}
    }
    
    // Billing
	@Override
	public IabHelper getIabHelper() {
//		return mBillingHelper;
		return null;
	}
	
	public interface FbPhotoPickDelegate {
		public void pickPhotoFromFacebook();
		public void pickPhotoFromFacebookFailure();
	}
	// request to pick photo from FB. this process checks and authenticates for user_photos permission
	public void fBRequestPhotoPick(final FbPhotoPickDelegate delegate) {
		// there are 3 options here: 1. no session, 
		// 2. session open with missing permissions, 3. session open with needed permissions.
		if (mFbSessionManager == null) {
			if (delegate != null) {
				delegate.pickPhotoFromFacebookFailure();
			}
			return;
		}
		
		if (!mFbSessionManager.isLinked()) {
			// Facebook is not linked, link FB with the required permissions
			authenticateWithFbRequest(
					FbProcessType.FB_PROCESS_PICK_PHOTO, 
					new FbConnectDelegate() {
						
						@Override
						public void onFbConnectFinished(FbProcessType processType) {
							// validate the process type
							if (processType == FbProcessType.FB_PROCESS_PICK_PHOTO) {
								if (mFbSessionManager.isOpenForFetchPhotos()) {
									if (delegate != null) {
										delegate.pickPhotoFromFacebook();
									}
									return;
								} else {
									// update the session permissions to fetch photos
									updateFbPermissionsToFetchPhotos(delegate);
								}
							}
							if (delegate != null) {
								delegate.pickPhotoFromFacebookFailure();
							}
						}
						
						@Override
						public void onFbConnectFailed(FbProcessType processType) {
							if (delegate != null) {
								delegate.pickPhotoFromFacebookFailure();
							}
						}
					},
					FacebookSessionManager.FETCH_PHOTOS_PERMISSIONS);
		} else {
			// FB is linked. check if it has the required permissions
			if (mFbSessionManager.isOpenForFetchPhotos()) {
				// FB linked with the required permissions. proceed to pick a photo
				if (delegate != null) {
					delegate.pickPhotoFromFacebook();
				}
			} else {
				// required permissions are missing. ask for additional permissions first.
				updateFbPermissionsToFetchPhotos(delegate);
			}
		}
	}

	private void updateFbPermissionsToFetchPhotos(final FbPhotoPickDelegate delegate) {
		authenticateWithFbRequest(
			FbProcessType.FB_PROCESS_UPDATE_PERMISSIONS, 
			new FbConnectDelegate() {
				
				@Override
				public void onFbConnectFinished(FbProcessType processType) {
					// validate the process type
					if (processType == FbProcessType.FB_PROCESS_UPDATE_PERMISSIONS) {
						if (delegate != null) {
							delegate.pickPhotoFromFacebook();
						}
						return;
					}
					if (delegate != null) {
						delegate.pickPhotoFromFacebookFailure();
					}
				}
				
				@Override
				public void onFbConnectFailed(FbProcessType processType) {
					if (delegate != null) {
						delegate.pickPhotoFromFacebookFailure();
					}
				}
			},
			FacebookSessionManager.FETCH_PHOTOS_PERMISSIONS);
	}
	
	@Override
	public void inviteFbFriends() {
		Bundle params = new Bundle();
		params.putString("title", getString(R.string.join_me_on_pixplit));
	    params.putString("message", getString(R.string.join_me_on_pixplit_body));
	    sendRequest(null, params);
	}
	
	public interface FbRequestDelegate {
		public void onRequestSuccess();
		public void onRequestFailure();
	}
	public void sendRequest(final FbRequestDelegate delegate, Bundle params) {
		if (!mFbSessionManager.hasOpenSession(null)) {
			if (delegate != null) {
				delegate.onRequestFailure();
			}
			return;
		}
		RequestsDialogBuilder builder = new RequestsDialogBuilder(this,
				mFbSessionManager.getActiveSession(), params);

		builder.setOnCompleteListener(new OnCompleteListener() {

		    @Override
		    public void onComplete(Bundle values, FacebookException error) {

		        if (error != null){
		        	if (delegate != null) {
						delegate.onRequestFailure();
					}
		        }
		        else{
		            final String requestId = values.getString("request");
		            if (requestId != null) {
		            	if (delegate != null) {
		    				delegate.onRequestSuccess();
		    			}
		            } 
		            else {
		            	if (delegate != null) {
		    				delegate.onRequestFailure();
		    			}
		            }
		        }
		    }
		});

		WebDialog requestDialog = builder.build();
		requestDialog.show();
	}
}
