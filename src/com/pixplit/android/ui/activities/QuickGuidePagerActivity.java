package com.pixplit.android.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.PagerControllerInterface;
import com.pixplit.android.ui.QuickGuidePageFragment;
import com.viewpagerindicator.CirclePageIndicator;


public class QuickGuidePagerActivity extends PixBaseActivity implements PagerControllerInterface{
	private final int QUICK_GUIDE_PAGES_NUM = 4;
    private ViewPager mPager;
    private FragmentPagerAdapter mAdapter;
    private CirclePageIndicator mIndicator;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_guide_tabs_layout);
        this.updateToolbar();
        
        mAdapter = new QuickGuideTabsAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        if (mPager != null) {
        	mPager.setAdapter(mAdapter);
        }
        
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        if (mIndicator != null) {
        	mIndicator.setViewPager(mPager);
        }
    }
		

	class QuickGuideTabsAdapter extends FragmentPagerAdapter {
        public QuickGuideTabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	Bundle args = new Bundle();
        	args.putInt(PixDefs.FRAG_ARG_PAGE_NUM, position);
        	QuickGuidePageFragment fragment = QuickGuidePageFragment.newInstance(args);
        	return fragment;
        }
        
        @Override
        public int getCount() {
            return QUICK_GUIDE_PAGES_NUM;
        }
    }


	@Override
	public void setPageNum(int pageNum) {
		if (mPager != null) {
			mPager.setCurrentItem(pageNum, true);
		}
	}
}