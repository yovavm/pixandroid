package com.pixplit.android.ui;


import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.CompsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixCompositions;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.CompLikeRequestDelegate;
import com.pixplit.android.http.CompLikeRequestDelegate.LikeRequestDelegate;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.AdminDeleteCompHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.CompLikeHttpRequest;
import com.pixplit.android.http.request.CompsFeedHttpRequest;
import com.pixplit.android.http.request.DeleteCompHttpRequest;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;
import com.pixplit.android.ui.activities.PixCompFeedControllerInterface;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.appreciation.AppreciationManager;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class PixFeedFragment extends PixListFragment<PixComposition> implements PixCompFeedDelegate{
	public static final String TAG = "PixFeedsFragment";
	private CompCommentsUpdateDelegate mCommentsUpdateDelegate = null;
	private PixCompFeedControllerInterface mFeedController = null;
	
    // Appreciation manager
    AppreciationManager mAppreciationManager = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAppreciationManager = AppreciationManager.getAppreciationManager(getContext());
	}
    
	@Override
	public void onStart(){
    	super.onStart();
    	this.mAppreciationManager.onStart();
	}
	    
    @Override
	public void onStop() {
    	super.onStop();
    	this.mAppreciationManager.onStop();
    }
    
	@Override
	public void onResume(){
    	super.onResume();
    	showGetStartedBarIfNeeded();
    	if (mActivitiesController != null && 
    			mActivitiesController.needRefresh(
    					ActivitiesControllerInterface.REFRESH_CONTEXT_COMPS_FEED)) {
    		this.refreshList();
    	}
	}
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mFeedController = (PixCompFeedControllerInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement PixCompFeedControllerInterface");
	    }
    }

    @Override
    public void onDetach () {
    	super.onDetach();
    	mFeedController = null;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mFeedController != null) {
    		mFeedController.registerCompFeedDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mFeedController != null) {
    		mFeedController.unregisterCompFeedDelegate(this);
    	}
    	this.mAppreciationManager.onDestroy();
    }
    
	@Override
	protected void handleListResponse(PixObject response, Bundle requestContext) {
		ArrayList<PixComposition> compsFeed = null;
		
		if (response != null && response instanceof PixCompositions) {
			PixCompositions compositions = (PixCompositions)response;
			compsFeed = this.removeDuplications(compositions.getItems());
			if (compositions.hasFeatured()) {
				compsFeed = this.addFeaturedToFeed(compsFeed, compositions.getFeatured());
			}
			updateAdapterData(compsFeed);
			handleEmptyFeed();
		}
	}
	
	private static final int featuredGap = 3;
	private ArrayList<PixComposition> addFeaturedToFeed(ArrayList<PixComposition> compsFeed,
								   ArrayList<PixComposition> featured) {
		if (compsFeed == null || featured == null || featured.size() <= 0) return null;
		ArrayList<PixComposition> feedWithFeatured = new ArrayList<PixComposition>();
		// insert a featured comp every 'featuredGap' regular comps.
		feedWithFeatured.add(featured.remove(0));
		while (featured.size() > 0 && compsFeed.size() >= featuredGap) {
			for (int i=0; i < featuredGap; i++) {
				feedWithFeatured.add(compsFeed.remove(0));
			}
			feedWithFeatured.add(featured.remove(0));
		}
		while (compsFeed.size() > 0) {
			feedWithFeatured.add(compsFeed.remove(0));
		}
		return feedWithFeatured;
	}

	protected ArrayList<PixComposition> removeDuplications(ArrayList<PixComposition> comps) {
		ArrayList<PixComposition> cleanComps = new ArrayList<PixComposition>();
		if (comps != null) {
			for (PixComposition comp : comps) {
				if (comp != null) {
					PixComposition lastInsertedComp = null;
					if (cleanComps.size() > 0) {
						lastInsertedComp = cleanComps.get(cleanComps.size()-1);
					}
					if (lastInsertedComp == null ||
						!lastInsertedComp.getCompId().equalsIgnoreCase(comp.getCompId())) {
						cleanComps.add(comp);
					}
				}
			}
		}
		return cleanComps;
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	String url = getRequestUrl();
    	BaseVolleyHttpRequest request = null;
    	if (Utils.isValidString(url)) {
    		request = new CompsFeedHttpRequest(this, url, Priority.IMMEDIATE);
    	}
    	return request;
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	CompsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new CompsListAdapter(getActivity(), this, R.layout.composition_layout);
    	}
    	return adapter;
	}

	@Override
	public void onCompLikeClick(PixComposition comp, boolean liked) {
		LikeRequestDelegate requestDelegate = new LikeRequestDelegate() {
			@Override
			public void LikeRequestFinishedWithError(PixError error) {
				if (mActivity != null) {
					mActivity.onError(error);
				}
			}
			@Override
			public void LikeRequestFinished(PixComposition comp) {
				if (mFbActivity != null &&
						comp != null) {
					mFbActivity.postLikesToFb(comp.getCompId());
				}
				PixFeedFragment.this.mAppreciationManager.onEvent("Like Click");
			}
		};
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, 
				(liked) ? PixDefs.APP_EVENT_LIKE_PRESSED : PixDefs.APP_EVENT_DISLIKE_PRESSED, null);
		
		CompLikeRequestDelegate likeDelegate = new CompLikeRequestDelegate(comp, requestDelegate);
		PixAppState.getState().getRequestQueue().add(new CompLikeHttpRequest(likeDelegate, comp, liked));
	}

	@Override
	public void onCompCommentClick(PixComposition comp, CompCommentsUpdateDelegate delegate) {
		if (comp != null && Utils.isValidString(comp.getCompId())) {
			mCommentsUpdateDelegate = delegate;
			Bundle args = new Bundle();
			args.putString(PixDefs.FRAG_ARG_COMP_ID, comp.getCompId());
			if (mActivitiesController != null) {
				mActivitiesController.gotoCompComments(getActivity(), args);
			}
		}
    }
	
	@Override
	public void onCompRelatedClick(PixComposition comp) {
		if (comp != null && Utils.isValidString(comp.getCompId())) {
			Bundle args = new Bundle();
			args.putString(PixDefs.FRAG_ARG_COMP_ID, comp.getCompId());
			if (mActivitiesController != null) {
				mActivitiesController.gotoCompRelated(getActivity(), args);
			}
		}
	}
	
	@Override
    public void onCompResplitClick(PixComposition comp) {
		if (!PixComposition.isResplitAllow(comp) || !this.verifyUser()) return;
		PixProgressIndicator.showMessage(getContext(), PixApp.getContext().getString(R.string.fetching_split), true);
		CompsFeedHttpRequest resplitRequest = new CompsFeedHttpRequest(new BaseVolleyHttpRequest.RequestDelegate() {
			
			@Override
			public void requestFinishedWithError(BaseVolleyHttpRequest request,
					PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
			}
			@Override
			public void requestFinished(BaseVolleyHttpRequest request,
					PixObject response) {
				if (response != null && response instanceof PixCompositions) {
					PixCompositions compositions = (PixCompositions)response;
					if (compositions.getCount() == 1 &&
							compositions.getItems() != null &&
							compositions.getItems().size() == 1) {
						final PixComposition comp = compositions.getItems().get(0);
						if (comp != null) {
							// fetch the joined split image and start the create process
							PixImagesLoader.loadImage(comp.getImage(), 
								new BitmapFetcherDelegate() {
									@Override
									public void onBitmapFetchFinished(String url, 
																	PixBitmapHolder bitmapHolder) {
										if (bitmapHolder != null) {
											PixProgressIndicator.hideMessage();
											resplitComp(comp, bitmapHolder);												
											return;												
										}
										PixProgressIndicator.hideMessageWithIndication(false, 
												PixApp.getContext().getString(R.string.failed));
									}
									@Override
									public void onBitmapFetchFailed() {
										PixProgressIndicator.hideMessageWithIndication(false, 
												PixApp.getContext().getString(R.string.failed));
									}
								}, Priority.IMMEDIATE);
							return;
						}
					}
				}
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
			}
		}, PixHttpAPI.SINGLE_COMP_URL_PREFIX+comp.getRootId(), Priority.IMMEDIATE);
		
		PixAppState.getState().getRequestQueue().add(resplitRequest);
	}
	
	private void resplitComp(PixComposition comp, PixBitmapHolder compImageHolder) {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_ACTIONS, PixDefs.APP_EVENT_RESPLIT_PRESSED, null);
		
		final Bundle args = new Bundle();
		args.putInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, comp.getCompType());
		comp.serialize(args);
		
		if (mActivitiesController != null) {
			mActivitiesController.gotoCreateNewSplit(getActivity(), args, compImageHolder);
		}
	}
	
	@Override
	public void deleteCompRequest(final String compId) {
		if (getActivity() != null) {
			PixProgressIndicator.showMessage(getActivity(), getActivity().getApplicationContext().getResources().getString(R.string.deleting), true);
		}
		
		if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				// admin delete comp request
				new AdminDeleteCompHttpRequest(new RequestDelegate() {
					@Override
					public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
						PixProgressIndicator.hideMessageWithIndication(false, 
								PixApp.getContext().getResources().getString(R.string.failed));						
					}
					@Override
					public void requestFinished(BaseHttpRequest request, PixObject response) {
						PixProgressIndicator.hideMessageWithIndication(true, 
								PixApp.getContext().getResources().getString(R.string.deleted));
						if (request != null) {
				    		Bundle requestContext = request.getRequestContext();
				    		if (requestContext != null) {
					    		onCompDeletion(requestContext.getString(PixDefs.REQ_CTX_COMP_ID));
					    	}
				    	}
					}
				}, compId).send();
				// comp deleted by admin, no need to continue
				return;
			}
		} 

		// regular user delete comp request
		new DeleteCompHttpRequest(compId, new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, 
						PixApp.getContext().getResources().getString(R.string.failed));
				if (mActivity != null) {
					mActivity.onError(error);
				}
			}
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, 
						PixApp.getContext().getResources().getString(R.string.deleted));
				if (request != null) {
		    		Bundle requestContext = request.getRequestContext();
		    		if (requestContext != null) {
			    		onCompDeletion(requestContext.getString(PixDefs.REQ_CTX_COMP_ID));
			    	}
		    	}
			}
		}).send();
	}
	
	protected void onCompDeletion(String compId) {
		// in case the calling activity is a compositions feed, refresh it 
		if (mActivitiesController != null) {
			mActivitiesController.markForRefresh(
					ActivitiesControllerInterface.REFRESH_CONTEXT_COMPS_FEED, true);
		}
	}
	
	protected void handleEmptyFeed() {
		if (mFragmentView != null && getActivity() != null) {
			// check if the feed had an 'empty feed' image and remove it (happens in case my feed is empty)
    		ViewGroup feedsListContainer = (ViewGroup)mFragmentView.findViewById(R.id.feedsListContainer);
    		if (feedsListContainer != null) {
    			// check if the list if empty
	    		if (mAdapter != null && !mAdapter.hasData()) {
		    		// add "empty feed" image
		    		if (feedsListContainer instanceof RelativeLayout) {
						ImageView emptyFeed = new ImageView(getActivity());
						emptyFeed.setId(PixDefs.emptyFeedViewId);
						emptyFeed.setBackgroundResource(R.drawable.empty_feed_img);
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.WRAP_CONTENT, 
								RelativeLayout.LayoutParams.WRAP_CONTENT);
						params.addRule(RelativeLayout.CENTER_IN_PARENT);
						feedsListContainer.addView(emptyFeed, params);
					}
		    	} else {
	    			ImageView emptyFeed = (ImageView) feedsListContainer.findViewById(PixDefs.emptyFeedViewId);
					if (emptyFeed != null) {
						feedsListContainer.removeView(emptyFeed);
					}
				}
    		}
		}
	}
	
	public void setLoginToolbarVisibility(int visibility) {
    	if (getView() == null) {
    		// the fragment was not created yet.
    		return;
    	}
    	
    	ViewGroup getStarted = (ViewGroup) getView().findViewById(R.id.getStarted);
    	if (visibility == View.VISIBLE) {
    		if (getStarted != null) {
    			getStarted.setVisibility(View.VISIBLE);
    				TextView getStartedBtn = (TextView)getStarted.findViewById(R.id.getStartedBtn);
    				getStartedBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_ANY_FEED, 
									PixDefs.APP_EVENT_GET_STARTED_PRESSES, null);
							if (mFbActivity != null) {
								mFbActivity.showGetStarted();
							}
						}
					});
	    		}
    	} else {
    		if (getStarted != null) {
    			getStarted.setVisibility(View.GONE);
	    	}
    	}
    }
	
	private void showGetStartedBarIfNeeded(){
    	if (!UserInfo.isUserLogedin()) {
    		setLoginToolbarVisibility(View.VISIBLE);
    	} else {
    		setLoginToolbarVisibility(View.INVISIBLE);
    	}
    }

	// PixCompFeedDelegate
	@Override
	public void onTotalCommentsUpdate(int totalComments) {
		if (mCommentsUpdateDelegate != null) {
			mCommentsUpdateDelegate.onTotalCommentsUpdate(totalComments);
		}
	}
	
	@Override
	public void removeCompositionFromFeed(String compId) {
		/*
		 *  as each feed has it's own adapter implementation, this should 
		 *  be override by sub-classes in order to implement properly
		 */
	}
	
	@Override
	public void refreshContent() {
		this.refreshList();
	}
}
