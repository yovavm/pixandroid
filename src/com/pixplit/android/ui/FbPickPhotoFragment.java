package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.FbPhotoListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.FacebookPhoto;
import com.pixplit.android.data.FacebookPhotos;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.fb.FbPhotoPickDelegate;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.FbPhotosHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.viewholders.FbItemsPack;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class FbPickPhotoFragment extends PixListFragment<FacebookPhoto> implements FbPhotoPickDelegate{
	String fbRefreshUrl;
	private static final String loading = PixApp.getContext().getString(R.string.loading);
	private static final String failed = PixApp.getContext().getString(R.string.failed);
	
	public static FbPickPhotoFragment newInstance(Bundle args) {
		FbPickPhotoFragment fragment = new FbPickPhotoFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        
        fbRefreshUrl = PixHttpAPI.FACEBOOK_PHOTOS_URL;
        Bundle args = getArguments();
        if (args != null) {
        	String albumId = args.getString(PixDefs.FRAG_ARG_ALBUM_ID);
        	if (Utils.isValidString(albumId)) {
        		fbRefreshUrl = String.format(PixHttpAPI.FACEBOOK_ALBUM_PHOTOS_URL, albumId);
        	}
        }
        setFragmentType(EFragmentType.FACEBOOK_PICK_PHOTO);
    }
	
	@Override
	public void onResume() {
		super.onResume();
		if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(PixApp.getContext().getResources().getString(R.string.select_photo));
        	mToolbarController.setToolBarCustomView(R.drawable.import_facebook);
        }
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.fb_pick_photo_layout, container, false);
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof FacebookPhotos) {
			FacebookPhotos items = (FacebookPhotos)response;
			this.fbRefreshUrl = items.getNext();
			ArrayList<FbItemsPack> parsedData = (ArrayList<FbItemsPack>) 
					FbItemsPack.createCompsPackList(items.getItems(), 3);
			updateAdapterData(parsedData);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new FbPhotosHttpRequest(this, this.fbRefreshUrl, Priority.HIGH);
	}
    
    @Override
    protected void addNextParam(BaseVolleyHttpRequest request) {
    	// next page is handled internally by facebook url's mechanism
    }
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	FbPhotoListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new FbPhotoListAdapter(getActivity(), this, this);
    	}
    	return adapter;
	}

	@Override
	public void onPhotoPick(FacebookPhoto photo) {
		if (getActivity() == null || photo == null) return;
		final String imageUrl = photo.getImage();
		if (!Utils.isValidString(imageUrl)) return;
		
		PixProgressIndicator.showMessage(getActivity(), loading, true);
		PixImagesLoader.loadImage(imageUrl, 
				new BitmapFetcherDelegate() {
					@Override
					public void onBitmapFetchFinished(String url, PixBitmapHolder holder) {
						if (getActivity() == null || !Utils.isValidString(imageUrl)) return;
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
								PixDefs.APP_EVENT_SUCCESSFUL_FACEBOOK_IMPORT, 
								null);

						PixProgressIndicator.hideMessage();
						Intent resultIntent = new Intent();
						resultIntent.putExtra(PixDefs.INTENT_EXTRA_PICKED_PHOTO_URL, imageUrl);
						getActivity().setResult(Activity.RESULT_OK, resultIntent);
						getActivity().finish();	
					}
					
					@Override
					public void onBitmapFetchFailed() {
						PixProgressIndicator.hideMessageWithIndication(false, failed);
						if (getActivity() != null) {
							getActivity().finish();	
						}
					}
				}, Priority.IMMEDIATE);
	}
	
	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		super.requestFinishedWithError(request, error);
		if (mActivity != null) {
			mActivity.finishFragment();
		}
	}
}
