package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.GoogleImagesListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.GoogleImage;
import com.pixplit.android.data.GoogleImages;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.google.GooglePhotoPickDelegate;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.GoogleImagesHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.elements.SearchBar;
import com.pixplit.android.ui.elements.SearchBar.SearchDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.viewholders.GoogleImagesPack;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class GooglePickPhotoFragment extends PixListFragment<GoogleImages> 
												implements GooglePhotoPickDelegate, SearchDelegate {
	private GoogleImagesHttpRequest searchRequest = null;
	
	private static final String loading = PixApp.getContext().getString(R.string.loading);
	private static final String failed = PixApp.getContext().getString(R.string.failed);
	
	public static GooglePickPhotoFragment newInstance() {
		GooglePickPhotoFragment fragment = new GooglePickPhotoFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.GOOGLE_PICK_PHOTO);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.google_search_images_layout, container, false);
		
		SearchBar searchBar = (SearchBar) 
				mFragmentView.findViewById(R.id.searchBar);
		if (searchBar != null) {
			searchBar.setDelegate(this);
		}
		
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof GoogleImages) {
			GoogleImages imagesResponse = (GoogleImages)response;
			ArrayList<GoogleImagesPack> parsedData = (ArrayList<GoogleImagesPack>) 
					GoogleImagesPack.createItemsPackList(imagesResponse.getItems(), 3);
			updateAdapterData(parsedData);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return searchRequest;
	}
    
    @Override
    protected void addNextParam(BaseVolleyHttpRequest request) {
    	String next = getNextPage();
    	if (request == null || !Utils.isValidString(next)) return;
    	request.addParam(PixHttpAPI.GOOGLE_SEARCH_START_PARAM, next);
    }
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	GoogleImagesListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new GoogleImagesListAdapter(getActivity(), this, this);
    	}
    	return adapter;
	}
	
	@Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
		super.requestFinishedWithError(request, error);
		Utils.showMessage(PixApp.getContext().getResources().getString(R.string.error) + ". " +
				PixApp.getContext().getResources().getString(R.string.cannot_show_data), true);
	}

	@Override
	public void onSearchRequest(String searchText) {
    	if (!Utils.isValidString(searchText)) return;
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SEARCH_IMAGE, PixDefs.APP_EVENT_SEARCH_PRESSED, null);
    	searchRequest = new GoogleImagesHttpRequest(this, searchText, Priority.IMMEDIATE);
		startSpinner();
		refreshList();
	}

	@Override
	public void onPhotoPick(GoogleImage item) {
		if (getActivity() == null || item == null) return;
		final String imageUrl = item.getUrl();
		if (!Utils.isValidString(imageUrl)) return;
		
		PixProgressIndicator.showMessage(getActivity(), loading, true);
		PixImagesLoader.loadImage(imageUrl, 
				new BitmapFetcherDelegate() {
					@Override
					public void onBitmapFetchFinished(String url, PixBitmapHolder holder) {
						if (getActivity() == null || !Utils.isValidString(imageUrl)) return;
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, 
								PixDefs.APP_EVENT_SUCCESSFUL_GOOGLE_IMPORT, 
								null);

						PixProgressIndicator.hideMessage();
						Intent resultIntent = new Intent();
						resultIntent.putExtra(PixDefs.INTENT_EXTRA_PICKED_PHOTO_URL, imageUrl);
						getActivity().setResult(Activity.RESULT_OK, resultIntent);
						getActivity().finish();	
					}
					
					@Override
					public void onBitmapFetchFailed() {
						PixProgressIndicator.hideMessageWithIndication(false, failed);
						getActivity().finish();	
					}
				}, Priority.IMMEDIATE);
	}
}
