package com.pixplit.android.ui;

public interface PixCompFeedDelegate {
	public void refreshContent();
	public void onTotalCommentsUpdate(int totalComments);
	public void removeCompositionFromFeed(String compId);
}