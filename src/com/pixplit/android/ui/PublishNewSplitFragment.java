package com.pixplit.android.ui;

import java.util.HashMap;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.capture.SplitPreviewFrame;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.data.PixNewComp;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.fb.FbConnectDelegate;
import com.pixplit.android.fb.FbProcessType;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionStatus;
import com.pixplit.android.globals.PixCompositionType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.globals.PixLocationServices;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.DeleteTempCompHttpRequest;
import com.pixplit.android.http.request.NewCompHttpRequest;
import com.pixplit.android.http.request.NewTempCompHttpRequest;
import com.pixplit.android.http.request.SetCompDataHttpRequest;
import com.pixplit.android.http.request.ShareCompByEmailHttpRequest;
import com.pixplit.android.ui.activities.InviteFollowersDelegate;
import com.pixplit.android.ui.activities.InviteFollowersDelegateInterface;
import com.pixplit.android.ui.elements.ChooseTagsContainer;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.fragments.PixFbBaseFragment;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public class PublishNewSplitFragment extends PixFbBaseFragment implements RequestDelegate, 
																		FbConnectDelegate,
																		InviteFollowersDelegate{
  	private static final String TAG = "PublishNewSplitFragment";
	PixCompositionType compType;
	PixCompositionMetadata compMetadata;
	Bitmap splitBitmap;
	String reservedCompId;
	
	EditText addTitle;
	RelativeLayout includeLocationBtn;
	RelativeLayout inviteFollowersBtn;
	ImageView inviteFollowersBtnIcon;
	RelativeLayout shareByEmailBtn;
	ImageView shareByEmailBtnIcon;
	EditText shareByEmailInputAddress;
	RelativeLayout shareToFbBtn;
	ImageView shareToFbBtnIcon;
	ImageView shareToFbBtnSwitch;
	ImageView includeLocationSwitch;
	ImageView includeLocationBtnIcon;
	boolean includeLocation;
	boolean shareToFb;
	String inviteeList = null;
	EditText addCustomTags;
	ChooseTagsContainer chooseTagsContainer;
	
	boolean isPostingImage = false;
	
	private static PixBitmapHolder joinedCompImage;
	private static PixBitmapHolder activePartOrigBitmap;
    public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
    	if (joinedImage != null) {
    		joinedImage.useBitmap(TAG);
    	}
    	if (joinedCompImage != null) {
    		joinedCompImage.releaseBitmap(TAG);
    	}
    	joinedCompImage = joinedImage;
    }
    public static void setActivePartOrigBitmap(PixBitmapHolder partBitmap) {
    	if (partBitmap != null) {
    		partBitmap.useBitmap("");
    	}
    	if (activePartOrigBitmap != null) {
    		activePartOrigBitmap.releaseBitmap("");
    	}
    	activePartOrigBitmap = partBitmap;
    }
    
    // save state
    private static final String PUBLISH_FRAG_INCLUDE_LOCATION_KEY = "PUBLISH_FRAG_INCLUDE_LOCATION_KEY";
    private static final String PUBLISH_FRAG_SHARE_TO_FB_KEY = "PUBLISH_FRAG_SHARE_TO_FB_KEY";
    private static final String PUBLISH_FRAG_RESERVED_COMP_ID_KEY = "PUBLISH_FRAG_RESERVED_COMP_ID_KEY";
    private static final String PUBLISH_FRAG_FINAL_SPLIT_PATH_KEY = "PUBLISH_FRAG_FINAL_SPLIT_PATH_KEY";
    private static final String PUBLISH_FRAG_INVITEE_LIST_KEY = "PUBLISH_FRAG_INVITEE_LIST_KEY";

    InviteFollowersDelegateInterface mInviteFollowersController;
    
	public static PublishNewSplitFragment newInstance(Bundle args) {
		PublishNewSplitFragment fragment = new PublishNewSplitFragment();
		if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
            fragment.setArguments(args);
        }        
		return fragment;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.PUBLISH_SPLIT);
        
        if (savedInstanceState != null) {
        	// restore from saved state
        	this.compMetadata = PixCompositionMetadata.deserialize(savedInstanceState);
        	this.compType = PixCompositionType.deserialize(savedInstanceState);
        	this.includeLocation = savedInstanceState.getBoolean(PUBLISH_FRAG_INCLUDE_LOCATION_KEY);
        	this.shareToFb = savedInstanceState.getBoolean(PUBLISH_FRAG_SHARE_TO_FB_KEY);
        	this.reservedCompId = savedInstanceState.getString(PUBLISH_FRAG_RESERVED_COMP_ID_KEY);
        	String filePath = savedInstanceState.getString(PUBLISH_FRAG_FINAL_SPLIT_PATH_KEY);
        	if (Utils.isValidString(filePath)) {
       			this.splitBitmap = Utils.getBitmapFromFile(filePath, 0, 0);
       			if (this.splitBitmap != null) {
       				Utils.deleteBitmapFile(filePath);
       			}
       		}
        	this.inviteeList = savedInstanceState.getString(PUBLISH_FRAG_INVITEE_LIST_KEY);
       		
        } else {
        	// initiate from arguments
        	Bundle args = getArguments();
            if (args != null) {
            	this.compMetadata = PixCompositionMetadata.deserialize(args);
            	this.compType = PixCompositionType.deserialize(args);
            }
            includeLocation = false;
            shareToFb = false;
            reservedCompId = null;
            
            if (this.compType != null && this.compMetadata != null) {
            	// check if the composition is completed
            	boolean isCompleted = (compType.getNumOfExistingComponents()+1 == compType.getMaxComponentsForType());
            	compMetadata.setCompleted(isCompleted);

    	        this.splitBitmap = SplitPreviewFrame.createFinalSplit(getActivity(), 
								this.compType, 
								(joinedCompImage != null) ? joinedCompImage.getBitmap() : null, 
								(activePartOrigBitmap != null) ? activePartOrigBitmap.getBitmap() : null,
								this.compMetadata.getNewComponentIndex(),
								this.compMetadata.isCompleted(), 
								this.compMetadata.hasBorder());
    			if (this.splitBitmap != null) {
    				this.isPostingImage = true;
    				new NewTempCompHttpRequest(this, splitBitmap).send();
    			}
            }
        }
    }
    
	@Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (outState != null) {
    		if (this.compType != null) {
    			this.compType.serialize(outState);
        	}
        	if (this.compMetadata != null) {
        		this.compMetadata.serialize(outState);
        	}
        	if (this.includeLocation) {
        		outState.putBoolean(PUBLISH_FRAG_INCLUDE_LOCATION_KEY, includeLocation);
        	}
        	if (this.shareToFb) {
        		outState.putBoolean(PUBLISH_FRAG_SHARE_TO_FB_KEY, shareToFb);
        	}
        	if (Utils.isValidString(this.reservedCompId)) {
        		outState.putString(PUBLISH_FRAG_RESERVED_COMP_ID_KEY, reservedCompId);
        	}
        	if (this.splitBitmap != null) {
       			String filePath = Utils.storeBitmapInTmpFile(this.splitBitmap, 
       					PUBLISH_FRAG_FINAL_SPLIT_PATH_KEY);
       			if (Utils.isValidString(filePath)) {
       				outState.putString(PUBLISH_FRAG_FINAL_SPLIT_PATH_KEY, filePath);
       			}
       		}
        	if (Utils.isValidString(this.inviteeList)) {
        		outState.putString(PUBLISH_FRAG_INVITEE_LIST_KEY, this.inviteeList);
        	}
    	}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.publish_new_split_layout, container, false);
    	
    	if (mFragmentView != null) {
	    	// title
	    	addTitle = (EditText) mFragmentView.findViewById(R.id.addCompTitle);
	    	if (addTitle != null) {
		    	if (compType != null && compType.getState() != PixCompositionStatus.PixStatusNew) {
		    		// it is not allowed to edit the title when joining a split 
		    		addTitle.setEnabled(false);
					if (compMetadata != null && Utils.isValidString(compMetadata.getTitle())) {
						addTitle.setText(compMetadata.getTitle());
					} else {
						addTitle.setVisibility(View.GONE);
					}
				} else {
					addTitle.setImeOptions(EditorInfo.IME_ACTION_DONE);
					addTitle.setInputType(EditorInfo.TYPE_CLASS_TEXT);
				}
	    	}
	    	
	    	// include location
	    	includeLocationBtn = (RelativeLayout) mFragmentView.findViewById(R.id.includeLocationBtn);
	    	if (includeLocationBtn != null) {
	    		includeLocationSwitch = (ImageView) mFragmentView.findViewById(R.id.includeLocationSwitch);
	    		includeLocationBtnIcon = (ImageView) mFragmentView.findViewById(R.id.includeLocationBtnIcon);
	    		updateIncludeLocationCheckbox();
	    		includeLocationBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (getActivity() != null) {
							includeLocation = PixLocationServices.enableLocationServices(getActivity(), !includeLocation);
							updateIncludeLocationCheckbox();
						}
					}
				});
	    	}
	    	
	    	// invite followers
	    	inviteFollowersBtn = (RelativeLayout) mFragmentView.findViewById(R.id.inviteFollowersBtn);
	    	if (inviteFollowersBtn != null) {
	    		inviteFollowersBtnIcon = (ImageView)inviteFollowersBtn.findViewById(R.id.inviteFollowersIcon);
	    		if (compMetadata != null && !compMetadata.isCompleted()) {
		    		inviteFollowersBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							inviteFollowers();
						}
					});
	    		} else {
	    			inviteFollowersBtn.setVisibility(View.GONE);
	        	}
	    		if (Utils.isValidString(this.inviteeList)) {
	    			// may happen in case of restore from saved state
	    			this.onInviteFollowersResult(inviteeList);
	    		}
	    	}
	    	
	    	// Share to Facebook
	    	shareToFbBtn = (RelativeLayout) mFragmentView.findViewById(R.id.shareToFacebookBtn);
	    	if (shareToFbBtn != null) {
	    		shareToFbBtnSwitch = (ImageView) mFragmentView.findViewById(R.id.ShareToFacebookSwitch);
	    		shareToFbBtnIcon = (ImageView) mFragmentView.findViewById(R.id.shareToFacebookBtnIcon);
	    		updateShareToFbCheckbox();
	    		shareToFbBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (shareToFb == false) {
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_EVENT_SHARE_TO_FACEBOOK_PRESSED, null);
							authorizeShareToFb();
						} else {
							// turning off share to facebook requires no interaction with facebook
							shareToFb = false;
							updateShareToFbCheckbox();
						}
	
					}
				});
	    	}
	    	
	    	// share by Email
	    	shareByEmailBtn = (RelativeLayout) mFragmentView.findViewById(R.id.shareByEmailBtn);
	    	if (shareByEmailBtn != null) {
	    		shareByEmailBtnIcon = (ImageView) shareByEmailBtn.findViewById(R.id.shareByEmailBtnIcon);
	    		updateShareByEmailIcon();
	    		shareByEmailInputAddress = (EditText) shareByEmailBtn.findViewById(R.id.shareByEmailInputAddresses);
	    		shareByEmailInputAddress.setImeOptions(EditorInfo.IME_ACTION_DONE);
	    		shareByEmailInputAddress.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
	    		if (shareByEmailInputAddress != null) {
	    			shareByEmailInputAddress.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
						@Override
						public void afterTextChanged(Editable s) {
							updateShareByEmailIcon();
						}
					});
	    		}
	    	}
	    	
	    	/* allow adding tags on join feature
	    	// previous tags
	    	PixTextView prevCompTags = (PixTextView)mFragmentView.findViewById(R.id.prevCompTags);
	    	if (compType.getState() == PixCompositionStatus.PixStatusNew) {
	    		// new composition. no previous tags
	    		if (prevCompTags != null) {
	    			prevCompTags.setVisibility(View.GONE);
	    		}
	    	} else {
	    		// not new, this composition is from a join action
	    		if (prevCompTags != null) {
	    			if (compMetadata != null) {
	    				String prevTags = compMetadata.getTags();
	    				if (Utils.isValidString(prevTags)) {
	    					// tags are a comma separated words (tag1,tag2,..), format them to: #tag1, #tag2,...
	    					String formatedTags = "#"+ prevTags.replaceAll("([,])", ", #");
	    					prevCompTags.setText(formatedTags);
	    				} else {
	    		    		if (prevCompTags != null) {
	    		    			prevCompTags.setVisibility(View.GONE);
	    		    		}
	    				}
	    			}
	    		}
	    	}
	    	*/
	    	// add tags
	    	addCustomTags = (EditText) mFragmentView.findViewById(R.id.addCompTags);
	    	if (addCustomTags != null) {
		    	if (compType != null && compType.getState() != PixCompositionStatus.PixStatusNew) {
		    		// disable the fields which are not allowed to edit 
	    			addCustomTags.setVisibility(View.GONE);
		    	} else {
		    		addCustomTags.setImeOptions(EditorInfo.IME_ACTION_DONE);
		    		addCustomTags.setInputType(EditorInfo.TYPE_CLASS_TEXT);
		    	}
	    		/* allow adding tags on join feature
	    		addCustomTags.setImeOptions(EditorInfo.IME_ACTION_DONE);
	    		addCustomTags.setInputType(EditorInfo.TYPE_CLASS_TEXT);
	    		*/
	    	}
    	}
        return mFragmentView;
    }
    
    @Override
	public void onStart() {
		super.onStart();
		if (mToolbarController != null) {
			mToolbarController.addToolbarTextBtn(
	    			R.drawable.done_btn_selector, 
					PixDefs.TOOLBAR_BTN_ID_DONE,
	        		new OnClickListener() {
						@Override
						public void onClick(View v) {
							delayFinishingUpIfNeeded(0);
						}
					}, 
					PixApp.getContext().getResources().getString(R.string.done));
		}
	}
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mInviteFollowersController != null) {
    		mInviteFollowersController.registerInviteFollowersDelegate(this);
    	}
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (includeLocation) {
			PixLocationServices.enableLocationServices(getActivity(), false);
		}
		if (reservedCompId != null) {
			new DeleteTempCompHttpRequest(reservedCompId).send();
		}
    	if (mInviteFollowersController != null) {
    		mInviteFollowersController.unregisterInviteFollowersDelegate();
    	}
	}
	
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mInviteFollowersController = (InviteFollowersDelegateInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement InviteFollowersDelegateInterface");
	    }
    }

    @Override
    public boolean onToolbarHomePressIntercepted() {
    	if (mActivity != null) {
	    	// the home should act as back and return to the calling activity
    		mActivity.finishFragment();
    	}
        return true;
    }
    
    @Override
	public void requestFinished(BaseHttpRequest request, PixObject response) {
    	Bundle requestContext = null;
    	if (request != null) {
    		requestContext = request.getRequestContext();
    	}
    	
    	if (response != null && response instanceof PixNewComp) {
			PixNewComp data = (PixNewComp)response;
			if (data != null && data.getCompId() != null) {
				if (requestContext != null) {
					String compId = data.getCompId();
					String users = data.getUsers();
					int requestType = requestContext.getInt(PixDefs.REQ_CTX_NEW_COMP_TYPE);
					if (requestType == PixDefs.REQ_CTX_NEW_TEMP_COMP) {
						reservedCompId = compId; // store the comp ID locally in order to delete it in case the publish get canceled
					} else if (requestType == PixDefs.REQ_CTX_NEW_FINAL_COMP) {
						//Utils.pixLog(TAG, "loading split success!");
						onSplitCreationSuccess(requestContext, compId, users);
					}
				}
			}
		}

		boolean hideFinalIndication = true;
		if (requestContext != null) {
			int requestType = requestContext.getInt(PixDefs.REQ_CTX_NEW_COMP_TYPE);
			if (requestType == PixDefs.REQ_CTX_NEW_TEMP_COMP) {
				hideFinalIndication = false;
			} 
		}
		if (hideFinalIndication) {
			PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getResources().getString(R.string.done));
		} else {
			PixProgressIndicator.hideMessage();
		}
		this.isPostingImage = false;
	}

    private void onSplitCreationSuccess(Bundle requestContext, String compId, String users) {
    	// share to facebook
		boolean shareToFb = requestContext.getBoolean(PixDefs.REQ_CTX_NEW_COMP_SHARE_TO_FB);
		if (shareToFb && mFbActivity != null) {
			mFbActivity.shareNewCompInFb(compId, users);
		}
	
		// share by email
		String email = requestContext.getString(PixDefs.REQ_CTX_NEW_COMP_SHARE_BY_EMAIL);
		if (Utils.isValidString(email) &&
				Utils.isValidEmail(email) &&
				Utils.isValidString(compId)) {
			String subject = String.format(PixApp.getContext().getResources().getString(R.string.share_main_subject_prefix), 
					PixAppState.UserInfo.getUserFullName());
			String body = PixApp.getContext().getResources().getString(R.string.share_body_prefix) + 
					Html.fromHtml(new StringBuilder()
				    .append(String.format(PixApp.getContext().getResources().getString(R.string.share_body_postfix), compId))
				    .toString());
			PixAppState.getState().getRequestQueue().add(
					new ShareCompByEmailHttpRequest(email, subject, body));
		}
		
		String parentId = requestContext.getString(PixDefs.REQ_CTX_NEW_COMP_PARENT_ID);
		
		// release the static bitmaps references
		setJoinedCompImage(null);
		setActivePartOrigBitmap(null);
		
		if (getActivity() != null) {
			Intent resultIntent = new Intent();
			resultIntent.putExtra(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, true);
			if (Utils.isValidString(compId)) {
				resultIntent.putExtra(PixDefs.INTENT_EXTRA_NEW_SPLIT_COMP_ID, compId);
			}
			if (Utils.isValidString(parentId)) {
				resultIntent.putExtra(PixDefs.INTENT_EXTRA_NEW_SPLIT_PARENT_ID, parentId);
			}
			getActivity().setResult(Activity.RESULT_OK, resultIntent);
			getActivity().finish();
		}
    }
    
	@Override
	public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
		this.isPostingImage = false;
		//Utils.pixLog(TAG, "Request finished with error");
		PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getResources().getString(R.string.failed));
		if (mActivity != null) {
			mActivity.onError(error);
		}
	}
	
	private void updateIncludeLocationCheckbox() {
    	if (includeLocationSwitch != null) {
	    	if (includeLocation) {
	    		includeLocationSwitch.setBackgroundResource(R.drawable.check_on);
	    		if (includeLocationBtnIcon != null) {
	    			includeLocationBtnIcon.setBackgroundResource(R.drawable.create_geo_icon_selected);
	    		}
	    	} else {
	    		includeLocationSwitch.setBackgroundResource(R.drawable.check_off);
	    		if (includeLocationBtnIcon != null) {
	    			includeLocationBtnIcon.setBackgroundResource(R.drawable.create_geo_icon);
	    		}
	    	}
    	}
    }

	private void updateShareToFbCheckbox() {
    	if (shareToFbBtnSwitch != null) {
	    	if (shareToFb) {
	    		shareToFbBtnSwitch.setBackgroundResource(R.drawable.check_on);
	    		if (shareToFbBtnIcon != null) {
	    			shareToFbBtnIcon.setBackgroundResource(R.drawable.create_facebook_icon_selected);
	    		}
	    	} else {
	    		shareToFbBtnSwitch.setBackgroundResource(R.drawable.check_off);
	    		if (shareToFbBtnIcon != null) {
	    			shareToFbBtnIcon.setBackgroundResource(R.drawable.create_facebook_icon);
	    		}
	    	}
    	}
    }
	
	private void updateShareByEmailIcon() {
		boolean hasValidMail = false;
		if (shareByEmailInputAddress != null && shareByEmailInputAddress.getText() != null) {
			String emailText = shareByEmailInputAddress.getText().toString();
			if (emailText != null && 
					Utils.isValidString(emailText) && 
					Utils.isValidEmail(emailText)) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_OPTIONS, PixDefs.APP_EVENT_SHARE_BY_EMAIL_PRESSED, null);
				hasValidMail = true;
			}
		}
		
		if (shareByEmailBtnIcon != null) {
			if (hasValidMail) {
				shareByEmailBtnIcon.setBackgroundResource(R.drawable.create_email_icon_selected);
			} else {
				shareByEmailBtnIcon.setBackgroundResource(R.drawable.create_email_icon);
			}
		}
	}

	private void inviteFollowers() {
		Bundle args = new Bundle();
		args.putString(PixDefs.FRAG_ARG_INVITEE_LIST, inviteeList);
		if (mActivitiesController != null) {
			mActivitiesController.gotoInviteFollowers(getActivity(), args);
		}
	}
	
	private void delayFinishingUpIfNeeded(final int delayedSeconds) {
		if (this.isPostingImage) {
			if (delayedSeconds == 0) {
				PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getResources().getString(R.string.uploading), false);		
			} else  if (delayedSeconds > 30) {
				// posting split may have race condition as two requests may return simultaneously - temp and final.
				PixProgressIndicator.hideMessage();

				// failed/didn't finish posting image in advance. continue anyway
				this.publishNewSplit();
				return;
			}
			// try again after 1 second
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					delayFinishingUpIfNeeded(delayedSeconds+1);
				}
			}, 1000);
			
		} else {
			this.publishNewSplit();
		}
		
	}
	
	private void publishNewSplit() {
		if (this.compMetadata != null) {
			// Add title
			if (this.addTitle != null && this.addTitle.getText() != null && 
					Utils.isValidString(this.addTitle.getText().toString())) {
				compMetadata.setTitle(this.addTitle.getText().toString());
			}
			
			// location
			if (includeLocation) {
				String location = PixLocationServices.getLocation();
				if (Utils.isValidString(location)){
					//Utils.pixLog(TAG, "setting location:"+location);
					compMetadata.setLocation(location);
				}
			}
			
			// share to facebook
			compMetadata.setShareWithFacebook(shareToFb);
			
			// share by email
			if (shareByEmailInputAddress != null && 
					shareByEmailInputAddress.getText() != null) {
				String email = shareByEmailInputAddress.getText().toString();
				if (Utils.isValidString(email) &&
						Utils.isValidEmail(email)) {
					compMetadata.setShareToEmailAddress(email);
				}
			}
			
			// tags
			String tags = null;
			if (addCustomTags != null && 
					addCustomTags.getText() != null && 
					Utils.isValidString(addCustomTags.getText().toString())) {
				Pattern p = Pattern.compile("[ ,.]");
				String[] customTags = TextUtils.split(addCustomTags.getText().toString(), p);
				for (String tag : customTags) {
					if (Utils.isValidString(tag)) {
						String cleanTag = null;
						if (tag.charAt(0) == '#') {
							if (tag.length() > 1) {
								cleanTag = tag.substring(1, tag.length());
							}
						} else {
							cleanTag = tag;
						}
						if (Utils.isValidString(cleanTag)) {
							if (tags == null) {
								tags = cleanTag;
							} else {
								tags = tags.concat(","+cleanTag);
							}
						}
					}
				}
			}
			// add the choosen tags
			if (chooseTagsContainer != null) {
				String choosenTags = chooseTagsContainer.getChoosenTags();
				if (tags != null) {
					tags = tags.concat(","+choosenTags);
				} else {
					tags = choosenTags;
				}
			}
			if (Utils.isValidString(tags)) {
				compMetadata.setTags(tags);
			}
			
			HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARAM_USED_GEO_LOCATION, (Utils.isValidString(compMetadata.getLocation())) ? "1" : "0");
    		context.put(PixDefs.APP_PARAM_SHARED_WITH_FACEBOOK, (compMetadata.getShareWithFacebook()) ? "1" : "0");
        	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CREATION, PixDefs.APP_EVENT_SPLIT_COMPLETED, context);

        	// publish
			if (reservedCompId != null) {
				compMetadata.setReservedCompId(reservedCompId);
				reservedCompId = null; // clear the reserved ID in order not to delete this composition (in case of back pressed)
				// we have previously uploaded the image successfully and have a compId. set the extra data 
				PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getResources().getString(R.string.finishing_up), false);
				new SetCompDataHttpRequest(this, this.compMetadata).send();
			} else {
				// if we don't have reserved id - we need to post the image again
				if (splitBitmap == null) {
					splitBitmap = SplitPreviewFrame.createFinalSplit(getActivity(), 
							this.compType, 
							(joinedCompImage != null) ? joinedCompImage.getBitmap() : null, 
							(activePartOrigBitmap != null) ? activePartOrigBitmap.getBitmap() : null, 
							this.compMetadata.getNewComponentIndex(),
	    	        		this.compMetadata.isCompleted(), 
	    	        		this.compMetadata.hasBorder());
				}
				if (splitBitmap != null) {
					compMetadata.setCompositionImage(splitBitmap);
				} else {
					//Utils.pixLog(TAG, "failed to create final split. aborting");
					return;
				}
				PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getResources().getString(R.string.uploading), false);
				new NewCompHttpRequest(this, this.compMetadata).send();
			}
		}
	}
	
	public void onInviteFollowersResult(String users) {
		inviteeList = users;
		if (this.compMetadata != null && Utils.isValidString(users)) {
			this.compMetadata.setInviteesList(users);
		}
		
		// update the text on the 'invite followers' button with the number of invitee
		if (inviteFollowersBtn != null) {
			TextView inviteFollowersBtnText = (TextView)inviteFollowersBtn.findViewById(R.id.inviteFollowersBtnText);
			if (inviteFollowersBtnText != null) {
				int inviteeNum = 0;
				if (Utils.isValidString(users)) {
					inviteeNum = Utils.countCharOccurrences(users, ',') + 1;
				}
				if (inviteeNum > 0) {
					inviteFollowersBtnText.setText(PixApp.getContext().getResources().getString(R.string.invite_followers)+
							String.format(" (%d)", inviteeNum));
					if (inviteFollowersBtnIcon != null) {
						inviteFollowersBtnIcon.setBackgroundResource(R.drawable.create_invite_icon_selected);
					}
				} else {
					inviteFollowersBtnText.setText(PixApp.getContext().getResources().getString(R.string.invite_followers));
					if (inviteFollowersBtnIcon != null) {
						inviteFollowersBtnIcon.setBackgroundResource(R.drawable.create_invite_icon);
					}
				}
			}
		}
	}

	private void authorizeShareToFb() {
		if (mFbActivity != null) {
			PixProgressIndicator.showMessage(getActivity(), 
					PixApp.getContext().getResources().getString(R.string.authenticating), true);
			mFbActivity.authenticateWithFbRequest(FbProcessType.FB_PROCESS_LINK_ACCOUNT, this, null);
		}
	}
	
	@Override
	public void onFbConnectFinished(FbProcessType processType) {
		// validate the process type
		if (processType == FbProcessType.FB_PROCESS_LINK_ACCOUNT) {
			if (mFbActivity != null) {
				mFbActivity.authorizePublishToFb(FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH, this);
			}
		} else {
			if (processType == FbProcessType.FB_PROCESS_AUTHORIZE_PUBLISH) {
				shareToFb = true;
				updateShareToFbCheckbox();
			}
		}
		PixProgressIndicator.hideMessage();
	}

	@Override
	public void onFbConnectFailed(FbProcessType processType) {
		//Utils.pixLog(TAG, "request to Facebook failed:"+processType);
		shareToFb = false;
		updateShareToFbCheckbox();
		PixProgressIndicator.hideMessageWithIndication(false, 
				PixApp.getContext().getString(R.string.failed));
	}
}
