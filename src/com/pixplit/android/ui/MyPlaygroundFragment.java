package com.pixplit.android.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;

public class MyPlaygroundFragment extends MyFeedFragment{
	//private static final String TAG = "MyPlaygroundFragment";
	
	public static MyPlaygroundFragment newInstance() {
		MyPlaygroundFragment fragment = new MyPlaygroundFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setAllowPaging(true);
        setRequestUrl(PixHttpAPI.PLAYGROUND_FEED_URL);
        setFragmentType(EFragmentType.MY_PLAYGROUND);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mFragmentView != null) {
			removeFragmentViewFromParent(); 
		} else {
			mFragmentView = inflater.inflate(R.layout.my_feeds_layout, container, false);
		}
		return mFragmentView;
    }
	
	@Override
	public void onFeedTabSelected() {
		this.showTipIfNeeded();
	}
	
	private void showTipIfNeeded() {
		if (!PixAppState.didTipDisplayed(PixDefs.PLAYGROUND_TIP_DISPLAYED) && 
				getActivity() != null) {
			final Dialog tipDialog = new Dialog(getActivity(), R.style.PixAlertDialog);
			
			if (tipDialog != null) {
				tipDialog.setContentView(R.layout.playground_tip_layout);
				View tip = tipDialog.findViewById(R.id.playgroundTip);
				if (tip != null) {
					tip.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							tipDialog.dismiss();
						}
					});
				}
				tipDialog.show();
			}
			PixAppState.setTipDisplayed(PixDefs.PLAYGROUND_TIP_DISPLAYED, true);
		}
	}
}