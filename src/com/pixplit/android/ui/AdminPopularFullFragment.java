package com.pixplit.android.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.PopularFullListAdapter;
import com.pixplit.android.adapters.PopularFullListAdapter.PopularFullAdapterDelegate;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixPopularComposition;
import com.pixplit.android.data.PixPopularCompositions;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.AdminPopularFullFeedHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;

public class AdminPopularFullFragment extends PixFeedFragment implements PopularFullAdapterDelegate{
	
	public static AdminPopularFullFragment newInstance() {
		AdminPopularFullFragment fragment = new AdminPopularFullFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        setFragmentType(EFragmentType.ADMIN_POPULAR_FULL);
        setRequestUrl(PixHttpAPI.ADMIN_POPULAR_FULL_URL);
        if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(PixApp.getContext().getResources().getString(R.string.admin_popular_feed));
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.theme_feed_layout, container, false);
		return mFragmentView;
	}
	
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
		BaseVolleyHttpRequest request = new AdminPopularFullFeedHttpRequest(this, 
				getRequestUrl(), Priority.IMMEDIATE);
		request.addAdminIdentity();
		return request;
	}
	
	@Override
	protected PixBaseListAdapter<?> createAdapter() {
		PopularFullListAdapter adapter = new PopularFullListAdapter(getActivity(), 
				this, R.layout.composition_layout, this);
		return adapter;
	}
	
	@Override
	protected void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixPopularCompositions) {
			PixPopularCompositions compositions = (PixPopularCompositions)response;
			ArrayList<PixPopularComposition> compsFeed = updatePopularActive(compositions);
			updateAdapterData(compsFeed);
			handleEmptyFeed();
		}
	}

	private ArrayList<PixPopularComposition> updatePopularActive(
			PixPopularCompositions compositions) {
		if (compositions == null) return null;
		HashMap<String, Boolean> activeComps = compositions.getActive();
		ArrayList<PixPopularComposition> compsFeed = new ArrayList<PixPopularComposition>();
		for (PixComposition comp : compositions.getItems()) {
			boolean isActive = comp != null &&
					  activeComps.containsKey(comp.getCompId());
			PixPopularComposition popularComp = new PixPopularComposition(comp, isActive);
			compsFeed.add(popularComp);
		}
		return compsFeed;
	}

	@Override
	public void onAdminNotAuthenticated() {
		if (mActivity != null) {
			mActivity.onAdminNotAuthenticatedError();
		}
	}		
}
