package com.pixplit.android.ui;


public interface MyFeedControllerInterface {
	public void registerMyFeedDelegate(MyFeedControlDelegate delegate);
	public void unregisterMyFeedDelegate(MyFeedControlDelegate delegate);
}
