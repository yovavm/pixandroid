package com.pixplit.android.ui.fragments;

import java.net.URI;
import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.UsersListAdapter;
import com.pixplit.android.data.PixChatUser;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.data.PixUsers;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.UsersFeedHttpRequest;
import com.pixplit.android.ui.PixListFragment;
import com.pixplit.android.ui.elements.SearchBar;
import com.pixplit.android.ui.elements.SearchBar.SearchDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.UserViewHolder.UserActionsDelegate;


public class SearchUsersFragment extends PixListFragment<PixUsers> implements SearchDelegate, UserActionsDelegate{
	UsersFeedHttpRequest searchRequest = null;
	private int userActionType = UsersListAdapter.USERS_LIST_ACTION_NONE;
	
	public static SearchUsersFragment newInstance(Bundle args) {
		SearchUsersFragment fragment = new SearchUsersFragment();
        if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        	fragment.setArguments(args);
        }
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(false);
        
        setFragmentType(EFragmentType.SEARCH_USERS);
        
        Bundle args = getArguments();
        if (args != null) {
        	boolean followMode = args.getBoolean(PixDefs.FRAG_ARG_USERS_ACTION_TYPE_FOLLOW, false);
        	if (followMode) {
        		userActionType = UsersListAdapter.USERS_LIST_ACTION_FOLLOW;
        	} else {
        		boolean messageMode = args.getBoolean(PixDefs.FRAG_ARG_USERS_ACTION_TYPE_MSG, false);
        		if (messageMode) {
        			userActionType = UsersListAdapter.USERS_LIST_ACTION_MESSAGE;
        		}
        	}
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.search_users_fragment, container, false);
		
		SearchBar mSearchUsers = (SearchBar) 
				mFragmentView.findViewById(R.id.searchUsersTextBox);
		if (mSearchUsers != null) {
			mSearchUsers.setDelegate(this);
		}
		
		return mFragmentView;
	}
	
    @Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixUsers) {
			PixUsers users = (PixUsers) response;
			ArrayList<PixUser> usersFeed = users.getItems();
			changeAdapterData(usersFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return searchRequest;
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	UsersListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new UsersListAdapter(getActivity(), this, R.layout.user_layout, userActionType, this);
    	}
    	return adapter;
	}
    
    public void onSearchRequest(String searchText) {
    	if (!Utils.isValidString(searchText)) return;
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SEARCH_USERS, PixDefs.APP_EVENT_SEARCH_PRESSED, null);
    	
    	String uriEncodedSearchString = null;
    	URI uri;
		try {
			uri = new URI(
			    PixHttpAPI.PIX_SCHEME,
			    PixHttpAPI.PIX_SERVER_HOST, 
			    PixHttpAPI.SEARCH_USER_URL_PATH_PREFIX + searchText,
			    null);
			if (uri.getRawPath() != null) {
				uriEncodedSearchString = uri.getRawPath().substring(PixHttpAPI.SEARCH_USER_URL_PATH_PREFIX.length());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (uriEncodedSearchString == null) return;
			
		searchRequest = new UsersFeedHttpRequest(this, PixHttpAPI.SEARCH_USER_URL_PREFIX+uriEncodedSearchString);
		startSpinner();
		refreshList();
	}

	@Override
	public void onUserClick(PixUser pixUser) {
		if (pixUser != null &&
				userActionType == UsersListAdapter.USERS_LIST_ACTION_MESSAGE) {
			if (mActivitiesController != null) {
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_SEARCH_USERS, PixDefs.APP_EVENT_MSG_INITIATED_FROM_SEARCH, null);

				Bundle chatArgs = new Bundle();
				PixChatUser recipient = new PixChatUser(pixUser);
				recipient.serialize(chatArgs, String.format("%d", 0));
				mActivitiesController.gotoChat(getActivity(), chatArgs, 0);
			}
		}
		
	}
}
