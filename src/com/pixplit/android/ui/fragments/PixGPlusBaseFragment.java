package com.pixplit.android.ui.fragments;

import android.app.Activity;

import com.pixplit.android.google.GooglePlusActivityInterface;
import com.pixplit.android.ui.PixBaseFragment;

public class PixGPlusBaseFragment extends PixBaseFragment {
	
	protected GooglePlusActivityInterface mGPlusActivity;
	
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mGPlusActivity = (GooglePlusActivityInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement GooglePlusActivityInterface");
	    }
    }
    
    @Override
    public void onDetach () {
    	super.onDetach();
    	mGPlusActivity = null;
    }
}
