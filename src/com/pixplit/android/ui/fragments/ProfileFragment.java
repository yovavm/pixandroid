package com.pixplit.android.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.ProfileListAdapter;
import com.pixplit.android.data.PixChatUser;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixProfile;
import com.pixplit.android.data.PixProfileCompositions;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixAppState.UserInfo;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.AdminAddSuggestedUserHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.ProfileFeedHttpRequest;
import com.pixplit.android.ui.ActivitiesControllerInterface;
import com.pixplit.android.ui.ChangeCoverDialog;
import com.pixplit.android.ui.ChangeCoverDialog.ChangeCoverFragmentDelegate;
import com.pixplit.android.ui.PixFeedFragment;
import com.pixplit.android.ui.UsersListFragment;
import com.pixplit.android.ui.activities.ProfileInfoActivity;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.elements.PixUserPictureDialog;
import com.pixplit.android.ui.elements.ProfileFollowBtn;
import com.pixplit.android.ui.elements.ProfileFollowBtn.ProfileFollowBtnDelegae;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.viewholders.PixCompsPack;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class ProfileFragment extends PixFeedFragment implements ChangeCoverFragmentDelegate, 
																ProfileFollowBtnDelegae{
	//private static final String TAG = "ProfileFragment";
	
	SegmentInfo[] segments;
	String username = null;
	SegmentType currentSegement;
	
	ImageButton userCompletedBtn;
	ImageButton userNonCompletedBtn;
	ImageButton userLikedBtn;
	ImageView coverPic;
	
	ProfileFollowBtn followBtn;
	TextView followingNum;
	TextView followersNum;
	PixProfile profile;
	
	boolean setHeader;
	
	private static final int[] coverPics = {R.drawable.cover_profile1, R.drawable.cover_profile2, R.drawable.cover_profile3, 
											R.drawable.cover_profile4, R.drawable.cover_profile5, R.drawable.cover_profile6,
											R.drawable.cover_profile7, R.drawable.cover_profile8, R.drawable.cover_profile9,
											R.drawable.cover_profile10, R.drawable.cover_profile11, R.drawable.cover_profile12};
	private static final int frameWidth = PixAppState.getState().getFrameWidth();
	
	private enum SegmentType {
    	COMPLETED, NONCOMPLETED, LIKED
	}
	
	private class SegmentInfo {
		ArrayList<PixCompsPack> data;
		String segmentUrl;
		String nextPage;
		
		public SegmentInfo(String url){
			data = new ArrayList<PixCompsPack>();
			segmentUrl = url;
			nextPage = "1";
		}
	}
	
	public static ProfileFragment newInstance(Bundle args) {
		ProfileFragment fragment = new ProfileFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }
	
	@Override
    public void onResume() {
    	super.onResume();
		if (mActivitiesController != null &&
				mActivitiesController.needRefresh(ActivitiesControllerInterface.REFRESH_CONTEXT_PROFILE_EDITED)) {
			String loggedInUser = PixAppState.UserInfo.getUserName();
			if (Utils.isValidString(loggedInUser) &&
					Utils.isValidString(username) &&
					username.equalsIgnoreCase(loggedInUser)) {
				setHeader = true; // refresh the header as well
				refreshList();
			}
		}
    }
    
    private void init() {
		Bundle args = getArguments();
        if (args != null) {
        	username = args.getString(PixDefs.FRAG_ARG_USERNAME);
        }
        
		segments = new SegmentInfo[3];
		segments[SegmentType.COMPLETED.ordinal()] = new SegmentInfo(PixHttpAPI.USER_COMPLETED_FEED_URL + username);
		segments[SegmentType.NONCOMPLETED.ordinal()] = new SegmentInfo(PixHttpAPI.USER_NONCOMPLETED_FEED_URL + username);
		segments[SegmentType.LIKED.ordinal()] = new SegmentInfo(PixHttpAPI.USER_LIKED_FEED_URL + username);
		
		currentSegement = SegmentType.COMPLETED;
		
		setAllowPaging(true);
		
        setFragmentType(EFragmentType.PROFILE);
        
        setHeader = true;
        
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.profile_layout, container, false);
		return mFragmentView;
    }
	
	@Override
	public void onStart() {
		super.onStart();
		if (getActivity() != null) {
			followBtn = new ProfileFollowBtn(getActivity());
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.setMargins(0, 0, PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.profile_pic_right_margin), 0);
			
			if (mToolbarController != null) {
				mToolbarController.addToolbarCustomView(followBtn, params);
			}
		}
	}
	
	private void showTipIfNeeded() {
		if (getActivity() != null &&
				!PixAppState.didTipDisplayed(PixDefs.PROFILE_TIP_DISPLAYED) && 
				UserInfo.isUserLogedin()) {
			final Dialog tipDialog = new Dialog(getActivity(), R.style.PixAlertDialog);
			
			if (tipDialog != null) {
				tipDialog.setContentView(R.layout.profile_private_msg_tip_layout);
				View tip = tipDialog.findViewById(R.id.profilePrivateMsgTip);
				if (tip != null) {
					tip.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							tipDialog.dismiss();
						}
					});
				}
				View profileTipPlaceholder = tipDialog.findViewById(R.id.profileTipPlaceholder);
				if (profileTipPlaceholder != null && coverPic != null) {
					profileTipPlaceholder.getLayoutParams().height = coverPic.getLayoutParams().height;
				}
				// remove the title - not used
				View topPanel = tipDialog.findViewById(android.R.id.title);
				if (topPanel != null) {
					topPanel.setVisibility(View.GONE);
				}
				LayoutParams params = tipDialog.getWindow().getAttributes();
		        params.width = LayoutParams.MATCH_PARENT;
		        params.height = LayoutParams.MATCH_PARENT;
		        tipDialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
		        
				tipDialog.show();
			}
			PixAppState.setTipDisplayed(PixDefs.PROFILE_TIP_DISPLAYED, true);
		}
	}

	@Override 
	public void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	protected PixBaseListAdapter<PixCompsPack> createAdapter(){
		ProfileListAdapter adapter = null;
		if (getActivity() != null) {
			adapter = new ProfileListAdapter(getActivity(), this, 
					R.layout.comp_threepack_layout, 
					segments[currentSegement.ordinal()].data);
		}
		return adapter;
	}
	
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
		String url = segments[currentSegement.ordinal()].segmentUrl;
		BaseVolleyHttpRequest mRequest = new ProfileFeedHttpRequest(this, url);
		mRequest.getRequestContext().putInt(PixDefs.REQ_CTX_PROFILE_SEGMENT, currentSegement.ordinal());
		return mRequest;
	}
	
	@Override
	protected int getHeaderLayout(){
		return R.layout.profile_header;
	}
	
	@Override
	protected void handleListResponse(PixObject response, Bundle requestContext) {
		ArrayList<PixCompsPack> parsedData = null;
		
		if (response != null && response instanceof PixProfileCompositions) {
			PixProfileCompositions profileCompositions = (PixProfileCompositions)response;
			if (setHeader) {
				if (profileCompositions.getProfile() != null) {
					this.profile = profileCompositions.getProfile();
				}
				updateHeader(this.profile);
				setHeader = false;
			}
			
			if (requestContext != null) {
				int requestSegment = requestContext.getInt(PixDefs.REQ_CTX_PROFILE_SEGMENT);
				
				if (requestSegment == currentSegement.ordinal()) {
					parsedData = (ArrayList<PixCompsPack>) PixCompsPack.createCompsPackList(profileCompositions.getItems(), 3);
					updateAdapterData(parsedData);
				}
			}
			
		}
	}
	
	private void updateHeader(final PixProfile profile){
		if (mHeader != null && profile != null) {
			TextView userName = (TextView)mHeader.findViewById(R.id.profile_username);
			if (userName != null){
				userName.setText(profile.getFullName());
			}
			LinearLayout followingBtnLayout = (LinearLayout)mHeader.findViewById(R.id.profile_following_layout);
			LinearLayout followersBtnLayout = (LinearLayout)mHeader.findViewById(R.id.profile_followers_layout);
			LinearLayout splitsBtnLayout = (LinearLayout)mHeader.findViewById(R.id.profile_splits_layout);
			followingNum = null;
			followersNum = null;
			TextView splitsNum = null;
			
			
			if (followingBtnLayout != null) {
				followingBtnLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_FOLLOWING_COUNTER_PRESSED, null);
						if (Utils.isValidString(username)) {
							Bundle args = new Bundle();
				            args.putInt(PixDefs.FRAG_ARG_USERS_FRAG_TYPE, UsersListFragment.FRAG_TYPE_FOLLOWING);
				            args.putString(PixDefs.FRAG_ARG_USERS_CONTEXT, username);
				            if (mActivitiesController != null) {
				            	mActivitiesController.gotoUsersList(getActivity(), args);
				            }
						}
					}
				});
				followingNum = (TextView)mHeader.findViewById(R.id.profile_following_num);
				if (followingNum != null) {
					followingNum.setText(formatLargeNumber(profile.getFollowing()));
				}
			}

			if (followersBtnLayout != null) {
				followersBtnLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_FOLLOWERS_COUNTER_PRESSED, null);
						if (Utils.isValidString(username)) {
							Bundle args = new Bundle();
				            args.putInt(PixDefs.FRAG_ARG_USERS_FRAG_TYPE, UsersListFragment.FRAG_TYPE_FOLLOWERS);
				            args.putString(PixDefs.FRAG_ARG_USERS_CONTEXT, username);
				            if (mActivitiesController != null) {
				            	mActivitiesController.gotoUsersList(getActivity(), args);
				            }
						}
					}
				});
				followersNum = (TextView)mHeader.findViewById(R.id.profile_followers_num);
				if (followersNum != null) {
					followersNum.setText(formatLargeNumber(profile.getFollowers()));
				}
			}
			
			if (splitsBtnLayout != null) {
				splitsBtnLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						scrollToAfterHeader();
					}
				});
				splitsNum = (TextView)mHeader.findViewById(R.id.profile_splits_num);
				if (splitsNum != null) {
					splitsNum.setText(formatLargeNumber(profile.getSplits()));
				}
			}
			
			PixImageView profilePic = null;
			FrameLayout profilePicFrame = (FrameLayout)mHeader.findViewById(R.id.profile_picture_frame);
			if (profilePicFrame != null) {
				
				int profilePicFrameDimen = frameWidth / 4;
				ViewGroup.LayoutParams layoutParam = profilePicFrame.getLayoutParams();
				layoutParam.width = profilePicFrameDimen;
				layoutParam.height = profilePicFrameDimen;
				profilePicFrame.setLayoutParams(layoutParam);
				profilePic = (PixImageView)profilePicFrame.findViewById(R.id.profilePic);
				if (profilePic != null && Utils.isValidString(profile.getPicture())) {
					PixImagesLoader.loadImage(profile.getPicture(), profilePic, 0, 0, Priority.HIGH, false);
					
					if (!profile.getPicture().contains(PixDefs.PROFILE_DEFAULT_PHOTO)) {
						profilePic.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(final View v) {
								if (!profile.getPicture().contains(PixDefs.PROFILE_DEFAULT_PHOTO)) {
									PixImagesLoader.loadImage(profile.getPicture(), 
											new BitmapFetcherDelegate() {
												@Override
												public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapHolder) {
													if (bitmapHolder != null) {
														if (getActivity() != null) {
															Bitmap bitmap = bitmapHolder.getBitmap();
															if (bitmap != null &&
																	(bitmap.getWidth() > 150 || bitmap.getHeight() > 150)) {
																if (Utils.hasHoneycombMR1()) {
																	startDetailedProfileInfoActivity(v, profile);
																} else {
																	new PixUserPictureDialog(getActivity(), profile).show();
																}
															}
														}
														bitmapHolder.releaseBitmap("ProfileFragment:updateHeader");
													}
												}
												private void startDetailedProfileInfoActivity(
														View v, PixProfile profile) {
													int[] screenLocation = new int[2];
										            v.getLocationOnScreen(screenLocation);
										            Intent subActivity = new Intent(getActivity(),
										            		ProfileInfoActivity.class);
										            int orientation = getResources().getConfiguration().orientation;
										            subActivity.
										                    putExtra(PixApp.PACKAGE_NAME + ".orientation", orientation).
										                    putExtra(PixApp.PACKAGE_NAME + ".userPicture", profile.getPicture()).
										                    putExtra(PixApp.PACKAGE_NAME + ".left", screenLocation[0]).
										                    putExtra(PixApp.PACKAGE_NAME + ".top", screenLocation[1]).
										                    putExtra(PixApp.PACKAGE_NAME + ".width", v.getWidth()).
										                    putExtra(PixApp.PACKAGE_NAME + ".height", v.getHeight());
										            startActivity(subActivity);
										            
										            // Override transitions: we don't want the normal window animation in addition
										            // to our custom one
										            getActivity().overridePendingTransition(0, 0);
													
												}
												@Override
												public void onBitmapFetchFailed() {
													// Failed to fetch the split image. Do nothing - can't show profile pic.
												}
											}, 
											Priority.IMMEDIATE);
								}
							}
						});
					}
				}
			}
			
			ImageView profileInfoBarBtn = (ImageView) mHeader.findViewById(R.id.profile_info_bar_btn);
			initProfileInfoBtn(profileInfoBarBtn);
			
			coverPic = (ImageView)mHeader.findViewById(R.id.profile_hdr_bg_pic);
			if (coverPic != null) {
				coverPic.getLayoutParams().height = (int)(((float)frameWidth)/PixDefs.PROFILE_COVER_PHOTO_RATIO);
				coverPic.setImageResource(ProfileFragment.getCoverPicture(profile.getCoverId()));
				if (profile.getIsFollowing() == PixUser.FOLLOW_STATUS_LOGGED_IN_USER) {
					coverPic.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							chooseCoverPhoto();
						}
					});
				}
			}

			// note that the follow button is located in the toolbar
			if (followBtn != null) {
				followBtn.init(this, R.drawable.profile_following_btn, R.drawable.profile_follow_btn);
				followBtn.bindUser(new PixUser(profile));
			}
			
			userCompletedBtn = (ImageButton)mHeader.findViewById(R.id.profile_completed);
			userNonCompletedBtn = (ImageButton)mHeader.findViewById(R.id.profile_noncompleted);
			userLikedBtn = (ImageButton)mHeader.findViewById(R.id.profile_liked);
			
			if (userCompletedBtn != null){
				userCompletedBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_COMPLETED_TAB_PRESSED, null);
						updateSegment(SegmentType.COMPLETED);
					}
				});
			}
			
			if (userNonCompletedBtn != null){
				userNonCompletedBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_NONCOMPLETED_TAB_PRESSED, null);
						updateSegment(SegmentType.NONCOMPLETED);
					}
				});
			}
			
			if (userLikedBtn != null){
				userLikedBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_LIKED_TAB_PRESSED, null);
						updateSegment(SegmentType.LIKED);
					}
				});
			}
			updateUserSplisBtns();
		}
		
		if (profile.getIsFollowing() != PixUser.FOLLOW_STATUS_LOGGED_IN_USER) {
			this.showTipIfNeeded();
		}
	}
	
	private void initProfileInfoBtn(ImageView profileInfoBarBtn) {
		if (profileInfoBarBtn == null) return;
		
		if (PixApp.inAdminBuild()) {
			if (UserInfo.hasAdminToken()) {
				profileInfoBarBtn.setBackgroundResource(R.drawable.profile_suggested_btn_selector);
				profileInfoBarBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!UserInfo.hasAdminToken()) return;
						PixProgressIndicator.showMessage(getActivity(), 
								PixApp.getContext().getString(R.string.admin_adding_user_to_suggested), true);
						new AdminAddSuggestedUserHttpRequest(new RequestDelegate() {
							@Override
							public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
								PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
							}
							
							@Override
							public void requestFinished(BaseHttpRequest request, PixObject response) {
								PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getString(R.string.done));
							}
						}, username).send();
					}
				});
				return;
			}
		}
		if (profile.getIsFollowing() != PixUser.FOLLOW_STATUS_LOGGED_IN_USER) {
			profileInfoBarBtn.setBackgroundResource(R.drawable.profile_message_btn_selector);
			profileInfoBarBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!ProfileFragment.this.verifyUser()) return;
					Bundle chatArgs = new Bundle();
					PixChatUser recipient = new PixChatUser(profile);
					recipient.serialize(chatArgs, String.format("%d", 0));
		        	if (mActivitiesController != null) {
		        		PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_MSG_INITIATED_FROM_PROFILE, null);
		        		mActivitiesController.gotoChat(getActivity(), chatArgs, 0);
					}
				}
			});
		} else {
			profileInfoBarBtn.setBackgroundResource(R.drawable.profile_settings_btn_selector);
			profileInfoBarBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
		        	if (mActivitiesController != null) {
						Bundle args = new Bundle();
						args.putInt(PixDefs.FRAG_ARG_ANIMATION_TYPE, 
								ActivitiesControllerInterface.PIX_ANIMATE_SLIDE_IN_BOTTOM_UP);
		        		mActivitiesController.gotoEditProfile(getActivity(), args);
					}
				}
			});
		}
	}

	private void updateUserSplisBtns() {
		if (userCompletedBtn != null && userNonCompletedBtn != null && userLikedBtn != null) {
			switch(currentSegement) {
			case COMPLETED:
				userCompletedBtn.setBackgroundResource(R.drawable.profile_completed_btn_selected);
				userNonCompletedBtn.setBackgroundResource(R.drawable.profile_open_btn);
				userLikedBtn.setBackgroundResource(R.drawable.profile_liked_btn);
				break;
			case LIKED:
				userCompletedBtn.setBackgroundResource(R.drawable.profile_completed_btn);
				userNonCompletedBtn.setBackgroundResource(R.drawable.profile_open_btn);
				userLikedBtn.setBackgroundResource(R.drawable.profile_liked_btn_selected);
				break;
			case NONCOMPLETED:
				userCompletedBtn.setBackgroundResource(R.drawable.profile_completed_btn);
				userNonCompletedBtn.setBackgroundResource(R.drawable.profile_open_btn_selected);
				userLikedBtn.setBackgroundResource(R.drawable.profile_liked_btn);
				break;
			default:
				break;
			}
		}
	}
	
	private void updateSegment(SegmentType newSegment){
		if (newSegment.ordinal() != currentSegement.ordinal()){
			// store current segment data
			segments[currentSegement.ordinal()].data = (ArrayList<PixCompsPack>)mAdapter.getData();
			segments[currentSegement.ordinal()].nextPage = getNextPage();
			
			// move to new segment
			currentSegement = newSegment;
			updateUserSplisBtns();
			setNextPage(segments[currentSegement.ordinal()].nextPage);
			changeAdapterData(segments[currentSegement.ordinal()].data);
			if (segments[currentSegement.ordinal()].data != null &&
					segments[currentSegement.ordinal()].data.size() == 0) {
				refreshList();
			}
		}
	}
	
	private static int getCoverPicture(int coverIndex) {
		if (coverIndex > 0 && coverIndex <= coverPics.length) {
			return coverPics[coverIndex-1];
		}
		return coverPics[0]; // default
	}
	
	@Override
	public void onCompImageClick(PixComposition clickedComp) {
		if (clickedComp == null || !Utils.isValidString(clickedComp.getCompId())) return;
    	Bundle args = new Bundle();
		args.putString(PixDefs.FRAG_ARG_COMP_ID, clickedComp.getCompId());
		if (mActivitiesController != null) {
			ArrayList<PixCompsPack> data = segments[currentSegement.ordinal()].data;
			ArrayList<PixComposition> comps = new ArrayList<PixComposition>();
			int clickedCompIndex = 0;
			// flatten the compositions array and find the clicked comp index
			for (int i=0; i<data.size(); i++) {
				PixCompsPack compsPack = data.get(i);
				if (compsPack != null) {
					int size = compsPack.getPackSize();
					for (int j=0; j<size; j++) {
						PixComposition comp = compsPack.getComp(j);
						if (comp != null) {
							comps.add(comp);
							if (clickedCompIndex == 0 &&
									clickedComp.equals(comp)) {
								clickedCompIndex = comps.size()-1;
							}
						}
					}
				}
			}
			args.putInt(PixDefs.FRAG_ARG_GALLERY_COMP_INDEX, clickedCompIndex);
			args.putInt(PixDefs.FRAG_ARG_GALLERY_FEED_TYPE, currentSegement.ordinal());
			args.putString(PixDefs.FRAG_ARG_GALLERY_TITLE, profile.getFullName());
			args.putString(PixDefs.FRAG_ARG_GALLERY_FEED_URL, segments[currentSegement.ordinal()].segmentUrl);
			args.putString(PixDefs.FRAG_ARG_GALLERY_NEXT_PAGE, getNextPage());
			args.putBoolean(PixDefs.FRAG_ARG_GALLERY_ALLOW_PAGING, true);
			args.putBoolean(PixDefs.FRAG_ARG_GALLERY_INIT_WITH_COMPS, true);
			args.putString(PixDefs.FRAG_ARG_GALLERY_PROFILE_USERNAME, username);
			mActivitiesController.gotoProfileGalleryFeed(getActivity(), args, 0, comps);
		}
	}

	private void chooseCoverPhoto() {
		if (getActivity() != null) {
			new ChangeCoverDialog(getActivity(), this).show();
		}
	}
	
	@Override
	public void onCoverPhotoChoosen(int coverIndex) {
		if (coverIndex > 0 && coverIndex <= 12) {
			int coverPicRes = ProfileFragment.getCoverPicture(coverIndex);
			if (coverPicRes > 0 && coverPic != null) {
				HashMap<String, String> context = new HashMap<String, String>();
	    		context.put(PixDefs.APP_PARAM_COVER_PHOTO_ID, String.format("%d", coverIndex));
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_CHANGED_COVER_PHOTO, context);
				
				coverPic.setImageResource(coverPicRes);
			}
		}
		
	}
	
	@Override
	public void onUserFollowClick(PixUser user) {
		if (!this.verifyUser()) return;
		super.onUserFollowClick(user);
		
		// update profile followers number
		if (user != null && followersNum != null && this.profile != null) {
			int currentFollowersNum = this.profile.getFollowers();
			if (user.getFollowStatus() == PixUser.FOLLOW_STATUS_FOLLOWING) {
				// started following the user
				PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_FOLLOW_BUTTON_PRESSED, null);
				followersNum.setText(formatLargeNumber(currentFollowersNum+1));
			} else {
				// un-follow the user
				if (currentFollowersNum > 0) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_PROFILE, PixDefs.APP_EVENT_FOLLOWING_BUTTON_PRESSED, null);
					followersNum.setText(formatLargeNumber(currentFollowersNum-1));
				}
			}
		}
	}
	
	private String formatLargeNumber(int num) {
		if (num < 0) {
			return "0";
		}
		if (num < 1000) {
			return (String.format(Locale.getDefault(), "%d", num));
		} 
		int thousand = num/1000;
		int left = num%1000;
		int unit = left/100;
		
		if (num < 10000) {
			return String.format(Locale.getDefault(), "%d,%03d", thousand, left);
		}
		return String.format(Locale.getDefault(), "%d.%dk", thousand, unit);
	}
}
