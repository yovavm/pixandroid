package com.pixplit.android.ui.fragments;

import android.app.Activity;

import com.pixplit.android.fb.FbActivityInterface;

public class PixFbBaseFragment extends PixGPlusBaseFragment {
	protected FbActivityInterface mFbActivity;
	
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mFbActivity = (FbActivityInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement FbActivityInterface");
	    }
    }
    
    @Override
    public void onDetach () {
    	super.onDetach();
    	mFbActivity = null;
    }
    
    public boolean verifyUser() {
    	if (mFbActivity != null) {
    		return mFbActivity.verifyUser();
    	}
    	return false;
    }
}
