package com.pixplit.android.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.FollowFriendsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixFollowUser;
import com.pixplit.android.data.PixFriendsFollowStatus;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.FollowUsersHttpRequest;
import com.pixplit.android.http.request.FriendsFollowStatusHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;


public class FollowFriendsFragment extends PixListFragment<PixFollowUser>{

	private int notifyFBFriends = 0;
	private String fbFriends = null;
	private ArrayList<PixUser> fbFriendsArray;
	private boolean inLoginProcess = false;

	public static FollowFriendsFragment newInstance(Bundle args) {
		FollowFriendsFragment fragment = new FollowFriendsFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);        
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setAllowPaging(false);
        
        Bundle args = getArguments();
        if (args != null) {
        	this.fbFriends = args.getString(PixDefs.FRAG_ARG_FB_FRIENDS_LIST);
        	this.inLoginProcess = args.getBoolean(PixDefs.FRAG_ARG_IN_LOGIN_PROCESS);
        }
        this.setFragmentType(EFragmentType.FOLLOW_FRIENDS);
    }
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.follow_friends_layout, container, false);
		if (mFragmentView != null) {
			TextView clearBtn = (TextView) mFragmentView.findViewById(R.id.clearBtn);
			if (clearBtn != null) {
				clearBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setAllUsersSelection(false);
						mAdapter.notifyDataSetChanged();
					}
				});
			}
			
			TextView selectAllBtn = (TextView) mFragmentView.findViewById(R.id.selectAllBtn);
			if (selectAllBtn != null) {
				selectAllBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setAllUsersSelection(true);
						mAdapter.notifyDataSetChanged();
					}
				});
			}
		}
		return mFragmentView;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		if (mToolbarController != null) {
			mToolbarController.addToolbarTextBtn(
					R.drawable.done_btn_selector, 
					PixDefs.TOOLBAR_BTN_ID_DONE,
	    			new OnClickListener() {
						@Override
						public void onClick(View v) {
							doneFollowing();
						}
					}, 
					PixApp.getContext().getResources().getString(R.string.done));
		}
		this.fetchFollowFrientsList();
	}
	private void setAllUsersSelection(boolean selected) {
		ArrayList<?> followUsersList = mAdapter.getData();
		if (followUsersList != null) {
			for (Object user: followUsersList) {
				if (user != null && user instanceof PixFollowUser) {
					((PixFollowUser)user).setSelected(selected);
				}
			}
		}
	}
	
	private void fetchFollowFrientsList() {
		startSpinner();
		if (getActivity() != null) {
			PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getString(R.string.loading), true);
		}
		PixAppState.getState().getRequestQueue().add(
				new FriendsFollowStatusHttpRequest(new RequestDelegate() {
					@Override
					public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
						hideSpinner();
						PixProgressIndicator.hideMessage();
						if (inLoginProcess) {
							if (mActivitiesController != null) {
								mActivitiesController.gotoMyFeed(getActivity(), 0);
							}
						} else {
							if (getActivity() != null) {
								getActivity().finish();
							}	
						}
					}
					@Override
					public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
						if (response != null && response instanceof PixFriendsFollowStatus) {
							PixFriendsFollowStatus friendsFollowStatus  = (PixFriendsFollowStatus)response;
							notifyFBFriends = friendsFollowStatus.getNotifyFBFriends();
							fbFriendsArray = friendsFollowStatus.getFriends();
							ArrayList<PixFollowUser> usersList = processFriendsFollowStatusReponse(friendsFollowStatus);
							
							if (usersList != null) {
								if (inLoginProcess && usersList.size() == 0) {
									// if in login process finish this view in case there are no users to follow
									if (getActivity() != null) {
										if (mActivitiesController != null) {
											mActivitiesController.gotoMyFeed(getActivity(), 0);
										}
									}
								}
								updateAdapterData(usersList);
							} else {
								// should not happen, but if so close this activity
								if (inLoginProcess) {
									if (mActivitiesController != null) {
										mActivitiesController.gotoMyFeed(getActivity(), 0);
									}
								} else {
									if (getActivity() != null) {
										getActivity().finish();
									}	
								}
							}
							PixProgressIndicator.hideMessage();
							hideSpinner();
						}
					}
				}, fbFriends));
	}
	
    @Override
    public boolean onToolbarHomePressIntercepted() {
    	if (!inLoginProcess) {
    		// go back to the calling activity
    		if (mActivity != null) {
    			mActivity.finishFragment();
    			return true;
    		}
    	}
    	return false;
    }
    
	private void doneFollowing() {
		String followUsersString = null;
		String notifyUsersString = null;
		ArrayList<String> followUsers = ((FollowFriendsListAdapter)mAdapter).getSelectedUsersList();
		followUsersString = TextUtils.join(",", followUsers);
		
		ArrayList<String> notifyUsers = null;
		if (this.notifyFBFriends == 1) {
			notifyUsers = new ArrayList<String>();
			for (PixUser user : this.fbFriendsArray) {
				if (user != null && Utils.isValidString(user.getUsername())) {
					if (!followUsers.contains(user.getUsername())) {
						notifyUsers.add(user.getUsername());
					}
				}
			}
			if (notifyUsers.size() > 0) {
				notifyUsersString = TextUtils.join(",", notifyUsers);
			}
		}
		
		if (Utils.isValidString(followUsersString) ||
				Utils.isValidString(notifyUsersString)) {
			
			HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARMA_USERS_COUNT, String.format("%d", followUsers.size()));
        	PixAppState.logAppEvent(PixDefs.APP_SCREEN_FIND_FRIENDS, PixDefs.APP_EVENT_FB_FRIENDS_FOLLOWED, context);
        	
			new FollowUsersHttpRequest(followUsersString, notifyUsersString).send();
		}
		
		// done. continue to the next activity depending on the calling context
		if (inLoginProcess) {
			if (mActivitiesController != null) {
				mActivitiesController.gotoMyFeed(getActivity(), 0);
			}
		} else {
			if (getActivity() != null) {
				getActivity().finish();
			}	
		}
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		// this list sends requests and handle their responses in a custom manner on start-up only
	}
	
    @Override
	public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
    	// this list sends requests and handle their responses in a custom manner on start-up only
    }
	
	private ArrayList<PixFollowUser> processFriendsFollowStatusReponse(PixFriendsFollowStatus friendsFollowStatus) {
		ArrayList<PixFollowUser> followUsersList = new ArrayList<PixFollowUser>();
		
		if (friendsFollowStatus.getFriends() != null) {
			for (PixUser user : friendsFollowStatus.getFriends()) {
				PixFollowUser followUser = new PixFollowUser(user, false);
				followUser.setSelected(true);
				followUsersList.add(followUser);
			}
		}
		if (friendsFollowStatus.getSuggested() != null) {
			for (PixUser user : friendsFollowStatus.getSuggested()) {
				PixFollowUser followUser = new PixFollowUser(user, true);
				this.removeIfExists(followUsersList, user.getUsername());
				followUser.setSelected(true);
				followUsersList.add(followUser);
			}
		}
		
		return followUsersList;
	}
	
 
	private void removeIfExists(ArrayList<PixFollowUser> followUsersList, String username) {
		// take care of users who appear on both users lists (remove from friends and keep in featured).
		for (PixFollowUser user : followUsersList) {
			if (user != null &&
					user.getUsername() != null &&
					username != null &&
					user.getUsername().equalsIgnoreCase(username)) {
				followUsersList.remove(user);
				break;
			}
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return null; // the request is sent by the fragment
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	FollowFriendsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new FollowFriendsListAdapter(getActivity(), this);
    	}
    	return adapter;
	}
    
    
}
