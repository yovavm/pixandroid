package com.pixplit.android.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.elements.PixDialog;

public class ConnectWithEmailDialog extends PixDialog {

	ConnectWithEmailDialogDelegate mDelegate;
	
	public interface ConnectWithEmailDialogDelegate {
		public void onSignupRequest();
		public void onSigninRequest();
	}
	
	public ConnectWithEmailDialog(Context context, ConnectWithEmailDialogDelegate delegate) {
		super(context, R.layout.connect_to_pixplit_dialog);
		this.mDelegate = delegate;
		
		// sign up button
		TextView signupBtn = (TextView)findViewById(R.id.signUpBtn);
		if (signupBtn != null) {
			signupBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_SIGN_UP_PRESSED, null);
					if (mDelegate != null) {
						mDelegate.onSignupRequest();
					}
					dismiss();
				}
			});
		}
		// sign in button
		TextView signupIn = (TextView)findViewById(R.id.loginBtn);
		if (signupIn != null) {
			signupIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_CONNECT_MENU, PixDefs.APP_EVENT_LOGIN_PRESSED, null);
					if (mDelegate != null) {
						mDelegate.onSigninRequest();
					}
					dismiss();
				}
			});
			}
	}

}
