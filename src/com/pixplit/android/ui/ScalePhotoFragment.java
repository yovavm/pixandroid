package com.pixplit.android.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.capture.SplitBorderType;
import com.pixplit.android.capture.SplitPreviewFrame;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionComponent;
import com.pixplit.android.globals.PixCompositionType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public class ScalePhotoFragment extends PixBaseFragment{
	private static final String TAG = "EditNewSplitFragment";
	private PixCompositionType compType;
	PixCompositionMetadata mCompMetadata;
	
	SplitPreviewFrame mPreviewFrame;
	private int previewDimen;
	private static final int mPreviewFrameId = 778;
	
	private static PixBitmapHolder joinedCompImage;
	private static PixBitmapHolder activePartOrigBitmap;
	private static PixBitmapHolder activePartProcessedBitmap;
    public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
    	if (joinedImage != null) {
    		joinedImage.useBitmap(TAG);
    	}
    	if (joinedCompImage != null) {
    		joinedCompImage.releaseBitmap(TAG);
    	}
    	joinedCompImage = joinedImage;
    }
    public static void setActivePartOrigBitmap(PixBitmapHolder partBitmap) {
    	if (partBitmap != null) {
    		partBitmap.useBitmap("");
    	}
    	if (activePartOrigBitmap != null) {
    		activePartOrigBitmap.releaseBitmap("");
    	}
    	activePartOrigBitmap = partBitmap;
    	setActivePartProcessedBitmap(activePartOrigBitmap);
    }
    private static void setActivePartProcessedBitmap(PixBitmapHolder processedBitmap) {
    	if (processedBitmap != null) {
    		processedBitmap.useBitmap(TAG);
    	}
    	if (activePartProcessedBitmap != null) {
    		activePartProcessedBitmap.releaseBitmap(TAG);
    	}
    	activePartProcessedBitmap = processedBitmap;
    }
    
	public static ScalePhotoFragment newInstance(Bundle args) {
		ScalePhotoFragment fragment = new ScalePhotoFragment();
		if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
            fragment.setArguments(args);
        }
        return fragment;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.SCALE_IMPORTED_PHOTO_IN_SPLIT);
        
        Bundle args = getArguments();
        if (args != null) {
        	this.mCompMetadata = PixCompositionMetadata.deserialize(args);
        	this.compType = PixCompositionType.deserialize(args);
        	this.previewDimen = args.getInt(PixDefs.FRAG_ARG_SPLIT_PREVIEW_DIMEN, 
        			PixAppState.getState().getSplitPreivewWidth());
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.scale_photo_layout, container, false);
    	
    	if (mFragmentView != null) {
	    	// approve split
	        ImageView approveSplit = (ImageView) mFragmentView.findViewById(R.id.approveSplit);
	        if (approveSplit != null) {
	        	approveSplit.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onPhotoScalingApproved();
					}
				});
	        }
	        
	     // mirror vertical image
        ImageView mirrorVerticalBtn = (ImageView) mFragmentView.findViewById(R.id.flipVerticalBtn);
        if (mirrorVerticalBtn != null) {
        	mirrorVerticalBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					flipImage(false);
				}
			});
        }
     
        // mirror image
        ImageView mirrorBtn = (ImageView) mFragmentView.findViewById(R.id.flipBtn);
        if (mirrorBtn != null) {
        	mirrorBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					flipImage(true);
				}
			});
        }

    	}
        return mFragmentView;
    }
    
    @Override
	public void onResume() {
    	super.onResume();
    	if (mToolbarController != null) {
    		mToolbarController.setToolbarVisible(false);
    	}
    	if (mCompMetadata != null &&
    			mFragmentView != null &&
    			mFragmentView.findViewById(mPreviewFrameId) == null) {
    		SplitBorderType borderType = mCompMetadata.hasBorder() ? SplitBorderType.BORDER_TYPE_WIDE : SplitBorderType.BORDER_TYPE_NONE;
	    	mPreviewFrame = new SplitPreviewFrame(mFragmentView.getContext(), 
							   this.previewDimen, 
							   this.previewDimen, 
							   this.compType,
							   borderType,
							   (joinedCompImage != null) ? joinedCompImage.getBitmap() : null);
	    	mPreviewFrame.setId(mPreviewFrameId);
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.previewDimen, 
																				 this.previewDimen);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
			((ViewGroup)mFragmentView).addView(mPreviewFrame, params);
    	}
    	// validate that the active part doesn't have a participant already
		if (compType != null) {
			PixCompositionComponent activeComponent = compType.getComponentWithIndex(mCompMetadata.getNewComponentIndex());
			if (activeComponent != null && 
					activeComponent.getParticipant() == null &&
					activePartOrigBitmap != null &&
					activePartOrigBitmap.getBitmap() != null) {
				mPreviewFrame.setPartBitmap(mCompMetadata.getNewComponentIndex(), 
						activePartProcessedBitmap.getBitmap(), SplitPreviewFrame.PART_SCALE_TYPE_SCALE);
			}
		}
    	if (this.mPreviewFrame != null) {
    		this.mPreviewFrame.show();
    	}
    }
        
    public void flipImage(boolean horizontal) {
    	if (activePartProcessedBitmap != null) {
	    	Bitmap flipedBitmap = Utils.flipBitmap(activePartProcessedBitmap.getBitmap(), horizontal);
	    	if (flipedBitmap != null) {
	    		setActivePartProcessedBitmap(new PixBitmapHolder(flipedBitmap));
	    		if (mPreviewFrame != null && mCompMetadata != null) {
	    			mPreviewFrame.setPartBitmap(mCompMetadata.getNewComponentIndex(), 
	    					flipedBitmap, SplitPreviewFrame.PART_SCALE_TYPE_SCALE);
	    		}
	    	}
    	}
    }

    public void onPhotoScalingApproved() {
    	final Bitmap updatedActivePartBitmap = mPreviewFrame.getActivePartBitmap();
    	if (updatedActivePartBitmap != null && mCompMetadata != null) {
    		Bundle args = new Bundle();
        	if (compType != null) {
        		compType.serialize(args);
        	}
        	if (mCompMetadata != null) {
        		mCompMetadata.serialize(args);
        	}
			args.putInt(PixDefs.FRAG_ARG_SPLIT_PREVIEW_DIMEN, previewDimen);
			
			if (mActivitiesController != null) {
				PixBitmapHolder updatedActivePartHolder = new PixBitmapHolder(updatedActivePartBitmap);
				mActivitiesController.gotoProcessNewSplit(getActivity(), args, joinedCompImage, updatedActivePartHolder);
			}
		}
    }
}
