package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.adapters.ChallengesListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixChallenge;
import com.pixplit.android.data.PixChallenges;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.ChallengesFeedHttpRequest;
import com.pixplit.android.ui.elements.SearchBar;
import com.pixplit.android.ui.elements.SearchBar.SearchDelegate;


public class ChallengesFragment extends PixListFragment<PixChallenges> implements SearchDelegate{

	public static ChallengesFragment newInstance() {
		ChallengesFragment fragment = new ChallengesFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setAllowPaging(true);
        
        setFragmentType(EFragmentType.CHALLENGES);
        
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.challenges_feed_layout, container, false);
		
		SearchBar searchTagBar = (SearchBar) mFragmentView.findViewById(R.id.searchTagBar);
		if (searchTagBar != null) {
			searchTagBar.setDelegate(this);
		}
		
		return mFragmentView;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixChallenges) {
			PixChallenges challengesResponse = (PixChallenges)response;
			ArrayList<PixChallenge> challengesFeed = challengesResponse.getItems();
			updateAdapterData(challengesFeed);
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new ChallengesFeedHttpRequest(this);
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	ChallengesListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new ChallengesListAdapter(getActivity(), this, R.layout.challenge_layout);
    	}
    	return adapter;
	}

	@Override
	public void onSearchRequest(String searchText) {
		Bundle args = new Bundle();
        args.putString(PixDefs.FRAG_ARG_TAG, searchText);
    	if (mActivitiesController != null) {
    		mActivitiesController.gotoThemeFeed(getActivity(), args);
    	}
	}
}
