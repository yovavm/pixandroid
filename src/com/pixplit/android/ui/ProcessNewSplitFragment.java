package com.pixplit.android.ui;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.capture.SplitBorderType;
import com.pixplit.android.capture.SplitPreviewFrame;
import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.data.PixChatMessageContainer;
import com.pixplit.android.data.PixCompositionMetadata;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixPrivateCompositionMetadata;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionComponent;
import com.pixplit.android.globals.PixCompositionType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.ChatAddMessageHttpRequest;
import com.pixplit.android.ui.elements.PixAdjustFilterButton;
import com.pixplit.android.ui.elements.PixAdjustFilterButton.PixAdjustFilterButtonDelegate;
import com.pixplit.android.ui.elements.PixFilterButton;
import com.pixplit.android.ui.elements.PixFilterButton.PixFilterButtonDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.ui.elements.ScaleBar;
import com.pixplit.android.ui.elements.ScaleBar.ScaleBarDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.util.bitmap.filters.PixFilterPreview;

public class ProcessNewSplitFragment extends PixBaseFragment implements ScaleBarDelegate, PixFilterButtonDelegate, PixAdjustFilterButtonDelegate {
	private static final String TAG = "EditNewSplitFragment";
	
	private PixCompositionType compType;
	PixCompositionMetadata compMetadata;

	SplitPreviewFrame mSplitPreview;
	private int previewDimen;
	
	PixFilterPreview filterPreview;
	LinearLayout mFilterParamsBar;
	ScaleBar mScaleBar;
	
	ImageView selectedFilter = null;
	PixAdjustFilterButton selectedAdjustFilter = null;
	ImageView approveSplit = null;
	
	private static final String PROCESS_FILTER_APPLIED_KEY = "PROCESS_FILTER_APPLIED_KEY";
	private int lastFilterApplied = PixFilterPreview.EFFECT_NONE;
	
	private static PixBitmapHolder joinedCompImage;
	private static PixBitmapHolder activePartOrigBitmap;
	private static PixBitmapHolder activePartProcessedBitmap;
    public static void setJoinedCompImage(PixBitmapHolder joinedImage) {
    	if (joinedImage != null) {
    		joinedImage.useBitmap(TAG);
    	}
    	if (joinedCompImage != null) {
    		joinedCompImage.releaseBitmap(TAG);
    	}
    	joinedCompImage = joinedImage;
    }
    public static void setActivePartOrigBitmap(PixBitmapHolder partBitmap) {
    	if (partBitmap != null) {
    		partBitmap.useBitmap("");
    	}
    	if (activePartOrigBitmap != null) {
    		activePartOrigBitmap.releaseBitmap("");
    	}
    	activePartOrigBitmap = partBitmap;
    	setActivePartProcessedBitmap(activePartOrigBitmap);
    }
    private static void setActivePartProcessedBitmap(PixBitmapHolder processedBitmap) {
    	if (processedBitmap != null) {
    		processedBitmap.useBitmap(TAG);
    	}
    	if (activePartProcessedBitmap != null) {
    		activePartProcessedBitmap.releaseBitmap(TAG);
    	}
    	activePartProcessedBitmap = processedBitmap;
    }
    
	public static ProcessNewSplitFragment newInstance(Bundle args) {
		ProcessNewSplitFragment fragment = new ProcessNewSplitFragment();
		if (args != null) {
        	args.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
            fragment.setArguments(args);
        }
        return fragment;
    }

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.PROCESS_SPLIT);
        
        Bundle initBundle;
        if (savedInstanceState != null) {
        	initBundle = savedInstanceState;
        	lastFilterApplied = savedInstanceState.getInt(PROCESS_FILTER_APPLIED_KEY, PixFilterPreview.EFFECT_NONE);
        } else {
        	initBundle = getArguments();
        }
        
        if (initBundle != null) {
        	this.compMetadata = PixCompositionMetadata.deserialize(initBundle);
        	this.compType = PixCompositionType.deserialize(initBundle);
        	this.previewDimen = initBundle.getInt(PixDefs.FRAG_ARG_SPLIT_PREVIEW_DIMEN, 
        										  PixAppState.getState().getSplitPreivewWidth());
        }
       
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.process_new_split_layout, container, false);
    	
    	if (compMetadata != null && this.compType != null && getActivity() != null) {
    		SplitBorderType borderType = compMetadata.hasBorder() ? SplitBorderType.BORDER_TYPE_WIDE : SplitBorderType.BORDER_TYPE_NONE;
	    	mSplitPreview = new SplitPreviewFrame(getActivity(), 
								   this.previewDimen, 
								   this.previewDimen, 
								   this.compType,
								   borderType,
								   (joinedCompImage != null) ? joinedCompImage.getBitmap() : null);

			// init the preview
	    	PixCompositionComponent activeComponent = compType.getComponentWithIndex(compMetadata.getNewComponentIndex());
			if (activeComponent != null && activeComponent.getParticipant() == null) {
				if (activePartOrigBitmap != null &&
						activePartOrigBitmap.getBitmap() != null) {
					mSplitPreview.setPartBitmap(compMetadata.getNewComponentIndex(), 
							activePartOrigBitmap.getBitmap(), SplitPreviewFrame.PART_SCALE_TYPE_DEFAULT);
				}
			}
			mSplitPreview.hide();
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.previewDimen, 
																				 this.previewDimen);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
			((ViewGroup)mFragmentView).addView(mSplitPreview, 0, params);
			if (PixDefs.hasImageFilters) {
				// hide the place holder and display the filters bar
				View buttom_bar_placeholder = mFragmentView.findViewById(R.id.buttom_bar_placeholder);
				if (buttom_bar_placeholder != null) {
					buttom_bar_placeholder.setVisibility(View.GONE);
				}
				View filtersContainer = mFragmentView.findViewById(R.id.filtersContainer);
				if (filtersContainer != null) {
					filtersContainer.setVisibility(View.VISIBLE);
				}
				// effects view should be on top of the active split part
				Rect activePartRect = mSplitPreview.getActivePartDisplayRelativeRect();
				if (activePartRect != null && 
						activePartOrigBitmap != null &&
						activePartOrigBitmap.getBitmap() != null) {
					this.filterPreview = new PixFilterPreview(getActivity(), 
							activePartOrigBitmap,
							activePartRect.width(), 
							activePartRect.height());
					params = new RelativeLayout.LayoutParams(activePartRect.width(), 
							activePartRect.height());
					params.setMargins(activePartRect.left, activePartRect.top, 0, 0);
					((ViewGroup)mSplitPreview).addView(this.filterPreview, params);
				}
				
				mScaleBar = new ScaleBar(getActivity());
				params = new LayoutParams(this.previewDimen, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.BELOW, R.id.createSplitTopBar);
				params.addRule(RelativeLayout.CENTER_HORIZONTAL);
				params.setMargins(30, 20, 30, 20);
				mScaleBar.setDelegate(this);
				((ViewGroup)mFragmentView).addView(mScaleBar, 1, params);
				mScaleBar.setVisibility(View.INVISIBLE);
			}
    	}
    	
    	// approve split
        approveSplit = (ImageView) mFragmentView.findViewById(R.id.approveSplit);
        if (approveSplit != null) {
        	approveSplit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onSplitProcessingApproved();
					if (approveSplit != null) {
						approveSplit.setEnabled(false);
					}
				}
			});
        }
        
        // add border to split
        ImageView addBorder = (ImageView) mFragmentView.findViewById(R.id.addBorder);
        if (addBorder != null) {
    		addBorder.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
			    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_CUSTOMIZATION, PixDefs.APP_EVENT_BORDER_APPLIED, null);
					if (mSplitPreview != null && compMetadata != null) {
						compMetadata.setHasBorder(!compMetadata.hasBorder());
						updateBorder();
					}
				}
			});
        }
        
        mFilterParamsBar = (LinearLayout)mFragmentView.findViewById(R.id.filterParamsBar);
        // initiate the filters
        if (PixDefs.hasImageFilters) {
	        // clear filter from split
	        PixFilterButton clearFilterBtn = (PixFilterButton) mFragmentView.findViewById(R.id.clearFilter);
	        if (clearFilterBtn != null) {
	        	clearFilterBtn.setType(PixFilterPreview.EFFECT_NONE);
	        	clearFilterBtn.setDelegate(this);
	        }
	        // apply filter to split
	        PixFilterButton addFilter1 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter1);
	        if (addFilter1 != null) {
	        	addFilter1.setType(PixFilterPreview.EFFECT_AUTO_FIX);
	        	addFilter1.setDelegate(this);
	    	}
	        PixFilterButton addFilter2 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter2);
	        if (addFilter2 != null) {
	        	addFilter2.setType(PixFilterPreview.EFFECT_CROSSPROCESS);
	        	addFilter2.setDelegate(this);
	    	}
	        PixFilterButton addFilter3 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter3);
	        if (addFilter3 != null) {
	        	addFilter3.setType(PixFilterPreview.EFFECT_TEMPERTURE);
	        	addFilter3.setDelegate(this);
	    	}
	        PixFilterButton addFilter4 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter4);
	        if (addFilter4 != null) {
	        	addFilter4.setType(PixFilterPreview.EFFECT_BLACKWHITE);
	        	addFilter4.setDelegate(this);
	    	}
	        PixFilterButton addFilter5 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter5);
	        if (addFilter5 != null) {
	        	addFilter5.setType(PixFilterPreview.EFFECT_GRAYSCALE);
	        	addFilter5.setDelegate(this);
	    	}
	        PixFilterButton addFilter6 = (PixFilterButton) mFragmentView.findViewById(R.id.addFilter6);
	        if (addFilter6 != null) {
	        	addFilter6.setType(PixFilterPreview.EFFECT_SEPIA);
	        	addFilter6.setDelegate(this);
	    	}

	        if (mFilterParamsBar != null) {
	        	mFilterParamsBar.setVisibility(View.VISIBLE);
		        PixAdjustFilterButton setBrightnessBtn = (PixAdjustFilterButton)
		        		mFilterParamsBar.findViewById(R.id.setBrightnessBtn);
		        if (setBrightnessBtn != null) {
		        	setBrightnessBtn.setType(PixFilterPreview.EFFECT_BRIGHTNESS);
		        	setBrightnessBtn.setScale(.0f);
		        	setBrightnessBtn.setDelegate(this);
		    	}
		        
		        PixAdjustFilterButton setSaturationBtn = (PixAdjustFilterButton)
		        		mFilterParamsBar.findViewById(R.id.setSaturationBtn);
		        if (setSaturationBtn != null) {
		        	setSaturationBtn.setType(PixFilterPreview.EFFECT_SATURATE);
		        	setSaturationBtn.setScale(.5f);
		        	setSaturationBtn.setDelegate(this);
		    	}
		        
		        PixAdjustFilterButton setContrastBtn = (PixAdjustFilterButton) 
		        		mFilterParamsBar.findViewById(R.id.setContrastBtn);
		        if (setContrastBtn != null) {
		        	setContrastBtn.setType(PixFilterPreview.EFFECT_CONTRAST);
		        	setContrastBtn.setScale(.0f);
		        	setContrastBtn.setDelegate(this);
		    	}
	        }
        }
        updateBorder();        
        return mFragmentView;
    }
    
    @Override
	public void onNewScaleValue(float scale) {
    	if (selectedAdjustFilter == null || filterPreview == null) return;
    	
    	selectedAdjustFilter.setScale(scale);
    	filterPreview.setEffectParamScale(selectedAdjustFilter.getType(), scale);
	}
          
	@Override
	public void onAdjustFilterClick(PixAdjustFilterButton adjustFilter,
			int type, float scale) {
		if (filterPreview == null) return;
		
		// update the selected filter adjustment
		if (selectedAdjustFilter != null) {
			selectedAdjustFilter.setSelected(false);
		}
		selectedAdjustFilter = adjustFilter;
		adjustFilter.setSelected(true);

		// apply the filter adjustment
		filterPreview.setEffectParamScale(type, scale);
		filterPreview.applyFilterParam();
		
		if (mScaleBar != null) {
			mScaleBar.setScale(scale);
			mScaleBar.show();
		}
	}
	
	@Override
	public void onFilterClick(PixFilterButton filter, int type) {
		if (filterPreview == null) return;

		if (mScaleBar != null) {
			mScaleBar.hide();
		}
		if (selectedAdjustFilter != null) {
			selectedAdjustFilter.setSelected(false);
		}
		
		// update the selected filter
		updateSelectedFilterBtn(filter);
		
		filterPreview.applyFilter(type);
		lastFilterApplied = type;
	}
    
    private void updateSelectedFilterBtn(final ImageView filterBtn) {
    	// remove the selected border from the previous filter
    	if (selectedFilter != null) {
    		selectedFilter.setImageResource(R.color.transparent);
    	}
		selectedFilter = filterBtn;
		// set the border on the current filter
		selectedFilter.setImageResource(R.drawable.filter_border);
	}

	@Override
	public void onResume() {
    	super.onResume();
    	if (mToolbarController != null) {
    		mToolbarController.setToolbarVisible(false);
    	}
    	if (PixDefs.hasImageFilters &&
    			this.filterPreview != null) {
    		this.filterPreview.onResume();
    	}
    	if (this.mSplitPreview != null) {
    		if (PixDefs.hasImageFilters &&
    				this.filterPreview != null) {
    			this.filterPreview.setVisibility(View.VISIBLE);
    			if (lastFilterApplied != PixFilterPreview.EFFECT_NONE) {
    				this.filterPreview.applyFilter(lastFilterApplied);
    			}
    		}
    		this.mSplitPreview.show();
    	}
    	if (this.approveSplit != null) {
    		this.approveSplit.setEnabled(true);
    	}
    }
    
    @Override
	public void onPause() {
    	super.onPause();
    	if (PixDefs.hasImageFilters &&
    			this.filterPreview != null) {
    		this.filterPreview.onPause();
    	}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (outState != null) {
    		if (this.compType != null) {
    			this.compType.serialize(outState);
        	}
        	if (this.compMetadata != null) {
        		this.compMetadata.serialize(outState);
        	}
        	if (lastFilterApplied != PixFilterPreview.EFFECT_NONE) {
        		outState.putInt(PROCESS_FILTER_APPLIED_KEY, lastFilterApplied);
        	}
    	}
    }
  
    public void onSplitProcessingApproved() {
    	if (this.mSplitPreview != null) {
    		if (PixDefs.hasImageFilters &&
    				this.filterPreview != null) {
    			this.filterPreview.setVisibility(View.INVISIBLE);
    		}
    		this.mSplitPreview.hide();
    	}
		Bundle args = new Bundle();
		if (compType != null) {
			compType.serialize(args);
		}
		if (compMetadata == null) return;
	
		if (filterPreview != null) {
			PixBitmapHolder filteredBitmapHolder = filterPreview.getFilteredBitmap();
			setActivePartProcessedBitmap(filteredBitmapHolder);
		}
		compMetadata.serialize(args);
		
		if (compMetadata.isPrivate()) {
			// send the private composition now - without publish/info screen
			sendPrivateComp(compMetadata.getPrivateCompMetadata());
		} else {
			if (mActivitiesController != null) {
				mActivitiesController.gotoPublishNewSplit(getActivity(), args, joinedCompImage, activePartProcessedBitmap);
			}
		}
		return;
    }
    
    /**
     *  due to the GLSurface View who 'floats' on top of the parts, handling 
     *  the border should take the surface view into account and add a border
     *  on top of it.
     */
    private void updateBorder() {
    	if (mSplitPreview == null || compMetadata == null) return;
    	
    	boolean hasBorder = compMetadata.hasBorder();
    	SplitBorderType borderType = hasBorder ? SplitBorderType.BORDER_TYPE_WIDE 
    										   : SplitBorderType.BORDER_TYPE_NONE;
		mSplitPreview.setBorder(borderType);
		if (PixDefs.hasImageFilters &&
				filterPreview != null) {
			filterPreview.updateBorder(borderType);
		}	
    }
    
    private void sendPrivateComp(final PixPrivateCompositionMetadata privateCompMetadata) {
    	if (privateCompMetadata != null && compMetadata != null) {
    		// check if the composition is completed
        	boolean isCompleted = (compType.getNumOfExistingComponents()+1 == compType.getMaxComponentsForType());
        	compMetadata.setCompleted(isCompleted);
        	
    		// create the final split
    		Bitmap finalSplitBitmap = SplitPreviewFrame.createFinalSplit( getActivity(),
    				this.compType, 
    				(joinedCompImage != null) ? joinedCompImage.getBitmap() : null, 
					(activePartProcessedBitmap != null) ? activePartProcessedBitmap.getBitmap() : null,
					compMetadata.getNewComponentIndex(),
    				compMetadata.isCompleted(), 
    				compMetadata.hasBorder());
			if (finalSplitBitmap != null) {
				compMetadata.setCompositionImage(finalSplitBitmap);
			}
			
    		final String conversationId = privateCompMetadata.getConversationId();
    		final String recipient = privateCompMetadata.getRecipient();
    		final String parentMsgId = privateCompMetadata.getParentMsgId();
    		
	    	PixProgressIndicator.showMessage(getActivity(), 
	    			PixApp.getContext().getString(R.string.finishing_up), true);

	    	HashMap<String, String> context = new HashMap<String, String>();
    		context.put(PixDefs.APP_PARAM_MSG_TYPE, PixDefs.APP_PARAM_MSG_TYPE_SPLIT);
			PixAppState.logAppEvent(PixDefs.APP_SCREEN_PRIVATE_CHAT, PixDefs.APP_EVENT_MSG_SENT, context);

			new ChatAddMessageHttpRequest(new RequestDelegate() {
				@Override
				public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
					PixProgressIndicator.hideMessageWithIndication(false, 
							PixApp.getContext().getString(R.string.failed));
					if (mActivity != null) {
						mActivity.onError(error);
					}
				}
				
				@Override
				public void requestFinished(BaseHttpRequest request, PixObject response) {
					PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getResources().getString(R.string.done));
					
					if (request != null && response != null && response instanceof PixChatMessageContainer) {
						PixChatMessageContainer chatMsgContainer = (PixChatMessageContainer)response;
						PixChatMessage msg = chatMsgContainer.getChatMessage();

						if (msg != null && msg.getComp() != null) {
							//Utils.pixLog(TAG, "loading split success!");
							
							if (mActivitiesController != null) {
								mActivitiesController.markForRefresh(conversationId, true);
							}
							if (getActivity() != null) {
								Intent resultIntent = new Intent();
								resultIntent.putExtra(PixDefs.INTENT_EXTRA_CREATE_NEW_SPLIT_SUCCESS, true);
								if (Utils.isValidString(conversationId)) {
									resultIntent.putExtra(PixDefs.INTENT_EXTRA_NEW_SPLIT_CONVERSATION_ID, conversationId);
								}
								getActivity().setResult(Activity.RESULT_OK, resultIntent);
								getActivity().finish();
							}
						}
					}
				}
			}, 
			conversationId, recipient, parentMsgId, PixChatMessage.PIX_MESSAGE_TYPE_COMP, null, compMetadata).send();
    	}
    }
}
