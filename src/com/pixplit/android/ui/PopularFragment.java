package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PopularListAdapter;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixCompositions;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.PixCompsPack;

public class PopularFragment extends PixFeedFragment{
	//private static final String TAG = "PopularFragment";
	
	public static PopularFragment newInstance(Bundle args) {
		PopularFragment fragment = new PopularFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setFragmentType(EFragmentType.POPULAR);
        setRequestUrl(PixHttpAPI.POPULAR_FEED_URL);
        setRefreshBtn(true);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mFragmentView != null) {
			removeFragmentViewFromParent(); 
		} else {
			mFragmentView = inflater.inflate(R.layout.popular_layout, container, false);
		}
		return mFragmentView;
    }

	@Override
	protected PopularListAdapter createAdapter(){
		PopularListAdapter adapter = null;
		if (getActivity() != null) {
			adapter = new PopularListAdapter(getActivity(), this, 
					R.layout.comp_sixpack_layout, R.layout.comp_sixpack_second_layout);
		}
		return adapter;
	}
	
	@Override
	protected void handleListResponse(PixObject response, Bundle requestContext) {
		ArrayList<PixCompsPack> parsedData = null;
		
		if (response != null && response instanceof PixCompositions) {
			PixCompositions compositions = (PixCompositions)response;
			parsedData = (ArrayList<PixCompsPack>) PixCompsPack.createCompsPackList(compositions.getItems(), 6);
			updateAdapterData(parsedData);
		}
	
	}
	
	@Override
	public void onCompImageClick(PixComposition clickedComp) {
		if (clickedComp != null && Utils.isValidString(clickedComp.getCompId())) {
			Bundle args = new Bundle();
			args.putString(PixDefs.FRAG_ARG_COMP_ID, clickedComp.getCompId());
			if (mActivitiesController != null) {
				if (mActivitiesController != null) {
					ArrayList<PixCompsPack> data = (ArrayList<PixCompsPack>)mAdapter.getData();
					ArrayList<PixComposition> comps = new ArrayList<PixComposition>();
					int clickedCompIndex = 0;
					// flatten the compositions array and find the clicked comp index
					for (int i=0; i<data.size(); i++) {
						PixCompsPack compsPack = data.get(i);
						if (compsPack != null) {
							int size = compsPack.getPackSize();
							for (int j=0; j<size; j++) {
								PixComposition comp = compsPack.getComp(j);
								if (comp != null) {
									comps.add(comp);
									if (clickedCompIndex == 0 &&
											clickedComp.equals(comp)) {
										clickedCompIndex = comps.size()-1;
									}
								}
							}
						}
					}
					args.putInt(PixDefs.FRAG_ARG_GALLERY_COMP_INDEX, clickedCompIndex);
					args.putString(PixDefs.FRAG_ARG_GALLERY_TITLE, PixApp.getContext().getString(R.string.popular));
					args.putString(PixDefs.FRAG_ARG_GALLERY_FEED_URL, PixHttpAPI.POPULAR_FEED_URL);
					args.putString(PixDefs.FRAG_ARG_GALLERY_NEXT_PAGE, getNextPage());
					args.putBoolean(PixDefs.FRAG_ARG_GALLERY_ALLOW_PAGING, true);
					args.putBoolean(PixDefs.FRAG_ARG_GALLERY_INIT_WITH_COMPS, true);
					mActivitiesController.gotoGalleryFeed(getActivity(), args, 0, comps);
				}
			}
		}
	}
}
