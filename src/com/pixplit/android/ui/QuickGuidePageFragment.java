package com.pixplit.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.util.Utils;

public class QuickGuidePageFragment extends PixBaseFragment{
	
	ImageView quickGuideLayoutBg;
	View quickGuideCardLayout;
	ImageView quickGuideImage;
	TextView quickGuideStepText;
	TextView nextBtn;
	int mGuideStep;
	PagerControllerInterface mPagerController;

	static final int colorTransparent = PixApp.getContext().getResources().getColor(R.color.transparent);
	static final int colorWhite = PixApp.getContext().getResources().getColor(R.color.white);
	static final int colorGrey = PixApp.getContext().getResources().getColor(R.color.light_grey_bg);
	
	static final int cardLayoutMarging = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.quick_guide_layout_horizontal_margin);
	static final int cardLayoutPadding = PixApp.getContext().getResources().getDimensionPixelSize(R.dimen.quick_guide_layout_padding);
	
	private QuickGuideStep[] steps = {
			new QuickGuideStep(0, null),
			new QuickGuideStep(R.drawable.guide_2, PixApp.getContext().getString(R.string.choose_a_split_frame)),
			new QuickGuideStep(R.drawable.guide_3, PixApp.getContext().getString(R.string.fill_one_of_the_slots_with_your_photo_publish_to_your_followers_)),
			new QuickGuideStep(R.drawable.guide_4, PixApp.getContext().getString(R.string.followers_can_now_complete_your_split_with_their_own_photo)) /*,
			new QuickGuideStep(R.drawable.guide_5, PixApp.getContext().getString(R.string.create_an_account_or_connect_with_facebook))*/ };
	
	private class QuickGuideStep {
		private int imageRes;
		private String stepText;
		
		QuickGuideStep(int imageRes, String stepText) {
			this.imageRes = imageRes;
			this.stepText = stepText;
		}
		public int getImageRes() {
			return imageRes;
		}
		public String getStepText() {
			return stepText;
		}
	}
	
	public static QuickGuidePageFragment newInstance(Bundle args) {
		QuickGuidePageFragment fragment = new QuickGuidePageFragment();
		Bundle b = new Bundle();
		if (args != null) {
			b.putAll(args);
		}
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setFragmentType(EFragmentType.QUICK_GUIDE);
        this.setPropagateTouch(true);
        this.mGuideStep = 0;
        Bundle args = getArguments();
        if (args != null) {
        	this.mGuideStep = args.getInt(PixDefs.FRAG_ARG_PAGE_NUM, 0);
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.quick_guide_page_layout, container, false);
    	if (mFragmentView != null) {
    		
    		quickGuideLayoutBg = (ImageView)mFragmentView.findViewById(R.id.quickGuideLayoutBg);
    		quickGuideCardLayout = mFragmentView.findViewById(R.id.quickGuideCardLayout);
    		quickGuideImage = (ImageView)mFragmentView.findViewById(R.id.quickGuideImage);
    		nextBtn = (TextView)mFragmentView.findViewById(R.id.nextBtn);
	    	if (nextBtn != null) {
	    		nextBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						gotoNextStep();
					}
				});
	    	}
	    	quickGuideStepText = (TextView)mFragmentView.findViewById(R.id.quickGuideStepText);
    	}
	    	
    	this.updateGuide(mGuideStep);
    	return mFragmentView;
    }
    
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    //Utils.pixLog(TAG, "==> onAttach called for "+fragment_type);
	    try {
	    	mPagerController = (PagerControllerInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement PagerControllerInterface");
	    }
    }
    
    @Override
    public void onDetach () {
    	super.onDetach();
    	mPagerController = null;
    }
    
    protected void gotoNextStep() {
    	if (mGuideStep < steps.length-1) {
			if (mPagerController != null) {
				mPagerController.setPageNum(mGuideStep+1);
			}
    	} else {
    		if (mActivitiesController != null) {
				mActivitiesController.gotoPopularRequest(getActivity(), null);
			}
    	}
	}

	@Override
    public void onStart() {
    	super.onStart();
    }

    private void updateGuide(int step) {
    	if (this.steps == null || 
    			step < 0 || 
    			step >= this.steps.length || 
    			this.steps[step] == null) {
    		return;
    	}
    	if (quickGuideLayoutBg != null) {
    		if (step == 0) {
    			quickGuideLayoutBg.setImageResource(R.drawable.guide_1);
	    	} else {
	    		quickGuideLayoutBg.setImageResource(0);
	    	}
    	}
    	if (quickGuideCardLayout != null) {
	    	if (step == 0) {
	    		quickGuideCardLayout.setBackgroundColor(colorTransparent);
	    	} else {
	    		quickGuideCardLayout.setBackgroundColor(colorWhite);
	    	}
    	}
    	if (quickGuideImage != null) {
    		// calc the image dimensions - in order to fit an optimal square on all screens
    		int imageDimen = PixAppState.getState().getFrameWidth() - 2*cardLayoutMarging - 2*cardLayoutPadding;
    		quickGuideImage.getLayoutParams().width = imageDimen;
    		quickGuideImage.getLayoutParams().height = imageDimen;
    		
    		if (step == 0) {
    			quickGuideImage.setBackgroundColor(colorTransparent);
    			quickGuideImage.setImageResource(0);
    		} else {
    			quickGuideImage.setVisibility(View.VISIBLE);
    			int imageRes = this.steps[step].getImageRes();
				quickGuideImage.setImageResource(imageRes);
    		}
    	}
    	if (quickGuideStepText != null) {
    		String stepText = this.steps[step].getStepText();
    		if (Utils.isValidString(stepText)) {
    			quickGuideStepText.setText(stepText);
    			quickGuideStepText.setVisibility(View.VISIBLE);
    		} else {
    			quickGuideStepText.setText("");
    		}
    	}
    	if (nextBtn != null) {
    		if (step == 0) {
    			nextBtn.setText(PixApp.getContext().getString(R.string.quick_guide));
    		} else if (mGuideStep < steps.length-1) {
    			nextBtn.setText(PixApp.getContext().getString(R.string.next));
    		} else {
    			nextBtn.setText(PixApp.getContext().getString(R.string.done));
    		}
    	}
    }
}
