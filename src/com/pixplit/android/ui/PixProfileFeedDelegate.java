package com.pixplit.android.ui;

import com.pixplit.android.data.PixUser;

public interface PixProfileFeedDelegate {
	public void onUserFollowClick(PixUser user);
}