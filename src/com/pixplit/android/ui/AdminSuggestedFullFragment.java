package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.adapters.SuggestedUsersListAdapter;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.data.PixUsers;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.PixHttpAPI;
import com.pixplit.android.http.request.AdminRemoveSuggestedUserHttpRequest;
import com.pixplit.android.http.request.AdminSuggestedFeedHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.UserViewHolder.UserActionsDelegate;

public class AdminSuggestedFullFragment extends PixFeedFragment implements UserActionsDelegate{
	
	public static AdminSuggestedFullFragment newInstance() {
		AdminSuggestedFullFragment fragment = new AdminSuggestedFullFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        setFragmentType(EFragmentType.ADMIN_SUGGESTED_FULL);
        setRequestUrl(PixHttpAPI.ADMIN_SUGGESTED_FULL_URL);
        if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(PixApp.getContext().getResources().getString(R.string.admin_suggested_feed));
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.users_layout, container, false);
		return mFragmentView;
	}
	
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
		BaseVolleyHttpRequest request = new AdminSuggestedFeedHttpRequest(this, 
				getRequestUrl(), Priority.IMMEDIATE);
		request.addAdminIdentity();
		return request;
	}
	
	@Override
	protected PixBaseListAdapter<?> createAdapter() {
		SuggestedUsersListAdapter adapter = new SuggestedUsersListAdapter(getActivity(), this, R.layout.user_layout, this);
		return adapter;
	}
	
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixUsers) {
			PixUsers users = (PixUsers) response;
			ArrayList<PixUser> usersFeed = users.getItems();
			updateAdapterData(usersFeed);
		}
	}

	@Override
	public void onUserClick(final PixUser pixUser) {
		if (pixUser == null || !Utils.isValidString(pixUser.getUsername())) return;
		
		PixProgressIndicator.showMessage(getActivity(), 
				PixApp.getContext().getString(R.string.admin_removing_user_from_suggested), true);
		new AdminRemoveSuggestedUserHttpRequest(new RequestDelegate() {
			
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
			}
			
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getString(R.string.done));
				if (mAdapter != null && mAdapter instanceof SuggestedUsersListAdapter) {
					((SuggestedUsersListAdapter)mAdapter).removeUser(pixUser);
				}
			}
		}, pixUser.getUsername()).send();
	}
}
