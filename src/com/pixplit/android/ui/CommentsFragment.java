package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.adapters.CommentsListAdapter;
import com.pixplit.android.adapters.PixBaseListAdapter;
import com.pixplit.android.data.PixComment;
import com.pixplit.android.data.PixComments;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.CommentsFeedHttpRequest;
import com.pixplit.android.http.request.DeleteCommentHttpRequest;
import com.pixplit.android.http.request.PostCommentHttpRequest;
import com.pixplit.android.ui.elements.PixConfirmationDialog;
import com.pixplit.android.ui.elements.PixConfirmationDialog.ConfirmationDialogDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.appreciation.AppreciationManager;
import com.pixplit.android.util.viewholders.CommentViewHolder.CommentViewDelegate;


public class CommentsFragment extends PixListFragment<PixComment> implements CommentViewDelegate{

	private String compId = null;
    // Appreciation manager
    private AppreciationManager mAppreciationManager = null;
	
	public interface CompCommentsUpdateDelegate {
		public void onTotalCommentsUpdate(int totalComments);
	}
	
	public static CommentsFragment newInstance(Bundle args) {
		CommentsFragment fragment = new CommentsFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAllowPaging(true);
        setFragmentType(EFragmentType.COMMENTS);
        this.mAppreciationManager = AppreciationManager.getAppreciationManager(getContext());
        
        Bundle args = getArguments();
        if (args != null) {
        	compId = args.getString(PixDefs.FRAG_ARG_COMP_ID);
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.comments_layout, container, false);
		updateAddCommentBar();
		
		return mFragmentView;
	}
	
	@Override
	public void onDestroyView() {
		PixApp.hideKeypad(mFragmentView);
		super.onDestroyView();
	}
    
	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		if (response != null && response instanceof PixComments) {
			PixComments comments = (PixComments)response;
			
			ArrayList<PixComment> commentsFeed = (ArrayList<PixComment>) comments.getItems();
			boolean addCommentRsp = false;
			
			if (requestContext != null) {
				addCommentRsp = requestContext.getBoolean(PixDefs.REQ_CTX_COMP_ADD_COMMENT);
			}
			
			if (addCommentRsp == true) {
				changeAdapterData(commentsFeed);
				// return the updated number of comments to the calling activity
				if (getActivity() != null) {
					Intent resultIntent = new Intent();
					resultIntent.putExtra(PixDefs.INTENT_EXTRA_COMP_COMMENTS_NUM, comments.getTotalComments());
					getActivity().setResult(Activity.RESULT_OK, resultIntent);
				}
			} else {
				updateAdapterData(commentsFeed);
			}
			if (mFragmentView != null) {
				mFragmentView.postDelayed(new Runnable() {
			        public void run() {
						scrollToLast();
			        }
			    }, 700);
			}
		}
	}
	
    @Override
	protected BaseVolleyHttpRequest getRefreshRequest(){
    	return new CommentsFeedHttpRequest(this, compId);
	}
    
    @Override
	protected PixBaseListAdapter<?> createAdapter(){
    	CommentsListAdapter adapter = null;
    	if (getActivity() != null) {
    		adapter = new CommentsListAdapter(getActivity(), this, R.layout.comment_layout, this);
    	}
    	return adapter;
	}
    
    private void updateAddCommentBar() {
    	if (mFragmentView != null) {
    		TextView sendNewComment = (TextView)mFragmentView.findViewById(R.id.newCommentSend);
    		EditText newComment = (EditText)mFragmentView.findViewById(R.id.newCommentText);
    		
    		sendNewComment.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					ViewParent vp = v.getParent();
					if (vp instanceof RelativeLayout) {
						EditText newComment = (EditText)((RelativeLayout)vp).findViewById(R.id.newCommentText);
						if (newComment != null) {
							sendNewComment(newComment.getText().toString());
							newComment.setText("");
						}
					}
					PixApp.hideKeypad(mFragmentView);
				}
			});
    		newComment.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					
					v.postDelayed(new Runnable() {
				        public void run() {
							scrollToLast();
				        }
				    }, 700);
					return false;
				}
			});
    	}
    }
    
    public void sendNewComment(String comment) {
    	if (!this.verifyUser()) return;
    	PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getString(R.string.posting_comment), true);
    	PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_COMMENTS, PixDefs.APP_EVENT_COMMENT_ADDED, null);
    	RequestDelegate addCommentDelegate = new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
				if (mActivity != null) {
					mActivity.onError(error);
				}
			}
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessage();
				if (request != null && response != null) {
					handleListResponse(response, request.getRequestContext());
				}
			 	CommentsFragment.this.mAppreciationManager.onEvent("Comment Added");
			}
		};
		new PostCommentHttpRequest(addCommentDelegate, compId, comment).send();
	}

	@Override
	public void onCommentClick(PixComment comment) {
		if (comment != null &&
				Utils.isValidString(comment.getUsername())) {
			onProfileClick(comment.getUsername());
		}
		
	}

	@Override
	public void onCommentLongClick(final PixComment comment) {
		if (comment != null &&
				Utils.isValidString(comment.getUsername())) {
			String loggedInUser = PixAppState.UserInfo.getUserName();
	    	String commenter = comment.getUsername();
			if (Utils.isValidString(loggedInUser) &&
	    			commenter.equalsIgnoreCase(loggedInUser)) {
				
				
				new PixConfirmationDialog(getActivity(), new ConfirmationDialogDelegate() {
					public void onConfirm() {
					PixAppState.logAppEvent(PixDefs.APP_SCREEN_SPLIT_COMMENTS, PixDefs.APP_EVENT_COMMENT_DELETED, null);
						
						// send the deletion request to the server
						new DeleteCommentHttpRequest(compId, comment.getId()).send();
						
						if (mAdapter != null && Utils.isValidString(comment.getId())) {
							// delete the comment from the list locally
							if (mAdapter instanceof CommentsListAdapter) {
								((CommentsListAdapter)mAdapter).deleteComment(comment.getId());
							}
						}
						
					}
					public void onDecline() {}
				},
				PixApp.getContext().getResources().getString(R.string.confirm_deletion),
				PixApp.getContext().getResources().getString(R.string.are_you_sure), 
				PixApp.getContext().getResources().getString(R.string.ok), null).show();
			}
		}
	}
	
	@Override
	public void onStart() {
    	super.onStart();
    	this.mAppreciationManager.onStart();
    }
    
    @Override
	public void onStop() {
    	super.onStop();
    	this.mAppreciationManager.onStop();
    }
    
    @Override
	public void onDestroy() {
    	super.onDestroy();
    	this.mAppreciationManager.onDestroy();
    }
}
