package com.pixplit.android.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.util.Utils;

public class GalleryFeedFragment extends PixFeedFragment{
	private String mTitle;
	private String mUrl;
	private ArrayList<PixComposition> mComps;
	private int mInitialIndex = 0;
	private String mInitialNextPage;
	private boolean initWithComps = false;
	
	public static GalleryFeedFragment newInstance(Bundle args) {
		GalleryFeedFragment fragment = new GalleryFeedFragment();
        Bundle b = new Bundle();
        if (args != null) {
        	b.putAll(args);
        }
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        fragment.setArguments(b);
        return fragment;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        setFragmentType(EFragmentType.GALLERY_VIEW);
        boolean allowPagin = false;
        if (args != null) {
        	mTitle = args.getString(PixDefs.FRAG_ARG_GALLERY_TITLE);
        	mUrl = args.getString(PixDefs.FRAG_ARG_GALLERY_FEED_URL);
        	mInitialIndex = args.getInt(PixDefs.FRAG_ARG_GALLERY_COMP_INDEX, 0);
        	mInitialNextPage = args.getString(PixDefs.FRAG_ARG_GALLERY_NEXT_PAGE);
        	allowPagin = args.getBoolean(PixDefs.FRAG_ARG_GALLERY_ALLOW_PAGING);
        	initWithComps = args.getBoolean(PixDefs.FRAG_ARG_GALLERY_INIT_WITH_COMPS);
        }
        
        setRequestUrl(mUrl);
        setAllowPaging(allowPagin);
        if (mToolbarController != null && Utils.isValidString(mTitle)) {
			mToolbarController.setToolBarTitle(mTitle);
		}
	}
	
	@Override
	protected BaseVolleyHttpRequest getRefreshRequest() {
		if (initWithComps && mInitialIndex >= 0) {
			// the feed was initiated with Comps and index. don't fetch feed.
			mComps = PixApp.getGalleryFeedComps();
			if (mComps != null && mComps.size() > 0) {
				changeAdapterData(mComps);
				setNextPage(mInitialNextPage);
				setPosition(mInitialIndex);
				mInitialIndex = -1;
				return null;
			}
		} 
		return super.getRefreshRequest();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.single_view_layout, container, false);
	 	return mFragmentView;
    }
	
	@Override
	protected void onCompDeletion(String deletedCompId) {
		super.onCompDeletion(deletedCompId);
		this.removeCompositionFromFeed(deletedCompId);
	}
	
	@Override
	public void removeCompositionFromFeed(String compId) {
		if (Utils.isValidString(compId) && mAdapter != null) {
			ArrayList<PixComposition> compArray = (ArrayList<PixComposition>)mAdapter.getData();
			if (compArray != null) {
				for (int i=0; i < compArray.size(); i++) {
					PixComposition comp = compArray.get(i);
					if (comp != null && comp.getCompId() != null &&
							comp.getCompId().equalsIgnoreCase(compId)) {
						mAdapter.removeItemAtIndex(i);
					}
				}
			}
		}
	}
}
