package com.pixplit.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixDefs;


public class WebViewFragment extends PixBaseFragment{
	
	String mUrl;
	String mTitle;
	
	public static WebViewFragment newInstance(Bundle args) {
		WebViewFragment fragment = new WebViewFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        b.putAll(args);
        fragment.setArguments(b);
        return fragment;
	}
	
	static WebViewFragment newInstance(String title, String url) {
		WebViewFragment fragment = new WebViewFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, false);
        b.putString(PixDefs.FRAG_ARG_URL, url);
        b.putString(PixDefs.FRAG_ARG_TITLE, title);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.WEB_VIEW);
        Bundle args = getArguments();
        if (args != null) {
        	mUrl = args.getString(PixDefs.FRAG_ARG_URL);
        	mTitle = args.getString(PixDefs.FRAG_ARG_TITLE);
        }
        
        if (mToolbarController != null) {
        	mToolbarController.setToolBarTitle(mTitle);
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mFragmentView = inflater.inflate(R.layout.web_view_fragment_layout, container, false);
    	if (mFragmentView != null) {
    		WebView webView = (WebView)mFragmentView.findViewById(R.id.webview);
    		if (webView != null && mUrl != null) {
    			webView.setWebViewClient(new WebViewClient());
    			webView.loadUrl(mUrl);
    		}
    	}
    	return mFragmentView;
    }
    
    @Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
}
