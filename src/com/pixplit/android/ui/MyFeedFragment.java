package com.pixplit.android.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.android.volley.Request.Priority;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixCompositions;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixCompositionFramesetType;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseVolleyHttpRequest;
import com.pixplit.android.http.request.BaseVolleyHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.CompsFeedHttpRequest;
import com.pixplit.android.ui.elements.NewSplitsBar;
import com.pixplit.android.ui.elements.NewSplitsBar.NewSplitsBarDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixServerDateFormat;
import com.pixplit.android.volley.PixImagesLoader;

public class MyFeedFragment extends PixFeedFragment implements NewSplitsBarDelegate,
																MyFeedControlDelegate{
	//private static final String TAG = "MyFeedFragment";
		
	private ArrayList<PixComposition> newPendingCompositions;
	private int newPendingCompositionsNum;
	private NewSplitsBar newSplitsBar;
	
	private MyFeedControllerInterface mMyFeedController;
	private BillingHelperController mIabController;
	
	static MyFeedFragment newInstance() {
		MyFeedFragment fragment = new MyFeedFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
		
	@Override
	public void onStart(){
    	super.onStart();
    	if (mToolbarController != null) {
			ImageView createNewSplit = mToolbarController.addToolbarImageBtn(
					PixDefs.TOOLBAR_BTN_ID_START_SPLIT,
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							if (mActivitiesController != null &&
									mIabController != null) {
								Bundle args = new Bundle();
								args.putInt(PixDefs.FRAG_ARG_COMP_FRAME_TYPE, 
										PixCompositionFramesetType.getInt(PixCompositionFramesetType.Pix2HorizontalFrameset));
								mActivitiesController.gotoCreateNewSplit(getActivity(), args, null);
					    	}
						}
					});
			if (createNewSplit != null) {
				createNewSplit.setImageResource(R.drawable.bar_camera_btn_selector);
			}
		}
	}
	
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mMyFeedController != null) {
    		mMyFeedController.registerMyFeedDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mMyFeedController != null) {
    		mMyFeedController.unregisterMyFeedDelegate(this);
    	}
    }
    
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mMyFeedController = (MyFeedControllerInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement MyFeedControllerInterface");
	    }
	    try {
	    	mIabController = (BillingHelperController)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement BillingHelperController");
	    }
    }

    @Override
    public void onDetach () {
    	super.onDetach();
    	mMyFeedController = null;
    	mIabController = null;
    }

    	
	private void processNewItems(int newItemsNum, ArrayList<PixComposition> compositionsList) {
		if (mFragmentView != null && newItemsNum > 0 && compositionsList != null) {
			newPendingCompositions = compositionsList;
			newPendingCompositionsNum = newItemsNum;
			
			// load the first new pending comp image in the bg
			if (newPendingCompositions != null && newPendingCompositions.size() > 0) {
				PixComposition comp = newPendingCompositions.get(0);
				if (comp != null && Utils.isValidString(comp.getImage())) {
					PixImagesLoader.loadImageInBg(comp.getImage());
				}
			}
			
			if (getActivity() != null) {
				if (this.newSplitsBar == null) {
					// create a new bar
					this.newSplitsBar = new NewSplitsBar(getActivity(), (this instanceof MyCompletedFragment), this);
					this.newSplitsBar.setNewSplitsNumber(newItemsNum);
					ViewGroup feedsListContainer = (ViewGroup)mFragmentView.findViewById(R.id.feedsListContainer);
					if (feedsListContainer != null) {
						LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 
								LayoutParams.WRAP_CONTENT);
						feedsListContainer.addView(newSplitsBar, params);
					}
				} else {
					this.newSplitsBar.setNewSplitsNumber(newItemsNum);
					this.newSplitsBar.show();
				}
			}
		}
	}
	
	@Override
	protected void onCompDeletion(String deletedCompId) {
		super.onCompDeletion(deletedCompId);
		this.removeCompositionFromFeed(deletedCompId);
	}
	
	@Override
	public void removeCompositionFromFeed(String compId) {
		if (Utils.isValidString(compId) && mAdapter != null) {
			ArrayList<PixComposition> compArray = (ArrayList<PixComposition>)mAdapter.getData();
			if (compArray != null) {
				for (int i=0; i < compArray.size(); i++) {
					PixComposition comp = compArray.get(i);
					if (comp != null && comp.getCompId() != null &&
							comp.getCompId().equalsIgnoreCase(compId)) {
						mAdapter.removeItemAtIndex(i);
					}
				}
			}
		}
	}

	@Override
	public void handleListResponse(PixObject response, Bundle requestContext) {
		super.handleListResponse(response, requestContext);
		
		// update the time stamp of the most recent composition
		if (response != null && response instanceof PixCompositions) {
			PixCompositions compositions = (PixCompositions)response;
			ArrayList<PixComposition> compositionsList = compositions.getItems();
			if (this.isFirstPage(requestContext)) {
				this.updateNewRecordsTimestamp(compositionsList);
			}
		}
	}
	
	private void updateNewRecordsTimestamp(ArrayList<PixComposition> compositionsList) {
		if (this.isFeedUpdate(compositionsList)) {
			// update the time stamp
			if (compositionsList != null && compositionsList.size() > 0) {
	    		PixComposition newestComposition = compositionsList.get(0);
				if (newestComposition != null) {
					String newTimestamp = newestComposition.getInsertDate();
					if (this instanceof MyCompletedFragment) {
						PixAppState.setTimestamp(PixAppState.COMPLETED_TIMESTAMP, newTimestamp);
					} else {
						PixAppState.setTimestamp(PixAppState.ACTIONS_TIMESTAMP, newTimestamp);
					}
				}
			}			
		}
    }
	
	private boolean isFeedUpdate(ArrayList<PixComposition> compositionsList) {
		// get the most recent composition time stamp
    	if (compositionsList != null && compositionsList.size() > 0) {
    		PixComposition newestComposition = compositionsList.get(0);
			if (newestComposition != null) {
				String newTimestamp = newestComposition.getInsertDate();
				if (newTimestamp != null) {
					// check if the new time stamp is newer then the last recorded time stamp
					String currentTimestamp;
					if (this instanceof MyCompletedFragment) {
						currentTimestamp = PixAppState.getTimestamp(PixAppState.COMPLETED_TIMESTAMP);
					} else {
						currentTimestamp = PixAppState.getTimestamp(PixAppState.ACTIONS_TIMESTAMP);
					}
					Date newUpdateDate = null;
					Date lastUpdateDate = null;
					try {
						newUpdateDate = PixServerDateFormat.getPixDateInstance().parse(newTimestamp);
						lastUpdateDate = PixServerDateFormat.getPixDateInstance().parse(currentTimestamp);
					} catch (ParseException e) {
						// parsing failure
						e.printStackTrace();
					}
					if (newUpdateDate != null && 
							lastUpdateDate != null) {
						return (newUpdateDate.compareTo(lastUpdateDate) > 0);
					}
				}	
			}
    	}
		return false;
	}
	
	private void onNewFeedItems(final int newItemsNum) {
		if (newItemsNum != newPendingCompositionsNum) {
			if (newItemsNum > 0) {
				// fetch the updated feed in the background
				PixAppState.getState().getRequestQueue().add(
						new CompsFeedHttpRequest(new RequestDelegate() {
							@Override
							public void requestFinishedWithError(BaseVolleyHttpRequest request, PixError error) {
								if (mActivity != null) {
									mActivity.onError(error);
								}
							}
							@Override
							public void requestFinished(BaseVolleyHttpRequest request, PixObject response) {
								if (response != null && response instanceof PixCompositions) {
									// the feed might have been detached from the activity: check activity != null
									if (getActivity() != null) {
										PixCompositions optionalNewCompositions = (PixCompositions)response;
										ArrayList<PixComposition> compositionsList = removeDuplications(optionalNewCompositions.getItems());
										if (isFeedUpdate(compositionsList)) {
											processNewItems(newItemsNum, compositionsList);
										}
									}
								} else {
									this.requestFinishedWithError(request, new PixError(PixError.PixErrorCodeInvalidRequest));
								}
							}
						}, this.getRequestUrl(), Priority.LOW));
			} else {
				// an update from the server indicates that there are no new notifications.
				this.clearNewPendingItems();
			}
		}
	}

	@Override
	public void onNewCompleted(final int newCompletedNum) {
		super.onNewCompleted(newCompletedNum);
		if (getActivity() != null) {
			if (this instanceof MyCompletedFragment) {
				this.onNewFeedItems(newCompletedNum);
			}
		}
	}
	
	@Override
	public void onNewActions(final int newActionsNum) {
		super.onNewActions(newActionsNum);
		if (getActivity() != null) {
			if (this instanceof MyPlaygroundFragment) {
				this.onNewFeedItems(newActionsNum);
			}
		}
	}
	
	private void clearNewPendingItems() {
		// remove the bar
		if (newSplitsBar != null) {
			newSplitsBar.hide();
		}
		// clear pending data
		newPendingCompositionsNum = 0;
		newPendingCompositions = null;
	}
	
	@Override
	public void refreshList() {
		super.refreshList();
		this.clearNewPendingItems();
	}

	@Override
	public void onNewSplitsBarClick() {
		// verification that the pending items are not null in order not to empty the list in race conditions
		if (newPendingCompositions != null) {
			this.changeAdapterData(newPendingCompositions);
			this. setNextPage("2"); // as this is an updated feed, it must have a second page and it's next.
			this.updateNewRecordsTimestamp(newPendingCompositions);
		}
		this.clearNewPendingItems();
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
			@Override
			public void run() {
				scrollToFirst();
			}
		}, 250);
	}

	@Override
	public void onFeedTabSelected() {
		// override by sub-classes
	}
}
