package com.pixplit.android.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.pixplit.android.R;
import com.pixplit.android.ui.elements.PixDialog;

public class AccountPhotoDialog extends PixDialog {

	AccountPhotoDialogDelegate mDelegate;
	
	public interface AccountPhotoDialogDelegate {
		public boolean hasAccountPhoto();
		public void takePhoto();
		public void pickPhotoFromLib();
		public void removePhoto();
	}
	
	public AccountPhotoDialog(Context context, AccountPhotoDialogDelegate delegate) {
		super(context, R.layout.account_photo_dialog);
		this.mDelegate = delegate;
		
		// take photo button
		TextView takePhotoBtn = (TextView)findViewById(R.id.takePhotoBtn);
		if (takePhotoBtn != null) {
			takePhotoBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null) {
						mDelegate.takePhoto();
					}
					dismiss();
				}
			});
		}
		// pick from library button
		TextView chooseFromLibraryBtn = (TextView)findViewById(R.id.chooseFromLibraryBtn);
		if (chooseFromLibraryBtn != null) {
			chooseFromLibraryBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDelegate != null) {
						mDelegate.pickPhotoFromLib();
					}
					dismiss();
				}
			});
		}
		
		// remove photo button
		TextView removePhotoBtn = (TextView)findViewById(R.id.removePhotoBtn);
		if (removePhotoBtn != null) {
			if (mDelegate != null && mDelegate.hasAccountPhoto()) {
				removePhotoBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mDelegate.removePhoto();
						dismiss();
					}
				});
			} else {
				removePhotoBtn.setVisibility(View.GONE);
			}
			
		}
	}

}
