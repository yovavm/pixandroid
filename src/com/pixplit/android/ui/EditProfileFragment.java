package com.pixplit.android.ui;

import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.android.volley.Request.Priority;
import com.pixplit.android.EFragmentType;
import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixAccountInfo;
import com.pixplit.android.data.PixEditAccountInfo;
import com.pixplit.android.data.PixEmailValidation;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.http.request.EditAccountInfoHttpRequest;
import com.pixplit.android.http.request.GetAccountInfoHttpRequest;
import com.pixplit.android.http.request.ValidateEmailHttpRequest;
import com.pixplit.android.ui.AccountPhotoDialog.AccountPhotoDialogDelegate;
import com.pixplit.android.ui.activities.PixPhotoPickerInterface;
import com.pixplit.android.ui.elements.PixImageView;
import com.pixplit.android.ui.elements.PixProgressIndicator;
import com.pixplit.android.util.PixPhotoPickManager.PhotoFetcherDelegate;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.bitmap.PixBitmapHolder;
import com.pixplit.android.volley.BitmapFetcherDelegate;
import com.pixplit.android.volley.PixImagesLoader;


public class EditProfileFragment extends PixBaseFragment implements RequestDelegate, 
																	AccountPhotoDialogDelegate, 
																	PhotoFetcherDelegate{
	private static final String TAG = "EditProfileFragment";
	
	EditText emailText;
	ImageView emailIcon;
	View emailValidationSpinner;
	Animation spinnerAnim;
	RelativeLayout addPhoto;
	PixImageView addPhotoBtn;
	EditText passwordText;
	ImageView passwordIcon;
	EditText fullnameText;
	ImageView fullnameIcon;
	PixBitmapHolder accountPhoto;
	boolean profileOrigHasImage = false;
	boolean profileOrigTypePix = false;
	String profileOrigEmail = "";
	String profileOrigFullname = "";
	PixBitmapHolder profileOrigAcountPhoto;
	String profilePictureUrl;
	
	// validation stuff
	private static final int EMAIL_INVALID_MAIL = 1;
	private static final int EMAIL_INVALID_EXISTS = 2;
	String validatedEmail;
	
	private PixPhotoPickerInterface mPhotoPickActivity;
	
	private static final String PROFILE_PICTURE_TEMP_FILE_NAME = "profile_temp_file";
	
	private void setAccountPhoto(PixBitmapHolder newAccountPhoto) {
		if (newAccountPhoto != null) {
			newAccountPhoto.useBitmap(TAG);
    	}
    	if (accountPhoto != null) {
    		accountPhoto.releaseBitmap(TAG);
    	}
    	accountPhoto = newAccountPhoto;
	}
	
	private void setAcountOrigPhoto(PixBitmapHolder origAcountPhoto) {
		if (origAcountPhoto != null) {
			origAcountPhoto.useBitmap(TAG);
    	}
    	if (profileOrigAcountPhoto != null) {
    		profileOrigAcountPhoto.releaseBitmap(TAG);
    	}
    	profileOrigAcountPhoto = origAcountPhoto;
	}
	
	// fetch user information once
	boolean fetchedUserInfo;
	private static final String EDIT_PROFILE_FETCHED_USER_INFO = "EDIT_PROFILE_FETCHED_USER_INFO";
	private static final String EDIT_PROFILE_ORIG_EMAIL = "EDIT_PROFILE_ORIG_EMAIL";
	private static final String EDIT_PROFILE_ORIG_FULLNAME = "EDIT_PROFILE_ORIG_FULLNAME";
	private static final String EDIT_PROFILE_ORIG_TYPE_PIX = "EDIT_PROFILE_ORIG_TYPE_PIX";
	private static final String EDIT_PROFILE_VALIDATED_EMAIL = "EDIT_PROFILE_VALIDATED_EMAIL";
	private static final String EDIT_PROFILE_ORIG_PICTURE_URL = "EDIT_PROFILE_ORIG_PICTURE_URL";
	private static final String EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH = "EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH";
	
	public static EditProfileFragment newInstance() {
		EditProfileFragment fragment = new EditProfileFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragmentType(EFragmentType.EDIT_PROFILE);
        //Utils.pixLog(TAG, "==> onCreate called. savedInstanceState is " + ((savedInstanceState != null) ? "not null" : "null") + ". fragment instance:"+this); 
        
        if (savedInstanceState != null) {
        	this.restoreState(savedInstanceState);
        } else {
        	this.fetchedUserInfo = false;
        }
    	if (!fetchedUserInfo) {
    		this.fetchUserInfo();
    	}
    	spinnerAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.spinning_anim);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	//Utils.pixLog(TAG, "==> onCreateView called. fragment instance:"+this);
    	mFragmentView = inflater.inflate(R.layout.edit_profile_layout, container, false);
    	if (mFragmentView != null) {
    		RelativeLayout enterEmail = (RelativeLayout)mFragmentView.findViewById(R.id.enterEmail);
    		if (enterEmail != null) {
    			emailText = (EditText)enterEmail.findViewById(R.id.emailText);
    			emailIcon = (ImageView)enterEmail.findViewById(R.id.emailIcon);
    			emailValidationSpinner = enterEmail.findViewById(R.id.emailValidationSpinner);
    			if (emailText != null) {
    				emailText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				emailText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    				emailText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (emailIcon != null) {
								emailIcon.setBackgroundResource(R.drawable.account_email_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				emailText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								validateEmailAddress();
								if (passwordText != null) {
									passwordText.requestFocus();
								}
								return true;
							}
							return false;
						}
					});
    				emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validateEmailAddress();
							}
						}
					});
    			}
    		}
    		
    		RelativeLayout enterPassword = (RelativeLayout)mFragmentView.findViewById(R.id.enterPassword);
    		if (enterPassword != null) {
    			passwordText = (EditText)enterPassword.findViewById(R.id.passwordText);
    			passwordIcon = (ImageView)enterPassword.findViewById(R.id.passwordIcon);
    			if (passwordText != null) {
    				passwordText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				passwordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
    				passwordText.setTransformationMethod(new PasswordTransformationMethod());
    				passwordText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (passwordIcon != null) {
								passwordIcon.setBackgroundResource(R.drawable.account_password_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				passwordText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								if (fullnameText != null) {
									fullnameText.requestFocus();
								}
								return true;
							}
							return false;
						}
					});
    			}
    		}
    		
    		RelativeLayout enterFullname = (RelativeLayout)mFragmentView.findViewById(R.id.enterFullname);
    		if (enterFullname != null) {
    			fullnameText = (EditText)enterFullname.findViewById(R.id.fullnameText);
    			fullnameIcon = (ImageView)enterFullname.findViewById(R.id.fullnameIcon);
    			if (fullnameText != null) {
    				fullnameText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    				fullnameText.setInputType(EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    				fullnameText.addTextChangedListener(new TextWatcher() {
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
							if (fullnameIcon != null) {
								fullnameIcon.setBackgroundResource(R.drawable.account_name_icon);
							}
						}
						@Override
						public void afterTextChanged(Editable s) {}
					});
    				fullnameText.setOnEditorActionListener(new OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
							if(actionId == EditorInfo.IME_ACTION_NEXT) {
								if (validateFullname()) {
									if (addPhoto != null) {
										addPhoto.performClick();
									}
								}
								return true;
							}
							return false;
						}
					});
    				fullnameText.setOnFocusChangeListener(new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								validateFullname();
							}
						}
					});
    			}
    			
    			addPhoto = (RelativeLayout) mFragmentView.findViewById(R.id.addPhoto);
    			if (addPhoto != null) {
    				addPhoto.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							onAccountPhotoClick();
						}
					});
					addPhotoBtn = (PixImageView) addPhoto.findViewById(R.id.addPhotoBtn);
    				if (addPhotoBtn != null) {
    					if (savedInstanceState != null) {
    						String filePath = savedInstanceState.getString(EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH);
    						Bitmap storedBitmap = Utils.getBitmapFromFile(filePath, 
    								PixDefs.PROFILE_BITMAP_SCALE_WIDTH, PixDefs.PROFILE_BITMAP_SCALE_HIGHT);
    						if (storedBitmap != null) {
    							setAccountPhoto(new PixBitmapHolder(storedBitmap));
    							addPhotoBtn.setImageBitmapHolder(accountPhoto);
    							Utils.deleteBitmapFile(filePath);
    						}
    					}
    				}
    			}
    		}
    		
    		TextView saveBtn = (TextView)mFragmentView.findViewById(R.id.saveBtn);
    		if (saveBtn != null) {
    			saveBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PixAppState.logAppEvent(PixDefs.APP_SCREEN_EDIT_PROFILE, PixDefs.APP_EVENT_SAVE_PRESSED, null);
						editAccountInfoRequest();
					}
				});
    		}
    		
    	}
    	return mFragmentView;
    }

    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    try {
	    	mPhotoPickActivity = (PixPhotoPickerInterface)activity;
	    } catch (ClassCastException e) {
	    	//Utils.pixLog(TAG, activity.toString() +" must implement PixPhotoPickerInterface");
	    	throw new ClassCastException(activity.toString() +" must implement PixPhotoPickerInterface");
	    }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.registerPhotoPickerDelegate(this);
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mPhotoPickActivity != null) {
    		mPhotoPickActivity.unregisterPhotoPickerDelegate();
    	}
    	// release bitmaps
    	setAcountOrigPhoto(null);
    	setAccountPhoto(null);
    }
    
    private void restoreState(Bundle savedInstanceState) {
    	if (savedInstanceState == null) return;
    	this.fetchedUserInfo = savedInstanceState.getBoolean(EDIT_PROFILE_FETCHED_USER_INFO, false);
    	this.profileOrigTypePix = savedInstanceState.getBoolean(EDIT_PROFILE_ORIG_TYPE_PIX, false);
    	String origEmail = savedInstanceState.getString(EDIT_PROFILE_ORIG_EMAIL);
    	if (Utils.isValidString(origEmail)) {
    		this.profileOrigEmail = origEmail;
    	}
    	String origFullname = savedInstanceState.getString(EDIT_PROFILE_ORIG_FULLNAME);
    	if (Utils.isValidString(origFullname)) {
    		this.profileOrigFullname = origFullname;
    	}
    	String validEmail = savedInstanceState.getString(EDIT_PROFILE_VALIDATED_EMAIL);
    	if (Utils.isValidString(validEmail)) {
    		this.validatedEmail = validEmail;
    	}
    	String origPictureUrl = savedInstanceState.getString(EDIT_PROFILE_ORIG_PICTURE_URL);
    	if (Utils.isValidString(origPictureUrl)) {
    		this.profilePictureUrl = origPictureUrl;
    	}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	if (outState != null) {
    		outState.putBoolean(EDIT_PROFILE_FETCHED_USER_INFO, this.fetchedUserInfo);
    		outState.putBoolean(EDIT_PROFILE_ORIG_TYPE_PIX, this.profileOrigTypePix);
    		if (Utils.isValidString(this.profileOrigEmail)) {
    			outState.putString(EDIT_PROFILE_ORIG_EMAIL, this.profileOrigEmail);
    		}
    		if (Utils.isValidString(this.profileOrigFullname)) {
    			outState.putString(EDIT_PROFILE_ORIG_FULLNAME, this.profileOrigFullname);
    		}
    		if (Utils.isValidString(this.validatedEmail)) {
    			outState.putString(EDIT_PROFILE_VALIDATED_EMAIL, this.validatedEmail);
    		}
    		if (Utils.isValidString(this.profilePictureUrl)) {
    			outState.putString(EDIT_PROFILE_ORIG_PICTURE_URL, this.profilePictureUrl);
    		}
    		if (accountPhoto != null && accountPhoto.getBitmap() != null) {
    			// store the photo to a temp file
    			String filePath = Utils.storeBitmapInTmpFile(accountPhoto.getBitmap(), PROFILE_PICTURE_TEMP_FILE_NAME);
    			outState.putString(EDIT_PROFILE_STORED_PROFILE_PICTURE_PATH, filePath);
    		}
    	}
    	super.onSaveInstanceState(outState);        
    }

    @Override
    public boolean onToolbarHomePressIntercepted() {
		// go back to the calling activity
		if (mActivity != null) {
			mActivity.finishFragment();
		}
		return true;
    }
    
    private void fetchUserInfo() {
    	// fetch user account information
        PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getString(R.string.loading), true);
        new GetAccountInfoHttpRequest(new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
				if (mActivity != null) {
					mActivity.onError(error);
				}
			}
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessage();
				if (response != null && response instanceof PixAccountInfo) {
					PixAccountInfo accountInfo = (PixAccountInfo)response;
					fetchedUserInfo = true;
					fillInAccountInfo(accountInfo);
				}
			}
		}).send();
    }
    
    private void fillInAccountInfo(PixAccountInfo accountInfo) {
    	if (accountInfo != null && getActivity() != null) {
	    	if (accountInfo.getFullName() != null && this.fullnameText != null) {
	    		this.profileOrigFullname = accountInfo.getFullName();
	    		this.fullnameText.setText(this.profileOrigFullname);
	    	}
	    	if (accountInfo.getEmail() != null && this.emailText != null) {
	    		this.profileOrigEmail = accountInfo.getEmail();
	    		this.emailText.setText(this.profileOrigEmail);
	    	}
	    	if (accountInfo.getPicture() != null && this.addPhotoBtn != null) {
	    		if (!accountInfo.getPicture().contains(PixDefs.PROFILE_DEFAULT_PHOTO)) {
	    			this.profileOrigHasImage = true;
	    			this.profilePictureUrl = accountInfo.getPicture();
	    			PixImagesLoader.loadImage(accountInfo.getPicture(), 
	    					new BitmapFetcherDelegate() {
								@Override
								public void onBitmapFetchFinished(String url, PixBitmapHolder bitmapHolder) {
									if (bitmapHolder != null) {
										setAccountPhoto(bitmapHolder);
										setAcountOrigPhoto(bitmapHolder);
										//Utils.pixLog(TAG, "setting fetched image");
										addPhotoBtn.setImageBitmapHolder(bitmapHolder);
									}
								}
								@Override
								public void onBitmapFetchFailed() {} // Do nothing.
							}, 
							Priority.HIGH);
	    		}
	    	}
	    	if (accountInfo.getPixAccount() == 1) {
	    		this.profileOrigTypePix = true;
	    	}
    	}
    }
    
    private void editAccountInfoRequest() {
    	PixEditAccountInfo editedAccountInfo = new PixEditAccountInfo();
    	
    	if (textHadChanged(this.profileOrigEmail, getEmailText())) {
    		if (!validateEmailAddress()) {
    			return;
    		}
    		if (!profileOrigTypePix && !validatePassword()) {
    			// non pixplit user who is adding email. verify password
    			return;
    		}
    		editedAccountInfo.setEmail(getEmailText());
    	}
    	
    	if (textHadChanged("", getPasswordText())) {
    		if (!validatePassword()) {
    			return;
    		}
    		editedAccountInfo.setPwd(getPasswordText());
    	}
    	
    	if (textHadChanged(this.profileOrigFullname, getFullnameText())) {
    		if (!validateFullname()) {
    			return;
    		}
    		editedAccountInfo.setFullName(getFullnameText());
    	}
    	
    	if (accountPhoto == null) {
    		if (this.profileOrigHasImage) {
    			editedAccountInfo.setDelimg(1);
    		}
    	} else {
    		if (accountPhoto != profileOrigAcountPhoto &&
    				accountPhoto.getBitmap() != null) {
    			Bitmap scaledAccountPhoto = Utils.scaleBitmap(accountPhoto.getBitmap(), 
    					PixDefs.PROFILE_BITMAP_SCALE_WIDTH,
    					PixDefs.PROFILE_BITMAP_SCALE_HIGHT);
    			if (scaledAccountPhoto != null) {
	    			editedAccountInfo.setImg(scaledAccountPhoto);
	    			// remove old image from cache
//	    			if (Utils.isValidString(this.profilePictureUrl)) {
//	    				ImagesLoader.getInstance().removeImage(this.profilePictureUrl, ImagesLoader.IMG_PROFILE_THUMB);
//	    			}
    			}
    		}
    	}
    	// send the edited account information to the server
    	PixProgressIndicator.showMessage(getActivity(), PixApp.getContext().getString(R.string.loading), true);
    	new EditAccountInfoHttpRequest(new RequestDelegate() {
			@Override
			public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
				PixProgressIndicator.hideMessageWithIndication(false, PixApp.getContext().getString(R.string.failed));
				if (mActivity != null) {
					mActivity.onError(error);
				}			
			}
			@Override
			public void requestFinished(BaseHttpRequest request, PixObject response) {
				PixProgressIndicator.hideMessageWithIndication(true, PixApp.getContext().getString(R.string.done));
				if (getActivity() != null) {
					Intent result = new Intent();
					result.putExtra(PixDefs.INTENT_EXTRA_PROFILE_WAS_EDITED, true);
					getActivity().setResult(Activity.RESULT_OK, result);
					getActivity().finish();
				}
			}
		}, editedAccountInfo).send();
    }
    
    private boolean textHadChanged(String origText, String newText) {
    	if (newText == null) {
    		if (origText != null) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    	if (origText == null) {
    		if (newText != null) {
    			return true;
    		}
    	}
    	// both texts are not null
    	if (!origText.equalsIgnoreCase(newText)) {
    		return true;
    	}
    	
    	return false;
    }
    
    private boolean validateEmailAddress() {
    	String email = getEmailText();
    	if (!textHadChanged(this.profileOrigEmail, email)) {
    		return true;
    	}
    	
    	if (Utils.isValidString(email)) {
    		if (validatedEmail != null && email.equalsIgnoreCase(validatedEmail)) {
    			return true;
    		} else {
	    		if (!Utils.isValidEmail(email)) {
	    			onInvalidEmail(EMAIL_INVALID_MAIL);
	    			return false;
	    		}
	    		// check if the mail exists in the server
	    		if (emailValidationSpinner != null) {
	    			emailValidationSpinner.setVisibility(View.VISIBLE);
	    			emailValidationSpinner.startAnimation(spinnerAnim);
	    		}
	    		new ValidateEmailHttpRequest(email, this).send();
    		}
    	} else {
			onInvalidEmail(EMAIL_INVALID_MAIL);
    	}
    	
    	return false;
    }

    private boolean validatePassword() {
    	// the password is considered as not valid until validation is done
    	boolean passwordIsValid = false;
    	
    	String password = getPasswordText();
    	if (Utils.isValidString(password)) {
    		if (password.length() < 6) {
    			Utils.showMessage(PixApp.getContext().getString(R.string.password_must_be_at_least_6_characters), true);
    		} else {
	    		Pattern passwordPattern = Pattern.compile("^[a-zA-Z0-9]+$");
	    		if (passwordPattern.matcher(password).matches()) {
	    			// password is valid
	    			passwordIsValid = true;
	    		} else {
	    			Utils.showMessage(PixApp.getContext().getString(R.string.please_use_letters_a_z_or_numbers_0_9_), true);
	    		}
    		}
    	} else {
    		Utils.showMessage(PixApp.getContext().getString(R.string.password_must_be_at_least_6_characters), true);
    	}
    	
    	updatePasswordIcon(passwordIsValid);
    	return passwordIsValid;
    }
    
    private boolean validateFullname() {
    	boolean fullnameIsValid = false;
    	
    	String fullname = getFullnameText();
    	if (Utils.isValidString(fullname)) {
    		fullnameIsValid = true;
    	} else {
			Utils.showMessage(PixApp.getContext().getString(R.string.please_fill_in_your_details), true);
		}
    	
    	updateFullnameIcon(fullnameIsValid);
    	return fullnameIsValid;
    }
    
    
    private String getEmailText() {
    	if (emailText != null && emailText.getText() != null) {
    		return emailText.getText().toString();
    	}
    	return null;
    }
    
    private String getPasswordText() {
    	if (passwordText != null && passwordText.getText() != null) {
    		return passwordText.getText().toString();
    	}
    	return null;
    }
    
    private String getFullnameText() {
    	if (fullnameText != null && fullnameText.getText() != null) {
    		return fullnameText.getText().toString();
    	}
    	return null;
    }
    
    private void onInvalidEmail(int invalidCode) {
    	validatedEmail = null;
    	if (invalidCode == EMAIL_INVALID_MAIL) {
    		Utils.showMessage(PixApp.getContext().getString(R.string.please_enter_a_valid_email), true);
    	} 
    	if (invalidCode == EMAIL_INVALID_EXISTS) {
    		Utils.showMessage(PixApp.getContext().getString(R.string.a_user_with_that_email_already_exists), true);
    	}
    	updateMailIcon(false);
    }
    
    private void updateMailIcon(boolean valid) {
    	if (emailIcon != null) {
    		if (valid) {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_green);
    		} else {
    			emailIcon.setBackgroundResource(R.drawable.account_email_icon_red);
    		}
    	}
    }
    
    private void updatePasswordIcon(boolean valid) {
    	if (passwordIcon != null) {
    		if (valid) {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon_green);
    		} else {
    			passwordIcon.setBackgroundResource(R.drawable.account_password_icon_red);
    		}
    	}
    }
    
    private void updateFullnameIcon(boolean valid) {
    	if (fullnameIcon != null) {
    		if (valid) {
    			fullnameIcon.setBackgroundResource(R.drawable.account_name_icon_green);
    		} else {
    			fullnameIcon.setBackgroundResource(R.drawable.account_name_icon_red);
    		}
    	}
    }

	private void onAccountPhotoClick() {
		if (getActivity() != null) {
			new AccountPhotoDialog(getActivity(), this).show();
		}
	}

	// account photo delegate
	@Override
	public boolean hasAccountPhoto() {
		return (accountPhoto != null);
	}

	@Override
	public void takePhoto() {
		if (mPhotoPickActivity != null) {
			mPhotoPickActivity.photoCaptureRequest();
		}
	}

	@Override
	public void pickPhotoFromLib() {
		if (mPhotoPickActivity != null) {
			mPhotoPickActivity.photoPickRequest();
		}
	}

	@Override
	public void removePhoto() {
		accountPhoto = null;
		if (addPhotoBtn != null) {
			//Utils.pixLog(TAG, "removing photo");
			addPhotoBtn.setImageBitmapHolder(null);
		}
	}

	// photo picker delegate
	@Override
	public void onPhotoFetchSuccess(Bitmap selectedImage) {
		if (selectedImage != null) {
			//Utils.pixLog(TAG, "onPhotoFetchSuccess: got selectedImage. fragment instance:"+this);
			setAccountPhoto(new PixBitmapHolder(selectedImage));
			if (addPhotoBtn != null) {
				//Utils.pixLog(TAG, "onPhotoFetchSuccess: addPhotoBtn is not null");
				addPhotoBtn.setScaleType(ScaleType.CENTER_CROP);
				addPhotoBtn.setImageBitmapHolder(accountPhoto);
			} else {
				//Utils.pixLog(TAG, "onPhotoFetchSuccess: addPhotoBtn is null");
			}
		} else {
			//Utils.pixLog(TAG, "onPhotoFetchSuccess: selectedImage is null");
		}
	}

	@Override
	public void onPhotoFetchFail() {
		//Utils.pixLog(TAG, "onPhotoFetchFail");
	}

	@Override
	public void requestFinished(BaseHttpRequest request, PixObject response) {
		if (emailValidationSpinner != null) {
			emailValidationSpinner.setVisibility(View.GONE);
			emailValidationSpinner.setAnimation(null);
		}
		if (response != null && response instanceof PixEmailValidation) {
			PixEmailValidation validateEmailResponse = (PixEmailValidation)response; 
			// if we got here the email was validated in the server
			updateMailIcon(true);
			this.validatedEmail = validateEmailResponse.getEmail();
		}
	}

	@Override
	public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
		if (emailValidationSpinner != null) {
			emailValidationSpinner.setVisibility(View.GONE);
			emailValidationSpinner.setAnimation(null);
		}
		//Utils.pixLog(TAG, "validate email request failed");
		int errorCode = error.getCode();
		
		if (errorCode == PixError.PIX_SERVER_ERROR_INVALID_EMAIL) {
			this.onInvalidEmail(EMAIL_INVALID_MAIL);
		} else if (errorCode == PixError.PIX_SERVER_ERROR_EMAIL_EXIST) {
			this.onInvalidEmail(EMAIL_INVALID_EXISTS);
		} else {
			//Utils.pixLog(TAG, "Request finished with error:"+errorCode);
			// default here
			this.onInvalidEmail(EMAIL_INVALID_MAIL);
			if (mActivity != null) {
				mActivity.onError(error);
			}
		}
	}
	
	@Override
	public boolean onBackPressed() {
		PixAppState.logAppEvent(PixDefs.APP_SCREEN_EDIT_PROFILE, PixDefs.APP_EVENT_CANCEL_PRESSED, null);
		return false; // don't consume the back event
	}
}
