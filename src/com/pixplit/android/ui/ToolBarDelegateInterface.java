package com.pixplit.android.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplit.android.ui.activities.ToolBarDelegate;
import com.pixplit.android.ui.elements.PixImageView;

public interface ToolBarDelegateInterface {
	public void registerToolBarDelegate(ToolBarDelegate delegate);
	public void unregisterToolBarDelegate(ToolBarDelegate delegate);
	
	public void setToolbarVisible(boolean visible);
	public void setToolBarTitle(String title);
	public void setToolBarCustomView(int resId);
	public void updateToolbarTitle(int resId);
	public PixImageView addToolbarImageBtn(int toolbarBtnId, OnClickListener onClickListener);
	public TextView addToolbarTextBtn(int bgResId, int toolbarBtnId, OnClickListener onClickListener, String btnText);
	public void addToolbarCustomView(View view, RelativeLayout.LayoutParams params);
}