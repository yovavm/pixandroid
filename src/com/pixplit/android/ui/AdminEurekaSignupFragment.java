package com.pixplit.android.ui;

import android.os.Bundle;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixObject;
import com.pixplit.android.data.PixUserInfo;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.globals.PixError;
import com.pixplit.android.http.request.AdminEurekaSignupHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest;
import com.pixplit.android.http.request.BaseHttpRequest.RequestDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;


public class AdminEurekaSignupFragment extends SignupFragment {
//  	private static final String TAG = "AdminEurekaSignupFragment";
	
	public static AdminEurekaSignupFragment newInstance() {
		AdminEurekaSignupFragment fragment = new AdminEurekaSignupFragment();
        Bundle b = new Bundle();
        b.putBoolean(PixDefs.FRAG_ARG_SECURED, true);
        fragment.setArguments(b);
        return fragment;
    }
	
    public void signupAtPixServer(PixUserInfo userInfo) {
		PixProgressIndicator.showMessage(getActivity(), 
				PixApp.getContext().getResources().getString(R.string.admin_creating_eureka_account), true);
		if (userInfo != null) {
			new AdminEurekaSignupHttpRequest(
					new RequestDelegate() {
						
						@Override
						public void requestFinishedWithError(BaseHttpRequest request, PixError error) {
							PixProgressIndicator.hideMessageWithIndication(false, 
									PixApp.getContext().getResources().getString(R.string.failed));
						}
						
						@Override
						public void requestFinished(BaseHttpRequest request, PixObject response) {
							PixProgressIndicator.hideMessageWithIndication(true, 
									PixApp.getContext().getResources().getString(R.string.admin_eureka_account_created));
							PixAppState.logAppEvent(PixDefs.APP_SCREEN_SIGN_UP, PixDefs.APP_EVENT_EUREKA_ACCOUNT_CREATED, null);
							if (mActivity != null) {
								mActivity.finishFragment();
							}
						}
					},
					userInfo).send();
		} else {
			PixProgressIndicator.hideMessageWithIndication(false, 
					PixApp.getContext().getResources().getString(R.string.failed));
		}
	}
}
