package com.pixplit.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.pixplit.android.EFragmentType;
import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.globals.PixDefs;
import com.pixplit.android.ui.activities.ToolBarDelegate;
import com.pixplit.android.ui.elements.PixProgressIndicator;

public class PixBaseFragment extends Fragment implements ToolBarDelegate,
														 BaseActivityDelegate {
//  	private static final String TAG = "BaseFragment";
	
	private boolean isViewSecured = false;
	
	// Fragment View
	protected View mFragmentView = null;
		
	private boolean propagateTouch = false;
	
	protected PixActivityInterface mActivity;
	protected ToolBarDelegateInterface mToolbarController;
	protected ActivitiesControllerInterface mActivitiesController;
	
	// activity type
	private EFragmentType fragment_type = EFragmentType.PIX_FRAGMENT; // default
	
	// refresh logic
	private TextView refreshBtn;
	private int refreshBtnId = PixDefs.TOOLBAR_BTN_ID_REFRESH;
    private boolean setRefreshBtn = false; // default
	private boolean inRefreshProcess = false;
	Animation refreshBtnAnim;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    
        //Utils.pixLog(TAG, "==> onCreate called. savedInstanceState is " + ((savedInstanceState != null) ? "not null" : "null"));
        
        Bundle args = getArguments();
        if (args != null) {
        	isViewSecured = args.getBoolean(PixDefs.FRAG_ARG_SECURED, isViewSecured);
        }
		refreshBtnAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.spinning_anim);
    }
    
	@Override
	public void onStart(){
    	super.onStart();
    	//Utils.pixLog(TAG, "==> onStart called for "+fragment_type);
    	// catch clicks on the fragment and do nothing on them
    	if (mFragmentView != null) {
    		if (!this.propagateTouch) {
	    		mFragmentView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onFragmentClick();
					}
				});
	    	}
    	}
    	
		if (!PixAppState.UserInfo.isUserLogedin() &&
				isViewSecured) {
			// user is not logged-in. don't allow starting a secure view
			if (mActivity != null) {
				mActivity.finishFragment();
			}
		}
		
		if (setRefreshBtn && mToolbarController != null) {
			refreshBtnAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.spinning_anim);
			refreshBtn = mToolbarController.addToolbarTextBtn(R.drawable.refresh_btn, refreshBtnId, new OnClickListener() {
				@Override
				public void onClick(View v) {
					onActionBtnRefreshPressed();
				}
			}, null);
		}

    }
	
	@Override
    public void onResume() {
    	super.onResume();
    	//Utils.pixLog(TAG, "==> onResume called for "+fragment_type);
    }
	
	@Override
    public void onPause() {
    	super.onPause();
    	//Utils.pixLog(TAG, "==> onPause called for "+fragment_type);
    }
	
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	//Utils.pixLog(TAG, "==> onActivityCreated called for "+fragment_type);
    	if (mToolbarController != null) {
    		mToolbarController.registerToolBarDelegate(this);
    	}
    	if (mActivity != null) {
	    	mActivitiesController = mActivity.getActivitiesController();
	    	mActivity.registerBaseActivityDelegate(this);
    	}
	    
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	//Utils.pixLog(TAG, "==> onDestroy called for "+fragment_type);
    	if (mToolbarController != null) {
    		mToolbarController.unregisterToolBarDelegate(this);
    	}
    	if (mActivity != null) {
	    	mActivity.unregisterBaseActivityDelegate(this);
    	}
    	// dismiss any on going message
    	PixProgressIndicator.hideMessage();
    }

    protected void removeFragmentViewFromParent(){
    	ViewGroup parentViewGroup = (ViewGroup) mFragmentView.getParent(); 
		if (parentViewGroup != null) { 
			parentViewGroup.removeView(mFragmentView); 
		}
    }
    
	protected void onFragmentClick() {
		/*
		 *  Do nothing on clicks, all the children will take case of their clicks
		 *  This is needed in order for fragments on the back stack not to be reachable.
		 */
	}
    
    @Override
    public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    //Utils.pixLog(TAG, "==> onAttach called for "+fragment_type);
	    try {
	    	mActivity = (PixActivityInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement PixBaseActivityInterface");
	    }
	    try {
	    	mToolbarController = (ToolBarDelegateInterface)activity;
	    } catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString() +" must implement ActionBarDelegateInterface");
	    }
    }
    
    @Override
    public void onDetach () {
    	super.onDetach();
    	//Utils.pixLog(TAG, "==> onDetach called for "+fragment_type);
    	mActivity = null;
    	mToolbarController = null;
    }
        
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      super.onSaveInstanceState(savedInstanceState);
      //Utils.pixLog(TAG,"==> onSaveInstanceState called for fragment:"+fragment_type);
    }
        
    protected void setFragmentType(EFragmentType type){
    	fragment_type = type;
    }
    
    public EFragmentType getFragmentType(){
    	return fragment_type;
    }
    
    public boolean isSecured(){
    	boolean secured = isViewSecured;
    	Bundle args = getArguments();
    	if (args != null){
    		secured = args.getBoolean(PixDefs.FRAG_ARG_SECURED, secured);
    	}
    	return secured;
    }
     
    protected void setPropagateTouch(boolean propagateTouch){
    	this.propagateTouch = propagateTouch;
    }
    
    @Override
    public String toString(){
    	return fragment_type.toString();
    }
	
	/**
	 * @return true if the back press is consume by the fragment
	 */
	@Override
	public boolean onBackPressed() {
		return false;
	}

    protected void setRefreshBtn(boolean withRefreshBtn) {
    	this.setRefreshBtn = withRefreshBtn;
    }

	protected void onActionBtnRefreshPressed() {
		// Override by sub-classes fragments		
	}

	// Action Bar Delegate
	@Override
	public boolean onToolbarHomePressIntercepted() {
		// Override by sub-classes fragments
		return false; // return false to indicate that the event was not intercepted by default.
	}

	@Override
	public void onToolbarCenterClick(){
    	// Default is to do nothing
    }

	@Override
	public void onNewUnreadConversations(int unreadConversationsNum) {
		// Override by sub-classes fragments
	}

	@Override
	public void onNewNewsRecords(int newRecordsNum) {
		// Override by sub-classes fragments
	}

	@Override
	public void onNewActions(int newActionsNum) {
		// Override by sub-classes fragments
	}

	@Override
	public void onNewCompleted(int newCompletedNum) {
		// Override by sub-classes fragments
	}

	protected void setInRefreshProcess(boolean inRefresh) {
		if (this.inRefreshProcess != inRefresh) {
			this.inRefreshProcess = inRefresh;
			if (refreshBtn != null) {
				if (this.inRefreshProcess) {
					// we're refreshing, show the animation
					refreshBtn.setBackgroundResource(R.drawable.refresh_btn_selected);
					refreshBtn.startAnimation(refreshBtnAnim);
				} else {
					// not in refresh
					refreshBtn.setAnimation(null);
					refreshBtn.setBackgroundResource(R.drawable.refresh_btn);
				}
			}
		}
	}
}
