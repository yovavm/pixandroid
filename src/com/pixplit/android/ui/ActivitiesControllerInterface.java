package com.pixplit.android.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.util.bitmap.PixBitmapHolder;

public interface ActivitiesControllerInterface {
	// used to indicate that a profile was edited and therefore a refresh is required
	public static final String REFRESH_CONTEXT_PROFILE_EDITED = "REFRESH_CONTEXT_PROFILE_EDITED";
	// indicates that the compositions feed needs to refresh its content (i.e. in event of comp deletion)
	public static final String REFRESH_CONTEXT_COMPS_FEED = "REFRESH_CONTEXT_COMPS_FEED";
	
	public static final String RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT = "RUNNING_ACTIVITY_CONTEXT_PRIVATE_SPLIT";
	
    // activities transitions animations
    public static final int PIX_ANIMATE_NO_ANIMATION = 0;
    public static final int PIX_ANIMATE_SYSTEM_DEFAULT_ANIMATION = 1;
	public static final int PIX_ANIMATE_SLIDE_IN_RIGHT_TO_LEFT = 2;
	public static final int PIX_ANIMATE_SLIDE_IN_BOTTOM_UP = 3;

	// activities running state logic
	public void markActivityIsRunning(String context, boolean running);
	public boolean isActivityRunning(String context);
	// refresh activities logic
	public void markForRefresh(String context, boolean refresh);
	public boolean needRefresh(String context);
	///////////////////////////////////////////////////////
	//  start activities methods
	///////////////////////////////////////////////////////
	public void gotoPopularRequest(final Activity activity, Bundle args);
	public void gotoCompComments(final Activity activity, Bundle args);
	public void gotoCompRelated(final Activity activity, Bundle args);
	public void gotoSingleCompView(final Context context, Bundle args, int flags);
	public void gotoGalleryFeed(final Context context, Bundle args, int flags, ArrayList<PixComposition> comps);
	public void gotoProfileGalleryFeed(final Context context, Bundle args, int flags, ArrayList<PixComposition> comps);
	public void gotoCreateNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage);
	public void gotoScalePhotoInSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage, PixBitmapHolder activePartBitmap);
	public void gotoProcessNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage, PixBitmapHolder activePartBitmap);
	public void gotoPublishNewSplit(final Activity activity, Bundle args, PixBitmapHolder joinedCompImage, PixBitmapHolder activePartBitmap);
	public void gotoInviteFollowers(final Activity activity, Bundle args);
	public void gotoMyFeed(final Activity activity, int anim);
	public void gotoUserProfile(final Context context, Bundle args, int flags);
	public void gotoFollowFriends(final Activity activity, Bundle args);
	public void gotoSearchUsers(final Activity activity, Bundle args);
	public void gotoQuickGuide(final Activity activity);
	public void gotoSignUp(final Activity activity);
	public void gotoSignIn(final Activity activity);
	public void gotoSettings(final Activity activity);
	public void gotoSettingsNotifications(final Activity activity);
	public void gotoSettingsFacebook(final Activity activity);
	public void gotoResetPassword(final Activity activity);
	public void gotoNews(final Activity activity);
	public void gotoInfo(final Activity activity);
	public void gotoWebView(final Activity activity, Bundle args);
	public void gotoInbox(final Context context, int flags);
	public void gotoChat(final Context context, Bundle args, int flags);
	public void gotoUsersList(final Activity activity, Bundle args);
	public void gotoThemeFeed(final Activity activity, Bundle args);
	public void gotoChallengesFeed(final Activity activity);
	public void gotoEditProfile(final Activity activity, Bundle args);
	public void gotoAdminSettings(final Activity activity);
	public void gotoAdminPopularFull(final Activity activity);
	public void gotoAdminPopularCandidates(final Activity activity, Bundle args);
	public void gotoPickInstagramPhoto(final Activity activity);
	public void gotoPickFacebookPhoto(final Activity activity);
	public void gotoPickGooglePhoto(final Activity activity);
	public void gotoAdminSuggestedFull(final Activity activity);
	public void gotoAdminAddEurekaAccount(final Activity activity);
	public void gotoAdminEurekaAccounts(final Activity activity);
}
