package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.data.PixUser;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.util.viewholders.UserViewHolder;
import com.pixplit.android.util.viewholders.UserViewHolder.UserActionsDelegate;

public class UsersListAdapter extends PixFilterableUsersListAdapter {
	
	ArrayList<PixUser> unFilteredData = null;
	UserActionsDelegate userActionsDelegate;
	
	public static final int USERS_LIST_ACTION_NONE = 0;
	public static final int USERS_LIST_ACTION_FOLLOW = 1;
	public static final int USERS_LIST_ACTION_MESSAGE = 2;
	public static final int USERS_LIST_ACTION_SUGGESTED = 3;

	int userActionType;
	
	public UsersListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, int userActionType, UserActionsDelegate userActionsDelegate) {
            super(context, listDelegate, viewid);
            this.userActionType = userActionType;
            this.userActionsDelegate = userActionsDelegate;
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	UserViewHolder userViewHolder = new UserViewHolder(v, userActionType, userActionsDelegate);
    	userViewHolder.init(mListDelegate);
        
        return userViewHolder;
    }
    
}
