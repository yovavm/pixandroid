package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import com.pixplit.android.PixApp;
import com.pixplit.android.R;
import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.ui.CommentsFragment.CompCommentsUpdateDelegate;
import com.pixplit.android.util.viewholders.PixViewHolder;


public abstract class PixBaseListAdapter<T> extends BaseAdapter {
    private LayoutInflater mInflater;
    protected ArrayList<T> mDataObjects; // our generic object list
    private int mViewId;
    protected PixBaseListDelegate mListDelegate;
    
    public interface PixBaseListDelegate{
    	public boolean pagingEnabled();
    	public void loadNextPage();
    	public void onCompImageClick(PixComposition comp);
    	public void onCompImageClick(String compId);
    	public void onCompLikeClick(PixComposition comp, boolean liked);
    	public void onCompLikersClick(PixComposition comp);
    	public void onProfileClick(String username);
    	public void onCompCommentClick(PixComposition comp, CompCommentsUpdateDelegate delegate);
    	public void onCompRelatedClick(PixComposition comp);
    	public void onCompResplitClick(PixComposition comp);
    	public void onCompJoinClick(PixComposition comp);
    	public void onUserFollowClick(PixUser user);
    	public void onTagClick(String tag);
    	public void deleteCompRequest(String compId);
    	public boolean verifyUser();
    	public Context getContext();
    }

    public PixBaseListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, ArrayList<T> objects) {

		this(context, listDelegate, viewid);
        mDataObjects = objects;
        if (objects == null) {
                mDataObjects = new ArrayList<T>();
        }
    }
    
    public PixBaseListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {

        // Cache the LayoutInflate to avoid asking for a new one each time.
        mInflater = LayoutInflater.from(context);
        mViewId = viewid;
        mListDelegate = listDelegate;
        mDataObjects = new ArrayList<T>();
    }
    
    public void updateData(ArrayList<?> objects){

        if (objects == null) {
            mDataObjects = new ArrayList<T>();
        } else {
    		mDataObjects.addAll((ArrayList<T>)objects);
        }
        
        notifyDataSetChanged();
    }
    
    public ArrayList<T> getData(){
    	return mDataObjects;
    }
    
    public void changeData(ArrayList<?> objects){

        if (objects == null) {
            mDataObjects.clear();
        } else {
        	mDataObjects = (ArrayList<T>)objects;
        }
        
        notifyDataSetChanged();
    }
    
    public boolean hasData(){
    	return (mDataObjects != null && !mDataObjects.isEmpty());
    }
    
    /**
     * The number of objects in the list.
     */
    public int getCount() {
    	int count;
    	if (mListDelegate.pagingEnabled() && mDataObjects.size() > 0) {
    		count = mDataObjects.size() + 1;
    	} else {
    		count = mDataObjects.size();
    	}
        return count;
    }

    /**
     * @return We simply return the object at position of our object list Note,
     *         the holder object uses a back reference to its related data
     *         object. So, the user usually should use {@link ViewHolder#data}
     *         for faster access.
     */
    public Object getItem(int position) {
    	if (position < mDataObjects.size()) {
    		return mDataObjects.get(position);
    	}
    	return null;
    }

    /**
     * We use the array index as a unique id. That is, position equals id.
     * 
     * @return The id of the object
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row. This method is instantiated for each list
     * object. Using the Holder Pattern, avoids the unnecessary
     * findViewById()-calls.
     */
    public View getView(int position, View view, ViewGroup parent) {
    	if (position == mDataObjects.size()) {
    		view = mInflater.inflate(R.layout.loading_layout, null);
    		view.startAnimation(AnimationUtils.loadAnimation(PixApp.getContext(), R.anim.spinning_anim));
    		mListDelegate.loadNextPage();
    	} else {
            // A ViewHolder keeps references to children views to avoid uneccessary
            // calls
            // to findViewById() on each row.
            PixViewHolder holder;

            // When view is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the view supplied
            // by ListView is null.
            if (view == null) {
            	//Utils.pixLog("ImageWorker", "===========> new cell");
                view = mInflater.inflate(getViewId(position), null);
                // call the user's implementation
                holder = createHolder(view);
                // we set the holder as tag
                view.setTag(holder);
               
            } else {
            	//Utils.pixLog("ImageWorker", "===========> recycled cell");
                // get holder back...much faster than inflate
            	holder = (PixViewHolder) view.getTag();
            	if (holder != null) {
            		holder.onExitRecycleBin();
            	} else {
            		holder = createHolder(view);
                    // we set the holder as tag
                    view.setTag(holder);
            	}
            }

            view.setBackgroundResource(R.color.transparent);
            
            if (position < mDataObjects.size()) {
            	holder.bindData(getItem(position));
            }
            
            if (position == mDataObjects.size()-1 && !mListDelegate.pagingEnabled()) {
            	holder.handleLastCellFooter();
            }
    	}
        return view;
    }

    public void onHolderEnterRecycleBin(View view) {
    	if (view.getTag() != null && 
    			view.getTag() instanceof PixViewHolder){
    		((PixViewHolder)view.getTag()).onEnterRecycleBin();
    	}
    }
    
    /**
     * Creates your custom holder, that carries reference for e.g. ImageView
     * and/or TextView. If necessary connect your clickable View object with the
     * PrivateOnClickListener, or PrivateOnLongClickListener
     * 
     * @param vThe view for the new holder object
     */
    protected abstract PixViewHolder createHolder(View v);
    
    protected int getViewId(int position){
    	return mViewId;
    }
    
    @Override
    public int getItemViewType(int position) {
    	if (position < mDataObjects.size()) {
    		return 0;
    	} else {
    		return 1;
    	}
    }

    @Override
    public int getViewTypeCount() {
    	int viewTypeCount;
    	
    	if (mListDelegate.pagingEnabled()) {
    		viewTypeCount = 2;
    	} else {
    		viewTypeCount = 1;
    	}
    	return viewTypeCount;
    }

	public void removeItemAtIndex(int i) {
		if (mDataObjects != null && i >= 0 && i < mDataObjects.size()) {
			mDataObjects.remove(i);
			notifyDataSetChanged();
		}
	}      
}
