package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.widget.Filter;

import com.pixplit.android.data.PixUser;

public abstract class PixFilterableUsersListAdapter extends PixBaseListAdapter<PixUser> implements android.widget.Filterable{
	
	ArrayList<PixUser> unFilteredData = null;
	
	public PixFilterableUsersListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {
            super(context, listDelegate, viewid);
    }
    
   	@Override
	public Filter getFilter() {
		return new Filter() {
			
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				changeData((ArrayList<PixUser>) results.values);
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				ArrayList<PixUser> filteredResults = getFilteredResults(constraint);

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
			}
			
			private ArrayList<PixUser> getFilteredResults(CharSequence constraint) {
				ArrayList<PixUser> result = new ArrayList<PixUser>();
				if (unFilteredData == null) {
					unFilteredData = getData();
				}
				
				if (constraint != null && unFilteredData != null) {
					for (PixUser user : unFilteredData) {
						if (user.getFullName().toLowerCase()
                                .contains(constraint.toString()))
                            result.add(user);
					}
				}
				
				return result;
			}
		};
	}
    
   	protected ArrayList<PixUser> getUnFilteredData() {
   		if (unFilteredData != null) {
   			return unFilteredData;
   		}
   		return getData();
   	}
}
