package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.data.FacebookAlbum;
import com.pixplit.android.fb.FbAlbumPickDelegate;
import com.pixplit.android.util.viewholders.FbAlbumViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class FbAlbumsListAdapter extends PixBaseListAdapter<FacebookAlbum> implements FbAlbumPickDelegate{
	private FbAlbumPickDelegate mDelegate;
	
    public FbAlbumsListAdapter(Context context, PixBaseListDelegate listDelegate, FbAlbumPickDelegate delegate) {
    	super(context, listDelegate, R.layout.fb_album_layout);
    	mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	FbAlbumViewHolder albumViewHolder = new FbAlbumViewHolder(v);
    	albumViewHolder.init(this);

        return albumViewHolder;
    }

	@Override
	public void onAlbumPick(FacebookAlbum album) {
		if (mDelegate != null) {
			mDelegate.onAlbumPick(album);
		}
	}
}
