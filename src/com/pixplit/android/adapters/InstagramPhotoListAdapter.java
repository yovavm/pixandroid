package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.data.InstagramItem;
import com.pixplit.android.instagram.InstagramPhotoPickDelegate;
import com.pixplit.android.util.viewholders.InstagramThreepackViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class InstagramPhotoListAdapter extends PixBaseListAdapter<InstagramItem> implements InstagramPhotoPickDelegate{
	private InstagramPhotoPickDelegate mDelegate;
	
    public InstagramPhotoListAdapter(Context context, PixBaseListDelegate listDelegate, InstagramPhotoPickDelegate delegate) {
    	super(context, listDelegate, R.layout.instagram_threepack_layout);
    	mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	InstagramThreepackViewHolder instaItemsViewHolder = new InstagramThreepackViewHolder(v);
    	instaItemsViewHolder.init(this);

        return instaItemsViewHolder;
    }

	@Override
	public void onPhotoPick(InstagramItem item) {
		if (mDelegate != null) {
			mDelegate.onPhotoPick(item);
		}
		
	}
}
