package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.data.PixConversationPreview;
import com.pixplit.android.util.viewholders.ConversationPreviewViewHolder;
import com.pixplit.android.util.viewholders.ConversationPreviewViewHolder.ConversationPreviewDelegate;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class ConversationsListAdapter extends PixBaseListAdapter<PixConversationPreview>{
	ConversationPreviewDelegate mDelegate;
	
	public ConversationsListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, ConversationPreviewDelegate delegate) {
            super(context, listDelegate, viewid);
            this.mDelegate = delegate;
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	ConversationPreviewViewHolder conversationViewHolder = new ConversationPreviewViewHolder(v, mDelegate);
        conversationViewHolder.init(mListDelegate);
        
        return conversationViewHolder;
    }
}
