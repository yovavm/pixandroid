package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.data.PixComment;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.CommentViewHolder;
import com.pixplit.android.util.viewholders.CommentViewHolder.CommentViewDelegate;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class CommentsListAdapter extends PixBaseListAdapter<PixComment>{
	CommentViewDelegate mCommentDelegate;
	public CommentsListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, CommentViewDelegate commentDelegate) {
            super(context, listDelegate, viewid);
            mCommentDelegate = commentDelegate;
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	CommentViewHolder commentViewHolder = new CommentViewHolder(v);
        commentViewHolder.init(mCommentDelegate);
        
        return commentViewHolder;
    }
    
    public void deleteComment(String commentId) {
    	ArrayList<PixComment> commentsFeed = null;
		commentsFeed = (ArrayList<PixComment>) this.getData();
		if (commentsFeed != null) {
			for (PixComment commentsFeedItem : commentsFeed) {
				if (commentsFeedItem != null && 
						Utils.isValidString(commentsFeedItem.getId()) &&
						commentsFeedItem.getId().equalsIgnoreCase(commentId)) {
					commentsFeed.remove(commentsFeedItem);
					this.changeData(commentsFeed);
					break;
				}
			}
		}
    }
}
