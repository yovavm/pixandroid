package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixPopularComposition;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.util.viewholders.PopularCompViewHolder;
import com.pixplit.android.util.viewholders.PopularCompViewHolder.PopularCompHolderDelegate;
import com.pixplit.android.volley.PixImagesLoader;

public class PopularFullListAdapter extends PixBaseListAdapter<PixPopularComposition> implements PopularCompHolderDelegate{
	Context context;
	PopularFullAdapterDelegate mDelegate;
	
	public interface PopularFullAdapterDelegate {
		public void onAdminNotAuthenticated();
	}
	
	public PopularFullListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, PopularFullAdapterDelegate delegate) {
        super(context, listDelegate, viewid);
        this.context = context;
        this.mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	PopularCompViewHolder compViewHolder = new PopularCompViewHolder(context, v, mListDelegate, this);
    	compViewHolder.init();
    	return compViewHolder;
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent) {
    	View mView = super.getView(position, view, parent);
    	fetchCompImagesInBg(position);    	
    	return mView;
    }
    
    private static final int BG_FETCH_START_OFFSET = 2;
    private static final int BG_FETCH_END_OFFSET = 4;
    
    private void fetchCompImagesInBg(final int position) {
    	if (mDataObjects != null) {
    		int dataSize = mDataObjects.size();
    		for (int i = position+BG_FETCH_START_OFFSET; 
    				(i < dataSize && i < position+BG_FETCH_END_OFFSET); i++) {
    			PixComposition comp = mDataObjects.get(i);
    			if (comp != null && Utils.isValidString(comp.getImage())) {
    				PixImagesLoader.loadImageInBg(comp.getImage());
    			}
    		}
    	}
	}
    
	@Override
	public void onAdminNotAuthenticated() {
		if (mDelegate != null) {
			mDelegate.onAdminNotAuthenticated();
		}
	}
	
    public void deleteComp(String compId) {
    	ArrayList<PixPopularComposition> popularFeed = null;
		popularFeed = (ArrayList<PixPopularComposition>) this.getData();
		if (popularFeed != null) {
			for (PixPopularComposition comp : popularFeed) {
				if (comp != null && 
						Utils.isValidString(comp.getCompId()) &&
						comp.getCompId().equalsIgnoreCase(compId)) {
					popularFeed.remove(comp);
					this.changeData(popularFeed);
					break;
				}
			}
		}
    }

	@Override
	public void onPopularDeactivateClick(PixPopularComposition comp) {
		if (comp != null && Utils.isValidString(comp.getCompId())) {
			this.deleteComp(comp.getCompId());
		}
	}

	
}
