package com.pixplit.android.adapters;

import java.text.ParseException;
import java.util.Date;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.globals.PixAppState;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.time.PixServerDateFormat;
import com.pixplit.android.util.viewholders.NewsItemViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.util.viewholders.NewsItemViewHolder.NewsProviderDelegate;
import com.pixplit.android.data.PixNotification;

public class NewsListAdapter extends PixBaseListAdapter<PixNotification> implements NewsProviderDelegate{
	private Date lastUpdateDate = null;
	
    public NewsListAdapter(Context context, PixBaseListDelegate listDelegate) {
            super(context, listDelegate, R.layout.news_item_layout);
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	NewsItemViewHolder newsItemViewHolder = new NewsItemViewHolder(v);
    	newsItemViewHolder.init(mListDelegate, this);

        return newsItemViewHolder;
    }
    
    public void resetLastNewsTimestamp() {
        try {
			lastUpdateDate = PixServerDateFormat.getPixDateInstance().parse(PixAppState.getTimestamp(PixAppState.NEWS_TIMESTAMP));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	@Override
	public boolean isItemNew(String itemInsertDate) {
		boolean isNew = false;
		
		if (Utils.isValidString(itemInsertDate) &&
			lastUpdateDate != null) {
			try {
				Date itemDate = PixServerDateFormat.getPixDateInstance().parse(itemInsertDate);
				if (itemDate.compareTo(lastUpdateDate) > 0) {
					isNew = true;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return isNew;
		
	}
}
