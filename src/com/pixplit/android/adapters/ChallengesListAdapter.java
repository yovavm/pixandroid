package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.util.viewholders.ChallengeViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.data.PixComposition;

public class ChallengesListAdapter extends PixBaseListAdapter<PixComposition>{
	
	public ChallengesListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {
            super(context, listDelegate, viewid);
            // nothing to do
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	ChallengeViewHolder challengeViewHolder = new ChallengeViewHolder(v);
    	challengeViewHolder.init(mListDelegate);
        
        return challengeViewHolder;
    }
}
