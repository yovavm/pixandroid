package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;


public abstract class PixReverseListAdapter<T> extends PixBaseListAdapter<T> {

    public PixReverseListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {
        super(context, listDelegate, viewid);
    }

    public void updateData(ArrayList<?> objects){

        if (objects == null) {
            mDataObjects = new ArrayList<T>();
        } else {
    		ArrayList<T> listEnd = mDataObjects;
    		mDataObjects = (ArrayList<T>)objects;
    		mDataObjects.addAll(listEnd);
        }
        
        notifyDataSetChanged();
    }
    
    /**
     * The number of objects in the list.
     */
    public int getCount() {
    	int count = 0;
    	if (mDataObjects != null) {
    		count = mDataObjects.size();
    	}
    	return count;
    }  
}
