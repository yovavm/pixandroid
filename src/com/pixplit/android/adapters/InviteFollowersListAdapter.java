package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.util.viewholders.InviteUserViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.data.PixUser;

public class InviteFollowersListAdapter extends PixFilterableUsersListAdapter {
	
	public InviteFollowersListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {
            super(context, listDelegate, viewid);
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	InviteUserViewHolder userViewHolder = new InviteUserViewHolder(v);
    	userViewHolder.init(mListDelegate);
        
        return userViewHolder;
    }
    
    public ArrayList<String> getSelectedUsersList() {
    	ArrayList<String> selectedUsers = new ArrayList<String>();
    	ArrayList<PixUser> allUsers = getUnFilteredData();
    	if (allUsers != null) {
	    	for (PixUser user : allUsers) {
	    		if (user.isSelected()) {
	    			selectedUsers.add(user.getUsername());
	    		}
	    	}
    	}
    	return selectedUsers;
    }
    
}
