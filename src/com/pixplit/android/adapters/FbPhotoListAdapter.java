package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.data.FacebookPhoto;
import com.pixplit.android.fb.FbPhotoPickDelegate;
import com.pixplit.android.util.viewholders.FacebookThreepackViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class FbPhotoListAdapter extends PixBaseListAdapter<FacebookPhoto> implements FbPhotoPickDelegate{
	private FbPhotoPickDelegate mDelegate;
	
    public FbPhotoListAdapter(Context context, PixBaseListDelegate listDelegate, FbPhotoPickDelegate delegate) {
    	super(context, listDelegate, R.layout.instagram_threepack_layout);
    	mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	FacebookThreepackViewHolder newsItemViewHolder = new FacebookThreepackViewHolder(v);
    	newsItemViewHolder.init(this);

        return newsItemViewHolder;
    }

	@Override
	public void onPhotoPick(FacebookPhoto item) {
		if (mDelegate != null) {
			mDelegate.onPhotoPick(item);
		}
		
	}
}
