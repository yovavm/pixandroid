package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.data.PixUser;
import com.pixplit.android.util.viewholders.FollowUserViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class FollowFriendsListAdapter extends PixFilterableUsersListAdapter {
	
	public FollowFriendsListAdapter(Context context, PixBaseListDelegate listDelegate) {
            super(context, listDelegate, R.layout.follow_user_layout);
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	FollowUserViewHolder userViewHolder = new FollowUserViewHolder(v);
    	userViewHolder.init(mListDelegate);
        
        return userViewHolder;
    }
    
    public ArrayList<String> getSelectedUsersList() {
    	ArrayList<String> selectedUsers = new ArrayList<String>();
    	ArrayList<PixUser> allUsers = this.getUnFilteredData();
    	if (allUsers != null) {
	    	for (PixUser user : allUsers) {
	    		if (user.isSelected()) {
	    			selectedUsers.add(user.getUsername());
	    		}
	    	}
    	}
    	return selectedUsers;
    }
    
}
