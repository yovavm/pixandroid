package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.data.PixUser;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.util.viewholders.SuggestedUserViewHolder;
import com.pixplit.android.util.viewholders.UserViewHolder.UserActionsDelegate;

public class SuggestedUsersListAdapter extends UsersListAdapter {
	
	public SuggestedUsersListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, UserActionsDelegate userActionsDelegate) {
            super(context, listDelegate, viewid, UsersListAdapter.USERS_LIST_ACTION_SUGGESTED, userActionsDelegate);
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	SuggestedUserViewHolder userViewHolder = new SuggestedUserViewHolder(v, UsersListAdapter.USERS_LIST_ACTION_SUGGESTED, userActionsDelegate);
    	userViewHolder.init(mListDelegate);
        
        return userViewHolder;
    }
    
    public void removeUser(PixUser userToRemove) {
    	if (userToRemove == null || !Utils.isValidString(userToRemove.getUsername())) return;
    	String username = userToRemove.getUsername();
    	ArrayList<PixUser> usersFeed = null;
		usersFeed = (ArrayList<PixUser>) this.getData();
		if (usersFeed != null) {
			for (PixUser user : usersFeed) {
				if (user != null && 
						Utils.isValidString(user.getUsername()) &&
						user.getUsername().equalsIgnoreCase(username)) {
					usersFeed.remove(user);
					this.changeData(usersFeed);
					break;
				}
			}
		}
    }
    
}
