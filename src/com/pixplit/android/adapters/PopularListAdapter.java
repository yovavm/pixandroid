package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.util.viewholders.PixCompsPack;
import com.pixplit.android.util.viewholders.PixCompsSixpackViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class PopularListAdapter extends PixBaseListAdapter<PixCompsPack> {
	private int evenViewId;
	private int oddViewId;
	
    public PopularListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, int viewid2) {
            super(context, listDelegate, viewid);
            // set the custom rows layout
            evenViewId = viewid;
            oddViewId = viewid2;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	PixCompsSixpackViewHolder compsSixpackViewHolder = new PixCompsSixpackViewHolder(v);
    	compsSixpackViewHolder.init(mListDelegate);

        return compsSixpackViewHolder;
    }
    
    @Override
    protected int getViewId(int position){
    	int view = oddViewId;
    	
    	if (position%2==0){
    		view = evenViewId;
    	}
    	return view;
    }
    
}
