package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.PixCompositionViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.volley.PixImagesLoader;

public class CompsListAdapter extends PixBaseListAdapter<PixComposition>{
	public CompsListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid) {
            super(context, listDelegate, viewid);
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	PixCompositionViewHolder compViewHolder = new PixCompositionViewHolder(v, mListDelegate);
    	compViewHolder.init();
    	
    	return compViewHolder;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
    	View mView = super.getView(position, view, parent);
		fetchCompImagesInBg(position);
    	return mView;
    }
    
    private static int BG_FETCH_START_OFFSET;
    private static final int BG_FETCH_END_OFFSET = 4;
    
    private void fetchCompImagesInBg(final int position) {
    	if (mDataObjects != null) {
    		int dataSize = mDataObjects.size();
    		// in case the position is 0, also fetch the next cell image
    		BG_FETCH_START_OFFSET = (position == 0) ? 1 : 2;
    		for (int i = position+BG_FETCH_START_OFFSET; 
    				(i < dataSize && i < position+BG_FETCH_END_OFFSET); i++) {
    			PixComposition comp = mDataObjects.get(i);
    			if (comp != null && Utils.isValidString(comp.getImage())) {
    				PixImagesLoader.loadImageInBg(comp.getImage());
    			}
    		}
    	}
	}
}