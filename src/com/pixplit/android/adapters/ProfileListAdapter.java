package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.pixplit.android.util.viewholders.PixCompsPack;
import com.pixplit.android.util.viewholders.PixCompsThreepackViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class ProfileListAdapter extends PixBaseListAdapter<PixCompsPack> {
	
    public ProfileListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, ArrayList<PixCompsPack> objects) {
            super(context, listDelegate, viewid, objects);
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	
    	PixCompsThreepackViewHolder compViewHolder = new PixCompsThreepackViewHolder(v);
    	compViewHolder.init(mListDelegate);
        
        return compViewHolder;
    }
}
