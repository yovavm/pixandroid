package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.data.PixAccount;
import com.pixplit.android.util.viewholders.AdminAccountViewHolder;
import com.pixplit.android.util.viewholders.AdminAccountViewHolder.AccountViewDelegate;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class AccountsListAdapter extends PixBaseListAdapter<PixAccount>{
	AccountViewDelegate mDelegate;
	public AccountsListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, AccountViewDelegate delegate) {
            super(context, listDelegate, viewid);
            mDelegate = delegate;
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	AdminAccountViewHolder accountViewHolder = new AdminAccountViewHolder(v);
        accountViewHolder.init(mDelegate);
        return accountViewHolder;
    }
}
