package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;

import com.pixplit.android.R;
import com.pixplit.android.data.GoogleImage;
import com.pixplit.android.google.GooglePhotoPickDelegate;
import com.pixplit.android.util.viewholders.GoogleImagesThreepackViewHolder;
import com.pixplit.android.util.viewholders.PixViewHolder;

public class GoogleImagesListAdapter extends PixBaseListAdapter<GoogleImage> implements GooglePhotoPickDelegate{
	private GooglePhotoPickDelegate mDelegate;
	
    public GoogleImagesListAdapter(Context context, PixBaseListDelegate listDelegate, GooglePhotoPickDelegate delegate) {
    	super(context, listDelegate, R.layout.instagram_threepack_layout);
    	mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	GoogleImagesThreepackViewHolder newsItemViewHolder = new GoogleImagesThreepackViewHolder(v);
    	newsItemViewHolder.init(this);

        return newsItemViewHolder;
    }

	@Override
	public void onPhotoPick(GoogleImage item) {
		if (mDelegate != null) {
			mDelegate.onPhotoPick(item);
		}
		
	}
}
