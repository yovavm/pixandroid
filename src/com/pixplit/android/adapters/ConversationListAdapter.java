package com.pixplit.android.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.data.PixChatMessage;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.ChatMessageViewHolder;
import com.pixplit.android.util.viewholders.ChatMessageViewHolder.ChatMessageViewDelegate;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.volley.PixImagesLoader;

public class ConversationListAdapter extends PixReverseListAdapter<PixChatMessage>{
	ChatMessageViewDelegate chatDelegate;
	boolean fetchImagesInBg;
	
	public ConversationListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid, ChatMessageViewDelegate chatDelegate) {
            super(context, listDelegate, viewid);
            this.chatDelegate = chatDelegate;
    }
    
    @Override
    protected PixViewHolder createHolder(View v) {
    	ChatMessageViewHolder conversationViewHolder = new ChatMessageViewHolder(v, chatDelegate);
        conversationViewHolder.init(mListDelegate);
        return conversationViewHolder;
    }
    
    public void updateData(ArrayList<?> objects){
    	this.fetchImagesInBg = true;
    	super.updateData(objects);
    }

    public void changeData(ArrayList<?> objects){
    	this.fetchImagesInBg = true;
    	super.changeData(objects);
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent) {
    	View mView = super.getView(position, view, parent);
    	
    	if (fetchImagesInBg) {
    		fetchCompImagesInBg();
    		this.fetchImagesInBg = false;
    	}
    	
    	return mView;
    }
    
    private void fetchCompImagesInBg() {
    	if (mDataObjects != null) {
			int dataSize = mDataObjects.size();
			for (int i = dataSize-2; i >=0; i--) {
				PixChatMessage chatMsg = mDataObjects.get(i);
				if (chatMsg != null &&
						chatMsg.getComp() != null &&
	    				Utils.isValidString(chatMsg.getComp().getImage())) {
					PixImagesLoader.loadPrivateImageInBg(chatMsg.getComp().getImage());
	    		}
			}
		}
    }
}
