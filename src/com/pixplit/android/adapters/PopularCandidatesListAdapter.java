package com.pixplit.android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.pixplit.android.data.PixComposition;
import com.pixplit.android.data.PixPopularComposition;
import com.pixplit.android.util.Utils;
import com.pixplit.android.util.viewholders.PixViewHolder;
import com.pixplit.android.util.viewholders.PopularCandidateViewHolder;
import com.pixplit.android.util.viewholders.PopularCandidateViewHolder.PopularCandidateViewDelegate;
import com.pixplit.android.volley.PixImagesLoader;

public class PopularCandidatesListAdapter extends PixBaseListAdapter<PixPopularComposition> 
													implements PopularCandidateViewDelegate{
	Context context;
	PopularCandidatesListDelegate mDelegate;
	
	public interface PopularCandidatesListDelegate {
		public void onCandidateApproved(String compId);
		public void onCandidateDiscard(String compId);
		public void onAdminNotAuthenticated();
	}
	
	public PopularCandidatesListAdapter(Context context, PixBaseListDelegate listDelegate, int viewid,
										PopularCandidatesListDelegate delegate) {
        super(context, listDelegate, viewid);
        this.context = context;
        this.mDelegate = delegate;
    }

    @Override
    protected PixViewHolder createHolder(View v) {
    	PopularCandidateViewHolder compViewHolder = new PopularCandidateViewHolder(context, v, mListDelegate, this);
    	compViewHolder.init();
    	return compViewHolder;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
    	View mView = super.getView(position, view, parent);
    	fetchCompImagesInBg(position);
    	return mView;
    }
    
    private static final int BG_FETCH_START_OFFSET = 2;
    private static final int BG_FETCH_END_OFFSET = 4;
    
    private void fetchCompImagesInBg(final int position) {
    	if (mDataObjects != null) {
    		int dataSize = mDataObjects.size();
    		for (int i = position+BG_FETCH_START_OFFSET; 
    				(i < dataSize && i < position+BG_FETCH_END_OFFSET); i++) {
    			PixComposition comp = mDataObjects.get(i);
    			if (comp != null && Utils.isValidString(comp.getImage())) {
    				PixImagesLoader.loadImageInBg(comp.getImage());
    			}
    		}
    	}
	}

	@Override
	public void onCandidateApproved(String compId) {
		if (this.mDelegate != null) {
			this.mDelegate.onCandidateApproved(compId);
		}
	}

	@Override
	public void onCandidateDiscard(String compId) {
		if (this.mDelegate != null) {
			this.mDelegate.onCandidateDiscard(compId);
		}
	}

	@Override
	public void onAdminNotAuthenticated() {
		if (mDelegate != null) {
			mDelegate.onAdminNotAuthenticated();
		}
	}
}
